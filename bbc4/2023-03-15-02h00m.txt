TRANSCRIPTION: BBC4_AUDIO/2023-03-15-02H00M.MP3
--------------------------------------------------------------------------------
 Coming up next, it's business matters right here on the BBC World Service.
Today Mark Zuckerberg's Metaverse was meant to transform the way we work, play
and interact. So why is it causing the Meta boss quite so many headaches? Meta
clearly knows that it needs to fix its short-term issues and it also needs to
make sure that it remains competitive as new threats to its business like AI for
example arise. And in order to do that it also needs to sunset projects that
aren't working or are expensive. Also today we hear from the boss hoping she's
ridden out the storm caused by Silicon Valley banks collapse but she fears other
tech startups to come won't be quite so lucky. And Bali's clamping down on
shirtless motorbike riders. Would that stop you holidaying there? Join us for
business matters, shirts on, off, whatever you prefer, after the latest world
news. Hello I'm Moira Alderson with the BBC News. The United States has accused
Russia of reckless and unprofessional behaviour after alleging a Russian fighter
jet forced an American surveillance drone to crash into the Black Sea.
Washington has summoned the Russian ambassador for an explanation. Moscow denies
hitting the drone and insists it lost control before crashing. Gary O'Donoghue
has the latest. We've been learning some interesting details. Apparently this
incident lasted for 30 or 40 minutes. Is it a serious escalation given where
it's happening in the Black Sea? Yes it is. It's a serious escalation. But it's
probably a calculated escalation by the Russians because if this had been a
plane with a US pilot on board and it had come down, this would have been much,
much worse. The former Pakistani Prime Minister Imran Khan has accused the
authorities of acting outside the law in trying to arrest him. Mr Khan told the
BBC that the law of the land no longer held in Pakistan, it was the law of the
jungle. On Tuesday, police clashed with supporters of Mr Khan outside his
residence as they tried to detain him on a court order. Our Pakistan
correspondent Caroline Davis, who interviewed Mr Khan, had this assessment.
Imran Khan has said that the cases against him are politically motivated and of
course the other side say that no, these are things that Imran Khan has done
wrong, these are cases that are relating back to his time in power. Imran Khan's
party, the PTI, say that there are 78 cases now against him in court and there's
quite a lot of intricacies here. At the moment, he has not been arrested and we
are waiting to hear if there are any further developments. The US Secretary of
State, Antony Blinken, is in Ethiopia to assess the implementation of the peace
deal that ended the country's civil war. Later, he'll hold talks with political
leaders in Addis Ababa. The two-year civil war between Ethiopia's federal
government and the regional Tigrayan administration is thought to have claimed
more than half a million lives. More than 200 people are now known to have died
in Malawi and Mozambique when Tropical Storm Freddie struck the countries for a
second time at the weekend. Forecasters say the storm is one of the most
powerful and long-lasting on record. Our correspondent Shingai Nyoka says
questions are being asked over the impact of climate change. The understanding
is that Africa, in particular Southern Africa, is one of the worst affected by
climate change, but it contributes so little to the carbon emissions. There have
been calls as a result of the cyclone for the Western nations to do more, to
make sure that those who are affected are set up in conditions that befit their
dignity, for example, that they're not just thrown into tents. And there's also
a call for the Western nations to contribute more money when these humanitarian
disasters occur, as they have in this case. Shingai Nyoka reporting. World News
from the BBC. Two different types of severe weather are affecting the opposite
sides of the continental United States. The latest atmospheric river storm
bringing intense rain from the Pacific Ocean is soaking California. Flash flood
alerts are in force and ten counties have evacuation orders in place. Meanwhile,
on the eastern seaboard, heavy snowfalls and strong winds have closed many roads
and schools in New York and New England. The technology start-up OpenAI has
released the latest version of the artificial intelligence chatbot that has
caused a global sensation. GPT-4 will initially be available to people who pay a
subscription for premium access, but it's already embedded in Microsoft's search
engine Bing. Here's our technology editor Zoe Kleinman. Dozens of people have
used chatGPT since it launched late last year. It can answer questions in a very
human-like way, as well as mimic styles. Popular requests include asking it to
write songs, poems, website copy and even school essays, although many teachers
have warned their students against it. The new version, GPT-4, can also respond
to images, draft emails and summarise text. In a demo, it answered a complicated
tax query which required processing lots of information, although the answer did
not get verified. The annual rate of inflation in Argentina has topped 100%, the
highest level in over 30 years. Official figures show the consumer price index
increased by just over 13% in the year to February. Argentina has been in
economic difficulty for years and three-quarters of the population now live in
poverty. Ecuador is demanding answers after a former minister convicted of
corruption escaped to Venezuela at the weekend. Maria de los Angeles Duarte had
spent more than two years holed up in Argentina's embassy in the Ecuadorian
capital, Quito. Argentina acknowledged on Tuesday the minister was now in its
embassy in Venezuela, but gave no details on how her transfer was made. BBC
News. Hello and welcome to Business Matters on the BBC World Service. I'm Will
Bain. Thanks for being with us on the programme today. We'll ask just what's
going on at Meta as the company behind Facebook, Instagram, WhatsApp announces
thousands of staff layoffs. Also today. It's been very nerve-wracking. It's been
a whirlwind. And the community of tech, the hub of innovation was really SVB, so
we've had a tremendous loss. Yeah, much more on the fallout from the collapse of
Silicon Valley Bank and what it means for start-ups and the wider banking system
to come. And visas to China are available for businesses and tourists from
Wednesday. I definitely think China should open a bit quicker because it's
definitely good for the economy and also for the people who work in the
industry. But the challenge is when you're looking at China, there's always a
politics inside that. Much more on what that means for the Chinese economy and
those hoping to travel there before we leave you today on Business Matters. As
always, well, you know, if you're a regular listener to the show, we're joined
by a pair of guests, Sushma Ramachandran, business journalist and columnist at
the Tribune newspaper. She's joining us from Delhi in India this morning.
Sushma, great to have you back with us as always. Thank you, Will. Good morning.
Good morning to you. And Fermin Koop, environmental reporter in Buenos Aires,
Argentina. Fermin, welcome back to Business Matters. Hi, great to be here. No,
great to have you with us. Plenty to get to. And as we heard in the news, a
couple of stories that are going to be very much on home territory for both our
guests that we'll come to throughout the hour here on the programme. We're going
to start, though, out in the Metaverse. Oh, hey, Mark. Hey, what's going on?
What's up, Mark? Whoa, we're floating in space. Who made this place? It's
awesome. Right. It's from a crater I met in L.A. This place is amazing. Buzz, is
that you? Of course, it's me. You know, I had to be the robot, man. I thought I
was supposed to be the robot. Yeah, laughs, gags, virtual cards. And you heard
them mention it there. Who made this place? Well, it was the brainchild of the
Facebook CEO, Mark Zuckerberg. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app. And he's the one who invented the Facebook app. And he's the
one who invented the Facebook app. And he's the one who invented the Facebook
app. And he's the one who invented the Facebook app. And he's the one who
invented the Facebook app. And he's the one who invented the Facebook app. And
he's the one who invented the Facebook app. And he's the one who invented the
Facebook app. And he's the one who invented the Facebook app. And he's the one
who invented the Facebook app. And he's the one who invented the Facebook app.
And he's the one who invented the Facebook app. And he's the one who invented
the Facebook app.