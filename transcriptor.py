import os
import sys
import textwrap
import whisper
import time
import shutil
import traceback



class Transcriptor:

    CHARACTERS_PER_LINE = 80
    DONE_FOLDER = '/dockerx/done_radio_transcripts'

    def __init__(self, model):
        self.model = model

    def transcribe(audio):
        # make log-Mel spectrogram and move to the same device as the model
        mel = whisper.log_mel_spectrogram(audio).to(model.device)

        # detect the spoken language
        _, probs = model.detect_language(mel)
        print(f"Detected language: {max(probs, key=probs.get)}")

        # decode the audio
        options = whisper.DecodingOptions()
        result = whisper.decode(model, mel, options)

        # print the recognized text
        print(result.text)

    def run(self, path, language='en'):
        out_file_path = ''.join(path.split('.')[:-1]) + '.txt'
        text_file_path = out_file_path.replace('_audio', '')

        if os.path.exists(text_file_path):
            print(f"Skipping {out_file_path}: file exists")
            self.move_to_done(path)
            return

        text_file_folder = '/'.join(text_file_path.split('/')[:-1])
        if not os.path.isdir(text_file_folder):
            os.mkdir(text_file_folder)

        result = self.model.transcribe(path, language=language)
        text = result["text"]

        with open(text_file_path, 'w') as out_file:
            out_file.write(f'TRANSCRIPTION: {path.upper()}\n')
            out_file.write('-' * self.CHARACTERS_PER_LINE + '\n')
            out_file.write('\n'.join(textwrap.wrap(text, width=self.CHARACTERS_PER_LINE)))
            
        self.move_to_done(path)

    def move_to_done(self, path):
        new_path = self.DONE_FOLDER + '/' + path
        new_folder = '/'.join(new_path.split('/')[:-1])
        if not os.path.isdir(new_folder):
            os.mkdir(new_folder)

        print('Moving' , path, 'to', new_path)
        shutil.move(path, new_path)


def main():
    language = sys.argv[1]
    file_paths = sys.argv[2:]
    model_name = os.getenv("MODEL", 'large-v2')
    print(model_name, language, file_paths)    
    t0 = time.time()
    print(f"[{time.time() - t0}] Loading model...")
    model = whisper.load_model(model_name)
    print(f"[{time.time() - t0}] Loaded model...")
    
    try:
        worker = Transcriptor(model)
        t0 = time.time()
        for file_path in file_paths:
            print(f"[{time.time() - t0}] Running on {file_path}...")        
            try:
                worker.run(file_path, language=language)
            except Exception as error:
                traceback.print_exc()

            print(f"[{time.time() - t0}] Finished {file_path}...")
            t0 = time.time()
    finally:
        model.cpu()
        del model.encoder
        del model.decoder
        del model
        whisper.torch.cuda.empty_cache()

    
if __name__ == '__main__':
    try:
        main()
    finally:
        print("Bye!")