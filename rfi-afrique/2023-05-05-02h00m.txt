TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-05-05-02H00M.MP3
--------------------------------------------------------------------------------
... Merci d'écouter RFI, les deux heures du matin à Paris, deux heures de moins
en temps universel.... Simon Bourtanbourg. À la une de l'actualité, la Russie
accuse les États-Unis d'avoir commandé l'attaque présumée des drones ukrainiens
contre le Kremlin. Washington dément et nie toute attaque, alors que les
bombardements continuent en Ukraine, Kiev a été la cible de nouvelles frappes
cette nuit. Crise diplomatique entre la France et l'Italie, Paris essaie de se
rattraper après les critiques du ministre de l'Intérieur français envers la chef
du gouvernement italien. Rome a déjà décidé d'annuler une visite diplomatique.
Et puis le nouvel an à Masryt est désormais jour férié au Maroc. Jusqu'ici, il
n'était pas reconnu officiellement. C'est désormais chose faite.... Moscou
accuse Washington qui dément. La Russie assure que les États-Unis étaient à la
manœuvre de l'attaque présumée contre la résidence de Vladimir Poutine. Un
mensonge selon la Maison-Blanche, de son côté l'Ukraine, nie également avoir
visé le Kremlin. Et alors que Vladimir Zelensky était en visite à l'Aïe où il
s'est rendu à la Cour pénale internationale, la Russie a une nouvelle fois
frappé Kiev cette nuit. Les attaques de Moscou redoublent d'intensité ces
derniers jours, notamment dans l'est de l'Ukraine. À une vingtaine de kilomètres
de Barmoutre, l'épicentre des combats, c'est la ville de Kostantinovska, qui est
régulièrement visée par des tirs russes. Reportage de nos envoyés spéciaux sur
place, Anastasia Bekio et Boris Vichit. Dans une cour d'immeuble, plusieurs
dizaines de personnes font la queue pour obtenir de quoi réparer leur
appartement endommagé par une attaque nocturne de drone. Antonina, une
retraitée, attend que sa fille vienne l'aider à porter un panneau en aggloméré,
des tasseaux de bois, des clous et un film polyhythilène. Grâce à Dieu, nous
venions de nous lever parce que notre chat était agité, on ne dormait pas. Il
était deux heures et demie du matin, on a entendu une sorte de bourdonnement
très fort, puis une explosion et nos vitres ont volé en éclat. De source
officielle, l'attaque a fait un blessé. Un bâtiment de deux étages a été
éventré, les vitres des immeubles alentours ont été soufflés. Vladimir
Kravchenko promène son fils de deux ans et demi, son appartement situé de
l'autre côté de la rue est indemne, mais l'inquiétude est toujours là. Notre rue
principale mène à Barmout et on pourrait être les prochains. On est très
inquiet, on scrute les informations qui viennent de là-bas. Déjà leur artillerie
arrive jusqu'à nous. Les Russes nous tirent dessus. Ils ont anéanti Barmout, ils
l'ont réduite en ruines. J'espère que notre ville ne connaîtra pas le même sort.
Vladimir et son épouse ont une valise toute prête et ils ont décidé de partir à
la campagne si la situation venait encore à se détériorer. Anastasia Becchio,
Boris Vichy, Constantinifka, RFI. Au Soudan, les combats continuent de faire
rage malgré une trêve qui devait commencer ce jeudi. La capitale Khartoum est
toujours plongée dans le chaos depuis maintenant 20 jours. L'armée du général
Al-Burhan se dispute le pouvoir avec les paramilitaires du général Emeti. Les
Nations Unies assurent avoir besoin de plus de 400 millions de dollars pour
aider les 860 000 soudanais qui pourraient fuir le pays d'ici le mois d'octobre.
Crise diplomatique ouverte entre la France et l'Italie sur le dossier de
l'immigration. Le chef de la diplomatie italienne a déjà annulé sa visite prévue
dans l'Hexagone. En cause, les propos tenus ce jeudi par le ministre français de
l'Intérieur, Gérald Darmanin, a déclaré que l'Italie et sa nouvelle première
ministre, Georgia Meloni, étaient, je cite, « incapables de régler les problèmes
migratoires sur lesquels elle a été élue », des propos inacceptables pour Rome,
Antreca. Le coup est parti sans sommation. À Rome, on croyait encore à un
rapprochement au Meloni-Macron. Après le sommet européen de Bruxelles, en mars,
la première ministre italienne s'était dite bien accueillie par ses partenaires
sur la question de la crise migratoire. On préparait une mission franco-
italienne de haut niveau en Tunisie. Les déclarations de Darmanin ont fait
l'effet d'une grande claque. Car en Italie, les centres d'accueil sont toujours
les mêmes. Depuis 15 ans, Comalem P12 a 400 places. Quand il en faudrait 2 ou 3
mille les jours où la mer est calme. Plusieurs maires de grandes villes comme
Milan ou Florence viennent de lancer un signal de détresse à l'État. Ils sont
débordés, sans ressources, pour accueillir les milliers de mineurs non
accompagnés débarqués sur les côtes italiennes et redistribués dans tout le
pays. Les débarquements de migrants en Italie sont déjà quatre fois plus
importants cette année que l'an passé. La situation devrait s'aggraver avec
l'été et l'Italie n'a annoncé aucune mesure d'envergure pour l'affronter.
Giorgia Melloni n'est pas seule responsable des insuffisances italiennes mais
elle n'a pris aucune mesure d'envergure. André Ca, Rome, R.F. Et pendant ce
temps-là, Emmanuel Macron lui poursuit ses déplacements en France. Le président
français s'est rendu en Charente-Maritime dans un lycée technique. L'occasion
pour lui de présenter une réforme pour faire des établissements professionnels
une cause nationale. Emmanuel Macron a annoncé un milliard d'euros
supplémentaires chaque année pour les lycées professionnels. Le président
français veut également une mesure en faveur des stagiaires. On doit aussi
valoriser davantage l'investissement des élèves. Les stages viennent compléter
la formation académique des élèves, ils ont donc beaucoup à y apprendre. A la
fois un savoir-faire, un savoir-être, on sait combien c'est important, c'est
porteur de qualification pour les élèves. Et il faut qu'on puisse garantir à ces
jeunes des stages de qualité dans lesquels, justement, ils les apprendront aux
côtés des entreprises et de leur maître de stage et en lien avec leurs
enseignants. Les élèves seront d'autant plus motivés qu'ils auront une indemnité
de stages progressives. Et je crois que c'est assez juste et que c'est cohérent
et que ça valorise aussi pour les élèves et leur famille cet engagement. 50
euros par semaine en première année de CAP et de seconde, 75 euros par semaine
en deuxième année de CAP et en première, et 100 euros par semaine en terminale.
Et cette indemnité de stage, c'est à la fois une mesure de justice et de mérite.
Et c'est un engagement fort de l'État qui prendra à sa charge l'indemnisation de
stage. Emmanuel Macron au micro de Valérie Gasse. Aux États-Unis, les banques
régionales, un peu plus sous pression, c'est désormais au tour de PacWest de
s'effondrer en bourse. L'établissement est désormais considéré comme le nouveau
maillon faible du système bancaire, dont sa chute PacWest a entraîné avec lui
tout le secteur. 12 milliards de dollars de marchandises vendues au mois de
mars, exportation record de l'Australie vers la Chine. Cette hausse intervient
alors que les relations économiques et diplomatiques entre Canberra et Pékin
sont difficiles depuis trois ans. Les explications d'Amélie Beaucourt. En 2020,
Pékin décide de bouder le charbon australien avant de lui rouvrir ses portes en
janvier dernier. Les résultats se font sentir aujourd'hui, plus 125%
d'exportation de ce combustible entre février et mars 2023. Autre dispute,
l'Australie est accusée de vendre son orge moins cher en Chine que chez elle.
Une pratique de dumping sévèrement punie d'un droit de douane prohibitif sur
l'exportation de la céréale. Mais cette querelle-là aussi est en passe d'être
apaisée. Canberra a indiqué en avril qu'elle suspendait la plainte déposée
auprès de l'Organisation Mondiale du Commerce. Et son voisin se donne trois mois
pour réexaminer le tarif qui lui a imposé. Ce réjouement progressif de la
situation coïncide avec l'arrivée au pouvoir il y a tout juste un an d'Antony
Albanese. Et de son gouvernement travailliste. C'est du côté diplomatique en
revanche qu'il y a le plus de nœuds à démêler. La semaine dernière, le Premier
Ministre a annoncé une refonte de son système militaire. Avec acquisition de
missiles et renforcement des ports à la clé. En 2021, déjà l'Australie signette
avec les États-Unis et le Royaume-Uni l'accord AUCUS. Pour se doter de sous-
marins à propulsion nucléaire. Un projet très critiqué par Pékin. Amélie
Beaucourt, au moins 130 morts dans les inondations au Rwanda. Une grande partie
du territoire rwandais a été dévastée par les fortes pluies de la nuit de mardi
à mercredi. Les recherches de victimes se poursuivent alors que des routes ont
été coupées et des milliers de maisons détruites. Au Maroc, le nouvel an à
Masryte, désormais jour férié officiel. Une décision du roi Mohamed VI. Au
royaume chérifien, le nouvel an berbère est célébré le 13 janvier. Mais
jusqu'ici, il n'était pas reconnu officiellement. C'est désormais chose faite. A
Rabat, les explications de Nadia Ben-Mafoud. Cela faisait des années que les
militants réclamaient cette décision. Dans un pays qui compte la plus importante
communauté berbère en Afrique du Nord, faire de Yenéer un jour de fête national,
férié et payé, c'est l'agoutissement d'un long combat, a réagi Adiladisou,
président de l'association des jeunes Amazigh de Tamasna. Selon le dernier
recensement de 2014, un quart de la population marocaine parle Tamazigh, la
langue berbère. Un chiffre contesté par les militants qui estiment que près de
la moitié de la population parle la langue. En 2011, le Tamazigh est inscrit
dans la constitution comme une langue officielle, mais il a fallu attendre 2019
pour l'adoption d'une loi officialisant la langue. Depuis, elle est enseignée
dans de nombreuses écoles du royaume, et la reconnaissance de la communauté
berbère et de sa culture comme une composante essentielle de l'identité
marocaine, s'est encore accélérée en 2021, lorsqu'Aziz Arhanouche, chef du
gouvernement d'origine berbère, a pris ses fonctions. En janvier 2023, la langue
amazigh est devenue une condition doctroie de la nationalité marocaine au même
titre que l'arabe, et le budget de cette année consacre 300 millions d'irames
pour améliorer la visibilité de la langue tamazigh dans l'espace public. Nadia
Benmafoud, Rabat et Refi. En football, Naples est sacré champion d'Italie ce
jeudi, 33 ans après leur dernier titre, les Napolitains remportent à nouveau le
Scudetto. Cinq journées avant la fin de championnat, il leur a suffi d'un nul
aux hommes de Luciano Spalletti pour décrocher le titre, c'est le nigérian
Victor Ozimene qui a marqué le but, qui offre à Naples le troisième titre de
champion d'Italie de son histoire. C'est la fin de ce journal, restez bien à
l'écoute de RFI dans 50 minutes, désormais votre prochain rendez-vous avec
l'information. Anga Musique et RFI Talent présentent Freddy Masamba en concert.
A l'occasion de la sortie de son nouvel album intitulé Trancestral, retrouvez
l'artiste Afrosoul Congolais sur la scène du Pan Peeper à Paris le 13 mai à 20h.
Plus d'infos sur pan-peeper.com Extracté par haltener meine Vous avez commencé
par réveiller ma curiosité. Couleur tropicale. Mais là vous captez mon
invention. Couleur tropicale avec Claudicia. Et Mathieu Salaber, Annabelle
Jogamandie, Lea Perera Zanutto, Victor Avendano, bon arrivé la famille. Comment
allez-vous? Il va falloir que ça aille, car la séquence de ce jour sera intense.
Voici Pepe Oleka. Tu le rasa? C'est déjà une diva, mais bien des bélemanes à
travers le monde ne la connaissent pas encore. Pepe Oléka, béninoise et pan-
africaine, son nouvel et dernier album en date s'intitule Ali Clan, qui signifie
« Là où les chemins se séparent ». Pepe Oléka, à l'instant pour Asiki, spécial
recommandation. Je voulais vous parler d'Haïti. Culturellement, historiquement,
ethniquement, le Bénin et Haïti sont liés. Je voudrais évoquer la passion du
compas, la plus populaire des musiques haïtiennes, exprimée par une Belge née à
Lubumbashi. D'ailleurs, en Haïti, pour évoquer les Africains, arrivés sur les
terres d'infortune des Caraïbes, ne dit-on pas « Neg Congo », tout est lié, je
vous dis. Voici Lous Enyakuza. Lous Enyakuza. Lous Enyakuza. Lous Enyakuza. Lous
Enyakuza. Lous Enyakuza. Chanteuse, autrice et compositrice, interprète
évidemment aussi, ex-manquin, Lous ne s'interdit rien. Elle rappe et elle chante
de la trap et du R&B et du zouk façon dancehall. Avez-vous remarqué cette
attirance, cette appétence de quelques chanteuses pour l'univers du Japon, pour
leur nom de scène, pour leur blaze sur scène, Aya Nakamura, Franco-Malienne,
Lous Enyakuza, Belgo-Congolaise et il y en a d'autres. Pour moi, celui qui a
ouvert la voie, c'est Keisha Adele, milieu des années 90. Dans l'actualité,
lorsque l'on évoque les Comores, c'est surtout pour rappeler l'émigration des
Comoriens vers Mayotte, l'une des îles de l'archipel des Comores devenue en 2011
le 101e département français. Mais ça dans couleur tropicale. J'ai pas besoin
que tu me le dises Ne t'approches plus de moi, je veux plus t'entendre J'ai plus
d'âme, plus d'rein-coeur J'ai attendu tout ce temps que tu reviennes à moi
Maintenant c'est trop tard et oui je ne suis plus là J'ai attendu tout ce temps
que tu reviennes à moi Que tu reviennes à moi Maintenant c'est trop tard, trop
tard Pour deux mois je t'ai étonné Et t'avais mis les voiles quand j'avais
besoin de toi T'as pas vu le mur ailleurs dont t'étais isolé Tu t'es trop
absenté tiens ça t'apprendra Stop moi tes stories Non je veux plus t'entendre
Stop moi toutes tes stories Tu vas perdre ton temps Oh baby sorry Non je veux
plus t'entendre Tes cheveux sortent de ma vie Maintenant tu me dis sorry, sorry
Et j'ai vu la lumière, j'ai vu ton vrai visage Tu m'avais l'air si sincère Mais
c'était qu'un mirage aujourd'hui tu me jettes la pierre Parce que mon coeur ne
bat plus Parce que mon coeur ne bat plus pour toi Baby je t'en prie C'est ce que
tu chantes comme ça tu penses tout arranger Je suis pas la meuf que t'as laissé
J'espère que tu te mets pas dans tes regrets Je t'ai attendu tout ce temps que
tu reviennes à moi Maintenant c'est trop tard et oui je ne suis plus là J'ai
attendu tout ce temps que tu reviennes à moi Que tu reviennes à moi maintenant
c'est trop tard, trop tard Tout de moi je t'ai étonné Et t'avais mis les voiles
quand j'avais besoin de toi T'as pas vu le mur ailleurs dont t'étais isolé Tu
t'es trop absent et tiens ça t'apprendra Stop moi tes stories, non je veux plus
t'entendre Stop moi toutes tes stories, tu vas perdre ton temps Oh baby sorry,
non je veux plus t'entendre T'es chier tout sort de ma vie, maintenant tu me dis
sorry, sorry Stop moi toutes tes stories, stop stop toutes tes stories baby J'ai
mis 6 faits comme moi, écoutez Couleur Tropical avec Claudicias C'est le Joker,
Big Daddy, Jekyll, Quat'Nuck, Claudicias, Raya C'est Morin, je suis en Couleur
Tropical Ici El Pantoulo de la Panterna, Krusta, Mix Premier On est en Couleur
Tropical avec Claudicias On est ensemble Oh, ça fait des 3, 3 chiennes
Collective de Flops puissants Dans la radio des gens Que l'on appelle RFI c'est
Couleur Tropical T'es maitre quand tu le beat finit, parce que on t'appellé Mia
Khalifa Y'a pas de concurrence, toi c'est toi sur moi, et c'est déjà qualifié La
caisse chapeau, bien miauleuse, non, bravo le faux Khalifray Faut pas
t'apprécier que ton foie Amado, il s'est bien calbré man A Benjamin il a
chapeau, à Coco si on me dit de chipper Big Ock, à Monfiel, c'est Cabo, à Pau et
ensemble, on d'abord à Cheke Ne le cachez en chapeau, tu mettez un petit
bouffet, pas pour chaté A Tanchouille, on d'abord pour chapeau, à Brunel, on
s'emmène pour à tuer le charbet Il a des goûts, la kia, son tir, la kia, son on,
la kia, la fat, la kia On veut faire du war, y'en a pas de raté, on est pas des
malades, tu joues ou la t'es Faut flasher le dj, va flasher le pain, t'y penses,
t'y penses, faut flasher la t'y... Aïe, y'a l'air Merci d'être sur mon gano,
c'est sur mon RFI Oh, tout le monde, c'est ma gano, c'est ma RFI Oh, tout le
monde, c'est ma gano, c'est ma RFI Oh, tout le monde, c'est ma gano, c'est ma
RFI Oh, tout le monde, c'est ma gano, c'est ma RFI Oh, tout le monde, c'est ma
gano, c'est ma RFI Oh, tout le monde, c'est ma gano, c'est ma RFI Oh, tout le
monde, c'est ma gano, c'est ma RFI Oh, tout le monde, c'est ma gano, c'est ma
RFI Inimini mani mare, je fais j'ai mon gros bala Petit à quatre par tête, maman
haute-ma Frère, quand j'ai ta folle, j'ai grillé la tonchache J'ai pas autant de
shit, j'aimais ta mienne au cosa Backwood, mais basse-moi dans le batman, calme
5, 6, 7, mais quel frappe Big boss, on maitrise, les filles sont des tristes On
t'inspire, mais faut jouer tes tristes Sur le terrain, les gars, on joue tennis
Toi t'es quoi, mais le chétif Maman, au cladis, y a shit à pas de cuisse On
t'inspire, le morgue du fétiche Et si t'as un salsa asymmétrique Y a les papas,
tu fais un bon beat Tu sais, le même comme à tes chiens, y'en se dit Frac, shit,
car il est bon, bitch Pour mon bébé, il a connu les bonnes trucs A l'écolo, il
se faut donner les bons prix Et ta flèche, il te fait ce bon spi Elle est trifa,
mais c'est des bonnes quittes Y'a tous ce qu'on appelle attisme Y'a fini ton
jet, checker pour mes bismales Même ça, si y'en a marre, c'est de cool, la
freestyle T'aimes le mood, le reste, je juin' dis-moi Même pour vous, mouillez,
qu'elle ne bêtisse pas Toi, la desa, c'est des sagas, t'maitrise pas Déjà, en
fait, y a besoin d'un p'tit sport A ça ma ganos, a ça ma api A ça ma ganos, a ça
ma api A ça ma ganos, a ça ma api A ça ma ganos, a ça ma api A ça ma ganos, a ça
ma api A ça ma ganos, a ça ma api A ça ma ganos, a ça ma api Le gars, chipé,
hein! J'ai entendu, il a bien chipé! Toutes les musiques d'Afrika et de ses
diasporas Marla est dans la place Et nous sommes tombés d'amour pour cette
rappeuse ivoirienne La Côte d'Ivoire de Coulibaly Bilon, Blais, Soros et Thiam
Ça manque de femme En musique aussi, il y a une nouvelle génération agissante
Marla en est la preuve Marla pour Akpi Et en featuring Tripa Nian Nian et Tchek
Kado Restez dans l'arène de la nouvelle génération Popola Missy et Aerefi La
Côte d'Ivoire de Coulibaly Le gars, chipé, hein! J'ai entendu, il a bien chipé!
Toutes les musiques d'Afrika et de ses diasporas Marla est dans la place Et nous
sommes tombés d'amour pour cette rappeuse ivoirienne La Côte d'Ivoire de
Coulibaly Restez dans l'arène de la nouvelle génération Popola Missy et Aerefi
Restez dans l'arène de la nouvelle génération Popola Missy et Aerefi Restez dans
l'arène de la nouvelle génération On ne fait que danser On ne fait que bouger On
ne fait que chouquer On ne fait que boucou et boucou Touche! Touche, touche,
touche, touche A la « touche », touche, touche Touche, touche, touche A la «
touche », touche, touche Oulembou Boucou et boucou Boucou et boucou Passons la
piagne Me cadama sous les puissances D'ex macan de rocorodem Il vous soigne de
rocorodem A l'émona budegga A l'émona gajou Il veut mes vans-mères Il veut mes
vans-mères Il voulait que j'aie ta mère Il voulait que j'aie Choué Il veut mes
vans-mères Il veut mes vans-mères Il voulait que j'aie ta mère N'oubliez pas
Alibi Eh yeah D'où les p... Com'elle est bonne Il voulait que j'aie ta mère Il
voulait que j'aie Choué Chacune de mes sacs Je ne sais pas que la Il veut mes
vans-mères Il vous soigne de rocorodem A l'émona budegga A l'émona gajou A
l'émona budegga Dans quelques minutes, la séquence consacrée aux auditeurs
leaders, c'est-à-dire vous, vous êtes les auditeurs leaders, nous écouterons
évidemment les notes vocales que vous nous avez laissées sur notre WhatsApp au
0033 637 42 60 22 24. Donc continuez, laissez vos messages, tous les sujets sont
permis, vous le savez, même les plus pan-africanistes et même les plus légers.
Tenez peut-être l'occasion pour vous de donner votre avis sur les nouveaux
talents d'Africa et de ses diasporas qui vont se succéder à l'instant le
chorégraphe, danseur et chanteur Popol Amisi. Il régla quelques chorégraphies
pour Falli, Pupa et Diamond Platinum, pour ne citer que « Confirmation », le
titre de la chanson de Popol Amisi. Toutes les musiques d'Africa, je vous disais
ça, c'est comme ça que ça se passe ici, nulle part ailleurs. Abel Zamani, fils
du Niger, un Mozart à lui tout seul, car c'est à l'âge de 8 ans qu'il enregistre
sa première chanson grâce à son père, Salif Balké du groupe Tandé. Ouais bon,
Mozart c'était un petit peu plus jeune, mais enfin bon vous avez compris, il
était très jeune, il a fait les choses très jeunes. Yeti Yetar, le titre, avec
en featuring, aux côtés d'Abel Zamani, Biza Kadey. Voici une femme au multiple
talent, humoriste et chanteuse, Eunice Zunon, d'Encouleur Tropical. Elle est une
chanteuse, un artiste, un artiste, un artiste, un artiste, un artiste, un
artiste, une artiste inconnue, cré observed, 오늘도, grâce à einet, un artiste que
est généralement делеux dans sa chanson. même j'ai peur. Savoir qu'on s'aime
trop pour vouloir se mentir même lorsque les mots nous manquent pour le dire. Se
voir pour contrepôts quand s'éveille le soleil et s'être renouveau quand
s'éveille le soleil. Oui j'ai la vie, y'a des orles au mon coeur jusqu'à moi
même j'ai peur. Je sais que beaucoup aimeraient que j'évoque encore l'affaire de
Baccaroma entre monsieur et madame. Mais non, non! Un iso-nom, nouvelle chanson,
parce que je l'aime. Est-ce une façon d'expliquer ce qui s'est passé dans le
bagarrement, dans le bagarrage? Non, non, non, non, non! On n'en parle pas. Nous
n'en parlons pas. Mais si vous le souhaitez, vous souhaitez revenir sur cette
bagarre mémorable vue sur les réseaux sociaux, et bien dans un hôtel, vous vous
souvenez, et bien cela vous est possible en déposant un message sur notre
WhatsApp au 0033 6 37 42 62 24. Vous pouvez, mais pas nous! Messieurs! Chalet!
Tata, tata, tata, page nocturne! Dans Coulé en tropical, le pouvoir est à la
musique! Bonsoir Coulé en tropical, bonsoir Clody, c'est Osoa Juste et Théodore
de Pylône-Germaine à l'Ouchat. Je vous souhaite une excellente émission. Hello
Clody, vous allez bien? C'est comment? C'est comment? Mon maître, je vous
appelle depuis la Guinée qu'on a créé. Vraiment, j'apprécie votre émission,
notre picard. Coola, coola, coola, big respect à vous Clody Cia. Salut grand-
fleur Clody, c'est Alex, depuis Nigeria, je dis big ob à la famille, et bonne
animation à tous les ma... Hey tonton Clody, c'est Jaurès depuis le Congo
Brazaville. Tous les jours, 21h10, se connecter à cette émission. Et en passant,
j'aimerais écouter le jeu qu'il y a. Merci. Oui bonsoir Clody, bonsoir à toutes
les équipes des techniques de couleur tropicale, à Cali Tjé qui fait la
différence. C'est moi, Khalilou, c'est mon technicien de la radio-nostalgie de
Caïd au Mali. J'ai le plaisir de vous rencontrer. Clody, bonsoir, ça va? Et la
famille? Je vous dis un grand bonsoir. Coucou, bye bye. C'est Jekyll, un bel
depuis Bonde. Tonton, vous vous êtes forts, vous êtes très forts, très forts,
très forts. Ah papa, Dieu est fort. C'est Yemen, Douboulaye, vivant au Ghana.
J'ai un message à passer aux chefs de dates africaines qui veulent s'accrocher
au pouvoir. Il y a des crises dans certains pays de la sous-légion. Il change
les anciens présidents que eux, ils sont démocrates et quittent le pouvoir après
leur demanda d'être des médiateurs. Si eux aussi veulent rester au pouvoir, qui
seront les médiateurs dans les crises? Je m'épris nommeré Béjaz, je m'épris
nommeré Béja Baga, j'étais nouveau félicité pour le combat qu'on remet ni. Je
voulais écouter une chanson dont le titre est il était une fois l'une de mes
artistes préférés, à l'occurrence Fanny Zizi. La terre peut s'arrêter de tourner
Mais je n'aimerais que te J'aurais beau te chasser de mes pensées Mais tourne,
tourne, tourne Passez, fais tourne Ta main là, toi Faire la tournée, tournée
Changer le continent de mon année Je demeure, mais je resterai finalement Je
serai tous Merci et bonjour à tous. Salut à tous les éditeurs du Coulère
Tropical. Moi c'est Hassan Diazoulis, depuis Bangui, la République
Centrafricaine. J'aime beaucoup cette émission. Mais j'ai un petit question à
vous poser, Monsieur Claudicia. Vous avez combien donné de carrière et vous avez
fait comment pour arriver à cette étape? Merci, il faut me donner une réponse
très claire s'il vous plaît. Coulère Tropical 00336 37 42 62 26 C'est une
génération consciente depuis Paris 1984, 2023 Je vous laisse faire le calcul Et
ça fait 28 ans que nous sommes sur RFI pour Coulère Tropical Elle est belle
cette histoire là Que je vais vous raconter Elle est belle cette France Qui m'a
cuit à bras ouverts J'aime la France qui unit Non la France que je juge,
multiculturelle et historique Paris, sa capital est célèbre pour ses
maisonnements Ses musées d'art classique Donc celui du Louvre, ses monuments
Comme la Tour Eiffel L'immense château de Versailles Les Champs Elysées Notre-
Dame de Paris Je m'y plaige, le gris Citroën du monde, je me permets de faire
couler mon encre Soyez pas qu'encre moi La paix, l'amour, le pardon, il entend
noir Blanc, marron, jaune On a les mêmes ancêtres, c'est quoi ces histoires Le
couleur de race, d'ethnie Aidez-moi Dis-ta Ritz & Moore que sa tête fait honte à
la France Dis-ta Sarkozy que l'histoire commence en Afrique noire Dis-ta Marine
Le Pen que je l'aime Mais pas sa politique Non, non, non Pas sa politique La
France est un babou La France est un colo La France de Gamel, Omar Sy, Absa Tou
La France de Zidane La France de Benzema La France de Cyril, Alnouna Les Niboss
L'histoire retient des gens qui l'ont marqué positivement Je pose ma première
pierre De l'édifice, m'éclate-moi Je vais bâtir mon empire, suffire d'un conduit
Servir la jeunesse Plus inquiète mon devenir Approche qu'on chante l'amour Viens
même qu'on fasse l'amour Viens qu'on parle de nos problèmes Et de nos soucis
majeurs De ce qui nous unit Pas ce qui nous divise Venez qu'on parle d'éducation
De délinquants siens Ça vous arrive de réfléchir De vous mettre à la patte des
autres Le monde serait mieux sans sa haine C'est moi qui le dis Million,
million, million Million, million Million, million Million, million Million,
million Million, million Million, million Million, million Million, million Oh
oh oh oh Big Délica Samouki nous écoutait depuis Bokeh, la localité de naissance
de Million. C'est lui, à l'instant, c'était son flow que vous venez d'apprécier.
C'est en juin 2022 qu'il sort son premier clip réalisé sur ce titre là que l'on
entend, dont on entend encore le Bach. Écoute ça, citoyen du monde, le message
est fort. Lui aussi, comme Valcero, a un regard sur cette France. Il se dit «
Ah, c'est quoi, c'est qui la France? ». La génération consciente en action, le
rap français des années 90-2000, furent aussi des années alibi Montana. Que
voici! Les années ont passé, mais rien n'a changé. Les miennes ont la dalle, je
veux les faire manger. On est guitarisé en cas de danger. Toujours, toujours
prêts pour la banaliser. Les années ont passé, mais rien n'a changé. Les miennes
ont la dalle, je veux les faire manger. Un plat, v'on y va Peu importe l'heure,
l'endroit Pour du bif, v'on y va Les mêmes accompagnent Après le casse, la puce
est cassée Tout est brûlé Une ou deux tasse, comptez les billets On peut plus
s'arracher Après le casse, la puce est cassée Tout est brûlé Une ou deux tasse,
comptez les billets On peut plus s'arracher Mais il a dit tout chaud chaud pour
un ibis Ici, arrêche à frérot J'ai les outils Qu'elle est que technique à qu'ils
enseignent Saint-Denis Je refrais à midi, j'ai tout claqué Dans la nuit, guitare
accordée L'orchestre a répété style de bandit En tédé sans un jour d'air tédé
Négocie, cagoule et gambier En cas de refus, on t'éteint Je me déplace pour
planta Où j'envoie deux gars pour planter On est guitarisé en cas de danger
Toujours, toujours prêt pour la banaliser Les années ont passé mais rien n'a
changé Les miens ont la dalle, je veux les faire manger On est guitarisé en cas
de danger Toujours, toujours prêt pour la banaliser Les années ont passé mais
rien n'a changé Les miens ont la dalle, je veux les faire manger Un plat, v'on y
va Peu importe l'heure, l'endroit Pour du bivou, on y va Les mêmes accompagnent
Après le casse, la puce est cassée Tout est brûlé Une ou deux tasse, comptez les
billets On peut plus s'arracher Après le casse, la puce est cassée Tout est
brûlé Une ou deux tasse, comptez les billets On peut plus s'arracher Hop Долго
znата� Super footy. Uuuuuuuh! Menu Martin 웃 Le titre, Sekai, le natif de Kaduna,
appartient au peuple Ibo. Sekai, à la scène, Shukuka et Kuan-Ii pour ses
parents. Comme couleur tropicale, Sekai est né en 1995. Une belle année, toi-
même tu sais. Depuis le 10 décembre, vous pouvez danser sur une des nouvelles
chansons de la choriste d'Alpha Blondie et de Bob Sinclar. Voici Valérie
Tribord. Coute ça, écoute ça, écoute ça. C'est toi. Il y a toujours du reggae
dans couleur tropicale. Toi-même tu sais. Wouah, écoute ça. Je ne sais pas ce
qui t'a conduit à moi. Alors que tout dans ma vie, il tourbléonne autour de moi.
Je sais que tu regardes au fond de moi. Et que tu vois des choses que je ne vois
pas. Mais est-ce que je saurais aimer un homme tel que toi? Alors que je n'ai
pas tout su ce qu'on vient pour moi. Je voudrais t'en te donner ce qu'il y a de
bon en moi. Et prendre de toi, 100% de toi. Parce que c'est toi. Parce que c'est
toi. Parce que c'est toi. Parce que c'est moi Ne me demande pas pourquoi Quand
je te réponds Que je ne sais pas Que je veux te dire Oui c'est tellement avec
toi Que je veux être là Pour enfin être moi Parce que c'est toi Parce que c'est
toi Parce que c'est toi Parce que c'est toi Parce que c'est toi Je ferai tout
pour toi Que tu sois loin ou près de moi Je serai toujours là Faisons pas se
fras Comme ci comme ça Juste vous ne perdons jamais Et elle commence avec du
très très très très lourd Il y a Mathieu Salabert en train de danser sur sa
chaise là Oh de l'autre côté de la vitre Il sait que cette chanson est
historique On en parle dans quelques instants La famille si vous nous rejoignez
à l'instant Bonne arrivée Table ronde Dépendance Table ronde Dépendance Table
ronde Dépendance Table ronde Dépendance Table ronde Dépendance Table ronde
Dépendance Table ronde Dépendance Table ronde Dépendance Table ronde Dépendance
Indépendance On a des pôres Du vieux serein On a aussi des hues Des hues en bas
On a des panneaux Des couloirs Et des couloirs On a des hues Et des pôres On a
des hues Des hues en bas On a des escloirs Des hues en bas La chevelée La
fleurante La spinole La rousseau On a des panneaux Table ronde Table ronde
Indépendance Table ronde Indépendance Table ronde Indépendance Le grand calais
Joseph Kabasele et son africain jazz pour Table ronde Cette chanson a
immortalisé ses instants historiques au cours desquels l'administration
coloniale belge et leader congolais ont dévoilé les conditions de l'indépendance
Durant les premiers pour parler, le mumba n'était pas présent car en prison
Alors, congolais et belges certains belges, ceux qui étaient pour l'indépendance
ont exigé la présence de Patrice et Meurée le mumba sous peine de cesser toute
négociation Les discussions se tardent du 20 janvier au 20 février puis du 26
avril au 16 mai 1960 bien sûr que le 20 janvier 1960 était plein d'espoir pour
les congolais Le 20 janvier 1989 est un jour de deuil pour les ivoiriens Fulgens
Cassi, homme de média à qui tant d'artistes doivent tant vient de mourir Musique
Dans le mumba Des hommes et des femmes sensibles à la chose musicale et capables
d'apprécier un bon chanteur et une belle orchestration musicale Quoi dire, vous
ne doutez pas vous ne doutez pas Le 20 janvier 1989 les lions c'était peine
Fulgens Cassi est parti trop vite Fulgens Cassi est parti trop vite Fulgens
Cassi est parti trop vite est parti trop vite Musique Nous t'appuyons Seigneur
Prends le vein toi Nous t'appelerons Seigneur Prends le vein toi Y a nous qui
s'y battent En écrouvant et pleurant Y a nous qui s'y battent En écrouvant et
pleurant Aidez-moi à pleurer Fulgens Cassi Aidez-moi à pleurer Mon homme mirojé
Aidez-moi Aidez-moi Musique Vendamment débattent Musique Venez Parer ses coups
Musique Venez Sous ma roue canardée Venez Si c'est l'image Venez Alpha blondie
Venez George Taibbenson Venez Daniel Bonny Venez Venez My Lennon-Gueilet Venez
Venez Aidez-moi à pleurer Fulgens Cassi célèbre reggae man ivoirien et du
continent africain, Serge Cassis pour un hommage à Fuljans Cassis. Cet homme de
média avait compris l'importance de la culture dans l'émancipation d'un peuple
et en particulier de la musique. Fuljans Cassis a révélé au grand public
ivoirien toutes les stars ivoiriennes des années 70-80, alpha blondie en tête.
Nous devons à Fuljans Cassis de célèbres émissions de la RTI, superstar,
station, première chance et puis podium aussi. Il était adulé bien au delà de la
Côte d'Ivoire. Le journaliste et animateur Fuljans Cassis est décédé le 20
janvier 89, il n'avait que 33 ans. 24 ans plus tard, ce sont les Antilles Guyane
qui furent endeuillés par la disparition d'Ebite Le Feuille. Ageni à ces
émotions, se plièst nous les plis, satisfaction, envie à l'égager, c'est ça
seulement nous commander, satisfaction, On joue des découvrares, ça qui est la
vérité Et oui, on joue des découvrares Dominique Perreuse, s'il vous plaît, au
piano Ampéance compas, c'est zouk mais c'est compas, coup de ça On va exploser,
on y est, échauffé C'est Mottus et Bouche Cousu Terminons cette séquence Gold
avec une chanteuse Martiniquez admiratrice d'Edith LeFell C'est Perle Lama Le
jour où Edith disait adieu à ce monde, Perle voyait son anniversaire de 2003
Gâché par la mort de notre petite sirène Perle Lama est donc née le 20 janvier
1984 à Fort-de-France Allez, c'est la fin avec un titre culte encore Emmène-moi
avec toi, Perle Lama Emmène-moi, Emmène-moi Salut Emmène-moi, oh oh Emmène-moi
Je pourrais rester là des heures Te regarder t'apprendre par cœur Je pourrais
rester là des heures Savourer chaque instant de bonheur Je garde ton sourire
dans chacun de mes souvenirs Ces mots que tu sais dire en livre mon corps de
désir Je pourrais rester là des heures Te regarder te savoir par cœur Je
voudrais partager ces heures Tout près de toi et nulle part ailleurs Je garde
dans ce souvenir ces moments qui me font frémir Je peux t'appartenir dix fois,
cent fois, je peux te dire Emmène-moi avec toi Baby garde-moi près de toi
Emmène-moi avec toi Lama Emmène-moi avec toi Baby garde-moi près de toi T'as
élacassé le moteur, c'est fini! Vous avec Jean-François Cadet du lundi au
vendredi à 15h10 TU RFI Tous les jours, Accent d'Europe propose de partager la
vie des Européens Frédéric Lebel Ce vendredi, le couronnement de Charles III au
Royaume-Uni est aussi une grande cérémonie commerciale. La famille Royale a
depuis longtemps joué la carte du marketing, une manne touristique qu'elle
utilise pour valoriser et défendre l'institution. Accent d'Europe, du lundi au
vendredi à partir de 19h10 TU, sur RFI.