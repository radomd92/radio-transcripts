TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-07-01H00M.MP3
--------------------------------------------------------------------------------
...... -"En raison d'un mot d'ordre de grève nationale, RFI n'est pas en mesure
d'assurer son programme habituel. Veuillez nous en excuser."...... I feel like
livin' under the sun I feel like livin' under the sun Laughin' under the sun
Laughin' baby! Laughin' baby! I feel like lovin' under the sun Oh, you got it,
oh I feel so good under the sun Il faut que les papiers soient arrêtés Je suis
la femme, la femme et même pas le être humain Vous savez, il y a encore des
nombres et des noms Si vous n'avez pas votre numéro, vous ne voulez pas vous
abonner Arranger un réseau Si vous n'avez pas ça, vous n'avez pas où aller Vous
n'avez pas le temps de rester en haine J'ai toujours attendu pour que la lausse
change J'ai commencé à boire pour tuer le temps L'aléviat, la paix et le vent
Restez le même à l'intérieur de votre frame Il y a de la place pour les routes
et les chaînes qui se débrisent Quelle est votre communauté de village Able de
vous donner votre part Où est-il comme les gens de mon pays Ils ne savent pas de
noms, les fous comptent les likes La vie est comme un rêve La crainte
scientifique est décollée par la perspiration Camine, personne n'est illégal
Caminons ensemble Camine comme tu veux Dans le jardin du monde Yo, un, deux, yo,
yo, yo On se fume la lumière Le micro est incroyable C'est quand je l'ai écrit
Avec un cible, je suis hyper Comme un rap rap Yo, on n'a pas de rap maintenant
Les histoires de la lutte C'est toujours le crime et le trouble L'ascendant, le
grouper et le ralentir La vie est subtile Le esprit peut changer C'est maintenu
C'est beau, c'est simple et simple C'est pas de jeu Le esprit est le roi Si tu
es au courant de ton échec Il te fait mal C'est toujours controversial Si tu
veux faire un tour Je passe par la service des choses C'est un circus wild C'est
le fait de kiffer les faits Ne relaxes pas Je suis un peu nerveux C'est Forum
Все-A 조 iOS et Honestly Comment tu es dans le verre de la moto J'ai une
nouvelle, j'ai une nouvelle à vous annoncer Une bonne nouvelle, une bonne
nouvelle Oh oui, une bonne, bonne, bonne, bonne problème c'est une bonne
nouvelle Attends laisse-moi réfléchir, ouais deux secondes, dix secondes,
laisse-moi t'expliquer Aujourd'hui je suis levé dans l'aide de mon réveil Il n'a
pas sonné, je l'avais jeté la veille Je me suis levé du bon pied, grand soleil
dans le ciel Parfait de me reposer à côté de ma belle Après un petit café, deux,
trois tartines de miel Un jus d'orange frais sel, je finis la vaisselle J'ai
besoin de cesser, j'ai une super nouvelle Tous mes dossiers sont battus à la
poubelle Oui c'est une nouvelle vie pour moi J'ai plaqué au boulot, s'en dit la
tête de l'eau Un tout autre avenir sort par moi J'ai comme gagné au loto, comme
décoché le gros lot Soit habite nouvelle vie, je vais la boire au boulot Je vais
enfin être libre, à moi l'aventure Fini cet équilibre entre raison et nature À
moi la passion, les grands espaces et l'artur J'ai besoin d'action, le grand
amour c'est moi sûr Plus d'auto-censure, on va pas se mentir Ça fait longtemps
que ça dure, on peut plus sentir C'est ton amant, si cela te rassure Garde là
pas peu, moi je prends les clés de la voiture Oui c'est une nouvelle vie pour
moi J'ai enfin plaqué ma gorge, s'en dit la tête de l'eau Un tout autre avenir
sort par moi J'ai comme gagné au loto, comme décoché le gros lot Soit habite
nouvelle vie, je vais la boire au boulot J'avais plus vie Complètement aliéné,
jamais laissé tomber Je ne portais plus d'amour, oui je filais tout droit Dans
mes engagements, non j'avais plus de temps Mais ça c'était avant Maintenant ma
vie est bien plus que parfaite Je peux sortir et puis faire la fête Y'a plus
personne pour me prendre la tête Mais bon en ce moment je me sens un petit peu
bête Comme j'ai plus de sable, ouais ben j'ai moins d'argent Ouais je sors moins
souvent, comme je sors moins souvent Ben je rencontre moins de gens Et comme
j'ai plus de meufs, ben je suis sale tout le temps Oui c'est une nouvelle vie
pour moi Il faut que je retrouve un boulot Si je veux avoir une gosse Un tout
autre à venir sort pas moi C'est comme la drogue au loto, c'est comme la telle
gros lot C'était une bonne ancienne vie, maintenant je crois au boulot Oui c'est
une nouvelle vie pour moi J'aurais dû gagner ma gosse Et retourner au boulot Un
tout autre à venir sort pas moi J'aurais dû gagner l'auto, de décrocher le gros
lot Nostalgie de ma vie, maintenant je crois au boulot Tu racontes ma life Fais
dans ce que tu veux J'ai plus de travail Et puis t'vas tout de suite revenir Tu
racontes ma life Alors fais dans ce que tu veux J'ai plus de travail J'ai plus
de travail Et puis t'vas tout de suite t'enverre Tu racontes ma life Alors fais
dans ce que tu veux J'ai plus de travail Et puis t'vas tout de suite t'enverre
J'ai plus de travail Et puis t'vas tout de suite t'enverre J'ai plus de travail
J'ai plus de travail Samedi, à partir de 9h10 TU RFI C'est pas du vent Le
magazine de l'environnement sur RFI Anne-Cécile Parra Bonjour, je vous avoue que
la logique de nos décideurs politiques est parfois difficile à suivre Avec ses
11 millions de kilomètres carrés, la France gère le deuxième plus grand espace
maritime de la planète Depuis quelques semaines, allez comprendre, Hervé
Berville, le secrétaire d'État chargé de la mer, sous-entendu de sa protection
se bat contre le plan d'action pour une pêche durable et résiliente proposée par
la Commission européenne Ce texte propose de supprimer progressivement les
chalûles, les dragues, les palangres ou les casiers dans toutes les aires
marines protégées Vous noterez au passage que toutes ces techniques de pêche
très destructrices de la biodiversité sont donc encore autorisées dans les aires
marines protégées Mais au lieu de s'appuyer sur ce texte non contraignant pour
préparer les pêcheurs français à la gestion durable de la biodiversité marine
qui est une nécessité sinon ils vont vraiment finir par pêcher des cailloux, eh
bien non! Hervé Berville a chauffé les pêcheurs qui se sont mis en grève Il est
allé à Bruxelles, clamé haut et fort que la France était contre Après une
rencontre avec le commissaire européen de l'environnement, le secrétaire d'État
a publié un communiqué de presse pour annoncer qu'il avait obtenu je cite, la
confirmation qu'une interdiction des engins mobiles de fond dans les aires
marines protégées ne serait pas imposée aux États membres Les pêcheurs ont
arrêté leur grève et Hervé Berville a bombé le torse Sauf que tout ce théâtre
n'était pas nécessaire puisque le plan d'action proposé par la commission
européenne n'était, je le répète, pas du tout contraignant L'océan qui couvre
plus de 70% de la planète est en crise Il est touché de plein fouet par le
changement climatique, l'acidification, la pollution et la surpêche Et selon
l'Organisation des Nations Unies pour l'alimentation et l'agriculture, environ
90% des stocks de poissons évalués sont pleinement exploités surexploités ou
épuisés au niveau mondial C'est donc le moment de citer Émile de Girardin
Gouverner c'est prévoir et ne rien prévoir c'est courir à sa perte Bonjour
Audrey Boély, bonjour Vous êtes journaliste scientifique, vous avez travaillé
pour plusieurs magazines Depuis 2020 vous vous consacrez à des projets
personnels mais toujours liés aux enjeux environnementaux À l'automne 2021 vous
avez débuté le projet Dernière Limite Au départ c'était un podcast désormais
disponible sur toutes les plateformes Et puis aujourd'hui c'est un livre,
Dernière Limite, apprendre à vivre dans un monde fini C'est publié aux éditions
rue de l'Échiquier Alors tout d'abord, vous êtes une réaction à cette agitation
politique Personnellement j'ai du mal à comprendre et vous? Oui politiquement
c'est complètement incompréhensible Alors qu'aujourd'hui on sait déjà que ces
aires protégées sont des mascarades En France c'est seulement 2% de ces aires
protégées qui sont vraiment en protection forte C'est-à-dire que dans toutes les
autres on a le droit de pêcher avec la pêche industrielle avec le chalut Ce qui
détruit les fonds marins, de faire de l'extraction minière etc Donc au contraire
il faut vraiment accélérer, donc là c'est absolument incompréhensible Voilà
encore un signe que notre gouvernement n'a pas compris que nous étions justement
dans un monde fini Alors ce projet Dernière Limite est né d'une prise de
conscience personnelle J'ai lu qu'un jour vous êtes sorti du bureau, vous avez
pris votre vélo, vous avez eu un coup de chaud et là vous êtes dit c'est plus
possible Alors c'est sûr qu'il y a de l'expérience personnelle comme tout un
chacun C'était à l'été 2018 quand il a fait presque 45 degrés à Paris Mais il y
a aussi la lecture d'un livre qui pour moi a été vraiment un électrochoc Qui est
le rapport MEDOS, alors je ne sais pas si vos auditeurs connaissent ce rapport
qui a été publié il y a 50 ans, 1972 Bah non comme plein de gens nous allons
d'ailleurs expliquer pourquoi mais non je pense que beaucoup ne le connaissent
pas En tout cas moi je ne le connaissais pas et c'est vrai que sa lecture a
vraiment m'a fait réaliser qu'en fait Parce qu'à l'époque j'étais très
préoccupée par le climat mais qu'il y avait bien d'autres choses, bien d'autres
problèmes écologiques Et qu'en fait on était en train de dépasser l'ensemble des
limites planétaires dont fait partie le climat mais que c'est un problème
écologique global Vous avez donc réalisé 13 entretiens d'experts autour des
limites planétaires et de leurs enjeux Alors ce concept de limites planétaires
en quelques mots vous pouvez nous le réexpliquer Alors c'est un concept
scientifique qui date de 2009 qui a été développé par une équipe du trentaine de
chercheurs dirigés par Johann Rockström Et qui en fait essaye de définir les
équilibres naturels dont on dépend en fait, dont nos sociétés humaines dépendent
Ce sont des équilibres naturels, ils en ont déterminé 9, qui datent du début de
l'holocène Donc c'est une ère géologique qui a débuté il y a plus de 10 000 ans
et qui a vu l'essor des sociétés humaines C'est vraiment une période très stable
en terme de conditions physiques Et donc après avoir déterminé quelles étaient
ces 9 limites planétaires, l'idée c'est de se dire ok, on a du coup une pression
importante des activités humaines Jusqu'où cette pression peut aller avant qu'on
passe des points de bascule en fait et qu'on renverse ces équilibres naturels
Voilà et qu'on remette en cause l'habitabilité de la planète pour l'ensemble du
vivant mais aussi pour notre espèce Alors revenons à ce point de départ, ce
rapport Mendoz, quels étaient les objectifs à l'origine de ce rapport? C'est la
question que vous avez posé à l'un de ses auteurs, donc l'un des plus connus,
Denis Mendoz Le projet a commencé en 1968 quand Aurelio Peccei a écrit un livre
intitulé Le gouffre avance C'est de là qu'est né le club de Rome et son intérêt
pour les enjeux mondiaux C'est un professeur du MIT, Jay Forrester, qui a
compris que ces problèmes étaient en fait les symptômes d'une planète surchargée
Où la population et la croissance économique étaient trop importantes C'est
comme ça qu'est né notre projet qui a été de comprendre les causes et les
conséquences de la croissance d'un point de vue physique Quelles ont été les
conclusions de votre rapport? Il était évident au moins pour nous que la
croissance physique ne pouvait pas continuer éternellement sur la planète C'est
comme dans une voiture, quand vous voyagez en voiture vous comprenez qu'elle ne
peut pas avancer éternellement Elle doit finir par s'arrêter, et il était clair
pour nous qu'il devrait y avoir un ralentissement de la croissance
Malheureusement, il y a de longs délais de réaction dans notre système, de sorte
que si vous ne commencez pas à agir avant de voir les problèmes se produire Il
est déjà trop tard, et c'était ça la principale conclusion de notre rapport Si
nous ne commençons pas immédiatement à essayer de stabiliser le système, il
allait dépasser des limites et ensuite bien sûr il allait décliner Dans votre
rapport vous expliquez que si nous poursuivons une croissance infinie Le
résultat le plus probable serait un déclin assez soudain et incontrôlable de la
population et de la capacité industrielle avant la fin du XXIe siècle Qu'est-ce
que cela signifie exactement et qu'est-ce qui pourrait déclencher une telle
situation? Nous avons étudié la croissance, pas la fin de la croissance Si vous
regardez les courbes de notre rapport, elles montrent une hausse puis une obèse
Nous comprenions d'où venait la hausse, mais nous ne pouvions pas prévoir
précisément ce qu'elle est spécifique ensuite On ne peut pas prédire l'avenir
avec précision, mais on peut dire que certaines choses n'arriveront pas C'est ce
que nous avons fait. Dans notre scénario standard, la croissance s'arrête parce
que la capacité de production alimentaire atteint ses limites Et cela retire des
ressources aux autres secteurs Notre projet en 1972 prévoyait dans un autre
scénario que la croissance se poursuivrait probablement pendant encore 50 ans Ce
qui s'est avéré vrai, mais que vers 2020 la croissance s'arrêterait À ce stade,
les forces de la physique deviennent moins importantes Ce sont les facteurs
sociaux, politiques et économiques qui entrent en jeu Ces facteurs n'ont pas été
étudiés dans notre livre, mais nous les observons actuellement Quels sont
d'après vous les principaux obstacles qui nous empêchent d'agir? Il y a de
nombreux obstacles, j'en ai mentionné deux. La première est celle de notre
espèce Les êtres humains, au beau sapiens, ont évolué génétiquement au cours des
dernières centaines de milliers d'années Pour se concentrer sur les problèmes à
court terme, pas sur les problèmes à long terme Si vous avez deux hommes des
cavernes et qu'un tigre arrive et qu'un homme des cavernes dit courons Et le
deuxième homme des cavernes dit, réfléchissons à la philosophie et à la culture
de notre société Le premier survit pour avoir des enfants et le second, non Avec
le temps, l'intérêt pour les questions à long terme disparaît Ça a l'air d'une
blague, mais en fait c'est une contrainte très sérieuse pour notre espèce Une
autre sur laquelle nous pouvons faire plus est le pouvoir des intérêts
particuliers Vous et moi regardons la situation actuelle et pensons à tous les
problèmes qu'elle cause Mais de nombreuses personnes, de nombreuses
organisations, l'industrie de la défense par exemple l'industrie pharmaceutique,
regardent la situation actuelle et pensent à l'argent qu'ils gagnent ou au
pouvoir politique que ça leur donne Ces intérêts particuliers bloquent
systématiquement les efforts de changement parce qu'ils se satisfont de la
situation telle qu'elle est actuellement Je me souviens qu'il y a eu récemment
une conférence sur le climat Quand j'ai appris que le plus grand lobby qui
participait représentait l'industrie des combustibles fossiles j'ai alors su que
rien de constructif ne s'y produirait Qu'on agirait pas parce que les nations
exportatrices de pétrole et les sociétés productrices de pétrole ont tout un
intérêt lucratif très fort dans la situation actuelle Et quand vous avez réalisé
cette interview Audrey Boelly pour votre podcast « Dernière limite » Denis
Meadows ne savait pas encore que la prochaine conférence des Nations Unies sur
le climat la 28ème qui va avoir lieu en novembre prochain elle est déroulée aux
Emirats Arabes Unies à Dubaï et qu'elle serait donc présidée par Ahmed Al-Jaber
qui est le ministre de l'industrie des Emirats Arabes Unies mais qui est aussi
le PDG du géant pétrolier national Oui effectivement, déjà Dubaï c'est vraiment
le symbole du dépassement des limites planétaires et là de mettre un pétrolier à
la tête de la COP c'est vraiment ce que dit Denis Meadows finalement ces
entreprises elles ont tout intérêt à maintenir le statu quo déjà qu'on peut se
questionner sur l'intérêt des COP, moi je pense qu'elles sont quand même le
mérite d'exister on a besoin de ces instances internationales mais malgré tout
ça jette un doute immense et ça va dans le sens de maintenir le statu quo Alors
nous reviendrons sur les enjeux des énergies fossiles dont la combustion est
massivement responsable du changement climatique mais restons sur le rapport
Meadows, c'est donc un rapport fondateur qui vous a inspiré mais qui nous a
alerté dès les années 70 finalement sur le fait qu'il y avait une croissance
infinie qui était en train de progresser sur une planète aux ressources limitées
que c'était une folie mais ce rapport est passé quasiment inaperçu, pourquoi?
mais vous avez posé justement la question à l'économiste Gaël Giraud C'est
absolument orthogonal à tout ce que font, disons, l'écrasante majorité des
économistes mainstream qui envisagent un monde complètement à l'équilibre dans
lequel vous n'avez pas de ressources naturelles ou quand vous en avez elles ne
jouent qu'un rôle complètement marginal et où tout se termine toujours bien quoi
qu'il arrive Pour vous donner un exemple, j'ai des collègues qui ont testé le
fameux modèle de Bill Nordhaus William Nordhaus qui a eu le prix Nobel il y a
quelques années qui est un modèle disons qui est le modèle standard utilisé par
les économistes mainstream pour traiter la question du climat et alors ils ont
forcé un peu le modèle pour essayer de voir comment il réagit lorsqu'on tient
compte du fait que le réchauffement climatique peut avoir des conséquences
vraiment catastrophiques et donc ils arrivent à simuler des trajectoires grâce à
ce modèle où la planète perd 90% de son PIB mondial donc vous vous dites 90% ça
y est c'est terminé, la messe est dite, on s'en remet pas mais dans ce modèle on
s'en remet Pourquoi? Parce que ces modèles néoclassiques sont construits de
telle sorte que quoi qu'il arrive vous revenez toujours à une situation
d'équilibre satisfaisante Alors évidemment à la fois la méthode et la conclusion
de l'équipe Meadows en 72 heurtent de plein fouet la manière de penser des
économistes mainstream Norda ce que vous évoquez était d'ailleurs un des
principaux contradicteurs par rapport au rapport Meadows Oui tout à fait, il a
très vite pris la plume pour expliquer que son rapport était très mauvais qu'il
avait négligé l'essentiel c'est à dire le rôle des prix avec l'idée que s'il y
avait des ressources qui devaient se rarifier et bien on le verrait, le marché
nous le dirait parce que le prix de ces ressources exposerait et donnerait les
bonnes incitations soit pour trouver des substitutions à ces ressources soit
pour trouver davantage de ces ressources mais si vous voulez ça c'est des
explications d'économistes qui sont complètement hors sol Les économistes
néoclassiques vous disent ah mais si la forêt amazonienne disparaît c'est pas
grave on va trouver des substituts en terme de dollars ou d'euros ou de livres
au sterling pour remplacer les services écologiques qui nous sont donnés par la
forêt amazonienne Ce qu'ils voient pas c'est qu'on ne se nourrit pas de dollars
ou d'euros et on ne boit pas les dollars et les euros Donc évidemment les
économistes de l'époque dans les années 70 pour une bonne partie d'entre eux ont
eu énormément de mal à intégrer les résultats du rapport Meadows En fait il est
tellement incompatible avec tout ce que professe les économistes néoclassiques
que même la confrontation est impossible Donc à partir de ce moment là la
communauté des économistes néoclassiques va réagir essentiellement par
l'ignorance, l'ignorance et l'indifférence Donc elle va complètement ignorer le
rapport, ne plus en parler Et c'est pour ça que notre génération a grandi sans
entendre parler du rapport Meadows ni des questions qui l'a soulevé Jusqu'à une
date récente les étudiants de l'économie n'entendaient jamais parler du rapport
Meadows Absolument jamais J'ai testé moi avec tous les étudiants que j'ai eu
depuis des générations et ils n'avaient jamais entendu parler du rapport Meadows
Jamais jamais Il fonctionne comme une espèce de mauvaise religion païenne qui
refuse la confrontation avec le monde réel Et ce que faisait le rapport Meadows
c'est juste rappeler que le monde réel ça existe et que si on s'en occupe pas on
a un retour de bâtons extrêmement sévère C'est catastrophique finalement André
Boélic Ce rapport n'est pas été présenté aux étudiants pendant des décennies
Oui, ni dans les études économiques ni dans les études scientifiques Finalement
moi j'ai fait des études scientifiques A aucun moment je n'ai entendu parler de
ce rapport Donc oui effectivement c'est très très problématique Au moment du
lancement de ce rapport ça a été un best seller ça a vraiment créé une onde de
choc Ça a contribué à lancer tout le processus de négociation internationale
qu'on connaît les sommets de la terre, les COP etc Donc c'est vraiment un des
catalyseurs de tout ce processus là Mais au final non seulement on a effacé les
conclusions du rapport on a inventé les jolis concepts de développement durable
et de croissance verte mais en parallèle en fait on a continué et on a même
accéléré la croissance Donc on a fait exactement l'inverse de ce qui était
préco-disé dans le rapport Et c'est l'économiste libéral William Nordos qui a
reçu le prix Nobel en 2018 et pas Denis Médos et sa femme Alors résultat notre
planète devient de plus en plus inhabitable notamment à cause d'une limite
planétaire qui a déjà été franchie c'est la quantité de gaz à effet de serre que
nous avons émis avec nos activités dans l'atmosphère c'est ce qui crée le
changement climatique et le réchauffement des températures que nous ressentons
déjà Comme vous l'a expliqué la climatologue Valérie Masson-Delmotte Oui, on bat
de plus en plus souvent des records de chaleur En plus de l'évolution de la
température moyenne c'est une augmentation de la fréquence et de l'intensité des
extrêmes chauds au-dessus des continents comme vous l'avez souligné mais aussi
au-dessus de l'océan avec également des vagues de chaleur marine plus fréquentes
et plus intenses Et donc plus le niveau de réchauffement va monter à la surface
de la Terre plus cela va se produire plus le climat accumule de la chaleur plus
on va avoir une intensification et une augmentation de la fréquence des épisodes
de pluie extrêmes On l'observe déjà par exemple du côté des épisodes Cévenol les
records de pluie dans le sud de la France en automne ont augmenté au cours des
dernières décennies c'est quelque chose qui va augmenter à mesure du niveau de
réchauffement planétaire et puis des conditions métrologiques favorables aux
incendies augmentent Un autre exemple de phénomène qui s'intensifie avec la
montée du niveau des mers on observe déjà dans de nombreuses régions une
augmentation des épisodes de submersion de marée haute et cela fait partie de
ces caractéristiques qui s'intensifient dans un climat qui se réchauffe A quel
niveau de réchauffement sommes-nous aujourd'hui par rapport à l'ère pré-
industrielle? Alors si on prend comme période de référence 1850-1900 on en est
sur la dernière décennie à 1,1°C donc en moyenne sur 2010-2019 par rapport à
1850-1900 Si on regarde d'un point de vue historique chacune des quatre
dernières décennies a été successivement plus chaude que la précédente ces
changements climatiques sont-ils d'une rapidité inédite? Alors si on prend le
réchauffement observé depuis le début du XXème siècle c'est une rupture sur plus
de 2000 ans en termes d'ampleur et en termes de vitesse Pour trouver une période
aussi chaude qu'aujourd'hui il faut remonter il y a plus de 120 000 ans en
arrière à un moment où la trajectoire de la Terre autour du soleil était un peu
différente et donc le climat un peu plus chaud environ 1,5°C plus chaud que le
climat pré-industrielle Différentes gaz à effet de serre contribuent au
réchauffement d'où viennent ces gaz? L'intégralité de ce réchauffement est la
conséquence des activités humaines avec l'effet réchauffant des gaz à effet de
serre Alors sur les rejets de dioxyde de carbone c'est principalement dû à la
combustion d'énergie fossile pétrole, charbon et gaz et puis en partie à la
déforestation quand on détruit des stockages de carbone dans la végétation et
les sols et que ça repart vers l'atmosphère Audrey Boelly, la synthèse du 6ème
rapport du GIEC le groupe intergouvernemental des experts sur l'évolution du
climat vient d'être publié et détaille justement les causes du changement
climatique, ses conséquences et bonnes nouvelles les solutions, nous allons en
parler un petit peu plus tard dans l'émission mais là on a fait quand même un
tableau un peu apocalyptique et ça ne va pas s'arrêter mais il y a des solutions
Oui tout à fait et ce qui est intéressant c'est que dans ce rapport du GIEC déjà
c'est la première fois qu'il y a vraiment un chapitre entier sur la question de
la sobriété et donc ça c'est vraiment partie des solutions qui sont des
solutions systémiques c'est à dire qu'elles vont faire du bien à plusieurs
limites planétaires au climat bien sûr mais aussi à la biodiversité en fait il
faut réduire globalement notre empreinte écologique l'empreinte carbone mais
aussi l'empreinte eau, l'empreinte matière l'empreinte forêt etc etc et donc ça
a un potentiel déjà énorme puisque on peut réduire grâce à ça de 40 à 70% notre
empreinte écologique Alors c'est quoi l'empreinte écologique? Vous avez fait une
interview là-dessous on ne peut pas toutes les diffuser mais qu'est-ce que c'est
cette notion d'empreinte écologique? Alors c'est un indice qui nous permet de
comprendre quelle est notre consommation en terme d'équivalent de terre, de
surface de la biosphère cette surface qui nous fournit de quoi consommer mais
qui absorbe aussi nos pollutions quand on parle de pollution c'est
principalement nos émissions de CO2 et donc cette surface elle ne fait
qu'augmenter à travers le temps et on la compare à ce que produit réellement la
biosphère et quand on compare les deux on voit qu'aujourd'hui on consomme
l'équivalent de deux planètes ça c'est la moyenne mondiale mais en France on est
plutôt autour de trois planètes aux Etats-Unis cinq planètes, au Qatar neuf
planètes donc en fait on est en dépassement, en très très fort dépassement on
consomme beaucoup plus que ce qu'offre aujourd'hui la biosphère Voilà et c'est
comme cela que l'on va en effet vers une situation de plus en plus compliquée et
notamment à cause de la consommation du charbon, du pétrole et du gaz les
fameuses énergies fossiles qui sont la principale cause du changement climatique
nous les avons tellement ponctionnés que là aussi nous atteignons des limites
nous en parlons juste après mais tout d'abord un morceau du nouvel album des
Turcs Altin Gun danser pas du vent sur RFI évidemment et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et