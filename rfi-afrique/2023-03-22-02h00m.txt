TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-03-22-02H00M.MP3
--------------------------------------------------------------------------------
... Soyez les bienvenus sur Radio France Internationale, nous sommes en direct
de Paris où il est 2h, 1h en temps universel pour vous présenter le journal....
Jérôme Bastion.... À la une, suite et fin de la visite d'État du président
chinois en Russie qui scelle une nouvelle ère de la relation spéciale entre les
deux pays sur fond d'initiatives de paix aux contours flous pour l'Ukraine et
surtout une union sacrée contre l'Occident. Un cargo chargé de dizaines de
milliers de blés ukrainiens a accosté mardi dans le port kényan de Mombasa pour
aider le pays à affronter la famine due à une sécheresse récurrente. C'est le
fruit du programme « Grain from Ukraine » qui vient d'être reconduit avec la
valle de Moscou. Et puis les violations des droits humains en Iran ces derniers
mois sont les plus graves que le pays ait connu au cours des quatre dernières
décennies. C'est-à-dire depuis la création de la République islamique, dénonce
un rapport de l'ONU.... Les relations Chine-Russie entrent dans une nouvelle
ère, s'est réjoui Xi Jinping à l'issue de son long tête-à-tête avec Vladimir
Poutine mardi à Moscou en clôture de sa visite officielle entamée lundi. À côté
de la signature de nombreux accords économiques, il n'y a pas eu sur l'Ukraine
d'avancées réellement concrètes et les deux dirigeants ont surtout affiché leur
unité face aux occidentaux. Juliette Gerbrand. Fort d'une relation spéciale,
selon les mots de Vladimir Poutine, Moscou et Pékin s'en sont pris aux Etats-
Unis et à l'OTAN. Ils ont accusé Washington de « saper la sécurité
internationale » dans le but de préserver son avantage militaire. Ils se sont
dit préoccupés de la présence croissante de l'organisation transatlantique en
Asie. Xi Jinping était arrivé à Moscou en médiateur, un plan de paix dans ses
bagages pour l'Ukraine, aucune avancée concrète. Mais Vladimir Poutine voit dans
l'initiative une base possible pour un règlement pacifique. Ce sont Kiev et ses
alliés qui n'y sont pas prêts, dénonce le président russe. Un président qui
s'est montré réjoui et qui veut croire que la coopération russo-chinoise a des
perspectives vraiment illimitées. Il est clair en tout cas que Xi Jinping n'a
pas été avare de son appui. « Je suis sûr que les Russes vous soutiendront dans
toutes vos bonnes entreprises », a-t-il déclaré en évoquant l'élection
présidentielle russe de 2024. Et pied de nez supplémentaire, aux Occidentaux,
quelques jours après le lancement du mandat d'arrêt de la CPI, le dirigeant
chinois a invité Vladimir Poutine à lui rendre visite à Pékin. Juliette
Gerbrand. Et parmi les accords bilatéraux annoncés, il y a ce gigantesque projet
de gazoduc Force de Sibérie 2, c'est son nom, qui, à sa mise en service,
transportera sur 2600 km entre la Sibérie et le Xinjiang, via les steppes de
Mongolie, 50 milliards de mètres cubes de gaz naturel. Et pour en revenir au
conflit en Ukraine, le président Volodymyr Zelensky a annoncé, lui aussi, avoir
invité la Chine à dialoguer et attendre une réponse à son invitation. 25 000
tonnes de blé ukrainien sont arrivées au port de Mombasa, au Kenya. Première
livraison dans ce pays grâce à l'initiative Grain from Ukraine, une opération
lancée par le président ukrainien Volodymyr Zelensky pour venir en aide aux pays
africains, en proie à une crise alimentaire majeure, exacerbée notamment par le
conflit russo-ukrainien. À Mombasa, la correspondance de Charlotte Simonard.
Cette immense paquebot aura mis plus d'un mois pour arriver ici à Mombasa,
depuis le port d'Odessa en Ukraine. Une traversée possible grâce au corridor
humanitaire en mer noire, mais ralentie par les contrôles russes, comme
l'explique le capitaine. Maintenant, quand on passe par la mer noire, on est
arrêté pour l'inspection. Ils regardent partout si on ne transporte pas des
armes ou autre chose. 25 000 tonnes de blé financées par plusieurs pays
européens à travers le programme alimentaire mondial des Nations Unies, tout un
symbole pour l'ambassadeur ukrainien au Kenya, Andriy Pravednik. Nous sommes
heureux que malgré l'invasion russe en Ukraine, nous soyons capables d'acheminer
ce blé au Kenya. Assurer la sécurité alimentaire du monde, ce n'est pas
seulement faire preuve de responsabilité, c'est aussi une arme diplomatique.
Nous montrons ainsi au Kenya et aux pays d'Afrique qui a raison et qui a tort.
Cette livraison vient à Point-Nommé, dans un pays frappé par une crise
alimentaire majeure. Le vice-président kényan applaudit. C'est très important,
car nous venons de traverser quatre saisons de plus insuffisantes. Plus de 5
millions de Kényans souffrent de la faim. Mais n'y voient pas pour autant un
rapprochement avec l'Ukraine. Nous ne nous occupons pas des problèmes
extérieurs. Quiconque veut nous aider est le bienvenu. Ce blé venu d'Ukraine
sera distribué dans le nord du Kenya. Charlotte Simonard, Nairobi Refi.
Rappelons qu'un accord prévoyant un couloir maritime sécurisé dans la mer Noire
pour le transport de ces céréales à partir de trois ports ukrainiens avait été
signé en juillet sous l'égide de l'ONU et de la Turquie. Il a permis
l'exportation de quelques 20 millions de tonnes de céréales jusqu'au début du
mois de mars. Et des crimes contre l'humanité ont-ils été commis en Iran? C'est
une possibilité soulevée par un expert des Nations Unies qui dénonce la gravité
et l'ampleur de la répression du mouvement de contestation lancé en septembre
dernier contre le régime des Mollah. Ce qui choque, c'est aussi le lourd tribut
payé notamment par les enfants, dirait Milanj. Plus de 22 000 personnes
arrêtées, soumises à la torture, à des viols, des disparitions forcées, des
protestataires gravement blessés, à dessein par les forces de sécurité. Voilà en
quelques mots à quoi s'exposent les Iraniens qui se font descendus dans la rue
après la mort de Massah Amini. Le rapporteur spécial de l'ONU sur l'Iran a
compté 527 manifestants tués directement par les forces de l'ordre, dont un
nombre considérable d'enfants, dit Javeid Rehman. Quelle loi peut justifier le
meurtre d'enfants innocents? Au moins 71 ont été tués par les forces de
sécurité. Les preuves sont là. Si c'est une pratique courante, systématique et
soutenue par l'État iranien, alors des crimes contre l'humanité et contre des
enfants ont bien été commis en Iran depuis le 16 septembre dernier. Téhéran, qui
ne reconnaît pas l'autorité du rapporteur spécial, a dénoncé des informations
mensongères. La réponse de l'intéressé. Si les autorités iraniennes estiment que
mes informations sont fausses alors qu'elles me donnent accès au pays, pourquoi
ont-elles si peur que je puisse venir en Iran? Autre motif d'inquiétude, les
attaques punitives de jeunes filles au gaz toxique. Elles pullulent dans le pays
ces dernières semaines. Là aussi, il pourrait s'agir d'un crime contre
l'humanité, a estimé Javeid Rehman. Mais encore faut-il prouver que l'État
iranien avait commandité ou couvert ces exactions. Jérémy Lanche, Genève, RFI.
L'Union européenne a justement sanctionné ce lundi le Conseil suprême de la
Révolution culturelle et huit responsables iraniens, dont l'imam de la prière du
vendredi de la ville de Mashhad, Ahmad al-Malhoda, jugé responsable de graves
violations des droits humains en Iran, lui a exhorté les autorités iraniennes à
mettre fin à la violente répression des manifestations pacifiques, à cesser de
recourir à des détentions arbitraires pour faire taire les voix critiques et à
libérer toutes les personnes détenues injustement, selon un communiqué publié à
l'issue d'une réunion des ministres des Affaires étrangères des 27 à Bruxelles.
En réponse, le guide suprême iranien, l'ayatollah Ali Khamenei, a déclaré mardi
que l'Iran n'était pas en colère contre les Européens, mais était prêt à
travailler avec les pays qui ne suivaient pas aveuglément les Etats-Unis. En
France, après avoir laissé sa première ministre Elisabeth Borne en première
ligne sur la très controversée réforme des retraites, le chef de l'État Emmanuel
Macron doit prendre la parole ce mercredi à l'occasion d'une interview
télévisée. Il y évoquera certainement son choix de faire adopter le texte sans
le vote des députés, en utilisant l'article 49.3 de la Constitution, très décrié
par ses opposants. Et alors même que la mobilisation, les mouvements de grève et
d'occupation de locaux et les manifestations se poursuivent, il devrait
également s'adresser à la rue. Qu'est-ce que les députés, eux, attendent de
cette prise de parole? Écoutez la réponse de deux parlementaires. Voici tout
d'abord Fabien Roussel, député communiste. J'attends à ce qu'il ouvre tout de
suite les voies d'un apaisement, à ce qu'il suspende sa réforme, à ce qu'il ne
la mette pas dans l'implication, parce que les Français restent mécontents,
parce que les organisations syndicales font encore front commun, parce que les
parlementaires ont déposé un recours au Conseil constitutionnel et ont déposé un
référendum d'initiative partagée. Pour toutes ces raisons, retrouver les voies
de l'apaisement est la seule issue pacifique. L'objectif, ce n'est pas qu'il y
ait un vainqueur et un vaincu, l'objectif c'est qu'on retrouve les voies du
dialogue et le respect de la démocratie sociale dans notre pays. Et maintenant
Éric Woerth, député Renaissance. Ils reviennent évidemment sur la réforme des
retraites, sur la façon dont ça s'est passé, sur le contenu aussi du texte.
Qu'il explique aux Français un peu plus en quoi cette réforme était absolument
nécessaire et que c'est plutôt une bonne nouvelle pour la France, même si les
Français aujourd'hui pensent le contraire. C'est même vraiment une bonne
nouvelle pour notre modèle de protection sociale. C'était un devoir de faire ça.
Le deuxième point, c'est qu'il éclaire un peu les mois qui viennent, car il faut
apaiser, mais il faut surtout avancer. Il y a beaucoup d'autres sujets sur
lesquels la France doit avancer, comme notre pays, entre nous. Propos recueillis
par Charlotte Urien-Tomaka. Tous les jours, Accent d'Europe vous propose de
partager la vie des Européens. Juliette Rangeval. Ce mercredi, à l'occasion de
la visite en Lituanie du patriarche de Constantinople, retour sur les
déchirements de l'église orthodoxe dans les Pays Baltes. Depuis le début de la
guerre en Ukraine, le rapport de l'église à la Russie est remis en question.
Accent d'Europe, du lundi au vendredi, à partir de 19h10 T.U. sur RFI. Vous
suivez Priorité Santé avec le groupe Sounou, présent dans 17 pays qui, depuis 25
ans, met le client au centre de tout. Priorité Santé. Caroline Paré. Bonjour à
toutes et à tous. De nombreuses maladies évoluent de manière silencieuse avant
que des symptômes surviennent, parfois à un stade avancé de la maladie. Le
diagnostic tardif est souvent un facteur de perte de chance pour les patients et
c'est encore trop souvent le cas pour l'affection chronique qui nous intéresse
aujourd'hui dans Priorité Santé, le diabète. Alors que le nombre d'adultes
vivant avec le diabète devrait plus que doubler dans les 20 prochaines années en
Afrique, d'après l'OMS, eh bien aujourd'hui, moins de la moitié des personnes
atteintes connaissent leur état sur le continent, ce qui augmente les risques de
complications, voire de décès en cas de crise. Nous allons donc parler du
dépistage, comme des différentes prises en charge de l'excès de sucre dans le
sang. En distinguant les deux types de diabète, bien comprendre l'importance sur
le long terme d'un diabète équilibré, contrôlé, avec les messages essentiels de
prévention pour lutter contre les facteurs de risque évitables, comme la
sédentarité et l'alimentation trop grasses ou trop sucrées, sensibiliser les
populations pour éviter le diabète et quand le diagnostic est posé, poursuivre
cet effort de communication pour comprendre sa maladie, apprendre à vivre avec
et éviter d'en supporter les séquelles. Bien sûr, on va répondre à toutes vos
questions sur la page Facebook de l'émission et à notre numéro au 33 84 22 75
75. Diabète de type 1 et 2, qu'elles avançaient dans la prise en charge?
Pourquoi l'activité physique est-elle l'un des piliers de la lutte contre le
diabète? Avec nous en studio, Dr Marine Albron, bonjour. Bonjour. Vous êtes
diabétologue à l'hôpital de la Pitié-Salpêtrière à Paris et on retrouvera en
deuxième partie d'émission le Dr Kossi David Kodjo, endocrinologue,
diabétologue, nutritionniste au CHU Sylvanus Olympio de l'OME au Togo, également
directeur médical du Centre associatif d'éducation thérapeutique du diabète et
de l'obésité, Diab-OB, président du Centre Giza Togo. On parlera aussi en fin
d'émission de la publication d'une étude sur les risques sanitaires de
l'exposition Chlordécone, ce pesticide utilisé jusqu'au début des années 90 dans
les bananeraies des Antilles françaises. On donnera la parole à Luc Multigné,
chercheur à l'Inserm, qui a travaillé en particulier sur la santé des enfants
exposés à ce produit neurotoxique. Dr Marine Albron, un objectif de tous les
médecins dans la prise en charge de tous les diabètes, également c'est la même
chose bien sûr pour leurs patients, c'est l'équilibre du taux de glycémie. Un
diabète contrôlé, c'est quoi exactement? Un diabète contrôlé, c'est un diabète
qui va vous mettre à l'abri des complications du diabète. C'est-à-dire, c'est un
diabète qui va contrôler, c'est un diabète qui va vous permettre de ne pas
devenir malade. Quand je dis malade, ça veut dire pas de handicap, pas de
douleur importante, évidemment pas de passage en dialyse ou de perte de la vue.
Et c'est ça, c'est cet équilibre du diabète qui va vous permettre finalement de
rester entre guillemets en bonne santé. Grâce à des traitements, grâce à une
surveillance, mais sans complications, c'est-à-dire sans retentissement dans
votre vie quotidienne. Évidemment, quand on apprend qu'on a un diabète, ce n'est
pas anodin. Néanmoins, quand on a cette connaissance sur le diabète contrôlé,
équilibré, ça permet non seulement de mieux l'accepter, mais de se projeter dans
l'avenir, on va vivre avec. Absolument. L'annonce d'un diabète, que ce soit un
diabète de type 1 ou de type 2, c'est un traumatisme parce que le diabète est
une maladie chronique, c'est-à-dire que ce n'est pas une maladie qu'on peut
guérir, dans le sens plus jamais de traitement et on revient à la vie comme
avant. Ça, ce n'est pas possible. Par contre, ce qui est possible, c'est de se
dire que grâce à une surveillance, grâce à une prise en charge, on va continuer
une vie avec une espérance de vie, des projets de vie qui sont ceux qu'on aurait
eus si on n'avait pas été diabétique. Alors, c'est vrai qu'au cours de cette
émission, on va surtout aborder ce qui concerne le diabète de type 2. La grande
majorité des personnes touchées par la maladie chronique sont dans ce cas-là.
Est-ce que vous pouvez nous indiquer la proportion, Dr. Albron? La proportion,
c'est environ 90% de diabétiques de type 2 contre moins de 10% de diabètes de
type 1. Alors, on dit souvent insulino-dépendants et pourtant, il y a certains
diabétiques de type 2 qui sont traités avec des injections d'insuline. Pourquoi
les médicaments pour eux ne suffisent pas? En fait, le diabète de type 2 est une
maladie qui est évolutive. Ça veut dire quoi? Ça veut dire que justement, pour
bien équilibrer le diabète, donc avoir des glycémies dans un objectif qui peut
être très difficile.