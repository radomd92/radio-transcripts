TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-06-13H00M.MP3
--------------------------------------------------------------------------------
......... We had to struggle through the years, yo Through all the blood that's
flowing, tears, yo Through all the bruises and all the tears, yo It took a
minute to get here, yeah, you alright, yo Ayo I'm popping no matter where
everybody wanted to rap Dropping the balls to take what's happening in the trap
Get killed in the back to back, spend a minute on the track Every time I did it,
learning in the pyre, radio flats Big teams standing on the mic back thing You
had to bring it with a 10 out of 10 The fountain pen, the A4 trend But now we're
riding on a foam, all we eat, no sand All your sound cloud rap starts sucking up
Ripping the mic for 20 years, I'ma bump that shit I love this shit, I'm kind of
a misfit But living off the music and giving it all a chip Playin' by the wall,
all you mambas are all in diapers Watched out for the snakes, they were built by
vipers Murphest stages, flashing liars, I'm a don that I know Ghost riders, yo
Ghost riders, yo I'm a reddit don, don, don Everywhere we go All we see is
drama, yeah But it's like, hey, hey, thank you Design the world like Michel
Abloh Toremi, Fasci, Silado, Dépussy, Jean-Mémy, Milado Ho ho ho, in front of me
there's Vimilado Willy Dom, Dada, Sochon, Milly Mon, Icon, Pity, Pablo You're in
the air, head, Mitty, up there, live I'm playing CVD on CD2, city watch show
You're at least a vinyl-acro My flow is the hero, you end up aggro This is Fafo,
hey hey hey And without Atty, Libabo Big over one on the little cakes I'm
lighting up the fire in your city, let's go to City H.O. It's me, the artist,
it's Kipi, Kassou, yo I'm a reddit don, don, don Everywhere we go All we see is
drama, yeah We had to struggle through the years, yo Through all the blood, the
sweat and tears, yo Through all the bruises and all the tears, yo Took a minute
to get here, yeah, you already know I've been a lot of places, seen a lot of
faces So many mics, I've been ripping on these stages From stadiums to periodic
grave shifts What's your city or your state, if I met a lot of Fairs who listen
on a basis Seen a lot of likes, I seen a lot of haters I read a lot of idiotic
statements, but only one I came face to face with, didn't want to say shit Daily
gon' flow, I've been living on a spaceship These bars are sharp, I got the
rhythm on my blades If we living large, with large figures on the pacers Sitting
on a yacht with so many indicators Skinny-dipping with the women on vacation We
never did anything in moderation We did on dada, everyone agree We know the
affection, I'm afraid this is what Yo I really done dada We did on dada
Everywhere we go Everywhere we go All we see is drama Everywhere we go
Everywhere we go I really done dada We did on dada Everywhere we go Everywhere
we go All we see is drama All we see is drama We did on dada Dada-dada Dada-dada
On the dada Dada-dada Dada-dada Dada-dada Dada-dada We did on dada On dada Dada-
dada Everywhere we go T'as vu? T'as vu? T'as déjà vu? T'as déjà vu? Vous, vous,
vous prenez de tout Vous débattez des questions de boulot Vous inventez des
histoires de fou Mais de vous à moi, je vous avoue Je suis fier de faire partie
d'un beau Vous, vous, vous prenez le chou Vous vous mettez pour des histoires de
sou Avec vous, vous ne pouvez pas parler de tout Mais de vous à moi, je vous
avoue Je suis content de pouvoir entreteindre Mais c'est toi sans, mais c'est
toi sans Mais tu me coupes là! Et tu sentis ma échao, goro bae, n'égue t'es
mougoshila Et tu sentis ma échao, goro bae, n'égue t'es mougoshila Safouru kidou
Et tu sentis ma échao, goro bae, n'égue t'es mougoshila Et tu sentis ma échao,
goro bae, n'égue t'es mougoshila Et tu sentis ma échao, goro bae, n'égue t'es
mougoshila Et tu sentis ma échao, goro bae, n'égue t'es mougoshila Et tu sentis
ma échao, goro bae, n'égue t'es mougoshila Et tu sentis ma échao, goro bae,
n'égue t'es mougoshila Pour vous inscrire, www.forum-afrique-cian.fr En
partenariat avec RFI Quand les candidats font campagne, ils nous disent qu'ils
vont faire des choses pour nous et quand ils arrivent au pouvoir, ils nous
oublient RFI, grand reportage C'est une protestation qui s'inscrit dans la durée
depuis le mois de décembre au Pérou et la destitution du président Pedro
Castillo La foule rejette la succession par d'inaboloirter La crise politique a
réveillé des problèmes de fonds dans la société péruvienne Inégalités, pauvreté,
absence de l'Etat, racisme Dans le sud du pays, dans la région de Puno, les
paysans se mobilisent depuis quatre mois et ne comptent pas s'arrêter là Dans le
sud du Pérou, la région du lac Titicaca en lutte, c'est un grand reportage de
Juliette Chénian Cette chanson, la démocratie n'est plus une démocratie, les
paysans du village de Chinchira la chantent tous les jours Leur village, au bord
du lac Titicaca, est perché à 3800 mètres d'altitude dans le sud-est du Pérou,
près de la frontière bolivienne Autour de Chinchira, les massives montagnes en
dînes et à l'entrée du village, depuis plusieurs mois, des dizaines de
manifestants qui bloquent le passage à l'aide de grosses pierres empilées Les
véhicules ne passent qu'à certaines heures, certains jours Davis a donc pris
l'habitude de finir son trajet à pied, le travailleur revient de Puno, la ville
située à 20 km de là, il remonte une file de voiture arrêtée Ils vont attendre
encore une demi-heure et puis ils pourront passer, ils luttent aussi mais comme
ils travaillent, ils ont besoin de circuler Moi aussi je suis l'un de ces
manifestants, ce que le peuple réclame c'est la démission de Dinaboluarte, la
dissolution du congrès et de nouvelles élections A l'entrée du village, une
dizaine de femmes assises sur un banc, elles portent un chapeau, de longues
tresses noires et la poyera et une jupe colorées, elles aussi manifestent sous
l'œil du porte-parole du village Bonsoir, pour des raisons de sécurité je vais
simplement dire que je suis l'un des manifestants de cette lutte nationale
contre la dictature Ce qu'on fait ici c'est bloquer la route et ainsi nous faire
entendre, c'est le seul moyen de nous faire entendre, il y a plein de points de
blocage comme ça sur la route jusqu'à Desaguadero, à la frontière bolivienne Que
vivent les aimaras? Que vivent les quechua? Que vivent les quechua? De repente
il y a de grandes entreprises qui transportent des marchandises, qui cessent
leur activité, en partie parce qu'elles ont peur des barrages, alors qu'ici nous
ne sommes pas des terroristes ou des vandales comme beaucoup le disent, on
laisse passer des particuliers La plupart nous applaudissent, même si certains,
y compris entre frères, nous critiquent parce que c'est leur porte préjudice Ce
jour là le barrage ouvre en fin d'après midi vers 17h, avant de remonter dans
leur voiture, les conducteurs doivent obtenir leur laisser passer Et bien allez,
on va chanter comme on l'a fait sur tout le chemin La vieille n'en renonce pas,
la vieille n'en renonce pas Certains disent que nous avons déjà perdu contre
cette dictature, mais on reste ferme dans cette lutte, si ça ne marche pas on
emploiera d'autres stratégies, peut-être prendre des mines ou un barrage
hydroélectrique, on va continuer à résister car nous sommes de sang aimara et
nous, le peuple aimara, nous avons toujours été rebelles Jusqu'où allez-vous? Je
vais jusqu'à Acora, j'ai de la place, oui jusqu'à Acora Delia profite de la
réouverture du barrage pour poursuivre son chemin, direction le village d'Acora,
à une dizaine de kilomètres de là On va voir ma soeur pour lui apporter des
provisions, avec la grève il n'y avait pas de passage la semaine dernière
Comment ces blocages vous les vivaient? En vérité cela nous affecte beaucoup,
tous ceux qui vivent dans le sud du pays, nous ce que nous réclamons c'est
surtout la constitution, une assemblée constituante qui soit en faveur du
peuple, pas comme celle du président Fujimori établie en 1993 qui est favorable
à ceux qui ont le pouvoir, pas au peuple Changer de la constitution une promesse
de l'ancien président Pedro Castillo aujourd'hui en prison, il est accusé de
corruption et d'avoir tenté un coup d'état le 7 décembre Ici 90% des habitants
avaient voté pour lui, un président d'origine andine, Péisan, trahi d'après les
manifestants par son ex vice-présidente des armées chef de l'état, Dinah
Boluardé Je suis d'origine indigène du côté de ma mère, ma mère ne savait pas
lire ni écrire, aujourd'hui la grande majorité nous ne sommes plus inalfabètes,
on sait lire et écrire, souvent le gouvernement dit que nous sommes inalfabètes
mais c'est faux, la constitution on l'a lu à l'endroit et à l'envers et on sait
qu'elle ne nous est pas favorable La conversation s'écourte, un nouveau barrage
à l'entrée d'Acorá, un tas de pierres surmontés d'un drapeau du Pérou, sur un
grand panneau les revendications politiques peintes en lettres rouges
accueillent les visiteurs, les manifestants les répètent en boucle depuis 4 mois
Je m'appelle Antonio, j'ai 56 ans et je suis d'ici, d'Acorá, dans les hauteurs,
et bon nous sommes ici pour demander la démission de Dinah Boluardé parce
qu'elle ne nous représente pas Mais derrière la crise politique de nombreuses
revendications sociales, dans la région de Puno, l'une des plus défavorisées du
pays, plus de 40% des habitants vivent sous le seuil de pauvreté, accès à l'eau
courante et à l'électricité limitée, santé et éducation précaires, Antonio est
éleveur à Acorá Les entreprises minières, elles, continuent de travailler
tranquillement. Combien elles rapportent au Pérou? Une partie des concessions
minières sont en train de prendre fin. Nous ne voulons pas que ces contrats avec
l'Etat soient renouvelés. Ce que nous voulons, c'est que les entreprises
internationales reversent 80% des bénéfices au pays. Tout irait mieux. Il y a
beaucoup de villages ici qui n'ont pas d'eau. Donc une fois que les entreprises
minières reverseront leurs bénéfices, l'Etat pourra sauver les gens. Les
entreprises minières continuent de travailler avec l'Etat pour soutenir le
secteur agraire, ce qu'ils n'ont jamais fait ici. Nous sommes à 1300 km de la
capitale Lima, ici au bord du lac Titicaca. La majorité des habitants vivent de
l'agriculture, de la pêche, cela se devine dans le paysage, de vastes pâturages
ou brutes vaches, brebis, alpagas. On y voit aussi des paysans courber le dos
comme Clara. On sait ce que c'est de travailler, pas comme les habitants de Lima
qui ne savent même pas planter une patate. Grâce à nous, ils mangent le quinoa,
le blé, la caniwa. À côté d'elle, Emilio Elver vient d'arriver sur le barrage,
lui aussi soutient la mobilisation. Cette année, la pluie ne tombe pas assez
dans la région, une raison de plus d'être en colère. Cette année, on va perdre
la récolte. Les animaux risquent de mourir ou d'être vendus. S'il n'y a pas de
pluie, il n'y a pas de vie. Nous avons le Titicaca, mais il nous faudrait des
outils de haute technologie pour en utiliser l'eau. On ne peut toujours oublier,
parce qu'on sait que Puno, la zone de l'Altiplano, ce n'est pas très productif
en pommes de terre. Les autres régions produisent plus, donc l'Etat serait
perdant en nous aidant ici. Plusieurs amis d'Emilio sont partis d'Acora pour
manifester à Lima en janvier puis en février. Sans résultat, les communautés à
Imara défilent aussi toutes les semaines dans la ville de Puno, capitale de la
région. En ville, les blocages routiers compliquent l'approvisionnement des
commerces, mais sur beaucoup de devantures, les commerçants accrochent des
banderoles. Viva El Paro, vive la grève. Les mêmes pancartes se retrouvent sur
le très touristique port de Puno ces dernières semaines. Aucun touriste pour se
prendre en photo devant le lac Titicaca. Les guides perdent espoir. Comment
puis-je vous aider? Bienvenue. Je suis Elias Kispe, de l'île de Taquileje, ville
à bar. Ici à Puno, je travaille dans le tourisme et cette grève nous a déjà
beaucoup affecté. En tant qu'habitant de Taquileje, j'aimerais que tout revienne
à la normale pour que nos amis étrangers de France, d'Espagne, d'Europe puissent
revenir nous voir, aussi les touristes nationaux. Avant, un jour normal, c'était
700 à 800 touristes sur ce port, donc ça fait une grande différence. Et des
pertes économiques importantes. Je dirais 99%, il n'y a aucun touriste depuis
que le président a été destitué. Mais en soi, d'un point de vue politique, ce
gouvernement ne nous représente pas. La grève est juste, surtout pour les
communautés. En tant que citoyen, je les soutiens. Car derrière, la carte
postale présentait aux touristes la vie sur les îles du lac Titicaca, est elle
aussi rude. Quand les candidats font campagne, ils nous disent qu'ils vont faire
des choses pour nous et quand ils arrivent au pouvoir, ils nous oublient. Mais
Pedro Castillo n'avait pas fait grand-chose non plus. Pour Castillo, je pense
qu'il avait de bonnes idées. Mais dès son premier jour, on ne l'a pas laissé
faire. Avant, dans les communautés, on ne s'intéressait pas à ce que faisait le
gouvernement, si c'était bien ou mal. Aujourd'hui, nous avons tous réfléchi et
comme on dit, le peuple qui dormait s'est réveillé. Le plan con punchepono
annoncé par Dinaboluarte début mars ne convainc pas dans la région. Les
subventions promises et l'envoi de matériel d'eau sont inutiles d'après Elias.
Ce n'est pas la bonne solution. Ce dont on a besoin, c'est qu'elle renonce et
qu'un nouveau président soit élu, avec des propositions pour nous aider. Cette
défiance est aussi à cause des petites phrases du gouvernement et de
Dinaboluarte et des commentaires qui ont marqué les peuples eymara et quechua
déjà très stigmatisés dans la société péruvienne. Il n'y a plus de tout
touriste. C'est à cause du gouvernement. C'est la faute du gouvernement, cette
peine de touristes. Attablé dans un restaurant vide du port de Puno, Juan Goyla
n'attend plus rien de la présidente Dinaboluarte. Trois quarts des péruviens
rejettent sa politique. Beaucoup de racisme. Le pouvoir nous traite très mal.
Ils ont dit qu'on mangerait de la luzerne. Ils ont dit que Puno ce n'était pas
le Pérou. Toutes ces choses. La présidente a dit qu'on utilisait des armes
mortelles, des balles dumdum. Beaucoup de mensonges. Maintenant les gens ici
sont dégoûtés. On pourrait pardonner au gouvernement s'il n'y avait pas eu
autant de morts et ça nous fait très mal. Les morts de la répression, tout le
monde en parle ici. Une cinquantaine de civils tués depuis le mois de décembre
et un choc le 9 janvier dans la ville de Ruliaca à une heure de Puno. Ce jour-là
des manifestations près de l'aéroport bilan un policier mort, 19 civils tués en
une après-midi. Donc on commence? Bonsoir, mon nom est Demetrio Aroquipa Mamani.
Je suis le père de Yamileth Natalia Aroquipa Anco. Ma fille est née en 2005.
Elle aura toujours 17 ans. Demetrio, ouvrier dans une mine, reçoit au premier
étage de sa maison en périphérie de Ruliaca. Il y vit avec sa femme Dominga et
leurs deux enfants. Le 9 janvier, la famille est sortie faire des courses en
ville. L'itinéraire habituel passe près de l'aéroport. Mais nous on ne voulait
pas passer par là-bas. On voulait contourner, ne pas aller dans les
manifestations. Et c'est là qu'elle a reçu une balle. Sa soeur, la cadette, qui
m'a dit, papa, papa, qu'est-ce qui se passe? Ta fille, ma soeur. Quand je me
suis retourné, ma fille était au sol. Elle avait 17 ans. On ne manifestait pas,
mais ils ont tué ma fille. C'est injuste. Yamileth est déclarée morte quelques
heures plus tard d'une hémorragie interne causée par la balle. Ça c'est une
photo quand elle était petite. Ça aussi, elle était petite avec sa soeur. Depuis
la mort de l'aînée, Rihanna a bougé dans sa chambre, le bureau où elle étudiait
sa licence de psychologie, la grosse peluche rose, les dessins de manga au mur.
J'aimerais simplement demander justice, car ils ont pratiquement assassiné ma
fille. Un être cher, une vie. Moi, mère, dans quel état ça me laisse? C'est une
douleur indécible. Personne ne peut comprendre cette douleur que nous
traversons. À côté de sa femme, Demetrio lance une vidéo de sa fille sur son
portable. J'attends que les responsables payent pour tout ce massacre du 9. La
justice pour apaiser nos âmes. Les manifestants qui ont été arrêtés, eux, ont
été condamnés tout de suite. 8 mois, 18 mois, immédiatement. Mais pour ceux qui
ont perdu un membre de leur famille, rien. Demetrio n'a aucune nouvelle de
l'enquête de la justice. Il n'a reçu qu'un appel de l'État. On m'a dit, monsieur
Arokipa, c'est bon, on vous fait le virement. L'argent, 12 000 dollars. Mais je
ne suis pas d'accord avec ça. La vie de ma fille vaut beaucoup plus que ça. Elle
n'a pas de prix. Non, l'argent, on peut le gagner en travaillant. Avec cet
argent, on essaie de nous faire taire, de nous acheter. Mais pour moi, c'est
non. Des dizaines de familles font appel à l'association des victimes du 9
janvier créée par le frère de l'une d'entre elles. Plusieurs ONG ont dénoncé ces
derniers mois l'usage abusif d'armes à feu de la part défense de l'ordre. Les
familles de victimes se réunissent régulièrement à la paroisse du quartier San
Miguel. Ce jour-là, des avocats consultent gratuitement. Les familles
patientent. Elles serrent dans leurs bras la photo de leurs proches. Eliséo,
lui, montre ses radios et ses cicatrices. J'ai des blessures sur la main, sur
l'arrière du bras gauche, sur le côté gauche, sur l'épaule. Au total, j'ai reçu
plus de 40 plombs dans le corps. Le 9 janvier, Eliséo, 27 ans, était chez son
oncle près de l'aéroport. Ce chauffeur de taxi assure qu'il ne manifestait pas.
Je peux travailler, mais avec difficulté. Ce n'est plus pareil. J'ai mal au bras
dès qu'il faut forcer. À l'hôpital public, Eliséo n'a pas pu être soignée, faute
de matériel et de compétences. Je suis donc allé à la clinique privée. Là-bas,
oui, ils m'ont extrait les plombs. Il y avait des parties infectées. Mais pour
tout enlever, il faudrait que je paie près de 2000 dollars. J'ai déjà emprunté
beaucoup d'argent à mes proches. Je ne sais pas comment payer ça. Pour les
blessés dits « légers » comme lui, pas d'aide de l'État ni de dédommagement
prévu. Rien non plus pour les conséquences psychologiques. Je suis traumatisé
avec tous ces tirs qui m'ont visé. Je n'arrive pas à dormir la nuit. Je n'arrive
pas à me sentir bien. Je ne sais pas comment dire. Oui, quand j'entends quelque
chose, un bruit de feu d'artifice par exemple, je me mets à trembler. Les morts,
les blessés et un sentiment de mépris du gouvernement, tout cela attise la
contestation dans la région de Puno. Déjà quatre mois de mobilisation et les
habitants se disparaissent à continuer. Encore autant de temps, les prochaines
élections présidentielles sont prévues dans un an. Dans le sud du Pérou, la
région du lac Titicaca en lutte, un grand reportage de Juliette Chénion,
réalisation Pauline Le Duc. Mon amour, approche-toi de moi. Je suis tout secret
dans mon hôtel glacé. Je suis désolé que je sois tard. Je serais là, mais je
t'ai vécu. J'espère qu'il est disparu. Ne me t'inquiètes pas encore et tu peux
vivre là pour toujours. Je ne te blâme pas mais le monnaie est enceinte. La
nature humaine ne fait pas de b***e ni de m***. C'est comme Picasso, le sang de
mon enfant. J'ai hâte de me sentir comme je ne sais pas comment t'aimer. Je ne
vais pas voir que tu fais de l'amour à la même personne. Jour que je me soyez
accompli. Mon amour approche trois de moi Je suis tout secret dans mon hôtel
glacé Le soir on rêve et on s'amuse Le soir on rêve et on chameuche La nuit les
vagues vont m'emporter La nuit les vagues vont m'emporter Pourquoi garder tant
de secrets? Baby girl I know you love me I know you love me I know you love me
full of spunk Tout effacé d'un seul regard Mon amour approche trois de moi Je
suis tout secret dans mon hôtel glacé Mon amour approche trois de moi Je suis
tout secret dans mon hôtel glacé Quelque chose de bien quand j'arrive tout
s'illumine Je suis trop belle quand je regarde dans le miroir Je sais que tu
rentres pas jusqu'à moi Pourquoi tu me regardes mal? Tu me trouves mieux fois
dix fois sans famille? Ce que tu veux faire de Tascale C'est ce que je fais de
moi Je suis avec mon gang, je sais tous tes copiers collés Tu veux juste la
fame, je pouvais faire de la team et le d'or est pillé Je suis devenu occidental
Je reste propre même quand je suis dans le salle Bien ou mal on a trop la dalle
Tu pars c'est la même Peu importe où je suis tout le temps j'te tais de ça Les
rats je veux de ma paire Tu m'écove ma gueule mon flow j'te tais de ça Ici on y
voit clair Pas de coups de putons ce qu'ils font j'te tais de ça On attend juste
la paix Et tes mains on brûle ce game on tait de ça Shoot shoot shoot Mes
ennemis se rondaient dans l'oncelle Shoot shoot shoot Moi j'ai des vices mais
j'en ai un peu rien là Shoot tu attends j'ai pas peur de flancher J'ai tout des
mains par mes côtés Dis-moi à qui tu veux t'attaquer On va tomber qui va te
relever Pire qu'un phénomène Tu me rêves c'est irrationnel Oh oui je sais que tu
m'aimes J'adore obsessionnel Oh oh oh no time Tu calcules perche Je suis bloqué
tout en haut Si jamais tu me cherches Je suis perdu dans les étoiles Si le son
je fluxse Et l'incrétion est verbal like Oh oh oh oh oh J'ai le son et toi t'as
le démon Tu calcules tout mais t'es toujours en bas Plus le time t'as que plus
vos blabla Tu pars c'est la même Peur importe où je suis tout le temps j'te tais
de ça Et là je veux le mappeur Tu me gouges ma gueule mon flow j'te tais de ça
Ici on y voit clair Pas de coups de putons ce qu'ils font j'te tais de ça On
attend juste la paix Et tes mains on brûle ce game on tait de ça Shoot shoot
shoot Mes ennemis se rondaient dans l'onceleur Shoot shoot shoot Moi j'ai des
vices mais j'en ai un peu rien à chute J'attends j'ai pas peur de flancher J'ai
tout des mains par mes côtés Dis-moi à qui tu veux t'attaquer Mon pote tombé qui
va te relier Spava non équipe En pole position Les affaires vont vite comme le
V8 T'as à faire attention Fraque dans mille les élèves et les huit Sors de son
transmission Le gang est dans les bails illicites Ragoune et légende Crois pas
qu'on espère On a grave eu des montagnes L'idée la prospère On est plus farde
que ça Un peu de la lumière surtout nous explose Bébé des pionnières
n'avanceurter pas Tu passes c'est la même Peu importe où je suis tout l'temps
j'dait de ça Mais un jeu vel ma paix Enchimé go ma gueule mon flow j'dait de ça
Ici on y voit clair Pas le coup d'puton ce qu'ils font j'dait de ça On attend
juste la paix Et tes mains on brûle ce game on tait de ça Shoot shoot shoot Mes
ennemis se rondaient dans l'oncele Shoot shoot shoot Moi j'ai des vices mais
j'en ai un peu rien à Shoot tu attends j'ai pas peur de flancher J'ai tout des
mains là par mes côtés Dis-moi à qui tu veux t'attaquer Mon pote tombé qui va te
relever Shoot shoot shoot Shoot shoot shoot Shoot shoot shoot Toi là toi tu veux
tout Sans rien dire pour moi c'est chelou J'sais pas trop c'que t'attends de moi
Eh il veut que j'chois sa nana Me faire des mini mots J'ai senti c'que t'attends
de moi Moi aussi j'suis dans ces laves là J'l'ai senti J'l'ai senti de loin sans
mentir On peut pas se fâcher J'l'ai senti c'que t'as senti Il veut câlins
partout, veut chier tout Partout, aficion et tout Entre nous c'est trop d'or
parce que j'suis sa Baby veut devenir mon taty Eh baby veut devenir mon taty Y'a
comme un truc qui me dérange Des manières qui m'ont mélangé Y'a comme un truc
qui me dérange De tout ça j'ai pas l'habitude Avec moi tu peux te balader Mais
je sais que t'as trop kiffé C'est pas facile mais faut pas lâcher Il faut que tu
me balades J'ai senti J'ai senti de loin sans mentir On peut pas se fâcher
J'l'ai senti c'que t'as senti Il veut câlins partout, veut chier tout Partout,
aficion et tout Entre nous c'est trop d'or parce que j'suis sa Baby veut devenir
mon taty Eh baby veut devenir mon taty Toi là toi tu veux tout Sans rien dire
pour moi si j'suis lou J'sais pas trop c'que t'as senti moi Eh il veut Il veut
câlins partout, veut chier tout Partout, aficion et tout Entre nous c'est trop
d'or parce que j'suis sa Baby veut devenir mon taty Eh baby veut devenir mon
taty Baby, Tati A une chose tout à fait passionnante Il fera du bruit A une
chose tout à fait passionnante Il fera du bruit A une chose tout à fait
passionnante Il fera du bruit Dirty city on the sea My deep friend don't take
ground on me I heard you got a bite to eat Now you got the swelling in me You
can give me a ride Play some with the light Go the extra mile I'm sure you'll
find Sometimes to play Let's go and compare the world I'm sure you'll find
Sometimes to play Let's go and compare the world Mercy, mercy me Let's go for
something more Before we turn into dust let's go out Talking about dirty turkey
please Let's go and ball the sail out Boy there's no love in the world let's
sail out All we are Let's go and play I'm sure you'll find Sometimes to play
Let's go and compare the world I'm sure you'll find Sometimes to play Let's go
and compare the world I'm sure you'll find Sometimes to play Let's go and
compare the world I'm sure you'll find Sometimes to play Let's go and compare
the world Y'a des jours comme ça Tu ne sais pas pourquoi mais tout va bien Je me
lève le matin Le car uni a l'univers Plexus solaire comme un refrain Tends d'une
pose sur mes calvaire Chante de rose dans ma tête Envie d'embrasser l'inconnu
Penser a rien et faire la fête Que du plaisir se le met en vue Y'a des jours
comme ça Tu ne sais pas pourquoi mais tout va bien J'ai lancé un appel Même la
météo m'a répondu Le soleil brille sur une chasse Aïe! Le pas de danse est perdu
Allez viens, t'es avec moi Aujourd'hui ma vie est arc-en-ciel Même le bonheur de
l'entrevoir Il étouffe le caramètre Y'a des jours comme ça Tu ne sais pas
pourquoi mais tout va bien Y'a des jours comme ça Tu ne sais pas pourquoi mais
tout va bien Y'a des jours comme ça Tu n'es pas pour moi mais tout va bien Y'a
des jours comme ça Tu ne sais pas pourquoi mais tout va bien Y'a des jours comme
ça Tu ne sais pas pourquoi mais tout va bien Koloya et Sego, tout va bien Koloya
et Sego, tout va bien Tout va bien Y'a des jours comme ça Tu ne sais même pas
pourquoi mais tout va bien Y'a des jours comme ça Tu ne sais pas pourquoi mais
tout va bien Y'a des jours comme ça Tu ne sais même pas pourquoi mais tout va
bien Y'a des jours comme ça Tu ne sais même pas pourquoi mais tout va bien
Koloya et Sego, tout va bien Y'a des jours comme ça Tu ne sais même pas pourquoi
mais tout va bien Très belle journée! Le soleil! Tout va bien! C'est la journée!
Belle journée! Approche-toi petit, écoute-moi gamin Je vais te raconter
l'histoire de l'être humain Au début y'avait rien, au début c'était bien La
nature avançait, y'avait pas de chemin Puis l'homme a débarqué avec ses gros
souliers Des coups de pied dans la gueule pour se faire respecter Des routes à
Sansonique, il s'est mis à tracer Les flèches dans la plaine se sont multipliées
Et tous les éléments se sont vus maîtrisés En deux temps trois mouvements
l'histoire est épliée C'est pas demain la veille qu'on fera marche arrière On a
même commencé à polluer les déserts Il faut que tu respires Et ça c'est rien de
le dire Tu vas pas nous rire de rire Et c'est pas rien de le dire D'ici quelques
années on aura bouffé la feuille Et tes petits enfants ils n'auront plus qu'un
œil En plein milieu du front ils te demanderont Pourquoi t'en as deux tu
passeras pour un con Ils te diront comment t'as pu laisser faire ça T'auras beau
te défendre leur expliquer tout bas C'est pas ma faute à moi c'est la faute aux
anciens Mais y'aura plus personne pour te laver les mains Tu leur racontera
l'époque où tu pouvais Manger des fruits dans l'air, balanger dans les prés
Y'avait des animaux partout dans la forêt Au début du printemps les oiseaux
revenaient Il faut que tu respires Et ça c'est rien de le dire Tu vas pas nous
rire de rire Et c'est pas rien de le dire Il faut que tu respires C'est demain
que tout t'en vire Tu vas pas nous rire de rire Et c'est pas rien de le dire Le
pire dans cette histoire c'est qu'on est des esclaves Quelque part assassin ici
bien incapable De regarder les arbres sans se sentir coupable Un moitié
défroqué, 100% misérable Alors voilà petite histoire de l'être humain C'est pas
joli joli et je connais pas la fin T'es pas né dans un chou, n'est plutôt dans
un trou On remplit tous les jours comme une fausse apurin Il faut que tu
respires Et ça c'est rien de le dire Tu vas pas nous rire de rire Et c'est pas
rien de le dire Il faut que tu respires C'est demain que tout t'en vire Tu vas
pas nous rire de rire Et ça c'est rien de le dire Il faut que tu respires Il
faut que tu respires Il faut que tu respires Il faut que tu respires Aucune
histoire banale gravée dans ma mémoire Aucun bateau pirate ne prendra le pouvoir
Aucune étoile filante me laissera dans le noir Aucun trac, aucun... Et demain
tout ira bien Tout sera loin Là au final Quand je prendrai le large Tout sera
loin Donne-moi la main Là au final Quand je prendrai le large Aucune larme,
aucune viendra m'étrangler Aucun nuage de brume dans mes yeux délavés Aucun
sable ni la dume arrête le sablier Aucun quartier de lune, aucun... Et demain
tout ira bien Tout sera loin Là au final Quand je prendrai le large Tout sera
loin Donne-moi la main Là au final Quand je prendrai le large Aucun autre décor,
aucun autre que toi Aucune clé à bord, aucune chance pour moi Et demain tout ira
bien Tout sera loin Là au final Quand je prendrai le large Tout sera loin Donne-
moi la main Là au final Quand je prendrai le large Aucun requin, aucun air
triste Aucun regret, aucun sismes Aucune langue de bois Aucun chaos, aucun aucun
Et demain tout ira bien Tout sera loin Là au final Quand je prendrai le large
Tout sera loin Donne-moi la main Là au final Quand je prendrai le large C'est
doulundi, c'est douludun Pandillé devant la vie, dans le crélevant Donne un
silence, manipule, largue mon cri Dans le volant sans pouvoir chanter résistants
Pour en chanter résistants Dans le roulegal et sous-maviers, cascade la poulet
Donne à moi mon l'air pour un monde courant mon fond de coeur Dans le roulegal
et sous-maviers, cascade la poulet Donne à moi mon l'air pour un monde courant
mon fond de coeur Donne à moi mon l'air pour un monde courant mon fond de coeur
Toi-même bébête mon minuit Qui se l'envoie à toi Toi-même bébête mon minuit
T'errances mon rêve Toi-même fait noir mon nuit Mais demain soleil à l'air Mais
demain soleil à l'air Oui, la la la la la Dans le roulegal et sous-maviers,
cascade la poulet Donne à moi mon l'air pour un monde courant mon fond de coeur
Dans le roulegal et sous-maviers, cascade la poulet Quitte à moi mon l'air pour
un monde courant mon fond de coeur Quand ça le temps y commence Quand ça le
temps y finit Juste pour une histoire, un romance Quand ça le temps y commence
Quand ça le temps y finit Juste pour une histoire, un romance Dans nos ordis
infinis S'amant le soleil la vie Un infini dans nos ordis Toi-même bébête mon
minuit Dans le roulegal et sous-maviers, cascade la poulet Quitte à moi mon
l'air pour un monde courant mon fond de coeur Quitte à moi mon l'air pour un
monde courant mon fond de coeur Dans le roulegal et sous-maviers, cascade la
poulet Quitte à moi mon l'air pour un monde courant mon fond de coeur Dans le
roulegal et sous-maviers, cascade la poulet Quitte à moi mon l'air pour un monde
courant mon fond de coeur L'un lundi et l'autre C'est lundi, c'est lundi C'est
lundi C'est lundi C'est lundi C'est lundi C'est lundi C'est lundi C'est lundi