TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-26-17H00M.MP3
--------------------------------------------------------------------------------
 Vous écoutez RFI, il est 17h à Paris, 15h en temps universel. Le journal vous
est présenté par Stéphane Duguet. Bonjour à toutes, bonjour à tous. À la une de
cette édition, l'Ukraine nomme un ambassadeur en Chine. L'annonce fait suite à
un entretien entre Volodymyr Zelensky et Xi Jinping, le premier ennemi depuis le
début de la guerre. Nous serons à Pékin en début de journal. Nous parlerons
aussi de cette interception de trois avions russes au-dessus de la mer baltique.
Ce genre d'intervention se multiplie. La dernière, remontée au 17 avril. Nous
irons aussi en Iran, où les autorités continuent de réprimer des enfants. Un
nouveau rapport du ONG alerte sur le sujet. Et enfin, pas de projet de loi
immigration en France. Le gouvernement annonce ne pas avoir de majorité.
L'Ukraine a nommé un ambassadeur en Chine. Annonce de Volodymyr Zelensky après
son entretien avec le président chinois, le tout premier depuis le début de
l'invasion russe l'année dernière. La Chine indique que l'échange a eu lieu à la
demande de Kiev. Xi Jinping a affirmé que son pays était toujours du côté de la
paix. Correspondance à Pékin, Stéphane Lagarde. La dépêche de l'agent chine
nouvelle résumant le coup de téléphone présidentiel a été lue pendant plus de
trois minutes ce mercredi par le présentateur des nouvelles du soir de la
télévision centrale de Chine. Dans l'après-midi de ce 26 avril, Xi Jinping s'est
entretenu par téléphone avec le président ukrainien Zelensky, indique le Média
d'État. Le respect mutuel de la souveraineté et de l'intégrité territoriale et
la base des relations ukrainiennes auraient fait savoir le numéro un communiste.
Xi Jinping a notamment remercié son homologue pour l'aide apportée à
l'évacuation des ressortissants chinois en Ukraine au début du conflit.
Réaffirmation de l'opposition à l'usage de l'arme nucléaire et recherche de la
paix via le dialogue et la concertation. La Chine enverra son représentant
spécial pour les affaires eurasiennes en Ukraine et dans d'autres pays pour une
communication approfondie avec toutes les parties sur le règlement politique de
la crise ukrainienne a encore fait savoir le président chinois. Les médias
chinois ne présistent pas en revanche ce que lui a répondu Volodymyr Zelensky.
Ce qu'on sait c'est que ce dernier avait invité le dirigeant chinois à venir
visiter l'Ukraine fin mars dernier. Selon le Wall Street Journal, Xi Jinping
aurait songé à appeler Kiev à la mi-mars. Puis les dirigeants européens ont à
chaque visite ici appelé le président chinois à passer ce coup de fil. Il
viendra au moment opportun, avait répondu ce dernier. Le moment est donc venu.
Stéphane Lagarde, Pékin et Refi. Côté ukrainien Volodymyr Zelensky n'a pas donné
beaucoup de détails supplémentaires sur cet entretien. Il explique seulement
avoir eu un entretien long et significatif avec son homologue chinois. Du côté
des réactions internationales, la France salue le dialogue entre les présidents
chinois et ukrainiens et encourage tout dialogue pour contribuer à une
résolution du conflit. Avis plus mitigé en Russie, le Kremlin dit prendre acte
de la volonté chinoise de mettre en place un processus de négociation. Moscou
reproche aussi à l'Ukraine de rejeter toute initiative pour régler le conflit.
La guerre en Ukraine continue d'avoir des répercussions diplomatiques sur les
relations qu'entretient la Russie avec ses voisins européens. Moscou a décidé de
renvoyer 10 diplomates norvégiens. Une mesure prise en représailles de
l'expulsion de 15 diplomates russes de Norvège mi-avril. Ils étaient accusés
d'espionnage. La Russie présente aussi dans les airs et sous les radars en
Europe. Trois avions militaires russes ont été interceptés par l'Allemagne et la
Grande Bretagne. Aujourd'hui, ils se trouvaient au-dessus de la mer baltique. La
dernière interception du genre remontait au 17 avril, correspondance à Berlin-
Pascal-Tibau. C'est un cammaïeux de bleu qui apparaît sur les trois photos du
compte Twitter de la Louvre de Waffen. On reconnaît les trois avions russes et
même les inscriptions qu'ils portent. L'armée de l'air allemande a publié l'info
au petit matin en précisant les deux SU-27 Flanker et un IL-20 volaient à
nouveau sans signaux de transpondeurs dans l'espace aérien international au-
dessus de la mer baltique. L'Allemagne et la Grande Bretagne effectuaient
ensemble un vol de surveillance de l'espace aérien des pays baltes dans le cadre
de l'OTAN. L'Estonie, la Lituanie et la Létonie qui ont des frontières communes
avec la Russie n'ont pas d'avions de combat. L'Organisation transatlantique est
chargée de ces missions. Les incidents impliquant des avions russes et les
appareils de pays membres de l'OTAN sont réguliers et nombreux. Ils avaient déjà
augmenté avant le début de l'invasion russe contre l'Ukraine. La dernière
interception remonte à moins de dix jours. La mer baltique est souvent concernée
mais aussi la mer noire et d'autres zones. Pascal-Tibau, Berlin RFI. C'est une
première depuis la fin de la guerre froide. Les États-Unis vont envoyer un sous-
marin nucléaire faire une escale en Corée du Sud. L'objectif affiché est de
dissuader la Corée du Nord de toute attaque. Cette escale devrait être annoncée
officiellement par le président sud-coréen, Yun-Soo Kyeol en visite aux États-
Unis et le président américain Joe Biden qui s'entretiennent en ce moment à la
Maison-Blanche. Les États-Unis qui ont permis la signature d'un cessez-le-feu au
Soudan, la trêve dans les combats entre l'armée régulière du général Al-Burhan
et le groupe paramilitaire du général Emeti est fragile. Dans la capitale
Khartoum, elle est partiellement respectée. Des raids aériens et des tirs ont
encore lieu. L'Organisation mondiale de la santé s'inquiète de la fermeture de
près de 60% des établissements de santé. De nombreux patients atteints de
maladies chroniques comme le diabète ou le cancer n'ont pas accès aux
établissements de santé et aux médicaments, explique le directeur général de
l'OMS. Le bilan grimpe à 95 morts, au Kenya 95 personnes membres d'une secte qui
prônait le jeune pour retrouver Jésus. Les autorités continuent les recherches
de corps à l'est du pays. 39 personnes ont aussi été retrouvées vivantes.
Direction l'Iran. À présent, où un ayatollah a été assassiné aujourd'hui, il se
trouvait à Bab al-Shar au nord du pays. L'ayatollah Abbas Ali Soleimani était
l'un des 88 membres de l'Assemblée des experts qui nomme notamment le guide
suprême iranien. Cette attaque intervait dans un contexte toujours tendu, sept
mois après la mort de Marsa Amini. La jeune femme était morte après son
enrestation par la police pour un port de voile jugé non conforme. L'ONG, Human
Rights Watch, a publié un nouveau rapport sur la répression iranienne des
enfants. L'association documente 11 cas d'abus commis sur des mineurs. Écoutez
Bill Vannesfeld, directeur associé des droits de l'enfant à Human Rights Watch.
Nous avons examiné des cas dans différentes régions du pays et à différents
stades d'interaction de l'enfant avec les autorités. Nous avons documenté un
recours à la force lors des manifestations avec des passages à tabac. Ensuite,
que se passe-t-il si l'enfant est détenu? Parfois, on assiste à des
disparitions. Personne ne sait où l'enfant a été emmené. Et puis nous avons
recensé des cas de torture vraiment horribles, y compris des abus sexuels. Des
enfants battus à tel point qu'ils ne peuvent plus marcher. D'autres à qui l'on
plante des clous et des aiguilles dans les doigts, ceux qui reçoivent des coups
de fouet ou sont suspendus la tête en bas. Enfin, si l'on en vient à la phase
judiciaire, les enfants sont censés passer devant un tribunal spécial pour
mineurs. Mais aujourd'hui, ils comparaissent devant des tribunaux
révolutionnaires qui les condamnent à des peines grotesques en plus d'être
injustes. Et même s'ils sont libérés, on voit des cas où les écoles les
empêchent de se réinscrire. C'est ainsi. Chaque étape est une violation des
droits de l'enfant. Des propos recueillis par Nicolas Feldman. La Cour suprême
iranienne a par ailleurs confirmé mercredi la condamnation à mort du dissident
irano-allemand Jamshid Sharmad pour son implication présumée dans un attentat en
2008. Berlin juge cette décision inacceptable. Il est 10h07 à Bogota où
l'ensemble du gouvernement colombien est appelé à la démission par le président
de gauche Gustavo Petro. Le président fait face à des difficultés pour faire
adopter ses projets de loi au Congrès. Mardi, il avait appelé à la refonte du
gouvernement sur Twitter. Gustavo Petro, en poste depuis le mois d'août, n'a
toujours pas réalisé des changements profonds dans le domaine du travail ou la
santé, par exemple qu'il avait promis lors de sa campagne présidentielle. Plus
au sud, au Brésil, l'ancien président d'extrême droite Jair Bolsonaro est
auditionné par la police fédérale. Les enquêteurs s'interrogent sur son rôle
dans les saccages des lieux de pouvoir à Brasilia. C'était le 8 janvier dernier.
L'ancien président n'a fait aucune déclaration à son arrivée dans les locaux de
la police. 17h08 à Paris. Le gouvernement français a présenté sa feuille de
route pour tenter de tourner la page de la réforme des retraites. Elisabeth
Borne, la première ministre, a annoncé et renoncé à une priorité du président de
la République, un projet de loi sur l'immigration avant l'été. On l'écoute.
Aujourd'hui, il n'existe pas de majorité pour voter un tel texte. Comme j'ai pu
le vérifier hier, en m'entret tenant avec les responsables des républicains, ils
doivent encore dégager une ligne commune entre le Sénat et l'Assemblée, tenant
compte de la nécessité de trouver une majorité dans chaque assemblée autour d'un
texte nécessairement équilibré. Aujourd'hui, ils n'y sont manifestement pas. Par
ailleurs, ça n'est pas le moment de lancer un débat sur un sujet qui pourrait
diviser le pays. Nous allons donc continuer les échanges pour trouver un chemin
autour du projet de loi. Et si nous ne pouvons pas trouver d'accord global, nous
présenterons en tout état de cause un texte à l'automne avec comme seul boussole
l'efficacité. La première ministre française, Elisabeth Borne, ce discours, très
attendu pour connaître la feuille de route du gouvernement, pour se relancer, a
largement été critiqué par les oppositions. Le fondateur de la France Insoumise,
Jean-Luc Mélenchon, appelle le gouvernement à s'en aller. Le Parti de droite,
les Républicains, a annoncé déposer son propre texte immigration. La démarche
est commune entre les sénateurs et les députés LR. L'actualité française est
aussi cette conférence sur le handicap. Le Conseil de l'Europe accuse la France
de violer les droits des personnes en situation de handicap. Elles sont 12
millions en France. Les annonces devraient être faites d'ici la fin de journée,
presque 17h10, ici à Paris. Bonjour, bonsoir. Quand vous visitez un musée, avez-
vous déjà songé à ce qui se passe une fois les portes refermées? Savez-vous que
les statues pouvaient redescendre de leur socle et les modèles de leur toile se
déplacer tels un chat ou un ours dans les galeries, se dégoudir les jambes et
même se retrouver pour discuter, raconter les derniers potins du petit monde des
œuvres et critiquer les visiteurs du jour? Eh bien, c'est ce qu'a imaginé ce
maître du noir et blanc qui nous livre un roman graphique haut en couleur, dans
le décor bien réel du prestigieux Musée d'Orsay, qui plus est avec un casting
qui va du Mercure d'Henri Chaput à l'Olympia de Manet, en passant par l'Héraclès
de Bourdel, Berthe Morizot ou les célèbres statuettes d'Honoré Domier. Un album
plein de poésie, de fantaisie et de malice qui interroge notre rapport à l'art
et aux œuvres, Musée de Christophe Chaboutet et Paris chez Glena dans la
collection Vend Ouest et vous m'en direz des nouvelles. Bonjour Christophe
Chaboutet, avant de parler de vos personnages en chair, en os, mais aussi en
bronze, en terre cuite ou en toile, on va commencer par évoquer leur écringé en
ce Musée d'Orsay, qui se trouve donc à Paris, évidemment, au bord de la Seine, à
la rive gauche juste en face du Journal des Tuileries, dans la diagonale du
Musée du Louvre. Graphiquement parlant, le Musée d'Orsay n'est pas n'importe
quel musée puisque c'est une ancienne gare. Est-ce que vous pouvez, là, en
quelques mots, nous décrire le bâtiment? Oh là là, vous décrire le bâtiment,
alors je pourrais peut-être mieux vous le dessiner que vous le décrire, mais
c'est un bâtiment énorme avec beaucoup de bas-reliefs et c'est très très
plaisant à dessiner, il y a des grandes fenêtres. Il y a la voûte et la verrière
qui sont très importants, ça donne aussi une luminosité très particulière. Oui,
puis il y a la Célébrée Grande-Orloge, graphiquement, c'est un bâtiment vraiment
génial à dessiner, vraiment chouette. Alors c'est vrai que la Grande-Orloge,
l'Orloge à cadran, elle est très présente dans votre album à de multiples
moments, c'est un lieu de rendez-vous pour vos personnages, pour les statues en
particulier qui descendent de leur socle. Il y a surtout quatre statues qui se
retrouvent tous les soirs pour regarder par là, à travers la Grande-Orloge et
voir ce qui se passe dans la rue. C'est toujours les quatre mêmes qui sont là et
qui ont toujours des trucs à dire sur ce qui se passe dans la rue, notamment les
voitures qui passent, les voitures avec les jolies couleurs, les voitures avec
des gyrofarces, enfin pour eux, ils ne savent pas ce que c'est une voiture. Pour
eux, c'est des boîtes à roulettes, donc il y a des boîtes à roulettes qui ont
des lumières bleues, il y a des boîtes à roulettes qui ont des lumières oranges
et qui s'arrêtent devant des lumières rouges et qui repartent devant des
lumières vertes. Donc c'est un peu les pipettes, ils ont plein de choses à
raconter en gardant dehors, à l'extérieur du monde. Et donc en fait, cette
horloge, ce n'est pas seulement le temps qui passe, c'est surtout une fenêtre
sur le monde, une fenêtre sur la vie, une fenêtre sur l'extérieur. Ah bah c'est
surtout une fenêtre, ça leur permet, c'est là à peu près le seul endroit où ils
peuvent se grouper à quatre pour voir dehors et pour voir ce qui est
effectivement ce qui se passe à l'extérieur du musée. Voilà, à l'extérieur, il y
a des passants, notamment un homme qui promène son chien, qui joue un rôle
important dans votre récit, et puis il y a aussi les statues en fonte de fer qui
se trouvent sur le parvis. Les touristes connaissent bien puisque quand ils font
la file d'attente pour rentrer dans le musée, ils ont la possibilité de les
regarder, ces statues en fonte de fer. Il y a les quatre statues d'animaux dont
Henry de Séros, sur la corne duquel un oiseau vient se percher dans l'album. Un
petit moment de poésie. Et puis il y a les fameuses statues des six continents.
Alors cette ancienne gare, elle date de l'exposition universelle de 1900. C'est
un petit peu comme le petit palais et le Grand Paris. Elle a ouvert ses portes
en tant que musée en 1986. Elle accueille entre 3 et 4 millions de visiteurs par
an. Et c'est par eux qu'on va commencer cette émission. Notre reporter Thomas
Cartini était hier sur place au musée d'Orsay et il a demandé tout simplement
pour quelle raison les visiteurs étaient là. Pour se cultiver et apprendre de
nouvelles choses et découvrir le patrimoine français. Nous, en fait, on est en
sortie scolaire, mais c'est vrai que c'est un moyen de découvrir la culture
française et même la culture européenne plutôt. Et puis c'est aussi très beau.
C'est le principal. En fait, je suis venu avec un groupe d'amis italiens de
Florence et ils voulaient découvrir un peu Paris et bien sûr les musées. C'est
un des plus importants donc voilà. C'est pour voir un peu les œuvres et
découvrir aussi la structure qui reste quand même une des plus belles de Paris.
Parce qu'il y a des jolies choses à voir. Son histoire au niveau déjà le tour et
puis les collections. Enfin pour moi c'est le plus beau musée. J'aime beaucoup.
C'est l'architecture est très simple de façon. Comment tu t'appelles? Archi.
Pourquoi est-ce que tu viens au musée? Je lui explique à l'école que tu as lu.
Bon, ceci dit, il a trois ans. Alors je peux vous expliquer. Alors à l'école, il
a lu mon petit Orsay, donc le livre de Marie Céliais. Et il a voulu voir en vrai
les œuvres que le petit livre donnait. C'est authentique parce que souvent dans
les musées, il n'y a que des peintures qui sont pareilles et tout. Alors que là,
il y a de la diversité. C'est intéressant parce que c'est mieux de voir les
œuvres en vrai que sur Internet. Ça permet de sortir et de découvrir des
tableaux qu'on ne verrait pas forcément sur Internet. Alors c'est vrai ça
Christophe Chagoutet. Pourquoi aller au musée aujourd'hui quand on peut voir
toutes les œuvres quasiment du musée d'Orsay en quelques clics sur son
ordinateur? Parce qu'on les touche moins bien avec les yeux. Parce qu'il n'y a
pas la matière, parce qu'il n'y a pas la lumière qui change, parce qu'il n'y a
pas l'odeur de... Parce qu'il n'y a pas l'ambiance du musée, il n'y a pas
l'atmosphère. Et puis c'est de la toile, c'est de la pierre, c'est du granit. On
n'a pas le droit de toucher mais on peut toucher avec les yeux et on touche
beaucoup mieux avec les yeux que sur un écran. Et quand vous visitez une
exposition permanente ou temporaire dans un musée, le musée d'Orsay ou un autre,
vous regardez les œuvres uniquement avec les yeux ou bien vous le faites aussi à
travers votre téléphone portable ou qui sait, à travers votre carnet de croquis?
Oh non, j'essaye de les regarder avec les yeux. Après, si je les regarde avec
mon appareil photo ou avec mon portable, c'est dans ces prétextes à me faire de
la documentation ou à préparer un travail effectivement. Ce n'est pas pour créer
une galerie de photos sur internet, etc. Parce que de plus en plus de visiteurs
utilisent ça. D'ailleurs, on a presque l'impression dans les grands musées
extrêmement fréquentés où finalement on ne prend pas le temps d'observer les
œuvres à travers son propre regard mais qu'on le fait toujours par le truchement
d'un téléphone ou d'un appareil photo, d'un tire-trag. C'est peut-être une façon
de vouloir les posséder aussi ou surtout de dire j'y étais. Moi j'ai essayé
surtout d'y aller mais pas montrer que j'y étais justement. Je faisais ma petite
souris, quand j'y étais je me cachais. Alors justement, il y a dans l'album une
sorte de galerie de portraits d'un certain nombre de visiteurs. Certains d'entre
eux, c'est vraiment la première scène de l'album, certains d'entre eux ont un
appareil photo ou un téléphone à la main ou devant les yeux. Et puis on a un qui
est assez particulier puisqu'il a un beret sur la tête, des baisiques et un
carton à dessin. Est-ce que ce personnage-là vous ressemble, est-ce que vous
étiez comme ça dans les salles d'exposition pour croquer les œuvres avec un
grand carton à dessin comme celui-ci? Non, j'avais pas de grand carton à dessin
parce que je voulais passer inaperçu surtout. Un petit calepin quand même non?
Un tout petit calepin oui. Et là encore, quand je faisais des croquis, il
m'arrivait de faire des croquis mais pas forcément des jolis croquis qu'on peut
mettre dans les livres. C'était plus des notes dessinées que des... C'était
vraiment que des notes, le croquis en lui-même n'avait aucun intérêt pour la
personne qui pouvait le voir. Par contre pour moi, c'était vraiment une note
dessinée, un repère. Et donc c'est après chez vous, dans votre atelier, que vous
avez tout dessiné? Après j'ai tout dessiné, retravaillé, effectivement j'ai fait
beaucoup beaucoup de photos aussi. J'y suis allé trois fois dans ce musée le
premier jour, j'y suis allé enfin trois jours, pas trois jours d'affilée mais
j'y suis allé trois fois une journée. Et la première fois que je suis allé me
promener là-bas, je me suis baladé les mains dans les poches. Alors j'avais déjà
l'idée de raconter une histoire qui se passe au musée d'Orsay mais je me suis
promené là-bas vraiment les mains dans les poches et je me suis... J'y suis allé
l'esprit complètement libre, je me suis laissé happé par le musée, happé par les
œuvres. Et en fait je suis allé me promener quoi. Et la deuxième fois j'y suis
retourné, j'avais déjà plus ou moins une structure de récit en tête, une base de
scénario et je savais à peu près vers quoi je voulais aller. Donc là mon œil a
changé, là j'ai commencé à prendre des photos, à faire vraiment du repérage, à
vraiment travailler quoi. Et la troisième fois que j'y suis retourné, c'était...
J'ai fait un peu une combinaison des deux mais j'ai essayé aussi de me...
Comment je peux vous dire ça? J'ai essayé de me mettre à la place des peintures
ou à la place des sculptures. Ça veut dire que je me mettais à côté d'une
sculpture et je regardais pas la sculpture ou je regardais pas la peinture mais
je regardais les gens qui les regardaient. Ce livre en fait raconte ce que
peuvent raconter les regardés, des regardeurs et ce qui m'intéresse beaucoup
dans l'histoire que j'ai raconté, c'est ce qu'en disent les regardeurs et
surtout leur manière de regarder les œuvres. C'est donc le regard de Christophe
Chaboutet sur le regard des visiteurs. On peut dire ça comme ça. Et c'est vrai
que c'est pas les visiteurs que commence l'album. Les trois premières pages ce
sont des portraits alignés, un peu façon gaufrier comme on dit en BD, cadrés au
niveau du visage ou au niveau du buste. Parfois ils ont la tête bien droite,
parfois la tête penchée, parfois ils ont une expression sur le visage, une
petite moue dubitative par exemple. Parfois il y a l'appareil photo, le
téléphone portable qu'on a évoqué. Pourquoi est-ce que c'était indispensable de
commencer par cette galerie de portraits? C'est un peu le petit musée à vous,
pour le coup Christophe Chaboutet. Oui, c'est une galerie de portraits mais elle
n'est pas forcément exhaustive non plus. Ce qui m'intéressait surtout c'était le
regard des gens. C'est pour ça que je les ai cadrés au niveau... C'est presque
des bustes quoi, mais c'est des bustes de visiteurs. Et je voulais qu'on voit
leur regard, la manière dont ils regardent, levez les yeux et effectivement
pencher la tête. Alors quand on penche la tête, on voit pas forcément mieux
quoi. C'est comme quand on se perd dans une ville et on baisse le son de l'auto-
radio, ça sert strictement à rien mais on le fait quand même, on sait pas
pourquoi. Et c'était... Je voulais ouvrir le musée sur les visiteurs en fait,
parce que c'est les vedettes, c'est eux, c'est eux les stars. Alors on les
retrouve tout au long du récit, les visiteurs que vous dessinez en ouverture.
Certains sont des spécialistes un peu pédants, comme ce monsieur barbu en
costume cravate qu'on le retrouve, page 52. Substantifique, constitutif dans
cette toile, le rythmi de la pluralité chromatique, la concaténation et la
contexture des constituants picturaux, l'édulcoration des flexiosités, ce degré
supplétif, cette primauté à la peinture sensorielle, un souffle
incontestablement et de manière apodictique les prémisses de l'abstraction.
D'autres sont des profanes complets, peut-être un peu plus sensibles, comme
cette jeune fille, devant le même tableau, en l'occurrence les coquelicots de
Manet. Cette douceur, ce calme, cette sérénité, on entend le doux et léger
frottement de l'herbe contre les robes. Voilà cette scène, Christophe Chaboutet
à la vite à rire forcément, mais elle en dit beaucoup finalement sur les
visiteurs de musée. Elle en dit beaucoup et c'est chouette d'entendre parler ces
personnages. Je suis tout, je suis très ému, ça fait bizarre de les entendre et
puis les voix collent très bien en plus. J'ai oublié votre question. Oui voilà,
à la vite à rire, disait cette scène-là notamment à travers le premier, le
personnage barbu qui étale, je ne fasse pas dire sa science, en tout cas des
mots que je ne connaissais absolument pas et je pense que je ne suis pas le
seul, elle prête à rire et en même temps elle montre l'extrême diversité du
public qu'il peut y avoir dans un musée comme le Musée d'Orsay. Oui et puis il y
a tellement de gens qui viennent en parler pour s'écouter parler et puis il y a
tellement de des brouffes, il y a tellement d'emphase et en fait le but quand on
veut voir un musée c'est l'émotion, c'est pas forcément la culture pour moi,
l'idée c'est pas d'afficher sa culture. Moi j'adore quand on raconte une
histoire, l'histoire d'une toile, l'histoire d'une peinture, comment elle est
née et tout mais ce que j'aime beaucoup aussi c'est quelqu'un qui arrive dans ce
musée sans bagage, sans prétention de savoir ou de vouloir savoir et qui a une
émotion devant une peinture ou une sculpture ou une toile sans forcément être au
fait de la peinture ou de la sculpture. Et cette émotion-là elle s'exprime par
les mots comme dans la scène qu'on vient d'entendre, elle s'exprime aussi comme
vous le disiez tout à l'heure, par le regard, comment le dessinateur que vous
êtes fait sentir les émotions d'un regard? Il y a une technique, c'est une
question de largeur de trait? Je peux pas vous répondre à cette question ou
alors faudrait qu'on prenne une semaine quoi. Comment je fais ça? Je crois qu'il
n'y a pas de recette technique, il n'y a pas de recette graphique, il y a juste
le fait de vouloir le dire avec sincérité. C'est l'intention qui compte? C'est
vraiment, c'est que l'intention oui, bien sûr, c'est que ça. Si elle n'y est
pas, il n'y a pas d'émotions. Alors le regard, les yeux, c'est vraiment un des
points forts de cet album, Christophe Chaboutet. Et puis il y a aussi une
séquence toujours à propos des visiteurs du Musée d'Orsay, dans laquelle vous
focalisez non pas sur les yeux, mais sur les jambes et sur les pieds des
visiteurs. Les pieds, ils disent beaucoup de nous, notamment dans un musée?
Surtout les pieds oui, quand vous observez quelqu'un qui regarde, soit ils sont
statiques, ils sont ancrés dans le sol, soit ils se mettent sur la pointe des
pieds pour essayer de voir une statue qui est grande, alors que en se mettant 3
cm plus haut, on ne la verrait pas mieux. Ou il y a des gens qui piétinent, il y
a des gens qui ont besoin de bouger, il y a des gens qui sont là, ils n'ont pas
envie d'être là, ils s'ennuient un peu, donc ils piétinent aussi. Il y en a qui
partent, qui reviennent, il y en a qui ne s'arrêtent pas. Et il y a tout un
balai des pieds, il y a tout un balai des jambes et tout. Et en plus c'est
rigolo aussi parce que à travers les chaussures, on arrive à plus ou moins
coller, pas coller une étiquette aux gens, mais à déterminer plus ou moins la
personnalité des gens où ça aide à... Il y a un côté... Prasco sociologique
dirons-nous? Oui, il y a un peu de ça, mais c'est surtout dans la manière de
regarder, dans la manière de se poser pour regarder, d'être devant l'œuvre en
fait. Day-oh, day-oh, daylight come and me wang-oh Day, me say day, me say day,
me say day, me say day, me say day-oh Daylight come and me wang-oh Work all
night and I drink a rum Daylight come and me wang-oh Stack banana till the
morning come Daylight come and me wang-oh Come Mr. Dali man, dali me banana
Daylight come and me wang-oh Come Mr. Dali man, dali me banana Daylight come and
me wang-oh People, 6 foot 7 foot 8 foot bunch Daylight come and me wang-oh 6
foot 7 foot 8 foot bunch Daylight come and me wang-oh Day, me say day, oh
Daylight come and me wang-oh....................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
..