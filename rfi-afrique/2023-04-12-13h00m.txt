TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-12-13H00M.MP3
--------------------------------------------------------------------------------
... Vous écoutez RFI, les 11 heures en temps universel, 13 heures à Paris.... Le
journal présenté par Anne Cantener. Bonjour Anne. Bonjour Emmanuelle, bonjour à
tous. À la une, une vidéo qui choque l'Union européenne, les Nations unies et
les autorités ukrainiennes. Selon Kiev, on y voit un soldat russe décapité un
prisonnier de guerre ukrainien. Un Moscou demande l'authentification de la
vidéo. Le président français de nouveau eué au deuxième jour de sa visite à
Amsterdam. Deux personnes ont été interpellées ce matin devant l'université
d'Amsterdam. Et puis du football à suivre. Ce soir, suite des quartes finales de
la Ligue des champions, deux rencontres au programme Naples-Milanassé et Chelsea
face au Real Madrid.... D'intenses bombardements dans l'est de l'Ukraine. Les
Russes frappent tout le long de la ligne de front. Et se rapprochent toujours
plus du contrôle total de Barhmouth. La ville est devenue un symbole. Elle est
au cœur de violents combats depuis plusieurs mois. Maintenant, les forces russes
contrôlent environ 80% de la ville, selon les autorités. Mais personne en Russie
ne revendique la conquête de Barhmouth. Ni l'armée, ni le groupe Wagner. Et ce
n'est pas anodin, à Nice Al-Jabri. Après la rivalité affichée au moment de la
chute de la ville ukrainienne de Soledar, la communication publique entre Wagner
et le ministère russe de la Défense semble avoir été lissée. C'est Evgeny
Prigogine qui a d'abord envoyé le premier message en forme de rameau d'Olivier
dans une vidéo publiée à la mi-journée mardi, assis à son bureau avec des
lunettes sur le nez, cartes et plans légèrement visibles. Le fondateur de Wagner
fait son point sur la situation militaire à Barhmouth et déclare « Nous avons
laissé les flancs de la ville à des unités du ministère de la Défense, une
rupture dans sa communication, lui qui assurait encore il y a quelques jours ne
pas avoir vu une quelconque action de l'armée russe dans la ville. » Très tard
cette nuit, c'est un communiqué du ministère de la Défense qui cite le rôle de
Wagner. C'est la deuxième fois mentionnant, tout comme Evgeny Prigogine, la
présence de ce qu'il qualifie de « force d'assaut Wagner » dans le centre de la
ville. C'est au milieu d'un long point du jour sur les combats, mais cette fois,
cette mention s'est faite sans un bras de fer médiatique de plusieurs jours.
Anissa Eljabri, Moscou, RFI. Les Nations unies se disent horrifiées par la vidéo
diffusée sur les réseaux sociaux, montrant des corps mutilés de personnes
présentées comme des prisonniers de guerre ukrainien. Elle semble montrer
également un soldat russe décapité l'un de ses prisonniers. L'Union européenne
prévient qu'elle demandera des comptes aux criminels de guerre en Ukraine.
Evolodymyr Zelenski, lui, condamne ceux qu'il appelle les « monstres russes ».
De son côté, le Kremlin appelle à vérifier d'abord l'origine de la vidéo. « Il
s'agit bien sûr d'images horribles », dit le porte-parole de la présidence
russe. « Mais dans le monde de deepfake dans lequel nous vivons, il faut
s'assurer de l'authenticité de la vidéo. » Nous y reviendrons dans une heure
dans le prochain journal sur RFI. Et sur notre site RFI.fr, vous pouvez
retrouver une enquête exclusive RFI, Le Monde et Lighthouse Reports d'anciens
agents secrets afghans qui ont travaillé avec la DGSE, les renseignements
français, affirment avoir été abandonnés par la France après l'arrivée au
pouvoir des talibans. Deuxième jour de visite d'Emmanuel Macron au Pays-Bas et
deuxième accueil mouvementé pour le président français, hué ce matin devant
l'université d'Amsterdam. Deux personnes ont été interpellées alors qu'elle
reprenait des slogans du mouvement de contestation des Gilets jaunes et contre
la réforme des retraites. Valérie Gasse. « C'est assez rare qu'un déplacement du
président de la République à l'étranger se déroule ainsi. Ce matin, c'est sur le
très calme campus de l'université d'Amsterdam où il est venu pour visiter le
laboratoire de physique quantique, un domaine sur lequel la France et les Pays-
Bas veulent travailler en partenariat, que deux manifestants ont essayé de
s'approcher d'Emmanuel Macron. L'un d'entre eux a tenté de courir vers le
président en chantant le chant des Gilets jaunes. »