TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-03-19-01H18M.MP3
--------------------------------------------------------------------------------
 et ensuite de le lyncher, sauvagement. Cet acte de très grande sauvagerie qui
fait qu'Emmet va être défiguré et atteint dans sa corporelité, c'est une
violence extrême, c'est une violence, dont je dis tout de suite, c'est une
violence terminale en fait. Et donc on va voir évidemment qu'elle est
intériorisée et que dans ce sud profond, cette chose-là pouvait paraître
absolument normale, mais il y a d'autres éléments qui rentrent en ligne de
compte pour expliquer tout à coup cette énorme angoisse qui pouvait conduire à
cela. Donc ils jettent le corps dans l'eau et trois jours plus tard, un jeune
gamin qui faisait de la pêche voit un bout du corps qui réapparaît. Et donc la
première visibilité publique de ce meurtre est ce message qui est envoyé par la
NAACP, je vais le prononcer à la française, la plus grande association de
défense des droits des Africains-Américains, qui décrit au fond ce qui vient de
se passer et qui demande à ce que ce soit rendu public et qu'il y ait des
explications. Les hommes de la NAACP, ils l'ont torturé et il a fait quelques
choses trop mauvaises pour répéter. Il y avait des cris et des sons à
l'intérieur du bar. Il y avait des rires et des sons à l'extérieur de la rue.
Ils ont tiré le corps dans l'eau au milieu d'un rain bleu et rouge. Ils l'ont
emporté dans les eaux blanches pour arrêter la douleur. Dans l'affaire, aimait-
il, le scénario est classique. Des blancs outragés s'en prennent à un jeune noir
accusé d'avoir importuné une blanche. Ce qui l'est moins, c'est que l'affaire
est très tôt médiatisée grâce à la volonté et à l'intelligence de la mère d'Aimé
Thiel. Malgré son incommensurable douleur, Mamie Thiel a la présence d'esprit de
solliciter un photographe de la revue Jet. Il va prendre les clichés de toutes
les étapes du calvaire de Mamie Thiel. On se retrouve à nouveau à Chicago, dans
la gare centrale, le 2 septembre 1955. C'est le moment où la mère d'Aimé doit
aller chercher le cercueil dont les autorités blanches locales ont tout fait
pour qu'il soit entièrement fermé et qu'elle ne puisse pas l'ouvrir. Mais la
première décision qu'elle prend quand le cercueil arrive, c'était une femme qui
était très pieuse. Donc le cercueil arrive, il est fermé, il est même sous-
cellé. Et le Chicago Defender, qui est l'un des plus grands journaux africains-
américains qui va vraiment suivre cette histoire, la presse, la presse écrite,
va jouer un rôle très important. Parce que là, on parle du Defender, mais il y a
plein d'autres petits journaux locaux. Et le rapport entre l'échelle locale et
l'échelle nationale, ou même l'échelle de l'État, l'échelle fédérale, est
constamment aussi quelque chose qui compte dans cette histoire. Parce que la
presse nationale va reprendre, en fait, un certain nombre d'articles qui sont
parus dans la presse locale.... Voici le moment le plus difficile pour la mère
d'Emett. Le corps a été amené dans cette chambre funéraire. Et donc là, elle a
demandé à un photographe d'une revue africaine-américaine qui s'appelle Jet, et
le photographe s'appelle David Jackson, de la prendre en photo dans ce moment-
là.... J'avais commencé à faire son analyse corporelle dans le genre de
détachement qu'un médecin légiste pourrait avoir. Mais je n'étais pas un médecin
légiste. C'est ainsi que Mamie Thiel relate la scène de reconnaissance du corps
de son fils dans ses mémoires. J'étais la mère d'Emett, et j'étais envahie par
l'angoisse d'une mère, tandis que je suivais dans son déroulement la nuit de
torture d'Emett. J'ai regardé ses oreilles, son front, sa bouche, son nez. Je
veux que le monde voit ce qu'ils ont fait à mon petit.... La mère d'Emett décide
d'exposer publiquement le corps de son fils dans son cercueil, et que le
cercueil soit ouvert et vitré, de telle sorte que tous les gens qui viennent
puissent voir le visage défiguré de son fils. Ça se passe dans cette église de
Dieu en Christ, une église évangéliste qui se trouve à Chicago. On n'a pas
exactement le nom, mais on sait que c'est entre 10 000 et 50 000 personnes qui
sont venues, à tel point qu'il a fallu retarder le moment de l'enterrement pour
permettre à toutes ces personnes de venir voir ce corps.... Nous sommes aux
Etats-Unis en 1955, et à l'époque, l'Association Nationale pour la Promotion des
Gens de Couleur, la NAACP, est l'organisation américaine de défense des droits
civiques la plus influente aux Etats-Unis. Sa mission est de gagner l'égalité
des droits politiques, éducatifs, sociaux, économiques, de tous les Noirs, afin
qu'ils deviennent des citoyens à part entière. Et la NAACP arrive à convaincre
Mamie Thiel de porter l'affaire de lynchage de son fils Emmett en justice.... Un
an plus tôt, en 1954, la NAACP avait été reçue par le président Eisenhower, dans
le cadre de la déségrégation des écoles américaines, première loi majeure de la
lutte pour les droits civiques. Mais le président Eisenhower, issu du parti
républicain, était contre l'arrêt de la Cour suprême, qui mettait fin à la
ségrégation raciale dans les écoles publiques. Néanmoins, le président a envoyé
l'armée dans l'Arkansas, pour faire respecter les décisions de justice en faveur
de l'intégration des Noirs dans les écoles.... Le président Eisenhower n'est pas
quelqu'un qui a extrêmement brillé, on va dire, par cette politique-là, mais
malgré tout, il a participé à cette réunion de la NAACP. Et on se trouve donc à
un moment où, la même année 1954, où il participe à cette réunion, la Cour
suprême a l'unanimité, vote la déségrégation, enfin interdit la ségrégation dans
les écoles publiques. Et c'est précisément cet événement-là, qui à l'intérieur
de ce sud très profondément raciste, va créer une poussée de violence, que je
qualifie donc de terminale, mais qui est extrêmement forte. En témoigne, cet
article rédigé, c'est un éditorial rédigé, j'ai évoqué le fait qu'il y avait des
journaux régionaux qui étaient africains-américains, mais il y en a aussi qui
représentent les intérêts des Blancs. Et voici donc ce qu'écrit Frédéric Soulet.
Il se pourrait bien que le sang coule dans plusieurs endroits dans le sud à
cause de cette décision, mais ce sont les marches de marbre blanc du bâtiment de
la Cour suprême qui seront souillées par ce sang. Mettre des enfants Blancs et
Noirs dans les mêmes écoles mènera au métissage, le métissage mènera au mariage
mixte, et les mariages mixtes mèneront à l'abattardissement de la race humaine.
Dans le même temps, John Stenstaker, l'éditeur de l'influent journal africain-
américain, le Chicago Defender, interpelle le président Eisenhower en envoyant
le télégramme suivant à la Maison Blanche. Un enfant de Chicago, Emmett Lutheal,
âgé de 14 ans, a été kidnappé et lynché dans le Mississippi cette semaine.
Pourriez-vous nous dire si votre administration compte effectuer une action pour
cet acte choquant contraire au droit? La réponse est fournie par un assistant du
conseiller spécial du président, donc à un niveau qui n'est pas très très
direct. Et dans cette réponse, il est indiqué que le département de la justice
suit cette affaire de près, mais que pour l'instant il n'y a pas d'éléments qui
permettent de la transférer du niveau de l'État vers le niveau fédéral. À son
tour, Mamie Thiel envoie un message à la Maison Blanche en disant « Je suis la
mère de Emmett Thiel et je plaide pour que justice soit rendue contre toutes les
personnes responsables de ce meurtre bestial, de ce lynchage bestial de mon fils
à Monet. J'attends une réponse directe de vous. » Mais la présidence ne répondra
pas. John Edgar Hoover, le chef du FBI, estime que Mamie Thiel est manipulée par
les communistes. Dans l'affaire Emmett Thiel, le procès est expédié en quelques
jours, du 19 au 23 septembre 1955 à Somner, dans l'État du Mississippi. Avec un
doublement accusé d'enlèvement et de meurtre, les assassins vont jouer au bon
père de famille, leurs jeunes enfants en permanence sur leurs genoux, tandis que
Mme Bryant, épouse de l'un des auteurs de l'lynchage d'Emmett, incarne
parfaitement son rôle de femme blanche outragée. Le jury va délibérer en un
temps record. Dans cette salle du tribunal, on a aussi une illustration à
travers ces photographies de ce qu'est la ségrégation, puisque les Africains-
Américains sont relégués aux deux derniers rangs. Et vous voyez tous les
journaux que j'évoquais tout à l'heure, les journaux régionaux, le Three-State
Defender, St. Louis, Argus, Ebony Magazine et le Jet déjà cité. Une photo volée
qui va avoir une importance très grande dans la mémoire de l'affaire Thiel,
puisque c'est le moment où M. Wright identifie très clairement les deux prévenus
comme étant ceux qui sont venus chez lui kidnapper Emmett Thiel. C'est une photo
volée par l'un des photographes Africains-Américains qui était présent dans le
procès, et qui a donné immédiatement matière à un dessin dans le magazine Time.
Celui qui a un oeuf papillon est un sénateur dans le Michigan, et celui qui est
juste derrière Mme Thiel s'appelle Willie Reed, et il sera l'un des témoins qui
va confirmer qu'il a bien entendu les cris de Emmett Thiel. Miss Mamie Thiel,
âgée de 33 ans, est une femme discrète dont le charme est mis en valeur par un
petit chapeau noir à la voilette relevé et une robe noire à col blanc. Par les
38 degrés qui règnent dans la salle d'audience, elle s'évente avec un éventail
de soie noire frappé d'un motif rouge. C'est ce qu'on peut lire dans le journal
le Daily Walker pendant le procès en septembre. Au risque de sa vie, Mamie Thiel
est descendue dans le Mississippi, elle est dans le prétoire face au meurtrier
de son fils Emmett, et lorsque les deux accusés sont acquittés, elle encaisse.
Il se trouve qu'après avoir nié toute responsabilité dans cette affaire, avoir
tenté de faire croire que le cadavre qui avait été récupéré dans la Tallahatchie
River n'était pas celui d'Emmett Thiel, et évidemment la question pouvait se
poser sur la suite. Évidemment la question pouvait se poser étant donné qu'il
était défiguré. Les deux prévenus libérés au mois d'octobre, donc quelques mois
plus tard, donnent un entretien à la revue Look pour lequel ils se font payer
4000 dollars. C'est énorme à l'époque. Et dans cet entretien, ils reconnaissent
avoir tué Emmett Thiel sans aucun détour, y compris du point de vue du langage.
Et donc Tom va vous dire quelques-uns des segments de ce dialogue qu'ils
rapportent eux-mêmes avoir eu avec Emmett. Je suis mort. Dieu bon. Vous savez,
le roi est mort. Le roi de l'amour est mort. Mais vous m'avez menti tous ces
années. Vous m'avez dit de ne pas me laver les mains et de parler bien, comme
une femme. Et vous avez arrêté de me demander de m'appeler maman et de me dire
ça. Je n'ai rien entendu. Mais mon pays est plein de menthe. Nous allons tous
mourir et mourir comme des oiseaux. Je ne crois plus personne. Je continue de
dire, allez lentement. Vous écoutez RFI, la marche du monde, Valérie Nivlon. Il
était une fois l'Amérique de 1955 où la justice déclare innocent deux hommes
blancs coupables du lynchage d'un jeune homme noir de 14 ans. Crime dont ils se
sont vantés quelques semaines plus tard dans la presse. C'est l'affaire Emmett
Thiel médiatisée grâce au courage de sa mère, grâce à sa conférence de presse,
grâce à sa tournée avec la NAACP, l'Association Nationale pour la Promotion des
Gens de Couleur, une tournée dont les dons en dollars battent tous les records.
1955, c'est aussi l'année où Rosa Parks refuse de laisser sa place à un blanc
dans le bus. Elle a souvent dit qu'elle avait été inspirée par Emmett Thiel ce
jour-là. C'est ce qu'on apprend en tout cas au Musée africain-américain de
Washington, inauguré en 2016. Washington DC, capitale des États-Unis d'Amérique,
où les lettres géantes Black Lives Matter s'écrivent en jaune en ligne de mire
du Capitol. Washington, où le Musée africain-américain revisite l'histoire de la
plus grande démocratie du monde et de ses paradoxes. Vous savez, nous avons
toujours lutté pour une union parfaite. C'est l'une des idées de départ de
l'américanisme et ce concept de comment devenir une société plus égalitaire, où
tout le monde jouit de la liberté autant que de l'égalité. C'est un idéal. Mais
l'histoire américaine a toujours été celle d'une lutte pour atteindre cet idéal.
Et c'est là tout le paradoxe. Je m'appelle Aaron Bryant. Je suis conservateur au
Musée national de l'histoire et de la culture africaine-américaine. Je suis
particulièrement focalisé sur la photographie et sur la façon dont la culture du
passé est représentée visuellement. Depuis la guerre de sécession jusqu'à la
victoire de Barack Obama à la présidence des États-Unis, le Musée africain-
américain de Washington réinscrit le rôle moteur des Noirs dans leur propre
émancipation. Des femmes et des hommes dont la conscience, la pensée et l'action
ont mis l'Amérique face à ses propres contradictions. Mais avec l'affaire Emmett
Till, c'est la première bataille du XXe siècle pour les droits civiques qui
s'écrit. Pour de nombreuses générations, Emmett Till représente un tournant clé.
Pour de nombreuses générations, Emmett Till représente un tournant critique dans
l'histoire politique et sociale américaine, mais aussi dans l'histoire de la
culture africaine-américaine. Emmett Till a marqué le début du mouvement moderne
des droits civiques. Des années 1940 jusqu'aux années 1960, il y a eu de
nombreux mouvements particulièrement liés à la modification des lois
ségrégationnistes par la Cour suprême. Et ça a changé la façon de penser
l'Amérique comme un pays avec deux sociétés séparées. Les gens ont exigé des
changements parce que, vous savez, cette grande idée de liberté appartient à
tous. Et l'affaire Emmett Till a été un tournant critique pour de nombreux
citoyens qui jusque-là ne s'étaient pas encore investis politiquement ou
socialement pour changer la société. L'affaire Emmett Till aurait-elle vu le
jour sans la clairvoyance de sa mère, Mamie Till, sans sa détermination à
exposer à la face du monde la photographie monstrueuse du visage défiguré de son
enfant? Lorsque Jackson a publié des photographies d'Emmett Till dans le
magazine Jet, ça a tout changé. Quand les gens ont pu voir l'impact de la
ségrégation, non seulement comment les gens vivent, mais aussi ce qu'ils vivent,
et puis l'impact sur leurs enfants, alors tout le monde s'est posé la question.
Est-ce que mon enfant sera le prochain? Pour la première fois, chacun a pu voir
des preuves visibles du racisme et des inégalités aux États-Unis. La foule
attend patiemment, parfois plus d'une heure, pour rentrer dans le mémorial
Emmett Till. C'est la seule salle du musée africain-américain où l'on ne peut
pas filmer, où l'on ne peut pas prendre de photos. C'est la seule salle du musée
où l'on vient se recueillir devant le véritable cercueil d'Emmett Till. Je
dirais que dès qu'on rentre dans cet espace, on ressent une énergie
particulière. C'est une expérience très puissante. Les gens se taisent
automatiquement lorsqu'ils entrent dans cet espace, et c'est notamment parce
qu'il a été construit comme une chapelle. C'est comme si le véritable cercueil
d'Emmett Till se retrouvait in situ, et les gens comprennent que ce n'est pas un
simple objet. Ce qui est arrivé à Emmett Till est réel, pas seulement pour lui
et sa famille, mais pour nous tous. Donc je pense que le fait que cet espace ait
été conçu comme une chapelle a une importance majeure. Vous voyez des signes et
des symboles dans l'espace qui exigent rétrospection, méditation, respect. Je
dirais aussi qu'à la sortie de cet espace, nous avons plusieurs exemplaires du
magazine Jet, ouvert à la page de l'article et de la photographie qui avait été
publiée à l'époque. Et ça, vous le voyez quand vous sortez. C'est important
parce que ça montre que ce qu'a subi Emmett Till a eu un impact national. À
travers tout le pays, les gens ont vu et vécu ce chagrin, cette colère, cette
frustration ou ce sentiment de détermination et de militantisme nécessaire pour
commencer à protéger les enfants américains. Il y avait très peu de marques sur
son corps jusqu'à ce que vous arriviez à son cou. Ensuite, il m'a fallu
affronter son visage. J'ai vu son oeil pendre de son orbite jusqu'à la moitié de
sa joue droite, l'avant de son visage et l'arrière de sa tête avait été percée
avec quelque chose comme une hachette. On m'a proposé de maquiller son corps.
J'ai dit non, il faut que les gens voient ce que j'ai vu. Mamie Till a insisté
pour que le monde entier soit témoin de ce qui est arrivé à son fils. Elle
voulait que le monde comprenne que si c'était arrivé à son enfant, cela pouvait
arriver à n'importe quel autre enfant. Elle a insisté pour qu'il y ait une
cérémonie à cercueil ouvert pour exposer le visage meurtri de son fils Emmett
Till. Son visage brutalisé était déformé, battu et enflé. Et elle voulait que le
monde entier en soit témoin. Et donc elle a eu la clairvoyance de documenter ce
moment de sa vie. Tout le monde devait en être témoin. Je pense qu'elle a
compris que ce moment n'avait pas seulement besoin d'être documenté, mais
surtout exposé comme une preuve pour le monde entier et que nous ne pourrions
jamais plus revenir à ce stade. En cela, je dirais qu'elle a eu une clairvoyance
incroyable. En 1955, 60 journaux ont relayé le procès de messieurs Milan et
Bryant, accusés du lynchage d'Emmett Till. 30 photographes et 60 journalistes
ont réussi à susciter un intérêt national et même international pour l'affaire.
Mais la justice n'est pas passée. Les deux accusés ont été déclarés de
violences. Le juge ne s'est même pas saisi du motif d'inculpation pour qu'il
napping alors que les deux assassins avaient reconnu avoir enlevé Emmett. Un
scandale absolu à la hauteur de la colère de l'écrivain africain-américain John
Edgar Weinman, dont l'historien Christian Delage m'a fait découvrir le livre
écrivain de l'époque. Lui était le père d'Emmett. Pour moi, en tant qu'enfant de
14 ans, du même âge qu'Emmett Till, quand j'ai vu cette photo, ce visage mutilé,
ça m'a frappé comme jamais. Une sensation unique, je n'ai jamais oublié. J'étais
un gamin de 14 ans et je n'ai jamais vu un visage mutilé comme celui-ci. J'étais
un gamin de 14 ans et je n'avais jamais vu rien de tel. En fait, je m'en suis
détourné. J'ai détourné aussitôt mon visage de cette horreur. Je n'ai pas pu
étudier la photographie, c'était bien trop horrible. Et ça m'a rappelé ma propre
mortalité, ma propre vulnérabilité. Parce qu'après tout, je n'étais qu'un jeune
homme noir, un jeune garçon noir, exactement comme Emmett Till. John Edgar
Weinman explique dans son livre comment le juge du procès de Emmett Till a été
manipulé par l'administration américaine. Le dossier judiciaire du père d'Emmett
a été sorti de sa confidentialité comme un lapin du chapeau, écrit-il. Le soldat
Louis Till aurait enlevé et violé une femme blanche en Italie pendant la seconde
guerre mondiale. Le tribunal militaire américain l'a déclaré coupable et l'a
pendu, un prêté pour un rendu. Mais revenons à notre conversation téléphonique
avec John Edgar Weinman et Christian Delage. Ma prochaine question portera sur
votre livre et en particulier sur ce dossier judiciaire que vous avez un jour
reçu par La Poste. Vous décrivez l'aspect strictement physique quand vous l'avez
reçu et qui vous paraissait tellement sale que vous vous demandiez dans quelles
circonstances il avait été conservé ou bien si cette saleté correspondait à la
manière dont le père d'Emmett, Louis, avait été traité. Le dossier n'était pas
sale, appropriément parlé, quand je l'ai reçu. Il était métaphysiquement sale.
Parce que j'avais le pressentiment que cela serait une ébauche et non un travail
approfondi et non une tentative honnête de regarder Louis Till. Donc c'était une
sorte de confrontation métaphysique. Le document était en ma possession et
j'avais très envie de le regarder. Mais son apparence était une sorte
d'avertissement, une menace. La couleur même de ce dossier ne me donnait pas
envie de le manipuler et ça m'a fatigué. Effectivement, vous parlez de la
couleur inconvéniente de ce dossier dans votre texte. Et puis vous ajoutez le
fait que cette histoire de Louis est sortie comme un lapin d'un chapeau deux
semaines avant l'ouverture du procès de ceux qui avaient tué Emmett. Il y a eu
deux réels incidents juridiques à propos de la mort d'Emmett Till. Le premier
était le jugement lui-même à Sumner, dans le Mississippi. Le second était qu'à
ce procès, les assassins, Milani et Bryant, les meurtriers, ont été déclarés
innocents après les délibérations du jury. C'était un procès de cinq jours, mais
le jury a délibéré moins d'une heure. Ils ont dit qu'ils avaient pris une pause
déjeuner et c'est ce qui leur avait pris le plus de temps. Ils sont revenus en
s'excusant, mais il leur a fallu une heure pour rendre leur décision de non-
culpabilité de ces deux hommes. Et cette affaire représentait un grand embarque
pour les États-Unis. N'oubliez pas que nous sommes en 1955, en pleine guerre
froide. L'Amérique essaie de dire et de montrer au monde que la démocratie est
la forme d'État idéal, un État démocratique, qui s'oppose à un État communiste.
Voici deux hommes qui manifestement étaient coupables et pourtant, ils ont été
libérés dans le Mississippi. Est-ce que l'Amérique est un État divisé et
ségrégationniste? C'est devenu la question. Ainsi, le deuxième type d'accusation
légale qui découlait de l'accusation des meurtriers d'héméthyles était censé
être un procès pour enlèvement. Et la preuve de cet enlèvement était
incontestable. Les ravisseurs eux-mêmes l'ont admis. Ils l'ont avoué au shérif.
Le shérif était à la barre des témoins et il a déclaré qu'ils avaient admis
avoir kidnappé Héthyl. John, alors effectivement, à partir de là, les soutiens
de Mamie Thiel pouvaient s'attendre à ce que le juge inculpe Bryant et Milan du
kidnapping d'Hémeth. Comment imaginez-vous la scène John Edgar Wildman? Les gens
ont pensé, eh bien accusons ces hommes d'enlèvement. C'est un crime très grave.
Et le procès devait avoir lieu dans une ville voisine du Mississippi. Mais cela
ne s'est pas passé ainsi. Pourquoi cela ne s'est pas passé comme ça? L'une des
raisons pour lesquelles cela ne s'est pas produit est bien à la base le racisme
et l'illégalité qui sévissait dans le Mississippi. L'autre raison est que le
sénateur et l'armée américaine se sont liguées pour balancer l'histoire du père
d'Hémeth Thiel, Louis Thiel, à la presse. Une fois que la presse s'est emparée
de cette histoire et l'a exposée aux yeux du public, l'audition pour enlèvement
a été tout simplement abandonnée. Cela ne s'est jamais produit. Cela n'est
jamais arrivé. Bryant et Milan n'ont même pas été poursuivis en justice pour
l'enlèvement dont tout le monde savait qu'il avait eu lieu et qu'ils avaient
admis avoir commis. Je continuerai par une autre question, John, sur le parcours
que vous avez fait depuis New York jusqu'à ce cimetière où est enterré Louis
Thiel, et la découverte à la fois de la taille de sa tombe, de son anonymat, et
ce que cela vous inspire. Il se trouve, parce que je suis resté très intéressé,
même s'il y avait eu très peu d'indices sur la façon dont il fallait enquêter
sur la vie de Louis Thiel, il se trouve que j'avais trouvé un papier d'une
auteure américaine nommée Alice Kaplan. Elle décrit dans son livre être allé au
cimetière et avoir trouvé la tombe de Louis Thiel. Donc, c'était une inspiration
pour moi de m'y rendre et explorer cette piste. Il m'était impossible de ne pas
me recueillir devant la tombe de Louis Thiel. C'est comme ça que j'ai atterri
là-bas, par intérêt et curiosité. Ce que j'y ai trouvé, j'ai essayé de le rendre
réel pour les gens. Et en rendant le lieu réel, c'est devenu un lieu, une ville
qui existe. C'est devenu un événement concret. Son enterrement s'est concrétisé.
Pourquoi tant de tombes dans ce coin secret? Comment se sont-elles retrouvées
dans une sorte de ghetto derrière le cimetière principal? C'est important de
garder vivante l'histoire de Thiel. Pas seulement celle d'Emmett Thiel, mais
aussi celle de Louis Thiel, car c'est la même histoire. RFI. Il aura fallu
attendre le 29 mars 2022 pour que le président des Etats-Unis, Joe Biden, puisse
enfin se féliciter de l'adoption de la loi fédérale criminalisant le lynchage.
Une loi contre la haine raciale qui porte le nom d'Emmett Thiel. La haine
raciale n'est pas un problème dépassé, c'est un problème persistant. Je sais
bien que les leaders des droits civiques ici présents m'ont entendu le dire au
moins 100 fois. La haine ne s'en va jamais. Elle se cache. Elle se cache sous
les rochers. Mais donnez-lui juste un peu d'oxygène et elle ressort en grondant
et en hurlant. Comment est-il possible que les Etats-Unis aient attendu 2022
pour légiférer au niveau fédéral contre le lynchage? John, qu'est-ce que vous en
pensez? Je ne suis pas surpris par tout ce qui se passe dans mon pays. Je ne
suis pas surpris par tout ce qui se passe dans n'importe quel pays. Je ne suis
ni surpris par ce qu'il se passe dans mon pays, ni par ce qu'il se passe dans
n'importe quel pays. Particulièrement dans mon propre pays. Parce que la vérité
est très, très effrayante, très, très illusoire. La vérité sur les vies humaines
et sur l'existence humaine et sur le fait d'être une personne est très, très
effrayante, très, très illusoire. Et accepter cela est difficile. Alors on
résiste, on ne fait pas les bonnes choses, nous mentons, nous nous cachons et
nous sommes lâches. Oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh,
oh, oh. Regardant mon premier rêve que je connaissais, J'ai demandé pourquoi mes
rêves ne sont jamais réalisés? Est-ce que c'est parce que je suis noir? Ainsi
s'achève notre épisode documentaire de la marche du monde Black Lives Matter,
l'affaire Emmett Till, signée Valérie Nivelon et réalisée par Sophie Jeannin
avec la complicité de David Alias. Un grand merci à Christian Delage, Aaron
Bryant et John Edgar Weinman pour leur généreuse participation. Plus
d'informations sur la page de la marche du monde sur RFI.fr. Retrouvez-nous sur
nos réseaux sociaux Twitter et Facebook. Réagissez, podcastez cette émission,
c'est la vôtre. En Irak, 5 ans après la chute de Daesh et 20 ans après
l'invasion américaine, dans quelle situation se trouvent ceux qu'on appelle les
chrétiens orientaux? Comment se reconstruisent les édifices et les liens entre
les différentes communautés religieuses? Nous faisons le point avec Mgr Pascal
Gollnisch, directeur de l'Oeuvre d'Orient. Religion du monde avec Véronique
Guémard. Tout à l'heure à partir de 9h10 TU. RFI. Session Lab, un podcast
musical réalisé par RFI Labo avec Hortense Voll. Du 23 au 25 mars, Session Lab
est à Marseille pour le Babel Musique XP, l'événement français consacré aux
musiques actuelles du monde. Du Liban à l'Indonésie en passant par le Maroc et
le Québec, on compte bien y faire des rencontres d'anthologie. Un podcast
musical en audio 3D à écouter au casque sur RFI Musique et sur toutes les
plateformes.