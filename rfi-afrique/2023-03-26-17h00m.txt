TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-03-26-17H00M.MP3
--------------------------------------------------------------------------------
 Bonjour et bienvenue, 15h, TU17h ici à Paris, le journal sur RFI. Dans
l'actualité, le projet de la Russie de stationner en Biélorussie des armes
nucléaires tactiques de réaction, l'Ukraine réclame une réunion urgente du
Conseil de sécurité de l'ONU et l'OTAN qualifie de dangereuse et irresponsable
la rhétorique nucléaire de la Russie. La Chine et le Honduras ont décidé de
nouer des relations diplomatiques. Le Honduras reconnaît Pékin et ron avec
Taïwan, Taïwan dont les soutiens deviennent encore moins nombreux à suivre dans
un instant. La France se prépare à une nouvelle semaine de mobilisation contre
la réforme des retraites. Avant une dixième journée d'action mardi, le président
Macron reçoit demain la Premier ministre Elisabeth Borne et les cadres de la
majorité. La liste des soutiens internationaux de Taïwan perd un nouveau membre
puisque la Chine et le Honduras ont décidé d'établir des relations
diplomatiques. Le Honduras reconnaît Pékin et ron avec Taïwan, un revers pour
Taipei qui n'est plus reconnu désormais que par 13 États dans le monde. A
l'inverse, on peut parler de nouveaux succès pour le régime communiste. Il
poursuit depuis des années une stratégie d'isolement diplomatique de l'île
nationaliste Xi Fan Liu. Pékin a déjà envoyé une invitation officielle pour une
visite d'État à la présidente ondurienne. La Chine, par la voix de son ministre
des affaires étrangères Tsinghan, a également lancé un avertissement au
gouvernement taiwanais et à leur promotion du séparatisme voué à l'impasse. La
présidente taiwanais Tsai Wen a de son côté dénoncé les coercitions et
intimidations ainsi que les promesses financières de la Chine depuis l'arrivée
au pouvoir en 2016 de cette dirigeante plutôt favorable à l'indépendance de
l'île. Le Honduras est le 9ème pays à tourner le dos à Taipei. C'est le résultat
d'une politique agressive de Pékin qui, outre les pressions économiques et
militaires, vise à isoler sur le plan diplomatique Taiwan qu'elle considère
comme une province rebelle. Cette annonce intervient alors que la présidente
taiwanais doit entamer une tournée en Amérique Centrale mercredi. Elle doit
visiter deux pays encore alliés, le Belize et le Guatemala, avant un arrêt
probable aux États-Unis. Une tournée déterminante pour la diplomatie taiwanais
plus que jamais isolée. C'était Ziphan Liu. La Russie accusée par l'Ukraine
d'avoir pris la Biélorussie comme otage nucléaire, accusation portée à Kiev par
le secrétaire ukrainien du Conseil de sécurité. Selon lui, l'annonce par le
président Poutine d'un déploiement d'armes nucléaires tactiques en Biélorussie
est un pas vers la déstabilisation du pays. L'Ukraine réclame une réunion
urgente du Conseil de sécurité de l'ONU. Autre réaction, celle de l'OTAN. La
rhétorique nucléaire de la Russie est dangereuse et irresponsable, déclare la
porte-parole de l'Alliance Atlantique. Le chancelier Olaf Scholz réunit ce soir
sa coalition pour tenter d'apaiser des tensions croissantes entre partenaires de
la coalition en Allemagne. Autour de la table, les libéraux du FDP, les
écologistes et les socio-démocrates. Leur désaccord porte aussi bien sur le
climat que sur le financement de l'armée et le budget 2024. La France, elle, se
prépare à une nouvelle semaine de mobilisation comprenant grève et
manifestations contre la réforme des retraites. Elle culminera mardi avec une
nouvelle journée d'action à l'échelle nationale. Le président Macron reçoit
demain la Premier ministre Elisabeth Borne et les cadres de sa majorité avec en
toile de fond le climat social délétère. Comme c'était à craindre la
manifestation contre le projet de Grand Bassin de rétention d'eau dans l'ouest
de la France. Cette manifestation a tourné hier à la bataille rangée entre
forces de l'ordre et manifestants. Il y a des blessés dans les deux camps. Le
pronostic vital est engagé pour l'un des manifestants blessés. Direction
maintenant l'Irak où les exportations de pétrole des champs situés dans le nord
vers la Turquie ont été interrompues. C'est la conséquence d'une décision rendue
par la Chambre de commerce internationale rendue au bénéfice de Bagdad. Cette
décision pourrait entamer les relations déjà fragiles entre le gouvernement de
la région autonome du Kurdistan irakien et le gouvernement fédéral de Bagdad.
Explication sur place de Marie-Charlotte Rouppi. C'est un conflit entre le
Kurdistan irakien et l'état fédéral au cœur duquel s'est retrouvé la Turquie.
Après une plainte déposée en 2014 par Bagdad contre Ankara, la Chambre de
commerce internationale a rendu sa décision. La Turquie viole un accord entre
les deux pays en important directement l'or noir des champs pétroliers du
Kurdistan sans autorisation du gouvernement fédéral. Ce samedi, les exportations
ont donc été interrompues et Ankara a annoncé se plier à cette décision. Elle
paiera une compensation d'un milliard et demi de dollars à l'Irak. Une
délégation irakienne se rendra prochainement en Turquie pour tenter de reprendre
les échanges. Reste que cette décision n'est pas sans conséquence pour le
Kurdistan irakien. L'économie de la région autonome dépend principalement de ses
revenus pétroliers, si bien que l'arrêt prolongé de ses exportations risque
d'avoir un impact non négligeable sur ses ressources financières et sur le long
terme c'est une question de souveraineté sur les réserves de pétrole qui se
posent. Cette décision met le Kurdistan dans une mauvaise posture pour négocier
avec l'état fédéral. Une délégation kurde s'est rendue à Bagdad ce dimanche.
Marie-Charles Troupi, Bagdad RFI. A Cuba, les électeurs sont appelés à voter
pour renouveler le parlement des élections en trompe-l'œil. Dans ce pays où le
parti communiste est solidement installé au pouvoir, 470 candidats se présentent
pour 470 sièges. Les résultats sont connus d'avance mais dans un pays aussi où
le vote n'est pas obligatoire. Il faudra donc surveiller le taux de
participation. Nouveau drame de l'immigration en mer méditerranée. Au moins 19
migrants sont morts dans le naufrage de leur embarcation au large de la Tunisie
alors qu'ils tentaient de gagner l'Italie dans ce contexte de pression
migratoire. La France et l'Italie demandent qu'un accord soit trouvé avec le
Fonds monétaire international pour soutenir l'économie tunisienne, le
commissaire européen à l'économie sera demain lundi à Tunis. Ce soir prend fin
le CIDACTION, opération conçue pour soutenir la recherche et aider les personnes
malades du SIDA. Certes des progrès très importants étaient faits depuis les
années 80. Les malades peuvent vivre avec le VIH qui doivent prendre en continu
et sans guérir car le virus n'est pas éliminé de l'organisme. L'obstacle est ce
qu'on appelle les réservoirs viraux et c'est un axe de recherche majeur Valérie
Cohen. Les médicaments d'irrétroviraux actuels permettent de vivre avec le VIH,
de le rendre indétectable dans le sang mais pas de l'éradiquer. À l'arrêt du
traitement, le virus se multiplie. En cause, les réservoirs viraux. Le virus
reste tapis dans certaines cellules immunitaires. Olivier Lambotte est chercheur
et professeur d'immunologie à l'Université Paris-Saclay. Les réservoirs du virus
c'est globalement certains types de globules blancs dont des lymphocytes T4 qui
sont la cible du virus mais une sous-population dans laquelle le virus reste
endormi, complètement silencieux en fait. Et donc ça il va persister toute la
vie durant dans ces cellules. Actuellement il n'y a pas de traitement permettant
d'éliminer ou contrôler les réservoirs mais les chercheurs explorent plusieurs
pistes. Le premier axe c'est d'aller cibler la cellule réservoir elle-même.
C'est-à-dire essayer de trouver des moyens de l'identifier, de la tuer,
d'empêcher que le virus s'en sorte. C'est cibler les cellules réservoirs elles-
mêmes de manière qu'avec la chino-thérapie vous allez cibler la cellule
cancéreuse. Il y a une deuxième stratégie qui est de cibler votre propre système
immunitaire pour que ce soit lui qui fasse le travail et soit boosté, soit
optimisé et ce soit lui qui contrôle le réservoir. L'enjeu de la recherche sur
les réservoirs, permettre aux personnes infectées par le VIH d'arrêter les
traitements antirétroviraux si ce n'est pour toujours au moins pendant une
longue période sans que le virus se multiplie. Valérie Cohen, aux Etats-Unis,
l'état du Mississippi est encore sous le choc des violentes tornades qui ont
déferlé alors que de nouvelles intempéries sont attendues ce dimanche. Le bilan
provisoire s'élève à 25 morts et des dizaines de blessés. Israël a connu une
nouvelle soirée de manifestation contre la réforme du système judiciaire. Le
ministre de la Défense fait une proposition pour engager le dialogue. Il propose
de suspendre cette réforme pendant un mois. Musique pour finir, en compagnie du
bluesman malien Boubacar Traoré en concert ce soir à Paris dans la salle du New
Morning. Le chanteur et musicien sort un nouvel album qu'il a enregistré dans
son pays natal et de Montsadaka. Sous son éternel casquette, Boubacar Traoré est
donc à 80 ans passé un vétéran du blues mandingue. Son destin, lui, se confond
avec l'histoire de son pays. Il avait inventé dans les années 60 ce que l'on a
appelé alors le « twist malien » multipliant les succès au lendemain de
l'indépendance. Après le coup d'état de 1968, il était tombé en disgrâce et
avait changé de vie avant d'être sorti de l'ombre au tournant des années 90
grâce à un producteur britannique. Sur la scène du New Morning dimanche, le
chanteur et guitariste présentera les titres de son dernier né, Tchékoro
Badiougou, un disque 100% malien, de 9 titres enregistrés aux côtés
d'instrumentistes du terroir tous rencontrés à Segou. Boubacar Traoré. Je suis
parti à Segou, au festival, j'ai vu le jeune musicien, j'ai dit qu'on va faire
une série ensemble, ils ont fait tout pour que je fasse une série avec eux.
Celui que ses admirateurs surnomment affectueusement « Carcar » a développé très
jeune et en autodidacte son propre style. Il s'inspire de la Chora, c'est
l'instrument traditionnel d'Afrique de l'Ouest. Boubacar Traoré parvient et
c'est ce qui fait sa singularité à jouer à 6 cordes les sons des 21 cordes de la
Chora. Boubacar Traoré en concert ce soir à Paris au New Morning. Très bonne
journée sur RFI. Le 27 février dernier, Ismaël Touré, un des piliers du groupe
Touré Kunda, disparaissait à l'âge de 73 ans. Fervent partisan d'un
multiculturalisme musical universel, il s'exprimait souvent sur nos ondes pour
défendre cette ouverture d'esprit à laquelle il tenait temps. Nous lui rendons
hommage en réécoutant un entretien qu'il nous avait accordé avec son frère Sirou
en 2008. L'épopée des musiques noires Joe Farmer. Tout à l'heure, à partir de
16h30 TU. RFI Bonjour, une fois n'est pas coutume, nous allons parler musique,
musique dite classique dans ce nouveau numéro du magazine ID. Parce que les
musiciens, les compositeurs ont aussi une vision du monde, une pensée sur le
monde. C'est le cas assurément de celui que nous allons évoquer avec notre
invité Laetitia Le Gay qui vient d'écrire une biographie de Béla Bartok. Bartok
qui écrivait ceci en 1931. « Ma véritable idée directrice, celle dont je suis
pleinement conscient depuis que j'ai la vocation de compositeur, c'est la
fraternisation des peuples, leur fraternisation malgré toutes les guerres et
tous les conflits. Grand compositeur du XXe siècle contemporain de Stefan Zweig
dont nous avons également parlé dans cette émission, Béla Bartok dont la musique
d'avant-garde est aussi nourrie de musique populaire, l'œuvre de Béla Bartok est
une synthèse inédite entre répertoire rural et musique savante, note Laetitia Le
Gay. Nous avons presque une heure pour en parler. Voici donc un nouveau numéro
du magazine ID, un magazine que vous pouvez écouter quand vous le souhaitez sur
le site de la radio et votre plateforme numérique préférée. »... Bonjour
Laetitia Leguerre. Bonjour. Merci d'être à ce micro. Vous êtes maîtresse de
conférence à Sergi Paris, universitaire, chercheuse, musicologue, spécialiste de
la musique. Vous êtes l'auteur de documentaires sur France Culture et France
Musique également. Et vous avez donc écrit cette biographie de Béla Bartok aux
éditions Actes Sud. J'ajoute que vous avez écrit dans la même collection, chez
Actes Sud, dans la même collection de musique, un Serge Prokofiev. C'était en
2012, La musique est votre affaire. Oui, puis il y a une parenté entre Prokofiev
et Bartok qui sont contemporains, qui fréquentent les mêmes milieux musicaux et
qui, dans leur démarrage, se posent les mêmes questions de renouvellement du
langage musical. Alors je parlais de Béla Bartok comme penseur de son époque.
C'est un homme qui était de plein pied dans son époque, qui s'intéressait à son
époque. Il était de plein pied dans son époque, il s'est beaucoup impliqué
politiquement, non pas par de l'action politique, mais par des prises de
position très claires, notamment au moment de la montée du nazisme qu'il a
dénoncé. Tout au long de son parcours, il a exprimé très clairement ses
positions. Il a certes passé une partie de sa vie assez sur son travail, presque
un peu isolé, mais toujours en défendant ses idées démocratiques, humanistes.
Alors je vais commencer par le commencement. C'est un musicien qui est né à la
fin du 19e siècle et qui est mort au milieu du 20e pour être précis, 1881-1945.
Donc il est à cheval sur deux siècles différents. Alors en fait, deux siècles,
on peut dire qu'il y a une première période qui va jusqu'à la première guerre
mondiale, qui marque vraiment une rupture, un tournant dans l'histoire
européenne. Et puis une deuxième phase qui est l'entre-deux-guerres avec toutes
les problématiques justement de la montée du nazisme, du fascisme, le régime
politique en Hongrie. Donc tout va être différent, d'autant plus que le grand
événement en fait pour la Hongrie, enfin événement néfaste du point de vue
hongrois, c'est le traité de Versailles en 1920 qui va redéfinir complètement
les frontières et qui donc va séparer des Hongrois, qui va, pour Bartok en tout
cas, le priver de ses chers collectes parce que c'était ça le nerf de sa musique
et de sa vie. Alors nous allons évidemment en reparler, en précisant, vous venez
de y faire allusion, Laetitia Leguet, que c'est l'homme d'un pays, la Hongrie,
c'est très important. Alors il est très attaché à son identité hongroise, oui.
Il commence en fait par être presque un peu chauvin. Voire nationaliste, non?
C'est surtout, en fait, il faut poncer cette question d'identité hongroise par
rapport à l'empire austro-hongrois et donc les Hongrois, en dépit du compromis
de 1867, qui avait fait de la Hongrie un royaume qui était un peu privilégié par
rapport aux autres régions de l'empire, donc en dépit de ce compromis, il y a
une partie de le pays hongroise qui voudrait l'autonomie de la Hongrie et Bartok
est proche de cette revendication, sinon de l'autonomie hongroise, en tout cas
d'une identité hongroise importante. Et cette question n'est pas séparable de la
question des langages artistiques puisque tout le parcours de Bartok dans la
recherche de son langage personnel, ça va être la recherche d'un langage qui
soit d'avant-garde et qui soit hongroise, c'est-à-dire qui soit à la fois du
niveau des autres avant-garde, qui soit de portée universelle et qui en même
temps soit enraciné dans l'identité hongroise. Laetitia Le Guesne, il faut déjà
aussi souligner que Véla Bartok est un surdoué. Il est remarqué d'abord pour ses
talents au piano, il donne ses premiers concerts vers l'âge de 10-11 ans, il va
faire des études musicales très poussées, il a un début de vie un peu compliqué
sur le plan familial puisque son père meurt quand il n'a que 7 ans, la famille
va être un peu ballotée puisque la mère est institutrice et donc elle va avoir
des postes provisoires et la famille va un peu bouger dans différentes villes de
l'Empire, certaines qui sont aujourd'hui en Ukraine, d'autres en Slovaquie et
finalement la mère de Bartok qui est très attentive au talent musical de son
fils va obtenir un poste fixe à la ville qui s'appelait Pogoni à ce moment-là,
donc Bratislava aujourd'hui, où il y a eu un très bon niveau musical et où
Bartok va faire de très bonnes études musicales avec un certain Erkel, certain
Laszlo Erkel qui est le fils du Erkel qui a fondé vraiment la vie musicale
hongroise avec Franz Liszt à Budapest et donc il est déjà dans une très bonne
lignée et puis rapidement il va entrer au conservatoire à l'Académie royale de
musique de Budapest, il fait le choix par attachement à la Hongrie de ne pas
aller au conservatoire de Vienne qui est pourtant la grande école de musique de
l'Empire. Pourquoi de raison? Parce que Vienne c'est la ville de Brahms, c'est
là où sont formés la plupart des professeurs qui sont ensuite professeurs à
Budapest, c'est la capitale de l'Empire. Donc Vienne, grande ville musicale,
mais Bartok préfère Budapest et il choisit Budapest en suivant aussi l'exemple
de quelqu'un qui va être important dans sa vie qui est Dornani qui est un
pianiste, compositeur, chef d'orchestre qui a quelques années de plus que lui
qui est le fils d'un professeur de mathématiques qu'il a eu au lycée et ils se
sont liés d'amitié et Dornani a choisi lui aussi dans cet esprit d'identité
hongroise le conservatoire de Budapest, qui s'appelait alors l'Académie royale,
et le recommande à Bartok qui donc va y faire sa première année en 1899-1900 qui
est une époque extraordinaire pour la ville de Budapest. Alors ce qu'il faut
retenir, Laetitia Leguet, c'est au coeur de votre ouvrage, c'est qu'il y a en
fait deux Bellas Bartok, je vous cite, « longtemps Bartok a été scindé en deux
images, le musicien nourri de folklore, d'une part celui des suites de danse, on
va en écouter dans un instant, ou danse romaine, transcrite à l'envie, le
novateur de l'autre au langage exigeant, voire aride. » Elle existe encore cette
division en deux Bartok, où l'œuvre est un tout? Alors l'œuvre est un tout, mais
peut-être que dans la réception, certains publics sont plus sensibles à des
œuvres plus marquées par le langage folklorique. En fait ce qui se passe c'est
que Bartok a bien expliqué que sa musique se nourrit des musiques paysannes
hongroises, on va y revenir, mais il y a différents degrés d'utilisation dans
son œuvre de cette musique, il peut en faire des arrangements, il peut en
utiliser des éléments comme motif, ou il peut, ce qui est le stade le plus
élaboré, en intégrer complètement des caractéristiques d'écriture, des
structures harmoniques, rythmiques, des timbres, des modes de jeu aussi
villageois, dans son œuvre. À ce moment-là, l'héritage folklorique est moins
immédiatement peut-être perceptible, mais il est toujours là, il lui donne
toujours sa sève, son intensité, et cette caractéristique de la musique de
Bartok qui est d'être entre une certaine apreté, une mélopée parfois, et puis
l'affirmation de la vie dans la danse. Dans quelles conditions Laetitia Luguet
a-t-il rencontré cette musique populaire qui a été quasiment une révélation, un
coup de foudre presque? Pendant ces années de conservatoire, il a surtout été
formé, c'est assez paradoxal pour quelqu'un qui a revendiqué l'identité
hongroise, mais il a été formé à l'école germanique en fait, c'est-à-dire les
grands, Bach, Beethoven, Brahms. Il a découvert pendant ces années à Budapest
Richard Strauss, Liszt, et puis il y a eu une période, il hésitait un peu, il ne
savait pas trop dans quelle direction aller, et en 1904, peu après la fin de ses
études à l'Académie Royale de Budapest, il s'isole dans la campagne pour aller
réviser ses programmes de concert de l'année suivante, et là il entend une jeune
servante qui s'appelle Lydie Dosa, qui est restée dans l'histoire de la musique
pour cet épisode, il l'entend chanter des chants paysans anciens, et il est
ébloui par ce chant qui lui semble tout à fait inouï justement dans l'écriture
musicale. Et là je vous cite Laetitia Le Gay, il fait l'expérience alors d'une
étrangeté sonore à la fois enchantement de l'inédit et entrevue de ressources
modales, rythmiques et mélodiennes soupçonnées qui lui ouvrent les portes d'un
nouveau langage. Oui, ça va être le début de sa quête, et il va à partir de là,
n'avoir de cesse de se nourrir dans l'écriture, dans l'inspiration, dans
l'imaginaire, de ce répertoire, parce que ce n'est pas que l'écriture, c'est
aussi un monde poétique, les chants populaires racontent des histoires, ils
campent des scènes, donc il va se nourrir de tout ça, et pour s'en nourrir il va
partir sur les chemins, collecter ce matériau, et ça il va le faire à partir de
1906. Lors de véritables expéditions? Véritables expéditions, où il part avec
son phonographe qui joue un rôle très important. Et alors il faut bien toujours
pour Bartok tenir les deux côtés des reines, c'est-à-dire qu'il y a le populaire
et en même temps il y a la musique de son temps. Et comme il s'était intéressé à
Liszt, Richard Strauss, Wagner, donc à partir des années 1906, 1907, etc., il va
avoir aussi en même temps que la révélation de la musique populaire des
révélations de musique de son temps, c'est-à-dire d'abord Debussy qu'il va
découvrir par son ami, le compositeur Zoltan Koday, mais aussi Schönberg,
Stravinsky bien sûr, et en fait il va faire une synthèse extraordinaire de tout
ça, d'où va naître son style si spécifique. Et il ne va pas arrêter de s'occuper
de cela pendant toute sa vie en fait, ça, à lui dire, non? L'ethnomusicologie,
on appellerait ça ethnomusicologie aujourd'hui, va être la passion de toute sa
vie. Première phase, il va aller sur le terrain, et puis arrive la Première
Guerre mondiale qui va finalement mettre fin justement avec Trianon, la
redéfinition des frontières, aux possibilités. Et la crise économique aussi, les
difficultés économiques de l'après-guerre, Bartok n'aura plus les moyens de
financer ses collectes, puisque la plupart de ses collectes il les finançait
lui-même, il doit renoncer et il va se passer en fait toute une deuxième phase
de sa vie qui va être une période un peu d'intériorisation de ce patrimoine
qu'il a collecté, qu'il a collecté aussi par souci de le préserver, parce qu'il
voyait bien qu'avec l'industrialisation, ses mélodies, ses chanses, ses danses
qui étaient préservées parce que les campagnes étaient éloignées, des moyens de
communication, tout ça risquait de disparaître. Il y a un travail de
préservation et donc de collègues dans un premier temps, de vie dans les
villages qui le rend très heureux et dans la deuxième partie principale de sa
vie, donc dans l'entre-deux-guerres, il va travailler sur ce matériau, le mettre
par écrit, faire des ouvrages et puis s'en inspirer pour sa musique. Je vous
propose d'écouter un extrait de ces danses populaires avec un morceau qui
s'appelle la danse du bâton, d'abord interprétée par Rémi de Langley et Vasiléna
Zerafimova. Ce sont des morceaux à chaque fois très courts, la glissade gué.
Oui, c'est une série de petites pièces qui reprennent des danses de villages,
donc ce sont des danses populaires de Roumanie parce que Bartok peu à peu a
élargi sa zone de collecte. Il a commencé par la Hongrie, la Slovaquie, la
Transylvanie Hongroise et puis donc en 1909, il a découvert la Roumanie qui ne
faisait pas partie de l'Empire et ses musiques spécifiques. Elle a eu un coup de
foudre pour les musiques roumaines et s'en est inspirée pour beaucoup de pièces
musicales. Il faut dire aussi que cette passion pour la musique roumaine a
débouché sur un travail d'ethnomusicologie très important qu'il a terminé en
exil à New York peu avant de mourir. C'était vraiment l'histoire de sa vie. J'ai
choisi, je vous l'avais suggéré cette version pour clarinette et marimba qui
n'est pas une version courante parce que le propre de ces musiques c'est aussi
de pouvoir être transposées, ce sont des musiques vivantes et là donc les deux
interprètes qu'on a entendu se sont appropriés de cette musique que Bartok a
composé d'abord pour le piano mais qu'il a ensuite lui-même orchestré. Écoutons
une troisième danse, un troisième morceau. Suite de ce numéro consacré à Béla
Bartok, pour une fois nous parlons de musique, de musique dite classique dans ce
magazine. Notre invité Laetitia Leguet vient d'écrire une biographie, une étude
de l'œuvre de Béla Bartok aux éditions Actes Sud. C'est un autre morceau très
différent, celui que vous avez choisi, la troisième danse. Celle-ci est beaucoup
plus mélancolique, on a une sorte de mélopée caractéristique de l'Europe
centrale et cette nostalgie a un nom particulier en Roumanie, ça s'appelle la
Doina et je trouve que ce morceau est très évocateur de cet aspect-là de la
musique d'Europe centrale et en particulier donc de Roumanie mais aussi du reste
de l'Empire. Béla Bartok qui aimait lire Nietzsche, apparemment vous le citez
dans votre livre Laetitia Leguet, dans une de ses lettres envoyer un camarade
d'enfance, il commente un aphorisme trop humain, je le cite, « Chacun doit
lutter pour s'élever au dessus de tous, rien ne doit l'atteindre, il doit être
complètement indépendant, complètement indifférent, ainsi seulement l'homme peut
se réconcilier avec la mort et l'insignifiance de la vie ». On peut dire qu'il
était un solitaire dans son genre, non? C'était un solitaire mais c'était aussi
un homme d'amitié et pendant l'avant première guerre mondiale il a participé à
des manifestations de la grande revue d'avant-garde qui s'appelait Newgate à
l'époque, qui signifie Occident, et il y avait des expositions organisées par le
groupe de Newgate et des concerts associés à ces expositions et Bartok y a pris
part. Je pense que c'était un solitaire particulièrement à certaines périodes de
sa vie où il était furieux de la mauvaise réception qu'il rencontrait en Gris.
Sa carrière musicale a été une sorte d'alternance de phase de retrait, de repli,
ou au contraire où il allait volontiers se produire sur scène. Mais par
ailleurs, c'était un homme d'amitié, il a eu des amitiés très importantes avec
certains interprètes, avec le compositeur Zoltan Koday et son épouse Emma
Gruber. Donc ce n'était pas non plus un misanthrope. Et puis, le temps passant,
Bartok n'arrêtait pas de travailler, il avait ses propres compositions, son
travail ethnomusicologique qu'il venait amener à bien, et donc il était pris par
son travail, puis s'est tourné pour gagner sa vie. Donc la vie est obligée à un
certain repli. Il faut préciser, Laetitia Luguec, et vous l'écrivez dans ce
livre, que l'œuvre de Bartok prend son envol à une période d'exploration
fervente des langages et des formes, écrivez-vous, des mouvements foisonnes?
Symbolisme, post-impressionnisme, futurisme, cubisme, expression naissance, et
dans ce contexte qu'il crée? Oui, c'est dans le contexte des années. Il lui-même
considère que son œuvre un peu décisive dans la naissance de son langage, ce
sont les Bagatelles, donc il écrit en 1908, 1908-1909, et ces œuvres en fait
marquent l'avènement de quelque chose de nouveau dans sa composition. Ce n'est
pas très loin des grandes œuvres, des grands premiers ballets de Stravinsky,
L'Oiseau de Feu, bientôt on va voir Petrushka, on va voir Le Sacre du Printemps,
donc c'est contemporain, chacun, c'est contemporain aussi de Bussi, Stravinsky
de Bussi, Bartok sont des grandes figures du renouvellement du langage musical,
un moment où tout le monde s'interroge sur que faire après Wagner, après le
post-romantisme, et comment trouver une musique qui ne soit pas dans l'outrance,
sinon dans l'outrance, dans une forme d'expression trop généreuse. Vous avez
également souhaité vous arrêter dans cette émission sur une des œuvres de Bartok
qui s'appelle «A l'egro barbaro», pour quelle raison? On va l'écouter dans un
instant. Cette œuvre est importante parce que, enfin, plusieurs écarts. La
première, c'est une œuvre où le piano est traité de manière percussive, et le
traitement du piano de manière percussive que l'on a aussi chez le jeune
Prokofiev, pas que le jeune, d'ailleurs aussi dans ses sonates de guerre à la
fin, chez Prokofiev que l'on a chez Stravinsky, c'est une caractéristique de
Bartok qu'il va exploiter tout particulièrement dans ses concertos pour piano,
notamment le premier et le deuxième, avec aussi un attachement au piano
percussif qui va le pousser à un travail sur les liens entre piano et
percussion, ce qui est assez particulier avec des effets de timbre absolument
merveilleux, assez magiques dans les concertos pour piano. On va voir par
exemple des dialogues du piano avec la timbale, ce qui est d'une grande poésie
musicale et qui avait été quasiment jamais fait avant lui. Donc le piano
percussif, ça c'est très important. Cette pièce aussi est importante parce que
le choix du titre révèle l'humour de Bartok. Bartok on le voit parfois comme un
être sévère, rigoureux, un peu strict, ce qu'il n'était qu'en apparence. Tous
les gens qui l'ont vu disaient qu'au premier abord on avait l'impression de voir
un professeur très strict. Tout de suite après ce qui était frappant c'était la
force de son regard, ses yeux très lumineux, très intenses et puis donc ce côté
à la fois raffiné, cultivé. C'est un homme de la mythole europa Bartok et son
humour. Et ce goût de l'humour on l'a dans le titre parce qu'Alegros Barbaro en
fait est un clin d'œil à un article qui avait été écrit à Paris des journalistes
qui avaient qualifié Bartok et Kodály de jeunes barbares hongrois. Et donc par
clin d'œil à cet article Bartok a choisi ce titre. Alors il faut aussi dire que
cet Alegros Barbaro a fait beaucoup d'effets et a été brandi un peu comme un
manifeste par l'avant-garde hongroise de l'époque. Et dernière chose qu'il faut
ajouter à propos de cet Alegros Barbaro c'est que cette œuvre est tellement
importante qu'elle a donné son nom à une grande exposition qui a eu lieu au
musée d'Orsay il y a quelques années qui s'appelait Alegros Barbaro Bartok et
l'avant-garde hongroise et qui proposait parmi les trésors qu'il y avait dans
cette exposition donc les fauves hongrois, des tas de photos aussi de Bartok en
collecte ou les meubles qu'il avait fait faire par des artisans de Transylvanie
chez lui. Donc parmi tous ces trésors il y avait un petit film qui montrait
Bartok en train de jouer lui-même son Alegros Barbaro. Je vous propose d'écouter
une interprétation d'Alegros Barbaro jouée par Sultan Coxis. Vous avez voulu
cette interprétation. La voici. La voici. La voici. La voici. La voici. La
voici. La voici. Alegros Barbaro de Bela Bartok choisie par notre invité
Laetitia Leguet grâce à laquelle nous parlons de ce grand musicien dans ce
numéro d'idées consacré à sa musique. C'est une première et j'en suis ravi. On
note la puissance effectivement du jeu de piano. Alors la puissance et en même
temps Bartok n'avait pas un jeu dur. C'est ce qu'on disait tous les gens qui
l'ont entendu. C'était un jeu qui était dans l'énergie mais qui était aussi dans
les couleurs du piano. Dans la tradition du piano du 19e siècle il a été formé
au piano par un certain Istvan Thoman qui était lui-même élève de Franz List.
Donc il était héritier direct de cette école de piano. Et l'un de ses élèves je
crois disait Bartok est un compositeur du 20e mais un pianiste du 19e. Voilà
donc l'énergie, la couleur mais pas la dureté. Alors il y a une autre œuvre. Il
y en a beaucoup. L'œuvre de Bela Bartok est immense. Elle est immense. Elle
compte des dizaines et des dizaines de créations. Il y a le concerto pour piano
numéro 3 qui est le dernier, le dernier œuvre. Il manque quelques mesures mais
une des toutes dernières partitions presque achevées porte le numéro BB puisque
le catalogue de Bartok est désigné par ses lettres BB 127. Donc ça veut dire au
moins 127 partitions plus toutes les œuvres de jeunesse que Bartok n'a pas gardé
dans son catalogue. Avant 1904. Avant 1904. Laetitia Le Gael, il y a un autre
morceau qui a été créé en 1911 également comme mal les gros Barbaros c'est le
Château de Barbebleu c'est ça en tout cas fini terminé non? Il est composé.
Bartok l'a repris, il a modifié par la suite mais il l'a terminé en 1911. C'est
l'une de ses trois œuvres scéniques. Il n'a fait que trois œuvres scéniques. Un
opéra, un ballet et une pantomime. Le Château de Barbebleu, le ballet c'est le
Prince de bois qu'on ne connaît pas assez qui est une œuvre vraiment très
intéressante, très belle. Et puis donc Le Vendard un merveilleux qui est une
pantomime expressionniste qui s'inscrit dans la mouvance expressionniste de
l'époque. Le Château de Barbebleu lui, singulièrement est un opéra très
symboliste. Et la reprise d'un conte célèbre. C'est la reprise d'un conte mais
c'est surtout le croisement de deux choses. Du théâtre de Mitterlinck et de
légendes assez horribles d'Europe centrale, l'histoire du maçon qu'elle émène,
c'est à dire d'un homme qui construisait un édifice. L'édifice s'écroulait sans
cesse et pour le faire tenir il a en murée vivante son épouse dans la
construction et c'est ainsi que la construction a pu tenir. Donc on a un mélange
de légendes folkloriques un peu comme dans la démarche générale de Bartok, le
folklore mais aussi des traditions savantes, une sorte de convergence entre ce
symbolisme qui vient de Mitterlinck et donc ses légendes populaires. Alors
pourquoi tout ce synchrétisme? Parce que Bartok a pour libretiste un certain
Béla Balache qui est un ami de Codaye qui lui s'intéresse au folklore comme
Codaye et Bartok mais du côté littéraire. Et donc qui a fait ce livret et il est
aussi un grand admirateur de Mitterlinck, de Maurice Mitterlinck, les pièces
courtes de Mitterlinck qui sont assez sombres comme Bartok et Codaye sont des
admirateurs de Debussy. Donc il y a une sorte de synchrétisme qui se fait,
Mitterlinck, Debussy, musique populaire, légende populaire. Il en résulte c'est
de cet opéra qui est extraordinaire, qui est une heure de musique extrêmement
intense. C'est ce que vous écrivez, c'est une fable musicale âpre et somptueuse,
une heure de tension dramatique continue, sans division en acte ou scène où les
protagonistes de ce conte ouvrent cette porte. Et je vous propose d'écouter
l'ouverture de la quatrième porte, le jardin qui ouvre sur le jardin secret dans
une interprétation d'Anne-Sophie Van Hauteur et de John Tomlinson. La mármère
다�lichen der G approached Je ne veux plus être un homme. Je ne veux plus être un
homme. Je ne veux plus être un homme. Je ne veux plus être un homme. Je ne veux
plus être un homme. Je ne veux plus être un homme. Je ne veux plus être un
homme. Je ne veux plus être un homme. Je ne veux plus être un homme. Je ne veux
plus être un homme. Je ne veux plus être un homme. Je ne veux plus être un
homme. Je ne veux plus être un homme. Je ne veux plus être un homme. Je ne veux
plus être un homme. Je ne veux plus être un homme. Je ne veux plus être un
homme. Je ne veux plus être un homme. Je ne veux plus être un homme. Je ne veux
plus être un homme. Je ne veux plus être un homme. Je ne veux plus être un
homme. Je ne veux plus être un homme. Je ne veux plus être un homme. Le concerto
pour piano numéro 3 de Béla Bartók. Vous écrivez, Laetitia, à propos de Béla
Bartók, vous dites que son œuvre a des accents tragiques mais autant d'énergie
inépuisable. Elle tient d'un bout à l'autre dans la tension entre ces deux
pôles, déploration et danse, intériorisation et extériorisation, savante et
imprégnée de l'esprit de la musique populaire, d'une force sauvage qui inspira
jusqu'au hard rock. Bientôt 80 ans après sa mort, Bartók et sa musique n'ont
jamais été aussi proches. C'est une aventure en tout cas cette biographie qui
vous a portée. Oui, parce que le compagnonnage de Bartók est un compagnonnage
heureux et qui nous porte vers le haut. C'est un homme très intègre, austère
mais passionné, comme sa musique. Sa musique est admirable, on a entendu des
extraits. Elle n'est pas si difficile qu'elle en a la réputation. Je pense que
plus le temps va passer, plus le public va s'acclimater à cette musique, ne plus
en avoir peur. Là, je vois par exemple que le Château de Barbe Bleue va être
donné à Athènes bientôt, il va être donné à Lyon ces jours-ci. Il y a de plus en
plus de Bartók qui sont joués. Même par exemple ces Six Quatuor qui sont des
oeuvres plus intérieures, plus difficiles, quand on commence à les écouter, il
n'y a plus de difficultés. Je pense que ce sont des oeuvres à écouter au concert
plutôt qu'au disque. Il y a des oeuvres comme Microcosmos qui sont très
singulières aussi. Microcosmos est une oeuvre singulière, attachante. Elle a été
composée pour son fils qui raconte que quand son père lui a travaillé son piano,
il allait gribouiller un petit morceau dans la pièce d'à côté pour le faire
travailler un autre point. L'idée c'est de mettre le monde entier dans cette
musique. Les Microcosmes c'est toute l'histoire de la musique, toutes les
musiques, tous les rythmes, souvent de toutes petites pièces, souvent pleines de
poésie et d'humour. Mais ce que je voudrais ajouter, puisque je vois que l'heure
tourne et ça me semble vraiment important, c'est de dire que c'est la musique
d'un humaniste. C'est la musique de quelqu'un qui pensait que la richesse
culturelle vient du métissage des sources et qui est partie d'une attitude un
peu chauviniste par rapport à l'Empire Austro-Hongrois. Mais qui, en découvrant
les musiques populaires et leur richesse et en élargissant son espace
géographique, petit à petit en découvrant la richesse des musiques slovak-
rhumaines, mais aussi les musiques populaires algériennes, puisqu'il est allé en
Algérie, les musiques populaires turques, puisqu'il est allé faire des collègues
en Turquie, petit à petit à attisser cet idéal de fraternité des peuples que
vous citiez au début. Je pense que rappeler cet humanisme de Bartok, c'est
vraiment essentiel en mettant ça en regard avec l'extrême force et diversité de
sa musique. Merci Laetitia Leguet, il faut lire votre livre qui s'intitule Bella
Bartok, publié aux éditions Acte Sud, et écoutez donc l'œuvre de Bella Bartok.
Nous avons essayé de vous donner envie d'aller plus loin avec cette émission et
en tout cas c'est le cas avec votre livre. Merci à vous. Idée réalisée par
Vanessa Rowensky, bien sûr, et on se quitte Laetitia Leguet, toujours en
compagnie de Bella Bartok avec Hostinato, qui est extrait des microcosmos, ces
petites pièces pour piano dont nous venons de parler ensemble, pièces
interprétées cette fois par deux grands pianistes, Bluetooth Jazz, Tchikoreya et
Herbie Hancock. A dimanche prochain!...... Samba Pezi et Rema. Stryker.