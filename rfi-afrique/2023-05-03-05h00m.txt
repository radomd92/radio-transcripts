TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-05-03-05H00M.MP3
--------------------------------------------------------------------------------
... Vous êtes bien à l'écoute de la Radio France International, 5h à Paris, 3h
en temps universel.... Et bonjour à tous, la liberté de la presse est attaquée.
Les mots du secrétaire général de l'ONU, Antonio Guterres, on célèbre
aujourd'hui la Journée Mondiale de la liberté de la presse. La guerre au Soudan
et cette petite lueur d'espoir, le Soudan du Sud annonce un accord entre les
deux camps pour des discussions de paix. À suivre également, après une flambée
de violences hier, Israël et les groupes armés palestiniens se mettent d'accord
pour un cessez-le-feu, c'est ce qu'annoncent des responsables palestiniens. Et
puis en football, Lionel Messi, sanctionné par le PSG pour un voyage en Arabie
Saoudite.... La Journée Mondiale de la liberté de la presse, mercredi 3 mai, un
enjeu plus que jamais d'actualité selon Reporter Sans Frontières. 55
journalistes ont été tués dans l'exercice de leur fonction l'année dernière. À
cette occasion, RFI s'intéresse à la situation particulièrement préoccupante en
Afghanistan. Bientôt deux ans que les talibans ont pris le pouvoir et les médias
du pays ne sont plus que l'ombre d'eux-mêmes. Pour tenter d'éviter
l'emprisonnement, les intimidations, les journalistes doivent s'auto-censurer.
Fatima Hassan Zada peut en témoigner, cette afghane qui vient d'arriver en
France à travailler pendant des mois de façon clandestine. Vincent Souriot a
rencontré la journaliste pour RFI. Elle a 26 ans, des lunettes rondes, un léger
voile blanc sur les cheveux et elle vient d'arriver à Créteil, dans un foyer
d'hébergement géré par France Terre d'Asile. En Afghanistan, elle travaillait
pour Soubikaboul, l'un des quotidiens les plus critiques envers les talibans.
Avant qu'ils ne prennent le pouvoir, on publiait beaucoup sur leurs activités et
celles d'autres groupes terroristes. Et c'est pour ça que dès qu'ils ont pris
Kaboul, ils ont fermé nos bureaux en même temps que d'autres médias anti-
talibans. Femmes et journalistes difficiles de faire plus dangereux sous le
nouveau régime afghan, malgré les risques, Fatima Hassan Zada et son mari sont
restés le plus longtemps possible en Afghanistan. Mais lorsque la police
talibane a kidnappé leurs plus proches collègues, il a fallu partir. Il a été
arrêté par les talibans et ils l'ont torturé pendant des heures pour savoir où
était notre rédacteur en chef et où on se cachait nous, ses collègues qui étions
toujours actifs en Afghanistan. Heureusement, il n'a pas craqué. Il a dit qu'il
était seul et qu'on était déjà tous partis à l'étranger. Ils l'ont gardé pendant
cinq jours et ils ont fini par le libérer dans un état de santé critique. Une
nouvelle vie commence en France. Journaliste, oui, elle le reste, elle veut à
tout prix continuer d'écrire pour faire exister son pays. Oh Burkina Faso,
Amnesty International pointe du doigt la responsabilité de l'armée et de ses
supplétifs VDP. Dans le massacre de Karma, le 20 avril dernier, ce village situé
au nord du Burkina Faso a subi des exécutions sommaires. 147 villageois dont 45
enfants ont été tués selon les ressortissants de cette localité. L'organisation
Amnesty International espère que l'enquête qui a été ouverte par le tribunal de
grande instance de la ville de Wahigouya, proche du lieu du massacre, sera menée
de façon impartiale et indépendante. Au Soudan, après plus de deux semaines de
conflit, cette petite lueur d'espoir, le Soudan du Sud annonce que les deux
camps, celui du général Al-Burhan et celui du général Emeti, sont d'accord pour
que des représentants soient nommés pour mettre en place des discussions de
paix. Autre annonce du Soudan du Sud qui fait office de médiateur, une nouvelle
trêve de 7 jours doit être mise en place demain. Les civils, eux, sont de plus
en plus nombreux à fuir. Plus de 100 000 sont déjà partis se réfugier à
l'étranger, selon l'ONU. Et l'exode pourrait s'amplifier dans les prochaines
semaines, Jérémy Lanch. L'Égypte et le Tchad ont enregistré le plus d'arrivée,
mais un nombre conséquent de personnes se dirigent également au Sud-Soudan,
souvent des Sud-Soudanais qui étaient eux-mêmes réfugiés chez leurs voisins du
nord. 1000 personnes se prestent aussi chaque jour à la frontière éthiopienne,
et il faut rajouter à cela plus de 334 000 déplacés internes, explique Paul
Dillon de l'Organisation internationale pour les migrations. Pour vous donner
une idée de l'ampleur de ce qui se passe, nous avons enregistré plus de déplacés
liés aux violences en deux semaines que pendant toute l'année dernière. À terme,
l'ONU table sur le chiffre astronomique de 800 000 nouveaux exilés à l'étranger.
C'est quasiment autant que le nombre de réfugiés présents au Soudan. Et les
besoins sont énormes. Jens Lark, du Bureau des Affaires humanitaires de l'ONU,
rappelle que le programme d'assistance au Soudan n'était financé qu'à 14% quand
les hostilités ont éclaté. Nous appelons tous les donateurs à fournir rapidement
l'argent nécessaire aux organisations humanitaires qui luttaient déjà avant le
conflit pour répondre aux besoins. Sans cet argent, elles ne peuvent tout
simplement rien faire. Le problème est le même pour les pays voisins du Soudan
et qui reçoivent les réfugiés. Le plan d'assistance humanitaire au Sud-Soudan
n'est financé qu'à 25% aujourd'hui. Pour le Tchad, c'est pire, avec 3% seulement
récoltés. Jérémy Lanch, Genève et Réfi. Et puis cette information qui vient de
tomber en Birmanie, la Jeanne de Birmane, annonce l'amnistie de plus de 2000
prisonniers qui étaient condamnés pour dissidence contre les militaires au
pouvoir. Et ce à l'occasion d'une fête bouddhique. Au Proche-Orient, Israël et
les groupes armés palestiniens conviennent d'un cessez-le-feu. C'est ce
qu'annoncent en tout cas des responsables palestiniens. Hier, suite à la mort de
Khader Adnan, il y a eu une flambée de violence entre Israël et Gaza. Des
roquettes ont été lancées depuis Gaza en direction d'Israël pour protester
contre la disparition de cette figure de la cause palestinienne. En réponse,
l'armée israélienne avait, elle aussi, mené des frappes. Ces tensions qui
montrent en tout cas l'importance de Khader Adnan comme symbole de la résistance
palestinienne. Sa grève de la faim aura duré plus de 80 jours. Il protestait,
comme auparavant, contre la détention administrative, un système de détention
très peu encadré, mis en place par Israël. Une attitude qui inspire beaucoup de
palestiniens, notamment ce jeune activiste palestinien de Ramallah. Israël,
d'une certaine manière, envoie un message, un message pour dire qu'il n'y a pas
d'autre moyen de contourner ce résultat. Si vous faites une grève de la faim,
vous pouvez mourir. Parce que je pense qu'ils ont réalisé qui était cet homme.
Khader Adnan, c'était une icône palestinienne, une icône de la lutte pour la
liberté. Et c'était une icône pour tous les palestiniens, car nous avons vécu
avec lui plusieurs de ces grèves de la faim. Nous l'avons vu s'agripper à sa
liberté déjà plusieurs fois. C'était cette personne qui était toujours dans la
rue, à chaque manifestation. Vous le voyez sur place. C'était le premier. Il
croyait à la liberté et se battait pour cette dernière. C'était un homme
impressionnant et humble, qui méritait bien plus. En France, une nouvelle
journée d'action contre la réforme des retraites annoncée par les syndicats,
elle aura lieu le 6 juin prochain. Les élections générales approchent en Grèce,
elles se déroulent dans trois semaines, le 21 mai. Et c'est dans ce contexte
qu'on apprend que le parti d'extrême droite, les Hélènes, a été interdit par la
Cour suprême. Le gouvernement grec réagit et salue une décision historique.
Nicolas Feldman. C'est à une large majorité de neuf magistrats contraints que la
décision a été prise. D'après les juges, le parti les Hélènes est une
continuation de la formation néonazie obdorée. Plusieurs de ses membres purgent
des peines pour appartenance à une organisation criminelle. Impossible donc,
estime la Cour suprême avec de telles condamnations de participer au scrutin.
C'est le cas du fondateur des Hélènes, Ilias Kassidiaris, détenu dans une prison
de haute sécurité pour une peine de 13 ans. Avec ce jugement, cet ex-député
d'Obdorée, bien connu pour ses excès de violence, ses diatribes racistes et
antisémites, ne pourra donc pas non plus briguer de sièges aux élections
législatives, contrairement à ce qu'il avait annoncé depuis sa cellule. Certains
sondages donnaient pourtant sa formation en mesure de passer la barre des 3%
nécessaires pour entrer au Parlement. Pour le Premier ministre conservateur
Kyriakos Smith sota kis, qui brigue un second mandat, cette interdiction est une
victoire. Pour l'heure, son parti Nouvelle Démocratie devance dans les
intentions de vote la formation de gauche radicale Syriza. Et puis en football,
l'EPSG vit décidément une saison rocambolesque. Lionel Messi suspendu pour deux
semaines, l'Argentin est parti en voyage sans prévenir ses dirigeants. En Arabie
Saoudite, il va donc manquer ses deux prochains matchs avec le PSG et risque
bien de vivre ses dernières semaines dans la capitale. Cédric de Oliveira. La
décision paraît surprenante au vu de la mensuétude habituelle du PSG en
Versailles-Stars. Mais cette fois, les têtes pensantes du club ont décidé de
frapper fort. Lionel Messi avait prévu un voyage de longue date en Arabie
Saoudite à partir de lundi. Il faut dire que l'Argentin touche 30 millions
d'euros par an pour promouvoir l'image du royaume. Le problème, c'est qu'au même
moment, ses coéquipiers participaient à un entraînement d'autant plus important
que le PSG traverse une grave crise de résultats et reste sur une défaite
cinglante à domicile face à l'Orient 3-Bus 1-1. Les dirigeants parisiens
n'étaient visiblement pas au courant de cet escapade et ont donc choisi de
sanctionner le champion du monde 2022 pendant deux semaines. Messi ne sera pas
payé pendant cette période et manquera le déplacement à Troyes et la réception
d'Ajaccio, deux mal classés du championnat. Il lui restera donc trois rencontres
à disputer dans cette fin de saison interminable pour Paris, qui n'est toujours
pas assurée d'être champion. Selon les informations de plusieurs médias, cette
sanction est aussi un signe de plus que le séjour de l'Argentin dans la capitale
française est sur le point de se terminer. L'année supplémentaire en option
présente dans son contrat risque de ne pas être levée. Et c'est la fin de ce
journal mais on reste ensemble pendant encore 20 minutes. Et vous écoutez RFI,
les 5h et 10 minutes ici à Paris, au sommaire pour ces 20 prochaines minutes.
Dans un instant, on vous emmène visiter l'une des propriétés du roi Charles III,
le manoir de Highgrove dans le sud-ouest de l'Angleterre. Dans le focus, on va
revenir sur cette enquête du magazine français L'OPS selon laquelle le géant
français Total est coupable de graves pollutions au Yémen. Juste après le
reportage France, les femmes sont de plus en plus nombreuses à faire du
skateboard. Un collectif a même été créé à Paris alors que les Jeux Olympiques
approchent, ce sera dans environ un an et demi. Et puis vous aimez aller au
cinéma, dans le Rendez-vous Culture, on vous parle de ce film qui vient de
sortir, qui nous retrace les pas d'un mercenaire de la Légion française, je vous
en dis plus, dans environ 15 minutes. Et le couronnement de Charles III approche
à grands pas, c'est dans 3 jours seulement, Charles III, qui au cours des 60
dernières années, a acquis de nombreuses propriétés à travers le Royaume-Uni.
Dans le reportage international, on vous emmène visiter l'une de ses préférées,
le manoir de Highgrove. Ça se trouve dans le comté du Gloucestershire, dans le
sud-ouest de l'Angleterre. Et vous allez l'entendre dans cette résidence,
Charles III a mis en place ses grands principes écologiques et architecturaux.
Emeline Vain. On ne se balade pas dans les jardins de Highgrove en toute
liberté, uniquement sur réservation et accompagné d'un guide photo interdite. La
visite s'ouvre sur la promenade du Teint, une grande allée bordée de taupières
aux formes arrondies et élaborées. Rosie Ardington, commissaire d'une exposition
dédiée à Highgrove, connaît le domaine par cœur. La promenade du Teint contient
70 espèces de teints. À l'origine, c'était un chemin de gravier, mais sa majesté
a voulu le rendre plus esthétique. Il a mis des pierres, des ardoises. Entre les
pierres, il a planté le teint. Il l'a fait tout seul. Quand vous vous promenez,
vous entendez les oiseaux, vous pouvez admirer les magnifiques jardins formels,
informels, et sentir le teint que vous écrasez en marchant. Une dizaine de
jardins composent le domaine. Ici, une cabane dans les bois où ont joué les
princes Harry et William enfant. Là, une fontaine créée à partir des ruines
d'une église voisine. Partout, l'harmonie géométrique chère au souverain et
surtout le respect de l'environnement. Il y a beaucoup de jardinage biologique.
Sa majesté appelle les moutons les sabots d'or. Avec leurs sabots, ils font
germer les graines et fertilisent le sol. Ils tombent la pelouse aussi. Dans ce
pré, ce sont des gens qui fauchent les herbes à la main. On n'utilise pas de
machine. On laisse beaucoup la nature jardiner. Attraction touristique aux 40
000 visiteurs annuels, la résidence secondaire de Charles et Camilla accueille
aussi un centre de formation architectural, un autre des dada du roi.
Constantine Inemé coordonne l'action de la Fondation du Prince, l'ONG éducatif
créé par Charles. Lorsque vous visitez High Grove, cela vous fait réfléchir.
Comment vivre en accord avec la nature? Que peut-elle nous apprendre? Et comment
incorporer cela au quotidien? C'est ce qu'apprennent nos étudiants. Ils peuvent
le mettre en pratique en utilisant par exemple du bois local lorsqu'ils créent
des meubles tout simplement. Mais ces principes s'appliquent à des projets plus
larges. Par exemple, comment utiliser des matériaux locaux à l'échelle d'un
lotissement pour le rendre réco responsable dès sa construction? Des principes
de durabilité et de circuit court respectés jusque dans la boutique. Nous
proposons de nombreux produits qui viennent de High Grove. On a du jean réalisé
avec des plantes de High Grove, du miel, des abeilles de High Grove. On a même
créé une autre parfum fabriqué avec l'essence d'étioles argentées du jardin.
Même si vous allez au restaurant, nous utilisons les produits du potager. À
l'écart du jardin, caché par des cerisiers, le roi s'est construit un sanctuaire
pour méditer un refuge auquel aucun employé n'a accès. Emeline Van, de retour de
High Grove, RFI. Et ce reportage international est à retrouver sur RFI.fr Le
focus à présent, le magazine L'OPS français l'affirme dans une enquête publiée
il y a deux semaines. Le géant français du pétrole total est coupable de
pollution majeure au Yémen. L'impact sur la santé des habitants et
l'environnement serait même considérable. Et ce sont les pratiques de tout un
secteur qui sont en cause. Bonjour Guilhem Delteille. Bonjour. C'est la province
du Hadramout qui est particulièrement touchée. Oui, nous sommes là à l'est du
Yémen. Il s'agit de la plus grande province du pays. Une région fertile et riche
en eau, comme en pétrole. Et cette province agricole est devenue aussi une
région pétrolière. Des puits ont été creusés sur les hauts plateaux, dominants
des vallées cultivées. Wadi Bin Ali est l'une d'entre elles. Sur ses hauteurs se
trouve le bloc 10, un lieu d'extraction. Et en 2008, l'oléoduc transportant le
pétrole a explosé en pleine nuit dans une vallée voisine. Pendant plusieurs
heures, la production s'est écoulée dans la nature. Depuis, les habitants vivent
dans la crainte, témoigne Ali. L'un d'entre eux joint sur place. Depuis 2008,
nous, les habitants de Wadi Bin Ali, nous sommes terrifiés. Les gens ont très
peur. Ils ont peur de ce qu'ils boivent. Ils ont peur de ce qu'ils respirent.
Tout nous fait peur. Et ils disent que c'est à coup sûr de la faute de
l'entreprise. L'entreprise, c'est total. Le groupe français qui exploitait alors
le bloc 10 s'est accusé d'avoir tardé à réagir et mené une dépollution à minima.
Dans la vallée, les cas de cancer se sont multipliés parmi la population. Et la
situation dure encore aujourd'hui. Sur Farouk Al-Jabri, le président de la
communauté de Wadi Bin Ali, les pluies faisant ressortir des bouts noirs du sol,
il réclame un examen environnemental. Nous exigeons notamment qu'une analyse
soit faite par des personnes extérieures au Yémen. Qu'elles nous disent ce qu'il
y a dans les nappes phréatiques d'où nous buvons. Et puis qu'elles analysent
aussi le sous-sol pour voir s'il y a encore des traces de pétrole ou s'il n'y en
a pas. Mais la gestion de cette explosion n'est pas le seul reproche adressé à
Total Guilhem. Non, en effet, l'enquête de l'Obs révèle que pendant près de 20
ans, Total s'est livré à des pratiques non conformes aux procédures d'extraction
pétrolières. En cause, l'eau de production. Il s'agit d'une eau qui se trouve
dans le pétrole lorsqu'il est extrait et dont il faut la dissocier. Au lieu de
construire une usine pour traiter cette eau, Total a creusé des bassins et
laissé cette eau toxique à l'air libre pour qu'elle s'évapore. Seule une bâche
en plastique au fond du bassin devait empêcher une infiltration dans le sous-
sol. Et quand ses bassins étaient pleins, Total avait recours à un autre
système, lui aussi dangereux. Explique Quentin Muller, il est l'auteur de cette
enquête. Le surplus de cette eau de production était injecté par Total dans des
puits d'évacuation, c'est-à-dire des puits qui sont inactifs et puis on enfouit
en fait grossièrement et vulgairement de l'eau toxique. Alors c'est des choses
qui n'existent pas dans les pratiques internationales. Ça c'est des pratiques
qui n'existent que pour des entreprises voyous. C'est ce qu'un ingénieur
pétrolier français me dit, qu'il n'a jamais entendu parler de ça et que c'est
criminel de faire ça. Dans un communiqué, Total assure qu'aucun puits
d'évacuation n'était utilisé pour stocker l'eau de production. Mais cette
version s'oppose au témoignage d'un ancien ingénieur du groupe recueilli par
Quentin Muller. Et Total affirme aussi qu'un système d'écrémage permanent avait
été installé dans les bassins et qu'il permettait ainsi de récupérer l'huile
résiduelle séparée de l'eau de production. Mais les résidus solides étaient eux
laissés au fond des bassins. Et Guilhem, cette enquête de l'Obs n'est pas la
seule à dénoncer l'impact environnemental de l'extraction pétrolière aux
Adramauts. Non, en effet, selon le centre de Sanaa pour les études stratégiques,
un groupe de réflexion, le fait que les pratiques en cours au Yémen déviaient
des normes environnementales saines était un secret bien connu des habitants
comme des professionnels du secteur. Yasmine Al-Riani est l'auteur d'un rapport
sur le sujet pour ce think tank. Elle évoque des pratiques répandues dans le
secteur et dénonce le champ libre laissé par les autorités yéménites. Nous
devons surveiller de manière appropriée ce qui se passe dans mon étude. J'ai
parlé à certains responsables de l'administration en charge de la protection de
l'environnement et ils m'ont dit qu'ils ne pouvaient même pas accéder à certains
locaux ou envoyer des comités d'inspection. Ils n'avaient pas obtenu
l'autorisation des entreprises ni le soutien des autorités. C'est un problème
sérieux en termes de mise en œuvre des réglementations et des contrôles. Et les
mécanismes de transparence, déjà très faibles avant la guerre, ont été encore
affaiblis par le conflit qui dure depuis près de 9 ans. Le géant français total
coupable de pollution au Yémen, le focus, signé donc Guilhem Deltey. Les Jeux
Olympiques de Paris, c'est dans moins d'un an et demi. Comme aux précédents JO,
une discipline aura une place de choix, le skateboard. Un sport qui se
démocratise et c'est le sujet de notre reportage France qui attire de plus en
plus de femmes. Elle compose quasiment la moitié des licenciés de skate de
l'Hexagone, à tel point qu'un collectif de skateuses Girls World s'est formé à
Paris l'année dernière avec l'objectif de faire en sorte que les femmes gagnent
en confiance dans cette discipline. Baptiste Coulon. Une artère sans voiture
leur sert de terrain de jeu. Assises, couchées, courbées, pendant 3 heures, 20
filles se laissent glisser sur leur planche d'un bout à l'autre de la rue.
Pauline est collideur du collectif Girls World, née en 2018 à Los Angeles. On
est en majorité à destination des femmes et des minorités de genre. C'est plus
difficile en tant que femme de prendre possession de l'espace urbain en skate.
Ça crée un environnement beaucoup plus facile pour débuter lorsqu'on est entouré
principalement d'autres femmes. Qu'est-ce que ça apporte de skater juste avec
des filles? Ça fait moins peur. Eva et Clara ont débuté le skateboard il y a 2
ans. Ça fait moins peur et puis se gameler dans des filles, ça fait moins
flipper que se gameler dans 40 mecs qui skatent depuis 12 ans et que tu as
l'impression qu'ils vont te juger, même si ce n'est pas le cas en plus. Mais moi
je trouve ça rassurant. Déjà eu cette expérience d'arriver dans un skatepark? Et
de faire demi-tour, carrément, parce qu'il y avait trop de mecs. Je ne me
sentais pas légitime. C'est impressionnant, on voit qu'il n'y a que des mecs et
puis ils se débrouillent super bien. C'est un peu flippant quand on débute. Donc
le fait de se retrouver entre filles, ça fait un peu girl power. Donc nous
sommes là, on existe aussi, nous aussi on skate. Ça nous redonne confiance en
nous et tout, donc c'est génial. Une main en équilibre, l'autre dans celle
d'Emily. Ilénia fait ses premiers pas sur un skateboard. Son amie lui vient en
aide. Tu mets le pied un ici et l'autre ici. D'accord? Et je te donne la main.
Allez, courage! On peut y aller doucement. 1, 2, 3. Voilà! Bravo Ilénia, voilà
c'est ça. Après tu mets de la vitesse et c'est comme ça. C'est facile ou pas?
Non, c'est compliqué. La première fois quand j'étais ici, j'étais trop peur et
maintenant je peux faire tout seul. C'est comme ça, la première semaine je
tombais tout le temps. C'était difficile. Je suis fière de toi. Les Jeux
Olympiques participent aussi à la démocratisation de ce sport. À le rendre plus
visible, observe Manon, 27 ans. Déjà là on a commencé à voir les compètes, ce
que ce soit au J.O. et celle qui se prépare actuellement. Il y a autant de meufs
que de garçons. Et on se rend compte qu'elles ont 12 ans, 10 ans et qu'elles ont
déjà un niveau excellent. En fait, elles se posaient pas la question à la base
si c'était pour les garçons ou pour les femmes. Elles ont juste fait plaisir
avec, elles ont kiffé. Effectivement, avec les J.O. je me dis que tout est
possible. Il faut juste le vouloir et c'est ouvert à tout le monde finalement.
La France compte 6000 licenciés de skateboard. 40% sont des filles. Le chiffre
ne fait qu'augmenter depuis une quinzaine d'années. Reportage France, signé
Baptiste Coulomb pour RFI.