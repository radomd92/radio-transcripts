TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-12-14H00M.MP3
--------------------------------------------------------------------------------
 RFI Midi, les 14h à Paris Midi Pile en temps universel. Un nouveau journal avec
Gilles Moreau. Bonjour à tous, dans l'actualité, nombreuses réactions indignées
à travers le monde devant de nouvelles images venant d'Ukraine, montre la
décapitation d'un prisonnier de guerre ukrainien par des forces prorusses. Le
président ukrainien Volodymyr Zelensky dénonce une nouvelle exaction des «
monstres russes ». La mission de l'ONU pour les droits humains en Ukraine se dit
horrifiée. Toujours beaucoup de tensions entre la Chine et Taïwan. Le président
chinois a demandé à son armée de renforcer l'entraînement au combat réel.
Dimanche prochain, Pékin imposera pour les besoins de ses exercices une zone
d'exclusion aérienne au nord de Taïwan. En France encore est toujours la réforme
des retraites, avant demain une 12ème journée nationale de mobilisation des
opposants et vendredi la décision en forme de couperet que rendra le conseil
constitutionnel. Dans 10 minutes après ce journal, le journal Esport avec les
deux derniers quarts de finale en Ligue des champions, Real Madrid Chelsea et
Milan Napla. JLMD avec ce degré de plus qui semble avoir été atteint dans les
atrocités commises en Ukraine. Sur les réseaux sociaux, on a vu une vidéo
montrant ce qui semble être la décapitation de la Chine.