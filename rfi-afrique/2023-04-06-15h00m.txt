TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-06-15H00M.MP3
--------------------------------------------------------------------------------
... En raison d'un mot d'ordre de grève nationale, RFI n'est pas en mesure
d'assurer son programme habituel. Veuillez nous en excuser.... Oh mon amour, tu
m'oppresses Tu es très lourd, j'avoue j'ai une redesse Plus trop d'amour, quelle
tristesse J'vais te quitter, mais j'vais l'faire en souplesse J'vais te le dire
en face J'vais te le crier en pleine face Je t'aime plus J'ai oublié comment je
t'ai aimé Je m'aime plus J'vais t'enlècher, moi qu'à des ménuppées Oh mon amour,
t'exagères J't'ai envoyé seulement 16 fois Je t'aime par message vocal cet
après-m' J'vais te le dire en face Le temps passe pas plus vite quand on est
tout seul Mais bon, le temps passe Un joli silence ça peut faire si mal, c'est
pas compliqué Tu déclares ta flemme et t'éteins la flamme, t'es pas mon briquet
En vrai j'suis rouge parce que tu veux plus sortir avec moi Mais j'pense être le
garçon adéquat Je t'aime plus J'ai oublié comment je t'ai aimé Je m'aime plus
J'vais t'enlècher, moi qu'à des ménuppées J'me demande pourquoi je ne ressens
pas La joie quand je te vois Je t'aime plus J'ai oublié comment je t'ai aimé Je
m'aime plus J'vais t'enlècher, moi qu'à des ménuppées La la la la la la la la En
le sous, ces brosses sont mes mots sur ta peau Tout est velours, c'est beau tout
ce chaos idéal J'ai posé des mots sur ta peau Joli coulant dans le creux de ton
dos J'ai posé des mots sur ta peau Dans le cou, le goût de mon désir t'a ravaisé
Et là sur mes poignées mouillées Le brésil de ta bouche brûle ma peau J'ai posé
des mots sur ta peau J'ai posé des mots sur ta peau Joli coulant dans le creux
de ton dos J'ai posé des mots sur ta peau Les mots brûlants de ma bouche
Couleront jusque dans ta gorge Et le souffle ne sera pas coupé mais transmis
Prends mon souffle, prends mes mots Des carottes dans le creux de ton dos C'est
bon, c'est comme de l'eau La la la la la la la J'ai posé des mots sur ta peau
Sur ton dos La la la la la la la Joli coulant dans le creux de ton dos La la la
la Ma peau Des mots je reposais sur la peau Des mots je reposais sur la peau La
la la la la la la La la la la la la la la la Je m'enivre avec elle quand je
m'ennuie la nuit Quand je m'ennuie et Jeune musicien qui attend de voir les
souvenirs J'suis l'aimant, s'il faut le faire J'attends, j'apprends à être
patient, pas à me taire J'veux pas tes commentaires, pas né ici, pas vu là-bas
L'insomnie du visage, remplace le mascara, collecte sur mascarade Prends tes
talons et ta cravate, des fossettes sur les joues, à l'humeur exécrable La rage,
perdue dans mes lettres, dans la peau, je joue au scrabble Pour rester droit
dans mes notes, j'reprends la diagonale J'veux pas ton manque d'affection,
réflexion, flexion Dans ma ponctuation, dans la mission, dans l'excel Oubli de
soirée, j'excelle, désorganisé sans les leçons Loin des tableaux, excel, j'suis
pas la relève Non, ni même un prodige, je cherche la save Qui trop grammaire
assigne, devine avec qui je suis En train d'refaire un titre, on changera pas
l'monde Mais on y participe Je suis les doutes, quand la lune m'écrit Je
m'enivre avec elle Quand je m'ennuie la nuit Je m'enHer喜 le sourier qui veut des
souvenirs Je mome Musicien qui attend de voir les souvenirs Je suis les doutes
quand la lune m'écrit Je m'enHábное avec elle Quand je m'm'ennεί la nuit Je mome
Musicien qui attend de voir les souvenirs Je mome Musicien qui attend de voir
les souvenirs Je suis un musicien qui attend de voir les souvenirs Les
souvenirs, j'évite le regret qui veut revenir avant de partir Sans plus avant de
se dire que je suis pas l'esclave De la voix classique qui vote contre l'envie
Avant de trouver le calme, je ne vis pas ce que ma vie a cherché qui je suis Je
crois que c'est possible, même si c'est pas facile De marcher sur un fil, quand
t'as pas l'équilibre Avec des si, tu sais, on ne fait pas des ça L'indestin
existe oui, mais l'ambitieux vivra Eh eh, oui on est plein dans la cervelle
Resserre un verre, je le sais qu'elle La mémoire de Douribas, le corps de Popeye
Simon Orson, Iker Rapel Je voulais faire de la musique comme quand j'étais
enfant Oui c'était du business, je l'avais pas vu avant Merde Je suis les doutes
quand la lune m'écrit Je m'enivre avec elle quand je m'ennuie la nuit Je m'en
dis, un sourire qui veut des souvenirs Ce musicien qui attend de voir les
souvenirs Je suis les doutes quand la lune m'écrit Je m'enivre avec elle quand
je m'ennuie la nuit Je m'en dis, un sourire qui veut des souvenirs Ce musicien
qui attend de voir les souvenirs Eh eh, qui est le musicien qui attend de voir
les souvenirs Je suis le musicien qui attend de voir les souvenirs Eh eh, je
m'enivre avec elle quand je m'ennuie la nuit Quand je m'ennuie, je suis le
musicien qui attend de voir les souvenirs Vous écoutez RFI Music Y a de l'eau
salée dans les yeux des passants De vieilles branches d'olivier sur les buffets
graissants Des squares et des allées de l'emboulevard glissant Vers des golfs
argentés sur des rochers cassants Y a des bleus et dans les blés et des boutons
d'or sur ta robe Qui brille dans mon regard troublé chaque soir quand tu te
déranges Y a des bleus et dans les blés et des pensées des immortels Qui dansent
leurs dents sans diable quand ton corps est le mien semel Y a de vieilles
fontaines qui débordent de sa voix Plaine de pièces anciennes, noyées de voeux
d'espoir Assis devant leurs portes, les vieux laissent filer Le temps et son
escorte, les visages exilés Y a des bleus et dans les blés et des boutons d'or
sur ta robe Qui brille dans mon regard troublé chaque soir quand tu te déranges
Y a des bleus et dans les blés et des pensées des immortels Qui dansent leurs
dents sans diable quand ton corps est le mien semel Des bleus et dans les blés,
des bleus et dans les blés Y a des reflets d'or et des ombres en dentelle Sur
l'amour qui s'endort dans les chambres d'autel Chaque heure est un clin d'œil au
roman de la vie Qui se mange en mille feuilles ce bois qui sec aussi Y a des
bleus et dans les blés et des pensées des immortels Qui dansent leurs dents sans
diable quand ton corps est le mien semel Des bleus et dans les blés et des
boutons d'or sur ta robe Qui brille dans mon regard troublé chaque soir quand tu
te déranges Y a des bleus et dans les blés et des pensées des immortels Qui
dansent leurs dents sans diable quand ton corps est le mien semel Des bleus et
dans les blés, des bleus et dans les blés Y a des bleus et dans les blés, des
bleus et dans les blés Des bleus et dans les blés, y a des bleus et dans les
blés J'suis toute seule sur un tandem Personne à qui dire je t'aime Je
m'organise, j'avance, je brise Tous les silences, je fonce, je mise Et le ciel
de Paris m'entraîne À la rama sur les quetzales Solo c'est pas facile de pédaler
Alors je prends mon temps pour traverser ma vie Traverser ma vie Traverser ma
vie Traverser sans dire Il reste une place à l'arrière Une place à la terre
J'regarde le vent, j'suis déterre J'suis déterre Fille, t'lèves la tête même si
souvent c'est dur J'écoute mes maquettes à fond dans la voiture Le ciel de Paris
toujours J'regrette pas d'avoir fait l'amour Alors je chante, je chante pour toi
Mais est-ce que tu danseras pour moi toute la vie? Traverser ma vie Traverser ma
vie Traverser sans dire J'suis toute seule sur un temps d'amour Toute seule sur
un temps de personne à qui je t'aime Seulement pour dire je t'aime Traverser
Est-ce que tu danseras pour moi toute la vie? Le ciel de Paris toujours
J'regrette pas d'avoir fait l'amour J'allume mon poste de télé pour admirer ce
qu'il s'y passe Un milliardaire s'envoi en l'air Quitte l'atmosphère pour voir
l'espace J'traque 30 bol d'air et 5 cuillères Contre un p'tit verre sur ma
terrasse J'en ai ras l'bol de tout ce béton J'ai la folie des grands espaces
Mais qu'est-ce qui s'passe dans nos p'tites têtes? On s'entasse tous comme des
sardines Dans les grosses boîtes que l'on conserve Le p'tit poisson doit suivre
sa ligne Dans les grosses boîtes que l'on conserve Le p'tit poisson doit suivre
sa ligne Et puis merde, j'ai décidé de vivre au loin sur la colline Vivre seul
dans une maison avec la vue sur ma raison J'préfère vivre pauvre avec mon âme
Que vivre riche avec la neuf Si le blé m'fluit du bonheur J'me ferais p't-être
agriculteur Et si le blé m'fluit du bonheur J'me ferais p't-être agriculteur Y a
trop d'feux rouges dans les grandes villes J'ai préféré me mettre au vert J'ai
plus d'bonheur à vivre en paix Que m'admirer au fond d'un verre J'boirai le
seigne de mon ruisseau Plutôt que l'eau sale du fond de la Seine Chargé en plomb
et en histoire Que la surface ne laisse plus voir Chargé en plomb et en histoire
Que la surface ne laisse plus voir J'ferais des bandes pour m'éloigner Pour
m'retrouver face au miroir Juste une seconde de vérité Pour qu'mon passé coule
sous les poings J'ferais des bandes pour m'éclipser Pour m'retrouver face à que
dalle Juste une seconde de vérité Pour contempler ce qu'on est tous Et puis
merde, j'ai décidé de vivre au loin sur la colline Vivre seul dans une maison
avec la vue sur ma raison J'm'faire vivre pour avec mon âme Que vivre riche avec
l'âneur Si le blé m'gile du bonheur J'me ferais p't'être agriculteur Ça fait
longtemps que j'n'ai plus vu Ce coin de soleil à l'horizon Ça fait longtemps que
j'l'attendais Le p'tit lueur de la raison Une p'tite chanson au clair de l'hum
Pour réchauffer le coeur de pierre Le grand retour à l'essentiel Le feu de bois
éclaire le ciel Le grand retour à l'essentiel Le feu de bois éclaire le ciel La
mélodie de la nature reprend ses droits sur la folie C'est toute la ville qui
nous observe Que l'on oublie au fil du temps La mélodie, celle de la vie Que
l'on consume à chaque instant Tout le monde s'acquise, s'écrase au sol Et j'ai
choisi la clé des chants Tout le monde s'acquise, s'écrase au sol Et j'ai choisi
la clé des chants Et puis merde j'ai décidé De vivre loin sur la colline S'en
vivre au sol dans une maison Avec la vue du Maraison sur ma paraison Avec mon
âme que vivre riche avec l'âneur Si le blé m'gile du bonheur J'me ferais
p't'être agriculteur J'me ferais p't'être agriculteur Si le blé m'gile du
bonheur J'me ferais p't'être agriculteur Au bord du fleuve tranquille Qui coule
vers l'océan Ces archipels, ces îles Ces horizons changeants Ces horizons
changeants Il y avait deux amants Le soir, sur la rive paisible Du grand fleuve
lent Serré contre le vent Riant comme deux enfants Riant comme deux enfants Ils
s'aiment fâlement Ils s'aiment fâlement Comme je t'aime Comme je t'aime Comme je
t'aime Tu es mon plaisir Et moi je t'aime Et moi je t'aime Et moi je t'aime Tu
es mon désir Au grand fleuve tranquille Coule vers l'océan Ces archipels, ces
îles Ces horizons changeants Ces horizons changeants Ces horizons changeants Du
levant au couchant J'aime tes os impassibles Ton cours calme et lent Tes rives
invisibles Dans les saules penches Dans les saules penches Ployées par le
courant Est-ce que tu m'aimes Est-ce que tu m'aimes Est-ce que tu m'aimes Oh mon
amant Si moi je t'aime Si moi je t'aime Si moi je t'aime Pas pour longtemps Et
sur le fleuve tranquille Et sur le fleuve tranquille Il s'est embarqué Là-bas
vers l'océan Ces archipels, ces îles Ces archipels, ces îles S'en est allé son
amant S'en est allé son amant Au bord du grand fleuve tranquille Qui coule vers
l'océan Ces archipels, ces îles Ces horizons changeants Ces horizons changeants
Ne sont plus les amants Pourquoi les mèges Pourquoi les mèges Pourquoi les mèges
Passionnément Pourquoi je l'aime Pourquoi je l'aime Pourquoi je l'aime Encore
autant Le soir sur la rive paisible Le soir sur la rive paisible Du grand fleuve
lent Le long du courant Elle songe à son amant Elle songe à son amant Son amant
Qu'elle attend Pendant que le beau fleuve lent Y suit son cours paisible Et
indifférent Toujours vers l'océan Toujours vers l'océan En oubli des amants
Pourquoi je l'aime Pourquoi je l'aime Pourquoi je l'aime Encore autant Pourquoi
les mèges Pourquoi les mèges Pourquoi les mèges Pourquoi les mèges Pourquoi les
mèges Passionnément Le soir sur la rive paisible Le soir sur la rive paisible Du
grand fleuve lent Songeant à son amant Elle entre dans le courant Elle entre
dans le courant Elle entre dans le courant Et s'en va ce noyant Maintenant Sur
le cours paisible Du grand fleuve lent Joue de grands oiseaux De grands signes
blancs De grands signes livres En souvenir des amants En souvenir des amants
R.F.I Musique R.F.I Musique R.F.I Musique R.F.I Musique M.R Middle Mais j'ai
pensé pas, je te fais peur de te faire mal girl tu m'aimes beaucoup Even if you
text me, I'm gonna go tired for you When you text me, two seconds ago I'll
answer you Cause you're special, you're my treasure These streets be too cold
but with you it's a different weather Jolie, fille jolie, mes capables font dits
sur la catapolie Baby chaque fois c'est pareil, ouais t'as toujours un problème
Baby chaque fois c'est pareil, ouais t'as toujours un problème Baby chaque fois
c'est pareil, on se fait de la guerre ça fait mal mais on s'aime beaucoup Baby
chaque fois c'est pareil, j'te fais la tête ça te fait mal girl tu m'aimes
beaucoup Baby chaque fois c'est pareil, ouais t'as toujours un problème Baby
chaque fois c'est pareil, ouais t'as toujours un problème Baby chaque fois c'est
pareil, on se fait de la guerre ça te fait mal mais on s'aime beaucoup Baby
chaque fois c'est pareil, j'te fais la tête ça te fait mal girl tu m'aimes
beaucoup Baby chaque fois c'est pareil A cette musique ton âme a fait de nos
mots et douleur de l'art Des dollars et de l'or avec des larmes Je suis la foire
de l'épaine dans du content Pour plantation tabac et chant de coton Les corps
russés par le fouet et les forts Je suis la foi de ceux qui ne cessent de
souffrir Eux qui sous des cieux qui le veulent souffrir Fredonnent leurs espoirs
prisés par les froids et les verts Je suis une chambre, sueur et âme Je suis
l'exploité, l'âme envenée par l'émancipation Cette méloprée qui se lament et
suscite l'émotion Une beauté tragique aussi noble qu'authentique Je suis la
complainte rude et simple des work songs La mélodie consolatrice qui rythme les
jours sombres Une musique errant entre blasphème et quantique Je suis une
chambre, sueur et âme Je suis la crainte des fusils, des bûchers, des matraques
La foirce de liberté, du marron, que jamais on ne rattrape Je suis l'aider dans
le delta, à la croisée de routes inférieures Je suis le chemin qui mène de la
plantation au pays nittancier La lithalie rurale en exaude vers le nord
industrialisé Je suis l'émeut de la rencontre du sport du luxeux Je suis la
chambre, sueur et âme Je suis une chambre, sueur et âme Je suis la chambre,
sueur et âme Je suis la chambre, sueur et âme De vive voix Je suis la chambre,
sueur et âme Je suis la chambre, sueur et âme Je suis la chambre, sueur et âme
Je suis la chambre, sueur et âme Je suis la chambre, sueur et âme Je suis la
chambre, sueur et âme Je suis la chambre, sueur et âme La révolution est la
liberté Je suis la chambre, sueur et âme Et pourtant dans le monde, notre voix
me répond Et pourtant dans le monde, notre voix me répond Tu me diras que j'ai
tort de crier et de clamer mes quatre vérités Qu'il vaut mieux se taire ou
mentir, surtout savoir garder le sourire Tu me diras que j'ai tort de parler de
l'amour comme s'il existait Qu'il ne s'agit que d'un mirage, d'une illusion qui
n'est plus de mon âge Et pourtant dans le monde, notre voix me répond Je suis la
chambre, sueur et âme Je suis la chambre, sueur et âme Je suis la chambre, sueur
et âme Je suis la chambre, sueur et âme Je suis la chambre, sueur et âme Je suis
la chambre, sueur et âme Je suis la chambre, sueur et âme Je suis la chambre,
sueur et âme Je suis la chambre, sueur et âme Je suis la chambre, sueur et âme
Je suis la chambre, sueur et âme Je suis la chambre, sueur et âme Je suis la
chambre, sueur et âme Je suis la chambre, sueur et âme Je suis la chambre, sueur
et âme Je suis la chambre, sueur et âme Avec ma gueule de métèque, de juif et
rond, de peintre grec Et mes cheveux aux quatre vents, avec mes yeux tout
délavés Qui me donne l'air de rêver, moi qui ne rêve plus souvent Avec mes mains
de marodeurs, de musiciens et de rôdeurs Qui ont pillé tant de jardin, avec ma
bouche qui a bu, qui embrassait mordu Sans jamais assouvir sa faim Avec ma
gueule de métèque, de juif et rond, de peintre grec De voleur et de vagabond,
avec ma peau qui s'est protée Au soleil de tous les étés et tout ce qui portait
du pont Avec mon cœur qui a su faire souffrir autant qu'il a souffert Sans pour
cela perdre histoire Avec mon âme qui n'a plus la moindre chance de s'allumer
Pour éviter le purgatoire Avec ma gueule de métèque, de juif et rond, de peintre
grec Et mes cheveux aux quatre vents Je viendrai ma douce captive, mon âme sœur,
ma source vive Je viendrai boire tes peintants Et je serai prince de sang,
rêveur ou bien adolescent Comme il te plaira de choisir Et nous ferons de chaque
jour toute une éternité d'amour Et nous vivrons à mourir Et nous ferons de
chaque jour toute une éternité d'amour Et nous vivrons à mourir Et nous vivrons
à mourir Je suis communiste, à ce qui paraît Rien n'est rohi, couille-moi Je
suis communiste, vois-ça, vois-ça Ça fait pas chic Je suis communiste, vois-ça,
vois-ça La chanson engagée, laquelle je tiens Alors justement, c'est une autre
chose que vous avez en commun avec Georges Moustaki L'engagement citoyen, on
l'écoute raconter mai 68 J'ai découvert la Sorbonne, les usines, les hôtes
d'université Et je crois que je peux dater de cette époque là le moment où j'ai
senti que j'étais prêt à monter sur une grande scène et de chanter pour un grand
nombre de spectateurs après des années passées dans des cabarets confidentiels
avec un public de 20 personnes Le sénior c'était un ami qui s'appelait Bernard
Paul, tournait un film qui s'appelait Le temps de vivre, et Le temps de vivre
faisait partie des slogans qu'on pouvait lire sur les murs de Paris et de
l'écratie latin Et le film était un film très politique, très social, et il
m'avait demandé d'écrire la musique et j'ai fait une chanson qui collait de très
près aux événements que je contemplais dans la rue qui s'est appelé Le temps de
vivre, et Le temps de vivre a été une chance peut-être dans mon répertoire, la
seule chanson vraiment 68 Et sans projet et sans habitude Nous pourrons rêver
notre vie Viens, je suis là Je n'attends que toi Tout est possible Tout est
permis Viens, écoute Ces mots qui vibrent Sur les murs du mois de mai Ils nous
disent La certitude Que tout peut changer aujourd'hui Viens, je suis là Je
n'attends que toi Tout est possible Tout est permis Le temps de vivre, Cyril
Mokaiash, c'est la chanson qui donne son titre à votre album. On parlait de
l'engagement citoyen, vous avez déclaré dans une interview, mon engagement n'est
pas politique, il est humain Oui, ça va un peu avec ce que j'ai lu de Georges
Moustaki pour le citer et c'est ce qui m'a aussi donné envie de le chanter, il y
a plein de rapprochements, celui disait je n'ai jamais été ouvrier, je n'ai
jamais été pris à partie de manière agressive en tant qu'étranger ou juif,
jamais les événements m'ont visé directement mais j'éprouve le besoin de
m'exprimer politiquement sur le plan moral par solidarité envers des amis, et il
dit des amis proches ou inconnus, j'aime bien cette idée. Et écrire des chansons
c'est une soupape quand on est en colère ou indigné? Oui, c'est une des choses
de se sentir un peu utile à travers ce qu'on fait, on en parle beaucoup et on
évite le sujet en chanson, mais je trouve que c'est quelque chose qu'il faut
faire avec toutes les maladresses dont on est capable, mais le faire avec
l'essentiel de ce qu'on a envie de dire, le plus simplement aussi, pas en faire
des caisses et pas jouer les héros, les oraux, je ne sais pas quoi, mais en tout
cas pas hésiter. Avec une forme de poésie et une forme tout court, on peut
arriver à faire passer des messages qui ouvrent une voie, c'est-on jamais. Et le
format chanson est celui qui vous convient le mieux? Oui, je crois pour
l'instant en tout cas. Vous n'avez pas envie, comme Dominique A, Jeanne Scherral
ou Arthur Teboule de Feuchâtartone, d'écrire des poèmes, vous qui émettant la
poésie? Oui, j'en aurais envie un jour ou l'autre, c'est sûr, je pense que ça
viendra, ou ce sera des nouvelles peut-être, je ne sais pas. On a évoqué son
engagement citoyen, mais Mustaqui était aussi et avant tout un épicurien, un
poète, il a superbement chanté l'amour, les femmes, le plaisir, la liberté, le
bonheur de vivre, si possible au soleil du Brésil. Vous avez quel rapport avec
le Brésil? Aucun, j'ai un rapport un peu avec l'Argentine, où j'ai eu le plaisir
de passer un mois et demi et d'écrire un album qui s'appelait L'Amour qui
s'invente. Mais c'est un pays que j'ai très envie de découvrir. Pour l'instant
je ne fais que écouter Chico Buarque ou Carlos Jobim ou d'autres, mais je n'ai
jamais foutu les pieds là-bas, j'en ai bien envie, oui. Quelle est la chanson de
Mustaqui que vous auriez aimé écrire vous-même? Je crois que ma liberté dit
beaucoup de choses. La liberté dit beaucoup de choses parce que c'est un choix,
si tant est que quelqu'un soit libre réellement. La liberté c'est une vue de
l'esprit, mais c'est quand même quelque choix. C'est vrai que jusque-là, moi
j'ai toujours privilégié le fait d'essayer d'être à peu près bien partout, mais
jamais vraiment ancré nulle part. C'est peut-être aussi ce qui me permet d'être
libre, d'écrire les chansons que je veux avec le ton que je veux et de ne pas me
sentir enfermé dans une idée de... ce que je pourrais projeter qui ne me
correspondrait pas ou que je pourrais payer après. C'est vrai que je me fais
plutôt discret, vous remarquerez, on ne me voit pas partout, mais certains jours
je peux me dire que j'aimerais bien être un peu plus dans la lumière. Et puis il
y a d'autres jours où je me dis, en fait je crois qu'on a ce qu'on veut aussi.
Je crois que moi ça me va très bien aussi d'apparaître de temps en temps et puis
de dire haut et fort ce que j'ai envie de dire, un mot précis, puis après je
retourne à mes histoires. Et est-ce que le fait de vous être glissé dans la peau
de George Moustaki vous a changé? On verra plus tard, mais oui, je pense que
déjà ça m'a sans doute... vous me trouvez comment épanoui, non? Absolument,
apaisé, souriant, décontracté. On va se quitter sur un air de bossanova de ce
Brésil que George Moustaki aimait tant. Un arbre millénaire, un nœud dans le
bois, c'est un chien qui aboie, c'est un oiseau dans l'air, c'est un tronc qui
pourrit, c'est la neige qui fond, le mystère au profond, la promesse de vie.
C'est le souffle du vent au sommet des collines, c'est une vieille ruine, le
vide, le néant, c'est la pie qui jacasse, c'est la verse qui verse, des torrents
d'allégresse, ce sont les eaux de mars, c'est le pied qui avance, à basse sur, à
pas lent, c'est la main qui se tend, c'est la pierre qu'on lance, c'est un trou
dans la terre, un chemin qui chemine, un reste de racine, c'est un peu
solitaire. Les eaux de mars sur une musique d'Antonio Carlos Jobim. Cyril
Mokaias, est-ce que c'est pour vous la chanson la plus poétique de Moustaki? Oui
peut-être, la plus mystérieuse aussi, énigmatique. D'ailleurs, quand il avait
voulu l'adapter en français, il n'avait entendu qu'une succession de sonorités,
de mots qui lui paraissaient un peu obscures, et puis il a demandé la
signification des paroles à Jobim qui lui a dit simplement, écoute, tu te mets
dans la nature, au mois de mars, tout a changé, tu regardes ce qui se passe et
puis tu l'écris. C'est un point de vue mystère et puis j'aime bien cette phrase
de la promesse de vie. Je trouve que ça clôturerait bien l'album et puis c'est
joyeux comme image. Merci beaucoup Cyril Mokaias d'avoir été notre invité et de
nous avoir présenté en exclusivité quelques titres de ce nouvel album, Hommage à
Moustaki, Le Temps de Vivre, qui sort le 12 mai prochain. Vous serez le 28 juin
au Théâtre de l'Atelier à Paris pour chanter Moustaki et avant, le 3 mai, vous
partagerez la scène de l'Olympia avec une vingtaine d'autres artistes pour le
concert anniversaire des 10 ans de la disparition de notre métèque préférée. À
la réalisation de De Vivoy, il y avait aujourd'hui Guillaume Ploquin, à la
coordination Damien Roucoup et à la documentation sonore pour les archives
Débora Lepage. À bientôt et portez-vous bien. Sous-titres réalisés para la
communauté d'Amara.org