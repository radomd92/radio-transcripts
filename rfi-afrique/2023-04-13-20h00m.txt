TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-13-20H00M.MP3
--------------------------------------------------------------------------------
... On écoute du RFI 20h ici à Paris, 18h en temps universel.... Et nous sommes
jeudi 13 avril, soyez les bienvenus si vous nous rejoignez. Un club français en
quart de finale de Coupe d'Europe de football ce soir, c'est Nice. On verra tous
les détails dans 12 minutes mais pour le moment c'est l'heure du journal.
Bonsoir Stéphane Jeunesse. Bonsoir Romain, bonsoir à toutes et à tous. C'est à
la haute, nouveau rebondissement au Maroc dans l'affaire de viol à répétition
d'une fillette. Les trois accusés nient l'ensemble des accusations. L'affaire
provoque l'indignation dans le pays. On sera en ligne avec notre correspondant à
Rabat. 12ème journée de mobilisation contre la réforme des retraites en France.
La bataille des chiffres continue. Entre 380 000 personnes et 1 million selon
les sources, nous étions dans le cortège parisien. Cette question est une. Si
aussi en France le fonds Mariana a-t-il financé des contenus politiques
favorables à Emmanuel Macron, plusieurs élus réclament une enquête. Au sommaire
également le gouvernement Biden qui saisit la Cour suprême américaine pour
contester les restrictions d'accès à la pilule abortive. Quel film présenté au
Festival de Calme cette année? La sélection officielle a été dévoilée, on vous
en dit tout. Et puis rencontre dans ce journal avec le jazzman Eric Truffaz. Il
sort son 17ème album où il revisite quelques musiques de films célèbres.
Première journée du procès en appel des trois agresseurs de la petite Sana au
Maroc. Cette fillette de 11 ans violée à de nombreuses reprises. Les trois
hommes n'avaient été condamnés qu'à deux ans de prison maximum. En première
instance ce qui a provoqué l'indignation dans le pays. Bonsoir Victor Moria,
vous êtes en direct du palais du 6 de Rabat. Victor, où en est-on dans le
déroulé de ce procès? Les plaidoiries des avocats viennent de s'achever. Ça a
duré à peu près une heure. C'était le moment le plus long d'un procès qui pour
l'instant a été extrêmement rapide. Dès le début de la journée les avocats de la
partie civile avaient demandé à ce que la procédure aille vite. On connaît les
faits, ont-ils assuré aux juges? Il faut désormais appliquer la loi. Cette
rapidité peut surprendre tant ces affaires sont d'ordinaire compliquées.
Certains magistrats avaient d'ailleurs reproché au ministre de la Justice de
s'être exprimés sur l'affaire, prenant ainsi le risque de l'influencer. Face à
la Cour, les accusés ont nié toute relation avec la petite Sana malgré la
présence d'un test ADN identifiant l'un d'entre eux comme le père de l'enfant
que la petite fille a mis au monde l'année dernière. Enfin, un mot sur le
verdict qui pourrait être connu assez rapidement lui aussi. Ça pourrait être ce
soir ou alors dès la prochaine audience, c'est-à-dire la semaine prochaine.
Victor Moria à Rabat pour RFI. L'actualité en France de ce jeudi, c'est cette
12ème journée de mobilisation contre la réforme des retraites. Des
manifestations un peu partout dans le pays pour s'opposer au texte et à l'âge
légal de départ à 64 ans. Mais aussi à Emmanuel Macron. Mobilisation donc en
baisse. Aujourd'hui, on recule par rapport aux précédentes journées. Plus d'un
million de personnes réunies d'après la CGT. 380 milles selon la police.
Reportage de Pauline Gleize et de Julien Boileau dans le cortège parisien.
Denis, dessinateur. Brandine, pancarte. Côté pile. Toujours non à la réforme.
Côté face. Et ton mépris il est légitime parce que bon, il se croit tout permis
quoi. C'est très chiant. 75% des gens sont contre. Tous les syndicats sont
contre. Ils s'en foutent. Mais il provoque les gens quoi. C'est ça le problème.
C'est moi qui le méprise. Il provoque les gens. Tout ça parce qu'il est doué.
Doué et habile mais ça suffit pas quoi. Emmanuel Macron a promis aux syndicats
un échange une fois la décision du conseil constitutionnel rendue. Une démarche
qu'ils y t'engagent dans un esprit de concorde. Il en faudra plus pour
convaincre Charles, professeur d'économie. L'idée d'une concorde, il faudrait
que ça passe par un réel débat et une réelle volonté de démocratie. Un réel
dialogue. Pour autant, ce qui se passe c'est que le dialogue il n'a pas été
présent. Il a été refusé à la Sémion nationale. Il a été refusé au Sénat. De la
rue on l'écoute pas. Pendant le premier quinquennat, on a eu la destruction à
petit feu des cordes intermédiaires. Cette destruction des cordes intermédiaires
elle a lieu. Après il n'y a plus de dialogue. Un esprit de concorde, pas
franchement partagé par Nicole, qui manifeste pour changer complètement les
règles de la société. Il aille se faire voir. Les syndicats devraient décider
ensemble s'ils répondront à l'invitation d'Emmanuel Macron et surtout quand ils
y répondront. Paul Inglès, Julien Boileau pour les moyens techniques dans le
cortège parisien. Par ailleurs, c'est demain que le conseil constitutionnel rend
deux décisions très attendues. L'une sur la constitutionnalité de la réforme des
retraites, l'autre sur l'ouverture d'un référendum d'initiative partagée fixant
à 62 ans l'âge maximal de départ. On reste en France puisque plusieurs élus
veulent saisir la justice autour du Fonds Marianne. Un fonds créé par Marlène
Schiappa après l'assassinat du professeur Samuel Paty en 2021 pour soutenir les
acteurs de la lutte contre la radicalisation et le séparatisme. Sauf que voilà,
d'après le site d'information en ligne Mediapart, cet argent public aurait été
utilisé par une association pour financer du contenu politique contre les
opposants. Emmanuel Macron, principalement le Rassemblement National et la
France Insoumise. Le groupe parlementaire dirigé par Marine Le Pen demande même
l'ouverture d'une commission d'enquête pour faire la lumière sur cette affaire,
Pierrick Bono. Reconstruire le commun, c'est le nom d'une association qui aurait
bénéficié de plus de 300 000 euros de subventions publiques pour déployer un
discours républicain. Chez les jeunes, c'est écrit dans son projet. Le problème,
selon Mediapart, c'est que certaines des vidéos qu'elle publie juste avant les
législatives de 2022 s'en prennent directement aux opposants à Emmanuel Macron,
notamment celle qui est aujourd'hui députée Europe Ecologie Les Verts, Sandrine
Rousseau. Si ce que dit Mediapart est vrai, il y a eu une vidéo financée par ce
fonds contre moi en pleine campagne législative. Donc si c'est ça, vraiment, ils
vont devoir rendre des comptes. Le Rassemblement National est également dans le
viseur des contenus publiés à l'époque. Le parti de Marine Le Pen demande donc
l'ouverture d'une commission d'enquête à l'Assemblée. Le député RN Thomas
Ménager, pourtant pas habitué à reprendre à son compte les révélations de
Mediapart. Dès lors qu'il y a suspicion, on doit lever tous les doutes pour, au-
delà de Madame Schiappa, éviter ce dégoût généralisé des Français qui mènent à
l'abstention, qui mènent à la situation de crise démocratique que nous
connaissons en ce long. Avant que cette commission d'enquête ne voit le jour, il
faut encore qu'une majorité de députés se prononcent pour son ouverture. C'est
très peu probable. Le camp présidentiel ne vote pas les propositions venues du
Rassemblement National et encore moins, on l'imagine, lorsqu'elles remettent en
cause l'action d'un membre du gouvernement. F. Bono du Service Politique de RFI.
V. Dorsy, ici à Paris, vous êtes à l'écoute du journal de Stéphane Genest. La
bataille de l'avortement se poursuit aux États-Unis. La Cour Suprême va de
nouveau intervenir sur le sujet. Guy Monodin, vous êtes en ligne avec nous en
direct de Washington puisque l'administration Biden fait appel d'une décision
limitant la distribution de la pilule abortive. Oui, Stéphane, c'était fortement
pressentiste et désormais confirmé. La Cour Suprême va devoir trancher au sujet
de l'accès à l'avortement médicamenteux au niveau national. La semaine dernière,
un juge fédéral texan connu pour ses positions anti-avortement décidait
d'annuler sur l'ensemble du territoire américain la commercialisation d'une
pilule abortive utilisée dans plus de la moitié des cas. Le département de la
justice avait presque immédiatement fait appel. Les juges, eux aussi fortement
conservateurs, n'ont bloqué que partiellement la décision de leurs collègues
texans. Non, l'autorisation de commercialisation n'est pas annulée, mais des
dispositions décidées en 2016, elles le sont. Les pilules ne peuvent plus être
expédiées par la Poste ni être utilisées au-delà de sept semaines de grossesse
contre dix avant le jugement de cette nuit. C'est donc dans les faits un recul
de l'accès à l'avortement aux États-Unis. Le ministre de la Justice Merrick
Garland exprime son fort désaccord et il porte l'affaire devant la Cour Suprême.
La majorité des juges y est conservatrice et est opposée à l'avortement. Elle
l'a déjà prouvé en annulant le droit constitutionnel à l'IVG en juin dernier
pour laisser le choix aux États. Guillaume Nodin, envoyé spécial permanent de
RFI à Washington. Joe Biden, lui on en parle et qui poursuit sa visite
officielle en Irlande du Nord. Il estime d'ailleurs que Londres devrait
travailler plus étroitement avec Dublin sur le dossier de l'Irlande du Nord.
20h08, ici à Paris, on file au cinéma. Puisque les plus cinéphiles et les plus
mélomanes d'entre vous ont reconnu ce morceau, le Carnaval des animaux de
Camille Saint-Saëns, la musique officielle du Festival de Cannes et bien la
sélection officielle pour son édition 2023 a été présentée tout à l'heure. À
pratiquement un mois de l'événement, Thierry Frémot, son délégué général, a
dévoilé la liste des heureuses élues pour cette compétition Elizabeth Le Quiré.
Cette année, c'est un vrai panorama du cinéma mondial en 19 films, dont 6
réalisés par des femmes, un record, qui trouve un équilibre presque parfait
entre nouveaux venus et monuments du cinéma. Tous les continents, Europe,
Amérique et surtout Afrique, sont représentés. Parmi les vétérans, on peut citer
le britannique Ken Loach, le finlandais Akiko Rismaki, les américains Wes
Anderson et Todd Haynes, tous les regards sont braqués sur le nouveau score 16
pour l'instant hors compétition. Killers of the Flower Moon avec Robert De Niro
et Leonard DiCaprio suit une série de meurtres dans l'Amérique des années 20.
Harrison Ford viendra présenter Indiana Jones et le cadran de la destinée. Et
puis sur le tapis rouge, une autre star, Johnny Depp, qui joue Louis XV face à
Maywen dans Jeanne Dubary, le film d'ouverture. L'Afrique, elle, est très bien
représentée cette année à Cannes, avec deux films en compétition signés par de
jeunes réalisatrices, la tunisienne Kauter Ben-Anya et la sénégalaise Ramatatou
Laessi. Et ce vent d'air frais venu du continent souffle aussi sur les sections
parallèles avec deux films marocains, un film soudanais, un film de gangster
algérien et le premier long-métrage du musicien et plasticien congolais Baloggi.
Elisabeth Le Quéré qui nous fera vivre évidemment ce festival de Cannes qui se
tiendra du 16 au 27 mai. Son président du jury d'ailleurs sera le suédois Ruben
Osloon, palme d'or l'an dernier pour son film sans filtre. Et puis rencontre
pour refermer ce journal, rencontre avec l'un des musiciens de jazz les plus
brillants de sa génération, le trompettiste franco-suisse Eric Troufface. Il
vient de sortir sur le célèbre label américain Blue Note son 17e album, album
qui s'appelle Rollin. Il y révisite quelques musiques de films célèbres et
évidemment Edmond Sadaka. À écouter ce disque pour nous. Le grand public avait
découvert Eric Troufface en 1997 avec l'album The Down dans lequel il mixait un
jazz très électrique à des rythmes tout droit sortis des pistes de danse. Sa
passion des sons l'a conduit ensuite à explorer de nombreux styles, des rythmes
arabes au rock en passant par le rap et les chants souffits. Cette fois le
voyage se fait donc à travers quelques grandes musiques de films dont les
partitions sont signées notamment Miles Davis, Enyo Morricone ou encore Philippe
Sard. Eric Troufface. C'est la première fois que j'enregistre un disque avec des
musiques qu'on ne compose pas avec mon groupe et donc oui c'était délicat parce
qu'il faut déconstruire la musique et après les reconstruire pour fournir
quelque chose d'original. Le trompettiste explorateur qui a retrouvé à
l'occasion de cet album le célèbre label américain Blue Note s'est entouré
notamment sur ce disque de la comédienne Sandrine Bonner, sa compagne dans la
vie et de la chanteuse Camelia Giordana qui reprend un air interprété par
Marilyn Monroe en 1954 dans le western Rivière sans retour. Eric Troufface. Non,
Edmond Sadaka qui nous fait découvrir le dernier album d'Eric Troufface pour
RFI. 20h12 à Paris, là c'est bon, c'est Romain Osby et RFI soir à l'église. Et
vous aussi vous faites de la musique Stephane Jolasse? Pas du tout hélas. Allez
les sports. Bonsoir Christophe Dirémzian. Bonsoir. Après la Ligue des Champions,
la semaine européenne est en train de s'achever avec les quarts de finale. Allez
des deux autres compétitions continentales, c'est évidemment du football, à
commencer par la Ligue Europa. Absolument Romain. Il y a un match en cours entre
les Néerlandais de Feyenoord de Rotterdam et l'AS Rom 1-0 pour Rotterdam en
seconde période. A 19h, temps universel, coup d'envoi d'une des affiches
Manchester United FC Seville. La Jumatusturin, accueillie pour sa part le
Sporting Portugal et le Bayer Leverkusen reçoit la belle surprise de cette
compétition les Belges de l'Union Saint-Giloise. Première campagne européenne de
leur histoire. Un quart de finale, c'est pas mal du tout. Un étage encore en
dessous, les quarts de finale allées de la Ligue Europa. Conférence, dernier
espoir qu'un club français fasse quelque chose sur la scène continentale cette
saison. Nice en déplacement chez les Suisses du FC Balles, dans un contexte que
les Asuréens auraient sûrement préféré ne pas vivre. Ces accusations de
discrimination et d'islamophobie à l'encontre de Lex, entraîneur de l'OGC
Christophe Galtier, porté par Julien Fournier, le directeur du club jusqu'à la
fin de la saison dernière. Un déballage public et dans la presse qui aurait pu
perturber la préparation du match de ce soir. Mais l'actuel entraîneur nicois
Didier Degard assure qu'il n'en est rien. J'ai aucune crainte que ça perturbe le
« On est unis ». Il n'y a aucune faille entre nous, entre joueurs et staff.
C'est une armée. Sincèrement, je n'ai aucune crainte sur l'état d'esprit. Il n'y
a rien qui va nous perturber. On sait ce qu'on veut et on va tout faire pour que
ça arrive. Il n'y a personne qui nous fera penser à autre chose. Didier Degard
serein avant FC Balles Nice dans trois quarts d'heure. Deux autres duels à
suivre. Au même moment, les polonais du Lech Poznane face aux Italiens de la
Fiorentina et Anderlecht contre les Néerlandais de Lazette-Alkmaar. En ce
moment, un autre club belge, la Gontoise, face aux Anglais de West Ham. Un
partout en seconde période. En football, une autre affaire qui fait beaucoup de
bruit, Sadio Mané, qui a giflé l'un de ses coéquipiers des Bayern Munich.
C'était mardi soir. Une scène incroyable dans le vestiaire bavarois à l'issue de
la déroute à Manchester City. En quart de finale allé de la Ligue des Champions,
le sénégalais n'aurait pas apprécié des propos de son partenaire, le Roi Sané.
Les conséquences de son geste ne se sont pas faites attendre. Olivier Praud,
l'attaquant des Lyons, ne jouera pas ce week-end. Suspension pour une rencontre
face à Hoffenheim. Samedi, à sorti d'une amende dont le montant est inconnu.
Mais quand on gagne plus de 20 millions d'euros par an, cela ne devrait pas être
trop douloureux pour le Ballon d'or africain. Qui devrait donc être à
disposition de Thomas Tourelle mercredi prochain pour le quart-retour. Mardi
soir, après la lourde défaite, 3-0 chez les Citizens. Une vive altercation a eu
lieu entre le champion d'Afrique 2021 et le Roi Sané. L'international allemand
est giflé. Les deux hommes sont séparés par leurs coéquipiers. Manet reproche à
Sané de lui avoir mal parlé pendant la rencontre. Malgré les sourires affichés
et les excuses prononcées de part et d'autre ce jeudi matin à l'entraînement.
L'ambiance semble délétère dans un club qui vit une saison compliquée. À l'image
de celle du champion d'Europe 2019 avec Liverpool. Sadio Manet arrivait l'été
dernier d'Angleterre pour 32 millions. A eu du mal à s'adapter puis s'est blessé
juste avant le mondial. Depuis, il peine à retrouver niveau et place de
titulaire après quatre mois d'absence. On le cite parmi ceux qui ont souhaité
écarté Nagelsmann, l'entraîneur. Il y a trois semaines, apparemment, les choses
ne se passent pas mieux pour lui avec son successeur Thomas Tourelle. De là à
imaginer qu'il pourrait faire ses valises dès le mois de juin, il n'y a qu'un
pas. Olivier, prend le tennis, enfin, Christophe Di Remzian. La suite du
master's meal de Monte Carlo est une grosse surprise. Oui, la sensation il y a
quelques minutes sur le rocher après la pluie. L'élimination de Novak Djokovic.
Depuis les huitièmes de finale, le numéro un mondial a été écarté en 3-7, 4-6,
7-5, 6-4 par l'italien Lorenzo Musetti, membre de cette génération montante
transalpine qui comprend aussi Yannick Siner. Les deux compatriotes
s'affronteront d'ailleurs en quart demain après la qualification de Siner au
dépend du polonais Ubert Urkax. 3-6, 7-6, 6-1, c'est passé également entre
autres. Pour le double talent du titre, le grec Stefano Tsitsipas, tombeur du
chilien Nicolas Jari 6-3-6-4 et qui retrouvera l'américain Taylor Fritz, de son
côté le russe Andriy Roublev qui a sorti un autre compatriote, Karen Katchanov
7-6-6-2, affrontera l'allemand Jan Lennart Struf, victorieux de la tête de série
numéro 2, le norvégien Kasper Rud 6-1-7-6-7. Voilà qui est complé. Journal
d'Esports, signé Christophe Di Remzian. La Géorgie est-elle sous influence
russe? Souvenez-vous il y a un peu plus d'un mois un projet de loi qui avait
provoqué trois jours de manifestations en Géorgie. Le projet de loi intitulé «
Transparence de l'influence étrangère » traduisait, selon les détracteurs, une
loi russe. Alors le gouvernement avait retiré le texte mais la tension reste
forte, des manifestants sont poursuivus. On va faire le point sur la situation
avec notre correspondant Adbillissie Régis-Janté. Régis, que se passe-t-il
depuis un mois en Géorgie? On a l'impression que l'état de confrontation, verbal
du moins, entre le gouvernement, contrôlé par le parti Le Reff Géorgien de
l'Oligarque, Bitzinaïvani Julie, et une partie de la société civile continue. Le
gouvernement n'a pas de mots assez durs contre ces manifestants qui ont été très
pacifiques. Pourtant, lors des rassemblements des 7-8 mars dernier, le premier
ministre Irakli Gareba Julie dit d'eux, ce sont souvent des étudiants, qu'ils
sont satanistes et ne parlent de l'opposition qu'en termes de mouvements qui
seraient extrémistes à radicaux. La société civile et les manifestants en
général, eux, n'en démordent pas. Ils estiment que ce gouvernement est sous
influence russe, essaie de ramener encore le pays dans la sphère d'influence de
Moscou. Ils lui reprochent aussi de s'attaquer aux médias et aux ONG. Donc les
manifestants, vous l'avez dit, un qualifié de « sataniste » par le premier
ministre Géorgien. « Sataniste », c'est un terme très fort. Oui, bien sûr, il
s'agit de démoniser les voix critiques, disons, mais pas seulement. Cela a un
sens politique très fort dans le contexte politique actuel. C'est exactement le
mot « sataniste » qu'ont employé le président russe Vladimir Kouttine et son
ancien premier ministre, Dimitri Medvedev, au sujet des Occidentaux. Donc
lorsque M. Gareba Julie emploie ce mot ici, chacun y voit d'une volonté de
montrer au Kremlin une similitude de vue. Et cela a même conduit à des
poursuites judiciaires contre un manifestant en particulier de 21 ans, Lazar
Grigoriadis, qui aurait voulu lancer un cocktail molotov sur la police. Il
irrite jusqu'à 11 ans de prison. Et le jeune homme avoir été choisi justement à
cause de son look, dont parlent abondamment et négativement les dirigeants du
Parti au pouvoir. Il a les cheveux teints, se maquille les yeux, etc. Et cela
irrite beaucoup la jeunesse géorgienne qui a organisé un concert de musique
techno devant le siège du gouvernement samedi de la semaine passée. Alors 2023
est une année très importante en Géorgie, puisque l'Union européenne doit
décider ou non d'octroyer le statut de pays candidat, statut qui avait été
refusé en juin dernier. Régis Janté, comment cette échéance est-elle observée
par les deux partis, donc à la fois le gouvernement et les manifestants qui se
sont opposés au début du mois de mars? Vraiment comme historique extrêmement
importante, d'autant que les sondages montant à montrer depuis très longtemps
que 80% des 3,7 millions de Géorgiens souhaitent l'adhésion de leur pays à l'UE.
Alors le Parti au pouvoir qui, par prudence ou bien par affinité pro-russes,
veut ramener le pays dans l'orbite de Moscou est très embarrassé. Il ne cesse de
faire semblant de satisfaire aux exigences des 12 points que Bruxelles lui a
soumises en juin dernier pour lui octroyer ce statut. Quant à la société civile
et l'opposition politique, elle s'agace et demande aux Européens d'octroyer à la
Géorgie ce statut de pays candidat à l'UE, de l'octroyer au peuple géorgien et
pas à l'équipe dirigeante contrôlée par l'oligarque Bidzina-Evany-Jewili. Régis
Janté correspond de RFI à Adbilici et dans RFI Soir. 20h20 ici à Paris. On parle
de Twitter à présent. Bonsoir Dominique Desonnet. Bonsoir Romain. Et oui car
l'échéance se rapproche. Twitter a annoncé que dans une semaine, très
exactement, finit le petit macarons bleus à côté du profil Twitter. Oui, ce
fameux badge bleu était pourtant devenu pour les stars, les personnalités
politiques, les organisations gouvernementales et les instituts, ou encore les
journalistes, la preuve ultime que leur message publié sur le réseau social
était bel et bien authentique. Mais voilà, après une première tentative annoncée
pour le 1er avril, la mesure avait déclenché une véritable pagaille entre les
anciens détenteurs du petit macarons bleus et les nouveaux abonnés qui avaient
souscrit au système de vérification des comptes Twitter Blue pour environ 9
dollars par mois. L'affaire semblait s'être un peu calmée mais c'était mal
connaître Elon Musk qui aujourd'hui persistait signe en annonçant sur son compte
Twitter que le 20 avril prochain sera la date finale de suppression des
certifications Twitter classiques, du moins dans leur formulation gratuite.
Alors, car avec cette décision, Dominique, l'objectif du patron de Twitter c'est
de rentabiliser au plus vite son investissement. Il a racheté, on le rappelle,
Twitter 44 milliards de dollars. Oui, et en premier de faire payer, les
entreprises, les gouvernements et les ONG qui moyennent en finances, doivent
souscrire un abonnement à 1140 euros par mois pour leurs certifications. Et à ce
prix, les entreprises où les ONG obtiendront un magnifique badge doré pour leur
compte Twitter, ainsi qu'une belle image de profil de forme carrée. S'il s'agit
d'un gouvernement ou d'une organisation internationale, leur compte bénéficiera
alors d'un badge gris. Ce nouveau système de vérification qui était annoncé
depuis des semaines pour le 1er avril avait provoqué un mouvement de
protestation de la part de nombreuses personnalités, ONG et médias en ligne sur
Twitter qui gasouillaient en masse qu'elles ne paieraient jamais au risque
d'ailleurs de perdre leurs certifications. Mais alors pourquoi cette date du 20
avril? C'est une date qui n'a pas été choisie au hasard. Effectivement, aux
Etats-Unis, elle s'écrit et se prononce 4 sur 20, soit 420 en anglais, un nombre
qui est devenu fétiche pour les fumeurs de cannabis, qui l'inscrivent dans leurs
messages sur les réseaux sociaux pour célébrer chaque année un 20 avril, de
préférence à 4h20 de l'après-midi, la légalisation de leur drogue favorite dans
de nombreux états américains. Le choix de la date 420 décidée par le patron de
Twitter qui ne s'est jamais caché d'être un amateur d'herbe bleue et de petites
fumées est sans doute un nouveau trait d'humour de sa part, mais cette fois,
l'annonce de la mise au rebut des anciens badges bleus gratuits semble
irrévocable. En revanche, le petit macaroon bleu ne disparaîtra pas pour autant
des écrans après ce 20 avril. Il indiquera seulement que votre compte dispose
d'un abonnement actif à Twitter Blue et répond donc au critère d'éligibilité à
la certification de vos messages. Rappelle à longueur de tweets promotionnels le
réseau social. C'est bien noté Dominique de Zonnet, merci à vous, 20h23 ici à
Paris. Dans 7 minutes, vous aurez rendez-vous avec la suite de nos programmes.
Avant cela, musique, cela tient davantage de la performance que du concert.
Voici Electric Fields, un voyage du XIIe au XXIe siècle, auteur autour de
l'œuvre visionnaire de la compositrice, poétesse et abbess médiévale allemande
Hildegard de Bingen, une création française qui vient de couronner la 10e
édition du Festival de Pâques d'Aix-en-Provence. Carmen Lundspan y était pour
RFI. Ça mélangeait beaucoup d'idées du côté mystique de Hildegard de Bingen,
très influencée par le magnétisme de la Terre. Il y avait beaucoup de problèmes
de migraine qui associaient des types de chants magnétiques. Electric Fields,
c'était aussi pour nous toutes les connexions électroniques, même dans ce que je
fais dans ma musique. De cette musique méditative hypnotisante presque surielle
émerge une voix, celle de Barbara Hannigan, soprano mille facettes et au plus de
80 créations. Plus spirituelle que charnelle, Edonne vie à une grande mystique
du XIIe siècle, une religieuse hantée par des visions tout au long de sa vie et
une féministe militante avant l'heure. On est parti de la figure d'Hildegard de
Bingen parce qu'elle a participé à tellement de révolutions déjà. Elle est allée
voir le pape, elle a obtenu pour la première fois de faire construire un couvent
que pour les femmes, alors qu'avant les femmes étaient vraiment les servantes
des prêtres, laver, repasser, faire la cuisine. Elle a écrit un livre sur les
plantes qui fait encore référence aujourd'hui. Elle a inventé une langue,
Linguas Inyotas. C'était secret, seulement parlait entre Hildegard et Seynon.
Parce que d'avoir leur propre langue, ça leur donnait un pouvoir et ça dit aussi
à quel point elles étaient proches les unes des autres. Deux compositrices du
XVIIe siècle complètent le tableau. Les italiennes Francesca Caccini, première
femme à composer des opéras, et Barbara Strozzi, 125 œuvres et 4 enfants, sont
jamais se mariés pour autant. Déjà à leur époque, elles étaient indépendantes,
elles ont vraiment eu un statut de compositeurs. De travailler avec des
compositrices, c'est toujours un rappel parce que malheureusement il y en a
peut-être encore besoin, que des femmes ont contribué à l'histoire de la musique
depuis toujours. Ça leur donnait beaucoup de liberté, c'est vraiment des sources
d'inspiration furtives. On a repiqué une petite phrase, un bout de mélodie ou un
texte pour redonner une troisième dimension, pour les emmener dans des espaces
inédits. Ça finit avec une pièce magnifique de Barbara, une très très belle
mélodie, très pure, très simple. Et peut-être que c'est ça, la chose ultime en
musique, c'est arriver à dire un maximum de choses avec très peu de notes.
Voilà, Electric Fields, en création française, au festival de Pâques d'Aix-en-
Provence dans le sud de la France. Et demain vendredi à la Philharmonie de
l'Elbe à Hambourg en Allemagne du Nord. 20h26, ici à Paris, en raison d'un mot
d'ordre de grève nationale RFI, n'est pas en mesure d'assurer son programme
habituel. Vous veuillez nous en excuser. Taki ton kami taki ton, Puli ton kami
t'uri ton Taki ton kami taki ton, Puli ton kami t'uri ton Taki ton kami taki
ton, Puli ton kami t'uri ton Taki ton kami taki ton, Puli ton kami t'uri ton Un
cospèle pour madame, autant que faire se peut, un service pour son âme. Juste un
signe du... Littérature sans frontières, Catherine Fruchon-Toussaint. Écrivain
suisse de langue allemande, Peterstam a un univers littéraire bien particulier,
tout en nuances et en mélancolie. Ainsi en est-il de son nouveau roman intitulé
Les archives des sentiments, l'histoire d'un homme seul, documentaliste qui ne
vit que par le passé et le papier et qui pourtant un jour s'éveille au monde.
Littérature sans frontières, Catherine Fruchon-Toussaint. Vendredi à partir de
13h30 TU. RFI. Une semaine d'actualité, Pierre-Edouard Eldic. C'est en compagnie
de l'écrivain Elgas qui vient d'écrire un essai qui s'intitule Les bons
ressentiments, essai sur le malaise post-colonial, que nous allons revivre 7
jours d'actualité grâce au reportage de la rédaction. Comme chaque samedi dans
le monde et en France, nous attendrons vos messages. Une semaine d'actualité,
Pierre-Edouard Eldic. Samedi à partir de 9h10 TU. RFI. Le bon ressentiment,
Pierre-Edouard Eldic. Le bon ressentiment, Pierre-Edouard Eldic. Samedi à partir
de 9h30 TU. RFI. Le bon ressentiment, Pierre-Edouard Eldic. Samedi à partir de
9h30 TU. RFI. Merci de votre compréhension. Merci de votre compréhension. Merci
de votre compréhension. Merci de votre compréhension. Merci de votre
compréhension. Merci de votre compréhension. Merci de vos commentaires. Merci de
vos commentaires. Merci de vos commentaires. Merci de vos commentaires. Merci de
vos commentaires. Merci de vos commentaires. Merci. Il est le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le le le le le le le le le le le le le le le le le
le le le le le le le le le le le