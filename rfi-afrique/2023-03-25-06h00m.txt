TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-03-25-06H00M.MP3
--------------------------------------------------------------------------------
... Merci d'écouter RFI. Il est 6h à Paris, 5h en temps universel.... Et on
démarre avec un journal signé Léa Boutin-Rivière. Bonjour Léa. Bonjour Gaëtan,
bonjour à toutes et à tous. À la une, la France épinglée par le Conseil de
l'Europe pour les violences contre les manifestants. Des arrestations
potentiellement à justifier un usage parfois excessif de la force Après les
rassemblements mouvementés de jeudi et la réaction sévère des forces de l'ordre,
l'Instance réagit, une première depuis 2019 pour Paris. Retour à la liberté pour
Paul Roussey-Sabagina, le farouche opposant Rwanda a été libéré hier après un an
et demi d'incarcération lié à une condamnation pour terrorisme finalement
commuée. L'affiche n'a rien d'exceptionnelle et pourtant, cet après-midi, en
éliminatoire de l'euro 2024, la Biélorussie affronte la Suisse en Serbie,
considérée comme terrain neutre par l'UFA. Or Belgrade ne cache pas ses
acquaintances avec la Russie et ne reconnaît pas le Kosovo dont sont issus de
nombreux joueurs suisses. Après les manifestations de jeudi en France contre la
réforme des retraites, les syndicats dénoncent la violence des forces de
l'ordre. À Paris, un militant de l'organisation Sudray a été grièvement blessé à
l'œil gauche dont il a perdu l'usage d'après le syndicat. Ailleurs, dans le
pays, l'inter-syndicale affirme avoir été ciblé par la police, notamment à
Rennes, en Bretagne, et ces protestataires ne sont pas les seuls à pointer le
rôle des autorités. Ils ont été appuyés hier par le commissaire au droit de
l'homme du Conseil de l'Europe. L'instance contredit le ministère de
l'Intérieur. Participer à une manifestation non déclarée ne justifie ni sanction
ni atteinte au droit de réunion. La justice française avait d'ailleurs elle-même
tranché en ce sens l'été dernier. Pierre Bénazel a prise de position européenne
et d'autant plus remarqué qu'elle est quasi exceptionnelle. D'habitude, quand le
Conseil de l'Europe s'alarme de la situation des droits de l'homme dans un des
46 pays membres de l'organisation, c'est plutôt pour les pays de l'Est du
continent européen ou alors de la Russie avant son exclusion il y a un an. La
dernière fois que le Conseil de l'Europe s'est inquiété de la situation en
France remonte à quatre ans, février 2019, au sujet du maintien de l'ordre face
au mouvement des Gilets jaunes. La commissaire au droit de l'homme, Dunja
Mijatovic, demande à la France de protéger la liberté de réunion et la liberté
d'expression contre toute forme de violence. Les autorités doivent faire en
sorte que ces libertés soient respectées et protégées manifestants pacifiques et
journalistes contre les débordements et contre les violences policières. Rien ne
justifie l'usage excessif de la force, affirme Dunja Mijatovic, qui souligne par
ailleurs que la libération sans poursuite de nombreuses personnes remette en
question la nécessité et la proportionalité de leur interpellation. Pierre
Benazel, Bruxelles, RFI. Autre réaction assez inattendue, celle de l'Iran qui
condamne la répression des rassemblements athéerants dont les autorités ont
étouffé dans le sang les manifestations de l'automne dernier, appelle le
gouvernement français à respecter les droits de l'homme et à s'abstenir de
recourir à la force. Mais la tension est assez forte en tout cas en France pour
provoquer le report de la visite du roi Charles III. Le monarque britannique
était attendu dès aujourd'hui et jusqu'à mercredi pour une visite de trois
jours, sa première visite d'État. Finalement, ce sera pour plus tard, annonce
hier de l'Élysée, qui témoigne de l'importance des enjeux liés à la contestation
selon la députée Europe Ecologie, écologie les Verts et membre de la NuPES,
Sandrine Rousseau. C'est le premier signe qu'envoie Macron que la situation est
grave en fait. Et c'est incroyable, après des semaines et des semaines de
conflits sociales, que le premier geste, ce soit le décalage de la visite du roi
Charles III. Mais je m'en réjouis parce que vraiment accueillir Charles III qui
descendrait les Champs-Élysées pour arriver à Versailles et voir Macron partager
une coupe de champagne dans la galerie des Glaces, alors que les gens sont dans
la rue, qu'ils ont peur pour leur santé, pour leur pouvoir d'achat, enfin
vraiment c'était un décalage total. Il viendra en France après le conflit
social, pas de problème. Mais pas maintenant, pas comme ça. Emmanuel Macron la
doit être à l'écoute des Français et des Françaises. Le roi Charles III, ça
passe après, désolé de le dire comme ça, mais c'est... Là, la priorité c'est les
Français et les Français, il est président de la République française. Et là, on
a une crise sociale qui est en train de tourner en crise démocratique, voire en
crise de régime, il a absolument besoin d'être là. Propos recueillis par
Charlotte Urrient-Omakar. Et je rappelle que le président Emmanuel Macron s'est
dit hier à la disposition des syndicats pour discuter des questions liées au
travail, sans pour autant remettre en cause sa réforme. La société civile, elle,
a déjà prévu une nouvelle journée de mobilisation, ce sera mardi. Une visite
maintenue, en revanche celle de Joe Biden au Canada, et cette fois, elle est à
grand soleil sur les relations entre Washington et Ottawa. Le président
américain Joe Biden était hier chez son voisin, l'occasion d'une rencontre avec
le premier ministre Justin Trudeau, qui a qualifié Washington de « grand ami ».
Les États-Unis ont reçu un tonnerre d'applaudissements lorsque Joe Biden a
évoqué l'engagement conjoint de leur pays dans l'appui à l'Ukraine. Et puis
cette rencontre et la conférence qui a suivi ont aussi été l'occasion pour le
chef d'État américain de confirmer les frappes d'hier matin en Syrie qui ont
fait 14 morts au sein d'un groupe pro-iraniens. En réaction, selon le Pentagone,
à une attaque de drone iranien qui a tué un Américain et en a blessé plusieurs
autres, on écoute Joe Biden. Les forces militaires des États-Unis ont mené une
série de frappes aériennes en Syrie, visant les responsables de l'attaque contre
les nôtres. Mon cœur et mes plus profondes condoléances vont vers la famille de
l'Américain que nous avons perdu, et je souhaite le rétablissement rapide des
blessés. Mais je suis reconnaissant du professionnalisme de nos soldats qui ont
si efficacement mené cette réponse. Et ne vous y trompez pas, les États-Unis ne
mettent pas l'accent ou ne cherchent pas le conflit avec l'Iran, mais ils sont
prêts à frapper fortement pour protéger les nôtres. C'est exactement ce qu'il
s'est passé. Nous allons continuer nos efforts pour lutter contre les menaces
terroristes dans la région, en partenariat avec le Canada et les membres de la
coalition pour battre le i. Et cette coopération américano-canadienne devrait
aller plus loin, avec notamment un accord concernant l'immigration, objectif à
décourager les traversées illégales de la frontière, en forte hausse l'an
dernier. Ottawa s'est engagé à accueillir de manière régulière 15 000 migrants
de plus venus de tout le continent américain. Plus discrètement, le texte
prévoit de fermer le chemin Roxham, une traversée de fortune par laquelle 40 000
personnes ont franchi la frontière l'an dernier. De quoi inquiéter les ONG qui
craignent de voir des migrants tenter des traversées plus dangereuses encore. Un
an et demi après sa condamnation, Paul Rousset-Sabagina est de nouveau un homme
libre. Ce farouche opposant rwandais, notamment connu pour avoir sauvé des
centaines de personnes au cours du génocide de 1994, a quitté la prison où il
était enfermé hier en fin de journée, d'après un responsable américain. Il
séjourne actuellement auprès de l'ambassade du Qatar et doit retourner aux
Etats-Unis où il était un résident permanent depuis plusieurs années. Sa peine a
été commuée hier par Kigali Magali-Lagrange. C'est le fruit de plusieurs
semaines de discussion. La peine de prison de Paul Rousset-Sabagina est commuée
sur ordre présidentiel après examen de sa requête de Clémence, annonce le
gouvernement rwandais. En tout, 19 personnes condamnées pour terrorisme sont
concernées par ces commutations de peine. Paul Rousset-Sabagina a été jugé en
2021 pour neuf chefs d'accusation, dont celui de terrorisme, pour des attaques
menées par le FLN. L'opposant a toujours nié toute implication. Dans une lettre
datée d'octobre dernier, Paul Rousset-Sabagina promettait, s'il était libéré, de
retourner vivre avec sa famille aux Etats-Unis, pays où il avait le statut de
résident permanent, avant son arrestation en 2020. Cette affaire a longtemps été
source de contentieux entre Kigali et Washington, les Etats-Unis estimant que
Paul Rousset-Sabagina était injustement détenu par la justice rwandaise. Le
Rwanda évoquait hier le rôle constructif du gouvernement américain pour mettre
en place les conditions du dialogue dans cette affaire, ainsi que la
facilitation du Qatar. Washington exprime de son côté sa reconnaissance au
Rwanda pour cette libération. Précision Magali Lagrange. L'Algérie et la France
tournent le dos à leur brouille diplomatique. Hier, Emmanuel Macron et son
homologue Abdel-Majid Teboun se sont entretenus, et d'après l'Élysée, les deux
pays sont résolus à renforcer leur coopération, entravés début février par
l'arrivée en France de la militante algérienne Amira Bourawi. Retour en France,
où le parti d'extrême droite, le Rassemblement National, est gêné en
entournures, puisque la formation vient de prononcer un blâme contre le député
Joris Ebrard. Et pour cause, ce dernier a inauguré dans sa commune une mosquée
financée partiellement par la Turquie, B.Bono. La photo a été largement
partagée, commentée, critiquée par les membres de Reconquête, le parti d'Éric
Zemmour. Joris Ebrard, tout sourire, posant devant un immense drapeau turc en
marge de l'inauguration du Centre culturel et cultuel du Pont-Ais. La toute
nouvelle mosquée d'une association franco-turque. Mercredi à l'Assemblée, Marine
Le Pen est interrogée sur cette initiative. C'est une initiative personnelle de
ce député que je désapprouve, très clairement. Faut-il sanctionner le député?
Visiblement un peu gêné par la question, la patronne du groupe RN tente un trait
d'humour. Vous savez, c'est un peu comme avec le fisc, il y a le droit à
l'erreur. Mais c'est une fois, je crois. Mais ça n'a pas du tout fait rire.
Jordan Bardella raconte un proche du président du parti. Après avoir entendu
Joris Ebrard, le bureau exécutif dont fait partie Marine Le Pen prononce un
simple blâme à l'encontre de l'élu et diffuse un simple communiqué pour rappeler
la ligne du parti, la lutte intransigeante contre toute forme de communautarisme
et le rejet des influences étrangères, mais pas d'image, pas de son, le RN tente
d'enterrer au plus vite une affaire bien embarrassante. Précision, Pierrick Bono
du service politique de RFI. Allez, on passe au sport, les A. La France qui
entame du bon pied, les éliminatoires pour l'euro 2024. Oui, oui, les Bleus se
sont imposés hier face aux Pays-Bas 4-0 avec un beau doublé de Kylian Mbappé
tout juste nommé capitaine. On en reparlera bien évidemment dans la matinale. Et
puis dans la journée, un autre match, celui entre la Biélorussie et la Suisse.
Derrière une affiche somme toute banale, cette rencontre cache de lourds enjeux
géopolitiques. Et car depuis le début de la guerre en Ukraine, la Biélorussie a
été écartée du sport international, mais pas son équipe de foot. Le match se
joue en terrain considéré comme neutre, la Serbie, et ce n'est pas vraiment du
goût des joueurs suisses, Jérémy Lanch. Les protestations des parlementaires
européens n'y ont rien fait. Jusqu'à ce jour, l'UFA n'a pas exclu la sélection
biélorusse des compétitions internationales. Seule sanction, l'obligation de
jouer sur terrain neutre, d'où le choix de la très pro russe Serbie pour le
match d'aujourd'hui. Problème, la Suisse redoute toujours d'affronter la Serbie
ou de s'y déplacer, parce que plusieurs de ses joueurs emblématiques sont
d'origine kosovare et que la Serbie ne reconnaît pas le Kosovo. Cela donne à
chaque fois des rencontres électriques. À la Coupe du Monde 2018, les Suisses
avaient par exemple mimé l'aigle bicéphale albanais pour fêter leur but. En
novembre, au Qatar, les Serbes avaient ressorti un drapeau niant l'existence du
Kosovo dans le vestiaire, avant que le milieu de terrain suisse Granitschaka ne
réponde en rendant un hommage déguisé à un dirigeant de l'armée de libération du
Kosovo. Pour se prémunir d'une nouvelle polémique, la Suisse a officiellement
demandé que le match contre la Biélorussie se joue ailleurs qu'en Serbie, mais
l'UFA a estimé que rien n'empêche la rencontre d'avoir lieu à Belgrade. Jérémy
Lanche, Genève et Refi. Biélorussie-Suisse, ce sera donc une rencontre à suivre
à 15h en temps universel. Et c'était le journal de Léa Boutin-River. On se dit à
tout à l'heure Léa, à 8h pour un nouveau journal. Ravie de vous retrouver pour
une nouvelle édition de RFI matin. Merci d'être fidèle au rendez-vous. On est
ensemble pendant 3h pour faire le tour de l'actualité dans le monde et de ses
alentours. L'actualité en France, c'est la possibilité de nouvelles
manifestations contre la réforme des retraites ce week-end. L'inter-syndical a
donné rendez-vous mardi pour une nouvelle journée de grève nationale. Mais les
détracteurs du texte pourraient bien d'ici là organiser de nouveaux
rassemblements spontanés. Les précédents ont été émaillés de violences,
notamment de la part des forces de l'ordre déployées en marge des cortèges. Mais
ça inquiète, d'ailleurs le Conseil de l'Europe en aura l'occasion d'y revenir.
En tout cas, dans ce contexte, le roi Charles III, qui devait arriver en France
demain, reporte sa visite. L'émissaire de l'ONU en Libye, Abdoulaye Abatili, est
l'invité de RFI à France 24 ce matin. Il nous le redira, malgré le blocage des
deux assemblées législatives libyennes, il veut aller aux élections. Et c'est
selon lui un avis que partagent la plupart des acteurs libyens. A réalité, il y
a que quelques responsables en position institutionnelle qui ne veulent pas des
élections, ou tout au moins qui traînent les pieds. Et il faut créer les
conditions pour que la question électorale ne reste pas entre les mains de cette
minorité qui bloque. Entretien à écouter après le journal de 6h30
T<|ko|><|transcribe|> On reviendra aussi ce matin sur la fin de 711 jours de
détention. A notre confrère Olivier Dubois est de retour en France, il avait été
kidnappé le 8 avril 2021 alors qu'il était en reportage à Gaô dans le nord du
Mali. Morgan Le Cam du Monde Afrique a enquêté sur les coulisses de cette
libération et a découvert que le rôle du Niger s'était notamment avéré décisif.
Le président Mohamed Bazoum se serait personnellement investi pour faire libérer
Olivier Dubois. Et quand il a été libéré, le journaliste a d'ailleurs rendu
hommage, et ce sont ses mots, au Niger et à son savoir-faire dans ce type de
mission délicate. Ce sera la une du monde Afrique avec Sidy Hansané.
L'intelligence artificielle tire à balles réelles. Ça se passe à Ebronville,
Palestinienne en Cisjordanie occupée. Les palestiniens qui passent tous les
jours dans sa ligne de mire ne sont pas franchement rassurés. Reportage à Ebron
de Sami Boukhalifa. Tourniqués puis détecteurs de métaux, comme tous les matins
pour passer d'un quartier à l'autre, Mohamed traverse le checkpoint de l'armée
israélienne à Ebron. C'est ici dans l'un des nombreux points de contrôle de la
ville que les forces d'occupation ont mis en place le smart shooter. On ignore
comment fonctionne exactement cette nouvelle arme qu'ils ont installée. On sait
seulement qu'elle peut tirer toute seule. Rien n'a vraiment changé depuis son
installation, mais Dieu sait ce qu'ils testeront encore sur nous à l'avenir.
Selon l'armée israélienne, le smart shooter tire uniquement des ballons
caoutchouc et des grenades de gaz lacrymogène. Monzer, 41 ans, passe également
tous les jours avec sa famille par ce checkpoint. Les Israéliens n'ont pas
besoin d'une arme intelligente pour se débarrasser de nous. Ici, c'est simple.
Les Palestiniens n'ont aucun droit. C'est votre pièce d'identité qui dit si vous
avez le droit de vivre ou de mourir. Lorsqu'un Palestiniens passe un checkpoint,
il doit faire attention à ses gestes. S'il a les mains dans ses poches, il peut
se faire tirer dessus. Le soldat pourra toujours justifier son acte. Il dira
qu'il vous a soupçonné de vouloir mener une attaque au couteau. Et si finalement
il ne trouve aucun couteau, il en posera un à côté de vous et ce sera une preuve
suffisante. À ses côtés, sa femme Latifa n'est pas du tout rassurée. Moi j'ai
vraiment peur quand je passe par ici. Je retiens mon souffle. Lorsque je
m'éloigne du checkpoint, c'est un soulagement. Je sais bien qu'on mourra tous un
jour mais je préfère une mort naturelle. Dans le quartier Rommel, un commerçant
est indigné. Tous les gouvernements israéliens qui se succèdent depuis 1948 sont
des gouvernements extrémistes. Ils sont le visage de l'occupation. Qu'ils soient
de gauche, du centre, de droite ou de l'extrême droite, ils sont tous pareils.
Le Likud, les travaillistes, ce sont tous les mêmes. Même les partis de la
gauche israélienne, comme le Marets ou le Parti communiste, ils appliquent tous
la même politique. Ce sont des racistes qui représentent une force d'occupation.
Selon lui, cette arme incarne la déshumanisation des Palestiniens par Israël.
L'armée israélienne de son côté indique que le Smart Shooter a fait l'objet d'un
examen complet sur le plan technique et juridique. Mais à ce stade, il n'est pas
utilisé de manière opérationnelle. Samy Bourquifa de Retour des Brons,
Jérusalem, RFI. Un reportage à retrouver sur notre site internet rfi.fr On part
en Chine maintenant pour Le Monde en question. La Chine a tout juste réélu pour
son troisième mandat à la tête du Parti communiste et donc du pays. Xi Jinping
multiplie les offensives diplomatiques à travers le Monde. On en parle avec vous
Cyril Payen, alors est-ce que ces offensives diplomatiques tous azimuts, ça veut
dire que le Monde entre dans une nouvelle ère géopolitique? L'image est forte et
va bien au-delà de l'opération de communication diplomatique. Vladimir Poutine,
qui raccompagne son homologue chinois à sa voiture au terme d'une visite d'État
dans la capitale russe, et Xi Jinping qui lance en lui serrant la main ce que
nous venons d'initier ensemble sont les transformations les plus novatrices
depuis un siècle. Comprènent qui pourra, mais cette sortie ne pouvait faire plus
plaisir à son autrice, isolée sur la scène internationale et visée quelques
jours plus tôt par un mandat d'arrêt pour crimes de guerre dans la déportation
d'enfants ukrainiens. A Vladimir Poutine aux anges, qui s'est même avoué, je le
cite, un peu « jaloux » des succès de son nouveau parrain chinois. Très
clairement, Xi Jinping n'est plus dans la neutralité, ambiguë et équivoque,
affiché depuis le début de la guerre en Ukraine, outre la livraison d'armes,
dont des drones armés dernier cri, Pékin a offert son soutien et son amitié
durable à son partenaire russe, quitte à mettre une partie du monde dans
l'embarras. Washington au premier chef, déstabilisé par l'impressionnante montée
en puissance de la diplomatie chinoise, assortie d'un discours quasi
civilisationnel, fustigeant ouvertement l'hégémonie occidentale. Oui, et
d'autant que si Xi Jinping ne s'embarrasse plus avec les fausses sangs blancs,
il connaît quand même quelques succès. Alors que le chef de la diplomatie
chinoise, Wang Yi, multipliait depuis plusieurs mois les voyages à travers la
planète, l'annonce surprise du rétablissement des relations le 10 mars dernier
entre l'Iran et l'Arabie saoudite grâce à la médiation de Pékin, a permis de
confortablement installer Xi Jinping sur le devant de la scène internationale,
tout en offrant une bien opportune porte de sortie à l'Iran sous sanctions et
autorisant quelques espoirs aux Yémen, en Syrie ou au Liban, prisonniers depuis
tant d'années de la guerre par procuration que se livrent les deux puissances du
Moyen-Orient. Mais c'est aussi et surtout un formidable pied de nez diplomatique
à Washington, battu dans son propre précaré moyen-oriental, la Chine y sent
l'étendard d'une paix inédite dans un nouvel ordre mondial mené par le Pékin et
par les pays du Sud. Une démondialisation à la chinoise ou encore une
désoccidentalisation des relations internationales. Au-delà de la tentation de
la nouvelle voie chinoise, beaucoup d'experts questionnent la diplomatie
d'opportunité au coup par coup de Pékin, qui balait dangereusement toute forme
de multilatéralisme au profit de sa propre montée en puissance et celle, bien
entendu, constitutionnellement illimitée en Chine, rappelons-le, du président Xi
Jinping. Et si certains pays comme la Russie, on l'a vu, acceptent une forme de
vassalisation, disons, de Pékin, beaucoup d'autres hésitent, Cyril. Sur sa
lancée russe, le président chinois a formellement convié les dirigeants de
plusieurs anciennes nations de la sphère soviétique au premier sommet Chine-Asie
centrale en mai à Pékin. Afrique, Proche-Orient, Asie et même Amérique du Sud,
les Européens sont quant à eux contraints à une périlleuse diplomatie de la
ligne de crête dans cette guerre d'influence que se livrent Washington et Pékin,
sur fond de dépendances énergétiques, d'alliances stratégiques et de
bouleversements mondiaux. Aussi la position inconfortable du président français,
Manuel Macron, qui doit se rendre en visite officielle dans la capitale chinoise
début avril. Succédant au chancelier allemand en novembre, qui, en dépit des
critiques à peine voilée, avait répondu que Pékin était tout simplement
incontournable. La Chine, fesseuse de paix, ce n'est pas exactement la vie
dominante à Taiwan. Quant au président ukrainien Volodymyr Zelensky, il a
annoncé mardi dernier avoir proposé à Pékin de devenir un partenaire pour le
règlement du conflit. Cette fois, Xi Jinping n'a pas répondu et n'a pas tendu la
main. C'était le Monde en question. Merci Cyril Payen. 6h21 à Paris. Cette fois,
ça se passe dans les Deux-Sèvres, département du centre-ouest du pays où près de
10 000 personnes sont attendues aujourd'hui aux abords de la commune de Saint-
Soline pour manifester contre les bassines, ces réservoirs dédiés à l'irrigation
agricole dénoncées par les organisations écologistes. Leur rassemblement a été
interdit par la préfecture et 3 200 gendarmes et policiers ont été déployés sur
place. Les Nations Unies se disent préoccupés profondément par l'exécution
sommaire de prisonniers de guerre depuis le début de l'invasion de l'Ukraine,
tant du côté ukrainien que du côté russe. 15 prisonniers ukrainiens auraient
ainsi été exécutés, dont 11 par le groupe de paramilitaires russe Wagner. Le
sommet européen à Bruxelles s'est terminé. Christine Lagarde, présidente de la
Banque Centrale Européenne, a réaffirmé sa confiance en la solidité du système
bancaire de la zone euro auprès des dirigeants de l'Union Européenne, alors que
la crainte d'une crise financière fait encore chuter les marchés, notamment hier
en clôture. Et puis le Maroc face au Brésil ce soir, match amical de football à
Tangier. Le coup d'envoi ce sera à 22h en temps universel. Emmanuel Macron a
disposition des syndicats pour discuter des questions liées au travail. Il l'a
dit hier depuis Bruxelles où il assistait à un conseil européen. Mais pas
question pour autant de mettre la réforme des retraites sur pause pendant 6
mois, comme lui demandait le patron de la CFDT Laurent Berger. Pas de grande
évolution donc et les manifestations devraient continuer je le disais, un
prochain jour de grève nationale mardi avec d'ici là deux possibles
rassemblements spontanés. C'est le cas d'ailleurs depuis que l'article 49.3 a
été déclenché pour faire adopter la réforme sans passer par un vote final à
l'Assemblée nationale. Le gouvernement craignait que le texte ne soit rejeté.
Cette semaine déjà le chef de l'Etat s'était exprimé à la télévision appelant à
nouveau à l'élargissement de son camp politique afin de pouvoir trouver des
majorités à l'Assemblée. Un discours surtout adressé à la droite. Bonjour
Aurélien de Verneuil. Bonjour. Alors le chef de l'Etat garde quand même sa porte
ouverte à la gauche mais sans grande conviction. Oui tout simplement parce qu'il
n'y a plus grand monde à aller chercher. Le camp présidentiel est allé sonder le
banc et l'arrière-ban chez nous lors des élections l'an passé, rappelle un cadre
du Parti socialiste et personne ne la rejoint. Aucune chance que cela change
dans le contexte actuel. L'immense impopularité de la réforme des retraites
refroidit en effet ce qui pourrait être tenté par un marocain ministériel ou par
la perspective de faire passer des projets de loi à l'Assemblée. La gauche
réfractaire à la nuppesse garde ainsi prudemment ses distances. Nous ne sommes
pas intéressés à rejoindre un projet politique de droite, 5 lunes proches de
Carole Delga, qui note toutefois que le ton de l'Élysée s'est réchauffé
récemment envers la présidente socialiste des régions de France. Une preuve de
faiblesse plutôt que d'ouverture car selon elle Emmanuel Macron est aux abois.
Oui car plutôt que l'élargissement Aurélien c'est la menace d'un rétrécissement
qui se profile. Et tout d'abord dans l'électorat du chef de l'Etat. Selon un
sondage Elam BFM TV, un électeur sur cinq d'Emmanuel Macron lors du premier tour
de la présidentielle regrette désormais son choix. Ce sont des français de
centre-gauche qui l'avaient conquis en 2017 jugeant député de la nuppesse. Et la
perspective de ramener au bercail ses électeurs attise les convoitises,
notamment celle de Carole Delga ainsi que de l'ancien premier ministre
socialiste Bernard Cazeneuve. Tous deux sont convaincus qu'une victoire de la
gauche à la présidentielle de 2027 passera par une offre politique plus
centriste que la nuppesse, qu'il se verrait bien incarnée. Mais la nuppesse
aussi on l'orne sur les déçus du macronisme. Les législatives 2022 ont déjà été
l'occasion de quelques prises de guerre, comme les ex-députés La République en
marche Aurélien Taché et Hubert-Julien Laferrière venus gonfler les rangs
écologistes. Le parti socialiste vient aussi d'intégrer dans sa direction une
ancienne figure de l'aile gauche de la Macronie, Emilie Carriou. Les retours
sont toutefois rares mais la porte reste ouverte car du PS au vert en passant
par LFI c'est une évidence. L'appel à l'élargissement d'Emmanuel Macron était de
pure forme pour entretenir l'illusion que son projet politique dispose encore
d'une jambe gauche. A merci Aurélien De Vernois du service politique de RFI. Il
y avait la Saudi Airlines, et bien il y aura maintenant en plus Riyadh Air. À
grand renfort de publicité pour ses appareils présentés comme confortables et
ultra modernes, l'Arabie Saoudite lance une seconde compagnie nationale, premier
vol annoncé pour 2025. Riyadh Air, qui ambitionne de concurrencer les compagnies
des voisins du golfe Persique, classés parmi les meilleurs du monde. Mais avec
ses 100 connexions prévues sur la planète, Riyadh Air incarne avant tout
l'ambitieuse ouverture du royaume. C'est la chronique transport avec Marina
Melsarek. Du mauve partout. Les sièges, les tenues des hôtesse, des stewardes,
les billets seront mauves aussi. Ce n'est pas un azhar. Ce mauve évoquera les
champs de la vente de l'Arabie Saoudite. Une particularité saoudienne plutôt
méconnue du grand public. Terminée donc, l'ère du tourisme 100% religieux place
à l'ouverture, au loisir, aux familles. Et comme l'explique Sylvain Bosque,
directeur d'Avico, une société de service aérien, la compagnie Riyadh Air
proposera de l'alcool à bord et n'aura pas de moment réservé à la prière.
L'Arabie se donne 7 ans pour attirer plus de touristes que ses voisins. Par
rapport à ses voisins, l'Arabie Saoudite est déjà un géant du tourisme. La
Mecque et Médine, où est enterrée le prophète, aujourd'hui il s'agit de
diversifier ce tourisme pour y rajouter un tourisme d'affaires évidemment, mais
aussi un tourisme culturel avec des sites archéologiques et aussi à multiplier
les interactions entre la société saoudienne et les touristes étrangers.
Changement de compagnie, changement d'aéroport et même grand changement dans les
infrastructures du pays. Le plus grand aéroport de Riyadh sera agrandi. Il ne
transportera pas que des passagers, des marchandises aussi. Une plaque centrale
du transil de marchandises entre Europe, Asie et Afrique. De nouveaux trains et
des rails sont déjà annoncés, mais ce pari sur le train est peut-être un peu
risqué, comme l'explique Astrid Latapi, l'auteur touristique du guide Petit
Futé. Les Saoudiens traditionnellement se déplacent en voiture ou en avion. Ils
ne sont pas encore très habitués aux trains. Et c'est bien dommage parce que
leurs trains sont très confortables. Ce n'est pas très cher. Des lignes assez
récentes, la seule chose c'est qu'il faut prévoir un pull parce que c'est
fortement climatisé. Olivier Dalage vient de publier le livre Géopolitique de
l'Arabie Saoudite aux éditions Complexe. La principale caractéristique du prince
Moab el-Salmane, c'est son volontarisme. Il veut changer l'Arabie Saoudite. Ce
n'est plus l'Arabie Saoudite de papa ou de grand-papa. Ses projets les plus
spectaculaires, c'est la nouvelle ville de Néum sur la mer rouge. Et puis il y a
le projet touristique d'Al-Ulaq qui veut faire explorer le passé anti-islamique
de l'Arabie Saoudite. C'était encore un tabou il y a quelques années avec le
poids de l'établissement religieux. Le prince Moab el-Salmane en a cure. Et ça
apporte également, en tout cas c'est son projet, des ressources économiques,
même si pour le moment on voit surtout les dépenses. 200 000 nouveaux emplois.
C'est la promesse du ministre des transports devant les images des nouveaux
appareils. Des Elzar ont dit comme un aigle déployer la nouvelle Riyadh Air.
C'était la chronique transport avec Marina Mielzarek. Des chroniques, il y en a
plein d'autres dans RFI matin. L'en reste avec nous dans une minute. Le journal
consacré à l'actualité africana. A tout de suite. Une semaine d'actualité.
Pierre-Edouard Eldic. Journaliste spécialiste de l'Afrique des questions du
Sahel, notamment Sedi Kaba est un des invités de l'émission que vous
plébiscitez. Vous allez une fois encore pouvoir l'écouter car c'est avec lui que
nous allons revivre 7 jours d'avènement grâce au reportage de la rédaction.
Comme d'habitude, nous attendons vos messages. Une semaine d'actualité. Pierre-
Edouard Eldic. Tout à l'heure, à partir de 9h10 T.U. RFI. Retrouvez
l'information de RFI en qui soit illie à 4h30, 5h30 et 15h T.U. Toute
l'actualité africaine et internationale, des magazines qui donnent la parole aux
auditeurs sur la santé, l'environnement, l'économie et l'agriculture, à écouter
sur RFI en FM et en nombre court sur l'application RFI Peur Radio et les réseaux
sociaux. A Saint-Ephama, RFI. Tous les détails sur le site RFI qui soit illie. A
vous écouter Radio France International. Il est 6h30 à Libreville et Paris, 5h30
en temps universel. RFI matin. Et bonjour Vincent Dublange. Bonjour Gaëtan,
bonjour tout le monde. Le journal avec vous. On commence au gabon. Il faut
utiliser le pluriel quand on parle d'opposition politique. La fracture semble de
plus en plus évidente malgré l'enjeu d'aller grouper à la présidentielle dans
quelques mois. Exemple, lors du vote sur la réforme électorale, jeudi à
Libreville, certains députés opposants ont voté contre quand d'autres se sont
abstenus. On évoquera dans un instant la proposition de loi d'amnistie des
condamnés ou exilés politiques. Proposition de l'opposition parlementaire au
Bénin qui demande le soutien de la majorité. La libération de Paul Rousset
Sababina, l'opposant condamné il y a un an et demi à 25 ans de réclusion pour
terrorisme. Après une médiation des États-Unis et du Qatar, il pourrait bientôt
rentrer aux États-Unis. Le romaniement ministériel en RDC. On en en parlait
hier. Nous nous brossons le portrait de l'un des entrants dans ce nouveau
gouvernement. Pierre Memba, ancien chef de guerre et ancien vice-président. À
Libreville, c'est le résultat officiel des discussions pouvoirs-oppositions qui
ont eu lieu en février. Les députés gabonais ont donc adopté la réforme
constitutionnelle. Les sénateurs doivent désormais faire la même chose. Le texte
prévoit entre autres l'alignement des mandats politiques sur cinq ans, la non-
limitation des mandats, l'adoption du scrutin à un tour pour toutes les
élections. Jeudi à l'Assemblée, je vous le disais en titre, l'opposition
gabonaise s'est divisée. L'opposition donc plutôt les oppositions. Certains ont
voté contre, d'autres se sont abstenus et c'est une nouvelle preuve des
dissensions, Sébastien Németh. Depuis janvier, les opposants ont affiché des
désaccords sur plusieurs dossiers sensibles. Participation au centre gabonais
des élections, aux concertations politiques ou encore vote de la réforme
constitutionnelle. Ces divergences ont permis une clarification. Ceux qui
soutiennent la politique du pouvoir ne peuvent pas se réclamer de l'opposition,
estime Jean Gaspard Ntutu-Mahy, vice-président de l'Union nationale.
L'opposition est donc désormais fracturée entre ses principales forces, UN, PSD
ou encore RPM d'un côté, PG-41 et les démocrates de l'autre. Il n'y a plus de
discussion, ils n'ont pas joué francs-jeux et ne sont pas sincères. Je ne suis
pas un radical, je veux d'abord que le pays aille bien, donc je parle avec tout
le monde, indique Lugaston-Mahila, qui avait participé aux concertations avec le
pouvoir. Le président de la PG-41 se dit néanmoins prêt à renouer le dialogue
avec les autres. Pour Seraphin Kouré-Davin, l'ambiance est délétère mais
certains se trompent de cible. « J'ai été insulté, traîné dans la boue et
l'union semble de plus en plus difficile », dit le président honoraire du parti
Les démocrates, tout en souhaitant que chacun dépasse ses États d'âme car ce
parler est vital, selon lui. Mais c'est sans compter des divisions internes.
Chez les démocrates, certains reprochent à Seraphin Kouré-Davin et ses députés
de s'être abstenus jeudi au lieu de s'opposer au projet de réforme. C'est
contraire aux orientations du mouvement, certains s'affranchissent et
collaborent avec le pouvoir, indique une bonne source. Le leader du parti,
Ginzou Mandama, toujours empêtré dans des déboires judiciaires, semble lui pour
l'instant impuisse. Il y a les démocrates au Gabon et les démocrates au Bénin,
même nom pour deux partis politiques d'opposition. À Porto Novo, le mouvement
donne de la voix à l'Assemblée nationale. Depuis les législatives, la formation
peut compter sur 28 députés, sur les 109 que compte l'institution. Ils viennent
de déposer une proposition de loi d'amnistie des députés qui s'inquiètent du
sort des opposants condamnés en prison ou en exil. C'est notamment le cas de
l'une des membres du Parti les démocrates, Rekia Madougou, RFI cotonou Jean-Luc
Aplogand. Peu avant la conférence de presse, l'ancien chef d'état Boni Yahi a
dénoncé l'isolement de son ancienne garde des Sceaux et réclamé sa libération
sans condition. Le Parti les démocrates, dont il est président d'honneur, a
présenté dans la foulée à la presse une proposition d'une loi spéciale
d'amnistie. Amnistie en faveur des personnalités politiques en prison, les
personnalités politiques béninoises en exil. Autre Rekia Madougou sont
concernées Joël Ayvon, Komi Kutche, Sébastien Diabon et bien d'autres. Toujours
pour sa championne, le Parti appuie sur un autre levier en exigeant l'exécution
sans délai des conclusions du groupe des espères de l'ONU. Elle demandait la
libération de Rekia Madougou. C'était en novembre 2022. Minoritaires au
Parlement, les élus de l'opposition, demandaient à la majorité de les soutenir
dans l'intérêt du bénin. Le dernier appel pour Patrice Tallon, le Parti l'invite
à œuvrer pour l'aboutissement heureux de l'initiative. Cela renforcera son bilan
économique, politique et social, estime l'opposition. Jean-Luc Aplogant,
Kotoulou, RFI. Au Burkina Faso, état d'urgence décrété à compter du 30 mars
jeudi prochain, donc dans 22 provinces sur les 45 que compte le pays. Selon le
gouvernement de transition, cela permet de renforcer les moyens juridiques pour
lutter contre le terrorisme. Les groupes djihadistes frappés par une opération
militaire au Niger, voisins, selon la communication officielle de Nehamiai, 79
combattants auraient été tués la semaine dernière dans l'ouest du pays, près du
Mali. Des motos, des armes, des munitions auraient également été saisis ou
détruites par l'armée nigérienne. Au Rwanda maintenant, la libération de Paul
Roussey Sabagina. En fin août 2020, il avait été arrêté, il pensait alors se
rendre au Burundi, il avait embarqué dans un avion à Dubaï, avion qui avait
finalement atterri à Kigali, les services de sécurité rwandais l'avaient arrêté.
Arrestation qualifiée d'arbitraire à l'époque par Human Rights Watch. Un an plus
tard, en septembre 2021, l'opposant était condamné à 25 ans de réclusion pour
des faits de terrorisme. Après une procédure inéquitable, avait dit à l'époque
Amnesty International. Depuis les États-Unis, notamment, c'était mobilisé pour
Paul Roussey Sabagina et cela semble avoir porté ses fruits hallucinouyaux. Sa
libération était en négociation depuis plusieurs mois. Paul Roussey Sabagina a
finalement vu sa peine de 25 ans de prison commuée sous ordre présidentiel, cinq
mois après avoir écrit une lettre datée d'octobre dernier demandant le pardon du
chef d'État, Paul Kagame. Alain Mukurhalinda porte parole à juin du gouvernement
rwandais. Il a bien expliqué qu'il demande cette grâce présidentielle en accord
avec sa famille et ses avocats. Et qu'il n'allait pas reprendre les activités
politiques, surtout en ce qui concerne le Rwanda. Donc je crois que tout cela a
été examiné par le gouvernement et que voilà, c'est grâce à sa cusée d'accorder
la grâce présidentielle. Dans sa lettre, Paul Roussey Sabagina promet, si la
grâce lui est accordée, de retourner vivre discrètement avec sa famille aux
États-Unis, où il avait le statut de résident permanent avant son arrestation en
2020. Un retour qui devra d'abord être...