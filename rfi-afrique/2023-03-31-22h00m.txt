TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-03-31-22H00M.MP3
--------------------------------------------------------------------------------
... A l'écoute de RFI, 22h à Paris, 20h en temps universel.... L'Ukraine salue
aujourd'hui la mémoire de la ville martyr de Bucza, triste emblème des atrocités
attribuées à l'armée russe. Bucza doit devenir un symbole de justice sur
Volodymyr Zelensky. Le haut commissaire des droits de l'homme s'est également
exprimé, vous l'entendrez. Qui est Alvine Bragg, ce procureur new-yorkais qui a
pris la décision historique d'inculper un ex-président, en la personne de Donald
Trump, son portrait dans cette édition. Le nouveau portrait, le nouveau procès
contre Penar Selek, renvoyé à fin septembre. La sociologue franco-turque risque
la prison à vie. La Turquie l'accuse d'être impliquée dans une explosion à
Istanbul en 1998. Et nouveau sélectionneur et nouvelle équipe pour l'équipe de
France féminine de football, Hervé Renard, a présenté une liste de 26 joueuses
aujourd'hui dans son viseur les matchs amicaux du mois d'avril et bien
évidemment la coupe du monde qui commence en juillet. Bucza doit devenir un
symbole de justice d'Ivolo Dymyr Zelensky, à l'occasion du premier anniversaire
du retrait russe de cette ville martyr. Devenu un symbole des atrocités imputées
aux troupes de Moscou. Depuis Genève, le haut commissaire des droits de l'homme
Volker Turck a pour sa part dénoncé de graves violations des droits humains
commises par la Russie. On écoute. Après 13 mois de guerre menées par la Russie
contre l'Ukraine, les violations graves des droits de l'homme et du droit
humanitaire international sont devenues scandaleusement routinières. Nos équipes
ont enquêté sur plus de 8 400 décès de civils et plus de 14 000 civils blessés
depuis le 24 février de l'année dernière. Ces chiffres ne sont que la partie
émergée de l'iceberg. Dans les zones occupées de l'Ukraine, nous avons recensé
de nombreuses exécutions sommaires et attaques ciblées de civils par les forces
militaires russes, y compris des groupes armés qui leur sont affiliés, tels que
le groupe Wagner. Nous avons également recensé 621 cas de disparition forcée et
de détentions arbitraires. Cette guerre défie toute raison. Cette folie doit
cesser et la paix doit être trouvée conformément à la Charte des Nations Unies
et aux droits internationales. Volker Turkele, haut commissaire des droits de
l'homme et réaction de Paris aussi. En ce triste anniversaire, Moscou a laissé
faire, voire encouragé, les crimes de guerre, estime Emmanuel Macron. Nouvelle
aide financière pour l'Ukraine, le Conseil d'administration du FMI valide un
nouveau plan. Décidée le 21 mars, cette validation permet le versement d'une
première tranche de près de 3 milliards de dollars. Ce soutien s'intègre dans un
programme global d'aide financière de 115 milliards. Il est celui qui a osé
inculper un ex-président pour la première fois dans l'histoire des États-Unis,
qui est le procureur Alvin Bragg. Eike Schmidt brosse le portrait du premier
magistrat afro-américain à la tête du parquet de Manhattan à New York. Eike
Schmidt. Alvin Bragg, l'enfant de harlem à la barbe, et ses les poivres bien
taillés, devient aujourd'hui le pire cauchemar de Donald Trump. C'est loin
d'être la première fois qu'il satie les foudres des républicains. Dès sa prise
de poste en janvier 2022, le procureur afro-américain, classé comme
progressiste, fait de la criminalité des cols blancs sans cheval de bataille.
Meilleur exemple, il réussit à faire condamner l'entreprise familiale Trump
Organization à 1 600 000 dollars pour fraude financière et fiscale. Marqué par
le meurtre de l'afro-américain George Floyd par un policier blanc, Alvin Bragg
promet l'équité et la sécurité à New York et s'engage à ne plus poursuivre des
infractions jugées mineures comme les refus d'obtempérer ou de payer les
transports en commun. Car selon ce juriste sorti de Howard, qui a vécu des
violences policières en ver les noirs dans sa propre chair, la prison ne doit
être que le dernier recours. Alvin Bragg est l'homme qui peut faire tomber
Donald Trump, Trump qui le traite régulièrement de raciste et de gauchiste. Aïke
Schmit, et dans cette affaire qui concerne donc le versement supposé de Donald
Trump à l'actrice de film X Stormy Daniels en 2016, la principale intéressée se
dit aujourd'hui fière et espère être appelée à témoigner. Après 21 mois de
négociation, Londres rejoint le partenariat de libre-échange transpacifique,
c'est l'accord commercial le plus important depuis le Brexit et il fait du
Royaume-Uni le premier pays d'Europe à le rejoindre. Ce bloc regroupe désormais
500 millions d'habitants et 15% du PIB mondial. Au Royaume-Uni, toujours grève
en vue dans les airs, pour Pâques, près de 1500 agents de sécurité vont débrayer
pendant 10 jours à l'aéroport d'Israël, l'un des principaux du pays. Nouvelle
mobilisation après l'échec des négociations salariales. Les grèves se
multiplient. Au Royaume-Uni, depuis des mois, les Britanniques réclament des
hausses de salaire en raison de l'inflation. Une inflation qui ralentit
justement faiblement, aussi bien en Europe qu'aux États-Unis, mais elle reste à
des niveaux toujours très supérieurs à la cible de 2%, visée par les banques
centrales. Une inflation que la présidente de la Banque centrale européenne juge
encore trop élevée. RFI, il est 20h05 en temps universel, 23h05 à Istanbul, où
se tenait un nouveau procès contre Pénar Seleik aujourd'hui. Finalement, pas de
verdict dans le procès de la sociologue et écrivaine franco-turque. Depuis 25
ans, elle est jugée pour une explosion au marché aux épices d'Istanbul, une
explosion accidentelle d'après les experts, mais que les autorités turques
persistent à considérer comme un attentat. Pénar Seleik est visée par un mandat
d'arrêt international. Elle est menacée de prison à vie. La chercheuse
n'assistait pas à l'ouverture de ce nouveau procès que le tribunal a décidé de
renvoyer à l'automne, correspondance d'Anne Andler à Istanbul. Après plusieurs
heures d'audience dans une salle comble, la 15e cour d'assise d'Istanbul a
décidé de renvoyer au 29 septembre le procès de Pénar Seleik. Les juges ont
également maintenu le mandat d'arrêt international, avec emprisonnement immédiat
émis en janvier à l'encontre de la sociologue, qui vit en France depuis 2011. Le
président du tribunal a déclaré au cours de l'audience qu'il revenait aux
autorités françaises d'arrêter et d'extrader la chercheuse franco-turque. Pénar
Seleik était représenté par plusieurs avocats, turcs et français, qui ont
rappelé l'absence de preuves dans le dossier d'accusation, et les quatre
acquittements dont elle a déjà bénéficié depuis 2006. La défense a demandé à ce
que la sociologue soit entendue en France, une demande que le tribunal a
rejetée, décidant d'attendre l'exécution du mandat d'arrêt international. Une
importante délégation de quelques 80 personnes universitaires, représentantes de
la société civile et du monde politique, venus pour la plupart de France,
assistaient au procès. La police, déployée en nombre aux abords du palais de
justice, les avait empêchés de faire une déclaration à la presse juste avant le
début de l'audience. Anne Andlore, Istanbul, RFI. L'influenceur américano
britannique Andrew Tate et son frère Tristan libéré, ils ont été arrêtés fin
décembre en Roumanie pour une affaire de proxénétisme. La cour d'appel de
Bucharest a ordonné leur assignation à résidence. Quatre personnes tuées par
trois avalanches, aujourd'hui en Norvège, au moins l'un d'eux est un touriste
étranger. On ne connaît pour l'heure pas sa nationalité, les avalanches se sont
passées dans le nord du pays. Plus de peur que de malheur. En Suisse, pour le
moment, du moins 15 blessés dans deux déraillements de train cet après-midi.
L'un d'eux est dans un état grave. Les accidents ont eu lieu quasiment au même
moment dans le nord-ouest du pays. Il pourrait être dû au vent violent qui
touche actuellement une partie de l'Europe. Et un peu de sport. Au lendemain de
sa nomination, Hervé Renard a été présenté au siège de la Fédération française
de football aujourd'hui. Le nouveau sélectionnaire des Bleus a donné une liste
de 26 joueuses, et ce pour préparer les matchs amicaux face à la Colombie et au
Canada. Ce sera les 7 et 11 avril prochain. Arthur Verdelay a aussi confié sa
joie de démarrer cette nouvelle aventure. C'est une immense fierté d'être devant
vous. L'équipe de France représente quelque chose d'important pour moi. Tout
sourire dans son costume noir, Hervé Renard prononce ses premiers mots en tant
que sélectionneur des Bleus. A 54 ans, l'entraîneur a décidé d'abandonner un
contrat myrifique avec l'Arabie saoudite pour tenter de devenir prophète en son
pays. Hervé Renard a en tout cas déjà convaincu le président intérimaire de la
Fédération française de football, Philippe Diallo. Il a l'expérience du haut
niveau, c'est un sélectionneur à succès. Et puis il a ce charisme, ce leadership
dont nous pensons qu'il est nécessaire pour notre équipe nationale. Très à
l'aise devant les micros, le technicien se dit motivé par l'urgence de sa
nomination, à moins de 4 mois de la Coupe du Monde. C'est une question de
challenge. Il n'y a rien qui remplace les grandes compétitions. Donc là, on est
servi puisque dans quelques semaines, cette Coupe du Monde, l'année prochaine,
les Jeux Olympiques qui plus est dans notre pays. Bien décidé à faire briller
les Bleus, Hervé Renard veut tourner la page de l'air corindiaque pour preuve
les convocations de l'ancienne capitaine Wendy Renard, sa coéquipière à Lyon,
Eugénie Le Sommert ou encore la milieu Léa Legarec. Trois retours qui en
appellent bien d'autres. Arthur Verdelay, du service des sports, de RFI, pas de
Kéram. Ah oui, en revanche, au sein de la première liste d'Hervé Renard. Un
choix avant tout sportif selon lui. Et coup d'envoi de la 29e journée de Ligue 1
aujourd'hui en football, après une trêve internationale et elle commence avec un
derby du Sud. Marseille reçoit Montpellier. C'est la pause pour les deux
équipes. Scores à la mi-temps 1 à 1. Demain, deux matchs au programme. Auxerre 3
à 17h et Reynlens à 21h. Il reste finalement en prison. L'ex champion
paralympique sud-africain Oscar Pistorius a vu sa demande de libération
conditionnelle refuser ce jour. Il est emprisonné depuis 2017 pour le meurtre de
sa compagne. L'écrivaine québécoise Andréa Michaud est l'une des auteurs invités
à Quai du Polar à Lyon, qui se déroule tout le week-end prochain. À cette
occasion, je la reçois pour parler de son nouveau roman noir intitulé « Proie »,
une version macabre du petit poussé dans une forêt canadienne. Littérature sans
frontières. Catherine Fruchon Toussaint. Demain, à partir de 13h30 TU. RFI.
Couleur tropicale. Tu montes, on monte. Tu descends, on descend. Tu pleures, on
n'a rien, on se noie ça. Le soir, c'est couleur tropicale. Si, les raisons que
nous avons de nous révolter. Génération consciente. C'est la lutte des... Vous
avez commencé par réveiller ma curiosité. Couleur tropicale. Mais là, vous
captez mon intention. Couleur tropicale avec Claudicia. Et Mathieu Salaber,
Annabelle Jogama-Handil et Aperera Zanutto. Bonne arrivée, la famille. La
séquence Gold va vous ravir, en tout cas, nous l'espérons. C'est un concept dans
le concept du concept. Rendez-vous enfin des missions pour la reprise de
chansons d'opéra façon afro. Avant cela, avant cela, la famille, vos notes
vocales, laissez sur notre WhatsApp au 00336 37 42 62 24. Et puis évidemment,
les gros sons du moment. Tout de suite. Et nous allons twerker. Moula, je fais
l'ambiance. J'vrape le dos. Turn it up. Twerk on the left. Twerk on the right.
Oh yeah, twerk, twerk, twerk, twerk, twerk, twerk. Dina, c'est, c'est, c'est,
c'est, c'est, c'est, c'est. Oh yeah, twerk, twerk, twerk, twerk, twerk, twerk,
twerk. Dina, c'est, c'est, c'est, c'est, c'est, c'est, c'est, c'est. Tu vas me
sous-signer, boire comme on a trash. Yeah, you know yolo, vigno, soyangoyo. À
chaque jour, ça pète, y'a les lotos, huit olis. Ticatomina, y'a qu'on s'appelle,
la mouto a piqué sa tée. Motto, eh bella, les locos, pématé. Mouto, y'a les
locos, ticatonte. Twerk, twerk, c'est, c'est, oh yeah. Twerk, twerk, dinSC,
c'est, moulaf, cache, bellé ambiance.е Ella, elle, elle, elle, elle, elle, elle
encore Happy general. Turn it up, twerk on the left TWERK ON THE RIGHT. Oh yeah,
twerk, twerk, twerk, twerk, twerk, twerk, Dina, c'est, c'est, c'est, c'est,
c'est, c'est. Draw your draws, draw, draw, draw, draw, draw, draw Dinner's set,
set, set, set, set, set, set Step into the club, always with my crew DJ play the
drum, came in with the good Scam back to the e-billy, beaucoup qui ressortent tu
vas voler Please don't kill my vibe, on a sicko snap ololo I came with levels,
I'm on the labels, I'm not here to explain on me I came to dance, if you excel,
you might as well dance on me Twerk, twerk, sing, sing, oh yeah twerk, twerk,
sing, sing M'boule up, gauche, belle ambiance, drop it low, turn it up Twerk on
the left, twerk on the right, oh yeah twerk, twerk, twerk... M'bullup, gauche,
belle ambiance, drop it low turn it up Twerk on the left, twerk on the right Oh
yeah, twerk twerk twerk twerk twerk In a say say say say say say say Oh yeah,
twerk twerk twerk twerk twerk twerk In a say say say say say say say Un trio de
choix et de poids Keisha, le Worldwide Chico, Bodhi, Sadvan, le DJ International
et Alekzh, lauréat du prix découvert, Terefi Ils sont ensemble pour nous faire
twerker C'est l'un des 50 titres que l'on retrouve sur le triple album de Keisha
Depuis son premier hit en 1998, Keisha en a signé des tubes pour lui et pour
tant d'autres Voici la nouvelle génération représentée par Bazarba Hey, good
stuff! Yeah! Listen! C'est là ton perso à la langue Noco, c'est là ton perso à
la langue Et si déjà ma ligne est là, y'a mon mi Nanute, c'est là ton perso à la
langue Oh, je suis un boga Oh, je suis un nala Oh, je suis un mignon Papa, c'est
là ton perso à la langue Et si déjà ma ligne est là, y'a mon mi Nanute, c'est là
ton perso à la langue Oh, je suis un boga Oh, je suis un nala Tu sais derrière
toi combien on drague Si tu veux, on se salait les bâtis à l'hanni Et tu me
toies les rayons, comme en Saint-Dimay Oh, ma gare, je te dis que je suis un
boga Mais de plus, tu m'es la calée là Oh, comme là, je te dis que je suis un
boga Ma chérie, t'es jolie Tu m'as fait un boulot à la langue Tu m'as fait un
boulot à la langue Mais est-ce que déjà c'est présent, t'as les papas, beaux
kilos Mon gars, t'as une vraie, mami Un constat ici de plébatter la bêbêno Pour
tout ce gars, des c'est qu'un peu, y'a tout T'as qu'à m'apprendre, t'as tué la
rabebêno Toi, c'est la bonne, y'a y'o Toi, c'est la bonne qui sort A mi, c'est
toi qui récupère Toi, comme ça, tu n'es pas un boga Toi, c'est le bon, y'a y'o
Toi, c'est le bon qui sort A mi, c'est toi qui récupère Toi, c'est toi qui sort
Toi, c'est toi qui sort Et si déjà, tu n'es pas là, t'es un boulot Tu m'as fait
un boulot Ah, c'est toi qui sort Ouais Ah, c'est toi qui sort C'est toi qui sort
C'est toi qui sort C'est toi qui sort C'est toi qui sort Et si déjà, tu n'es pas
là, t'es un boulot La noteur La noteur C'est elle, la mannequin chanteuse Aira
Star, béninoise de naissance Nigérienne d'origine, en 2018 Elle est gosse, elle
devient mannequin En étant sous contrat avec l'agence Quove Models Puis, elle,
parce qu'elle était prise par la musique Elle est prise par la chanson Alors en
2019, elle fait ses premières reprises Qu'elle poste sur Instagram Eh oui, Aira
Star, c'est la génération Instagram Le 22 janvier 2021 Sors son premier album
produit par Luda et Don Jay-Z Celle qui s'identifiait à Nicki Minaj Et propulsée
au devant de la scène Pourtant, elle n'a rien d'une Nicki Minaj Elle est, enfin,
quoique, elle n'hésite pas A aller voir le clip de Sability Et vous nous en
direz des nouvelles Sability, c'était Aira Star..................... En ce
moment, au pays de Trump C'est elle qui, avec d'autres, tient le temple des gros
sons à grands succès Ice Spice, sans retenue, c'est... comment vous dire? C'est
un peu vulgaire mais pas trop Un peu bitch mais pas trop Juste comme les aiment
Trump In a mood, c'était l'afro-américaine Ice Spice Voici une rappeuse
ivoirienne à l'opposé de Ice Spice Elle s'appelle Oprah Eh, écoute
ça!................................................... Amina, Amina,
Aminatollah, elle est si bas Amina, Amina, Amina connaît, elle sait s'y prendre
Mélou, Mélou dans la tête, y'a les beaux fous fait la fête Des riz beaux collés
sur tête, c'est le mouvement Nous on est là pour la quête, y'a pas de quelqu'un
pour nous test Y'a pas de bus et nous les chênes Si Oprah est issue des
quartiers chic d'Abidjan et que sa réalité est aussi française, elle n'est pas
éloignée des préoccupations du quotidien des gens. Texte dans l'air du temps et
ciselé, posture digne, car son objectif est de donner une autre image d'un rap
français gorgé, gorgé je dis bien d'africanité. Amina c'était Oprah. Amina,
Amina, Aminatollah, elle est si bas Amina, Aminatollah, elle sait s'y prendre
Mélou, Mélou dans la tête, y'a les beaux fous fait la fête Amina, Aminatollah,
elle est si bas Amina, Aminatollah, elle est si bas Amina, Aminatollah, elle est
si bas Aminatollah, Aminatollah, elle est si bas Aminatollah, Aminatollah, elle
est si bas Amina, Aminatollah, elle est si bas On peut pas dire qu'elle est trop
cool C'est à cause de ses Toutes les musiques d'Africains, la rencontre de la
Mauritanie et du Sénégal, deux pays frères, même si en 1989 la guerre entre les
deux pays fut sanglante. Mais dans chaque pays, aujourd'hui je répète bien, dans
chaque pays, il y a tant d'originaires de l'autre pays, je dis bien tant
d'originaires dans l'autre pays, me suis-je bien fait comprendre. Advaiseur
l'homoritania, Maguissé la sénégalaise, un duo de circonstances pour RGN. Voici
la Caraïbe, la Guadeloupe pour être plus précis et pour un autre duo. Devs un
grand 𝗒𝗮 diagonal sitzt sur la grève et les neuf ne sont pas aux Si à la Saint-
Gling-Gling, seulement, Si à la Saint-Gling-Gling, comment gagner et oublier, Si
à la Saint-Gling-Gling, seulement, seulement, Si à la Saint-Gling-Gling, comment
nous perniver et séparer. Léo, Saint-François, et moi-même à Saint-Claude, C'est
qu'on signe nous aux antipodes, Des joues de l'ombre à la Saint-Sylvestre, Tous
les ans, l'ombre ou nous, pas qu'à mollis. Pas, pas ni tchac, pas ni pièce,
problème entre nous, Nous vraiment en harmonie. Si on sait qu'à caractériser
l'ombre ou nous, C'est saint-valentin. D'ailleurs c'est toujours, tous les 36
dix mois, Qu'à nous guérir et oublier. Si à la Saint-Gling-Gling, seulement, Si
à la Saint-Gling-Gling, comment guérir et oublier, Si à la Saint-Gling-Gling,
seulement, Si à la Saint-Gling-Gling, comment perniver et séparer. Si jamais on
joue, l'ombre ou nous réplique, C'est que seulement à la Saint-Gling-Gling,
Saint-Thomas qui s'est Thomas, pas qu'à douter, De sentiments, nous n'y on, pour
l'autre. Toutes 5 calendriers témoins de janvier à décembre, Y'a aucun fête et
nous, tous les jours. Si on sait, qu'arriver décoller, qu'on oublie en huit,
C'est cinq quête matin, oui c'est seulement, Les coques qu'il potait dans,
qu'est-ce qu'il voulait quitter. A la Saint-Gling-Gling, Dans quelques minutes,
vos notes vocales seront diffusées sur RFID en couleur tropicale et tous les
sujets sont permis, vous le savez, qu'ils soient légers aux générations
conscientes. Je vous redonne notre WhatsApp 00336 3742 6224. Jean-Pierre Cari et
Jocelyne Labille, un Guadeloupéen, Guadeloupéenne pour Saint-Gling-Gling, c'est
la Saint-Gling-Gling. La belle chanson du pays de Jacob Desvariieux, avec une
chanteuse extraordinaire Jocelyne Labille, aux chansons devenues cultes, laisse
parler les gens, j'ai déposé les clés, on verra quand tu veux et quelques
autres. Oui pétit pétit, couleur tropicale avec Clauditia. Celui qui dit que
couleur tropicale a ce pâché, ça t'a bien écouté avant de parler. La musique là,
ce sont les églons. Bonsoir, couleur tropicale, c'est Atono Adifabien, et
depuis, je n'avais rien connu sous le nom de Fabze Orgaïz Le Bangandev. Je suis
très ravi de votre émission, car cette émission est une émission très
nécessaire. Bonjour Claudit, bonjour à tous les auditeurs de la couleur
tropicale. Moi, c'est Monsieur Herb Eoman, à la Tue depuis le Cameroun, je vous
salue. Ça me fait plaisir la soirée de musique. Merci Claudit. Bonsoir Claudit,
je m'appelle Ngrihe Isufa, je vous appelle des Derhenshin. J'aimerais entendre
la chanson de Chris M, qui croira le vrai. Claudit, j'apprécie vraiment
l'émission que tu fais. Bonjour Claudit, c'est Blaise du Cameroun. Je vous
appelle pour dire une indignation face à ce qui se passe en Tunisie. Le
président de la République de ce pays a osé prendre la parole pour stigmatiser
les Africains noirs. Cette situation a déclenché des vandettas et des agressions
en série sur tous les Africains noirs, avec ou sans papiers. C'est un véritable
scandale, c'est honnêteux, c'est indigne. Je suis un étudiant centrafricain plus
précisément en Tunisie. Il y a des agressions qui se perpétent contre les noirs.
Il y a même des enfants de deux ans qui sont emprisonnés. Et agression contre
les étudiants et l'apprisantement, c'est vraiment pénible. Donc, Claudit, on ne
marche pas, on est toujours à la maison. Même si c'est pour marcher, on a la
peur au ventre. Claudit, je dis coucou à tous les Tadjins qui nous suivent d'ici
et d'ailleurs. Claudit, je demande la chanson de Djamal Zioun. On ne lâche pas.
Oui, pour faire plaisir à tous les Tadjins qui lui... Je veux dire comme je suis
fier du courage de mes frères. De tout ce qu'on enterre et de tout ce qu'on
appelle. De tout ce qui se dresse face à la haine du système. De tout ceux à qui
on sous-estime la vraie paix. Mais tant que prendre les rênes, la patrie reste
la reine. On ne lâchera rien. On ne lâchera rien. Tu vois même que tu es seul.
Bonjour, Réveil, bonjour, Clauditia. C'est Alou Diallo depuis Dakar au Sénégal.
Je vous adore parce que vous êtes un animateur engagé. J'aime toujours les gens
engagés. Merci. Bonsoir, Claudit. Bon émission à toi. Depuis la fois que je
vois, précisément à Davout. Je veux qu'il grève. Couleur tropicale avec
Clauditia. Ceux qui acceptent le mal sans jamais les dénoncer sont aussi
coupables que ceux qui le commettent. Merci vraiment pour toutes vos notes
vocales. Pour tous vos témoignages d'attention à l'endroit de votre émission.
C'est la génération consciente qui s'exprime. Voici la gode Mathieu. Tu es la
p*** qui adore tout le monde. On a le café aussi, le casino. Il y a des gens qui
sont de la qualité. Ils te mettent la main, mais une fin n'est pas très bonne.
Vous êtes à côté si vous n'y réagissez pas. Vous savez, c'est moi qui t'embête.
Ils se prétendent, ils sont chauds, et puis on dit, pourquoi pas? Les films
montrent que je suis un pilote. Ils me disent, ils sont chauds, et puis on dit,
pourquoi pas? Les films montrent que je suis un pilote. J'aime ça. Rachel
Allison sur RFID en couleur tropicale, maman et jeune femme au caractère bien
trempé et à la vision bien claire sur le combat des femmes pour leur affirmation
jusque dans les choix de leur sexualité. Too Hot chante Rachel Allison et puis
il y a des femmes pour qui la dépression, le mal-être sont une compagne et un
compagnon. People chante Libianca. People, people don't really know you, they
don't really know you cause you see people, people, people they don't really
know you, they don't really know. Oh, I only fight that Oh, make you drive on
the star Yeah, who got no death for you? Cause you see people, people, people,
people don't really know you They don't really know you Cause you see people,
people, people They don't really know you They don't really know you I've been
drinking more alcohol for the past five days Did you check on me? Now did you
look for me? I walk in the room, eyes are red and I don't smoke but no Did you
check on me? Now did you notice me? I've been drinking more alcohol for the past
five days Did you check on me? Now did you look for me? Libianca pour People
Dépressive dit-on mais artiste magique vous en conviendrait Elle est originaire
de Minneapolis, une ville américaine dont Prince fit la renommée internationale
Apparemment une ville du génie musical Changement de ton et de son, préparez-
vous à du compas et dans tous ces états Voici Sensei et c'est toujours dans
Couloir Tropical sur RF et la radio du monde Tu veux rester mais je suis pas
investi, toi tu veux tester, j'ai pas coeur sous la veste Si j'investis t'es le
pas de resto, c'est toi le festin Pas de boisson c'est toi la horny d'un oiseau
Tu finiras dans my eyes only, pour ce toi et moi y'a plus de raison T'es le rêve
de toutes mes insomnie Baby à les bosses, baby, baby à les bosses Elle a donné
sans effort, elle sait même bouger sans l'effort Horny d'un oiseau, Jean-Bolive
dans la gorge Sans colis d'un vas-en, elle boit horny d'un oiseau Baby à les
bosses, baby, baby à les bosses Elle a tout, mais fait des faces Bonne vie qu'on
ait pas les bases Mais j'suis dans les bailles, je sais comment t'éteins J'suis
très près du sable, je sais comment descendre Tu sais que j'fais du mal, mais tu
bouges pas c'est dingue Chouchou des femmes dans mon iPhone, c'est blindé Dis-
moi tout, dis-moi baby on fait comment J'suis le truc qui vibre avec
télécommande Quand j'suis pas trop dans l'love, j'ai déjà donné j'avoue J'suis
pas trop dans celle qu'on a mélangé pas tout Baby à les bosses, baby, baby à les
bosses Elle a dormi sans effort, elle s'est même bougée sans l'effort Horny d'un
vas-en, j'embolis dans la gorge Sans colis d'un vas-en, elle boit horny d'un
oiseau Baby à les bosses, baby, baby à les bosses Elle a tout, mais fait des
faces Bonne vie qu'on ait pas les bases Qu'on ait pas les bases Qu'on ait pas
les bases Qu'on ait pas les bases Un gars sûr si on aime le bon son Le son en
tout cas prompt à la danse C'est doux si c'est à l'état civil C'est c'est pour
la scène et son panafricanisme musical Puisque dans ce micro-sillon Je reprends
le truc comme si c'était un vinyle. Il y a laïcien parisien, Joey Douet-Fillet,
afin de soutenir Seysay de Melun, banlieue parisienne, pour Oney Damoiseau par
avant. Ne perdez pas la cadence, voici Jay Briggs! Yeah! Qui t'aille qu'on pas
marcher! Jay Briggs pour... Bon bisouen! C'est pas tel, j'ai ou méritant, non
Qui compte sans besoin Non pas besoin de parler, non je mouais tout s'enver Vous
me besoin d'un jeune amour, Y'a un amour tout le jour Oui, moi j'ai un jeune
amour, On joue à l'amour tous les jours Des gens en forme tactile, Qui sont
limite, Depuis ma ville y'en vînt deux Y'ont monde qui panne en vis, Mais qui
complice, Depuis mon delt, c'est riba Chouchou, telle, Dolo, telle, Babo, telle,
Moibler et retrain, c'est mon thé, C'est mon dieu, c'est mon plaît, Pour vous,
pour vous besoin Y'ont monde qui casque au prano, Qui boquette pour t'en en
charge Vous méritez, y'ont monde qui bâle, Toute saquée ou empêchée Vous
méritez, y'ont monde qui compte sans besoin Non pas besoin de parler, non je
mouais tout s'enver Vous me besoin d'un jeune amour, Y'a un amour tout le jour
Oui, oui, bonjour l'amour, Bonjour l'amour tous les jours Bingo, à quelle heure?
Chérie ou mère, y'est-elle? Basics, basics Tasse-toi, papa, chale, chale, chale
Soit tu me laisses me chier Et puis c'est rude qu'il te besse là, yes, tu te
besse là Où qu'on se rogne, j'en ai un, Tasse, tasse, tasse, tasse Basics,
basics Basics, basics Chérie, j'ai cru regarder, Où fait-il l'heure, je me
verrai On s'en dit, il me fait la guerre, ouais Qu'il me passe mon carousel, On
fait tête pointe, tout le monde On s'en dit, il me fait la force, on me fait, on
me fait Je n'ai pas mérité, tout le monde s'en est, on me fait, on me fait Je
suis au marché, non tout le monde Allez, s'en fait quoi, c'est chéri C'est pas
moi, non pas moi, non pas moi, c'est Betty On s'en dit, méchant Chérie, ouverte
le rythme Aïe, aïe, aïe, aïe, aïe, aïe, aïe, aïe, aïe, aïe, aïe, aïe Fouton, et
mes compas Oh, quitter le marché Oh, voyons, musique, la, dis mal C'est pas mal,
c'est pas mal C'est pas nous, non, qu'on parle les bords Ou j'assage, moi, mes
compas Voyez, qu'on parle les mounes La planète a couté Afrika et ses diasporas
Oh, j'arrête de parler créoles Afrika et ses diasporas En musique sur RFID en
couleur tropicale Du lundi au vendredi Bisous de le titre, c'était Jay Briggs
Avec deux X Coola, coola, coola, coola, coola Celui qui dit que couleur
tropicale a ce pas chic M'a quand bien écoute avant de parler Réagissez,
commentez et posez vos questions Sur les pages Facebook, Instagram et Twitter De
couleur tropicale Exprimez-vous avant, pendant et après l'émission Tu comprends
ou tu n'as pas compris La Parole Si Le C'est Le C'est Le C'est Le Le C'est Le
C'est Le Le C'est Le C'est Le C'est Le Le Le Le Le Quand tu es loin, je rêve au
horizonte et les paroles manquent Et je sais, je sais que tu es avec moi, avec
moi Tu es ma lune, tu es avec moi Je ne suis plus, tu es avec moi, avec moi,
avec moi, avec moi Conte Partiro Le monde entier connaît cette chanson
interprétée par le ténor italien Andrea Bocelli. De nombreuses versions existent
de Conte Partiro sorti en 1996. Ses tubes et ses cultes. En 2013, au Sénégal, un
prince d'une ballarde décide de reprendre le hit de Bocelli. Conte Partiro
devient Partir, pas Diouf, et son groupe Génération Consciente. Oui, son groupe
s'appelle Génération Consciente, en référence à notre mouvement. Conte Partira
Le monde entier connaît cette chanson interprétée par le ténor italien Andrea
Bocelli. De nombreuses versions existent de Conte Partiro sorti en 1996. Ses
tubes et ses cultes. En 2013, au Sénégal, un prince d'une ballarde décide de
reprendre le hit de Bocelli. Conte Partira Le monde entier connaît cette chanson
interprétée par le ténor italien Andrea Bocelli. De nombreuses versions existent
de Conte Partiro sorti en 1996. Ses tubes et ses cultes. En 2013, au Sénégal, un
prince d'une ballarde décide de reprendre le hit de Bocelli. De nombreuses
versions existent de Conte Partiro sorti en 1996. Ses tubes et ses cultes. En
2013, au Sénégal, un prince d'une ballarde décide de reprendre le hit de
Bocelli. De nombreuses versions existent de Conte Partiro sorti en 1996. Ses
tubes et ses cultes. En 2013, au Sénégal, un prince d'une ballarde décide de
reprendre le hit de Bocelli. De nombreuses versions existent de Conte Partiro
sorti en 1996. Ses tubes et ses cultes. En 2013, au Sénégal, un prince d'une
ballarde décide de reprendre le hit de Bocelli. De nombreuses versions existent
de Conte Partiro sorti en 1996. Ses tubes et ses cultes. En 2013, au Sénégal, un
prince d'une ballarde décide de reprendre le hit de Bocelli. De nombreuses
versions existent de Conte Partiro sorti en 1996. Ses tubes et ses cultes. En
2013, au Sénégal, un prince d'une ballarde décide de reprendre le hit de
Bocelli. De nombreuses versions existent de Conte Partiro sorti en 1996. Ses
tubes et ses cultes. En 2013, au Sénégal, un prince d'une ballarde décide de
reprendre le hit de Bocelli. De nombreuses versions existent de Conte Partiro
sorti en 1996. Ses tubes et ses cultes. En 2013, au Sénégal, un prince d'une
ballarde décide de reprendre le hit de Bocelli. De nombreuses versions existent
de Conte Partiro sorti en 1996. Ses tubes et ses cultes. En 2013, au Sénégal, un
prince d'une ballarde décide de reprendre le hit de Bocelli. De nombreuses
versions existent de Conte Partiro sorti en 1996. Ses tubes et ses cultes. En
2013, au Sénégal, un prince d'une ballarde décide de reprendre le hit de
Bocelli. De nombreuses versions existent de Conte Partiro sorti en 1996. Ses
tubes et ses cultes. Seule la main... De nombreuses versions existent de Conte
Partiro sorti en 1996. Ses tubes et ses cultes. De nombreuses versions existent
de Conte Partiro sorti en 1990. La séquence Gold en couleur tropicale, bon
arrivé si vous nous rejoignez. Il y a un thème à cette séquence, Les airs
d'opéra, version afro. À l'instant c'était Pat Duve pour partir la reprise de
Conte Partiro Conté par Tyro, d'Andréa Bocelli. Voici l'un des plus grands
ténors de l'histoire, Luciano Pavarotti, pour Caruso, l'une des plus célèbres
chansons d'opéra. Dove del mare loge, e tira forte e vento Suna vecchi Melbourne
sur sort dizendo Un homme abrace une fille, après avoir pleuré. Puis s'éteint la
voix et recommence à chanter. Je veux bien te savoir. Mais tant, tant, Venice
est une chaine normale qui déchirme la veille de la Venise. Je vois les lumières
au milieu du mou, je pense aux noix là, en Amérique. Mais ce n'était qu'un
lampard et la mouillée blanche d'une éleve. Je sens le douleur de la musique
s'envoi fort du piano. Mais quand je vois la lune sortir de la nuve, je me sens
plus douce que la mort. Je regarde dans les yeux la fille, qui verdit comme le
mal. Puis à l'improviso, j'imprime les larmes et lui prédette d'affogar. Je veux
bien te savoir. Mais tant, tant, Venice est une chaine normale qui déchirme la
veille de la Venise. Mais tant, tant, Venice est une chaine normale qui déchirme
la veille de la Venise. Mais tant, tant, Venice pourulate treescia qui 다臻ically
ce n'est que de la halteine estpps dans de la vu Jamais que fass come Et mon la
tout pitié Manque à rêver Tous les jours Et mon pas plus avouqui Manque à
voyager toujours Ralph Tamar, rafo pour les intimes Des lémices qu'on lotte mon
nid Qu'à fait chez moi Et tout mes murs Nos musiques par les voies frontières Et
cabolés d'anciens lèvres Manque on est tout côté Et il a mon beaucoup On peut
aller Nous peu chanter à vie La musique qu'on est Dans tout le pays Tout pas
tout tout long qu'est ni Ensoleil qui caléve On m'aille à qui cas souris Un
moment qui chantait La musique qu'on est R.F.I.