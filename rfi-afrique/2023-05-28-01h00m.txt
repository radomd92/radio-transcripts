TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-05-28-01H00M.MP3
--------------------------------------------------------------------------------
 Vous êtes bien sur Radio France International en direct de Paris où il est
exactement une heure ici, 23 heures en temps universel et c'est l'heure de faire
un point sur l'actualité de notre planète. Jérôme Bastion. À la une de ce
dimanche 28 mai 2023, les électeurs turcs aux urnes pour un second tour décisif
de la présidentielle, pour lequel le chef de l'état sortant, Réjep Taïyib
Erdogan, part favori, malgré une conjoncture économique défavorable qu'il a su
retourner à son avantage. Les rumeurs de la fachosphère russe sur la disparition
violente du chef d'état-major ukrainien ont fait long feu, l'intéressé dément en
apparaissant lui-même dans une vidéo où il appelle à la reconquête des
territoires perdus. Et puis en Italie, il a repris en main des médias publics
par le gouvernement nationaliste de Georgia Meloni. Car ils seront supposés
d'être aux mains d'une hégémonie de pouvoir de gauche, le domaine de la culture
subit le même traitement, vous l'entendrez. Dans quelques heures, l'ouverture
des bureaux de vote en Turquie pour le second tour de la présidentielle. Il
oppose le président sortant, Recep Tayyip Erdogan, au social démocrate Kemal
Kuluc Darolo. Lors du premier tour, Recep Tayyip Erdogan avait créé la surprise
en arrivant en tête avec 49,5% des voix contre 45% de voix à son rival, déjouant
les pronostics. Après 20 ans passés au pouvoir, le dirigeant sortant a été
pourtant donné fatigué, affaibli par la grave crise économique que traverse son
pays et les conséquences du séisme dévastateur du 6 février. Mais Recep Tayyip
Erdogan a su mettre en place une économie de campagne ciblée pour conserver son
électorat, explique Denise Hunal, économiste au Centre d'études prospectives et
d'informations internationales. On l'écoute. Selon les chiffres officiels,
l'inflation sur un an apparaît comme 50%. D'autres autorités que l'Institut
national turc mesurent une inflation proche de 100%. Et par rapport à cette
situation qui a été créée en Turquie pratiquement de toutes pièces par une
gouvernance chaotique, qui est indépendante de la situation internationale où
l'inflation est très élevée aussi, le gouvernement agit par des mesures qu'on
appelle orthodoxes, mais en réalité, c'est des mesures vraiment pour juste
sauver l'instant présent sans cohérence. Et par ailleurs, on a mené une
économie, ce qu'on appelle en Turque, une économie de campagne, en effectivement
distribuant largement les fonds, les revenus de l'État qui n'existent pas. On
emprunte sur le marché international pour pouvoir distribuer toute forme de
cadeau. Et en particulier, il y a un ciblage évidemment sur l'électorat
d'Erdogan. L'économiste Denise Hunal du CEPII interrogée par Clémentine
Pavlotsky. La guerre en Ukraine avec la publication d'une vidéo par le chef
d'État major ukrainien, le général Valery Zaloujny, où il appelle à récupérer
les territoires qui appartiennent à l'Ukraine. Son apparition met donc un terme
aux spéculations qui couraient sur son compte, certains affirmant qu'il aurait
pu être tué. Les explications à Kiev de Stéphane Siorand. Durant tout le mois de
mai, le général Valery Zaloujny s'est fait plutôt rare dans les médias et sur
les réseaux sociaux. Et pour cause, on peut s'imaginer assez facilement que le
chef d'État major des forces armées ukrainiennes a suffisamment affaire au jour
le jour à préparer ses troupes à une contre-offensive à bien des égards
cruciales. Cependant, les médias russes et les cercles conspirationnistes se
sont emparés de cette cure médiatique et ont soutenu Mordicus, que le général
bien aimé des Ukrainiens avait été grièvement blessé voire tué. Vendredi, le
colonel Anatoly Stéphane, un des bras droits de Zaloujny, s'est fendu d'une
vidéo assez drôle dévoilant au monde un terrible secret. Eh oui, Zaloujny est
bien vivant, celui-ci apparaissant des bonheurs, faisant le V de la victoire.
Samedi, c'est le patron lui-même qui poste une vidéo d'une minute, une revue de
troupes ukrainiennes très martiales et fortement esthétisées intitulée « Une
prière pour la victoire ». « Il est temps de récupérer ce qui nous appartient »,
dit la vidéo, qui indique que les autorités ukrainiennes préparent tout
doucement la population au déclenchement des hostilités, à une date que seul
justement le général Zaloujny, le président Zelensky et une poignée d'autres
personnages connaissent. Stéphane Suwan, Lviv, RFI. Direction l'Italie, à
présent, où la première ministre d'extrême droite, Georgia Meloni, se lance dans
une bataille contre le secteur culturel et des médias. Selon elle, la culture,
les médias italiens sont acquis à la gauche. Dans un discours prononcé hier,
vendredi, elle a en effet affiché sa volonté de reprendre en main les
institutions culturelles du pays et ça commence par la raille, la radio-
télévision publique italienne. Anne Treca. Beaucoup d'Italiens l'appellent
maintenant « Télé Meloni ». La droite a placé ses hommes au poste clé de
l'audiovisuel public. Administrateur, directeur de journaux, les nouveaux sont
ouvertement de droite, pas toujours dotés d'une expérience de la télévision.
C'est une tradition italienne, les partis politiques se répartissent les chaînes
du service public, chacun devenant ouvertement la voix de son maître. La gauche,
en son temps, avait aussi placé ses pions, mais avec une majorité écrasante au
parlement, la coalition Meloni-Salvini-Berlusconi ne laisse à l'opposition que
des miettes en radio et télévision. Furieux, deux présentateurs vedettes ont
claqué la porte, d'autres pourraient suivre. Mais l'audiovisuel n'est pas le
seul domaine où le gouvernement s'impose. Par un décret surprise, abaissant
l'âge de la retraite à 70 ans pour les directeurs d'institutions culturelles, la
reconstruction de la région ravagée par les inondations ne sera pas confiée à
son gouverneur élu, un démocrate, contrairement à la tradition, mais à un
manager extérieur et les exemples se multiplient. Les professionnels choisis
n'ont pas de compétences évidemment meilleures que ceux qu'ils remplacent, mais
ils ont tous une qualité en commun d'être en excellent terme avec les partis
nationalistes. Antreka Arom, RFI. Les Israéliens à nouveau dans la rue ce samedi
soir, la rue de Tel Aviv, mais également les rues des grandes villes du pays, et
ce pour la 21e semaine consécutive. Ils ont entendu protester contre le projet
de réforme controversé du système judiciaire du gouvernement de Benjamin
Netanyahou. Des manifestations ont eu lieu donc dans diverses villes du pays.
Ces manifestants se rassemblent chaque semaine depuis janvier, à la fois pour
dénoncer la réforme et pour conspuer le gouvernement de M. Netanyahou, inculpé
pour corruption dans une série d'affaires. En bref, la Russie, qui somme
plusieurs centaines de fonctionnaires et d'employés locaux d'institutions
allemandes, de quitter le pays ou de tout simplement perdre leur emploi. Moscou
veut réduire drastiquement la présence de Berlin dans le pays, que ce soit
l'ambassade, les consulats ou bien les institutions actives, notamment dans les
domaines de la culture et de l'éducation. La Russie, qui a fait état samedi,
attaque de drones contre deux installations d'oléoducs sur son territoire, dans
la région de Tver, tout d'abord au nord-ouest de Moscou, à donc quelques 500 km
de la frontière avec l'Ukraine, ainsi qu'à Pskov, dans l'ouest du pays,
rapportent les médias et les gouvernements locaux. Un incident similaire s'est
produit près du village de Litvinovo, à moins de 10 km, cette fois de la
frontière russe avec la Biélorussie, toujours sans faire de victime. Les
autorités ukrainiennes n'ont pas confirmé ces attaques. Et puis le grand final
sur la Croisette où le 76ème Festival de Cannes s'est refermé ce samedi soir.
Séance de rattrapage pour ceux d'entre vous qui n'ont pas pu suivre en direct
sur RFI, la cérémonie de clôture et le palmarès à Cannes, Elisabeth Le Quéré. Le
palme d'or 2023 va à Justine Trier. Justine Trier, troisième femme couronnée par
la Palme d'or et deuxième française après Julia Ducournaut, anatomie d'une chute
et le portrait d'une femme accusée d'avoir tué son mari. Après les remerciements
d'usage, la réalisatrice de 44 ans s'est servie de cette tribune internationale
pour condamner la répression des manifestations contre la réforme des retraites
devant une salle partagée entre sifflet et applaudissements. Le grand prix a été
attribué à The Zone of Interest du britannique Jonathan Glazer, un portrait
glaçant du commandant du camp d'Auschwitz et de son épouse. Le prix du jury est
allé au Finlandais Akki Korismakki, grand maître de l'humour à froid. Celui-ci
raconte dans les feuilles mortes l'histoire d'amour entre deux marginaux. Le
prix de la mise en scène, enfin, est allé à une autre histoire d'amour et de
gastronomie, la passion de Dodin Bouffant du franco-vietnamien Tran Anh Uong
avec Juliette Binoche et Benoît Magimel. Elisabeth Le Quéré, Apolline Verlon,
Cannes, RFI. Et la gagnante de la Palme d'or du festival de Cannes pour son film
Anatomie d'une chute a dénoncé le traitement par le gouvernement de la
contestation de la réforme des retraites réaction de la ministre de la Culture.
Elle s'est dit estomaquée par un discours si injuste. Fin de citation. Il est
une heure dix ici à Paris. Manga à musique et RFI talent présente, Freddy
Masamba en concert. Retrouver l'artiste Afrosoul congolais sur la scène de
l'Institut français de Brazzaville au Congo le 3 juin à 19 heures. Info sur
institutfrancais-kongo.com Et si dessiner, c'était lutter contre l'oubli. Il
semblerait que notre invité Alfonso Zapico en soit convaincu. L'oubli de
certaines pages d'histoire restait trop longtemps dans le placard comme un
cadavre forcément encombrant. C'est ainsi que la Commune des Asturias fait
l'objet d'une série best-seller sur les lieux du crime en Espagne. Merci qui?
Merci Futuropolis qui publie les deux tomes de ce roman graphique qui vaut bien
dix cours d'histoire à lui tout seul intitulé Le chant des Asturias. Où l'on
comprend que Zapico n'est pas fils des Asturias pour rien, qu'il dessine après
avoir lu Camus, Pushkin ou James Joyce, il a aussi croqué L'homme de Dublin,
qu'il a un œil sur la politique basque, lire ceux qui construisent des ponts et
qu'après les ponts il s'est intéressé aux traits d'union. En quelques traits de
crayons, c'est ce qu'il a réalisé entre Blimea, sa ville natale en Espagne et
Angoulême en France où il réside aujourd'hui et où il a déjà été récompensé.
Hola que tal Alfonso Zapico? Très bien, merci beaucoup. Vous savez que vous avez
un nom de personnage de bande dessinée? Un peu bizarre, c'est vrai que mon nom
de famille est Asturian, ce n'est pas très habituel en Espagne. Mais vous êtes
d'accord que ça pourrait faire un nom de personnage non? Oui, c'est un peu
personnage bande dessinée, comme c'est un peu exotique. Il faudrait y penser. Le
jour où je suis né, quel planètre regnerait-il? C'est un peu comme ça, c'est un
peu comme ça. C'est un peu comme ça. C'est un peu comme ça. C'est un peu comme
ça. C'est un peu comme ça. C'est un peu comme ça. C'est un peu comme ça. C'est
un peu comme ça. C'est un peu comme ça. C'est un peu comme ça. C'est un peu
comme ça. C'est un peu comme ça. C'est un peu comme ça. Le jour où je suis né,
quel planètre regnerait-il? C'est un peu comme ça, c'est un peu comme ça.
folklorique. Vous diriez ça vous aussi? Ah oui, c'est bien parce qu'il projecte
une image des sastouris et du folklore des sastouris, que c'est très moderne,
très choquante et ça c'est bien parce que ça change aussi un peu le regard que
les gens hors les sastouris reçoivent de notre petit pays. Que raconte cette
chanson qu'on vient d'entendre? Ça c'est une version de l'império alchentien,
c'est la typique chanson que chantait il y a 30 ans dans la cuisine de la
maison, notre grand-mère et ça c'est bien, c'est un peu notre image de marque
des sastouris pour l'avenir. Bon Alfonso j'espère que vous avez apporté avec
vous vos crayons? Oui, j'ai un stylo de feutre. Tout va bien, vous allez en
avoir besoin. S'il vous plaît dessine-moi ton enfance. Je pense que j'ai dessiné
le noir et blanc parce que quand je viens de un petit village du bassin minière
des sastouris, d'un territoire industriel où il y avait des mines de charbon, la
fumée noire dans les ciels, je pense que j'ai dessiné noir et blanc avec
beaucoup beaucoup de gris. Et si on devait rajouter des sons? Je pense qu'on
entendra les usines, les mines, les camions, les ouvriers, le trafic, c'était un
peu le lézant industriel mélangé avec les ondes de la nature parce que c'était
les mines de charbon étaient aussi entourées par des montagnes. Alors il va
falloir malheureusement nous situer sur la carte où se trouvent les asturis.
Votre village s'appelle Blimea, Alfonso Zappico, mais malheureusement on ne sait
pas situer les asturis sur la carte. Même les asturis c'est vrai, c'est une
région assez inconnue en Espagne, c'est une région à côté de la mer, c'est pour
ça qu'il plaît beaucoup. C'est une région quand même qui attire les gens pour
savoir plus ce qui se cache là derrière ces montagnes. Vous avez été dans
l'enfance très attaché à au moins trois personnages, Enrique, Blanca et Ramon.
Ah oui, Enrique c'était mon grand-père mineur, il travaillait dans une mine de
charbon. Il était l'archétype, un peu le cliché de mineur asturien, un pas très
poli, avec pas beaucoup d'éducation mais avec une grande sagesse naturelle. Et
ma grand-mère c'était très différente, elle était maman des neuf enfants, elle a
géré un peu la famille et comme une petite entreprise. Et mon grand-père Ramon
il était voyageur, il était lecteur, il aimait beaucoup, aimait les livres,
c'était pas très habituel. Et bon c'est un mélange qui m'a construit d'une
certaine façon. Alors j'ai vu que les asturis avaient obtenu son statut
d'autonomie en 82, est-ce qu'on était, est-ce qu'on est indépendantiste dans la
famille? Dans ma famille non, il n'y a pas un nationalisme, le typique
nationalisme régional n'existe pas, ça existe là bas un sentiment très fort des
communautés. Mais c'est pas exactement par rapport à la langue, même s'il y a
une langue asturienne. C'est plus un clan, une tribu? C'est des clans, il y a
des petits clans dans les familles mais surtout c'est une communauté qu'on a
construit avec le travail, avec les mines de charbon, avec l'industrie. C'est un
travail qui a créé une communauté un peu bâtarde, de gens qui venaient d'autres
régions d'Espagne, qui parlaient d'autres langues avec d'autres accents. Il n'y
avait pas une patrie, on va dire, à réivindiquer mais un sentiment de
collectivité, de solidarité. Alors en tout cas à la maison j'ai cru comprendre
que la grande histoire de l'Espagne, même murmurée, avait toujours eu son rond
de serviette. Quand ils sont venus chercher ma mère, ils l'ont mise dans l'école
qui tenait lieu de prison pour femmes. Et là, on lui a rasé la tête. Elle
n'avait rien fait. C'était une moissonneuse. Ils disaient que c'était une rouge.
Ils l'ont fait traverser tout le village, au son des tambours et des trompettes
pour que tout le monde puisse la voir. Nous, les enfants du village, on suivait
le cortège mais on m'a interdit de m'approcher d'elle. Mon père m'a dit, tâche
de récupérer sa dépouille un jour et tu me la porteras. Je ne cherche pas la
vengeance. Je veux juste sa dépouille pour l'enterrer auprès de son mari. Je ne
demande rien de plus. Cette guerre d'Espagne évoquée à l'instant avec le
témoignage de Maria Martin, le silence des autres de Robert Bahr et Almudena
Caracedo, pour dire que cette guerre d'Espagne reste, il n'y a qu'à vous lire et
on va le faire, une blessure pour toute l'Espagne et même pour vous, le
quarantenaire espagnol en exil, Alfonso Zapico. Même pour moi et pour les
générations à venir, même si les jeunes qui sont en Espagne aujourd'hui, ne sont
pas encore conscients de comment cette blessure va marquer un peu beaucoup son
avenir, l'avenir du pays parce que la guerre d'Espagne est déjà très loin, parce
que bientôt ça sera un siècle. Ça continue encore à nous poser des questions, à
nous obliger à réfléchir. Ça devient une obsession parfois pour beaucoup de
jeunes, pour des adultes. Comment vous l'expliquez? Moi par exemple, j'étais
élevé dans une famille de silence. On dirait que la moitié des familles en
Espagne sont des familles de silence, et l'autre moitié des familles sont des
familles de mémoire. Mais vous qui êtes un fils du silence, on va dire Alfonso,
vous oeuvrez pour la mémoire. En 2018, il ne vous a pas échappé que
l'organisation armée indépendantiste Basque, l'ETA, a annoncé sa dissolution. Et
là, vous avez proposé à deux personnalités politiques basques, Fermin Muguruza
et Edu Medina, qui sont de deux camps opposés, de se rencontrer pour parler
histoire et avenir. Ça a donné une très belle bande dessinée sous votre plume
intitulée « Ceux qui construisent des ponts », où là vous refaites un peu
l'histoire et où vous l'interrogez. C'est-à-dire qu'après que le SCAD, c'est-à-
dire le pays basque, ait eu son lot de fusillades, de bombardements, de milliers
de morts, de prisonniers, de ces revendications, de ces manifestations, il y a
peut-être une question que vous posez, au nom de quel monde est-ce qu'on peut
tuer? Est-ce que ça, ce n'est pas la question révolutionnaire par excellence que
vous posez? C'est une question qui se pose toujours dans tous genres de
conflits, mais avec cette conversation, parce que le livre, c'est une
conversation dessinée, on dessine aussi un peu quelques générations de basques
pour récupérer un peu la mémoire et surtout pour construire l'avenir. Et c'est
pour ça que c'est bien de revenir sur ce qui s'était passé à travers les
histoires de Eduardo de Fermin, chacun avec sa propre version, avec sa propre
sensibilité, parce qu'ils viennent de villes différentes, de côtés idéologiques
différentes, ils sont vécus différentes situations personnelles, mais on trouve
toujours un espace commun. Alors parmi les voix littéraires, c'est ce qu'on va
découvrir en sol majeur, qui vous accompagne Alfonso, il y en a une qui a
volontairement évité la route politique, je crois, pour emprunter celle de la
fiction, c'est un certain James Joyce, très important pour vous. Pour en sol
majeur, je vous propose de l'écouter, James Joyce lisant un extrait de son
fameux roman, Ulysse. Alors je sais bien, Ulysse déjà en français c'est pas
simple pour les francophones, mais alors Ulysse en anglais dans le texte et puis
enregistré autour des années 20, mais c'est pour un sol majeur, parce que
effectivement chez Futuropolis en 2013, Alfonso Zopico, vous avez sorti James
Joyce, l'homme de Dublin, vous aviez déjà entendu sa voix? Oui, j'avais déjà
entendu cet extrait de Joyce en train de lire à Ulysse. Alors qu'est-ce qui vous
a véritablement fasciné? Je l'ai regardé, c'est presque une biographie dessinée,
puisque vous avez vraiment reconstitué les origines familiales et presque
l'origine de sa vocation littéraire à James Joyce. Qu'est-ce qui vous fascine au
départ dans cette aventure? Son œuvre qui par définition est vraiment
fascinante, ou ce personnage né en Irlande en 1882? En fait c'est un peu tout,
parce que l'œuvre de Joyce, c'est les personnages, et les personnages de Joyce
lui-même, c'est aussi son œuvre. Et ce qui m'a beaucoup inspiré, c'est surtout
un peu le processus des créations, comment il est parti de l'Irlande, il a eu
une espèce de sentiment de l'aventure, et il a traversé l'Europe dans la
première moitié du XXI siècle pour écrire ses livres. Il a eu une vie romanesque
qui m'a poussé aussi à dessiner. Dans ce moment-là j'habitais à Blimea, dans Les
Sastouris, que moi dans ce moment-là j'ai détesté un petit peu parce que je
voulais aussi partir et connaître d'autres endroits, d'autres horizons. Donc
j'ai pris en Joyce une excuse pour partir aussi, pour voyager, pour aller vers
la France, vers la Suisse, vers l'Irlande. J'ai voyagé avec ses livres et c'est
à travers cette vie aventurière romanesque comment je suis parti et comment je
devais, moi-même, dessinataire en France. Donc c'est une forme d'autoportrait à
l'envers. Je conseille cette bande dessinée parce que je pense qu'il y a peut-
être des éléments puisés justement dans son regard sur cette Irlande, sur son
roi sans couronne en 1888, sur le poids de la religion dans les affaires
politiques, des éléments qui ont sans doute orienté la pensée du futur James
Joyce. Est-ce que vous diriez, Alfonso, que vous êtes un dessinateur venu de la
planète littéraire? Complètement. C'est vrai que la bande dessinée c'est une
langue, c'est une langue, c'est une forme d'expression, c'est une langue. Peut-
être que c'est ma langue maternelle plus que l'espagnol. Dans la bande dessinée,
chaque auteur a des sources d'inspiration. Et moi c'est vrai que ce qui m'a
nourri, ce que j'ai mangé beaucoup pendant que j'étais adolescent, pendant que
j'étais enfant, c'est la littérature. On va dire que c'est la matière prime que
j'utilise pour construire mes histoires. Mais vous avez lu beaucoup, ça ne vous
a pas donné envie d'écrire, même s'il y a effectivement des dispositifs
narratifs assez puissants, vous vous êtes dirigé vers le dessin. Le dessin a
commencé comment? Ça c'est une question que je me pose parce que je me dis
pourquoi je n'écris pas, parce que je pense que je suis capable d'écrire. Je me
sens plus proche de la littérature que de l'illustration, que de dessin, que de
planète beaux-arts, que de planète images. Mais c'est parce que j'ai toujours
dessiné. C'était un peu la façon que j'avais pour m'exprimer quand j'étais
gamin. Et j'ai trouvé dans les dessins un outil pour me communiquer avec les
autres, pour envoyer un message. Quand j'étais au collège, j'ai dessiné de bande
dessinée dont les protagonistes étaient mes collègues de classe. Quand j'étais
lycéen, j'ai dessiné le journal du lycée. J'étais toujours un peu dessinataire
de la réalité qui m'intourait à chaque moment. Et nous voilà en terre asturienne
sous votre plume, Alfonso Zappico, avec le chant des asturies, donc roman
graphique qui s'ouvre... Eh ben voilà, c'est pas une surprise, sur une citation
d'Albert Camus. Je crois que je me suis bien battue, peut-être parce que je
n'avais plus grand-chose à gagner. Au prochaine neige, personne ne parlera plus
de moi sur terre. Pourquoi vous ouvrez ce roman sur cette citation? C'est une
citation que Camus a écrit, parce qu'il a écrit avec des amis une pièce de
théâtre sur cette révolte des asturies, la révolution asturienne de 1934. Il
était à Alger à ce moment-là et il ne connaissait pas du tout les asturies. Mais
j'ai beaucoup aimé cet extrait parce que je pense que même aujourd'hui, c'est
une bonne description de ce qui se passe avec ces territoires, avec ces
ouvriers, avec ces mineurs. Parce que le dernier moment de mouvement ouvrier des
asturies, les mineurs de charbon, c'était à 2012. Ça, c'était la dernière fois
qu'ils sont sortis, parce que c'était le moment de la fin des mines charbon. Ils
sont allés à Madrid à pied pour demander un avenir en futur pour ces enfants,
pour ces familles, pour ne pas laisser mourir, ne pas laisser abandonner ces
territoires. Et je pense que Camus, dans ce petit texte, il a bien condensé, il
a fait un petit bilan d'essais qui ont avec où, je pense, dans ces derniers
siècles. Parce que c'est vrai que ces mineurs qui ont entré à Madrid pendant la
nuit, ils ont été reçus par de milliers et milliers et milliers de personnes qui
les ont reçus avec un grain émotion. Peu de temps après, ils ont été oubliés.
Dessinés contre l'oubli, donc. Cette page d'histoire, l'insurrection de ces
mineurs asturiens dès 1933, qu'on va raconter un peu, vous diriez d'abord que
c'est une sorte de poids, de fardeau personnel et familial que vous trimballez
avec vous? C'est un sac à dos. Moi, j'ai su la première génération qui ne va pas
travailler dans des mines de charbon. Et c'est pour ça que nous avons un sac à
dos très lourd parfois, d'essayer de garder la mémoire et de la transmettre.
Alors dans ce sac à dos, il y a une sacrée mémoire puisque vous nous faites
rentrer dans cette histoire avec des dates. 1909, ce vieil empire qu'est
l'Espagne a un monarque inutile. 1917, hommes et femmes lancent une grève
générale révolutionnaire. 1931, Alfonso XIII, le roi s'enfuit, la République est
proclamée. Janvier 1933, Hitler entre en scène, il est accueilli favorablement
au congrès de Nuremberg par Gilles Robres, le chef de la Confédération espagnole
des droits des autonomes. Noël 1933, la droite catholique en Espagne s'empare du
pouvoir. Et c'est au cours de ce Noël que votre héros, Tristan Valdivia,
apparaît. Justement, où s'arrête la vérité historique et où commence la fiction
dans votre récit? C'est pour ça que je suis très attaché à la littérature.
J'utilisais le même système que le vieux écrivant du 19e siècle. Il y a un fonds
historique très réel avec cette date, avec ces personnages réels. Il y a aussi
des protagonistes de fiction comme Tristan Valdivia. Et même les personnages de
fiction sont construits avec de petites tranches de vie réelles. Parce que les
dialogues qu'il y a dans les livres, les anecdotes, sont de la mémoire de
quelqu'un qui m'a transmis telle ou telle anecdote. Donc on peut dire que dans
ces livres, c'est la même chose qu'avec les romans du 19e, il n'y a plus de
matière réelle que de fiction. Votre personnage Tristan, c'est un jeune marquis
qui quitte au début du tome 1 sa femme et Madrid, la capitale, pour retourner
dans son village des Asturis. Il n'a pas l'air bien. Il a cette phrase, je suis
un peu comme la République, pour l'instant je tiens le coup. Qu'est-ce qui lui
arrive? Il est un personnage qui est malade, il vient de Madrid, de la capitale,
il revient au Nord. C'est un système classique parce qu'il y a ce personnage-là
qui va croiser notre personnage, Isolina, qui est la fille d'un mineur qui
appartient à une autre classe sociale. Alors là on est entre les Capulets, les
Montaigus, c'est des amorches Shakespeareanes qui le vont vivre. Voilà, c'est un
système qui on a beaucoup beaucoup utilisé dans la littérature. On va croiser du
monde antagonique pour montrer aux lecteurs toutes les grilles de la palette des
couleurs, pour ne pas montrer qu'il y a un côté, et ça sera quand même une image
la plus réelle, la plus objective possible de l'époque et de la société. Oui,
puis on adhère, le lecteur adhère, il a envie absolument que cette histoire
d'amour entre un marquis et une fille de mineur fonctionne. Alors même c'est
vrai, c'est d'ailleurs la force de cette bande dessinée, c'est que même si votre
cœur il bat pour les ouvriers, Alfonso Zapico, vous traitez je dirais à égalité
dans la narration, dans la restitution des conditions de vie, et les
aristocrates et les mineurs. Il y a un objectif dans ces livres, même si c'est
un roman, un roman graphique, donc il y a une partie des fictions, mais essayer
d'être toujours très honnête, ça veut dire de tout montrer, de tout expliquer.
S'il faut parler de ce qui se passe dans le monde aristocratique, de
l'entreprise, des compagnies mineures, il faut tout expliquer, il faut entrer
aussi dans les foyers ouvriers, il faut entrer aussi dans expliquer la
République espagnole avant la guerre d'Espagne, et c'est déjà assez compliqué.
Moi j'avais un compromis d'auteur, de très honnête, mais pas adapté, l'histoire
a mes envies, mais tout expliqué au lecteur. Dites nous Alphonse aussi, il y
avait une bande-son de ce chant des Asturis que vous publiez, est-ce que ça
pourrait être ce chant? Oui, ça pourrait être, c'est une chanson de Titio
Sanchez Ferlocio, c'est une chanson qui est née d'une grève, d'une grève de
mineurs en 1962, ceux qui ont appelé la grève du silence parce que c'était
pendant la dictature, pendant le franquisme, et les mineurs, comme toujours, les
mineurs et des Asturis, ils ont été en première file, en avance, on va dire sa
mémoire, et c'est tout ça qu'ils ont fait, générer des chansons de réaction
nationale, internationale, et c'est pour ça que c'est plus douloureux arriver
aujourd'hui à l'oubli, parce que les passés, c'est très lourd, c'est un passé
quand même, plan des histoires dont on peut être très fier. Alors cette tension
que vous mettez en place entre fiction et grande histoire, vous la faites vivre
notamment à travers les pages d'un journal, la Noticia, alors on est obligé de
tourner la bande dessinée, je vois que vous jouez avec nos yeux, avec notre
concentration, c'est le journal du matin que j'ai sous les yeux, je voudrais
bien que vous nous disiez quels événements vous avez retenus, parce que là il y
a quand même votre grille de lecture à vous. Les journaux, c'est notre parti de
système narratif, un parallèle à cette histoire d'amour qui commence,
j'utilisais la première page d'un journal pour raconter ce qui se passe dans la
Catalogne où il y avait de conflits, les meetings politiques où on voit que la
température de l'actualité augmente, la gauche ouvrière menace de déclencher la
révolution, dit le journal, les gouvernements centrales annulent la loi de
contrat des cultures. Donc on voit là comment les gouvernements qui arrivent, un
gouvernement de droite, un réactionnaire, commencent à démanteler tout ce qui a
mis la République à son arrivée, parce que le problème c'était que surtout pour
les souffriers, pour les gens plus fragiles, la République c'était une illusion,
c'était l'illusion d'envoyer ses enfants à l'école, de gagner des salaires plus
dignes, d'avoir des conditions de travail plus dignes, tout ça c'était des
illusions qui ont commencé à disparaître en 1933, et tout ça aussi a déclenché
la révolution.