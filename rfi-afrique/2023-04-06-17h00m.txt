TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-06-17H00M.MP3
--------------------------------------------------------------------------------
... En raison d'un mot d'ordre de grève nationale, RFI n'est pas en mesure
d'assurer son programme habituel. Veuillez nous en excuser.... En Afrique, il
est bienvenue, soleil en continue, le fond de mer n'est place, tu seras jamais
déçu, la nature elle dessus, on boit de l'eau sans clore, on visite la faune et
découvre la flore, sac à dos vis-à-bas, ma courne approche, la famille se
rapproche, met la main à la poche, faut pas tomber dans le trou gris-gris autour
du cou, le palus sans métoque, seul doc c'est le marabout, continent plus grand
que les magasins continent, sans écran, viens voir la terre des immigrants, en
route pour Dakar, la merde te fout des claques, sort ton numérique, fais cla cla
cla, Kodak, aux filles on leur dit qu'on mène la grande vie, ça n'en vient pas
si cher, un petit tour en taxi, ça parle pas en wallop, négoces dans les
boutiques, crème anti-moustique, un thé, une bonne zik, yeah! Begunna, do
khentou man, bir afrika, afrika yosama reyou man, begunna, yeah! Fou mouti man,
atou man, j'emma sangéman, welcome to afrika de tout Ténin, yeah! En clair en
dents, on dit Jean Dieu merci, tout va mieux, le soleil brûlant, on parle, on
danse un peu, on se rouille et tu roules dans les concerts de classe, on est en
train de faire des boutiques, crème anti-moustique, un thé, une bonne zik, yeah!
Begunna, do khentou man, bir afrika, afrika yosama reyou man, begunna, yeah! Fou
mouti man, atou man, j'emma sangéman, welcome to afrika de tout Ténin, yeah! En
clair en dents, on dit Jean Dieu merci, tout va mieux, le soleil brûlant, on
parle, on danse un peu, on se rouille et tu roules dans les concerts de classe,
minibus, passion, c'est guinea connection, on se lave encore les mains, on
bouffe à douala, en danse cotonou, par-ci, bandit par-là, dédié à kikali aux
victimes de la haine, nommé jibé en jeans, sans cuir, sans gel, t'avances et je
vis de la France, elle te fatigue, direction libre, ville ou bras à ville, les
horaires, c'est l'horreur à Paris, pas pareil! Finis donc ton assiette, parlons
de ce qui va bien, regarde le temps qu'il fait, s'emmerde et y'a pas moyen,
vraiment pas besoin de te faire de soucis, profitez de la vie, voilà un tour de
magie! Beugna de Khentouman, guère Africa, Africa et Ossama, réhoman Beugna, le
Foumouti manatouman, je m'en sens chez moi, Welcome to Africa de tout Télalala
Mon trôve et mon prénom, bosser mon occupation, le rap à tourbillon, j'me suis
fait mon opinion, tu peux pas me louper, j'suis dans ma chaleu, je brille par
mon toupee, décalé, coupé, plonge dans l'façade, on oublie pas l'passé, il faut
pas être pressé, c'est safari en français, quoi de neuf, c'est un coup, on tient
encore le coup, du côté de chez nous c'est des vacances de fous! Beugna de
Khentouman, guère Africa, Africa et Ossama, réhoman Beugna, le Foumouti
manatouman, je m'en sens chez moi, Welcome to Africa de tout Télalala safari,
safari, safari, safari, safari, safari, safari M'en bats qu'on a joué, tout le
jeu qu'on a joué, ça pas fait pour moi, ou toujours qu'à zérer qui oublie mon,
même si aucun temps meurt, m'en fait pour l'amour pas pour j'ai aucun jouet,
m'en met pour l'immière, m'en capaisir n'a tout j'ai aucun jouet, m'en pas qu'à
réclé. Qui j'ai, qui j'ai, qui j'ai aucun jouet? N'ont pas qu'à réjouer, qu'à
s'injurer, n'ont pas plus vifli, tout qu'en temps, moi, tout le monde m'aurait
dit, à sa tour, et qu'à me dire, qu'une joue me savait qu'il m'aurait qu'y
aller, j'en ai qu'à arriver, à m'outre les poux jouets, j'ai aucun jouet, même
pas les jouets. Qui j'ai, qui j'ai, qui j'ai aucun jouet? Qui j'ai, qui j'ai,
qui j'ai aucun jouet? Qui j'ai, qui j'ai, qui j'ai aucun jouet? Non, non, non,
moi je dis non, non, non N'y m'aie, n'y aie, mon goulil, ma fous pourrie, j'ai
Non, non, non, moi je dis non, non, non N'y m'aie, n'y aie, mon goulil, ma fous
pourrie, j'ai Non, non, non, moi je dis non, non, non N'y aie, n'y aie, mon
goulil, ma fous pourrie, j'ai Non, non, non, moi je dis non, non, non N'y aie,
n'y aie, mon goulil, ma fous pourrie, j'ai Non, non, non, moi je dis non, non,
non J'ai une petite chambre, je ne sais pas où elle est Je ne sais pas où elle
est Je ne sais pas où elle est Non, non, non, moi je dis non, non, non N'y aie,
n'y aie, mon goulil, ma fous pourrie, j'ai N'y aie, n'y aie, mon goulil, ma fous
pourrie, j'ai Eh chérie, bébé, ne bilez pas Eh chérie, bébé, ne bilez pas Chimoy
Chimoy, y'a un calabi, pour demain Chimoy, y'a un calabi, pour demain Sama kerni
jaye, luna mai Chimoy, y'a un calabi, pour demain Sama kerni jaye, luna mai N50,
le agenda, sound armam Kouima, binakale, numutudu Kouma, ko Jatakulli, ninjai,
kulli Jatakulli, ninjai, kulli Jatakulli, ninjai, kulli Eh bah! A écouter sur
toutes les applications de podcast et sur RFI.fr N.Y.K. dans lequel notre invité
a passé 20 ans, joué avec d'autres grands noms et pleinement participé au
bouillonnement créatif de la Grosse-Pomme 10 titres comme autant de chapitres
d'un recueil de nouvelles ou comme les étapes d'un voyage musical qui reflète
son parcours et ses inspirations et qui rendent hommage à l'histoire et aux
habitants du quartier de Harlem The Harlem Suite, le nouvel album de Jacques
Chansmart, est disponible chez Rapid Opt Records, à votre distribution et vous
m'en direz des nouvelles. Bonjour, salutations Jacques Chansmart. Oui
salutations au soleil, vous voulez vous? Voilà, Sun Salutations, c'est le nom de
ce morceau qui ouvre l'émission et qui ouvre aussi ce nouvel album. Le caribéen
que vous êtes, se devait de dire bonjour au soleil pour commencer? Oui en fait,
vous savez, à New York les bâtiments sont tellement grands, pour quelqu'un
d'insulaire comme moi, on devient un petit peu, on est en manque de soleil ici.
Les gratte-ciel caffent l'allure du soleil. Ah oui, il faut vraiment,
littéralement se tordre le cou et guetter les petites oasis de soleil entre les
immeubles. Et je me rappelle qu'un hiver il avait fait sombre pendant environ
deux semaines, et un matin le soleil nous a aveuglé de façon extraordinaire et
libératrice. Et je trouve aussi que plus tard dans cette journée, j'ai reçu un
appel pour tourner avec D'Angelo. Et donc il y avait une sorte de confluence, de
moments de lumière si vous voulez. Et j'ai écrit le lendemain ce morceau, «
Sense of Alutation ». Alors vous connaissez peut-être la chanson d'André
Comatias, « Les gens du Nord » avec ses mots, les gens du Nord ont dans le cœur
le soleil qu'ils n'ont pas dehors. Est-ce que vous diriez la même chose des gens
de Harlem? Oui, absolument. D'ailleurs c'est le thème de mes morceaux « Twisted
», je ne sais pas s'il est sur votre liste. Eh bien si, on va l'écouter juste
après. Mais ça tombe bien, vous faites les enfaînements vous-même. D'instinct,
écoutez, oui c'est complètement le cas pour les gens de Harlem. Et je décris un
type de personnage qu'on rencontre aussi à Harlem, ce sont des gens qui semblent
avoir été contorsionnés de l'intérieur et de l'extérieur. Contorsionnés, tordus
quoi. Oui, oui, oui. Et donc meurtris je dirais plutôt. Meurtris, oui. Et
pourtant? Et pourtant ce sont eux qui trouvent le bon mot pour rire et les mots
d'encouragement, les mots d'espoir, qui vous motivent pour continuer. Et donc
oui, le soleil dans le cœur c'est complètement ça. Eh bien on écoute Twisted. On
écoute Twisted. On écoute Twisted. Twisted, extrait du nouvel album de Jacques
Schwarzbart avec nous aujourd'hui sur RFI. Et ce morceau là finalement il
illustre ce que l'on disait à l'instant, il illustre dans le tempo, dans la
modulation finalement à la fois c'est haut et c'est très bas mais toujours cette
capacité de rebond des habitants de Harlem. On est dans le jazz d'évocation pour
le coup? Oui, on est dans le thème des vies transitoires où les étapes
s'enchaînent constamment et il y a peu de permanence. Et donc c'est aussi ça
l'habit du jazzman en fait. Aujourd'hui on joue avec tel groupe, demain on joue
dans un autre et on est en mutation constante et on suit un processus
d'adaptation accélérée. Mais on s'adapte à soi-même, on s'adapte aux autres? Non
seulement on s'adapte à la musique des autres, on s'adapte au caractère des
autres, à la dynamique du groupe en particulier, on s'adapte au voyage, donc à
des environnements climatiques différents, microbien différents. On est
complètement dans une adaptation quasi-incentanée et continue. Alors on parlait
de tempo et il y a un instant je voudrais qu'on réentende le tout début de
Sunsallutation parce que bon, Hedrico Macias nous a fait dériver peut-être un
petit peu trop vite mais pour le plus grand bonheur, j'espère, de certains de
nos auditeurs, je voudrais qu'on réécoute les toutes premières mesures du
morceau qui ouvre le disque. Et on se dit tout de suite en écoutant ces notes
là, les notes qui sortent de votre saxophone, Jacques Schwartzbart, eh bien que
c'est un sacré exercice d'agilité pour les doigts pour le coup ce morceau là.
Pourquoi commencer un album par un morceau aussi rapide? Écoutez, c'est juste un
coup de fouet, c'est comme lorsqu'on entend quelqu'un qui vous siffle fort dans
la rue ou qui vous appelle, on est obligé de tourner la tête, de se dire mais
qui m'appelle? Vous voulez sortir les gens qui vous écoutent, les gens qui vont
découvrir ce disque de leur torpeur? Oui, absolument et de leur rappeler qu'il y
a quelque chose de magnifique dans la vie. Souvent, surtout lorsque je vais dans
les villes, je trouve une monrosité un petit peu générale. Et voilà ça. Plus
qu'avant, est-ce qu'il y a eu un avant et un après Covid, vous qui justement
voyagez beaucoup? Absolument, non, c'est indubitable, ça se voit un petit peu
partout et justement, non seulement pour moi-même mais pour les autres, Sun
Salutation, c'est un appel à la vie si vous voulez, un rappel aussi de ce qu'on
a perdu mais qu'on peut retrouver en fait. Vous parliez de coups de fouet à
l'instant Jacques-Chortes-Bartes, est-ce que c'est ce que vous avez ressenti la
première fois que vous avez posé le pied à Arlem? Absolument, ce n'est pas
exactement un coup de fouet mais je dirais une conscience violente de ma liberté
personnelle. Alors expliquez-nous, qu'est-ce que vous avez ressenti à ce moment-
là? J'ai ressenti que dans cet environnement, je pouvais être exactement ce que
je voulais être et que personne ne me jugerait. C'est un sentiment que je n'ai
jamais eu parce que tant dans ma petite île où tout le monde se connaît et tout
le monde se surveille un petit peu qu'à Paris où les gens s'observent suivant
leurs apparences et s'il y a quelqu'un qui dénote un petit peu, les gens
chuchotent etc. Là-bas, j'ai rencontré un monde où il y avait une diversité
absolument étonnante et justement si étonnante et si vaste que personne ne juge
qui que ce soit. Ça veut dire que pour le petit Guadeloupéen que vous étiez,
Jacques-Chortes-Bartes, Arlem, New York, c'était la porte d'entrée idéale dans
un autre monde? Absolument. Et à la fois dans une porte d'entrée vers toutes
sortes de monde extérieur à moi-même mais aussi intérieur à moi-même puisque
c'est là que je me suis laissé créer et approfondir le concept par exemple de
gros cas de jazz. Alors expliquez-nous parce que là c'est quoi le gros cas de
jazz? C'est cette espèce de fusion entre les rites et les mélodies du gros cas,
donc cette musique afro-caribéenne qu'on trouve en Guadeloupe, dérivée des
musiques rituelles des religions africaines et du jazz. Donc cette fusion que
j'ai réalisée avec mon disque qui est sorti sur Universal en 2006, Soné Kala. Et
si j'étais à Paris en Guadeloupe, donc à Paris ça n'aurait pas été suffisamment
jazz et en Guadeloupe ça n'aurait pas été suffisamment gros cas. Et les gardiens
du temple ici et là m'auraient dit mais qu'est-ce que tu fais etc. Soit tu fais
du gros cas, soit tu fais du jazz. Il faut choisir. Et vous vous avez dit ben
non on va pas choisir ce serait bête. Et au contraire ce que j'avais toujours vu
depuis petit en fait c'est l'extension et la fusion naturelle des musiques afro-
caribes et du jazz. Et c'est plus tard que j'ai appris que les gens qui ont créé
le jazz sont des afro-caribes des îles françaises. Juste au retour des choses.
D'Haïti, de Guadeloupe, de Martinique qui se réunissait au Congo Square à la
Nouvelle-Orélande. Et donc qui comme Jerry Martin qui était un Haïtien
vaudouisant et chryolophone comme nous en fait, comme les Guadeloupéens, comme
les Martiniquais. Et donc en fait j'avais perçu très très vite étant petit qu'il
y avait une filiation et quelque chose à faire pour renouer la fleur et la
racine. Et étant petit disiez-vous à l'instant Jacques-Joseph Smart, à quel âge
avez-vous commencé à écouter du jazz? Tout petit en fait. Mes parents avaient
quatre ou cinq morceaux, disques vinyles de jazz que j'ai usés jusqu'à la corde.
Et donc dès l'âge de 3 à 4 ans. À 3 à 4 ans on peut entrer, on peut se laisser
transporter par les émotions du jazz? C'était mon cas en tout cas. J'étais
vraiment excité chaque fois que j'écoutais ce disque de Charles Mingus. Tant les
compositions qui un petit peu à l'instar du chemin que j'ai choisi comprenait
beaucoup d'influences différentes si vous voulez. On entendait du gospel, du
blues, de la musique contemporaine et tout ça mélangé dans le swing américain.
Il y avait même un morceau dédié à Haïti. Et donc je me suis dit que à travers
le jazz on pouvait s'ouvrir sur l'univers. Et non seulement l'univers des hommes
mais on l'a vu aussi avec Coltrane, l'univers dans son entité complète, dans sa
globalité spatiale. Et vous évoquiez Coltrane, et bien justement quand vous
étiez enfant vous avez sans doute écouté ça. C'est évidemment les Queen Hugs en
1964. Et puis on aura évidemment des grands tubes de John Coltrane. Qu'est-ce
que vous avez ressenti en écoutant ces quelques notes chaque short part? Je me
suis senti hanté tout de suite. J'ai dû découvrir ce morceau lorsque j'avais 4-5
ans. C'est un morceau qui est resté en moi. Et d'ailleurs lorsque j'ai commencé
le saxophone c'est le premier morceau que j'ai tenté de jouer. Et donc je me
rappelle avoir passé mon diplôme de sciences pro et je suis retourné en
Guadeloupe pour passer des vacances et célébrer avec la famille et les amis.
J'avais une amie qui adorait collectionner toutes sortes d'instruments. Elle
avait un saxéneur sur son lit et je lui ai dit, est-ce que tu peux me le monter?
Quelle sensation j'aurais de souffler à un instrument qui est pour moi tellement
mystique depuis si longtemps puisque j'étais fan de Coltrane depuis tout petit.
Donc elle me l'a monté et au bout de 20 minutes j'ai réussi à jouer cette petite
mélodie de Coltrane. Ensuite je me suis aventuré dans quelques phrases de Big
In. Et il y avait des amis qui étaient dans la chambre d'à côté qui avaient un
petit groupe et qui m'ont dit, tu sais on a besoin d'un saxophoniste demain,
est-ce que tu es libre? Je leur ai dit, mais je viens de commencer à jouer il y
a 20 minutes, je ne suis absolument pas prêt pour me présenter devant un public.
Moni, arrête tes conneries, personne ne te croit ici, à même ton saxophone. Je
lui ai dit, mais ce n'est même pas mon saxophone! C'est incroyable, à quoi ça
tient ces histoires incroyables? Et donc cet été j'ai joué entre 4 et 7 fois par
semaine avec ce petit groupe. Et c'est dire effectivement si ce morceau Equinox
de Coltrane a beaucoup compté pour vous et pour la suite de votre carrière
musicale. C'est vraiment la fondation de tout ce que j'ai fait. A tel point que
sur ce nouvel album, Jacques-Choix-Smart, vous avez arrangé Equinox et votre
version à vous, elle s'appelle Equivox. Alors expliquez-nous un petit peu ce
travail de réarrangement Jacques-Choix-Smart. Comment est-ce que vous êtes passé
d'Equinox à Equivox? Déjà j'ai changé la métrique rythmique, c'est à dire que
j'ai imposé un rythme de 4 sur des mesures de 3 temps. En transformant les
quadruplets en backbeat. C'est ce qu'on voit par exemple au début. Donc c'est ça
qui change complètement la perspective. Il y a là-dessus cette espèce d'harmonie
impressionniste glissante qui donne toutes sortes de couleurs différentes à
cette note que je tiens. Donc qui sert de point d'ancrage à ces harmonies qui
tournent un petit peu en spirale. Et la fin du thème retrouve en fait le swing
de John Coltrane. Comme une sorte de clin d'œil à la source si vous voulez. Donc
ça devient une composition polymorphe si vous voulez. Coltrane c'était un homme
de défis, il voulait en permanence se dépasser sur le plan technique, explorer
aussi de nouveaux territoires. Bon les nouveaux territoires de ce point de vue
là, je sais que vous aussi aimez beaucoup les explorer. Mais ce côté dépassement
de soi-même sur le plan technique, vous fonctionnez aussi comme lui? Écoutez, me
comparer à Coltrane d'une façon même complètement éloignée, je ne peux pas me le
permettre. Mais j'essaie de m'inspirer de tout ce qu'il a fait bien entendu, y
compris cette quête de dépassement qui me semble absolument élémentaire et
essentielle à tout travail de créateur. Alors sur l'album il y a un titre qui
s'appelle Central Park North, là c'est une réponse au Central Park West de
Coltrane, notre clin d'œil. Absolument donc Central Park West c'est une partie
de New York qui est très huppée, où on peut s'imaginer très bien conduire une
voiture de luxe en sirotant un petit verre de champagne dans sa cadillac si vous
voulez. Et j'ai voulu aller plus au nord parce que lorsqu'on marche au delà de
la 110e rue, on trouve tout d'un coup des immeubles délabrés, des baudégats, des
gens qui ont subi beaucoup de choses et donc ça se voit dans leur physique et
dans leur regard. Je voulais décrire plus cette réalité là, ça m'intéresse plus
que Central Park West. Et Central Park tout court, alors Jacques-François
Sberdert, est-ce que vous aimez vous balader dans Central Park, est-ce que vous
êtes un flâneur? Je suis un amoureux de la nature. Lorsque j'étais petit, je
m'en allais parfois pour plusieurs journées tout seul dans la forêt et il
m'arrivait de dormir à même le sol, d'aller pêcher, d'aller chasser juste pour
me nourrir et continuer à flâner dans la jungle si vous voulez. Et quand vous
marchez, il y a des rythmes, des ambiances musicales, des mélodies, des notes
qui vous viennent comme ça en tête? Ce qui m'inspire dans la nature, ce sont mes
années de plongée et de pêche sous-marine. Le monde sous-marin est littéralement
un monde extraterrestre, pas au-delà de la planète, mais au-delà de la terre
ferme qu'on connaît. Et je pense que si on rencontre des formes de vie à
l'extérieur de cette terre, certaines seront certainement inspirées de ce qu'on
trouve sous l'eau. On trouve des morphologies absolument impossibles et tant
qu'on ne s'est pas immergé dans ce monde, on ne peut pas imaginer à quel point
ça peut être divers et surprenant. Time Travel, nouvel extrait du nouvel album
de Jacques Schwarzbart, The Alan's Suite, dans VMDN sur RFI. On pourrait
traduire par « Voyage dans le temps ». On sait bien ça, Jacques Schwarzbart. On
voyage dans le temps dans ce morceau, on voyage entre les rythmes aussi, on
voyage aussi entre les musiques. C'est un morceau qui montre justement votre
souci de marier des origines, des traditions, l'ancien, le présent, les
continents. Oui, c'est aussi un voyage des rythmes. Parce que « time » c'est
aussi « rythme » en anglais, si vous voulez. Donc c'est une façon de faire se
parler toutes sortes de rythmes différents et aussi des rythmes d'origine
différentes. On y retrouve des saveurs afro-caraïbes et même un peu afro-
latines, bien sûr arrangées à ma façon, avec des métriques en 5, etc. Mais qui
sont dansantes en fait. On explore ensuite des syncopes très modernes et du jazz
nouveau. C'est un petit peu ça d'ailleurs, l'élément essentiel de la modernité
du jazz, c'est l'exploration des rythmes. C'est ce qui distingue le jazz
d'aujourd'hui, même du hard bop d'il y a une quinzaine d'années. Est-ce qu'il y
a un vrai syncrétisme naturel entre tous les rythmes de la diaspora africaine et
du continent? Absolument. Mais il faut que ça parte d'une connaissance réelle.
C'est-à-dire qu'il y a forcément une dimension savante dans la façon d'agencer
et de marier tous ces rythmes? Absolument. J'entends parfois des projets où des
gens se rencontrent sans connaître le langage de l'autre. Et pour moi ça fait
toujours collage. J'entends des jazz man qui flottent un petit peu dans tous les
sens sur des rythmes qu'ils ne comprennent pas. Et d'un autre côté des musiciens
traditionnels de telle ou telle partie de l'Afrique ou des Caraïbes qui ne
comprennent pas vraiment le langage du jazz qui est parlé par-dessus tout cela.
Ce sont des projets auxquels j'ai été invité mais que j'ai toujours refusé parce
que le collage ne m'intéresse pas. Mais ça veut dire, si je vous entends bien,
que l'improvisation c'est très important quand on est dans le monde du jazz,
l'impro, mais que toute improvisation, tout essai est forcément préparé. Il y a
une incompréhension fondamentale dans le mot improvisation. L'improvisation dans
le cadre du jazz c'est la composition dans l'instant. Et qui dit composition
implique une science de la composition. Donc une étude profonde de tout ce qui
fait l'harmonie, de tout ce qui fait le développement des thèmes, des mélodies,
la syncopation, les rythmes de tous genres, la façon dont ils s'imbriquent. Et
sans une science analytique profonde de tous les éléments qui font les
différents langages impliqués dans une création, je pense qu'on est à côté de la
création, on est dans le collage. Et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et et et et et et et et et et
et et et et et et et et et et et et et et et et et et