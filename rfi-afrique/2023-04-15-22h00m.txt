TRANSCRIPTION: RFI-AFRIQUE_AUDIO/2023-04-15-22H00M.MP3
--------------------------------------------------------------------------------
 Bienvenue sur Radio France International, il est 22h à Paris, 21h à Kinshasa.
Et bonsoir à tous au sommaire de ce journal. En France, l'exécutif assume la loi
sur la réforme des retraites a été promulguée par le président dès cette nuit,
une promulgation éclair dénoncée par les syndicats. Dans ce journal également,
le président du conseil européen Charles Michel dénonce des intimidations. Il
est accusé d'avoir réalisé de trop nombreux déplacements dernièrement, et
surtout des déplacements coûteux et polluants. En Chine, les réfugiés nord-
coréens craignent d'être expulsés vers leur pays d'origine. Les frontières entre
les deux pays devraient bientôt réouvrir. Et puis du football, le PSG vient
parti pour s'asseoir un peu plus confortablement au sommet du classement de
Ligue 1. Les Parisiens dominent en ce moment, lancent 3 buts à 0. C'est
l'information du jour en France. La loi sur la réforme des retraites déjà
promulguée par le chef de l'Etat Emmanuel Macron s'est passée cette nuit,
quelques heures seulement après que le conseil constitutionnel se soit prononcé.
Un tempo, une rapidité critiquée largement par les opposants à la réforme. Les
syndicats s'estiment méprisés et parlent de leur incompréhension. Une nouvelle
journée de mobilisation est prévue le 1er mai, mais avant cela, dès jeudi
prochain, les syndicats de la CGT appellent à la grève. De son côté, le
gouvernement assume, ça ne servait à rien d'attendre, estime le ministre du
Travail, Olivier Dussopt. La constitution dit que lorsque le conseil
constitutionnel a rendu son avis, le président de la République doit, ce n'est
pas peu, promulguer dans les 15 jours qui suivent. Nous avons fait le choix de
promulguer dès après la décision, comme c'est le cas pour toutes les lois de
finance de l'Etat ou la sécurité sociale. Elles sont toujours promulguées dans
les 24 heures qui suivent l'avis du conseil constitutionnel. Alors que les
syndicats demandaient aux chefs de l'Etat d'attendre? Pardon, mais j'ai entendu
cette demande. Quelle différence cela faisait d'attendre 3 jours, 4 jours ou 5
jours alors que la loi est votée et validée et donc elle doit être publiée en
l'Etat? Et certains opposants à la réforme des retraites ont décidé de ne pas
attendre le 1er mai pour manifester à nouveau des rassemblements à Paris ce
samedi de quelques centaines de personnes. À Rennes, la préfecture parle
d'attroupements violents. Plusieurs personnes ont été interpellées. Enfin, pas
sûr que ces propos soient de nature à apaiser. Alors que la crise sociale se
poursuit dans l'Hexagone, la Premier ministre Elisabeth Borne affirme que
l'exécutif est déterminé à accélérer les réformes pour bâtir une France du plein
emploi. L'ONU, les Etats-Unis ou encore la Russie appellent à un cessez-le-feu
immédiat. Au Soudan, le réveil a été douloureux pour les 45 millions
d'habitants. Tire d'armes à feu, explosion à Khartoum, la rivalité entre deux
généraux a dégénéré. Et depuis ce matin, des affrontements entre d'un côté
l'armée régulière du général al-Buhane et de l'autre, les paramilitaires de
Mohamed Hamdan Daghallo. Des combats se poursuivaient encore il y a quelques
heures. La situation au Soudan, on vous en parle également sur RFI.fr. La guerre
au Yémen et les libérations de prisonniers qui se poursuivent. Plus de 300
prisonniers libérés hier. Et ce samedi, un nouvel avion est parti d'Arabie
Saoudite, direction la capitale du Yémen, avec une centaine de rebelles outils à
l'intérieur. Ces libérations se déroulent sur fond de pourparlers, des
pourparlers pour mettre fin au conflit qui ravagent le Yémen depuis plus de 8
ans maintenant. RFI, il est 22h, passé de 3 minutes à Bruxelles. Il dénonce des
intimidations. Le président du Conseil européen, Charles Michel, a riposté dans
la presse cette semaine les journaux Le Monde et Politico, épingle en effet le
haut responsable européen, pour des déplacements nombreux et surtout très
coûteux et très polluants. Mais Laura Broulard, Charles Michel tient à donner sa
version des faits. L'article du Monde, paru le 9 avril, révèle l'explosion des
frais de déplacement de Charles Michel et de ses délégations et son goût
prononcé pour les avions privés. En 2024, ses voyages devraient coûter près de 2
millions d'euros contre 500 000 environ pour ses prédécesseurs. L'enquête de
Politico, sortie fin mars, décrit un président du Conseil très critiqué à
Bruxelles plus impliqué dans ses voyages à l'étranger que dans ses fonctions au
sein de l'UE. Dans la presse belge, Charles Michel a rendu coup pour coup. S'il
voyage beaucoup, c'est parce que le contexte international l'exige, avec d'abord
la pandémie de coronavirus, puis la guerre en Ukraine. Et s'il utilise souvent
des avions privés, y compris pour des trajets courts, c'est pour répondre à un
agenda chargé. Surtout, ces déplacements sont organisés par des fonctionnaires
indépendants sur lesquels il n'a pas d'autorité directe. L'ancien Premier
ministre belge dénonce une tentative de déstabilisation et d'intimidation liée à
sa volonté de faire exister l'UE sur la scène mondiale ou à des jeux de pouvoir
au sein des institutions internationales. L'orbre ou l'arbre Bruxelles, RFI. La
répression se poursuit en Iran. La police annonce qu'elle va sanctionner de
façon plus stricte les femmes qui ne portent pas le voile, la technologie
devrait même être utilisée pour identifier les personnes qui enfreignent la loi.
Une annonce faite alors que de plus en plus de femmes en Iran défient le code
vestimentaire obligatoire. Un proche de l'opposant Ousmane Sonko interpellé est
placé en garde à vue au Sénégal. Basirou, Dio Maïfaï et le numéro 2 du parti
Pastef. Il est accusé de diffusion de fausses nouvelles ainsi que outrage à
magistrats. L'opposant sénégalais a récemment critiqué le comportement de
certains magistrats au Sénégal et ce sur les réseaux sociaux. On en vient à
présent à ce sentiment d'inquiétude pour les réfugiés nord-coréens qui vivent en
Chine. Ils connaissent un moment de répit depuis le début de la pandémie de
Covid-19 car depuis cette période la frontière entre la Chine et la Corée du
Nord est fermée et de fait les expulsions de réfugiés nord-coréens sont donc
interrompues. Sauf que les frontières en question pourraient bien être
prochainement rouvertes et que des vies pourraient basculer. A Séoul, Célio
Fioriti. Depuis trois ans et le début de la pandémie de Covid-19, les expulsions
de réfugiés nord-coréens hors de Chine se sont arrêtées. Mais alors que la
frontière entre les deux pays pourrait prochainement réouvrir, les Nations Unies
et les ONG alertent sur le sort réservé aux réfugiés à leur arrivée en Corée du
Nord. Ces derniers risqueraient emprisonnement, travaux forcés et même torture
pour avoir fui le pays. Elisabeth Salmon, rapporteuse spéciale sur la Corée du
Nord aux Nations Unies s'exprimait le mois dernier. Avec la fermeture des
frontières, plus d'un millier de réfugiés nord-coréens sont retenus indéfiniment
en Chine. Leur rapatriement forcé risque de les envoyer dans des « quannes
lisseaux », des camps de prisonniers politiques. D'après le DLNK, média
spécialisée sur la Corée du Nord, la police chinoise a déjà commencé à
intensifier ses contrôles, notamment des téléphones, pour débusquer les réfugiés
nord-coréens. Pour Pékin, ces derniers ne sont pas considérés comme des réfugiés
ou demandeurs d'asile, mais des immigrés illégaux, ce qui justifie leur
expulsion du territoire. Le représentant américain pour la Corée du Nord, Sung
Kim, a appelé la Chine à « jouer un rôle » pour arrêter les violations des
droits humains et a rappelé les obligations prévues par les Nations Unies au
sujet du traitement des réfugiés. À ce jour, la frontière entre les deux pays
reste close, mais l'arrivée d'un émissaire chinois à Pyongyang au début du mois
d'avril donne l'hypothèse d'une réouverture prochaine. Selon les observateurs,
la Corée du Nord pourrait officialiser l'ouverture de sa frontière après le 15
avril, l'anniversaire de Sung Kim fondateur du régime. Cédric Furetti, Séoul
RFI. L'actualité, c'est également la France qui tente de rassurer les autorités
de Taiwan, alors qu'Emmanuel Macron a tenu des propos controversés récemment, le
président français a exhorté l'Europe à ne suivre ni les États-Unis ni la Chine.
Sur le dossier de Taiwan, un ministre français l'assure aujourd'hui. La France
soutient la paix depuis le début et s'oppose à toute modification du statu quo.
En football, c'est une affiche à l'allure de petite finale. Le PSG accueille
l'Anse depuis une heure. On parle bien sûr de Ligue 1. Les Parisiens ont
l'occasion d'asseoir un peu plus leur première place au classement. Et c'est
clairement bien parti, en direct du Parc des Princes à Paris. On retrouve Cédric
de Oliveira. Bonsoir Cédric, les Parisiens dominent déjà les cents et or. Trois
buts à zéro. Bonsoir et le jeu vient de reprendre. Il y a quelques instants en
deuxième période et vous l'avez dit, c'est bien parti puisque Paris mène 3 à 0
face à Deslansois. Réduit à 10 quand même on le rappelle. Et c'est sans doute le
tournant du match à la 19ème minute. Et c'est une exclusion de Salis Abdulsamed,
le gagné pour une vilaine semelle sur la cheville d'Astra Fakimi. Alors que les
Parisiens progressent, là un centre de Kylian Mbappé qui ne donnera rien. Ce
ballon va sortir en touche. Carton rouge logique donc, on revient sur cette
exclusion. L'Anse dominait avant ça. Mais ensuite, ça a été plus compliqué.
Paris a fait la différence en 10 minutes. D'abord avec la frappe croisée de
Kylian Mbappé, poteau rentrant 1-0. Le 20ème but de Mbappé en Ligue 1 cette
saison. Puis ensuite le tir le lointain de Vitinha, le portugais, et derrière
l'enchaînement entre Messi et Mbappé, l'argentin qui s'est appuyé sur le
français et qui lui remet parfaitement dans la surface d'une talonnade. Frappe
croisée derrière du gauche et ça fait donc 3-0 pour ce PSG qui n'avait pas
montré grand chose avant. Et bien ça a supérieurité numérique. A priori, le
match est plié. C'est dur pour Leslansois, scénario cruel. Ils vont essayer de
limiter les dégâts maintenant. Mais Paris semble bien parti. Et en cas de
victoire, Adan Safri donc 9 points d'avance pour ces Parisiens. Et peut-être une
petite bouffée d'oxygène pour Christophe Galtik qui a passé une semaine
compliquée. Lui qui est embroué dans une polémique après des accusations de
racisme avec son ancien club de l'OGC Nice 3-0. On joue depuis 52 minutes au
Parc des Princes. Et on imagine en effet la déception pour Leslansois. Merci
beaucoup Cédric de Oliveira en direct du Parc des Princes à Paris. Plutôt dans
la journée Reims, déplacée à Rennes. Reims largement dominé par les Bretons. 3
buts à 0. C'est la fin de ce journal. Très belle soirée sur RFI. La marche du
monde, Valérie Nivellon. Tout à l'heure à partir de 0h10 TUE. RFI. Music du
monde, Laurence Alois. Stephen Hensley. Music du monde sur RFI. Bienvenue dans
les musiques du monde, dans la session live, enfin les sessions live. Parce que
nous recevons deux groupes aujourd'hui. Des colombiens de Bogotin, la Sonora
Mazuren. Très allumés comme toute la jeune scène Kumbia Chicha, Psykeli Ibre de
la capitale. Ils seront là dans 25 minutes à peu près. Mais notre première
invitée a une chaise à son nom dans le grand studio. C'est une régulière de
l'émission. Elle s'appelle Sandra Nkake. Alors elle chante, elle écrit, elle
s'est mise à la guitare, elle fait du cinéma. Parfois, elle a nommé une émission
de télévision sur TV5. Et dans cet emploi du temps de ministre, elle a trouvé le
temps de fabriquer avec son binôme Sweet Home, le flutiste Jidru. Son quatrième
album, Scars, qui veut dire les cicatrices mais aussi les failles dans le
mental. La vulnérabilité, la vérité, l'authenticité. La consultation démarre
donc en chanson avec le titre Sisters. Et après on se dit toute la vérité. Enfin
presque. Bonsoir. As your lips turn to blue When the storm blows the dew I'll be
around just for you And together with them I'll come running Don't be afraid
We'll come running Don't quit the race We are sisters Sisters, daring hearted
Oh, we are sisters Carrying, sister You know love goes all the way back to you
Oh, love goes, love We're all fighters in tears We'll wipe away all your fears
Don't let the blades get you down Hand in hand we'll hold round We'll come
running Don't be afraid We'll come running Don't quit the race We are sisters
Sisters, daring hearted Oh, we are sisters Carrying, sister You know love goes
all the way back to you Oh, love goes, love Come on and reach out for my hand
Together we'll chase the pain Your love is lifting me higher I'm telling you no
place is safer now No one can break the bond Your love is making me stronger
Come on and reach out for my hand Together we'll chase the pain Your love is
lifting me higher I'm telling you no place is safer now No one can break the
bond Your love is making me stronger Cause we are sisters Sisters, daring
hearted Oh, we are sisters Carrying, sisters Sisters, daring hearted Bonjour
Sandra, on voulait vous abonner, j'ai oublié La famille est la famille que l'on
se construit, elle n'est pas définie par le sang et les gènes Les femmes ont
décidé une route sans attendre qu'on leur donne la permission Ces femmes me
donnent de la force et continuent à m'en donner La toute première qui continue à
habiter en moi c'est Nina Simone, ce chemin de musicienne indépendante, autrice,
compositrice, militante Il est important et Sisters c'était assez évident pour
moi que cette chanson devait s'appeler comme ça d'abord Parce qu'on parle
beaucoup de sororité et que les mots sont importants et c'est bien de savoir
nommer les idées Nina Simone qui a une carrière, elle n'était pas du tout amenée
à faire ce type de musique, c'est quelqu'un qui aurait voulu être une pianiste
classique Et que son grand regret c'est de ne pas avoir enregistré d'albums de
musique classique occidentale, couleur de la peau, encore une fois Est-ce que
c'est aussi ce lien que vous comprenez avec Nina Simone? Vous comprenez mieux
que peut-être quelqu'un d'autre, quelqu'un comme moi, sa douleur et cette
agressivité qu'il y avait parfois parce qu'il fallait qu'elle s'impose et ça se
fait pas dans la douceur? Alors pour être très honnête avec vous, je pense que
ça ne se passe pas au niveau de l'intellect mais vraiment au niveau des tripes
Je sais pas si je la comprends mais je la sens et ça depuis toute petite j'avais
l'impression que quand on entend enchanté que je, oui que je comprenais dans le
sens prendre avec Que je prenais cette douleur et que, et en même temps quand
j'écoute ces interviews ce que je retiens c'est freedom is no fear La liberté
c'est de ne pas avoir peur et qu'elle espère un jour pouvoir être libre et moi
j'espère un jour que je serais adulte et que je serais libre aussi et que je
n'aurais plus peur Et elle fait partie des personnes qui me donnent cette force,
cet élan d'essayer de dessiner une route qui va me ressembler et qui, sur cette
route il y a plein de choses Évidemment de l'exil, de la douleur, de la violence
mais il y a aussi beaucoup de joie, beaucoup de tendresse, s'autoriser, la
tendresse, la lenteur, la douceur Parce qu'elle avait ça aussi, parce qu'on a
tendance à ne mettre en avant que son caractère, que son tempérament, que sa
colère et c'est vrai et c'était une colère juste Mais elle avait aussi beaucoup
d'humour, beaucoup de tendresse, beaucoup d'amour, beaucoup de douceur Je pense
notamment à une chanson qui s'appelle Little Girl Blue qui est juste
merveilleuse et même dans sa manière de jouer du piano il y a une délicatesse,
une poésie, évidemment un groove incroyable Mais moi surtout ce qui me... Par
exemple la chanson Four Women, l'évolution de l'interprétation en fonction des
femmes qu'elle évoque est tout simplement unique, j'ai jamais entendu ça depuis
Four Women je pense à quatre autres femmes, à Raphaël Anadère, Jeanne Haddad,
Camelia Jordana et vous même sur scène Je pense aussi à La Chica avec qui vous
avez joué au Café de la danse, j'aime beaucoup cet artiste aussi parce qu'on a
parlé ces derniers temps beaucoup de cet artiste espagnol qui s'appelle Rosalia
Et à chaque fois je dis vous savez qu'on a une Rosalia en France depuis
longtemps, elle s'appelle La Chica Et donc c'est quoi cette famille, c'est quoi
l'adjectif que l'on pourrait donner à ce groupe de femmes que vous avez formé
sur scène le temps de certaines protestes, qu'est-ce que vous avez en commun? La
lutte, l'indépendance Alors je crois qu'on a beaucoup de choses en commun, une
forme de résistance, d'envie de tout faire péter mais en même temps de respecter
nos singularités De montrer qu'il est possible de faire société en justement en
acceptant en regardant l'autre comme il est, ce que l'on a en commun c'est, je
sais pas si ça se définit vraiment, ça s'appelle de l'amour je crois De l'amour,
de la tendresse, du respect, on est chacune très différente et dans nos corps,
dans nos manières d'arpenter le monde, dans la manière d'écrire, mais il y a un
endroit où on se retrouve Et je pense que ça dit quelque chose de très fort,
qu'il y a plus de choses qui nous rassemblent que de choses qui nous séparent Et
à nous quatre, on dit ça aussi, parce qu'on a des termes de voix qui n'ont rien
à voir et pourtant on arrive à faire matière commune mais pour ça il suffit de
le vouloir aussi Vous parliez de la délicatesse du jeu de piano Nina Simone,
vous vous avez réussi à trouver dans la guitare un nouvel écran, ça fait
longtemps que vous jouez Parce que j'avais l'impression que les dernières fois
que vous êtes venue dans cette émission, la guitare n'était pas dans vos bras
Non, elle n'était pas dans mes bras et ça fait déjà quelques années que j'ai
cette envie là Mais comme souvent les personnes assignées au féminin, on ne
s'autorise pas, on ne se croit pas autorisé, on doute Et pendant longtemps j'ai
douté et puis est venu le moment où justement grâce à une de mes sœurs qui
s'appelle Janice in the Noise On a discuté très très longtemps au téléphone et
j'ai raccroché et j'ai dit je vais m'acheter une guitare et c'est vraiment parti
sur un coup de tête Et donc j'ai passé la journée dans un magasin et les mecs
m'ont pris pour une folle Mais j'avais cette intuition très puissante que
c'était le moment où il fallait que j'approche cet instrument Et c'est vrai que
le moment du confinement a été un moment très à la fois chaotique et violent
mais aussi propice à soulever le tapis et déterrer tout ce qu'il y avait en
dessous On a fait le ménage Et j'ai la chance d'avoir un partenaire de travail
qui est assez fabuleux Le flutiste J'ai dit voilà il y a des sujets dont il va
falloir que tu t'empares pour ne pas qu'ils prennent toute la place et pour ne
pas qu'ils te mangent complètement Ça va te détruire, il faut que tu t'en
occupes Et la guitare a été pour moi un espace de vibration très puissant Ça
fait quelques années maintenant j'en joue, ça fait trois ans donc je suis
totalement autodidacte comme pour le reste Je ne suis pas guitarriste
professionnelle mais j'adore ça Ça m'a ouvert des chemins de composition assez
extraordinaires mais aussi d'interprétation Parce que je ne chante pas pareil
quand je l'ai sur moi Parce que la manière dont elle est accordée va tout d'un
coup ouvrir des couleurs que je ne savais même pas que j'avais en moi En fait
c'est fabuleux, j'adore la voix, j'adore la flûte traversière et j'adore la
guitare Ça y est maintenant on est trois Bon ben, trois feuilles Se sont
envolées la lune Les a emporté une affine Se sont envolées, se sont envolées
Trois filles dans le vent N'en font qu'à leur guise Portent en elles le
printemps Comme une fuite de vie Elles se tournent lentement Une étoile passe en
filant Elles ont choisi cet instant Pour prendre leur élan La lune les a appelé
une affine Se sont envolées la lune Les a emporté une affine Se sont envolées,
se sont envolées Le vent vient frapper la porte Et chasse toutes les failles
mortes Et les bonjons viennent tutoyer La lune rousse Le chant de ceux qui font
le vœu De voir en cet œil mineux Tous les espoirs, les nouveaux rêves Des jeunes
pourçants La lune les a appelé une affine Se sont envolées la lune Les a emporté
une affine Se sont envolées, se sont envolées La lune les a appelé une affine La
lune les a appelé une affine La lune les a appelé une affine Elles ont été mon
cœur, ma boussole, mon centre, mon équilibre, mon endroit de vérité. Quand j'ai
des moments de doute, je sais que je peux leur parler et qu'elles me savent,
elles me connaissent. Elles vous savent. J'aime bien. C'est assez rare de
mémoire. La mienne est assez courte. Je ne vous ai pas souvent entendu chanter
en français en fait. Ça fait quoi comme effet? Je suis sûre qu'on rentre dans
l'intime. Alors, j'ai déjà chanté en français souvent beaucoup mais souvent pas
des chansons que moi j'avais écrites. Aussi parce que je... Parce que souvent on
me pose la question mais pourquoi? Bon déjà j'ai envie de dire pourquoi pas. Je
chante dans la langue que je veux. C'est que chanter en français des chansons
que moi j'avais écrites, en fait les sujets que je voulais aborder, je les avais
pas encore suffisamment digérés. J'avais pas encore assez de distance pour
pouvoir les partager avec les gens sans que ce soit douloureux, sans que ce soit
difficile. Et il s'agit pour moi dans mon geste artistique d'être dans la
lumière, dans la douceur, dans le... Voilà et que chacun a chacune une
temporalité différente. Et j'essaye de respecter mon animal totem qui est la
tortue. J'essaye de respecter son énergie, son rythme, sa petite voix. Et je
savais que le moment où je serais prête, je le saurais. Et là est arrivé le
moment. Le moment où... Voilà, décider que ces blessures, j'ai pas envie
qu'elles continuent de me définir. Donc c'est un travail qui est long et qui est
encore en cours mais qui me permet de dire, ben voilà ça y est, ce sont des
cicatrices. Donc maintenant je peux parler de, notamment de, voilà de cette
terre rouge qui m'a vu naître, qui me manque mais qui continue à vibrer en moi.
Parler de mes sœurs, c'était assez naturel que ça se fasse aussi en français.
Quand on s'est posé sur une table avec Didry pour commencer le disque, c'était
d'abord les idées. C'était d'abord les textes. Qu'est-ce que j'ai envie de
raconter et il fallait répondre à la question, qu'est-ce qui me constitue. Et
dans tout ça, il y avait, voilà, comment j'ai réussi à trouver, grâce à la
musique, grâce aux rencontres, des ressources pour me guérir. Et parmi ces
ressources, il y a évidemment la musique, il y a l'amour, le partage mais
l'amitié et la sororité tout particulièrement. Je lisais une interview que vous
avez donnée à Jazz Magazine récemment et c'est vrai que sur cet album, il y a
certaines douleurs qui sont exprimées. Il y a souvent le mot larmes qui est
utilisé dans vos chansons. J'ai vu le mot inceste aussi. Vous n'en aviez jamais
parlé. Non, je n'en avais jamais parlé, en tout cas pas de manière frontale mais
parce que rien que dire le mot inceste m'était… Insupportable. Je ne sais même
pas si c'est insupportable mais je ne pouvais pas le dire sans que ça provoque
une douleur. C'est comme si j'y retournais instantanément. Et j'ai fait la
rencontre artistique avec une autrice qui s'appelle Axel Jeanne-J.K. qui a fait
un podcast qui s'appelle La fille sur le canapé et dont le sujet c'était les
violences sexuelles faites sur les enfants dans la communauté afro. Et elle m'a
demandé de faire la musique de ce podcast en ne sachant pas que j'étais
concernée par le sujet. Je ne lui ai pas dit et j'ai dit oui pour faire la
musique. Une fois que j'ai fait la musique, j'ai écouté le podcast. J'ai
tellement pleuré et je l'ai appelé pour la remercier parce que sans le savoir,
elle m'a permis d'arriver au bout d'un chemin. C'est-à-dire, vas-y, il faut que
tu t'en occupes et il faut que tu réussisses à ce que ça ne te définisse plus.
Il faut que tu acceptes que tu n'es pas le sujet de cet incest si tu en es
l'objet et que tu n'es pas responsable de ça. Et qu'en fait, tout le problème de
cette violence que l'on subit régulièrement, en plus de la violence qui est
subie, c'est le silence qui entoure ça. Et qu'on nous dit oui, mais maintenant
la parole se libère, mais il ne s'agit pas de la libération de la parole parce
qu'elle n'est pas enfermée. On n'est pas dans un pays où la parole est enfermée,
elle n'est juste pas entendue. Et il y a elle, mais il y a aussi une activiste
qui s'appelle Sophia 7, qui elle a lancé le hashtag MeTooIncest. Et en fait, ces
deux femmes-là, elles ont ouvert en moi un truc et là, je me suis sentie capable
d'eux. Mais je pense qu'il fallait tout ce chemin de vie pour pouvoir en parler,
pour pouvoir le partager. Ensuite, j'ai fait une vidéo que j'ai postée, mais
j'ai bien pris le temps de la poster entre le moment où je l'ai faite et le
moment où je l'ai postée. Et c'est passé deux ans parce que je savais qu'il
fallait que j'attende le bon moment pour être prête à recevoir ensuite la
réaction des gens qui allaient m'écouter et me lire. Parce que ce n'est pas rien
la parole. Et c'est aussi pour ça que, pour en revenir à ce que vous disiez de
parler en français, aussi parce qu'il fallait que je redessine le chemin de mon
histoire avec le français, que chanter en français, c'était directement lié à
mon incesteur. Et donc, je ne pouvais pas et je ne voulais pas lui faire ce
cadeau-là. Et on est sur un chemin, on est tout seul, il faut se protéger tout
seul, il faut se construire tout seul. C'était non quoi. Mais encore une fois,
j'ai cette grande chance d'être entourée, d'avoir beaucoup d'amour, d'amitié, de
respect et d'avoir rencontré la musique qui me fait un bien mais incroyable. Et
je le dis souvent, mais plus je chante, plus j'aime chanter et je trouve ça
complètement fou-dingue. Et que, entre le premier disque et maintenant, ça n'a
rien à voir, mais c'est normal. C'est le quatrième album. Voilà, puisque la
personne que je suis continue d'évoluer. Et même dans notre relation avec G.
Drue, la manière d'écrire, elle a changé. Même si, voilà, il y a des choses
qu'on peut reconnaître, puisqu'on ne va pas changer notre nature profonde.
Cependant, les chemins qu'on va arpenter, les objectifs qu'on va se donner vont
être complètement différents à chaque fois. Donc c'est merveilleux. On est là
quoi. Merci Sandra. Écoutez, de l'amour, vous allez en partager avec vos
musiciens. Je donne quand même quelques dates parce que vous êtes en tournée.
Alors vous qui vous soignez avec la musique, vous allez être guéries. Le jazz ou
les paumiers écoutants, ce sera le 18 mai, le 15 juin, ce sera à Paris. Au café
de la danse, d'autres dates. Il n'y a qu'à aller sur votre site, Sandra Nkake.
Je vais proposer à Alice Blanchard, à Jérôme Peres, à G. Drue et à vous Sandra,
de vous remettre en place dans la session live sur le Grand Plateau, dans le
Grand Studio, pour le titre My Heart. Un mot sur My Heart, même deux voire
quatre. Deux voire quatre, il raconte l'histoire d'une femme qui est quasiment
laissée pour morte sous les coups de son conjoint. Voilà des scènes que moi j'ai
vues et qui arrivent tous les jours, qu'on entend tous les jours. Et j'avais
envie de lui redonner la parole et de dire que même à terre, elle va se relever.
Même si on nous empêche d'ouvrir notre gueule, on va continuer à l'ouvrir, on va
continuer à résister, on va continuer à militer, on va continuer à être en
colère et à transformer cette colère en énergie fédératrice. Et même si l'autre
ne veut pas, en fait on est là. Je suis un étranger de votre hétérisme, je suis
plus fort. Dread and malice ne me rend pas mal. Je suis un peu chelou, je me
garde. Même si des centaines de soldats marchent pour me tuer. Je suis un peu
chelou, je me garde. Même si des centaines de soldats marchent pour me tuer. Il
n'y a pas de temps pour moi de marcher. Je suis libre et indépendant. Je dois
dire la vérité et me détruire. C'est la silence, c'est la violence. Et mon cœur
est en colère. Je suis un étranger de votre hétérisme. Je suis un peu chelou, je
me garde. Même si des centaines de soldats marchent pour me tuer. Je suis un peu
chelou, je me garde. Même si des centaines de soldats marchent pour me tuer. Je
suis un peu chelou, je me garde. Même si des centaines de soldats marchent pour
me tuer. Je suis un étranger de votre hétérisme. Je suis un peu chelou, je me
garde. Même si des centaines de soldats marchent pour me tuer. Je suis un
étranger de votre hétérisme. Je suis un peu chelou, je me garde. Même si des
centaines de soldats marchent pour moi tuer. Je suis un étranger de votre
hétérisme. Je suis un étranger de votre hétérisme. Je suis un étranger de votre
hétérisme. Je suis un Non-back wanna, determinant. Rage, won't bring me down
Rage, won't be my game Rage, can you hear it? Rage, won't be my game Oh so loud,
I'm in my chest, you can't even come and get me Oh so bad, running through my
veins, you can't even come and get me to you Can't get out of my knees and stand
on my line Rage, won't be my game Rage, won't be my game Rage, won't be my game
No need, no need, no need I don't want to rest, I don't want to rest Rage, won't
be my game Rage, won't be my game Rage, won't be my game I don't want to rest No
need, no need, no need I don't want to rest, I don't want to rest I don't want
to rest No need, no need, no need I don't want to rest No need, no need, no need
I don't want to rest, I don't want to rest No need, no need, no need I don't
want to rest I don't want to rest I don't want to rest I don't want to stop No
need, no need, no need I don't want to restamouré la Bugün la sonoritéоссATION
LIFE DE MERE MUSIC DE MON脉 Laurenz Aloua Musique du monde sur RFI La sonora
Mazuraine avec Nicolas Eccarn qui était à la basse, Juan David Lacorazin à la
guitare, Luis Lizaralde à la batterie, Yvonne Medellin à l'accordéon et au
chant, Giovanna Mogollon qui était au percussion et aussi au chant, Miguel Ángel
Rodriguez Rebollejo au percussion, et Diana San Miguel au percussion également
au chant. J'espère que j'ai pas trop écorché les noms. Ça va Giovanna? Oui c'est
très bien. À peu près. 5 sur 10. Alors on a Giovanna qui parle français, qui va
nous aider pour faire l'interview d'Yvonne et puis de son interview à elle aussi
bien sûr. Donc vous êtes originaire tous de Bogotá. De Bogotá. Alors Bogotá il y
a une année colombienne en France il y a quelques années, donc ce qui nous a
permis de découvrir qu'il y avait des groupes absolument fabuleux entre Bogotá
et Medellin et vous en faites partie. Alors vous sortez cet album qui s'appelle
Bailando con extraños. Bailando con extraños. Qui veut dire? En dansant? Dansant
des étrangers. Danser avec les étrangers. Bon Yvonne vous venez d'où? Je sens
que vous avez à peu près les trois grandes civilisations dans votre ADN. Je vous
sens amérindiens, je vous sens espagnols, je vous sens africains, je vous sens
de tout. Oui, Bogotá et la Colombie c'est un pays très divers et nous avons un
héritage aussi des indigènes colombiens, les africains et les espagnols pour
donner la langue que nous parlons. Tout fait la mélange de notre musique et les
différentes régions de notre pays. Quand est-ce que le groupe a démarré, quand
est-ce que la Sonora Mazuren est devenue la Sonora Mazuren? C'est vous Yvonne
qui avez créé ce groupe? Iván et Nicolas oui. Et Nicolas pardon. Oui. Et quelle
était l'idée au départ? Ça fait combien de temps que ça existe et qu'est-ce que
vous vouliez mettre dans cette aventure? Oui, la Sonora Mazuren est née en 2015
avec l'idée d'Yvonne et Nicolas pour faire de l'investigation de la musique
traditionnelle, spécialement de la côté caribienne en Colombie, mais aussi
d'autres cumbias que c'est fait en Perro et en Ecuador. La Sonora Mazuren est
très importante aussi la cumbia là parce qu'ils ont un style un peu plus
psychodélique. La Sonora fait la mélange sur la cumbia traditionnelle de
Colombie mais aussi sur d'autres cumbias comme la Peruvienne et la Equatorienne.
Et alors quand vous avez commencé à organiser des fêtes clandestines, comment ça
se passait à Bogotá? Oui, la fête à Bogotá c'est très intense et très bizarre.
Mais c'est la mélange aussi d'autres musiques comme les Pong, les Rock, les
Fong, les Afro-Bide. C'est un grand mélange et c'est très énergique. C'est un
cité avec beaucoup de musique et de culture. Bogotá se trouve à 2640 mètres
d'altitude. Est-ce que c'est pas un peu excitant aussi? Oui, oui, oui.
Absolument. Pour la jeune qui, je ne sais pas, habite sur la mer, c'est très
difficile de faire de la respiration. Bon, je vous propose d'écouter un extrait
de cet album, Bailando con Esteragnoz, donc dansé avec les étrangers, qui était
produit par le type de Meridian Brothers. Emily Salvarez, oui. Oui, oui, oui.
Pour nous, on jouait hier pour la première fois ici au Festival d'En-Lieu-Bleu.
Et pour Meridian Brothers, était la première console aussi en France. C'est très
important pour nous de suivre le chemin des… Les Meridian Brothers. C'est un
chemin assez rigolo aussi, un autre genre. Je vous propose d'écouter le titre.
Alors, je ne sais pas le prononcer, c'est les tasé-pignées. Tasé-pignées. Et
qu'est-ce que ça veut dire? C'est un concept africain. Oui, c'est un concept
africain. La vie, c'est un cycle. Et nous revenons tous le temps sur le cycle,
mais avec plusieurs connaissances. Avec plus de connaissances, à chaque fois
qu'on part. Oui, plus de connaissances. Oui, c'est l'altitude, vraiment. Oui, la
montagne, c'est très fou. Oui, c'est l'altitude, vraiment. Oui, c'est
l'altitude, vraiment. Oui, c'est l'altitude, vraiment. Oui, c'est l'altitude,
vraiment. Oui, c'est l'altitude, vraiment. Vous vous entendez, vous êtes à
l'antenne. Ils sont dissipés, l'insomnera mazuré. Mais c'est comme ça, c'est la
Colombie, ça. La France et la Colombie ont beaucoup échangé, et puis il y a
aussi des artistes qui viennent de ce pays qu'on apprécie énormément, notamment
Fernando Botero, le sculpteur. Il y a eu une expo géniale, c'était en 1993, il y
a 30 ans. Les sculptures de Botero étaient sur les Champs-Elysées à Paris. Ok.
Et puis, il y a aussi des sculptures de solitude de Gabriel Macias Marquez, à
prix Nobel de littérature en 82. Et alors lui, son style d'écriture, on le
ramenait au type d'accordéon dont joue Ivan, c'est le Valle Nato. Ce que ramène
aussi Ivan dans ce groupe, avec son accordéon qu'on appelle l'accordéon Valle
Nato. Est-ce qu'il peut nous raconter l'histoire du Valle Nato et de l'histoire
de l'accordéon et le Valle Nato? En réalité, l'accordéon est un instrument. Son
nasciment est en Chine, mais c'est très populaire en Chine. En Asie, absolument.
Oui, mais c'est très populaire en Amérique latine et aussi en Colombie. Et en
Europe. Et donc, la légende, l'accordéon est arrivé... Il raconte la légende que
l'accordéon a flotté sur la mer parce qu'il est arrivé par bateau, il s'est
installé dans la plage et quelques personnes les retrouvent et commencent à
jouer. Mais ils jouent les sons traditionnels de Colombie, non? Les cumbias,
c'est le plus vieux rythme très traditionnel de la Colombie, né dans la côte
caribienne. Il y a deux différents types de musique pour faire dans l'accordéon.
L'un est le Valle Nato, l'autre manière de faire la musique dans l'accordéon
c'est la cumbia. C'est un peu plus pour les jeunes, les familles, pour les gens
de la petit village. Ce sont des gens agricoles, on raconte le quotidien d'un
petit pays. Oui, il y a des choses très quotidiennes de la vie. La sonora
massouraine, on dit massouraine? Massouraine. Massouraine, c'est un quartier de
Bogota? Oui, c'est un quartier nord de Bogota. Joli, pas joli? C'est normal oui.
Pas terrible? Non non, c'est joli. Non, parce que par rapport à Candelaria, qui
est le truc, ça c'est pour les touristes. Ah oui, la Candelaria c'est très joli
oui. On ne peut pas dire que vous soyez un groupe de musique traditionnelle,
même si vous utilisez beaucoup d'éléments des musiques traditionnelles de
Colombie, dont la cumbia, qui arrive du 19e siècle, c'est pas très très loin non
plus. Malgré tout, vous n'êtes pas des puristes, l'authenticité c'est pas ça le
propos. Vous êtes en fait comme tous les artistes, vous voulez proposer un
nouveau son. Un nouveau spirit. Est-ce qu'on peut parler vraiment de nouveaux
sons à Bogota? Est-ce qu'il y a une concurrence avec Médéhyne? L'autre ville
connue de Colombie. Est-ce qu'il y a le son de Médéhyne et le son de Bogota?
Oui, à Bogota nous trouvons beaucoup de différentes traditions de la côté
atlantique, de la côté pacifique, du Amazonas et bien. Alors dans l'année 2000,
c'est comme une explosion de la musique traditionnelle de la côté atlantique et
pacifique. Mais Bogota aussi est une ville très rock. Très rock. Bonky. Très
punk. Hip-hop. Il y a de tout. Afrobeat. Et la musique que vous faites, sur cet
album c'est très instrumental. Il y a quelques titres chantés mais pas beaucoup.
Quand vous chantez, quand il y a des paroles, qu'est-ce que ça raconte en gros?
Il y a un morceau, c'est la parque, c'est la prochaine. La parque c'est pour la
mort. C'est pour la mort. C'est pour leur rendre hommage ou c'est pour les
fêter, c'est pour dire du mal? C'est un peu de célébration pour la mort. En
Mexique par exemple, la mort c'est très important parce que c'est la vie aussi.
La célébration de la vie n'existe pas sans la célébration de la mort. Et c'est
sur ces notes d'une grande beauté, sur notre propre finitude que nous allons
terminer cette émission. Merci beaucoup la Sonora Amazonienne. Merci à Jurbana
Ivan et à tous les musiciens. Dites le nom de tous les musiciens. Un salut à
toute l'audience de RFI et je vais répéter le line-up de la bande. Nicolas
Eckhart dans le bas. Nicolas Eckhart. Juan David La Coraza dans la guitare. Luis
Lizarralde dans la batterie. Miguel Ángel Rodríguez dans la percussion. Giovanna
Mogollón et Diana San Miguel dans les voix et la percussion. Et Ivan Medellín
dans l'accordion et les clés. C'est tout! Merci. Merci à Jurbana Ivan et à tous
les musiciens. Nicolas Eckhart dans le bas. Nicolas Eckhart. Juan David La
Coraza dans la guitare. Nicolas Eckhart. Juan David La Coraza dans la guitare.
Nicolas Eckhart. Juan David La Coraza dans la guitare. Nicolas Eckhart. Juan
David La Coraza. La parga, pilando sus manos el destino La parga, la parga,
levandome siempre en su camino En el filo de sus labios Oscuro mismo,
alucinación País y esto fuera eterno, nadie vivirá Después de este baile, nadie
vivirá La parga, la parga, pilando sus manos el destino La parga, la parga,
levandome siempre en su camino La parga, la parga, levandome siempre en su
camino País y esto fuera eterno, nadie vivirá Después de este baile, nadie
vivirá Muchas gracias Amaret WERVANA