TRANSCRIPTION: ABC-RN_AUDIO/2023-03-24-13H00M.MP3
--------------------------------------------------------------------------------
 That's the music show after the news. I'll surely be fine. Music ABC News with
Tom Melville. A member of the referendum working group on an Indigenous voice to
parliament proposed final wording of the referendum question was the result of
months of informed discussion and debates. Australians will vote this year on
whether a voice, which would act as an independent advisory body for First
Nations people, should be enshrined in the Constitution. However, there are
concerns the proposed inclusion of the words executive government will result in
legal challenges should the government not consult the voice on a decision. The
chair of advocacy group Uphold and Recognise, Sean Gordon, says the working
group came to the right decision. We had the discussions, we understood each of
the concerns that were raised from a legal perspective, but also from a
practical perspective on our communities. And we formed the position that it was
absolutely vital that executive government remain in the amendment. Two Brisbane
women have briefly appeared in court charged over their alleged involvement in
an international cybercrime outfit. Peter Quattrocelli has more. The two women,
aged 27 and 34, have both been granted bail on strict conditions after appearing
in the Brisbane Magistrates Court this morning. They were arrested and charged
along with a Melbourne man and an Adelaide man yesterday. Police allege the
group is responsible for 15 significant cybercrime incidents between 2020 and
2023. It's also alleged they set up 80 bank accounts with stolen identities to
transfer nearly $2 million to criminal associates in South Africa. Victims
losses ranged from $2,500 to almost $500,000. New South Wales Premier Dominic
Perete has plugged his track record of 12 years in government on the final day
of the state election campaign. Mr Perete started the day at a pre-polling booth
in the Sydney suburb of Chatswood in the seat of Willoughby, previously held by
popular former Premier Gladys Berejiklian before campaigning in the neighbouring
seat of North Shore. Both seats are held by the Liberals, but they're under
serious threat from independent candidates. Mr Perete says tomorrow's election
is a clear choice for voters. It's not about the Liberal Party, it's not about
the National Party, it's about the great people of New South Wales. I say to the
people of New South Wales, it is only the Liberals and National with that long
term economic plan to take our state forward. I say to the people of New South
Wales today, back our plan, protect you, protect your family. Heading overseas
where a former cyber security adviser to US President Barack Obama says Western
governments have a right to raise concerns over the popular video sharing app
TikTok. US CEO Xiu Xichu has been questioned by a US congressional committee as
pressure mounts for an outright ban of the app. The company has been dogged by
claims that its Chinese ownership means user data could end up in the hands of
the Chinese government. Michael Daniel is now head of the Cyber Threat Alliance.
He says governments, including Australia's, should review the risks, but isn't
calling for an outright ban. I only think that any government needs to be making
a risk benefit trade off are the benefits of, for example, having TikTok or any
other social media platform. Is the risk of that worth the benefits that they
bring? King Charles will postpone his first official state visit to France due
to widespread anti-government protests. Harry Sekulich has the details. Hours
after France's interior minister fronted national television to say his country
was ready to welcome King Charles and what he described as excellent conditions,
the monarch has pulled out of the trip. There were concerns about the violent
protests erupting across France with people turning out in droves to rally
against the government's reforms to the pension system, which will lift the age
of retirement from 62 to 64. King Charles was meant to visit the south-western
city of Bordeaux, but the town hall has been set on fire. It's not yet clear
who's responsible. Israel's Prime Minister Benjamin Netanyahu has arrived in
London to fierce protests. Mr Netanyahu shook hands with his British counterpart
Rishi Sunak outside No 10 Downing Street as a wall of demonstrators jeered and
shouted nearby. Jewish and Israeli organisers rallied under the banner Defend
Against Democracy to voice opposition to Netanyahu's judicial reforms, which
would increase the PM's powers. And in the NRL, the Broncos have claimed
bragging rights in the Battle of Brisbane. Downing Newside the Dolphins 18
points to 12. The Broncos led 8-0 early in the second half before two quick
tries saw the Dolphins leap into a 10-8 lead. The Broncos went back ahead
through Kurt Gapwell before Ketoni Staggs scored a length of the field try to
seal the six-point victory. Earlier, the Storm ended a two-game winless streak
with a 24 points to 12 victory over West's Tigers in Melbourne. You're up to
date with the latest ABC News. For more news at any time, you can download the
ABC Listen app. Hi, I'm Robbie Buck and this is the music show. Robbie, the buck
stops here, ladies and gentlemen. That's music show regular Joseph Towardris and
his ood later on today's show. We're also going to talk to Niamh Regan. She's an
Irish singer-songwriter on her debut Australian tour. First, though, it's the
Australian Chamber Orchestra with their world premiere recording of a new work
by Thomas Adders. This is Shanty Over the Sea. Yeah. Yeah. Yeah. Yeah. Yeah. A
little bit of Shanty Over the Sea from the Australian Chamber Orchestra
performing that work by Thomas Adders. And we're staying with the ACO now for
our first guest and that is Joseph Towardris. He's been our guest on the music
show so often he probably needs no introduction. But just in case, Joe is a
virtuoso of the ood in the Egyptian tradition and he's an expansive
collaborator, including a long relationship with the Australian Chamber
Orchestra. He's in the midst of touring a version of Vivaldi's Four Seasons with
them right now. Joe and I will chat about some of those collaborations in a
moment, but we've got him in front of a mic in our studio. So let's hear Joseph
Towardris and his ood. This is Constellation. Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. Yeah. The sound of the ood being played beautifully live for
you in the music show studio. And if it's an ood being played beautifully, it's
perhaps no surprise that the performer is Joseph Towardris. Hello. Hey, Robbie.
Thanks for having me, mate. It's good to see you. Yeah. Welcome back to the
music show. It's lucky you've got a key. That's it. I do. I've got one of those
passes. Now that is a piece called Constellation that turned up on a live
recording at the Opera House recorded back in 2019. And of course little did you
know when you were playing that at the Opera House back then that the whole
world was going to change. And your world was going to be thrown into disarray.
Tell me about it, man. Yeah. But yeah, what a time that we've lived through. The
last time you were on the show, Andrew Ford was speaking with you. You were
stuck in London, couldn't get back to Australia to perform with the ACO. The
good news is that you are back in Australia. Before we get to that though, let's
talk about what you've been doing overseas. Because you've been performing with
some pretty interesting ensembles recently. The Britain Sinfonia. Yep. What was
that like? Oh, that was amazing. I mean they're a great orchestra and a really
important ensemble on the classical music scene in Great Britain actually. And
so it's really nice to be invited there and to actually be commissioned to write
a new piece and premiere it with them in London and then later in Essex. And so
what did you write for them? It was a piece called The Three Stages of
Hindsight. So it's in three movements, a through composed piece, about 22
minutes. An oud concerto I'd say. Because it did have cadenzas and moments for
solo oud playing and a real call and response between the orchestra and the oud.
But the three movements were retrospect, regret and reality. And it was my take
on hindsight. I mean I invented, I don't know if there's three stages or four or
five, but according to me there's three. And so that's what I came up with. And
funnily enough the movements can be played not in order, which is good. So you
can choose depending on what day you want. Do you want regret first? But you
need to have the retrospect I think before you can regret anything. So it's
modular. Yeah, so it's a modular. So it's ergonomically designed for your ears,
this piece. So yeah, that was quite interesting. And also the finish of it as
well, you can choose to have it not resolve. Finish is on a B flat or if you
want it to resolve to an A that's up to you. Now the other performance that we
were interested in recently is you opened the Economic and Political World Forum
at Davos. Now this intrigued me. This is I guess where the world's leaders and
billionaires get together to work out what the world's going to look like from
their perspective. Yeah, total opposite to any of the musicians on stage
actually. Well I bet, yeah. What was the experience like? It's an interesting
experience. I mean it was a big, it was like a convention centre really. But it
was really nice that they decided to have a bit of music and culture to colour
something which is otherwise kind of avoided at these places. But it was a
really nice experience. We had Idris Elba, he gave us two standing ovations. I
wasn't sure if he thought it was the end of the show or decided just to give us
two standing ovations. But he loved the music, him and his wife Sabrina. They
were there in the front row and you know there was you know Raleigh Fleming also
in the front row. And you know it was interesting. It was an interesting vibe. I
mean not my most favourite of venues but it was a good experience. And it was
interesting to open the World Economic Forum, the two Tewardros brothers back by
an orchestra. So that was a bit of fun. But at the end of the day I'm really
looking forward to playing here and you know. Well before we get to those
performances with the ACO and Vivaldi and the Four Seasons, the other project
that you've been involved in is a really interesting one. And that's a
collaboration with William Barton amongst other musicians. Let's have a little
listen to the title track. This is History Has a Heartbeat. Yeah. Joseph
Tewardros is with me in the studio today and a little from a recent project of
his, History Has a Heartbeat, a collaboration with William Barton on the
Yadarki. Now you've known William for a long time. Yeah. William I guess we rose
up in the ranks of the classical music world kind of together. We have similar
stories. We're really affected by our ancestry and we carry a lot of that
lineage actually in the instruments we play. There's a strong tradition. But
also me and my brother James actually going to Redfern Public School. You know
we had Indigenous kids and we had multicultural kids and it was this kind of
melting pot at school. And so we had a lot of Yadarki and Didgeridoo players
come and elders come and you know do ceremonies. And so that was a soundtrack
that kind of I grew up with as well. And so and a lot of people might or may not
know I was at the Redfern's Beach with you know Paul Keating. You know that was
a school excursion. So we had all those things that and we cooked kangaroo on
the oval. You know this is all things that we went through. And they're both
ancient instruments. Two ancient instruments you know me and William you know
doing our thing. And it's really nice. There is something to it. There is
something special when you get instruments of that age together. Yeah and how do
they meld together sound wise? Well there is an earthiness to both instruments I
think. William's an absolute master at what he does. He's brilliant and so he
adds his colourings to the stuff we bring. So it is a kind of collaborative idea
and we just work around that. He's also you know brought up in improvisation and
William is a big improviser. And you know he's been able to have his dig on all
sorts of places. And you know I share the same sort of idea is that you know
we've got to take the instrument where we can. But in a place with integrity I
think. And how do you place the colours of the sounds of the two instruments
together? I mean do you tune for each other? William can. And usually you'd have
to go around with him because you know the instrument is kind of tuned to
itself. So you have to kind of work with that. But sometimes he uses a little
trick of some toilet roll that he puts to extend the instrument. I know it's not
as romantic as people think. You probably go oh yeah he picks the gum from you
know a four thousand year old tree or something. But no it's just toilet roll.
Bit better. Yeah well you're back in the country and you are finally getting to
tour the program with the Australian Chamber Orchestra that had been cancelled
because of COVID. And that is the Vivaldi Four Seasons program. Give me a bit of
insight into how you prepare for such a well known piece of music. I mean it's
one of the best known pieces of music in the world. I've got to say I'm not just
saying this but Richard does a fine Four Seasons. He's amazing at it. And you
know I've had the privilege of being able to listen to him perform it you know
more than 30, 40 times probably. And I really enjoy his version of it. And for
me it's not about getting in the way. It's about colouring it. So I'm not
getting away in that he's genius playing you know. I really love what he's
doing. It's just about colouring some lines some unisons which make it a little
bit heavier or thicker. And of course you've got James on percussion the wreck
driving it as well which is not something that's common in the Four Seasons.
There's really no percussion. So you've got James's percussion driving it which
is already giving it a different soundscape. You've got Richard doing his thing.
And I just have to come in and colour that you know. And to appear to enrich
what we already have which is already something very strong. It's just to enrich
it a little bit or add a different flavour to it. But it's about staying out of
the way of something that's really quite amazing in itself. Alright well let's
have a little listen to you and the Australian Chamber Orchestra and Summer. The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra The Australian Chamber
Orchestra The Australian Chamber Orchestra The Australian Chamber Orchestra The
Australian Chamber Orchestra The Australian Chamber Orchestra The Australian
Chamber Orchestra The Australian Chamber Orchestra