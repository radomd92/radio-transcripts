TRANSCRIPTION: ABC-RN_AUDIO/2023-04-06-16H00M.MP3
--------------------------------------------------------------------------------
 There was a moment of tear, terror. I was drinking to pass out. Come follow
Earshot and lose yourself completely. Saturdays at 2 on ABC RN and anytime on
the ABC Listen app. Well, it's not a normal cafe. We specialise in the milk of
the platypus. Aunty Donna have millions of views. Now the superstars of socials
have an ABC TV show and a cafe. Here's your sandwich, you dingus. And more
celebrity cameos than latte flavours. Doesn't that sound silly? Pretty silly to
me, right? Aunty Donna's Coffee Cafe starts Wednesday, April 12 on ABC TV and
streaming on ABC Eyeview. ABC News with Chelsea Hetherington. The federal
government's special envoy for reconciliation, Pat Dodson, has accused the
Liberal Party of treating Indigenous Australians as rag dolls in the
reconciliation process. Former Indigenous Affairs Minister Ken Wyatt has handed
in his party membership after the Liberals announced they would oppose a
constitutionally enshrined voice to parliament. Senator Dodson says Mr Wyatt
would not have made the decision lightly. Currently the coalition parties seem
to be bedded in the old assimilationist approach and the old colonial approach.
They know best what Aboriginal people can have. South Australia's Energy
Minister has defended the government's decision not to notify the public about
an explosion on a gas pipeline in the state's north. Oil and gas company Santos
has confirmed one of its pipelines at the Big Lake gas field, 700 kilometres
northeast of Adelaide, exploded on January 25. It says it reported the incident
to the state government immediately and the pipeline is operating again at
reduced pressure. Energy Minister Tom Koutsantounis says there was no threat to
life or property from the incident. This is no different to any other incident.
The difference here is the term explosion. So if there had been any loss to any
supply, if any households had been impacted, if there had been any injuries,
absolutely a press release would have been warranted. Hundreds of flood-affected
residents are still waiting to return home in the Northern Territory as the
government calls for support from public servants to help out at the Howard
Springs Evacuation Centre. Kyle Dowling reports. After being evacuated about a
month ago, more than 500 evacuees are still being housed at Howard Springs, with
almost 200 returning to Kalkorinji late last month. An NT government
spokesperson has confirmed the first group from Daguragu is expected to return
home after Easter, but there is still no set return date for residents from
Pigeon Hole. Earlier this week, an email sent to public servants asked for
urgent help to staff the Howard Springs facility to provide relief for people
who have worked huge hours as part of the emergency response. Two European
leaders visiting Beijing have pressed China to use its influence with Russia to
stop the fighting in Ukraine. France's President Emmanuel Macron has urged his
Chinese counterpart to make Russia's President Vladimir Putin see reason. The
head of the European Commission, Ursula von der Leyen, has encouraged China to
promote what she's called a just peace. China's President Xi Jinping says
Beijing is seeking a political solution to the war. On the Ukrainian crisis,
China favours peace through political negotiation. China is willing to join the
French side in calling on the international community to maintain restraint and
avoid taking actions that will aggravate the crisis. The foreign ministers of
Saudi Arabia and Iran have met for the first time in more than seven years
during talks held in Beijing. The BBC's Sameer Hashmi reports. Saudi Arabia
state media released footage of Foreign Minister Prince Faisal bin Farhan and
his Iranian counterpart, Hussain Amir Abdul Lahyan, shaking hands and smiling in
Beijing. Both countries announced in a joint statement that they have agreed to
resume flights, reopen embassies and facilitate visas for citizens. Saudi Arabia
cut ties with Iran in 2016 after its embassy in Tehran was stormed by a crowd
during a dispute between the two countries over Riyadh's execution of a
prominent Shia cleric. The head of London's police force says there could be
hundreds of officers removed from the service after an internal review is
conducted into wrongdoing. Last month, a major review branded the Met as
institutionally sexist, racist and homophobic. London's police commissioner Sir
Mark Rowley says the Met needs a shake-up. I think over the next two or three
years we're going to be removing hundreds of people from this organisation who
shouldn't be here. We need to work through the investigations, people would
expect fair process. We're bringing independent advisors in to make sure that
we're not just marking our own homework, we've got other people looking over our
shoulder and checking our judgements. In the AFL, Collingwood has lost its first
game of the season, going down to Brisbane by 33 points at the Gabba. ABC
Sports' Quentin Hull reports. The Lions trailed by 13 points at quarter time in
a high-paced affair, but Brisbane kicked 10 unanswered goals in the second and
third terms to take control of the contest. Charlie Cameron kicked six goals and
Cam Reiner kicked four, as Brisbane won 18 goals, 8-116, to Collingwood's 11-17,
83. The Lions are now two wins and two losses, while the Magpies are three and
one. Good Friday Footy is a sell-out at Docklands, as North Melbourne hosts
Carlton. Quentin Hull, ABC Sport, Brisbane. ABC News. RN. RN. On air, online, on
your digital TV and on the ABC Listen app. This is Duet on RN with Tamara Anna
Czyszlowska. Yes, it's time for another duet. And today I'm joined by a family
man who loves nothing better than spending time with his wife and two daughters.
And they're guinea pigs, I'm told. Their names are Zip and Zap. Um, can I
interrupt there? They're dead, I think. They're dead. They're definitely dead.
OK, I haven't even got to the end of the intro. Sorry. Sorry. Just wait a
second. Um, and the possibly alive or maybe dead Zip and Zap. He also enjoys the
free-range eggs laid by his many hens, brews his own beer and spends quite a lot
of time with a viola that I think is named Martha. I'd like to introduce to you
violist Christopher Moore. Hello. Sorry for interrupting. Is that really true?
It is. They're deceased. It is. We came back from holidays. And the girl who
was...we were paying to look after them, well, didn't do a very good job. But
that's kind of OK anyway, because they were sort of parasitic pets. You know,
they just ate all of our vegetables and stared at us and hoped that we didn't
look at them, let alone pick them up. You know, they were kind of useless pets.
You know? Oh, viola and Zip and Zap. I mean, I should feel sad. But RIP.
Christopher Moore, performing Mozart. T LO VERS熊 Christopher Moore there with
Richard Tognetti and the Australian Chamber Orchestra performing Mozart's
Sinfonia Concertante for violin, viola and orchestra in E flat. So the pairing
of the violin and viola, I mean that's a fairly common one but you do so many
collaborations and there's a track here from Concerto of the Greatest Sea by
Joseph Tuadros. So that was you Chris with the Tuadros brothers and Mac McMahon
I believe. Yes it was and certainly very much out of my comfort zone. But was it
the extemporization that took you out of the... Yes by extemporization I think
you mean, is that what you mean, improvisation? Yeah improvising. I've never
heard that word before. Yes so it was certainly not something I'm used to doing
but I think I've got a good enough ear to sort of pick these things up but
certainly just going off on your own playing, improvising a tune over the top is
more daunting than it would seem. I always have the impression with his music
that it stays within the tonal boundary and of course it has the traditional
influences. But there is something very modern about it. Oh there is, I mean
just hearing him improvise I'm absolutely blown away. So that's certainly very
inspirational. Sometimes there's that tyranny of freedom isn't there where you
have so many things that you could do. Yeah options, too many options. It's like
going to Bunnings when you're trying to put together an irrigation system for
your plantar boxes. Too many choices. Music of Joseph Twardros that's from the
album Concerto of the Greater Sea featuring my guest this afternoon Christopher
Moore the oldist. Well done Joe and James and Matt that sounded great. So Chris
it sounds like you spend a fair amount of time at Bunnings. I did actually go
there today and it was really overwhelming. It had nothing to do with running
repairs on Martha here? No, no, no I wouldn't do that. I mean I put the tail
piece on and the chin rest on but I don't do anything else. You're saying you
have finally got the chin rest that you really want. I'd lost it for a long time
but you know over the holidays you tend to clean up your house. Well I mean just
for the listeners it's the most magnificent viola I'm seeing before me and am I
right in thinking that it's particularly big? Oh it's a 17 and a half inch body
which is pretty big but it doesn't really feel that big to me. I mean I've had
it for so many years and actually the instrument that I was playing in the ACO
was the same size the Magini. The Magini? Yeah Gianpaolo Magini from 1610. That
would have had wood from the 16th century wouldn't it? Yeah incredibly.
Unbelievable. This instrument actually was the one that the ACO was comparing,
my instrument I should say, the Smith was the one that we were comparing all the
violas that we were trying all around the world and nothing really stood up to
the Smith until we found this one which had the same sort of rich fruity tone
you know. And is there a way to kind of summarise Smith? I mean such a great
influence, one I mean obviously our greatest ever instrument maker and so many
violists use them. You were telling me before that wasn't only Australian
violists that were interested in Smith but David Oistrakh. Yes he apparently he
was given one or borrowed one while his instrument was being repaired or
something like that I don't know exactly the story but he was certainly very
impressed at the time. We've got a track now of David Oistrakh he could be
playing do you think he's playing the Smith on this recording? I don't know. Let
me listen to it and I'll tell you whether he is or not. See if you can tell this
is David Oistrakh with London Symphony Orchestra, Max Brooks Scottish Fantasy.
This is the skirts. So so so so so so so so so so so so so David Oistrakh with
the London Symphony Orchestra, music of Max Brooks, the Scottish Fantasy, that
was the skirts. So now my guest this afternoon is Christopher Moore, violist and
Chris you've done a lot of premieres of Australian works haven't you? Yeah over
the years certainly a lot in ACO, a lot in the Melbourne Symphony. And it's
interesting that a lot of new works combine instruments in a particularly
interesting way I mean I suppose all composers are looking for another angle to
genres that I wouldn't say they're worn out but there's certainly a lot of piano
and viola sonatas and that sort of thing. So we've got a work now by Nigel Sabin
and it's one that you were saying that you perhaps would like to program in the
future it's his postcards from France and this is for viola and clarinet. Is
that a combo that you've tried? Well it's on the cards because I'm sort of
related to a very talented clarinist called David Griffiths as you'll probably
all know from Ensemble Liaison and other things. So we're always looking for
some excuse to do something. It's interesting because in lots of contexts the
viola and the clarinet can be interchangeable and play in the same register. So
how do you think, are they like shadows of one another? Well the viola is better
isn't it? I mean don't you think? Oh don't tell David I said that but no no. I
think that's obvious. I mean certainly the tone colour is quite similar so in a
way but of course there are sounds you can make on the viola that you can't make
on the clarinet and vice versa. So yeah they do work together very well in this
sort of context I think. So Patricia Pollet and Philippa Robinson performing
Postcards from France, the work by Nigel Sabin. I guess this afternoon
Christopher Moore. Now this next recording is one with you in it in your
capacity as Principal Violist of Melbourne Symphony Orchestra. It's a post
you've held for two or three years now isn't it Chris? Yeah sort of two and a
half something like that. And I think I read somewhere that you missed some
friends of yours, friends like Mahler and Strauss and Schoenberg. Exactly yeah
and many others and certainly the symphonic repertoire which the ACO does very
rarely. So it's always been in my blood all this the big work so it was like
coming back home really. We've got you here performing Ricard Strauss and this
is from an ABC Classics release. We have the four symphonic interludes from
Intermets so I think we're going to hear the first one it's called Travel Fever
and Waltz Scene. This is the Melbourne Symphony Orchestra under Sir Andrew
Davis. And it's a very good recording I must say. The Melbourne Symphony
Orchestra conducted by Sir Andrew Davis. That was the first interlude Travel
Fever and Waltz Scene from the four symphonic interludes from Intermetso Ricard
Strauss. So Chris what really drew you back to the MSO? Was it perhaps you know
you travelled so much with ACO? Well speaking of travel Fever yeah. Yeah
certainly it took a large toll on the family life really. It was very difficult
for my partner Jill to be doing everything while I'm away and even when I was
home it was quite difficult as well because I was working then. So I was so that
took a large toll so it just so happened that there was an audition coming up
and so I just went for it. I didn't tell anyone I was going for it and then I
got it and then I had to call Richard and say I've got a job in the Melbourne
Symphony. So yeah that wasn't a very nice time. But it's something we don't get
to talk about that much and that is that the travel factor and there's almost an
inherent restlessness in the nature of performing because a lot of you know even
orchestral jobs like ACO or being a soloist or being in a smaller ensemble it
just takes you to so many different places and often people feel that the travel
is a bit like a holiday and it really is. Well yeah I mean it's funny I just
actually recently went to Europe with ACO and certainly I hadn't done any travel
for quite a long time for almost two years and yeah it was certainly I
remembered what it was like and actually when you're doing it all the time you
kind of get used to it it's just part of the job and you don't worry about it
too much but just doing that out of the blue it was yeah quite a drag. It's very
hard to decompress don't you find between you know the exhilaration of the late
night performance and then getting up for the maybe 8am flight in the morning.
Yeah but then of course you have to get up for the bus to go to the airport etc
etc so it's quite it can be really quite taxing. This last tour wasn't so bad
but in the past they were always brutal schedules and with a you know like a
three week schedule with one day off in the middle which was luckily in
Amsterdam so you can you know enjoy the sights of Amsterdam usually but um yeah
so that was very interesting to see and to feel again but you know compared to
the Melbourne Symphony where you're working quite hard and in a different way
because you have to learn a new program every week essentially whereas the ACO
they would do the same program for two or three weeks so it sort of balances out
in a way. Are you ever tempted to just go it alone and just play solo? Oh well I
did think about it when I was sitting in the Brindabella National Park on
holidays I thought I might just stay there. Did you take your instrument with
you? No no I didn't. Chris I want to play the very first viola concerto and it's
by Telemann isn't it? Is it something that you became familiar with at an early
age? No because I was I came to the viola late I was a violinist not a I've
never heard of the Telemann viola concerto until I had to play it no I'm joking.
Was the first concerto you ever played a violin one? Yes yes I mean I was a
violinist for most of my professional career until the last 15 years now so
that's not most of my professional career but a fair whack of it. I read that
you said you liked how the viola made your head look smaller. Yeah because I do
have a big head don't you think? Well you can't tell on the radio. I think you
look very in proportion to me. Oh yeah that's because I've got a big tummy. Well
Chris we're going to hear you now performing Telemann and this is from I think
it was last year. Yes it was some last year at the at the Melbourne Recital
Centre with a beautiful hall with a beautiful orchestra the Melbourne Chamber
Orchestra directed by William Hennessey. This is concerto for viola strings and
continuo in G. Telemann's concerto for viola strings and continuo in G and that
was the first real viola concerto wasn't it Chris? The first one for solo viola
I think yeah. Chris I've heard you talk about how it's important to be versatile
as a violist is that something that you bring to bear with all the different
ensembles that you play with? Yeah I mean by virtue of the instrument being part
of the inner voice in chamber works or orchestral stuff you really do need to be
able to play with anyone and be quite flexible so. It's the beating heart isn't
it? Yeah I mean that's what really attracted me to the viola in the first place
was certainly the all the inner machinations of the music it's in there and all
the great composers actually played viola. So many of them. Like Bach and
Mozart. What about Dvorak? Dvorak he was a viola player which is my ghost well
I'd always question why the hell did he write such hard little inner voices if
he's going to play them but anyway he does and you know Brett Dean is a viola
player um who else Hindemith of course you know all these all these composers
that loved and played the viola. And I've always wondered maybe it's something
about that just where you're placed it placed in the ensemble as well just
purely orally you're right in the middle and you can it's as though you know the
eye of the storm perhaps. Yeah and this I love the way how you can influence
what's going on I mean you have to and if you if you don't you're just basically
a bum on a seat anyway. Christopher Moore is sitting with me at the piano this
afternoon and in just a few moments time we're actually going to be playing a
duet are you still up to that challenge Chris? I think so I got my viola out
yesterday and brushed it off a bit I can still see a bit of dust on it but yeah
we should be okay. You've brought a number of pieces with you. I think I'm
heading I'm thinking of something Russian. Yeah I like this one because it
certainly it reminds me of my childhood in a way because my mother used to play
all the swoon CDs the ABC classic swoon CDs and I first heard this with Yvonne
Kenny. Now don't tell me what it is for a moment before that we're going to hear
another landmark viola work this is the viola concerto of Cecil Forsyth the
third movement Allegro con fuoco. So so Lawrence Power the soloist there in
Cecil Forsyth's viola concerto in G minor that was the third movement. Lawrence
Power was with the BBC Scottish Symphony Orchestra conducted by Martin Brabbins
soloist and conductor both known to my guest on duet this afternoon Christopher
Moore. Now Chris we're coming to a very important part of the afternoon in fact
the last thing before I'm going to let you go I notice you've got my viola yeah
you've got your viola and you've got your iPad do we have to play anything like
after that I mean Lawrence god he's a great viola player I don't think I can
play after that all right I will yeah I've got I've got something on the iPad
for you okay which I use all the time so this is this is what you heard so tell
us now Yvonne Kenny you're in the car singing Rachmanoff's Vocalese and it's a
piece that can be played on any instrument and I guess the the the title
suggests that it's really to be sung you know and so to imitate the sound of the
voice so that's kind of what I will try to do can I borrow your iPad here here
do you want here I'll just pass it over okay do you want to turn the page just
tap it oh it's moved around that's fine okay hang on just tune oh gosh oh that's
right I forgot how to play for a minute this is duet on RN with Tamara Anna
Czyszlowska yeah Christopher Moore thanks for the duet my pleasure I think that
my tag now is probably going to be Christopher Moore than just a bum on a seat
definitely ever wondered what it's like to drink with Putin I remember him
running around with a vodka shot glass and he never took