TRANSCRIPTION: ABC-RN_AUDIO/2023-04-15-10H00M.MP3
--------------------------------------------------------------------------------
 head back to Yoriyoda country. That's all coming up on away, but right now it's
news time. Hello, this is ABC News with David Rowlands. The Japanese Prime
Minister has been evacuated after an explosion was heard before he was due to
make a speech at a fishing port. North Asia correspondent James Oton reports. A
bang was heard at the Saikasaki fishing port in western Japan where Prime
Minister Fumio Kishida was due to campaign for local by-elections. Footage shows
security detail pinning down a man as bystanders run away from the chaos. Local
media is reporting one man was arrested. White smoke was seen after the
explosion. The Prime Minister was evacuated and is unharmed. There are no
reports of injuries. James Oton, ABC News, Tokyo. Staff of the Indigenous
Affairs Minister Linda Burney tried to save a woman who was fatally stabbed
outside a hotel in the centre of Darwin overnight. Ben Gabbana has the story.
It's understood the woman had been attacked by her partner in a park. She fled
to the Hilton where hotel staff and some of Linda Burney's team, who were
staying at the hotel, tried to help her while the minister comforted the woman's
family. Paramedics took her to hospital but she died a short time later. Ms
Burney says her heartfelt condolences go out to the woman's family and her loved
ones. A 56-year-old man has been arrested. It comes less than a month after the
fatal stabbing of 20-year-old bottle shop worker Declan Lavity which has fuelled
community anger over escalating crime in the Northern Territory. Tributes are
being paid to a newlywed paramedic who was fatally stabbed outside a Sydney fast
food restaurant. 29-year-old Stephen Toga was on a break with his New South
Wales ambulance colleagues outside of Campbelltown McDonald's when he was
allegedly attacked. A 21-year-old has been charged with his murder. Emma Gedge,
who was a union branch official with Mr Toga when he worked as a nurse, says
she's thinking of his family. For his new wife, I just, your husband was an
amazing guy and his legacy is going to live on in his kids and we all, we have
your back. We are a tight-knit community and if you affect one of us, if you
affect all of us, so we are there with her. Around 200 residents of Keelaw in
Melbourne's North West have gathered to call on the Federal Government to reject
a proposed third runway at Melbourne Airport. They're worried about the
environmental, health and noise impacts of a third runway. The Federal
Environment Department is assessing the proposal. The airport says it needs a
third runway to cater for increasing passenger numbers. Canada and South Korea
are holding top-level talks in Seoul amid tensions with North Korea over its
ongoing weapons testing program. Canada's Foreign Minister has been meeting with
her South Korean counterpart as the two countries mark their 60th anniversary of
diplomatic relations. They're expected to discuss Indo-Pacific strategy as well
as ways to improve cooperation on economic security and technology. However, the
situation on the Korean Peninsula features prominently with Canadian Foreign
Minister Melanie Joly labelling North Korea's activities as reckless and putting
regional security at risk. Germany is about to shut down its last nuclear power
stations amid an ongoing energy debate in the country. The BBC's Jenny Hill has
more. By midnight tonight Germany's last remaining nuclear plants will be
disconnected from the power grid. It's an historic moment. This country's been
trying to phase out atomic energy production for years but it's a controversial
one too. Nuclear power, once rather unpopular here, is now seen by many as a
cleaner alternative to fossil fuels. It'll take years, perhaps decades, to fully
decommission the nuclear plants. Even as the highly specialised work begins,
Germany's politicians are still bickering over whether they've made the right
decision. The Australian Olympic Committee admits that reaction has been mixed
to its decision to get athletes to leave the Olympic Village within 48 hours of
their events finishing at next year's Paris Games. Under the change, athletes
who choose to stay after their events will do so at their own expense. AOC Chief
Executive Matt Carroll says despite some pushback it's the best path forward. It
is actually all about the performance of the athletes and those four years they
put into it. We spoke to the sports about this and we spoke to most importantly
our Athletes Commission who are elected by the athletes from the Tokyo Games and
they all agreed that this was the best policy to put in place. And a 45 year old
man has died after the truck he was driving left the road and rolled in North
Queensland. Police say the crash happened in Townsville around a quarter past
six this morning. The man suffered critical injuries and died at the scene.
Police are investigating and are urging anyone with dashcam footage of the truck
to come forward. That brings you up to date with ABC News. For more at any time
you can download the ABC Listen app. ABC Listen app. Hello and welcome to Away
Indigenous Art and Culture on ABC RN. I'm Rudy Bremer. Today Filipino Wiradjuri
artist Moju has released their fourth album. Sectioned into three chapters Oro
Plata Mata reflects on capitalism, community and letting go. Moju explains how
the album came together and what impact the last few years of change from
politics to becoming a parent has had on their creativity. You'll also hear from
Emmanuel James Brown as he prepares to take part in a triple bill with
Marigekku, Australia's leading Indigenous and intercultural dance company. Plus
for Word Up, Ebony Joachim shares some more Yorta Yorta language and explains
why she loves being able to learn and speak her language on country. Stay with
me here on Away. Award-winning artist and powerful vocalist Moju is known for
creating music that's highly personal, political and heartfelt. And their fourth
album is no exception. Oro Plata Mata was released last month and is a
compelling collection of neo-soul music. I spoke with Moju about finding
inspiration amidst change and why Oro Plata Mata is the definition of a concept
album. I think the start and the end of the album are the two songs that I'm
most in love with. If that's, you know, I can say that about my own work. It's
just like I really, I'm really happy with how we began the album with gold or
like Oro, which is like the kind of overture or like interlude into gold, which
is quite an unusual sort of structure for a song in, you know, popular music. It
feels like the album is this perfect circle for me, you know, kind of follows
this sunset into sunrise and the middle of the album is like the dead of night.
And that's how I feel it. So for me, Swan Song, which is the very last song on
the album, it's one of the ones that I just feel most attached to because I
really tried to leave with a sense of hope. And that song for me is about that.
It's about sort of saying, well, I'm not ready to sing my Swan Song yet. I'm not
ready to just roll over and kind of, you know, surrender to that fate. I'm still
holding on, you know, there's still a little bit of hope and optimism in me that
I can find an inner peace. There's still uncertainty and there's fear there for
sure. Like, you know, that's the reality of it, but there's hopefulness there.
And I think it's about, you know, the collaborative process and that, you know,
that human connection, human voice is one of my favourite instruments. I think
the way that the voices kind of weave together, it reminds me that, you know,
that's really important. The human connection, come back to community, come back
to family, come back to connecting with one another, because there is a real
disconnect. The more online we are, the more like chronically online we are, the
less we actually hold each other, you know, the less that we see each other. And
that, I don't know, to me, that feels really integral to the life I want to be
living. So just a little puff of smoke. So if this is it, if this is how we're
going to go, will you dance me to the end of time and everything we've ever
known? Will you love me like there's no tomorrow? Let me love you like there's
no tomorrow. Let me love you like there's no tomorrow. Let me love you like
there's no tomorrow. Sing the loveliest songs. I don't wanna sing it all. I
don't wanna sing it all. I don't wanna, I don't wanna. Hey, I would like to see
your face, hold you in one last embrace. Just in case. The sun sets on the world
today. Yet until all hope is gone, I intend to carry on. Until the night becomes
the dawn. Until the night becomes the dawn. I don't wanna sing it all. I don't
wanna sing it all. I don't wanna, I don't wanna. I don't wanna sing it all. I
don't wanna sing it all. I don't wanna, I don't wanna. I don't wanna sing it
all. I, I don't want to, I don't want to close my eyes. I don't want to say
goodbye I don't want to say goodnight I don't want to close my eyes I don't want
to say goodbye I don't want to Swans sing the loveliest songs Swans sing the
loveliest songs Sing the loveliest songs Sing the loveliest songs Swans sing the
loveliest songs Sing the loveliest songs Sing the loveliest songs Sing the
loveliest songs I'm not going to say goodbye I don't want to say goodbye I don't
want to say goodbye I don't want to say goodbye I don't want to say goodbye I
don't want to say goodbye I don't want to say goodbye I don't want to say
goodbye I don't want to say goodbye I don't want to say goodbye I don't want to
say goodbye I don't want to say goodbye I don't want to say goodbye I don't want
to say goodbye I don't want to say goodbye I don't want to say goodbye I don't
want to say goodbye I don't want to say goodbye I don't want to say goodbye I
don't want to say goodbye I don't want to say goodbye I don't want to say
goodbye I don't want to say goodbye I don't want to say goodbye I don't want to
say goodbye I don't want to say goodbye I don't want to say goodbye Somehow the
sunset bleeds into night like money and blood, money and blood There's blood on
our hands yet still we stand here Dripping in gold, dripping in sunlight Somehow
the sunset bleeds into night like money and blood, money and blood There's blood
on our hands yet still we stand here Dripping in gold, dripping in sunlight I
will be able to see the light of day I will be able to see the light of day I
will be able to see the light of day I will be able to see the light of day I
will be able to see the light of day I will be able to see the light of day I
will be able to see the light of day I will be able to see the light of day I
will be able to see the light of day I will be able to see the light of day I
will be able to see the light of day Can't you hear the bell, signal the
warning? Here comes the storm, best we be gone Out to the street where the
legions are forming I heard the call, more than ever before If we just scream
out our screams, we will forget what it means I am flesh, I am blood, I am down
in the mud To protect all the things I believe in I believe in, I believe in
love, rising above hatred I believe that love, love will elevate us Change has
to come, change has to come Ah, ah, ah Why must we wait while you tend to your
ego? There are some wounds so much deeper than that There are brothers and
sisters whose burdens Are stacked so it's breaking their backs If we just scream
out our screams, we will forget what it means I am flesh, I am blood, I am down
in the mud To protect all the things I believe in I believe in, I believe in us,
rising above hatred I believe that love, love will elevate us Change has to
come, change has to come Change has to come Ah, ah, ah Ah, ah, ah Change has to
come, change has to come Well, Change Has To Come comes from the second chapter.
It does, yes. And I've been thinking about the circumstances, you've already
said a little bit of this, but I was thinking about the circumstances under
which the album was written and that there has been so much change politically,
for you personally, you've become a parent, pandemically, we've been through a
lot. Yeah, we've been through a lot. Do you think that all of these kind of
changes and pressures have changed the way that you approach music? Not so much
like my approach to music, I think that my approach to music has always been
consistent in that I never do the same thing twice. My approach to music has
always been that I like to experiment and push myself outside of my comfort zone
and really continue to evolve. I think growth is the most important thing for me
as an artist and I say artist all the time, there are a lot of people who think,
oh, music's a commercial industry, it's not an art. They don't think of it in
the same way as the arts, but for me and the way that I, my practice and the way
that I approach, to me it is art, I treat it as such, so I'm always trying to
learn something from the process. In terms of the way I'm telling stories and
the way I, all the things that I'm thinking about and I want to talk about in my
art, then yeah, sure, like having a child changes everything and it's really one
of those things that everybody says and you don't know until you've had one, but
it really, it did flip my world. And to have a child like right at the start of
a pandemic was such a shock to my reality, because not only is having a child
like this earth shattering thing, it's like what kind of world did I just bring
you into? And so it really, it really gave me a lot to think about, to write
about, to digest and process through the music and there's, you know, that's
what music is good for I think. Is there a particular song that you think kind
of encapsulates that or speaks to that? Definitely, like there's a song on the
Marta chapter of the album, the third and last chapter of the record that I
wrote specifically for my child. Little one, you are my moon, you are my sun,
your whole world has only just begun, please don't be scared about the future.
Little one, such big, big heavy things to hold, but you are only one year old,
please don't be scared about the future. Oh little one, little one, you are
growing fast before my eyes and I would not be at all surprised if you can tell
that I am scared about the future. Because it's raining, raining fire here and
the animals are dying here and I wonder just how many years we have left to call
the future. Our future, our future. Little one, I pray that we can heal
ourselves, I want to make a better world where you can have a future. Little
one, little one, such big, big heavy things for you to hold, I hope you never
have to know why I'm still scared about the future. Our future, our future. Our
future, our future. Our future, our future. It was probably the last song I
wrote for the album and because I knew that something wasn't missing and I think
what it was was that I hadn't really said why I was thinking about all this, I
hadn't really addressed the most vulnerable part of myself, which was the part
of me that's parent now that is like, you know, that has to kind of explain to
my child, well why did I bring you into this world when this is what I'm, this
is what I think about the world right now. So that song is probably, and it's so
far it's proven to be the most challenging one to perform live because it
really, it's very emotional for me to say that. And it was a really hard call to
sort of say, yeah, I'm going to put this on the album even though it feels like
I'm, you know, exposing myself in a way that I'm reluctant to do because of, you
know, I've realised now how much these things can, once they're out in the
world, they take on all this other meaning and they change because it becomes
part of a bigger conversation. It's no longer in control of the context of you,
you know, have control over it in your own context. It's like in this other
conversation now. So yeah, sure. That was like, that was scary to do, but it's
also I think really vital to the narrative of this record. While we're on the
subject of Marta and also in particular live performance, Marta is the final
chapter. And in talking about the end of the album, I do kind of also want to go
back to the start-ish. When the head of the album's release, you performed it
for two special occasions, accompanied by Sydney Symphony Orchestra at the
Sydney Opera House and then Melbourne Symphony Orchestra. Why did you want to
introduce people to the album in this way? Well, because who doesn't want to
play with an orchestra? Right. Like really fair point. Yeah. I mean, why
wouldn't you? It's funny, hey, I always thought this album lent itself to an
orchestral arrangement. And in a lot of ways, like kind of a lot of the
references in terms of the songwriting and the structure of the album and
things, we were listening to a lot of Pacini, you know, like a pop opera, which
kind of was the soundtrack to the film Oro Plata Marta. So, you know, in our
kind of research sort of phase of the album, we'd been listening to that. And so
there's little things that we kind of borrowed from that school of music. You
know, there's kind of an overture approach to the way that we did the interludes
on the album. So we're taking like, you know, interludes being really
commonplace in like hip hop records and, you know, R&B and that kind of genre of
music, which obviously we take a lot of inspiration from as well. But then, you
know, kind of introducing these melodic motifs that happen throughout the album
and like a lot of the instrumentation that kind of sets different moods
throughout the album and introducing them in these little, yeah, like as an
overture, I guess, to the record. So there's things like that. And then, you
know, in terms of like some of the songs, instead of kind of approaching it in
like a more of a pop songwriting structure with like a verse chorus verse, you
know, there's three movements in the song. And so there was definitely a lot of
the songwriting was informed by orchestral music. So to me, even though it's
electronic, it was like, I can hear this being played by an orchestra and I
would love to hear that. And then these conversations started happening with the
SSO and the MSO. And it really, I was, I said to my manager, I was like, Sarah,
don't you think it would be cool to play the whole album live, start to finish
with an orchestra? And she just looked at me like with absolute terror in her
eye, like fear in her eyes and was like, OK, Mojo, whatever you like. No, she's
amazing and really supports me. And I just come up with these like really
intense things that are so ambitious. And I think any other manager would
probably have shut it down. But, you know, anyone reasonable, but she was, she's
just so, I think she's so great. She was just like, you know what, if you want
to do that, let's just see how we go. Like, see if we can make this happen. And
then, of course, you know, she made that happen. And I'm really grateful. It was
a lot of work and a very overwhelming thing to undertake. But I just kind of
imagined it any other way. I think it was like very special and such an
incredible thing to be able to do. I heard that you learned a lot in doing this
process, like in undertaking this process. What was kind of the biggest takeaway
for you? There were so many. There was a lot of technical stuff that I kind of
learnt that we'd never done before. But really, the thing that was really
reinforced for me is just the collaborative nature of music is I think for me
where the joy is at, it is just about being with people in a room and playing
music together and hearing it en masse like that. You've got 40 members of the
orchestra and then like another seven in my band up the front. And it's just
like people in that room that I've never got to actually speak to one on one,
but we communicated with music in that space. And it just, you know, you're
connected through that. There's something else going on that you're performing
that music together, even though I don't know their names. I don't know all of
their names individually. And we never, you know, we never got to speak one on
one, but we played music in a room together and it felt so powerful and so
intense and just so emotional. That's the power of music. Filipino Wiradjuri
artist Moju there speaking about their latest album. Oro Plata Mata is available
to buy or stream now. Or if you'd like to see Moju perform live, they've got a
handful of shows scheduled around the country. For full details, head to their
website, mojumusic.com. This is Away, Indigenous art and culture on ABCRN.
You're with me, Rudy Bremer. Working from bi-coastal operations in Broome,
Western Australia and Sydney, New South Wales, Marigheku harnesses intercultural
and trans-indigenous approaches to performance to expand the possibilities of
contemporary dance. And their new triple bill brings together three creatives to
develop and present a trio of new solo works. Marigheku's co-founder Dahlia
Pigram explains how Borgoja Yalira has grown. It means dancing forwards in Yaru
language and it's a wider capacity building or mentorship program where we've
identified at least 10 artists that we've been investing in, slowly over the
last few years since the program's birth. And our first outcome of that was our
first triple bill, titled Borgoja Yalira 1. And at that time we had three pieces
presented, three short works from Eric Avery, Miranda Ween and Edwin Lee
Mulligan, all collaborated with different and paired with different team members
or choreographers or artists to create those intercultural works. This triple
bill is the second outcome of the wider program. So Borgoja Yalira 2 is just the
new artists that we've given this opportunity to, to bring their artistic ideas
to fruition by curating, again, another triple bill that they can create teams
with and collaborate with other artists that can bring out their amazing stories
and they, you know, allow for their dance to grow. This triple bill also have
their separate titles like New Dew and Bloodlines and There Are No Gods in the
three works that will make up this triple bill, but each of them kind of, while
very, very different from each other in content, they have resonating themes as
well like ancestry and connection. Emmanuel James Brown is one of the creatives
taking part in Borgoja Yalira 2. He's an impressive actor and traditional
dancer. And when he's not performing, you can find EJB working with his
grandfather as a cultural tour guide on Bunabah Country. I spoke with EJB about
his contribution to the triple bill. Hello, my name is Emmanuel Brown. I'm a
Waka Jenga Walunga Yeri, going here in the Bunabah block from Future of
Crescent. And yeah. I'm sure people tell you this all the time, but that is a
star power name. Yep, exactly. He's a star. Yeah. Look out. And you're one of
three choreographers in Marigekku's newest work. Yep. Tell me about your piece,
which is Nyu Jyu. I was looking at YouTube and I just came upon my great
grandmother by the name of Stumpy Brown. Her original name is Nyu Jyu. And I was
looking at it and he was telling a story about a journey from the great sandy
desert coming up over there, where it's homeland. We grew up with a young girl
and we went hunting and learned all the traditional way of life out in the
desert. And when I seen that, I... Also, we was in Perth and I saw the
documentary about her and here we are doing a contemporary dance and telling a
story about her, my great grandmother Nyu Jyu Brown, which is her brothers, one
of the great known artists, Robert Thomas from Turkey Creek. And Robert Thomas
is one of the first Aboriginal artists to have gone to the Biennale in Venice.
Very significant artist, but of course his sister, your great grandmother, also
a very important artist. Yep. How much did you know about her and her work
before this? I was growing up and I was a bit small growing up and not really
know about my great grandmother. Just slowly getting to know her and of course,
even at a younger age, you don't know very much about stuff and probably what
I'm talking about. And so I just know a little bit about her storyline and how
she was a cultural woman and represented herself through her culture and through
her art. And yeah, that's a little bit of things I knew about her. I mean, when
I think about my great grandmother and what I know about her, it's about what
kind of woman she was. Doing more research into her, did you uncover anything
exciting? Oh yeah, especially how she survived in the desert, like for now, this
generation. A lot of people that grew up around in the towns, cities, or
wherever that's close to shops and shops that we can buy food but out there in
the great sandy desert, where she came from, Nga Paoala, there's just vast land,
just sand and desert and spinifex. No water, but through their knowledge and
culture, they knew where everything was, just by falling little animals, such as
the butterfly, also the little finch that lead them to the water hole. Soak
water, they used to dig up, I call them jilla. They used to dig up and water
just bubbled up out of the ground. It was drinkable. AJB, what prompted you to
take this next step into turning it into a dance work? Oh, I was surprised. I
was surprised. I'm Dali and Rachel, they came over and said, AJB, we're going to
make a triple bill. Put Karayala, Yara too. I was just surprised. No worries,
I'm in. If any word comes in your way, comes to you, any opportunity, just make
sure you grab it with both hands, because you'll never get one again. So I got
the opportunity and here I am with Dali and Rachel and a couple of other
dancers, such as Benji Ra, Seth Barrow and Stanley. Yeah, a couple of my friends
and family now, I guess. And yeah, we're really enjoying it so far, and
hopefully everything will get together so we can rock Sydney wherever we travel.
AJB, I'd love to hear from you about what the experience has been like working
on this show. The experience was kind of awesome, I reckon. Yeah. Of course,
I've been learning new things along my journey with this putting everything
together, you know, from little things, big things grow, you know, from just
telling stories and looking at documentary and doing little things like doing
tasks. And Dali has been giving me a few tasks to do, you know, and I was kind
of doing some research for what I can do and put into my solo for my juggle, New
Joe Brown, and I kind of really enjoyed it. And I'm feeling this moment right
here right now and everything coming together, I'll be going to Sydney tomorrow
and see what's going to happen there. Do you want to tell me about your first
experience with Marigekku as a company? First of all, I'm, you know, working
with Dali. 2016 that I met Dali working on a play called Solang Sakhis with Yiri
Yaking. They were two performance actors by the name of Ian Wilson, Peter
Docher. The director was Carl Morrison, and so he decided to tell Dali to take
along for come and work with us for doing a bit of a contemporary on there. I
met Dali then, and at first I didn't even know nothing about contemporary
dancing. I didn't know the steps, the direction, or whatever, you know, because
I used to do an acting for a John DeMar play with Stephen Hawke, and they just
gave me a script and I just created this character. But I'm working with
Marigekku with contemporary dance, oh, that's kind of mind blowing as well. And
I've learned heaps. What do you think has been your favourite thing that you've
learnt from doing contemporary dance? Oh, I don't know how to answer that one,
because there's many of them. I love meeting a lot of friends, family, and got
to travel around a lot, you know, being a pretty boy lately. Yeah, I don't know.
Yeah, really, I always in for the ride and just love every moment of it. You
reckon you're going to stick with contemporary dance for a bit longer too? I
think so, maybe. Maybe, yeah, I think so I can, yeah, I will, I reckon. Until I
figure something out, I'll write a theatre play, or maybe do a bit of acting and
put two of them together, acting and contemporary. Actor and dancer Emanuel
James Brown there. His work, Nyulu, inspired by his great-grandmother and artist
Nyulu Stumpy Brown, is part of Marageku's triple-bill, Burkutja Yalira II. It's
on at Carriageworks in Sydney from the 21st to the 29th of April, before heading
to Perth Institute of Contemporary Arts on the 17th to the 19th of May. This is
Away on ABC RN, and it's time now for Word Up. Bukulba means happy. Word Up,
bringing you the diverse languages of Black Australia. One word at a time. I'm
Eboni Joakim, Yoriyoro woman from Yoriyoro country. Today I'd like to share the
word waka. Waka in Yoriyoro means country, land, ground, district. The reason
I've chosen waka is, waka is our purpose of being here. And we are here along
with all the other beings on this country to care for it because it cares for
us. Yoriyoro waka is quite beautiful. We're the people of the natcha or natchan,
which is plains, and walla, water. We're quite rich with water. We have big
river systems that feed into smaller river systems, creeks, lakes, lagoons. And
one of my favourite things in the world is to be by the water. I've also chosen
waka because waka is the reason why we have language. Language comes from waka.
It is the sound of our waka, the sound of the animals, the sounds the country
makes, the visual connections between those that give us sound and don't give us
sound. Waka talks to us and we talk to waka. Being able to learn and even teach
Yoriyoro language on Yoriyoro waka has been quite powerful for me. Because I'm
on country, it just makes it so much more real. Not that that should take
anything away from people that are learning their languages off country. It's
just you can feel country when you're speaking the country's language. And to
me, learning language has helped me feel waka and to feel the Mulanun more
deeply than I would off country. Ngā Ebeni Djuwakum, Yoriyoro winyā, Yoriyoro
waka. My name's Ebeni Djuwakum, Yoriyoro woman from Yoriyoro country, and I'd
like to say, Ngaan ngaan. Word Up, bringing you the diverse languages of Black
Australia, one word at a time. Yoriyoro language worker, Ebeni Djuwakum. You can
hear the full series of Word Up on the ABC Listen app or wherever you get your
podcasts. And that's our program for this week. Away is produced on Gadigal
Country by Riley Mansfield and me, Rudy Bremer. The sound design is by Roy
Huberman. We'll be back next Saturday at 6pm and on Tuesday evening at 9.
Otherwise, we're always online at abc.net.au slash rn. Or tune in on the app,
ABC Listen. Are you an early career researcher or do you know someone who is?
It's time to apply to be one of this year's ABC Top 5. It's a unique
opportunity. Spend two weeks with the ABC learning how the media works and how
to best communicate your research. We're looking for enthusiastic scientists,
humanities academics, artists and cultural researchers. Head online and see if
you're eligible to apply. Just search for ABC Top 5 for full details.