TRANSCRIPTION: ABC-RN_AUDIO/2023-03-19-01H16M.MP3
--------------------------------------------------------------------------------
 This takes a look into its history. It is right on the border of Jerome and Mt.
Hermon ♪ ♪ ♪ ♪ ♪ ♪ ♪ ♪ Joseph Tawadros is with me in the studio today, and a
little from a recent project of his, History Has a Heartbeat, which is a
collaboration with William Barton on the Adarki. Now, you've known William for a
long time. William, I guess we rose up in the ranks of the classical music world
kind of together. We have similar stories. We're really affected by our
ancestry, and we carry a lot of that lineage, actually, in the instruments we
play. There's a strong tradition. But also me and my brother James actually
going to Redfern Public School. We had Indigenous kids and we had multicultural
kids, and it was this kind of melting pot at school. And so we had a lot of
Yidaki and Didgeridoo players come and elders come and do ceremonies. And so
that was a soundtrack that kind of I grew up with as well. And a lot of people
might or may not know, I was at the Redfern's Beach with Paul Keating. That was
a school excursion. So we had all those things and we cooked kangaroo on the
oval. This is all things that we went through. And they're both ancient
instruments, the Yidaki and the Yud. Two ancient instruments, me and William
doing our thing. And it's really nice. There is something to it. There is
something special when you get instruments of that age together. Yeah. And how
do they meld together sound-wise? Well, there is an earthiness to both
instruments, I think. William's an absolute master at what he does. He's
brilliant. And so he adds his colourings to the stuff we bring. So it is a kind
of collaborative idea and we just work around that. And we're also, you know,
brought up in improvisation and William is a big improviser and he's been able
to have his dig on all sorts of places. I share the same sort of idea, is that
we've got to take the instrument where we can but in a place with integrity, I
think. And how do you place the colours of the sounds of the two instruments
together? Do you tune for each other? William can. Usually you'd have to go
around William because the instrument is kind of tuned to itself. So you have to
kind of work with that. But sometimes he uses a little trick of some toilet roll
that he puts to extend the instrument. It's not as romantic as people think. You
probably go, oh, yeah, he picks the gum from a 4,000-year-old tree or something.
But no, it's just toilet roll. A bit better. Yeah. Well, you're back in the
country and you are finally getting to tour the program with the Australian
Chamber Orchestra that had been cancelled because of COVID and that is the
Vivaldi Four Seasons program. Give me a bit of insight into how you prepare for
such a well-known piece of music. I mean, it's one of the best-known pieces of
music in the world. I've got to say, I'm not just saying this, but Richard does
a fine Four Seasons. He's amazing at it and, you know, I've had the privilege of
being able to listen to him perform it, you know, more than 30, 40 times
probably and I really enjoy his version of it. And for me, it's not about
getting in the way, it's about colouring it. So I'm not getting away in that
he's genius playing, you know, I really love what he's doing. It's just about
colouring some lines, some unisons, which make it a little bit heavier or
thicker. And, of course, you've got James on percussion, the wreck driving it as
well, which is not something that's common in the Four Seasons, there's really
no percussion. So you've got James' percussion driving it, which is already
giving it a different soundscape. You've got Richard doing his thing and I just
have to come in and colour that, you know, and to appear to enrich what we
already have, which is already something very strong. It's just to enrich it a
little bit or add a different flavour to it. But it's about staying out of the
way of something that's really quite amazing in itself. Alright, well, let's
have a little listen to you and the Australian Chamber Orchestra and Summer.
VIOLIN PLAYS VIOLIN PLAYS VIOLIN PLAYS Joseph Tawadros with the Australian
Chamber Orchestra and Summer from Vivaldi's Four Seasons. And Joseph will be out
on the road with the ACO in the coming weeks. Can you give us a little bit of
insight into how you play with Richard? You've done a lot of jazz in your time
with the Oords, and we know that, but when it comes to classical music, how does
that relationship work? I think it's just, again, you've got to be on the same
page. And so Richard and I are good mates, and as much as I've known, you know,
William for 20 years, I've known Richard for 20 years, and we've had a very good
performing relationship and personal relationship as well, and we're happy to
tell each other what works and what doesn't, you know. And as well as the Four
Seasons, there's also the program of my music in the concert series as well. So
I have that to really shine, you know what I mean? That's where I'm very
comfortable with playing my tunes and the arrangements with Richard work. But
then when we go back to the classical, I'm applying what I know through what
I've learned through Arabic music to the Western classical arena. Well, tell me
more about that. So Arabic music's made up of many modes, and so for me, when I
learn a piece, and although we have the sheet music, I like to do it by ear
because as I was saying to Richard today is that any time I've learned any piece
of sheet music, I forget it. I forget it a couple of months later. I just can't
remember it. So I do everything from memory, and I also like not having a music
stand there because you usually have a good pair of shoes. You don't want the
music stand to get in the way. You don't want the audience to see this no-
expenses, spared extravaganza on stage, Robbie, you know what I mean? You can't
deny them that. You're a well-dressed man, Joseph. But it's about finding the
similarities. So I approach it in finding these modes that I've grown up with in
Vivaldi's music, and that's how it works for me, and that's why the music
relates to it because there are so many crossover moments, and for me it's just
like, OK, that's D, this scale, F, this scale, and that's how I'm learning it.
And what about that role of call and response? How does that change through the
piece? That's the thing that kind of combines Baroque music and also with the
Middle Eastern element. Middle Eastern music is strong in melody. In fact,
that's what it is on paper, and theoretically it's only a melody, to be honest.
There's no harmony that's taught. It's just a monophonic music, and everyone is
playing the same melody but in their own way, and through that you get
accidental harmony, a type of heterophany. So with Baroque music and Arabic
music, they have call and response passages, and so when I hear the Four
Seasons, I'm hearing call and responses. How can I respond to Richard? Can I be
a part of the statement that Richard makes and have the orchestra respond? So
it's about finding your role, and that comes with playing it more, listening
more and being comfortable within yourself that that is the right decision. And
you're finding a rhythmic bass line in there as well? Sometimes you can just
join in on the bass line, but it is drifting in and out of what, again, as I
said, you don't want to get in the way of something that's really nice. It's
like a nice meal. When you have really nice ingredients, you don't want to do
too much to it, and that's what it is. I guess the Four Seasons, you can have
paprika, all that sort of stuff. I can't name three other seasons. What have you
got? What about Zata? You've got in there cumin. But watching you play with
Richard and with the ACO, it's really interesting to see the visual cues as
you're playing with each other as well. I guess there's an intimacy to that.
Yeah, I mean, look, that's the way it is. When you play with these amazing
players throughout the years, you just know. And also some of those nods are
also just kind of spurring the players on or a thanks. It's almost like a
gratitude or a surprise, and that's what's really beautiful. I'll look at James
if he's played something that he hasn't played before and I go, oh, I haven't
heard that variation before. And same as Richard, you know. And so that's the
amazing thing because you're also not only just the performer, you're the
audience, and that's what I love about this. And that's what I love with playing
with an ensemble like the ACO and having arrangements like that is that everyone
is involved and everyone is playing something fun and it's a team. You know, you
really have to have that team shine. And when the players are having fun, the
audience are going to have fun and that energy carries across. And that's the
beauty of music and that's the spirituality of music actually is that, you know,
these nonverbal cues that you can just kind of pick up on. And I think that
there is a strong spirituality, not talking about religious, but spirituality.
There's a musical spirituality when you hit that stage and it's not talked
about. And if you're well prepared, then, you know, you feel invincible and the
whole ensemble is invincible actually. Well, Joseph, let's summon some of that
spirituality right now because you're cradling your oud. Can we ask for one more
song? Of course, Robbie. How could I deny? The world needs oud. The world needs
more oud. Joseph Tawadra is performing live for you here in the Music Show
studio. acoustic guitar plays in bright rhythm acoustic guitar plays in bright
rhythm acoustic guitar plays in bright rhythm acoustic guitar plays in bright
rhythm acoustic guitar plays in bright rhythm acoustic guitar plays in bright
rhythm acoustic guitar plays in bright rhythm acoustic guitar plays in bright
rhythm The Dreaming Hermit by Joseph Tawadras, performed live in our Music Show
studio. And he's on tour with the Australian Chamber Orchestra. Details on the
website. Now, we're feeling very spoiled on the Music Show today because we're
going to get another couple of songs live in the Music Show studio. Niamh Regan
is an Irish singer-songwriter from Galway who grew up playing traditional Irish
music, went on to study the flute and guitar at university and has since made
connections to the rich musical fabric of California. She's in the midst of her
first tour of Australia, so we've lured her and her guitar into our studio. This
is Late Nights. Late nights taking its toll on your body Late nights thinking
what I should have said Heart and soul wants to go a little bit deeper
Hindsight's no good now it's setting its turn I'm constantly trying to make you
feel better I was holding your hand holding family together I mean if you don't
go but I don't want to stay here But it's what it is Late nights taking its toll
on your body Late nights thinking what I should have said Heart and soul wants
to go a little bit deeper Hindsight's no good now it's setting its turn I'm
constantly trying to make you feel better I was holding your hand holding family
together I mean if you don't go but I don't want to stay here But it's what it
is All the lovers they'd figure it out some way Oh my lover I'd figure you out
some way Cause you're constantly trying to make me feel better I was holding
your hand holding family together I mean if you don't go but I don't have to
stay here But it's what it is That is Nev Regan performing live on the music
show and Late Nights from her most recent EP In The Meantime. Hello Nev. Hello.
We've all had a few late nights. How autobiographical is that song? Tell me
about it. I know everything's with a pinch of salt, you know. I don't want to
ruin the mystery. Your musical arc is such an interesting one because you have
this real pillar of influence from growing up in County Galway and the
traditional Irish music but you have this other one across the Atlantic in
California and we'll get to the California side of your music in a little while
but let's talk about growing up in Ireland and being a part of the trad scene,
the traditional music scene. Yeah, just describe it for us. In Ireland most kids
learn to play the tin whistle at around five or six. That's kind of like their
first introductory in primary school and I took that very seriously altogether
and by the age of I think seven or eight I was playing flute and competing in
these competitions and I did that all the way up until about 18. And yeah, just
loved it. I came from a family that just, it was all about the music and singing
and ballads and all that so I had no choice really either when I think about it.
So how much of that influence, and I take it it's quite a bit, finds its way
into what you do as a professional musician? I don't know how much people know
about Irish folk songs and stuff. They're usually about death or something
tragic or horrendous happening and I was really young and unoblivious to what
that meant. So I think storytelling, I learned a lot from that, like learning
ballads that I had no clue what they were about. And I guess then just playing
trad tunes was the foundation for my melodies. I definitely write songs that
repeat motifs a lot and I think that's common in trad tunes. And yeah, I think
it definitely consciously and unconsciously informs what I'm at as a songwriter
now. So give us a few details on those trad tunes. What are one or two that have
played a role in your evolution? So I guess the first tune I ever learned was
Maggie in the Woods. ♫ It's just a beautiful little simple tune I think most
kids tackle and I just loved it. I love the simplicity and I love like it's
almost you get into a bit of a trance when you get going with all trad tunes and
then there's another reel that I really loved. I remember it was really morbid
but it's Martin Hayes and his father and it's called the PJ Joe reel. And I
remember hearing it for the first time and I was like, oh God, that'll be my
funeral tune, if not my wedding tune. It's so beautiful. Your voice is so
distinctive and I'd like to know how that early musical influence has played out
in the way that you sing and I wondered have you always had the kind of tone and
approach that we hear now or was it something that developed? Well, I've never
been trained. I've always just sang, definitely learned everything by ear and
yeah, I'm probably doing it all wrong but like I love doing it the way I do. It
feels natural for now anyway and yeah, I don't know how it came about really.
I'm sure like the way I breathe and stuff is informed by flute playing and stuff
but singing is the one thing that I don't overthink and that's why I think I
enjoy it the most. And when you say self-trained, does that mean just a lot of
shower singing or what's the process? It begins in the shower, like all good
days. I grew up in a culture where you would often be asked to sing a song, do
you know that kind of thing and everyone would so you didn't have to be great,
you just had to do it. So then when you grow up in a kind of low pressure
environment that way, I think that helped. Niamh, something that strikes me in
the way that you sing is you introduce some of this almost speak singing. You
sing very beautifully but you bring it into what seems to be a very intimate
method of connecting with the listener. Is that something that's intentional? I
think by now it's probably a little bit more intentional but I used to be doing
that definitely unintentionally. I think laziness sometimes, I'd kind of lose
heart in a song and I'd just speak it and it would drip out. Yeah, sometimes
it's just nice to take a break from singing and just finish the sentence. I
never sing a song the same way. It always has a bit of a change, keep myself on
my toes. Something else that you've mentioned in the past about growing up in
the trad scene and singing currently in the trad scene when you do get the
chance is the lack of ego in it. I wonder how that's informed you too. I think
that's one of my favourite aspects to the trad scene. You'll have a kid that's
about 11 years of age, play everyone under the table and child prodigies walking
around casually but you'll also have the person who can barely string three
chords together and sit in and it all comes and melds together and I think
there's just a real beauty in that. I know I mentioned that I was in
competitions and stuff but just turning up to the FLA and these events was just
really community based and the fact that all these tunes are passed on by just
listening to each other. Do you know? The majority of the players can't read
music, or I'm sure they can but they choose not to. The way they learn is by ear
and I think, yeah, I've kind of taken that with me. You have to be a certain
amount of competitive in life in general, I'm sure, just to stay alive but, you
know, it's just music. If you play it to listeners and you mean it when you play
it, you usually make a connection and I learned that from trad. That said,
you've studied flute and the guitar at university level so you've got the rigour
of that experience too. Yeah. And, I mean, does it combine effortlessly? No,
when I was 18 I started learning to read music properly, do you know what I
mean, after playing for years. And I actually found it really difficult. Like,
it took a lot of patience because I was just so used to like, oh, I'll listen to
this now three or four times and I'll noodle around until I get it. And I loved
that process but it was important for me, if I wanted to make this as a living,
to understand both aspects so I could work with different genres and, you know,
just be more, you know, full circle, I suppose. Niamh Regan is with us today on
the Music Show, as you can probably tell, she is from Ireland, but in Australia
for the very first time. Let's talk about this other pillar of influence,
obviously that experience growing up in Galway and going to university in
Limerick is such a big part, but you have this other connection to Californian
music and to Laurel Canyon and I'm really interested in that. This comes through
your husband, does it? Yeah, so my husband's from Southern California and he's a
songwriter himself and comes from a string of songwriters, hobby songwriters,
and first time I was over there I kind of realised how rich their culture is of,
you know, the garage band scene and the amount of kids that just have bands and
like expressing themselves through songs. I feel like I didn't have that really
until I was a young adult, do you know what I mean? Like I was always just doing
covers or learning other pieces of music, but I love the attitude in America.
It's just such a rich songwriting culture. And so what does an experience like
that, what kind of effect does it have on what you're doing these days? Well, my
brother-in-law has a band called Hippie Cream, right? And they're a bunch of
dads with full-time jobs, very like really sound guys, but they've been together
for I think 20 years and have like a hundred albums, I couldn't tell you, like
it's non-stop. Never maybe commercial success, but prolific. And what I took
away from that was these guys who just wrote songs constantly and some of them
mightn't be the best, but some of them are just so beautiful because they don't
care if people like it or not and they do it for themselves. And then I think
sometimes golden gems come from that when you're not trying to please everyone,
you're just trying to express yourself in that moment and have fun. I took a lot
from that. Well, there is a real freedom in that, isn't there? Right, yeah. I
mean, speaking of California music though, there are those voices that are such
a strong part of the tapestry of music. It's Joni Mitchell is a name that gets
thrown around. But I guess speaking of Joni and Stevie, then there is that
circular aspect that they were influenced by Irish folk music too. I should add,
it's all 365 these days. So when you write, you write with that music heritage,
but when you write for your voice, is there a distinctive approach that you're
taking knowing the way that you want to sing? No, I wish I was that smart to be
that decisive when I'm at it. But usually I just come up with the melody, be it
on the piano or the guitar, and after that I'll hum along with that melody,
searching for a counter one. Usually I have a few keys that I kind of stick to,
I kind of know that's my range. And then I say something over it and then I'm
like, there's a very profound song. And sometimes I really enjoy co-writes as
well. I often write with my husband, he's better with the words and I'm better
with the melody. We fuse them together at the odd time and sometimes it works,
sometimes it doesn't. But yeah, that's kind of my process anyway. Well, in a
very short space of time you have moved through quite a bit of evolution, I
think, musically. In a moment you're going to play us something from your debut
album which came out a few years ago from Hemet, and we'll get a sense of where
you've come from. But let's take a little listen to where you're at at the
moment. This is your latest single, Her Eyes Are A Blue Million Miles. Her eyes,
yeah her eyes Her eyes are a blue million miles Far as I can see She loves me
Far as I can see You love me Yeah, her eyes Her eyes Her eyes are a blue million
miles Far as I can see She loves me Far as I can see You love me As I look at
her she looks at me And in her eyes I see a sea I can see what she sees in me If
she says she loves me Her eyes, yeah her eyes Her eyes are a blue million miles
Far as I can see She loves me Far as I can see She loves me Her eyes, her eyes
Her eyes are a blue million miles Far as I can see She loves me Far as I can see
She loves me Far as I can see Far as I can see I look at her, she looks at me In
her eyes I see a sea I don't see what you see in me You say you love me Far as I
can see She loves me Far as I can see She loves me Far as I can see You love me
Nev Regan and her latest single Her Eyes Are A Blue Million Miles. Now, if
you're a keen listener, you will notice one of two things. The first is that
that is a cover and the second is that that is quite different to well, what her
debut album from a few years ago, Hemet, sounded like too. Let's start with the
cover. It's actually a Captain Beefheart song. Yeah, a bold choice. Yeah, well,
he's a Californian musician. Why Captain Beefheart? Well, to be absolutely
honest, the first time I heard this song was on that film, You Know The Big
Lebowski? And like, I just thought it was nice. And then during lockdown, I
think I was watching a documentary about Frank Zappa. And this name cropped up a
few times and I was like, right, better look this man up. And I fell in love
with this catalogue. It's just wild. You know, I don't know everything about his
personality or anything like that. Well, that was pretty wild too. Yeah. But
musically speaking, I was really struck by how similar it impacted me the way
Tom Waits did. And I've been an avid Tom Waits fan, I'd say all my life. And I
was like, I can't believe this guy fell under the radar for me. The other aspect
to that single is that it is sonically quite different to what you've done
before. There's lots of texture in there from electronics and other kinds of
sources. So is this something that you are moving towards? What's going on
there? So I've just wrapped up recording my second record. And it's definitely a
blend. This cover was a great exercise of exploring new sounds. I probably won't
lean that hard into it for my own. But yeah, it's like a progression into
something different. Because I think every artist wants to just see a little bit
of, I think, not so much growth, but change. You don't want to bore yourself out
with the same stuff all the time. So I've been really excited about the new
production with a guy called Thomas McLachlan in Ashgah Recording Studios in
Donegal. We've been working away and it's been really exciting. Irish trad can
be, well, it can be rooted in the real authentic. And with your explorations
into electronics and more heightened production, is there a need to reconcile
those two disciplines? Yeah, I think it's important. Like there's always the
purists, right? That are always like you have to stick to the book and hold the
tradition up. But like it's ever evolving and ever growing. And for the second
album, I have very distinct ballads that are just piano and voice. And that's
kind of one style. And then the other half are like, you know, the kitchen sink
and having all the fun. And it's okay to have both. You know, I'm not using old
tunes from East Clare and chopping them up and doing whatever I like with them.
I think there's room in this world for both. And I think knowing that both
inform each other is all you have to remember. So going back to those two
musical pillars, you know, one's in Ireland and the other's in California. For
the forthcoming album, whereabouts is the Concord? Are we closer to California
or are we still pretty close to Ireland? I think we're in the middle of the
Atlantic, drowning. Drowning. I don't think so. Look, thank you so much for
coming in and performing live for us today, Niamh. You are going to play one
more song for us. And this is Freeze Frame from that debut album, Hemet, which
came out in 2020. What do you want to tell us about this one? I guess it's just
a song about loss and trying to see the bright side of loss, which can be a hard
thing to do sometimes. I was out, out walking, when I heard two men talking.
There was something in the air, no I knew it wasn't right. But there was
something so inviting to my broken heart, which is trying. There I go, what we
had to fight. I was out, out walking, when I heard your voice talking. I knew it
was in my head, no I knew it'd be okay. There was something so inviting to this
broken heart, which is trying. There I go, what we had to fight, could freeze
the frame. Every moment, if I could freeze the frame. Every moment with you,
cause I was looking at the Hemet. Cause I was looking after him on you. I was
looking after, looking after. Just a little bit more of your time. Just a little
bit more of your time. I'll surely be fine. I was out, out walking, when I heard
your voice talking. I knew it was in my head, no I knew it'd be okay. Cause
there was something so inviting to this broken heart, which is trying. There I
go, what we had to fight, could freeze the frame. Every moment, if I could
freeze the frame. Every moment with you, cause I was looking at the Hemet. And I
was looking after him on you. I was looking after, looking after. Just a little
bit more of your time. Just a little bit more of your time. I'll surely be fine.
Irish singer-songwriter Niamh Regan live in our studio and Freeze Frame, which
first appeared on her album, Hemet. That's all for this week. The music show is
produced by C. Benedict and Kezia Yap. Technical production this week by Russell
Stapleton, Sean Doyle and John Jacobs on Gadigal land. And by Paul Penton on
Wurundjeri Woiwurrung country. Coming up on the music show, we've got the
academic Leah Broad on four of the women who defined the 20th century in
classical music. You mightn't have heard of all of their names, but you'll love
their stories. playing in bright rhythm playing in bright rhythm playing in
bright rhythm playing in bright rhythm playing in bright rhythm playing in
bright rhythm playing in bright rhythm playing in bright rhythm playing in
bright rhythm playing in bright rhythm playing in bright rhythm playing in
bright rhythm playing in bright rhythm playing in bright rhythm playing in
bright rhythm