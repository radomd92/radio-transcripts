TRANSCRIPTION: ABC-RN_AUDIO/2023-05-15-00H00M.MP3
--------------------------------------------------------------------------------
 to the United States and former Senator Arthur Sinodinos joins RN Breakfast.
We're going to talk to him ahead of the Quad Leaders meeting in Sydney next
week, but of course as a senior Liberal of many years, lots to talk to him about
too. That interview coming up right after the news. It's coming up to eight
o'clock. Good morning, Helen Zaremas with ABC News. The Business Council of
Australia says a lack of housing supply, not migration demand, is to blame for
Australia's worsening housing shortfall. It comes as the Federal Government's 10
billion dollar housing fund remains stuck in parliament for at least another
month over concerns it doesn't address the crisis. A Business Council report
calls for an overhaul of housing planning systems, but the Federal Housing
Minister Julie Collins says the Government's already trying to address the
issues. I call for planning ministers to be meeting before the end of the year
to actually look at what reforms they can do in each state and territory to help
get more supply on the ground more quickly. We are adding at every opportunity,
we're doing things, all of the things that are being asked of us right across
the board. A National Anti-Scam Centre is being established to help combat scams
and online fraud. Eighty six million dollars will go towards creating a central
location for people to report scams and to distribute information to banks, law
enforcement and vulnerable communities. Australians lost more than three billion
dollars to scams last year alone, with the issue increasing almost fivefold
since 2020. The Federal Assistant Treasurer Stephen Jones says the Anti-Scam
Centre won't be the only line of defence. We're also going to set up fusion
cells which are like a hit squad where we're going after certain types of scams
and taking the fight up to the scammers to ensure that they don't get an even
break. The Federal Government's announced funding to improve flood warning
systems across the country. Two hundred and thirty six million dollars will be
spent over the next decade to upgrade flood gauges in disaster-prone areas. The
Environment Minister Tanya Plibersek says it'll help make flood data more
reliable. We've seen really terrifying examples in recent years where
communities were left with not enough information, they were unprepared during
floods because of broken or outdated flood gauges. What we're aiming to do is
take responsibility for those flood gauges to give us better warning of floods
and keep people safer. Thirteen regional New South Wales courts will lose the
support of the Aboriginal Legal Service from today. More from Courtney Barrett-
Peters. Aboriginal and Torres Strait Islander legal services in Foster and Byron
Bay courthouses are among the thirteen that will be suspended from today. It
comes after the peak body for Aboriginal legal services called for a two hundred
and fifty million dollar emergency lifeline ahead of the Federal Budget. The ALS
relies on funding from both state and federal governments to provide culturally
sensitive legal support. The Aboriginal Legal Service has reported a 100 percent
increase in demand since 2018 and says access to justice for Aboriginal and
Torres Strait Islanders isn't a priority for the government in this year's
budget. But a spokesperson for the Federal Attorney General says the
government's committed to working with Aboriginal and Torres Strait Islander
people to achieve better justice outcomes. South Australia's strangulation laws
could soon change to hold more perpetrators accountable. The current Act
requires proof a person's breath was restricted by strangulation, making it too
difficult for many cases to stick. South Australia's Attorney General, Kai Marr,
says a draft bill which would only require proof of applying pressure to the
neck will probably be up for consultation this year. Strangulation often leads
to escalations in violent domestic abuse and strangulation is shown to be a
precursor to domestic homicide. But we want to catch this sort of behaviour
early. We want to make it easier for police to catch people and we want to make
it easier for prosecutors to get convictions. To sport in the English Premier
League, Arsenal has suffered a potentially fatal blow to its hopes of winning
the championship. The second place Gunners went down 3-0 to Brighton overnight.
Top Place City extended its lead to four points with a 3-0 win over Everton. The
Sky Blues can clinch the title next weekend by beating Chelsea at home or if
Arsenal loses to Nottingham Forest. In this morning's other match, Brentford had
a 2-0 win over West Ham. Aussie golfer Jason Day secured his first win on the
PGA Tour in five years, claiming the Byron Nelson event in Texas by a shot. The
former world number one shot nine birdies on the final day to finish at nine
under par. Aussie tennis player Lexi Poppren threw to the fourth round of the
Rome Masters. He beat Russia's Roman Saffolin in straight sets 7-5-7-5.
Poppren's next opponent, the Danish seventh seed Holger Ruin. And Australian
writer Jack Miller has crashed out of the French Moto GP. Miller was leading
during the early stages but had dropped to seventh by the time he fell on lap
25. You've been listening to ABC News. On RN Breakfast, you're with Patricia
Carvelis. Good morning. Coming up, voting is closed in Thailand's general
election. And early results show there's a strong challenge from two progressive
anti-military parties. So could this be a turning point for the country, ending
nearly a decade of military backed government? Find out when we speak with a
former Thai foreign minister and democracy activist very soon here on RN
Breakfast. And a new Outbreak Noir film opens this week, outback rather, which
follows an investigation of the cold case murder of a local Indigenous girl 20
years ago. The film, it's called Limbo, explores the impact of the justice
system. And you're going to hear from Indigenous Australian filmmaker Ivan Sen
and Emmy and Golden Globe nominated actor Simon Baker just before nine o'clock
this morning about that film and their work. Remember, you can text 0418 226 576
and all of the other ways too, including the ABC Listen app. And you're
listening to RN Breakfast, six minutes past eight. It began as an informal
diplomatic group. Now it's a key pillar of Australia's foreign policy. The Quad
brings Australia, the US, India and Japan together. And next week, Australia
will host the Leaders Summit for the first time. But US President Joe Biden
could be forced to cancel his visit if he fails to strike a deal on the biggest
issue in DC right now, which is raising the debt ceiling. Arthur Sinodinos is
Australia's former ambassador to the United States and our guest this morning.
Arthur, welcome. Hey, PK, how are you? Good. Thanks so much for joining us. The
Quad Leaders Meeting is the biggest geopolitical event held in Sydney, I think
since what 2007 and the APEC summit. Would Biden being absent undermine its
success? Well, I think it's not the same if the president is not there. And I'm
hoping that, you know, he can make sufficient progress with the debt ceiling
negotiations here in DC in the next week or so that it makes it easier for him
to come. I mean, the stakes are getting pretty high in the debt ceiling
negotiations. And obviously, there are major economic implications both for the
US and globally, if those don't get resolved sooner rather than later. But it'd
be great if he could be in Australia. Of course, it'd be both a bilateral visit
as well as the Quad meeting itself. But, you know, Joe Biden did a great thing
in elevating the Quad to the leaders level. It's really turbocharged the
process. And there's lots of good stuff coming out of the process. So it'd be
great to maintain the momentum in Sydney. Critics question what the Quad
actually achieves. What do you think the tangible outcomes from this summit will
be? Well, I mean, first of all, the process of meeting in itself is important,
particularly having India there as India starts to flex its muscles and become
stronger. As the Modi government, if anything, cements appears to cement its
position, it's important that India is broadly coming into the global rules
based order in a way that I think is consistent with promoting the aims of that
order. So the process itself is important. And having India as a full fledged
partner in it is being very important. What it achieves, the first thing is it's
about positive outcomes. It's not defensive. It's not an Asian NATO. It's not a
defense pact. It's actually to provide a set of public goods for the region,
including initially around pandemic vaccine production distribution.