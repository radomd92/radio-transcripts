TRANSCRIPTION: ABC-RN_AUDIO/2023-04-15-03H00M.MP3
--------------------------------------------------------------------------------
 The Music Show. Good morning, Kerry Worthington with ABC News. A Sydney man
charged with selling information about Australian defence and security secrets
to Chinese spies remains in custody. Alexander Sergo was arrested in Bondi
yesterday following a joint ASIO and counter foreign interference task force
investigation. Federal police allege after two foreign intelligence officers
offered the 55-year-old payment, he compiled reports for them about national
security. He's faced paramount a local court via video link with the matter
adjourned until Monday. A 21-year-old US National Guardsman has appeared in
court in Boston accused of leaking highly sensitive military intelligence
documents. Jack Teshera was charged with two counts related to the unauthorised
removal, retention and transmission of classified documents and materials.
Former federal prosecutor Mark Chutko says the case is very serious because the
leaks concern current world issues. What makes this case harmful and more
significant is that the real-time nature of the information. This potentially
reveals sources and methods of intelligence gathering of the United States. It
provides information to the Russians, munitions and other battlefield tactics of
Ukraine and supply of Ukraine by other countries. The mother of a man who took
his own life after receiving demands to pay a $28,000 robo-debt says she plans
to develop a suicide prevention program. Jenny Miller's son, Reece Calzo, died
in January 2017 after he was contacted repeatedly by debt collectors. Ms Miller
and Kath Madjouic, whose son Jared also died after being notified of a debt,
want to begin the program in schools after the findings of the robo-debt royal
commissioner handed down. Ms Miller says education is key to preventing suicide.
We need to talk about it more. All of us have that responsibility, I think, to
get this out there in the media and go, guys, we need to talk about this. You
know, sometimes people aren't going to pick up the phone and I do think it all
goes back to education. If you or someone you know needs someone to talk to,
call Lifeline on 131114. A man charged with the stabbing murder of a paramedic
in Sydney remains in custody after facing court. Leah Harris reports. 21-year-
old Jordan Finianginofo did not apply for bail in Parramatta bail court this
morning and it was formally refused until his case returns to court on June 28.
He's accused of fatally stabbing a 29-year-old rookie paramedic outside a
McDonald's in Campbelltown yesterday. The paramedic was sitting in the ambulance
with a colleague on a break after a night shift before the alleged unprovoked
attack. Finianginofo's lawyer, Javid Faiz, told media outside court his client
is being treated for mental health issues. Northern Territory police are
investigating a fatal stabbing in Darwin overnight. NT police say a woman was
killed after being stabbed outside a hotel in the CBD. They say an injured woman
had entered the hotel seeking help and she died a short time later at Royal
Darwin Hospital. A man's been arrested and a crime scene declared. Police are
urging any witnesses to come forward. The RSPCA has seized more than 200
roosters allegedly used for illegal cockfighting in Victoria. Yvette Gray
reports. The RSPCA's major investigations team raided a property in the Melton
area in Melbourne's west yesterday following an anonymous tip-off the location
was expected to be used for an illegal cockfighting event. More than 200
roosters were found. Many were confined in cages with no access to water and in
poor overall health. Six RSPCA inspectors and two vets assessed the birds who
were described as highly aggressive. Authorities say they have significantly
disrupted an alleged illegal animal fighting ring and investigations continue to
identify and prosecute those involved. Authorities in Western Australia are
turning their attention to damage assessments as ex-tropical cyclone Ilsa moves
into the Northern Territory. Ted O'Connor reports. The cyclone's most
destructive winds set records and ripped apart a coastal roadhouse but avoided
townships such as Port Hedland and Marble Bar. There's fears stock losses at
some cattle stations could be significant. A small crew of workers who remained
at gold and copper mining operations in Telfer will clear debris and assess
potential infrastructure damage this morning. The weather system dumped huge
amounts of rain in desert catchments and could leave some remote dirt roads
impassable for an extended period. Ted O'Connor, ABC News, Broom. Briefly in
tennis, the defending champions Stefano Zitsipas and third seed Daniil Medvedev
have been knocked out of the Monte Carlo Masters in the quarterfinals. Zitsipas
was beaten by American eighth seed Taylor Fritz while Holger Ruin downed
Medvedev. This is ABC News. This is The Music Show and I'm Robbie Buck. We are
listening to Ruth's song for the sea off Kit Down's first solo album recorded
with ECM, Obsidian. Later in the show we'll sit down with Kit for a chat and a
live performance from the Music Show studio. Our list of guests to the show is
listed below. First, let's dive into some heavy metal. We'll Play Till We Die is
Mark Levine's second book and explores the revolutionary music cultures of the
Middle East and the Islamic world, before, during and beyond the waves of
resistance that shook the region from Morocco to Pakistan. Mark is a rock
guitarist and professor of history at the University of California Irvine. Lucia
Sobera is the senior lecturer and chair of discipline of Arabic language and
cultures at the University of Sydney, where she studies the colonial and post-
colonial history of the Arab world and Africa. Let's start with some music
coming out of Morocco. Music【Music Ah, la la la la la This is my rock and roll
My rock and roll I laugh I laugh I laugh for no reason I laugh because of the
hype Your Your Your Your Your Your Your Your Your Your Your Your Your Your Your
Your Your Your Your Your Your Your Your Your Your Your Your Telecaster Football
Game Game Game Game Game Ha There is El Cahid, Motorhead by Hobo Hobo. There is
El Cahid, Motorhead by Hobo Hobo Spirit. One of the groups that's referenced in
the book will play till we die and we have the author Mark Levine with us today
on the music show. Hello Mark. Hi, how are you? Great to be here. And also
joined by Lucia Sorbera who's a senior lecturer and chair of Discipline of
Arabic Language and Cultures at the University of Sydney. Hello Lucia. Hello.
Alright, the book is subtitled Journeys Across a Decade of Revolutionary Music
in the Muslim World and that decade is an interesting decade. Let's go to the
start of it where things were quite tumultuous, Mark. Well, this book is in fact
a follow up to the first book I wrote about music in the region called Heavy
Metal Islam. My research there started in 2002 so I've been researching this for
over 20 years. That book kind of predicted that something big was going to be
happening soon. It was really clear if you were working with young people,
especially artists on the avant garde that something was going to happen, that
things were just too repressed and this huge youth culture was just too
explosive. And so when it happened it wasn't really surprising to me in late
2010, early 2011 that so many of the people who were involved as the most
important activists in the region came out of the scenes, the music scenes I was
looking at which was the heavy metal scenes, then the emerging hip hop, hardcore
punk scenes, the same skills that it took to create an underground music scene
it turns out are very useful for creating an underground political scene and
breaking it big. It's like breaking a band in a way. So that's how this book
really came to be is seeing all the friends I had made over the previous 10
years out on the streets in the lead of these uprisings and I wanted to just
follow their stories and how their lives evolved and then bring in a lot of new
artists and new activists who I'd met along the way. And Lucia, your perspective
on the cultural output of that very vibrant time. It's very multifaceted. I
focus mostly on women's movements and women's political activism in Egypt and
what I have seen over the years is a growing participation of women in the broad
political spectrum of all political formations and especially the social
movements that emerged in the 90s. So women have one century of political
activism in Egypt. They have been in all the political movements, the
underground Marxist movements, the Islamists, the liberals, the democratics, the
students movements. So it was not surprising that they were at the forefront of
the 2011 revolutions because they were already there and especially young women
were very vocal and they raised topics that until then were taboo. Some of the
most high profile activists for the freedom of political prisoners today are
some high profile women, lawyers, writers and activists. Mark you mentioned your
first book which was Heavy Metal Islam. Give us some insight into that
particular genre and how it played a role and indeed how that attitude grew into
different genres too. You know I guess when I was growing up my best friend was
from Yugoslavia, the one that was still Yugoslavia and he used to tell me how
they used to sneak, this is in the 70s, into the woods and play all these band
heavy metal albums like Black Sabbath and Nazareth and all these old 70s bands
because for them it just represented a kind of freedom. We all know in the
former Soviet Union also rock and roll and especially metal was really big. So
similar thing here. The difference is as so many people have said to me, when
you look at an Iron Maiden album cover and you see Eddie with skeletons and
corpses and tanks and it's like a fantasy but for so many people in the region
that's their reality. So of course metal is going to make sense if you grow up
in highly authoritarian societies with a lot of violence and even war, metal
makes sense, really makes sense. And also the cathartic element just as for kids
all over the world especially in these kind of societies it's a release that you
really can't get anywhere else especially if you're already not someone who fits
into the norm. And what about in Egypt, Ligia? It's very broad in Egypt, the
scene is very diverse and it's also affected in the broader cultural scene, if
you look at the literary sphere there has been for instance an explosion of
comics and graphic novels and pop cultures more in general which comes before
the revolution and in many ways some of the themes that were discussed in these
graphic novels predicted the revolution as well. They all discussed very deep
and important political and social issues that were then put at the forefront of
the revolution like social justice, the fight against corruption, the fight
against the violence of the police. Similarly if we think of the lyrics of metal
songs or also early hip hop songs when it was much more political, groups that
were big were really the hardcore metal like Deicide or Cannibal Corpse or
Napalm Death, these kinds of really extreme metal genres became the ones that
were really popular and the lyrics make sense. And the other thing is for young
people in the region, especially where you're heavily surveilled and monitored,
to be able to sing brutally in English where no one really can understand you
unless they're like really fluent in metal brutal singing, it's a very safe way
to say things about regimes that wouldn't be very happy if they knew what you
were singing. But these days it's not just metal is it that gets such a
representation and I take it it's the attitude not just the sound of what's
being played here, you've also got as you mentioned hip hop, there's what is it,
maqraganat? Maqraganat it's a wonderful genre of music that emerged out of hip
hop and maqraganat means festival in Arabic. It started at weddings, street
weddings, blasting sort of what we call shabi or popular music with very fast
and danceable beats and then people would start rapping over it and it just
evolved into its own genre. I've attended some of the performances and I and you
know there's also a broad scholarly literature about that now you know there are
ethnotropologists who are studying those and from multiple perspectives
generational gender perspectives and they are all emphasising how these genres
are politically very engaged and it's played an important role in mobilising
people. So what's the reaction been like in places like Egypt and elsewhere in
the Middle East to these kinds of musical forms which as we know can be
viscerally rebellious? The scenes especially the metal scene which came first
became really popular first across the region we're talking Morocco to Pakistan
right straight across every country had a decent metal scene and Egypt one of
the best but certainly not the only one. By the mid late 90s as it became
popular it started getting a backlash just like happened in the US the decade
before and people's kids started getting arrested they were prosecuted for being
Satan worshippers a lot of the scenes went underground for another five or six
years but by the mid 2000s especially as these authoritarian governments which
really couldn't do much for their citizens except arrest them and torture them
they really couldn't give them what used to be known as the authoritarian
bargain you know better education social development healthcare jobs that all
was falling apart by then so there was kind of a liberalisation culturally going
on similar to what happened in China in a way and so you know by then people
were like it really doesn't matter it's not really against Islamic law to play
certain forms of music even Saudi Arabia now has a thriving open metal scene
when I first started this it was mostly underground now even Iran you know
depending on the week sometimes you can have a metal show sometimes you can't so
in most countries though music has become accepted once again because music's
always been at the centre of Muslim cultures for 1400 years there was a short
period and you know that it hasn't been but now most places other than
Afghanistan perhaps don't really have any limits on music as long as it's not
explicitly anti-religious. If you turn on the TV and look at the Middle East a
lot of the reporting of course is looking at the Palestinian territories and the
situation there has deteriorated in many ways what's happened to the musical
activity whilst this has been going on Lucia? Music is a space of resistance and
of assertion of existence of people so there are there continues to be festivals
of music but also of literature in May the Palestine Festival of Literature
which is an itinerant festival organised by the writer Ahdaf Suhaif who is
Egyptian and she's an advocate for Palestinian rights and Robert Homer Hamilton
they try to bring world literatures across the Palestinian territories and their
idea is as the people can't travel we travel with books and writers. Palestine I
always tell to my students is a miracle of culture because there is not I think
other people in the world that could keep alive such an important literary
musical theatre culture under these circumstances and they managed. Yeah also
there's the Palestine Music Expo which very much is built on the same framework
as the literary festival which brings in artists from outside and yeah the hip
hop scene of course is very big and very important the first video ever Arabic
language hip hop video ever to get a million views on YouTube which I know today
seems like nothing but back in 2006 to get a million views in Arabic that was
something was the band called Dom which was actually a band of Palestinian
citizens of Israel and since then Palestinian hip hop has become perhaps the
most important hip hop scene in the Arab world the most political hip hop scene
certainly right up until a couple of years ago during one of the Israeli
invasions of Gaza when there were protests against the border a rapper named MC
Gaza literally was filming in the midst of the fighting a music video which he
then released to the world and to people who were participating in the Palestine
Music Expo showing which was really one of the most powerful videos I've ever
seen as the smoke and the tear gas and bullets are flying he's rapping so it's
very much powerful but also the first Arabic metal band to actually sing brutal
in Arabic is a band called Halas which means enough which came out of Haifa and
Akko or occur so how son has always been at the forefront because it is
literally the crossroads of the region and because so many Palestinians are in
the diaspora and so many people come in and out and also the influence of Israel
and the Israeli metal scene the Israeli hip hop scenes and the Israeli
electronic scenes also some of the most avant-garde and best DJs and electronic
dance music people also coming out of Palestine. Mark you come to the age-old
argument about protest songs from an interesting perspective you studied at the
conservatory for a little while you're a professional musician you're also a
scholar of Hebrew and Judaism too and you come to this world of music with a lot
of on the ground experience from your perspective seeing what you've seen and
being in these areas of the world what is the role of music for the protest? I
can give you two quotes but I think capture it the first is from Fela Kuti you
know the great Nigerian founder of Afrobeat who said you know music is the
weapon of the future it's such a powerful cultural force that you can actually
use it as a weapon against authoritarian regimes because it can tell the truth
in ways that no other artistic genre can as viscerally and as powerfully
connecting to people the other quote that I come back to almost every day in my
life is by this African-American playwright and documentary filmmaker Tony Kade
Bambara who said the role of the artist is to make revolution irresistible and
that's really what art does it gives you a conception it helps you go from just
a subculture to a counterculture to a revolutionary culture articulating a new
vision of the world it can't you can't win you can't defeat bullets just with
music but if it helps make the idea of a different future not only possible but
urgent then it's playing a very necessary role I don't think there's been a
revolution that didn't have a music or a form of art that was associated with it
so in that sense I think it's necessary it's crucial but of course so many other
factors go into whether it actually works in the long run especially as Bob
Dylan's career would show us you know how easily the artist and the music can
become co-opted commercialized commodified monetized and which point happened to
hip-hop especially you know it loses its revolutionary impetus and Lucia from
your perspective the role of women in all of this they're everywhere it's
crucial you know maybe we should not even talking more about the role of women
because you know they're just there and of course they're you know with their
bodies with their presence they are they are at the forefront of a fight Farid
Al-Nakash she's a veteran of journalism in Egypt and she has been you know a
high-profile political activist told me once in an oral history interview women
are attacked brutally because if you take away from the square if you control
for the population you control all the population yeah and and therefore there
is this tendency by authoritarian regime to attack women first because they they
are absolutely crucial to generate you know identities and education and and
space the way we live the space if you make the city unsecure for women the the
city is unsecure for everyone and and that's exactly why in the in the phases
where the regimes were more brutal or where revolutions had to be crushed the
first to be attacked were women I would just say also you know the first great
metal band I saw a thrash band was an all-girl band who were playing at this
festival this DIY festival in Morocco called the Boulevard Festival in 2005
there have been you know women rappers you know metal artists EDM artists etc
punk rockers from the very beginning in this region they've been at the
forefront of it when they weren't out front on the stage they've been behind as
some of the main organizers of concerts of festivals so women have been at the
avant-garde of the scene of all these scenes and when you go to a show like in
Egypt I organized one of the first metal shows to be held in Cairo after the
satanic metal scares in the mid-2000s and you see a bunch of teenage girls all
of them wearing head scarves head banging next to a bunch of guys you know right
next to them like in a real mosh pit kind of situation squeezed together and you
think even I thought you know that's not supposed to be there right that's not
supposed to happen but of course it's just normal for them that's why I think
music's so powerful because it forces you to re-examine our you know long-held
prejudice about what it means to be a woman or a girl or how people interact in
reality it's so much more complicated than we imagine and the interesting thing
is that with women activists they suffer so much violence from regimes but these
regimes are also really smart so women artists they like to co-opt so in Morocco
that band I mentioned this band of young women the the king's daughters became
fans of them so that they started getting money now of course once you get money
are you going to say anything about the king of course not right so if you're an
artist you can be co-opted a lot more and that's really the tension also that
artists face in the region where they're so desperate to have some support yet
they also want to maintain their integrity and the political grounding of the
music and you know women have usually been the best at negotiating this well
let's go out with a song this is God Allow Me Please to play music who's this by
oh they're so powerful and so wonderful voice of Acer Pro is an Indonesian all-
female metal band same thing you know people said why are these young women
playing metal boy can they shred that's all I can say I wish I played guitar
like them and I've been playing 30 plus years so they're a great way to go out.
Michael Vine and Lucia Sobera thank you so much for your time. Thanks for having
me. I'm not the criminal I'm not the enemy I just want to sing a song to show my
soul I'm not the criminal I'm not the enemy I just want to sing a song to show
my soul God Allow Me Please to play music God Allow Me Please to play music God
Allow Me Please to play music Why today many perceptions have become toxic Why
today idealizations are using our minds I feel like I'm falling down in the deep
hole of hatred I'm not the criminal I'm not the enemy I just want to sing a song
to show my soul I'm not the criminal I'm not the enemy I just want to sing a
song to show my soul God Allow Me Please to play music God Allow Me Please to
play music God Allow Me Please to play music That was God Allow Me Please to
play music by Voices of Barchiprot or VOB an all-female rock trio from
Indonesia. You're listening to the music show. Kit Downs is a BBC Jazz Award
winning and Mercury Music Award nominated solo recording artist for ECM Records
with a practice that spans solo piano, solo organ, composing and a diverse range
of musical collaborations and we've got him in front of the piano in our studio
right now. He's a tricky artist to define so we might let his music start the
conversation. Kit Downs with a live improvisation at the piano. Kit Downs with a
live improvisation at the piano. Kit Downs with a live improvisation at the
piano. Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs with a live improvisation at the piano. Kit Downs with a live
improvisation at the piano. Kit Downs with a live improvisation at the piano.
Kit Downs playing live in the Music Show Studio with an improvisation inspired
by Australian Birdsong and Jetlag. Kit Downs playing live in the Music Show
Studio with an improvisation inspired by Australian Birdsong and Jetlag. Kit
Downs playing live in the Music Show Studio with an improvisation inspired by
Australian Birdsong and Jetlag. Kit Downs playing live in the Music Show Studio
with an improvisation inspired by Australian Birdsong and Jetlag. Next time on
the Music Show, the life and music of Australian composer Margaret Sutherland
via her biographer Gillian Graham with plenty from the ABC Archives, a pivotal
figure in the architecture of Australian music both metaphorically and
literally. Kit Downs with a live improvisation at the piano. Kit Downs playing
live in the Music Show Studio with an improvisation inspired by Australian
Birdsong and Jetlag. Kit Downs playing live in the Music Show Studio with an
improvisation inspired by Australian Birdsong and Jetlag.