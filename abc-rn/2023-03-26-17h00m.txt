TRANSCRIPTION: ABC-RN_AUDIO/2023-03-26-17H00M.MP3
--------------------------------------------------------------------------------
 walks and gardens and the really dedicated among us have the year-round early
morning ocean swim on God forbid daily rituals and secular habits and why we're
drawn to them a special series so join me Sunday 6 a.m. or anytime on the ABC
Listen app. ABC News with Tom Melville. Thousands of people have gathered
outside the South Australian Parliament as the state passed legislation making
it the first jurisdiction in the country to establish a First Nations voice to
Parliament. Evelyn Manfield reports. Both Houses of Parliament sat in a special
Sunday sitting so the passing of the voice legislation could be witnessed by as
many members of the public as possible. On North Terrace a crowd of thousands
cheered and waved Aboriginal and Torres Strait Islander flags as the laws were
assented to by the governor in a ceremonial meeting of the state's executive
council on the steps of Parliament. Attorney-General and Aboriginal Affairs
Minister Kaia Ma addressed the crowd. Today we've lit a beacon of hope for the
rest of the nation. The voice representatives will be elected in coming months
with the body expected to be running before the end of the year. South
Australians who witnessed the bill pass from outside Parliament say emotions
were high as they watched history take place. I've come here today to celebrate.
I think this is a really momentous, significant moment in Australian history.
For years and years and years we've waited and it's finally happened today on
the 26th of March, 23. Everyone's emotional today, every little people here. One
of the potential front-runners to take over as New South Wales Liberal leader
Matt Keane has ruled himself out of the race. The former treasurer has long been
touted as a likely candidate for the leadership. Outgoing Premier Dominic Perete
on Saturday said he was standing down as leader in the wake of the election
defeat. Matt Keane says he has a young family and wants to spend a little more
time with them so won't be running as leader or stay on as part of the
coalition's leadership team. In a statement he says he'll continue to serve his
Hornsby constituents and the party and thanked outgoing Premier Dominic Perete
for his service and the community for its ongoing support. Liberal MP Anthony
Roberts has told the ABC he's considering running for the leadership after being
approached by federal and state MPs. The Victorian Premier will travel to China
today, the first Australian leader to do so since the signing of the Aukus
submarine deal. Daniel Andrews says the week-long trip will be focused on
attracting more international students to the state. China slammed the Aukus
deal earlier this month as a blatant act that hurts peace but Mr Andrews says
he's looking to cement Victoria's trading relationship with the country. The
diaries and the remit of the federal government are a matter for the federal
government. I'm there to represent Victoria's interests and that is all about
more students, more trade, more deals being done so that more product and
services from Victoria goes to China so that our income goes up. To Sport Now
where the AFL is investigating alleged racist remarks from a St Kilda fan during
Saturday's match against the Western Bulldogs at Melbourne Docklands Stadium,
Patricia Tiano reports. The Western Bulldogs have released a statement saying
racism of any kind does not belong in our game nor in our society. The racist
comments were allegedly directed at 20 year old Western Bulldogs player Jamar
Yugal Hagan during and following the Saints 51 point win over the Bulldogs on
Saturday night. St Kilda says that to be repeatedly addressing these repugnant
instances of racism is obliged on our game and society. Both clubs are working
with the AFL to identify people involved. Fans from both sides have shown
support taking to social media to say I stand with Jah. The AFL has confirmed
it's investigating the matter and is working towards identifying the individuals
concerned. Western Australia has underlined its dominance over domestic cricket
claiming a second successive Sheffield Shield title with victory over Victoria
in Perth. WA wrapped up its nine wicket victory inside four days knocking off
the required 91 runs in under 25 overs. The victory hands WA consecutive triple
crowns in men's domestic cricket as winners of the one day cup, Big Bash League
and Shield. WA opener Cameron Bancroft said the group was playing some
incredible cricket. Having got a lot of words it's just been a pretty
unbelievable little period of time. Yeah we all really enjoy playing with each
other and you know winning games of cricket and and trying to thrive in the big
moments and that's what you live for and yeah we've just been able to play some
really good cricket and it's been pretty amazing. Newcastle has won its first
NRL game at Hunter Stadium in seven matches coming from behind to beat Canberra
by 10 points. The Raiders led 14 points to 8 at halftime but the Knights scored
three unanswered tries after the break to secure a 24 points to 14 victory.
Winger Greg Marzieu scored two tries on his first start for the Knights and told
ABC Sport his teammates laid the scores on a plate for him. In Saturday's other
matches the Sharks thrashed the Dragons by 40 points to 8 and the Warriors beat
the Bulldogs 16 to 14. You're up to date with the latest ABC news. For more news
at any time you can download the ABC Listen app. This is Duet on RN with Tamara
Anna Czyszlowska. My guest today has occupied many of the most prestigious
positions in the Australian classical music scene. He was for several years
principal cellist with the ACO and has been a frequent guest principal with the
Sydney Symphony, the Tasmanian Symphony and the Australian Opera and Ballet
Orchestra. In the early 90s he was invited to join the Australia Ensemble and in
1995 formed the highly successful Goldner String Quartet. These two groups
continue to be regarded as representing the pinnacle of chamber music in the
country and the Goldner Quartet has established an international reputation with
rave reviews of their recordings and performances in the UK. He has performed a
critical acclaim at major venues and festivals throughout the world, made over
30 CDs on leading labels and premiered many works by Australian and
international composers. Julian Smiles, welcome to Duet. Hello Tamara. It's a
very long bio, I couldn't even find a shorter one, I could only find longer
ones. And I think a lot of people will be quite familiar with that one, it's one
I've been using for a long time. But you've just done everything, is it possible
you've done everything? I've done a lot. I'm getting to an age where I can look
back on my musical career and sort of feel that it's had sections which are
quite distinct from each other but there's a thread of continuity through it,
very strongly based in chamber music and through my work with the Goldner
Quartet and the Australia Ensemble. Let's travel back to a lounge room in the
ACT. Sometime I would say in the 1970s, the lights are out, there's a small boy
listening intently, he's lost in his own thoughts and he can barely contain the
excitement that he feels in his own body. Who is that boy? That sounds a bit
like me. I grew up in a very musical household. My mother was a very keen
amateur pianist. The only reason she wasn't a professional was that in those
times it was really necessary for her to bring up the three children. I was the
youngest of three. But she loved music so much and we had a wonderful recording
set of records. And I just discovered from a very early age that music was
something that moved me and excited me and made me feel all sorts of emotions.
And as you said, I might listen to something like Sibelius, Karelia Suite and
find myself dancing around the room just through this sort of sheer build-up of
excitement which was impossible to suppress. Would you escape the family and go
into the lounge room? Would you do it yourself? Music was very much a shared
thing in our household. We all loved it and I was just following in the
footsteps of my brother and my sister in loving music and taking up instruments
and being involved in the Canberra Children's Choir and all these wonderful
things that we were very fortunate to be able to experience. Do you remember any
pieces that your mum used to play that stick in your mind? Well, I just loved
listening to her practice and she had a particular fondness for the music of
Brahms and Schumann and she in particular, there's a piece, I've often thought
it'd be nice to go through my life and find the pieces of music that mean a lot
to me. And one piece that came out was the Brahms Intermezzo from the Opus 118
set of pieces and she used to play this and it just resonated through my whole
life. I heard a very fine pianist play it at the Huntington Festival a couple of
years ago and it was so wonderful to hear it again. The A major Intermezzo from
the Opus 118 of Brahms, you heard Radulupu on piano and that was the choice of
my guest on duet today, Julian Smiles. Julian, does that bring back memories for
you just hearing that again? It's just extraordinary music in any context. Of
course it has great resonance for me but just extraordinary music, extraordinary
confidence in that composition. Who is Nelson Cook? Nelson Cook was this pillar
of the Australian music scene, one of the greatest cello teachers that this
country has ever had and responsible for the careers of many very fine cellists
such as Howard Penny and Li Wei-Chin and myself. And Nelson I met when I was
about seven years old. I thought he was terribly old then but he's probably
about my age now and I was taken by my mother with my agreement to the Canberra
School of Music where they were setting up an elementary strings program and
these three old guys looked at me and they said oh he's a cellist so I started
learning from Nelson and I learnt with Nelson all the way through my high school
years and through my degree course so I was with him for about nine or ten
years. You said that he made you love performing, love music. Yeah, from the
very beginning just the idea of walking on stage let alone sitting down and
playing an instrument. He brought that out in us and we were you know these
little kids walking on stage and confidently announcing to the audience what we
were going to play and he would strut. He would walk around after his students
played and said wasn't he fantastic, wasn't she wonderful and this sort of gave
this feedback of joy in making him happy and making the audiences happy. He was
such an inspirational person. Does that bring back any memories? You've chosen
Stravinsky next. Yeah, I was just remembering that Canberra in the mid-80s was a
wonderful place to be for a musician. The Canberra School of Music was at its
peak and the Canberra Youth Orchestra Society was something I discovered through
again through following my brother and sister and I ended up in the Canberra
Youth Orchestra in about 1983 and in 1985 we did a big tour of Europe conducted
by the wonderful Rick McIntyre and we performed all over Europe and the pinnacle
of it for us was playing at the International Festival of Youth Orchestras in
Vienna and the Canberra Youth Orchestra won the prize for the best symphony
orchestra. On that tour we were playing the Firebird Suite which has such a
wonderful ending to it. It just lifts your spirits in such an incredible way.
The finale from Stravinsky's The Firebird and you were saying Julian that
there's a famous incident where a lady woke up in fright at a performance. Not
at the end but somewhere around what I was playing before. Yes, it's one of
these little videos that does a circuit on the internet. A lady screaming when
that big surprise bass drum is whacked at the beginning of the last section.
Such an amazing composer again. I'm in awe of composers. To have come up with
the ideas they come up with whether it's Brahms or Stravinsky is just amazing.
How did you feel when you went overseas for the first time? Because when you
went with that Canberra Youth Orchestra I think you'd never been out of the ACT
and everyone you knew lived near you. Pretty much. I was about 16 years old when
I went overseas and a great adventure. Not many kids my age at those times had
the opportunity to fly in an airplane let alone go overseas. It was eye-opening
to experience musicians from all over the world and to get a sense of
perspective. The first experiences for me of an incredible sadness of passing of
situations that I particularly felt were wonderful for me and whether it was
relationships or playing particular pieces and then not playing them again. I
remember in particular that in the beginning of 1985 I went to a music camp and
I learnt the Schoenberg Transfigured Night and we played it as a chamber
orchestra and that was a wonderful occasion and I was meeting new friends and
learning these incredible pieces of music. And later in the year again on this
Canberra Youth Orchestra tour we visited the Menuhin School and a chamber group
performed the Transfigured Night and I just lost it during that performance. I
was so overwhelmed with the sense of the fact that nothing is permanent and
these things that we learn when we're older and we have people that we love pass
away or leave us. I hadn't experienced that before but that was the first time I
felt that real deep grief about thinking well that situation back in January
which I found so beautiful it was never going to happen again. Schoenberg's
Transfigured Night you heard the quartet Ebbén with violist Antoine Tamaschtit
and Nicholas Alstead on cello and that was the choice of my guest on duet today
Julian Smiles. It's a magical D major chord isn't it at the end. Is there a
better ending to a piece? Look when you invited me to do this program you said
think of all your favourite pieces and I mentioned to you in our pre-discussion
that it's like walking through a forest and then saying which are your favourite
trees. I think that if I had enough time I could find a thousand pieces that
finish just as beautifully. Well I haven't told you, you're coming back every
week we're going to do this every single week until we we get through every
piece in the repertoire. If someone was going to ask me about who are the
strongest most athletic musicians I know with the best physiques, best dressed,
best diets, best fitness regime who do you think would be at near the top of
that list Julian Smiles? Well your list would probably have me near the top of
it but no I like staying fit I think it's very important for me actually as a
cellist to be able to play well. Again I was inspired in this by Nelson he
always said you have to be fit. I took his word at it and kept myself fit but I
feel for example if I don't swim I that's when I often feel like there are sort
of the beginnings of little problems and niggling pains that are associated with
the sometimes not completely natural acts of playing a cello. Again also was an
inspiration for my family because my dad was a very athletic guy and swam a lot
and took us swimming in the morning in Canberra on freezing cold mornings when
there was steam rising off the water only because the water was warmer than the
air temperature but not very warm. Yeah so I keep myself very fit. So you're
used to the rugged athletic life? Let's not say rugged. Rugged? I like that
word. But it wasn't that long ago that you got a very different kind of
diagnosis something that must have been a huge shock. Yeah about a year ago now
in fact I was diagnosed with a fourth stage cancer which is the cancer I was
diagnosed with is not one that's very good to have and I've very quickly say I'm
doing very well at the moment. As everyone would agree that in similar
situations it's a moment to take stock of what you've achieved more than
anything else in your life and what you've experienced and I did a lot of
thinking about that right at the beginning and I found myself very pleased with
where I've been what I've experienced and very fortunate to have done all those
things. So for me the glass is very much more way past half past half full you
know it's my glass is overflowing and I feel really wonderfully blessed to have
had what I've had and from now on every year is a bonus for me. I've just passed
the first anniversary so and I'm doing really really well. I'm surrounded by
people who love me. Does a health crisis bring you closer to music or do you
turn to other things at a time like that? It's an interesting question but again
one of the first things I thought about as one should is what do you really find
valuable? In your life what are the things that you find that you you you love
doing and for me I'm so happy to say it's exactly what I have been doing which
is performing and teaching. Through the first half of 2022 while I was on
chemotherapy I continued to teach and I continued to perform. I did cancel a few
tours which were a bit questionable in terms of my energy levels but I just love
music so much and it gave me a great sense of purpose continuing with that and
also there's a sort of feeling that that when you love music so much it's like
you treasure every particular performance of a piece and one of the first things
that hit me when I was diagnosed was I was listening to a performance of a
particular work I loved and I thought I might never get a chance to play that
again and that in a sense upset me as much as anything else that was going on at
the time. It was quite interesting reaction. I asked you about music that you
find unbelievably exquisite and you mentioned a piece it's actually a piano
piece but I thought I actually brought the music in because I thought we might
just play a little bit of it together. Seeing as you have the cello and all and
I have the piano here but do you know where I'm going with this Julian? I think
you're talking about the second movement of the Shostakovich second piano
concerto. Is that one of your favorites? It's just an extraordinary piece of
music. You know you don't like to talk about you know people ask you this every
now and then what would you like to have played at your funeral and I must
confess that that's been closer to my mind over the last little while than usual
but far before this year I'd often thought about this particular piece and
there's a quite extraordinary way it builds up with the sort of dark and
brooding sounding string section in the beginning and then the way the piano
comes in the major key near the beginning is quite extraordinary and then later
on when there's a recapitulation it comes back in the minor key and it's just
devastating at that point. And if we kept going we'd get to your favorite part
where it's minor again. I don't know I think my favorite yeah but that main tune
which is in the major so the first time that's yes it's devastating to play as
well. The other thing that occurs to me is that I'm always interested in this
this sort of thought experiment about you know people that say they don't like
music how could they listen to that and not be affected in some way. I was just
reminded for some reason of something that Nelson had said Nelson Cook that he
used to say to you that's going to be very very good yes that will be very very
good the emphasis on going to be that's going to be very good. But it's so
positive. It is. It's a beautiful encouragement. You know it's in a way it's a
sort of slightly needling comment because you're saying if you work hard it will
be good. Is that the sort of thing you say to your students? Oh you put me on
the spot there. I try to be unfailingly positive with them. They'd probably tell
you something completely different. If I was your student would I be scared of
you? I would hope not but I'm unfortunately I find a lot of them are scared of
me and I really don't know why. I think my strength or what I like to think of
as my strength as a teacher is that I approach every student differently. I look
at where they are. I look at their physical build. You know the way they they're
built. How big their hands are and their musical approaches. What they like
musically. What they don't like musically and try and just get inside their mind
and work with them rather than saying okay everybody's got to play this piece
this way and this is the repertoire that we're going to work through. The one
size fits all approach. Why is Corngold your next choice? Eric Corngold is one
of these composers that we sort of know about fairly well because of his film
compositions but I think he's enjoying a renaissance now for very obvious
reasons which are that he's an absolutely amazing composer and many of us in the
music profession would know the violin concerto very well. We've had this long
relationship in the Goldner Quartet with Hyperion Records and we've done a
series of about 10 or 11 discs so far with the great Australian pianist Piers
Lane and one of the most recent ones in one of the most recent ones it was
suggested to us that we do the piano quintet by Eric Corngold which none of us
knew and as is so often the case when I'm learning a new piece I'll get sort of
perspective of the composer by listening to other music by them and I suddenly
found this incredible world of music which is completely unlike any other
composer. He had his own language which is so incredibly rich and there's spice
in it. It's dense as well in the most extraordinary way. We re-performed that
piece this year well in 2022 at the Australian Festival of Chamber Music in
Townsville and I was reminded that I think it is the hardest piano quintet I
have ever played. The slow movement is a set of variations on a piece by him
from a set a song cycle called Songs of Farewell and the song is I think titled
something like Moon Once Again You Rise and I would urge anybody to listen to
this incredible song but he takes that song and turns it into this incredible
set of variations. Corngold's piano quintet in E major that was the second
movement the Goldner string quartet and Piers Lane. This is Duet on Rn with
Tamara Anna Czyszlowska. Corngold just transports you doesn't he Julian? It's
I'm still reeling from that performance. As I said completely unlike anything
else and for as a performer to be inside the perfect performance of that would
be a life-completing experience and it's something in any piece we're searching
for all the time but there are moments in the middle of that which are just so
incredibly beautiful that it's a privilege to be playing them. Why is chamber
music the best thing of all to do Julian? I think the the biggest thing for me
is the the input that we all have. It's often something I've reflected on with
my career that when I was a student I think that a lot of people might have
assumed that I would have pursued a solo career and in a sense I got distracted
from that because I became very successful and involved with very fine chamber
groups right at the beginning but I think that as a soloist the one thing you
you miss out on is this this opportunity to keep working with people and try to
persuade them about different ideas and have other people put ideas to you about
how music should go. Often when we take a break over Christmas and we come back
working together in whatever February as the Golden Quartet or the Australia
Ensemble I'm so happy to get back with my colleagues and start sort of talking
about music and picking apart a Beethoven string quartet that we've been playing
for 18 years but still looking for something new in it, still trying to hear it
as the audience will almost like you know some of them will be hearing it for
the first time so to to try and achieve that objectivity where you you're not
playing it as if you know it inside out you're playing it as if you're playing
it for the first time and again as I said with the corn gold when you when you
hit that perfect performance of a piece whether it's you know the variations
from Death and the Maiden Quartet or Dumkey Trio or something like that to be
part of that where everybody's got an input into it and you you you're reacting
to each other and you're speaking to each other is really wonderful experience.
I really enjoy playing in orchestras and and did a lot of that when I was
younger but I think as I get older I love chamber music more and more. Do you
think as time goes on you get more nostalgic or less? Oh god more nostalgic.
I've become like I don't know whether it's just getting older but I just I've
become more and more emotional so I often will talk about a particular piece of
music to people whether you know sometimes embarrassingly to a student and choke
up in the middle of what my description of why I think a piece is so beautiful
or what I think the composer is thinking of or what it means to me. If there was
someone that you could choose Julian an inspiration to you maybe a singer that
you would hope to embody as a cellist would there be a singer that stands out
for you? I particularly love Dietrich Fischer Disco singing and purely by
accident I came across a recording of him back in the day when I was playing
with the Australian Youth Orchestra. We had a big tour of Europe again in 1988,
bicentennial tour and we were being conducted by a very fun conductor by the
name of Christoph Eschenbach and it all sort of came together that it was about
the time that CDs were being you know introduced quite strongly into the market
and I just bought my first CD player on the way across in Singapore and I went
to a record store in London and I thought I must try and find something with
Eschenbach on it and I found this recording of Schumann's Lieder with Dietrich
Fischer Disco and Eschenbach playing the piano with him and ever since I've just
been completely taken by the way Fischer Disco sings, it's very human, very free
singing and I also particularly love his approach to ensemble which is not to be
together all the time he actually pulls on the his associate artist he sort of
leads them and there's a quote about playing in string quartets that it was a
member of one of the great string quartets said they spent the first 10 years of
their career learning how to play together and then the next 10 years learning
how not to play together so I love this sort of feeling of tension in the
ensemble. Montnacht that was Christopher Eschenbach on piano with Dietrich
Fischer Disco singing Robert Schumann's song that was the choice of Julian
Smiles who's been my guest on duet today we're sitting together in Eugene
Gusen's hall and there's something spellbinding about that song in particular
just the key the the timbre of his voice it's everything all together isn't it?
There's no artifice in it I'm sure behind that level of control is a tremendous
amount of practice and you know study but to bring it to that level where it
just sounds like it streams out of you is what we all aim for really. Did you
bring something along that we might play? I did now there's a piece that for so
many years I would hear this piece and I think what is that I love that piece
and then it would slip through my fingers and I could never find out what it was
but eventually I found it and it is the minstrel song by Alexander Glazunov. Do
you have a piano score for me? I think I could rustle one up. Is it one of those
ones where I just have to play a few chords if you have a big melody and do all
the funny the fun and showy things and I just tag along behind? That sounds like
exactly the way we should do it and we will apologize that Glazunov was famously
he sabotaged the first performance of Rachmaninoff's first symphony by being
quite drunk. He was on the source. Yeah well he was constantly on the source. I
understand that at the conservatoire he had a bottle of vodka under the desk
with a long tube running out. Did you bring one of those as well? I don't think
I should reveal that on live radio. Okay let's just get ready and you can tune
Julian and I'll just remind everyone at home that you can listen to this episode
with Julian again and to other episodes and you can watch clips at abc.net.au
slash classic and just follow the links to duet. Julian smiles that was very
very good. Thanks for the duet. It was a great pleasure. Thanks for having me.
ABC RN often informative. Chat GPT is fast and scary. The expert tweaking really
made it sing. Always entertaining. Move over Mills and Burn. The hot smart
scientists are in the house. Saudi Arabia is untouchable. Your emotional actions
are connected to the physical response. It's vaguely licorice sort of phenyl
taste. The great unretirement. Analyzing the data. Just hanging on tight mate.
Stay informed and be entertained. ABC RN.