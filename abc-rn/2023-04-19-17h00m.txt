TRANSCRIPTION: ABC-RN_AUDIO/2023-04-19-17H00M.MP3
--------------------------------------------------------------------------------
 You'll also hear from Emmanuel James Brown as he prepares to take part in a new
triple bill from Marigheku. Plus, for Word Up, we head back to Yorta Yorta
country. That's all coming up on Away, but right now, it's news time. ABC News,
I'm Clive Hunter. The federal government says it will provide in principle
support to all of the recommendations made by a wide-ranging inquiry into the
Reserve Bank, including a separate monetary policy board and governance board.
In the coming hours, Treasurer Jim Chalmers will release the findings of the
highly anticipated review and announce the appointment of two new board members.
Federal government is being urged to back a key recommendation of the Economic
Inclusion Advisory Committee to substantially increase jobseeker payments to $68
a day in the upcoming May budget. The government has already ruled it out,
citing the $24 billion cost. Edwina MacDonald is the deputy CEO of the
Australian Council of Social Service. She wants the government to think again.
We've got a government right now that's planning to spend $250 billion on tax
cuts for the wealthy. Lifting jobseeker to adequate levels would cost half of
what we're seeing the statutory tax cuts will cost. New research shows young
gamblers are losing more money than ever. As the presence of sports betting
advertisements on social media grows, the Australian gambling research centre's
Rebecca Jenkinson's found gambling ads are having the greatest impact on those
aged between 18 and 34. The more that young people are exposed, the more likely
they are to bet and engage in that riskier betting. But it was across all
platforms, so it's not only TV. We really saw strong links between exposure on
those social media platforms or streamed content and that engagement in risky
behaviour and harm. The helicopter pilot who rescued a group of Indonesian
fishermen from an island 300 kilometres west of Brune has described it as a
textbook mission. The 11 fishermen were rescued on Monday after being
shipwrecked during a cyclone Ilsa and surviving six days on a tiny island at
Roly Shoals without food or water. Wayne Thompson says one of his crew was set
down to determine whether the helicopter could land safely on the sandy island.
A decision was made to winch the survivors aboard instead. Mr Thompson says the
main concern was whether they had enough fuel to complete the trip. We thought
we were going to have maybe just over an hour of fuel on site and as it turned
out that was enough for us to arrive, assess the scene and winch all the
survivors back on board, get back to Brune safe and sound. The paramilitary
force.