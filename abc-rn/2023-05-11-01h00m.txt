TRANSCRIPTION: ABC-RN_AUDIO/2023-05-11-01H00M.MP3
--------------------------------------------------------------------------------
 Music Music Good morning, Helen Zerremas with ABC News. The Federal
Government's defending its budget ahead of the opposition delivering its reply
tonight. The opposition leader Peter Dutton expected to back some of the smaller
initiatives proposed by the government and attack others he argues are
inflationary. The Federal Treasurer Jim Chalmers says he's very, very confident
his budget won't add to inflation. The $14.6 billion cost living package doesn't
all hit the economy at once. It's spread out over four years. A big amount of
the spending in the coming year is actually funding ongoing programs. Some of it
is the small business tax breaks, the impact of that on the budget. The Federal
Greens say they've been left unimpressed after the government made a concession
to crossbench demands on its $10 billion housing fund. Labor's proposal to
establish a future fund would invest in 30,000 social and affordable homes over
the next five years. The Federal Government now offering to index the spending
cap against inflation from 2029 to 2030. But the Greens say the shift isn't
enough, saying they'll support the bill if Labor works with states to implement
a rent freeze and adds more funding for affordable housing. The Nurses and
Midwives Union says Australia needs to develop a consistent and coordinated
national plan if it wants to successfully combat the health worker shortage. It
comes after the Queensland Government declared it'll pay frontline health
workers $20,000 to relocate to Queensland from interstate or overseas. The
Queensland Union's Kate Veach has welcomed the announcement, but says the entire
country needs to work together. We are welcoming of the incentive, but we see
that there is a puzzle here and it has many, many pieces. So while states and
territories take initiative individually, we're not going to have the
coordinated response that we need across Australia. The newly merged group
pursuing a no vote in the Indigenous Voice to Parliament referendum says it
hopes to send a clearer message about its campaign. Until now, the two main
groups, with the Warren Mundine-led, recognise a better way and fair Australia.
The group with the backing of Shadow Indigenous Australians Minister Jacinta
Ngampunginba Price. Mr Mundine says the creation of the Australians for Unity
campaign was months in the making. It's able to make sure that some people can
be able to hijack things and turn into very vitriolic racist type stuff. And so
by working together in that way, we've reduced that opportunity. The New South
Wales State Government says banning political donations from clubs which receive
money from poker machines will close a loophole in the political system. The
Government is set to table a bill in State Parliament today which will include
clubs that house gambling facilities in the list of prohibited political donors.
Gambling businesses are currently banned but clubs are exempt because of their
non-profit status. The New South Wales Special Minister of State John Graham
says he hopes to change that. This bill that we're going to bring before the
Parliament would close what some have described as a loophole and would mean
that clubs with gaming interests are no longer able to donate into the political
system. A British tabloid says it owes Prince Harry compensation for spying on
him. More from Europe correspondent Nick Dole. The admission came at the start
of a phone hacking trial against the publisher of The Mirror. It says there was
one occasion back in 2004 when it unlawfully obtained information about the
Prince and his activities at a nightclub. It's apologised but it's disputing his
claim that it routinely ran stories based on hacked voicemails. The Duke of
Sussex's lawyers claim some of the paper's senior staff including former editor
Piers Morgan knew about the practice, something he denies. Nick Dole, ABC News,
London. To sport European Soccer Champions League and Inter Milan's taken a step
towards reaching the final. Inter beat its cross-town rival AC Milan 2-0 in the
first leg of their semi. This morning both goals scored in the first 11 minutes.
To AFL and Port Adelaide forward junior Rioli's been handed a two-game ban for
striking. Rioli was referred directly to the AFL tribunal for striking
Essendon's Jordan Ridley during the Power's five-point win at Adelaide Oval last
weekend. Rioli will miss Port's games against North Melbourne and Melbourne. And
Aussie cyclist Caden Groves has won the crash-marred fifth stage of the Giro
d'Italia. Groves among those to fall on the final bend with seven kilometres to
go. He recovered to win from Italy's Jonathan Milan while Norway's Andreas
Legnuschen retained the overall lead. Groves had two third-place finishes in the
opening four stages and says he's pleased to have now broken through. This is
ABC News. I'm Hilary Harper. Time to get out your down ball or your hacky sack
today. We're looking at high school playgrounds. Sadly a lot of them are
basically an oval and a basketball ring if you're lucky and a lot of hard
surfaces. And that will suit some kids but others might feel there's no place
for them in that kind of space. On Life Matters Today, coming to you from the
lands of the Kulin Nation, can playgrounds make high schools more inclusive?
Let's find out. If you've got everyone from year sevens to year twelves using a
playground, all genders, different needs and interests, can you design it to
cater to everyone? I'd love to hear what might be included in your ideal high
school playground, particular facilities, climate-related features, some kind of
innovative seating. Send me a text, 0418 226 576. Can you design a playground
with features that everyone will love? Gwyneth Lee has been delving into the
world of playgrounds for some time now. She's a PhD candidate in public health
at Canberra University. Gwyneth, great to have you on the program. Great to be
here. Great to be here. Now, you're a landscape architect as well. What got you
interested in school playgrounds? Well, I've always had a strong interest in the
impact that we have on the way that we design open space. I mean, a lot of
research shows that not all open space is created equal. Some deliver well-being
benefits in a better way to users than others. And we know that growing research
shows more time spent outside is actually really good for us. It can improve our
mood, it can change our habits by being more reflective, it helps us engage with
people and be physically active. So school yards really caught my attention
because it is this opportunity where students are provided every day with a
daily dose of time outside, especially at a time when they really need it as a
source to escape from, say, academic and social stresses. But it wasn't always
clear to me whether the designs of these spaces were really enabling students to
reap these well-being benefits of time outside. Well, yes, we kind of inherited
certain design features of playgrounds from very long ago schools, didn't we?
How well are they contributing to health and well-being for kids now? That's a
really good question, Hilary. I mean, the design brief for school yards really
haven't changed much in the decades that have passed. It's probably a space we
can relate to from our parents and our own experiences now and to what our kids
are experiencing. But I think given that we live in an age of COVID and climate
change, I think these spaces can really work harder. And I think from the
research I've been finding is that I think they really need to because students
right now, they're experiencing increasing rates of anxiety and stress and
mental health issues. And I think we can use the outdoor spaces as a real
preventative tool to help them cope during the school day. Well, we learned a
bit from the lockdowns, didn't we, here in overseas, about how useful
playgrounds can be. Can you just summarise for us some of the takeaways
researchers learned during that time? Yeah, sure. Well, I think we found that we
need access to open space. I think a lot of us, when we found we were
constrained to our houses, we did provide us with a sense of relief. When we got
outside, we could engage with others and we can get a bit of fresh air and
change the conversation. And we're finding the same kind of results in some of
the case studies I've been looking at. I had the opportunity to survey students
in years seven to 10 and to really understand and explore, well, how are they
deriving benefits from these spaces? And I looked at it in two ways. One way was
by actually asking the students, well, what do you want from your schoolyards?
And are your spaces delivering what you want? And the students told me they
wanted places that allowed them to be adventurous, that gave them a sense of
belonging, that was liberating and welcoming. And then I asked them another set
of questions in this survey, which was more about wellbeing. And it was asking
questions such as, are there interesting places in the schoolyard? How does it
make you feel? Are those surroundings making you feel like you've got separation
from some of your classroom stresses? And it was really interesting to see what
the results revealed. That in year seven, a lot of these students, both boys and
girls, were around the same place. They were really excited to be in these
spaces. And they were pretty positive about them. They actually, if you could
give them on a typical scale, they'd give them a solid B grade, I'd say. But
then as they progressed, say, to year 10, there was starting to be a real sense
of dissatisfaction within these spaces. And there was quite a contrast in how
boys and girls were rating this dissatisfaction. Boys seemed to move from giving
the schoolyards a B grade to more of a C grade, but the girls were quite
different. They plummeted pretty fast and they didn't recover in their rates of
approval of these schoolyard spaces. They went more from a B grade, more to a D
or an F by the time they got into year 10. Well, I'm interested in that gender
difference, but the age difference is interesting too. Is it that people started
out wanting to play and the outdoor spaces were good for play and then they had
less interest in play, or just that the ways they wanted to play weren't being
served by those spaces as they aged? I think it's a bit of both. I think when
the students initially make that jump from year six to year seven, it's pretty
exciting. They move from these playground space, program spaces, to these big
open fields. It can feel quite liberating, like they have more freedom. The
scale of it is just bigger. But then after a while, it can also be quite
startling. I remember hearing from a lot of parents that I talked to that year
seven students, they fumble around a bit. They don't know how to engage with
these spaces because there isn't a lot of programming and encouragement of
informal and imaginative play. I think that as they progress into year 10 and
12, I think that there's limits in terms of the variety of type of play spaces
that they have. As you mentioned earlier, you've got sporting fields, you've got
concrete courtyards. And if you're lucky, you'll have some benches and seating
areas, but it's pretty sparse. And I think it limits not having a very diverse
and a lot of variety available. It can limit what they feel they can do. You
mentioned the stark difference in satisfaction between boys and girls. What was
behind that? I think there's a lot of factors at play here. I think one is that
given that you have low variety, as we say in terms of the hard courts and the
ovals, there can be an uncertainty in how to engage with those spaces,
especially at a time when students are experiencing adolescence. Their bodies
are changing, they're maybe not as confident in their physical skills. And so it
can kind of cement traditional stereotypes in terms of the boys as they grow
larger and they're more physically active to engage in more traditional sport.
And I talked to some female students and they find that they'd like to engage,
but they can find it quite confining and a bit intimidating. And on top of that
can be uniforms, which actually play a really big role, especially for female
students, because suddenly they also jump from wearing active wear in primary
years to suddenly skirts and dresses. And that can be really confining in terms
of the types of activities that they feel comfortable engaging in. Yes,
depending on the school. We're speaking with Gwyneth Lee, who's been researching
for her PhD the ways that playgrounds are designed and how well they're serving
the needs and interests of high school students. I mean, Gwyneth, would
redesigning the playground help with that kind of overarching gendered
difference? Would girls suddenly feel like they wanted to play more sport and
feel less, I don't know, observed and judged and monitored just if the
playground was a different shape? Oh, listen, I think it would make a tremendous
difference. And I think giving students the agency to have a voice in expressing
what they want because they've got so many ideas. And they are really the
experts of schoolyard spaces because they know them so well. There was one study
I looked at which had principals and students identify risks within play yards.
And the principals identified maybe 10 to 20. Students had twice as many. They
know these spaces well. And the students, what they've shared with me,
especially the females, is that they want something like slow speed socializing,
right? They want to move, but maybe they don't want to play soccer. They want to
be able to be social and engage with their friends. So it's places like having
opportunities for gymnastics, being able to dance and play music. They talk
about incorporating traditional structures like ovals, but also fitness
equipment where they can actually work on their own fitness as well as still
engaging in conversations. So I think the schoolyard, it's a very social space.
It's a space that people want to have a sense of belonging. And I think we need
to recognize that. And as a result, create a variety of spaces. As we have
teenagers kind of in this tadpole state of transitioning from youth into
becoming an adult, I think the more variety that we can provide them, the better
off they can be. We're talking a moment about some of the lovely ideas that the
students had that you asked them. But I want to read you some of the texts that
are popping in because this is something that obviously touches a nerve with a
lot of our listeners, Gwyneth. Kids need access to open space and to moving in
open space. Says Kim, and she's used capital letters very clearly to illustrate
the passion that she has on this topic. Given its real overall benefits, says
Dean in Mount Albert in Victoria, more immersive nature in schoolyards is an
absolute necessity for kids' physical and mental health into the future.
Concrete tar and barriers are not the answer. One of the main issues with high
school playgrounds, says another text, is the lack of shade for kids.
Unbelievable when we have such a high rate of melanoma. Biggest problem is with
newer builds, they're like concrete jungles. I feel sorry for the kids. Put up
some shade cloths before the trees grow up. So a lot of, you know, different
ideas. Young people have particular favourite spots, don't they, Gwyneth Lee?
There are aspects of design that they favour. Tell us what's behind that. Oh,
absolutely. A part of it is that just as we enjoy going to cafes and having a
chance to people watch, so do students, right? So they want these areas where
they can feel safe, where they can retreat and hang with a cohort of their
friends. But at the same time, they want to be able to observe and to see what's
going on and to feel like they're part of that greater community. And I think
that we need to keep that in mind as we design these spaces. I think right now
you'll find a lot of students, you know, they'll sit on the sidelines in the
grassy ovals, but it's not a very comfortable place to be. I think students, and
some of the ideas they've shared with me, is that they want a sense of prospect,
a sense of horizon, where they can look across the schoolyard and they can have
opportunities, or have hangout spots where they can hang out with their friends
and then watch what's going out on the playing field, but have a chance to then
duck out and join the game when they feel comfortable, as well as duck back into
the hangout zones where they can hang out and just be at ease with their friends
again. So it's really, it's giving them choice and helping them feel safe and
helping these spaces feel accessible to them. And on that issue of choice and
safety, the text reads, is it a gender difference or just a difference in how
they like to take a break? I'm male, says this person, and when I was at school
I hated sport. I either wanted to play music or talk to friends. And I know
there's been work done too on, yes, that nuance that some people, male or
female, want to play computer games in a quiet spot, and some people want to
play sport and it's not necessarily gender segregated. On that too, Melbourne's
Kingswood College, I'm informed, ditched its football uniform in favour of
activewear, I think that means school uniform in favour of activewear, and found
an increase in student activity levels. Excellent to hear. Gwyneth, you ran a
competition that came up with some really innovative solutions for playground
design. What were some of the changes that students suggested? It was fantastic.
I mean, on the one hand, as a lot of students were suggesting oversized play
areas. So there was still that desire to get that sense of awe and that sense of
adventure. Some suggested Versailles-styled mazes. Others suggested castle
playgrounds where you had a moat that was 30 metres wide, which you had to cross
on a canoe. So there's that real desire. Completely. Everything that would
challenge and encourage you to take risks. But there were also a heap of ideas
talking about welcoming spaces, spaces that were comfortable, where you could
lounge around and just feel relaxed and get away from issues during the day. And
it was surprising and impressive to me that there, the desire to extend that
welcome across year levels. The thought of having students from year seven
through to year 12 could feel comfortable and safe within those spaces. And
issues of colour and beauty. That really makes a difference in terms of how a
student feels valued within their spaces. So they're talking about blue for
relaxation or bright colours that remind some of the playgrounds from primary
years. Some were incorporating slides, as you mentioned earlier, and nest
swings. And it wouldn't just be one slide. It would be many so that you could
make it a game and make it a competition. Wow. We're speaking with Gwyneth Lee,
who's a landscape architect and PhD candidate in public health at the University
of Canberra, looking at how playgrounds could be different and better and make
students happier because they're not that satisfied with them, particularly as
they move through the secondary school year levels. Let's check in on what's
happening in design at the moment. Daniel Smith is chair of Learning
Environments Australasia, which is the peak body for school design in Australia
and New Zealand. Daniel, welcome. How focused are designers on playgrounds for
high school students in particular? Playgrounds are actually starting to become
a lot more of a focus in schools now more than ever. And for us, I guess we
would articulate a lot of that to, I guess, when we're looking at Australia
specifically and the Eastern seaboard, the land costs are going up. So we
actually need to be a lot more inventive with play spaces and social spaces than
what we had before. So in other words, some of those spaces are becoming a
little bit more constrained for fiscal reasons, which basically means that we
need to, I guess, up the investment a bit with those spaces. So in short,
Hilary, I would say that the design is becoming a lot more of a focus with most
schools. Well, and I guess, you know, as we mentioned before, a lot of schools
have inherited their spaces and it costs a lot and takes a lot of space
sometimes to change them. Daniel, what are some of the ways those challenges can
be overcome if schools want to make a difference for their students? I think a
lot of that comes down to, I guess, with the conversation with Gwyneth, I really
do think is on point. We need to think about a wider array of diversity for our
learners, our students, not just in terms of gender needs, but neurodiverse
learners, various personality types, introvert, extrovert. I think with the
person that sent through that text saying he didn't like sport as a kid, but he
really was interested in performance. And often we've just ignored some of those
other ways of becoming active. And look, the research will demonstrate that if
we get kids active through the course of the day, get blood flow moving through
the bodies or oxygenating the body, the performance of kids in learning spaces
will actually increase in that regard. So there's a lot more benefit than just
play and social aspects. We will have a tangible link to increasing learning.
Now, we can do that in a multitude of different ways. And a lot of the things
that Gwyneth is talking about in terms of that choice, it is very helpful. I
like to think about a simple concept which was done through some of the Frank
Lloyd Wright designs, which are spaces within spaces. We come into a larger open
space, but we might sit in a particular setting. And that makes me feel a little
bit more, I guess, concurrent, but connected at the same time, if that makes any
sense. Yeah, the conversation pit idea. Yeah, exactly. It's like those sorts of
things. And then if you start to think about the settings of how do we seek
people together, a lot of play space.