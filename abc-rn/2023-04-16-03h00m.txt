TRANSCRIPTION: ABC-RN_AUDIO/2023-04-16-03H00M.MP3
--------------------------------------------------------------------------------
 Good morning, Kerry Worthington with ABC News. Fierce clashes have broken out
between rival military factions in Sudan. The 100,000 strong paramilitary rapid
support forces say they've taken control of the presidential palace and at least
three airports. The army says there'll be no dialogue until the RSF dissolves,
while the air force has carried out airstrikes on paramilitary bases. This woman
in the capital Khartoum says military leaders are vying for control of the
country. Al-Burhan and Hamedi are fighting over power and we are fighting for
food and drink. They are fighting over authority and looting of the country and
the people. But we are fighting for food, drink, education and health services,
which they haven't provided for us. So why are they fighting? Germany has shut
down the last three nuclear power plants in the country. The closures brought
cheers from anti-nuclear campaigners who held decades of protests against
nuclear power in Germany. Meanwhile in Finland, Europe's largest nuclear
reactors began regular output. Officials say it'll boost energy security in a
region hit hard by cuts to Russian gas. The new reactors expected to produce
power for at least 60 years. Coalition Senator Jacinta Namachempa Price says she
assumes allegations of child sexual abuse in Alice Springs, made by opposition
leader Peter Dutton, have been reported. During a visit to the region last week,
Mr Dutton repeated claims that sexual abuse of Indigenous children is rampant
and that children are being returned to their abusers. The peak body for
Aboriginal and Islander children have refuted the claims, accusing Mr Dutton of
using the issue as a political football. Senator Price was asked whether she
passed any of the allegations of abuse onto police. If that's what the territory
families know about them, I'm assuming that they have reported that. I know
foster parents have. They've done the due diligence and they have done the
reporting. Rangers are still searching for a dingo in WA's Pilbara region after
it attacked a toddler on Friday night. The two-year-old boy was staying with
family at the Dales Campground in Karajini National Park when the attack
occurred. He was taken to Tom Price Hospital with injuries but has since been
discharged. Rangers intend to euthanise the dingo if it's found. The Victorian
Government has come under fire from medical groups for planned funding cuts to
community health services in next month's state budget. The Victorian Health
Care Association says the state's health department contacted many community
health services to inform them of looming funding cuts to their health promotion
programs. The HA's acting CEO Juan Paulo Legaspi says it's understood millions
of dollars will be cut from the budget. We should be looking at things that keep
people at the centre of health care. These types of programs also help keep
people out of hospital. Our hospital systems are stretched right now, not only
because of increased demand to the health system but because we've got a
workforce shortage as well. Victoria's Premier Daniel Andrews has defended the
budget cuts. These are decisions that were made not in this upcoming budget but
I made a couple of budgets ago and there was a whole lot of services that were
basically double ups. We've got local public health units in place now. We've
got all manner of different programs that have been delivered. Disability
advocates say people are being stranded for hours waiting for wheelchair taxis
in Queensland. Sarah Richards reports. Amy Tobin, who has a disability, used to
regularly catch wheelchair accessible taxis but was always waiting up to four
hours for it to arrive. I don't really want to go out because I don't know when
or if I'm going to get picked up. Last year, Taxi Council of Queensland CEO
Blair Davies blamed COVID for the driver shortages but 12 months later the
situation is even worse. Until we can get all of our cabs out on the road more
often we're going to struggle to provide the service that we want. The State
Transport Department has held a round table with the taxi industry and the
disability community. It says it's reviewing several options to try and improve
the wheelchair taxi services. To sport, round seven of the NRL season wraps up
this afternoon with the Canberra Raiders hosting the St George Illawarra Dragons
and in the final game of the weekend the Parramatta Eels host the Bulldogs at
Western Sydney Stadium. And the AFL's inaugural gather round wraps up in South
Australia today with three matches to be held across Adelaide and Norwood ovals.
The Geelong Cats will be looking to build on their first win of the season when
they take on the West Coast Eagles. There will also be matches between the GWS
Giants and Hawthorne and Collingwood will take on the undefeated St Kilda.
That's the latest ABC News. For news updates you can download the ABC Listen
app. Think bigger. RN. This is the music show. I'm Robbie Buck. I wanted to
write. It's extraordinary how people wouldn't accept that you wanted to write.
That's the voice of Margaret Sutherland and you're listening to the music of
Margaret Sutherland too. This is the final movement of her concerto for strings.
Sutherland was one of Australia's great 20th century composers and the
importance of her role in the shaping of Australian art music is being further
illuminated all the time. Not least in a new biography by Gillian Graham. In the
next hour we'll talk to Gillian and hear from Margaret herself via the ABC
Archives along with much more of her music. I would personally say Settings.
There is Margaret Sutherland and the third movement of her concerto for strings
performed by the Queensland Symphony Orchestra and conducted by Patrick Thomas.
And we're joined on the music show by Gillian Graham, the author of In a Song, a
biography of Margaret Sutherland. Hello, Gillian. Hello, Robbie. It's an honour
to be here and particularly since Margaret had so much to do with the ABC.
That's right and an interesting relationship that we'll get to. But let's put
Margaret in context in Australian music. Well, she was a pioneer for Australian
music really and particularly in recognising developments that were happening in
Europe and the USA and the UK and being aware that they were happening but
knowing that they weren't reaching Australia. She wanted to bring modernism to
Australian music, I mean modernism in the broadest terms. She wanted to see
music progress in a particular way in Australia but informed by what was going
on overseas. So therefore some of her music was seen as quite avant-garde in
Australia to the fairly conservative audiences of the time. She felt when she
first came back from overseas, her overseas trip in the early 20s that people
didn't really know what she was about, what she was getting at. And that was one
of her frustrations really. But yes, as a pioneer for modernist music in
Australia and also just a real fighter for quality music to be disseminated in
this country. And yet for such a major figure in our musical history, so much of
her work has been lost. That's exactly right. I mean much of it was not
published, much of it was not performed. And I think there were several reasons
for that. One being that she was a woman and she was I guess surprised to
discover that she had difficulty as a female composer because her family
background had not led her to believe that she would. And there was also just
the fact that it was hard for any Australian composer really at the time to have
their music recognised. And so she used to write music that she could have her
friends perform essentially. And that was one way of getting her music out
there. And she was very good at ensuring as much as she possibly could that her
music would be heard. So later on with her orchestral music getting it played by
orchestras was a challenge. And she had many feisty interchanges with the ABC
about that. Who were her contemporaries here in Australia? Well her
contemporaries I guess, many of them went overseas. But there was Peggy
Glanville Hicks and Esther Rove, Phyllis Batchelor. She actually didn't have
many contemporaries, there were a couple. She really stuck out I guess in her
desire to be a very serious composer and not really just a hobbyist. And to have
audiences exposed to it because she really felt that audiences were ready to
experience this music. She was very lucky in the family that she was born into.
It was described as one of the most creative, intellectual, liberal families in
Australian history. How did that early experience shape her? It shaped her
hugely. She was born in Adelaide and when the family moved from Adelaide to
Melbourne when she was about four, she then got immersed in her father's side of
the family. And there was this house, a large house in Kew where three of her
aunts and two of her uncles, all single, lived together. And her parents bought
a house quite nearby. And so she was often in the house of her aunts and uncles.
There was a beautiful grand piano there that she remarked on when she first saw
the house. And she was taught piano by her aunt Julia, who was a great influence
on her, taught singing by her aunt Jessie. She learned to appreciate nature,
particularly through her uncle William. And she was just always over there and
they would have these discussions around the dinner table, around about
intellectual subjects. And she was encouraged to contribute, as were the women
in the family. And her father was a journalist and he went to work later in the
day. So when all her sisters and brothers had gone off to school, she was at
home with her dad and he paid a lot of attention to her, which was not always
necessarily the case in those days. And he'd play the piano for her and she'd
become involved in his little inventing projects and building cubby houses and
things like that. So he was a great influence on her as well as the extended
family. But this ideal childhood wouldn't last, would it? Her early life was
rocked by two major events, tragic events. Indeed, yes. The first, I guess, is
the death of her father and all her uncles. They were all around the age of 50,
all died of heart disease. And therefore the women in the family had to find
ways to earn money. And Margaret, of course, did so through teaching and
performing. But neither of those was her passion. And her mother died rather
suddenly too of a kidney ailment. And this was just before she planned to go
overseas. I just had to save up all the time, because in those days you didn't
get very much for teaching. And I had eight years of that before I went abroad.
Margaret's compositional training wasn't the most traditional, was it? She went
to a kind of bohemian alternative conservatorium. Tell us the story here. Yes.
There were two conservatoriums in Melbourne at the time. There was Albert Street
and then there was the University Conservatorium. And her aunt encouraged her to
go to the Albert Street Conservatorium because she felt that it was, well, as
you say, a bit more bohemian, a bit more progressive. There was, I guess, more
freedom of thought there. And her aunt had said to her, don't go to the
University Conservatorium. It's full of people from Britain who are more
interested in exams than creativity. And I think that, of course, that's changed
now. Of course. But that was an attitude that would remain with her throughout
parts of her career, a sort of a hesitancy about trusting some of these
institutions. Yes, a hesitancy about trusting probably any institution really,
or indeed any particular school of thought or style within music. I didn't like
conservatoriums. I didn't like places like that because they just confused
everybody. But I think with this superb teacher, you get away from that. She
liked to think that she was independent and she wasn't even terribly happy to
acknowledge that she was influenced by other composers, which of course she was
because it's very difficult not to be in some way or other. So she did actually
end up at the University Conservatorium. But yes, I guess that's another story.
But when she went overseas, she was determined not to go to the Royal College of
Music, as did many of her contemporary Australian musicians, composers, would
tend to want to go to the Royal College. But not Margaret. She sought her own
composing teacher and she was very fussy. Well, let's have a listen to Margaret
playing the piano in one of her own six profiles for piano back in 1948. Should
if What kind of pianist was she? She was a very fine pianist. She probably could
have made a career as a pianist, but that was not her desire, as I said. But she
did a lot of performing. She went on many tours for the Conservatorium, country
tours. She performed a lot in Melbourne. She played concertos. She was a very
good pianist, but I think never entirely comfortable in that, perhaps in the
limelight in that way that one needs to be when one's a performer. Did the idea
of being a concert pianist attract you at all? No, not at all. I was only
getting some money to go abroad with. You see, I was a concert pianist as well
as being a person who accompanied. And that was all that happened. A lot of
people think that the life of a concert pianist is the fact that people come to
hear you play, that this must be rather marvelous and that you enjoy being a
performer. But you felt that this was only a means to an end. Yes, very much so.
Gillian, tell us about Margaret's trip overseas. She left Australia on the SS
Austerly on February 19th of that year and she went unchaperoned. This would
have been a major adventure for her. Yes, unchaperoned. That was interesting
because it was not common, but I'm not sure who actually would have chaperoned
her, not her mother, because her mother had just died. So yes, off she went. She
just, as I said before, she just knew that there were things happening overseas
and she needed to find out about them. She didn't have a lot of money. She had
worked very hard to save money to be able to go overseas and it was enabled by
Edward Goll, who was her piano teacher at the conservatory, who went overseas
for the year before and allowed her to take over his teaching on full fees,
which I think says something about his respect for her playing. So she managed
to get overseas and she had letters of introduction to various well-known
composers, Vaughan Williams, Gustav Holtz, John Ireland, but she was not really
terribly impressed with any of them. She thought Vaughan Williams was a nice old
chap, but she didn't really see him as her teacher. She had heard, she's
obviously listened to a lot of music and she heard some music by Arnold Bachs.
When I went to London, I went to several people to get some kind of advice, but
there were certain people who couldn't give me any at all until I got to Arnold
Bachs and I found he was the absolutely ideal person because he wasn't a
teacher. He wasn't known as a teacher, but he told me a tremendous lot about
what you can do. She also liked him as a person because of his humility. I liked
him as a man very much and I found that he had that very genuine quality that I
admired. So she approached him and he said, oh well, I really am not a very good
teacher, but she just kept persisting and I think in the end he was curious
about this young woman from the antipodes who clearly wanted to work with him.
I'd heard a lot of Arnold Bachs in concerts, you know, but I could never meet
him because he always disappeared down the back door just when anything had been
done, you see, and this was very frustrating. So eventually I wrote a letter
saying that I'd come from Australia and, you know, is it possible for me to have
any lessons? And then he rang me up and then that was how we got into contact.
He was so surprised that I'd come, he said, I don't know anything about
teaching. And I said, well, it's not so much teaching, it's a sort of discussion
and a sort of discipline that I need. And he said, oh, well, I can do that. And
what I can do also is to give you all sorts of music to study and some of mine
and some of other people and so on. We can discuss them. And I said, well,
that's exactly what I wanted. I couldn't be more to the point. He had had
wonderful scores all around his studio and I was allowed to go do anything and
have a look at them and absorb them. And they worked very well together because,
as she said, he didn't pontificate in the normal way. And he obviously liked
Australia because he had a stuffed cockatoo in his house. Yes, exactly. There's
something in that. Who knows why he had a stuffed cockatoo in his house? But
obviously, Margaret noticed it as soon as she entered. But I don't think they
ever actually discussed it. So I'm not sure if she ever found out why he had a
stuffed cockatoo. But perhaps, you know, that was part of his curiosity about
Margaret. At this time, she also began conducting lessons and a growing interest
in what the orchestra could do. She did do a little bit of conducting later on,
but never a lot of it. And I think her first experience of conducting was she
talked about the wretched violas or something being told to play a little bit
behind the beat so that she would have to, you know, bring them into the right
place, as it were. But that was quite a brief experience. But certainly, it
would have contributed to her desire to eventually write orchestral music, which
she felt she had to do. She spent time in Europe, though, and as part of that
experience, she would have been coming up in front of a lot of contemporary
music, a lot of modernist music, too. Yes, a lot of modernist music, British
music, of course, because she was mostly based in the UK. But she also went to
France and to Austria. And I think it was really French music that impressed
her. Ravel and Debussy, etc., and the French Cis of Montparnasse. And although
she respected Richard Strauss and saw him conduct, it wasn't German music that
attracted her. And she felt that, in a way, that the Germans were not doing
anything particularly inspiring in music, which is quite a surprising comment,
really, because there were huge developments there. But they weren't
developments that Margaret necessarily respected. I think she was much more
interested in the kind of so-called impressionism of the French composers, and
they definitely had an influence on her, as did Shostakovich and Prokofiev and
Bartok, I think, in particular, in various ways. And how did those experiences
fold into her later compositions? Well, when she got back to Australia, she only
could afford to spend two years overseas. She wasn't quite ready to come home,
but in other ways, she really wanted to come. She always wanted to be in
Australia and always wanted to work in Australia because she wanted to see
Australian music develop this national idiom. And she was shocked when she got
back. Her thinking had been altered while she was overseas, and she had been
exposed to so much new music. And she found that audiences in Australia were
still quite conventional. And she had this concert organised for her in the
assembly hall in Collins Street by her friends, and several of her compositions
were performed. But particularly interesting was the Sonata for violin and
piano, which she had composed while she was in Britain studying with Arnold
Bachs. And Bachs was quite impressed by this. He was impressed by that piece of
music. In fact, he said it was the best piece of music by a woman that he'd ever
heard. And I think nowadays, to us, that sounds extremely patronising. But I
think for the times, well, it reflected the times really. And Margaret respected
Bach so much that there's no indication that she took that as anything other
than a compliment. And in fact, Bachs supported her a lot with this music,
particularly this piece, this Sonata. He wrote to various publishing houses,
including Chester, saying, you know, you should really publish this music, it's
good. So the reviews for that concert were actually quite good. So I think it
was just a feeling that Margaret absorbed from people in the audience that, you
know, this was avant-garde music and Australian audiences were not ready for it.
But... That's a little of Margaret Sutherland's Sonata for Violin and Piano, one
of the first pieces of hers performed back in Australia after her travels to
Europe. Gillian Graham, how did her career progress after she returned? Well,
she had hoped to make a career, I think, as a composer when she came back, but
she discovered it was really much harder than she thought. And she was very
dismayed. I think she used the words cold and dismayed. She felt cold and
dismayed about what she found. And this is partly because of her trip overseas,
but I think also her family background that had led her to, I guess, expect she
could do what she wanted. They had always encouraged her musical endeavours. So
she was a bit shocked, and I think particularly shocked by the fact that she was
discriminated against as a woman. She said at one point, it's as if they think a
woman can't be creative, which of course is exactly what they did think in the
past. But she just kept going. She just kept fighting. And I think there were
bouts of depression in her life, which her mother had experienced and also her
sister. So it was in the family, but also of course because of all the various
losses that she experienced in her life. But she kept going and she got married
to Norman Alberston, who she had met earlier before her trip overseas on a
country tour to Port Ferry. And he had given her a lift, I think, from Port
Ferry to her next destination. And he was quite taken with her. And he had just
separated from his first wife. And in the end, he actually pursued her to
England. So he was there for a while while she was there. He wanted to pursue
his own musical interests, which didn't really work out for him. He ended up
studying to be a psychiatrist. And when he came back to Australia, of course,
there were not too many psychiatrists around. There was a group of them, but
they were in the minority. And his study of psychiatry, I think, came out in his
treatment of his wife sometimes. But it was just not a happy marriage from the
start, which of course made composition all the more difficult for her. Well,
let's talk about her philosophy of composition and her philosophy of being a
composer, in fact. We can hear from Margaret herself first. Most people take the
wrong point of view of a person who writes music or plays music, which is a
completely different thing. You're thinking in a different way. Because you
know, a musician is akin to a mathematician. There is a certain gift in
mathematics, and there's a gift in the writing of music. But it's the sort of
thing that you really can't be given. You can't go to a person and be told how
to write. I go to the piano occasionally where there are certain knotty things.
But by and large, it's not worked out at all. It's just there. It might be in
scraps. It mightn't be right at the beginning. The central feature might be in
the center. It comes eventually. You can't say, now by tomorrow I'm going to get
here, you know? You've just got to take when it comes, if you give it time.
You're listening to the Music Show, and my guest today is Gillian Graham, whose
book In A Song presents a new biography of Margaret Sutherland. Gillian,
Margaret was considered part of the avant-garde, but did she actually see
herself as part of a modernist movement? I don't think she did see herself
because she didn't really like to think that she reflected any particular
school. And I think that also, it reflects her whole thinking in a way, because
even with Australian music she always felt that Australian composers should
absorb everything around them, you know, Birdsong or Didgeridoo, whatever, and
then just forget about them and let them come out, I guess, naturally in music.
And I think that was the way she thought about composition was that she talked
about this kind of inner song, which is where the title of the book comes from,
from a talk that she gave in 1956 to the Catalysts, who were a group of
intelligent, intellectual women, a small group of women who got together in
Melbourne. They still exist, this group, and they give talks and have dinner and
talk about things, intellectual and creative, and she gave a talk to them. And
in this talk she really expressed about this chant antérieux, which is kind of
these fragments of music going through one's head all the time until they could
actually be written down. But yes, as to belonging to any particular school, she
didn't like to be labelled, although as her music went along I think many people
have referred to it as being neo-classical, which I think is fair in many cases.
You mentioned her interest in an Australian idiom, and at the time that was
probably a very contested idea or an idea that was being challenged, but from
her point of view she preferred the much more subtle, nuanced approach to
reflecting the Australian landscape and the Australian sounds. Yes, she did
indeed. She hated imitative music, in other words, you know, music where it was
very obvious that a sparrow was chirping or a magpie was singing or something
like that. She really thought that it should come from the inside, I suppose,
and that wasn't the way to get at Australian music. It's very difficult to
define that Australian idiom, but I think what she was really talking about was
that composers shouldn't just automatically follow what was happening in Europe.
They needed to know about it and listen to it and then let it come out in a way
that it sort of will naturally do if one is in a different environment, which of
course Australia was and is. Well later in Margaret's career she would deliver
Haunted Hills in 1950, which probably drew on her wonder for the Victorian
landscape, the Dandenong Ranges. How was that received when it was first
performed by the Victorian Symphony Orchestra? It was actually received
reasonably well. I mean this was a very, I keep using that word avant-garde, for
the time it was I guess shockingly dissident in a way, but it still sounds very
contemporary. So you can imagine when it was first heard people would be sort of
possibly blocking their ears or some people would be. But then again it's very
evocative and being evocative was interesting because Margaret, as I said, she
actually didn't like to necessarily give descriptive titles to works, but she
felt that this was an occasion when she could actually do that. And it's really
a tone poem I guess you would say, but she said that it was influenced by the
underlying urge or feeling that was born of the sheer physical age of these
hills. And it was the Dandenongs where she sketched it, but I learnt not so long
ago that there is actually a mountain area near Mohe in Gippsland in Victoria
that is actually called the Haunted Hills. So it's kind of a combination I
suppose. And you can hear it in the music, once you've had that description, her
profound appreciation for Australia's natural environment. But also she said
that it was written in contemplation of the first people who roamed the hills
and their bewilderment and their betrayal. And that of course was Australian
Indigenous people. And she was really ahead of her time for her sympathy for the
plight of Australian Indigenous people. So the frenzied dance that occurs in
this music she said was born of despair. And the jazz that needed more words —
at this point actually — was because that wasn't Margaret Sutherland's Haunted
Hills performed by the Melbourne Symphony Orchestra conducted by Patrick Thomas.
Gillian Graham is with me today on the music show and Gillian's the author of
Inner Song, a biography of Margaret Sutherland. It's obviously one of the larger
pieces of Margaret's but much of what she wrote was at a smaller scale. She was
more interested in the chamber composition for much of her career. Take us
through some of that evolution. Several reasons why she preferred chamber music
or she said she preferred chamber music. Probably the one that comes first to
mind is the ability to get a piece performed and Margaret knew many musicians,
she was entrenched in the musical world of Melbourne and therefore had a lot of
friends and a lot of supporters who were musicians who would play her music. So
that was one reason. Another reason was that she did attach a kind of feminine
sensibility to chamber music and I think that's partly because women at that
point were more confined to the domestic sphere and thus if they wanted to
compose music that is the sphere in which they did it. But she felt that
symphonic music was a male thing. She sort of said you know all men want to
compose this symphonic business she referred to it as. Now I think that partly
came from those things I've just described but also partly because she did
struggle a little with orchestration. I mean I guess as many composers do, they
need to learn. She did have some assistance with it on various occasions. I
think she nailed it in the end actually. But the other reason why she didn't
compose much orchestral music while her children were growing up and while she
was married was that she simply didn't have the time or the brain space. But
later on when she had time to really sit down and think about it after the
marriage had ended and she was feeling calmer and happier in herself she could
focus more on it which she did and I think she also felt that pressure that if
you wanted your music to enter into the musical canon well you needed to tackle
the larger forces. Well as you said she nailed it in the end. Let's listen to a
bit of her concerto grosso. Let's listen to her concerto grosso. Dr. Margaret
Sutherland. She had an interesting relationship with the organisation that we're
chatting on today, the Australian Broadcasting, well the Commission back in
those days. It was a complicated relationship with the ABC indeed but well I
should say first that without the ABC Margaret's life as a composer would have
been very different and she acknowledged that it was enormously supportive in
the 1930s when it started up. It started some composer competitions for
Australian composers and these really helped Margaret to get her music out there
a bit and of course other composers as well. She was always very grateful for
that. As time went on she wasn't always happy with the way the ABC represented
Australian music and she felt that it was often, you know they started these
programs of Australian music and she was never in favour of having a whole lot
of Australian works in one program or in one concert all after the other because
she felt that they were just treating it as something to be got over as she put
it. She was always more in favour of integrating it with other works and letting
it stand on its own or in comparison with music from other places. So she had a
series of feisty interactions where she expressed her opinion and it's
interesting because she didn't always wear her heart on her sleeve. I mean I
think part of the title of the book comes not only from that talk that I
mentioned before but also because she had an inner nature I guess. She was not
particularly demonstrative but when she was angry she could be reasonably
demonstrative and that came out in her interactions with the ABC often about
programming, often about getting her pieces performed, often about getting other
pieces by other Australian composers performed. But Margaret had a very definite
idea of what quality was in classical music and she felt that they often just
sort of threw in a piece of music because it fissured the time slot or whatever
without really considering whether it represented Australian composition. The
first of her commissions was in 1966 and it came from APRA, the Australian
Performing Rights Association, that was then chaired by Robert Hughes who was a
composer and he apparently refused to end one particular meeting until they had
decided to commission Margaret so he was obviously a champion of her work. That
work was a string quartet number three but in the same year she received a
second commission from the Australian Musicians Overseas Scholarship Fund which
was a scholarship founded by pianists Ada Corder and Nancy Weir, people may know
those names, and they commissioned piano works by Australian composers and
enabled Australian pianists to study abroad. This piece was extension and it
really demonstrates Margaret's desire to show her mastery of contemporary
methods and it's probably the closest to serialism that she ever got or ever
wanted to get. Margaret Sutherland's extension for solo piano performed by Sully
Mays. The other big influence that Margaret has left us with is her role in the
arts scene, particularly in Victoria and Melbourne, it's now called the Arts
Centre in Melbourne, but she was a huge proponent to establish an arts centre on
a space where many people were pushing for a business park. They were indeed,
she was deeply shocked by that. It was at a time when a new space was being
sought for the National Gallery and the Wirths Park site, which is obviously the
site where Arts Centre Melbourne now is, had been found and identified as being
an ideal position for the National Gallery, but it was in danger, very much in
danger of being given over to private enterprise and Margaret said, hang on a
minute, we do not only need a new National Gallery, this city is ready for a
cultural centre. This was actually during the Second World War when the
government, I guess, was starting to think about post-war cultural development
and so Margaret saw that as an opportunity and she thought that the time for a
cultural renaissance in Melbourne had come. So she and her good friend Lorna
Stirling, who was a violinist and musicologist, got together and thought about
this a bit and Margaret said, well, we need to see who else is interested and
she sent a letter to the paper and only got one response from a man named John
Lloyd who worked for the Titles Office, but he was very, very interested in the
arts. And Margaret thought, well, that's excellent because we probably need a
man involved. She wasn't silly, you know, at that point a man might give us a
bit more credibility. So the three of them met in her music room in Kew and
hatched up this plan. It was called the Combined Arts Centre Movement, the CACM.
The first step was to get a whole lot of arts organisations together and fired
up to distribute a petition which eventually got 40,000 signatures, which wasn't
bad for a city of only about a million people at that time. Presented to
government, it's a long story, it took a long time, but Margaret was there from
the beginning and kept fighting by sending letters to the press. She was there
for the opening too, just a couple of years before her death. Yes, 1982, that's
when the concert hall opened, now Hamer Hall, and there were many celebrations
for the opening of Hamer Hall and they went on all day. There was a procession,
there were all sorts of things, but in the evening there was a concert. Good
evening from the new Melbourne Concert Hall. Well, here inside the hall where
the atmosphere shows mounting excitement, the last of the invited guests are
taking their places. And I must say the audience looks brilliant tonight. I
haven't seen such dressing for a concert occasion in years, certainly not in
Melbourne. Australian Premier, or then Victorian Premier Sir Rupert Hamer gave a
speech and he acknowledged Margaret's presence at the concert and very briefly
her importance to Australian music. From the beginning the citizens have been
involved, led, cajoled and prodded originally by that great Australian musician,
Margaret Sutherland, and we're very delighted to see her with us tonight. But
none of her music was performed in that concert and no music in fact by any
Australian composer was performed, which shocks us nowadays. There were some
repercussions in the press because of that. What Margaret's response was, I
don't know, because she was frail and blind by that point. Well we're going to
finish with a song from Margaret's Six Songs, set to text by the poet Judith
Wright. Gillian, how did Margaret and Judith come to find each other? Margaret
didn't know Judith Wright terribly well in fact, but obviously appreciated her
poetry. They did exchange a few letters because Margaret wanted to set her poems
to music and they were kindred spirits with their appreciation of nature and
feminine themes of procreation and motherhood and childhood. Margaret thought
that Judith Wright's poetry was meaningful and musical. Obviously of course as a
composer she was looking for poetry that was suitable for setting to music, but
it's interesting that Judith Wright wasn't particularly musical herself, or she
said she wasn't, but she did say that verse should be close enough to music for
them to meet somewhere and of course they did meet. And that is a poignant place
for us to finish because when Margaret died on the 12th of August 1984, by pure
coincidence on the very same day mezzo-soprano Elizabeth Campbell and pianist
Anthony Fogg performed Margaret's Six Songs as part of a program called An
Anthology of Australian Song. In a Song, a biography of Margaret Sutherland by
Gillian Graham is published by Melbourne University Press. And thank you Gillian
for being my guest on today's show. Thank you. The music show is produced by C.
Benedict and Kezia Yap. Musical production by Simon Brathwaite and Roy Huberman
on Gadigal Country. Next time on the music show, live performances from Ty Coz
and you'll hear from Australian composer Lisa Illion. Let's go now to the stage
of the Sydney Opera House on the 12th of August 1984 for that performance of
Woman's Song from Margaret Sutherland's Six Songs. Oh move in me my darling, for
now the sun must rise, The sun that will dawn upon the lids upon your eyes.
Awake in me my darling, the knife of day is griped, To cut the thread that binds
you within the flesh of night. Today I lose and find you whom yet my blood would
keep, Would weave and sing around you the spells and songs of sleep. None but I
shall know you as none but I have known, That there's a death and a maiden who
wait for your love. So move in me my darling, most dead I cannot pay, Now the
dark must claim you and passion on the day. The wait is over. The new season of
Short and Curly has arrived. The perfect podcast to get children and adults
talking about life's biggest questions. Like is it okay to kill insects that
annoy us? Is it rude to stare? And should children be allowed to swear? I would
never swear in front of my parents but I do at school. That's Short and Curly.
Hear it now on the ABC Listen app.