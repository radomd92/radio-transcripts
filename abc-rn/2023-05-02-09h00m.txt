TRANSCRIPTION: ABC-RN_AUDIO/2023-05-02-09H00M.MP3
--------------------------------------------------------------------------------
 My pleasure. It's coming up to five o'clock here on ABCRN. The biggest
intergenerational crime, according to Mark, was our fresh water being sold as a
commodity. According to Jenny in Hobart, she says the biggest intergenerational
crime is sterilising members of the next generation in the name of gender
ideology. And plenty of you, like Ruth in Adelaide, saying it really is climate
destruction. There is no more carbon budget left and our current trajectory
risks the collapse of civilisation. Thanks for all of your texts on what is the
biggest intergenerational crime. We'll get to some more of those after the news
and we'll also find out what the departure of Alan Joyce from Qantas will do to
the aviation business's protracted industrial issues. This time, five o'clock.
Hello ABC News with Rachel Hader. The Treasurer says the Reserve Bank's decision
to increase interest rates makes clear the pressures on the federal budget.
Political reporter Nicole Hegarty has more. Jim Chalmers says today's quarter of
a percentage point rate rise, the 11th in a year, is a brutal reminder of the
difficult economic conditions confronting the government as it finalises the
budget. He says it will contain some relief. Responsible cost of living relief
that doesn't add to inflation. While Shadow Treasurer Angus Taylor says a
broader plan is needed. We haven't seen a clear plan from the government to deal
with the inflationary pressures. The government won't be drawn on the specifics
of its budget cost of living relief package, saying that detail will be revealed
next Tuesday. The Australian Council of Social Services has urged the federal
government to increase job seeker payments in next week's budget. There's
speculation the payment could be increased for people aged over 55, but the
government is refusing to confirm that. The council says young people are
particularly vulnerable and there should be a substantial rise for everyone to
help combat inflation. Treasurer Jim Chalmers says the government is aware many
Australians are struggling with cost of living pressures. The reason why I've
cautioned you against assuming what's in or not in the budget is because we
should look at what the government announces next Tuesday night in its entirety.
The peak body for GPs has welcomed the federal government's crackdown on vaping,
saying it will help stamp out a new generation of smokers. The Health Minister
Mark Butler says some flavoured e-cigarettes will be banned and imports of non-
prescription vapes will be blocked. Under the new rules, vapes will only be sold
in pharmacies using plain packaging. President of the Royal Australian College
of GPs Dr Nicole Higgins says any measures to stop younger people vaping will
help avoid a future health crisis. What we need to do is stop the next
generation of smokers and that is why we must really clamp down on vaping now.
We know that vaping is toxic to people's lungs. Justice Department figures
revealed at the Yurok Justice Commission show how Victorian government bail
reforms introduced after the 2017 Bourke Street Mall attack led to a dramatic
rise in the number of Aboriginal people in prison. Last financial year, 150
Aboriginal people entered Victorian prisons under a sentence, while almost 1200
Aboriginal people entered prison on remand. 89 per cent of Aboriginal people who
were in prison were on remand. Yurok Commissioner Maggie Walter says the 2018
bail reforms were a monumental policy failure for First Nations people in
Victoria. And the impact is being felt daily. We've met all these people in
prison in recent times. We know what it's done to their lives, the lives of
their children. I'm just struck by the lack of urgency to do something about it.
The Transport Workers Union says it wants to work with the incoming Qantas CEO
to help rebuild trust in the airline. Vanessa Hudson will take over as Qantas
chief executive when Alan Joyce retires in November after 15 years in the top
job. Union National Secretary Michael Caine says working together will lead to a
better airline for workers and passengers. There is a real fear that she's cut
from the same managerial cloth as Alan Joyce. We hope that that fear is
unfounded and certainly we'll be taking this in the spirit that Vanessa Hudson
has just outlined there to the Australian public that she wants to rebuild
trust, that she wants to take a different approach. A computer scientist known
as the godfather of artificial intelligence has quit his job at Google, saying
part of him now regrets his life's work. Dr Geoffrey Hinton's pioneering
research on deep learning and neural networks paved the way for AI systems like
ChatGPT. But Dr Hinton is warning AI might soon become more intelligent than
humans. What we're seeing is things like GPT-4 eclipses a person in the amount
of general knowledge it has and eclipses them by a long way. And given the rate
of progress, we expect things to get better quite fast. So we need to worry
about that. You're listening to ABC News. Think bigger. Bigger. RN. Think
bigger. What does Alan Joyce's departure from Qantas mean for the airline and
for the industrial struggles that the industry's faced? Some reaction from the
union in just a minute. Welcome back to the Nation's Live conversation across
today's politics, current affairs and culture. You're with Andy Park on RN
Drive. PM with Samantha Donovan will be up in 25 minutes' time, Samuel, taking a
look at the possibility there'll be an increase to JobSeeker in the budget.
Spoiler alert, it's only for the over-55s. Yes, a pretty controversial
suggestion, Andy. That speculation came out last week or last night, I should
say, and the Treasurer Jim Chalmers isn't ruling it out. But is that fair when
we know people of all ages are struggling to make ends meet on JobSeeker? We'll
obviously have to wait until the budget next week, but our reporter Stephanie
Smale will have more on that story for us tonight. Yes, very relevant to our
discussion about intergenerational crimes. That's PM with Samantha Donovan
coming up at 5.30 here on Radio National. But before then, stay with me. I'm not
really into fashion. I know you might be. And I do know that who wore what to
the Met Gala and why tends to kind of dominate popular culture even though it
looks like a bit of a tropical bird display to me. However, I am intrigued as to
which well-known actor and musician came dressed as a giant white cat. You'll
find out shortly with the Australian newspaper's fashion editor. You're with
Andy Park and this is RN Drive. The line of succession has been confirmed for
Qantas, the airline confirming its first ever female chief executive officer.
Vanessa Hudson, who has been with Qantas for nearly 30 years and is currently
working as its chief financial officer, will take over from Alan Joyce when he
retires in November. She says the airline will invest heavily in improving
service. We have all been working very hard in the last six months and last
year. We were very upfront in terms of recognising that the customer experience
was not where we wanted it. I can absolutely say she's an outstanding executive
with outstanding leadership skills. One of the jobs they say as a CEO and
probably the final job that they have to do is make sure they have good internal
succession. Outgoing CEO Alan Joyce there who's spent 15 years in the top job at
Qantas. Well joining me is the National Secretary of the Transport Workers
Union, Michael Cain. Welcome to you. Good afternoon, Andy. What was your
reaction to the news of Alan Joyce's retirement at the end of the financial
year? I mean it was long rumoured really, it was just a case of when I suppose.
Yeah, well I mean without being flippant about it, I think we're a little
disappointed that we have to wait till November until Alan Joyce goes. I mean
this is a CEO that his position has become untenable. We've heard about the
service delays, we've heard about how these workers have been really affected
badly by the choices that he's made. So that was disappointing. Look we're
hopeful that the announcement of a new CEO will be a moment in time for us to
switch that around. Vanessa Hudson said some useful things today about
rebuilding trust and the like. We'll take that on face value but this is a big
job ahead for Qantas. It really has lost its way in recent years. You've spoken
about the dismantling of standards. I mean Qantas has seen during Alan Joyce's
tenure as boss. Apart from delays, which is certainly a big part of that, what
exactly do you mean? Well the spirit of Australia was built by workers, by a
workforce that was so proud to work for Qantas. In fact generations of families
would work for Qantas. The jobs were secure jobs, they were permanent, they were
well paid. And of course we all know that feeling when we get on a plane
overseas and we hear that Australian accent when we're coming home. It's a
source of pride to us all. But with respect back in those days there wasn't as
much competition in the skies was there? Well competition of course is intense
in aviation but the key thing that Alan Joyce has done is he has pushed these
workers away from the company. He splintered the workforce just so that he can
get a cost benefit, a short term cost benefit at that. And doing that over a
succession of years has meant that we now have rather than workers being
directly engaged by Qantas, engaged by literally dozens and dozens of different
companies which are in competition with each other and over which Qantas has
very powerful contracts and can really call the shots. So that's led to a
degradation of workers standards in aviation and just when we need to rebuild
Andy, just when we need to rebuild coming out of COVID, there are jobs in
aviation that people don't want to do. I mean Alan Joyce guided the company
through the COVID-19 pandemic and the company's posting profits which
shareholders would no doubt be happy about. Do you give him any credit for that?
Well Alan Joyce actually didn't guide the company through the pandemic. The
Australian community did. $2.7 billion worth of community funds went into
supporting and keeping Qantas afloat during the pandemic. And now what we're
seeing is a $1.3 billion half year profit being announced at the same time as
service standards are through the floor and airfares are through the roof and
workers are being hit at every opportunity. Just next week Andy, Qantas is
taking 1700 workers that it illegally sacked according to the Federal Court to
the High Court to try and overturn that decision. This is a CEO that has a lot
to answer for. He's a CEO that really the Australian community will be drawing a
sigh of relief is on his way out and we're hopeful that his replacement will
acknowledge that, take a good hard look and take the company in a different and
better direction. How hopeful and optimistic are you about a new direction
particularly when it comes to industry relations between your union and the
airline? I mean you know Vanessa Hudson has been there the entire time in the
senior leadership team. Do you see things at Qantas being any different really?
Well of course we have a fear about that because there is a fear that Vanessa
Hudson is cut from the same manager of cloth as Alan Joyce. She was the CFO, the
Chief Financial Officer supporting the decisions that Alan Joyce has made. So we
do fear that but we really do hope on behalf of workers in the Australian
community that that fear is unfounded. We do want to take Vanessa Hudson at face
value of her words today when she said she wants to rebuild trust with workers
and with passengers of Qantas and that's a good start. We'll take it at face
value. We'll take the hope that's embedded in those words and we'll put our
shoulder to the wheel to try and make a difference. It's 13 past five here on RN
Drive. You're hearing from the National Secretary of the Transport Workers Union
Michael Cain. We're talking about Qantas and their succession plan with the
retiring Alan Joyce to be replaced by the airline's Chief Financial Officer
Vanessa Hudson before the end of the year. You've also reiterated your calls for
a Safe and Secure Skies Commission. This is something that the TWU has been
pushing for for a few years now. How in your mind would this commission work?
Well first thing is that we do need an independent decision maker in aviation.
For too long we've had aviation decisions. Aviation is critical infrastructure.
We learned that once again in COVID. And for too long decisions have been made
in aviation with the profit motivations of airline and airport CEOs being in
charge. What we need is some community input into aviation decisions. This is
critical infrastructure. It affects workers and workforce. It affects
passengers. So we need an independent decision making body. We say call it the
Safe and Secure Skies Commission which would look at the problems in aviation.
What should be the workers standards? What should be the service standards that
should apply to regional areas? That should apply right across the board? And we
say to the government put that in place so that we can take some of the heat out
of this decision making and make sure that the decision making is done in the
community interest. And what is the Labor Federal Government telling you in
response about your request for this Safe and Secure Skies Commission? Well
Labor's been in power for a year. We've been putting this position up front.
There's a conference, an ALP conference in August. We'll be taking this forward
as a policy position in that conference and we'll be asking for the support of
ALP membership. And then we'll push it forward because I think we have to make a
difference. We have to put a line in the sand in aviation. We've got to rebuild
good jobs and until we do our service standards are not going to be where we
need them to be in this critical infrastructure. We'll have to leave it there.
National Secretary of the Transport Workers Union Michael Cain, thank you for
your time this afternoon. Thanks Andy. And we did approach Qantas for comments
seeking an interview with either Alan Joyce or Vanessa Hudson. However, Qantas
said they were unavailable for an interview this afternoon. Lots of you on the
text line weighing in on this. Uma says all airlines around the world have
experienced the same problems as Qantas. The militant unions made it hard to do
business. Alan made the difficult decisions or it was a mess when he took over.
This one too, look, basically saying they're glad that Alan Joyce is going. He's
stuffed up Qantas. Lost standards, lovely crews, screwed. You want quality,
you've got to pay for it. Glad he's on his way. Says JJ from the Blue Mountains.
Thanks for your text. 0418 226 576. A quarter past five. Attention passengers.
Ah, ah, thank you Simon. Hi, I'm Jonathan Green and if you caught the first two
statements, you're going to be Jonathan Green and if you caught the first two
seasons of Return Ticket, you may be wondering where on earth will we go for
season three? Return Ticket, we take journeys of the mind. No passport required.
Come fly with us. Return Ticket on the ABC Listen app. RN Drive with Andy Park
on ABC RN. When you look at some of the big names from Hollywood and the fashion
world at the annual Met Gala, it's safe to say that anything goes. With a lavish
display of pearls and camellias, the 400 guests at this year's event honoured
designer Karl Lagerfeld, who was at the helm of Chanel and Fendi before his
death back in 2019. Even Lagerfeld's favourite pet was honoured with two guests
dressed up as a cat. No Kim Kardashian wasn't one of them. Let's talk through
this opulent event. You're joined by the fashion writer and commentator at the
Australian, Glynis Traelnash. Welcome to you. Thank you. I'm no longer at the
Australian. I should point out. My apologies. Quite alright. OK, thank you for
the correction. Look, let's, before we get to this year's theme, Kim Kardashian
did not try to outdo last year's stunt when she wore Marilyn Monroe's famous
happy birthday dress. Are you relieved or disappointed? Look, I kind of am
relieved for all the curators around the world, certainly, trying to borrow
museum pieces going forward. But that was an interesting one for her because at
first glance you go, oh, the pearls, it's very Chanel. It's this, it was a
Schiaparelli outfit. And then you, the internet went alive with the fact that it
was actually something of an homage to a shoot that she did for Playboy.