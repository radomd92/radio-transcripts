TRANSCRIPTION: ABC-RN_AUDIO/2023-03-19-16H00M.MP3
--------------------------------------------------------------------------------
 ABC News with Chelsea Hetherington. Victorian Liberal MP Moira Deeming is set
to be expelled from the party over her involvement in an anti-transgender rally
attended by neo-Nazis at the weekend. She spoke at the Let Women Speak event
where a group of neo-Nazis repeatedly performed the Nazi salute, sparking
violent clashes outside Victorian Parliament. Opposition leader John Pizzuto
says he would move a motion to expel Ms Deeming, calling her position untenable.
On social media, Ms Deeming has said she was disappointed with Victoria Police
for letting masked men into the rally buffer zone. The Kremlin says Russian
President Vladimir Putin has made what it has called a spontaneous trip to the
devastated Ukrainian city of Mariupol, which Russia seized last year. The BBC's
James Landau reports from Kiev. The timing of this is crucial. On Friday the
International Criminal Court accused him of war crimes, the deportation of
children from occupied Ukraine to Russia. Throughout this conflict we have seen
a pattern of behaviour. Whenever Russia or Mr Putin suffers some kind of setback
there's always a response and that tends to be either military response,
diplomatic or one of propaganda. I think that's what we're seeing here in these
images. Essentially I think there's probably a message of business as usual. The
international community can say what it wishes but Mr Putin remains head of
state, his forces remain in charge of occupied parts of Ukraine. Police in the
Pakistani capital have filed charges against former Prime Minister Imran Khan
and a number of his aides and supporters, accusing them of terrorism and other
several offences. On Saturday Mr Khan's followers clashed with police outside a
court where the former Prime Minister was to face charges that he'd sold state
gifts he'd received while in office. Riot police fired tear gas while Mr Khan's
supporters threw firebombs and hurled rocks, with more than 50 officers injured.
Mr Khan has blamed police actions for his failure to appear in court. Serbia's
President Aleksandar Vučić says he's declined to sign a proposal by the European
Union aimed at normalising relations with Kosovo. Over the weekend the EU said
the leaders of the two sides had met and struck a deal, 15 years after Kosovo
declared independence. But Mr Vučić says Serbia remains opposed both to the
recognition of Kosovo and to it gaining full membership of the United Nations. A
disabled hip-hop artist hopes long overdue changes to the music industry will
occur following the Queensland government's declaration to make tourism
accessible in 2023. The state government pledged $12 million to upgrading
infrastructure and technology for visitors with disabilities in November last
year. Nathan Tessman, also known as MC Wheels, says a continued lack of access
may deter other artists from entering the music industry. He believes getting
input from people with disabilities will be a huge asset in developing spaces
where everyone can enjoy what the state has to offer. I think there's a lot of
artists out there that have disabilities that can't showcase what they want
because they can't access venues. I've had to be lifted up onto stage because
they didn't have a ramp. St Kilda have given coach Ross Lyon the perfect start
to his second stint in charge of the AFL club with a 10 goals 767 to 7 goals
1052 victory against Fremantle. Ben Cameron reports. Ross Lyon enacted revenge
on the club that sacked him three years ago as an injury-depleted Saints
defeated Fremantle. Missing nine of their best 23, St Kilda kicked four goals to
none in the final term to run away with victory. Top ten draft pick Mateus
Philippou kicking a goal on debut while amassing 16 disposals. In the earlier
game Essendon dismantled Hawthorne by 59 points while GWS came from 31 points
down to win by 16 over the Crows. Ben Cameron, ABC Sport. The Raiders have won
their first game of the NRL season holding on to beat the Sharks 24 points to
20. Canberra led by 14 points midway through the second half before conceding
two quick tries to set up a grandstand finale. Raiders forward Hudson Young told
ABC Sport it was good to get that first win. We did the same thing in the first
two games but that's all we need to make one win for our confidence. We weren't
far away in the first two games so kick start our season now and into the nights
next week. In yesterday's earlier game the Bulldogs hung on to beat the winless
Tigers 26 points to 22. And Australian Danielle Scott has won her first Crystal
Globe after being crowned as the world's number one ranked aerial skier. Scott
finished second behind fellow Australian Laura Peel in the final World Cup event
of the year in Almaty to seal the overall title. Scott said she was thrilled to
finally claim the top award. And you're up to date with the latest from ABC
News. RN on air, online, on your digital TV and on the ABC Listen app. This is
Duet on RN with Tamara Anna Chislovska. Today I'm joined by a renowned performer
and composer of The King of All Instruments. He was born in Cairo and raised in
Australia and he's become synonymous with an instrument as ancient as it is
beautiful, evocative and as he's proven time and time again incredibly
versatile. Joseph Tawadros, welcome to Duet. Thank you for having me. It's so
exciting to be here. Do you like that intro? What an intro. Beautiful.
Synonymous. I love that. Synonymous. I love being synonymous with things. I wish
people could see what you're wearing. Yeah, I'm not synonymous with anything
else except the UD. Because I'm audio, I'm sound to most people. So you know,
the people that know the UD and the UD in Australia, I guess I'm being lucky to
try and forward it in some way in this great land. You've said that the UD is
like your refuge, your comfort, your true friend. Yeah, well, because you know,
have you seen my friends? I'd rather go and play an instrument. That's how bad
it's become over the years. But no, look, I think it's very important. I mean,
there's a definite nurturing of the spirit. And I think that's something
important that we have to celebrate in a way and we've been very lucky as
musicians to have that option and to know the benefits of music nurturing the
soul and I think and hope and pray that, you know, that youngsters coming up,
see the importance of music and how good it is for them in all sorts of areas.
Permission to evaporate. Yes. Tell me about that. That's what we're going to
hear first. Well, Permission to Evaporate. It's very, I mean, the title sounds
quite interesting, but yeah, it's one of great depth actually. I wrote
Permission to Evaporate in a tough time when my parents had both passed away. My
mother passed away in 2012 and my father a year later. And so it was a tough
time and it's one of confusion and that is a perfect time where the Ud and music
were, you know, refuge and somewhere to take some sort of comfort. And that was
Permission to Evaporate by Joseph Tawadros and you also heard Christian McBride,
James Tawadros and Matt McMahon. And that was your bro, Joseph, wasn't it? Yeah,
that was James. James plays percussion. He plays all sorts of Middle Eastern
percussion. On that recording, he was playing the frame drum, which is called
the bendir. Lots of people in your family have played musical instruments. We
all dabble. We love music. My whole family have always loved music and, you
know, we're brought up on it. So yeah, it was only natural that, you know, other
players in the family played it. But James is actually a biomedical engineer. So
he's got a proper job apparently and he comes and plays, you know, frame drums
when he can. Your uncle played trumpet? He played trumpet. Well done Tamara.
This is amazing. Extensive. Oh man, I can't believe that you've gone into this.
This is kind of like ASIO. Yeah, he played trumpet and then I, yeah, I really
wanted to play the trumpet, but my family said, no, it's not a great idea. They
were anti-trumpet. And so I saw the oud and I really loved it. And they said,
okay, oud sounds all right. And it was a link to my heritage and I was very keen
on it. And I love it and I still love it now. Didn't your grandfather play as
well? Well, I never met him, but yeah, I've got photos of him playing and he
died in 1957. But I have photos from the 1930s with his band playing the oud.
And it's a really inspiring thing to see as a kid. And it was something that
really made me want to learn the instrument, learn more about the culture and
the history of the place where I was born. So I'm a big fan of traditional
music, traditional Arabic music. And that was the music that I was brought up
with. My mother used to record these radio shows when she was in Egypt to learn
the old repertoire. So I had this big library of cassette tapes, which I still
have. And I listened to and learned some of the old repertoire and that was my
journey with the instrument. So my parents were very big influences on it and I
had, you know, many traditional influences and that's what I was brought up on
and I still listen to today. There was a singer that was a particular influence
on you. Yeah, Umm Kulthum. I mean, there's really two, well, three massive
singers in the Middle East. One is Umm Kulthum, the biggest diva, you know, ever
known in the Middle East. She's just as popular now as she was back then. She
died in 1971. She had a very popular career in the 20s and 30s. But at the same
time, there was also Abdel Wahab, who was a very big composer and singer. And
the movie industry was really, it was the golden age of Egyptian cinema and all
these singers and composers became movie stars. And the third one was a little
bit more modern and introduced, you know, electric guitar and accordion. It was
more poppy. It was Abdel Halim. So between those three, you have most of Egypt's
popular music through that whole period. And so Umm Kulthum and Abdel Wahab were
rivals and in the 60s, they joined forces because of the president suggesting
it. And so then they created an amazing era of these songs where Abdel Wahab
composed for her. But she has a, the amazing thing about the golden age was that
they have the most amazing lyricists and composers and most of the music that's
played on the oud or on the Arabic instruments are from that period. What about
piano? Did piano enter your life at any point? Well, I love piano, actually.
Piano is a great instrument. And it's, I always find that, you know, the oud is
the king of all instruments in the Middle East, but you know, the piano is
really up there. And it's, it's, it's, the parallel is that, you know, piano is
the instrument of choice for composers and the oud is in the West and oud is the
instrument of, you know, choice for composers in the East. So they're very
similar in that sense, but they couldn't be further apart, you know, in the way
they are, you know, you've got black and white keys that can't play quarter
tones. And whereas the oud is a fretless fingerboard that, you know, you know,
shares a lot of quarter tone music and scales with quarter tone. So when playing
with piano players, you always try and find a happy medium between the both and
the right sort of intonation. So it's not kind of all over the shop. So I
shouldn't have tuned the piano? Well, I tuned it to quarter tones earlier. You
didn't realize. I just got in with my mallet. I haven't played it yet.
Rachmaninoff. Yeah. The Prelude in C sharp minor, the really famous one that
starts, do you want to play the beginning? I want to. I'm not a, I'm not a big,
you know. I'll give you a C sharp. So have you ever played that on piano? I've
never played it on piano. I just gave it a crack for the first time now on the
oud. But I find his pace and his soul, Rachmaninoff, it's such that passion,
which I think a lot of the Arabic music has. And I also am a big fan of, you
know, performers that are composers. And that's something that I always look up
to and see what they're doing and how they challenge themselves on the
instrument. Yeah? ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪ ♪♪
♪♪ ♪♪ ♪♪ ♪♪ Rachmaninoff's Prelude in C-sharp minor, Opus 3, Number 2, and it
was performed by Vladimir Ashkenazy. It was the choice of Joseph Tawadros, who's
with me on duet. Joseph, you met Vladimir, did you? Yeah, I met him, you know,
backstage in the BBC. We were both doing interviews. You know, I had to warm up
the stage, you know, that sort of thing. No, no, just kidding. No, he was a
lovely guy. Tell me about your interest in Khalil Gibran. Khalil Gibran, I was
really, I love his book, The Prophet, the one he wrote in 1923, very famous
book. And I received it as a kid and I hated it. I was like, what's this guy
going on about? But then, you know, you go through some things and then his
words kind of resonate with you a bit more. And I think that's the great thing
about him. His words do resonate with a lot of people and provide a lot of
comfort for people. He has wonderful quotes. One of them I remember about, you
know, if your heart's like a volcano, you can't expect flowers to grow. All
these beautiful little wisdoms. I've never heard that, but, you know, it's a
good point. Don't do that with flowers or volcanoes. And he had all these, you
know, they're quite philosophical, the idea of your thoughts being like leaves
that love to sway in different directions. Well, there you go. That reminds me
of you, Justin. Well, thank you very much. I'm just, I thought, you know, reason
and passion are the rudder and sails of your seafaring soul. We can do all that.
But I wrote three albums based on his words, one of which was The Hour of
Separation, which was, you know, his quote was, love knows not its own depth
until the hour of separation. And another album called The Prophet, which was
actually inspired by his words. And the third album, Concerto of the Greater
Sea, which was kind of, you know, more words of a return and more comforting.
Joseph, I brought in just a little Carl Little Gibran volume, because it's like
parables and things like that. There's one there that I think is really
appropriate to you. It's about the earth. Yeah. Do you see it? Why do you think
it's appropriate to me? Because you'll see. All right. OK. You read it. I'll
read it for everyone. The red earth. Said a tree to a man, my roots are in the
deep red earth, and I shall give you of my fruit. And the man said to the tree,
how alike we are. My roots are also deep in the red earth, and the red earth
gives you power and bestow upon me of your fruit. And the red earth teaches me
to receive from you with thanksgiving. What do you think? I mean, it's a lovely,
lovely quote. I don't know if it sounds like me. I don't know. I've never talked
to a tree, for starters. I was thinking about your connection to the ancient,
connection to the earth. Yeah, of course. No, that's true. Music being our
common language. Well, it is. I mean, I've always been a strong believer in
honoring the ancestors and the music that's come before us, because I think
music is constantly evolving, and we have to know at what points it was
evolving. It just doesn't drop from the sky. There's efforts made by musicians,
and I think that's really important to really honor the roots but move forward
with some pioneering things. So I think we're listening to the next one, Self-
Knowledge, which is a solo wood piece from the pieces based on his works. Music
playing Music playing Music playing Music playing Music playing Joseph Tawadros
there with his own Self-Knowledge from The Prophet, which is music inspired by
the poetry of Khalil Gibran. And we mentioned that commonality of musical
language. And I've read you've talked about that a lot. In fact, that seems to
be one of your, maybe one of your missions. Yeah, well, I mean, it's just there.
I mean, I was brought up in Australia. I was born in Egypt and love the Egyptian
culture and Arabic music. But there's so much great music out there, and you can
find always similarities between music. And it has to be done well, and I think
great collaboration comes from meeting points rather than just having two things
run at once. And you've seen those collaborations where it's just, you know,
everyone applauds on the end, but no one really gets it. Whereas, you know, it's
really important to understand about the musicians that you deal with, their
style, their upbringing, and then to find a meeting point, which is, you know,
that we both can understand rather than just having two things run at once.
You've talked a lot about labelling and how labelling certain genres and
musicians as world or ethnic, et cetera, kind of undermines that commonality of
the musical language and the musical ideas. Well, I don't actually mind the
labelling. I mean, you're going to get labelling you don't like or, you know,
people putting you in certain boxes. But I do think there are many more
similarities than differences. And, you know, audiences are almost shocked
about, you know, these music that they do like and how they've liked a certain
collaboration. But I don't think it's that hard. It's just that you have to
listen, and you've got to have musicians that are prepared to learn about the
other genres and to learn about the other musicians and to find a really good
meeting point. And that's what I've always tried to do is find the right
musicians to undertake that. You know, Richard Tognetti was one of them. We
worked on arrangements with the Australian Chamber Orchestra, and that was a
very successful tour. And we then did one again based on Vivaldi and music of
the Baroque, which showed the similarities between some of the things that I do
and Baroque music and the progressions and the way it moves and the intervals.
So, yeah, I think there's much more similarities, and it's not that hard to
find. And I think playing an ethnic instrument or labelled as ethnic, I think,
shouldn't deter people from listening to the music and loving it. Do you think
Mozart is a great example of this? I wouldn't call it the common touch, but it's
something that speaks to everyone, isn't it? Mozart's a great... I mean, I love
Mozart. And there's a lot of... I mean, there's been experiments with Arabic
musicians. There is a CD floating around called Mozart in Egypt, where they get
a lot of Mozart's music and have Egyptian musicians play along with it. But this
is why I picked the Symphony No. 25, the first movement in G minor, because it
really feels like an Arabic piece. I really, you know, feel that, you know, it's
like an Arabic folk song with some harmonies over the top. I performed with the
Sydney Symphony, and they did this piece, and my brother and I were able to do a
little version of it on the oud and the percussion, just to show what we talked
about. Do you remember how it went? Could you give us a little list? I think I
can... The opening, I can't really remember. It's off the top of my head, but
something like... ¦ That'll do, I think. ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ The Royal
Concertgebouw Orchestra under Joseph Cripps, that was the first movement of
Mozart's Symphony No. 25 in G minor, and that was the choice of Joseph Tawadros,
who's sitting with me in the Goossens Hall. We're kind of sweating it out today,
aren't we? I know, it's the way it is. All those cutbacks, they're killing me.
Gosh, give me some water. We wanted to build the electricity. Somebody bring me
some water. Somebody should write a song about it. So think about... I know
you've always... I think they tried to recreate the Middle Eastern conditions
here. They go, here's the Sahara for your interview, buddy. Thank you. All
right. I know you've always got lots of pieces at your fingertips. Have you got
a few in mind, so we'll have a little play at the end? Yeah. Oh, we'll just see
how it is, you know. I'll just pull out a score for you tomorrow. I know you're
so good at sight reading. Well, I was just a little bit worried about my tuning.
That's all right. Well, I should be worried about my tuning. That's what I have
to be concerned about. You're never going to say you're out of tune. How long
does it take to tune the organ? I mean, it depends. I mean, under these
conditions, we have to do the interview again next week. But no, you get used to
it after a while. Generally, an oud stays in tune, or the instrument. You know,
any instrument usually relatively is in the area of its tuning. But yeah, I
mean, being a fretless fingerboard, you have to kind of adapt to the players
you're playing with and certain tunings. But, you know, violinists are the same
and cellists are the same. So what are the notes? What are the strings?
Basically, the oud is in fourths. There are five doubles. The oud that I have is
five doubles and two single bass strings, whereas normally it's five doubles and
one single. So I have a seven-course instrument, so I have a wider range than
some of the other players. What's your lowest? Well, I can get down to a G below
middle C and then probably two Gs above middle C, or three Gs, actually. I'm on
three G at the moment. I'm hoping to advance to four G, depending on the
technology. And talk a little bit about quarter tones and using them and how
they affect us. Because for me, I feel like it certainly adds a kind of
earthiness, maybe a humanness to the sound. People sometimes get confused that
quarter tones are ornaments and they're not really ornaments. They're actually
set notes that are used in Middle Eastern scales. And some scales in the Middle
East don't have quarter tones, so that's also something that people should
understand. So in the West, quarter tones are generally seen as out of tune or
something as a bit of a colour or something to pass through, getting to another
point. But really, in Arabic music, they should be called three-quarter tones
because they're never really a quarter tone apart. Intervals are in relation to
the next note. So if we look at the major scale, let me play the major scale,
which in Arabic music is called agam. ♫ And I play the harmonic minor, for
instance. ♫ That's called nahawand. I can play one of the most important scales
in the Arabic world called rust, which is basically the major scale. If we
listen to the major scale. ♫ And if I flatten the third and the seventh note to
a quarter tone, you get this. ♫ So if I do that, it's not the same, is it? You
can't get it on the piano. It's just impossible to do on the open fingerboard of
a piano. Just to explain it further, if I play the first three notes of major
and the first three notes of minor, if we listen to the third notes of these
scales, you can hear that I can fit an interval between the E natural and the E
flat. So here's C major. ♫ So if I played Waltzing Matilda. ♫ So it's a very
popular scale in the Middle East. And as you can see, that out of tune element
is slightly off. But in that scale, none of the intervals are a quarter tone
apart. They're three quarters apart. You've got a great melody called Frio. Is
that about Fremantle? Yeah, about Frio. Well, you know, I was born in Egypt but
brought up in Australia, so I've got to, you know, get into the Australianness
of it all. But do you think I could play, like, I know it's got like a D. Yeah,
it's like a D mixolydian, really. So it's like a D major with a flat seventh.
Can we give that a go? Yeah, let's give it a go. Well, the scale would be... ♫ ♫
♫ ♫ ♫ Oh, that was lovely, Tamara. Excellent stuff. What is the Concerto of the
Greatest Sea? I bet I couldn't play that. Well, no, you could. Why not? It's
actually, it does have piano on it, so you could play that. But it was teaming
up again with musicians that I really liked, and one of them was Matt McMahon,
who's been a long-time collaborator. First time he recorded with me was in 2008
for an album called Angel. And ever since then, he's been on numerous
recordings. And this one was a really great project we did with the viola player
Christopher Moore. At the time, he was in the Australian Chamber Orchestra, and
I was playing a lot with the Australian Chamber Orchestra. So we became good
friends, and I thought he would have such a beautiful sound for this, which he
does. And the Greatest Sea was, again, coming off the Kilojubran series of
recordings. The first one was The Prophet, the second one was The Hour of
Separation, and this one rounded off the Jubran trilogy. And it just features
Christopher Moore and his beautiful, sweet viola tone with some piano backing
and my brother on frame drum. ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ ¦ And that was the
first movement, Seafarer, from the Concerto of the Greater Sea, in fact, from
the Greater Sea Suite in A by Joseph Tawadros, who's with me in the Eugene
Goossens Hall, clasping the ood. Mm. Clasp. Ready to perform. Clasping. Who was
playing with you there? That was Christopher Moore on viola, Matt McMahon on
piano and my brother James Tawadros on bendir, which is the Egyptian frame drum.
You've always composed, Joseph, haven't you? Yeah, well, I mean, since the
beginning, since the first album, I've always composed my own music, and that
was a part of the challenge. There was just no repertoire to play with other
musicians, you know, and it was so exciting. I think throughout the years I've
been very lucky to collaborate with some of the great Australian musicians here
and musicians from around the world and to create works for them and to be along
and play along with them as well. And, you know, I've done something I'm very
proud of that doesn't get much light. Speaking about Gibran earlier was, you
know, a project with the Song Company, which had SATB voices, so that was cast
upon me. And then most recently was the Ood Concerto, which I find very
important in the development of the ood and in the development of where I'm
going as a composer. So how do you go about composing for a symphony orchestra?
I mean, do you do it at the piano or at the ood or sitting down? No, it's always
at the ood. And the thing is, ood players, and again, with that kind of dream of
being in Egypt and loving Egyptian music and all that sort of stuff, most ood
players are composers and most of the ood players do write their own songs. So I
felt like that was my kind of duty as an ood player to be also a composer and an
improviser. And I think we played Rachmaninoff earlier. It's that kind of
getting into that mind space, you know, what you want to achieve with a certain
piece. And whenever I compose for musicians, I try and get into their brain and
compose to their strengths, because it's important to compose to collaborators'
strengths so the collaboration can be strong. I'm not a fan of these give
musicians stuff they can't play just because, you know, let's see what happens.
It really has to be done very well so you have a strong collaboration. And with
the concerto, I wanted something which would challenge me. Most of the music
that I wrote, I couldn't play, but that's a thing that composers do for other
musicians. You know what I mean? A composer will compose a violin concerto not
knowing how to play the violin. So I didn't find it too far off. But again, it's
that challenging trying to create a work that was, you know, that would hold the
test of time. Now, our last track is Vivaldi. And I think I'm right in saying
that you're the only Australian composer that the Academy of Ancient Music has
performed? Yeah, I know. I mean, that's by default because I had to play one of
my – because I only play old music. But it was a real honour. They actually sent
me a letter saying, well, yeah, because I don't play modern music. But in this
program, again, Richard went over and we did a program, again, the same program
we did with the Australian Chamber Orchestra, which was – I spoke about it
earlier with the Baroque music and my music and finding similarities between
that. Because Vivaldi, you know, there was a lot happening in Venice and a lot
of trade in Venice, and so a lot of the music was affected and a lot of the art
was affected. So that's what we were trying to bring to the foreground. And I've
always loved Vivaldi. It was my in. It was my gateway composer into music of the
West, and I really enjoyed his music, and I found his music very close to that
of the Middle East. And this violin concerto we actually played together in that
series. And so it just – for me it sounded like an Arabic folk song. ♫ ♫ ♫ ♫ ♫ ♫
♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫
♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫
♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ ♫ And that was Vivaldi, the Violin Concerto in A minor.
You heard the Academy of Ancient Music. Elizabeth Wilcock violin and directed by
Christopher Hogwood. It was the choice of Joseph Tawadros. You've brought
something a little bit challenging, haven't you? Yeah. Well, I thought if I was
going to challenge anyone, it would be you, Tamara, and that's why I brought a
piece called Give or Take, which was originally as a duet for double bass player
John Patitucci on the Hour of Separation album. So when I found out that he was
going to be on the album, I wanted to challenge him and write this piece, which
is kind of a showpiece. We then later did it with the Australian Chamber
Orchestra in violin duets. I've seen you with Satu Vanska. Yeah, good old Satu.
She gives it a really good go, this piece. And so I thought it would be great to
hear it in its... You know, this is a world premiere on piano. No-one's ever
played it on piano with me. OK, so give me some tips. Remember... Just play
fast. Just play fast. Just smudge. Smudge is your friend, you know. And the
other thing is, you know, a lot of the listeners are going, oh, that smudgy
Middle Eastern stuff. Don't worry, get away with it. It's all right. This is
Duet on RN with Tamara Anna Chislovska. GUITAR SOLO PIANO SOLO GUITAR SOLO
GUITAR SOLO Joseph Tawadros, I've been forced to count to four and beyond yet
again. Thank you so much for the duet. Thank you very much for having me. Hello,
James Carlton here. You know, we Australians keep rather monastic habits. Many
of us get up at dawn, slipping into our daily rituals, not in churches or
temples, but secular spaces, beaches, walks and gardens. And the really
dedicated among us have the year-round early morning ocean swim. On God Forbid,
daily rituals and secular habits and why we're drawn to them, a special series.
Join me Sunday 6am or any time on the ABC Listen app.