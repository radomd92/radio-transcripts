TRANSCRIPTION: ABC-RN_AUDIO/2023-04-13-15H00M.MP3
--------------------------------------------------------------------------------
 ABC News, I'm Clive Hunt and communities in Western Australia's north west are
on red alert after severe tropical cyclone Ilsa was upgraded to a category five
system. Anna Murphy has the latest. An area from the remote Aboriginal community
of Bidjidanga to Wim Creek is currently on a red alert for the cyclone which is
bearing down off the northern coast. It's expected to make landfall later
tonight or early tomorrow at category five strength. It will be the strongest
cyclone to hit the WA coast in 16 years. Emergency services have evacuated some
residents and roads have been closed as a precaution. Bureau of Meteorology's WA
manager Todd Smith says the storm is only hours away. The system is likely to
cross the coast later this evening so conditions will really start to ramp up as
we head towards sundown today. Evacuation centres have been set up in Marble
Bar, Newman, Nuland Gain and South Headland. German tourist John Kirchner
travelled through the Kimberley before reaching Port Headland. It's a pity
because this area is supposed to be very beautiful and we wanted to visit as
much as possible of it. For example Karajini, so now everything's drowning so
it's not the same experience but we still like it. And local councillor Annabel
Landy says it's a tense time for everyone. It's really scary for everybody here.
Like category five is really big and you don't know what's going to happen. When
you're going to hit the inland and the rainfall and you know it's going to mess
up the roads. To other news, the Northern Territory's police minister has
accused the federal opposition leader of political game playing. During a visit
to Alice Springs, Peter Dutton called for the Prime Minister to intervene to
address crime rates and again called for the Australian Federal Police to be
deployed there. However, Kate Warden says government policies, including alcohol
restrictions in Alice Springs, have already had an impact on crime. He's got no
statistics, he hasn't met with the agency. This is just a political stunt to try
and make himself more relevant in a party that's every day becoming more and
more irrelevant. Norway is expelling 15 Russian diplomats accused of operating
as spies, the BBC's Paul Moss reports. They use the normal euphemism of
international relations. Norway's government said 15 Russian diplomats had
engaged in activities not compatible with their status. Translated into regular
English, they're accused of spying and this marks just the latest step in a
wider Norwegian crackdown on Russian espionage. Last October, a university
researcher was arrested in the northern city of Tromso. He'd identified himself
as a Brazilian academic but was accused of being a Russian intelligence officer.
The same month, seven Russians were arrested for flying drones near areas
considered militarily sensitive. The Russian Foreign Ministry says it will
respond to those expulsions but isn't giving details yet. The European Space
Agency has cancelled its Jupiter IC moons explorer mission at the last minute.
The satellite was due to leave French Guiana today on an eight-year journey to
reach the giant planet's major moons. Scientists say the risk of lightning means
the weather conditions are not right for launch at the moment. The team will try
to launch the satellite again tomorrow. If and when it does get into space, it
will examine three of Jupiter's moons to see if their hidden oceans of water
could contain microbial life forms. Carol Mundell is the agency's director of
science. Deuce is an incredibly exciting mission. It's very complex. It has 10
instruments on board. It's a very interdisciplinary mission. And I think from a
personal perspective, what's most exciting is we're actually going to study a
mini solar system in its entirety. US President Joe Biden is visiting the
official residence of Irish President Michael Higgins as the trip to the country
continues. Mr Biden is holding talks with the president before meeting with the
Prime Minister Leo Verrattika. He'll become the first US President since Bill
Clinton to address a joint sitting of both houses of the Irish Parliament in
Dublin. In the NRL, the Rabideauxs of Beetham the Dolphin's 36-14 in Brisbane.
Here's Quentin Hull. Bunney scored five unanswered tries in the second half to
overcome a 14-6 halftime deficit and cruise to their fourth win of the season.
Captain Cam Murray starred and Campbell Graham scored two tries as Souths posted
back-to-back wins for the first time this year. Round seven continues with the
Friday doubleheader featuring the sharks hostering the roosters before the sea
eagles take on the storm. Quentin Hull, ABC Sport, Brisbane. And in the AFL,
Adelaide have beaten Carlton 118 to 62. And that's ABC News. This is the music
show and I'm Robbie Buck. Thank you. Later in the show we speak to George
Galeotsis, one of the founding members of Apotomy Companion who called in very
early in the morning from Athens about the art of Ribetica and its place in the
Greek communities around Australia. But first we remember the life and work of
composer Ryuchi Sakamoto who died last week at the age of 71. We're currently
listening to 2020-0207 from his final album 12 which was released in January
this year. The album is an intimate meditation on mortality that he recorded
following his second cancer diagnosis. Sakamoto was a pioneer across many
musical worlds. In the late 70s he was at the forefront of techno music with his
band Yellow Magic Orchestra, one of the first groups to incorporate the 808 drum
machine and the vocoder amongst many other electro innovations. He was also a
film composer, perhaps he's best known for his score to Merry Christmas Mr
Lawrence, which he also starred in alongside David Bowie. His other film music
includes the Oscar winning score to The Last Emperor and 2015's The Revenant.
Today we're digging into the Music Show archives to remember Ryuchi Sakamoto,
two interviews with the man himself in 1996 and 2018, as well as a conversation
from 2018 with Michael Nomura-Sheeble, the director of the documentary Ryuchi
Sakamoto Koda. When Andy last spoke to Ryuchi Sakamoto on the Music Show he
asked whether the piano was still the core of his musical existence. Yes, the
piano was the very first instrument I started playing when I was maybe three or
four years old. And since then, you know, the piano has been always beside me
and it is still the easiest device I can seek some harmony and notes and
melodies. So I'm still using every day, exploring some sounds, new sounds and
harmony every day. And when you play the piano keys, do you hear other sounds in
your head, electronic sounds or other instruments? Yeah, not only myself
probably, but most of the composers use the piano for even writing for the
orchestra. So I play the piano but I'm hearing like let's say three string
ensemble in my head. He is capable, he's the man who's capable of creating very
kind of romantic, powerful melodies and yet at the same time when he creates his
own music for his solo albums he tends to restrain himself and shy away from
doing that a lot. This is Michael Nomura-Sheeble, director of Ryuchi Sakamoto
Koda. What I find amazing in his film scores is that, you know, he pours a lot
of emotion into his scores and some of the melodies are just so powerful and
overwhelming. So I find it unique because especially now and recently he's quite
a minimalist when it comes to his expression. But when he composes for films I
find that he's quite romantic. He's influenced by French composers and that
tends to come out. He's just very creative. In 1996, Sakamoto released an album
simply called 1996, where this version of a theme from The Last Emperor comes
from. It was a departure for him, just violin, cello and piano. In a sense it's
the opposite of your electronic music. Why were you drawn to working as a piano
trio? Meeting with these musicians, I knew instantly we had to play together and
we started playing. The material we've been playing is just mine, my music,
because we need to have some rhythm music. But we can improvise, yes we
improvise a lot in the studio, but to introduce this ensemble, I thought it
might be good to play just my major work and we'll do something different in the
future. Are you tempted to play classical music with them? On some pieces of
music, it might sound more like techno or minimalistic music. The other ones
might be more purely classical, but some other pieces might be more pop. I don't
care – these are all genres. I'm inevitably reminded of the late music of
Debussy and I read references to Debussy in lots of interviews that you've
given. Why is that music so important to you? I found Debussy sort of by
accident. One of my uncles collected records and his collection is mainly
Mozart. But he had one record of Debussy's string quartet and Ravel's quartet. I
just grabbed it when I was maybe 13 years old. The shock I listened to it for
the first time was still very visible to me. That remains forever. Because
before Debussy I was into Beethoven and Bach and Beatles. But Debussy's music is
very different from others. In the late 1970s though you were very specifically
seeking something quite different which was, to use your word, techno music. You
virtually invented techno pop with the Yellow Magic Orchestra. What was that a
reaction to? In the beginning people were puzzled about what we were doing. What
we were doing is very different from the major music scene. Not only in Japan
but in the West also. Probably our closest music was only craft work from
Germany. Then maybe a couple of years later they started realising this was
going to be a new standard or new format to create music. And they started doing
that. So you led the way. Yes. The end of Waaraschhorn bellow. turn it on there
The last 20 years of Sukumoto's life featured a seven album collaboration with
the glitch artist Alvin Oto. Maybe we could talk about working with a glitch
artist. I think there's something very touching about working with somebody who
deals in the area of malfunctioning technology. It's more human. Yeah, in no way
more human, but his music is more cold and metallic. And very first time I met
him and I heard his music, I was totally fascinated because our backgrounds are
totally different. My background is very musical, academic, but he didn't have
any kind of academic musical education. So he thinks music sort of
mathematically. So he manipulates bars and rhythms and everything numerically.
And that's very unusual. How does that work in the context of your musical
ideas, particularly the notion of skipping CDs and electrical distortion in
glitch music? That was also fascinating. It's a new color to music. And so
before that, we didn't have that palette. So that was good. And somehow my very
gentle Satie-esque piano works with his glitches very well. It's a kind of a
miracle. Theoretically, it is very difficult, but it works very well. Do you
know how we put up the album called Glass? I think it was last year. And the
glass music was the very first time we improvised completely. And before the
Glass album, we always played the tracks from the albums. But then at the Glass
house in Connecticut, USA, we just improvised and it went very well. We liked
the result. So this time, this year, Karsten and I decided to combine
improvisation and playing back from the albums. Sort of half and half, but not
like a separated half, half and half. Informations and tracks must be
integrated. Thirty years after you won a BAFTA for Merry Christmas, Mr.
Lawrence, you won another for the Revenant, the Iñárritu film. The score is a
collaboration with Alvin Notho. When you write a film score for a film like
that, which is not an easy film to watch, I wonder what role you feel your music
plays. Is it the role of your music in a sense to make it harder to watch or to
maybe ameliorate some of the darkness in the film? Yeah, the film itself is not
easy to watch for everyone. And also the role Mr. Iñárritu gave me was not easy.
He ordered a full of sounds, not the music, but the sounds, the accumulations of
sounds. Yet, you know, those accumulations of sounds would emphasize the
emotional story throughout the film. So normally, music can emphasize the
emotions according to the story, but the sounds, it is difficult to do that with
the sounds. And you've been very successful at it. Oh, thank you very much. He
didn't want to have like a very conventional film music melodies or themes. It
should be very subtle. Subtle, yet very emotional. I mean, that is full of dark,
glacial sounds. And I wonder whether, in a sense, that was related to your
illness. I mean, it was composed after recovering from surgery, wasn't it, for
throat cancer? When I started working on that film, I was not fully recovered. I
was still struggling to get myself recovered. So it was very, very hard. And
yes, I felt I was one of those revenants. In 2018, the documentary Coda was
released. We heard from director Michael Namura-Sheeble earlier and we'll hear
from him again in a moment. Andy spoke to Sakamoto himself about this kind of
documentary portrait. Well, I'm very shy to see myself on the big screen. So I
cannot be very objective. But I think it's well done. You know, it's not over
melodramatic or anything, you know, very subtle. And I like that. Yes, yes, it's
not melodramatic at all. No, it's a beautifully paced film. And of course, the
film was partly about your illness. And can I ask how you are? It has been the
full four years now since the treatments. And I'm pretty recovered fully. Thank
you for asking. It's very good to hear. In that film, we see you sitting at a
former evacuation center for survivors of the 2011 earthquake and tsunami. And
you're playing that music on a piano. And it obviously it obviously still means
a lot to you. Well, probably it means a lot to them, to them, to the people, to
me, maybe. The song is so popular in Japan. You still hear it everywhere,
especially during Christmas season. And it is a melody that is very dear to the
to the people over there, as well as, you know, all over the world, of course.
And I was there with Ryuichi, we were filming that scene that you mentioned in
the disaster zone in northeastern Japan. Yes. And you can see that the music has
such an effect on this audience that is sitting in the dark in the cold, very
still looking traumatized, actually. And then he begins to play and the mood
changes, the mood lifts, doesn't it? It was an experience that I really cannot
forget. But what I remember was people told me that, you know, they felt calm
for the first time since the disaster when Ryuichi played his music. Some people
told me that they were able to sleep as they heard the melody for the first
time. So and, you know, the actual gymnasium where we filmed the performance of
the melody was an evacuation facility immediately after the quake. And I
understand many people, you know, some people even died there and so forth. So I
guess it says something about the healing power of music, if you will. It was a
piano that, as you had mentioned, it was overtaken by the tsunami and it was
flooded. And Ryuichi, early in the movie, he says, you know, he felt as though
when he first saw the piano and started playing it, he felt as though he was
playing the corpse of a piano that had died. Later on, as he starts to think
more about the instrument and about music and about technology and so forth, he
says that he starts to like how the piano sounds to him. And he says that
perhaps, you know, it is because subsequently in 2014, as we continued filming
with him, he learned that he had cancer of the throat and he became very kind of
mindful of his own mortality. And that perhaps changed the way he saw things. So
he started to relate to this piano in a different way. And he started to hear it
sound and feel that it is actually quite beautiful. And I found it to be very
amazing, the story and his connection to the piano, because initially he is a
person saw the tsunami and nature as a destructive force, if you will. But he
later began to think of nature as something that is liberating at the same time.
And he started to think that the piano and all the particles inside of the
mechanism that the piano actually is, wanted to kind of be set free from the
conditioning that people had imposed upon it. So the piano becomes a metaphor in
the movie in that sense as well. It starts to symbolize not only Ryuichi and his
illness and his mortality, it also starts to kind of become a key point, if you
will, through which we kind of explore ideas of technology and humanity, art and
nature, and so forth. And the way in which we live in relation to nature as a
society as a whole. Mortality has been a theme that started to appear in your
music about 10 years ago, which is, I suppose, not surprising. The decaying and
the disappearance of the piano sound is symbolic of life and mortality. Is it
also true to say that music is a kind of lifeline for Ryuichi Sakamoto? I think
so. But also, like a very harsh moment. For example, like a 9-11, after 9-11, or
after 3-11 of Japan's tsunami disaster, it was very hard even for me to listen
to music. But then after some weeks or a month, yes, the music saved me. I was
slowly getting into the work of music. Then I recognized the music saved me
after those harsh moments. He's an artist. It's a part of his soul, if you will.
And I kind of loved the archetype that we ended up embracing through this whole
journey of ours, which is you return to your craft, you return to your calling.
And he wanted to create more meaningful music, more beautiful music, with the
time that he has left. And that becomes kind of the narrative arc in the journey
of our story. Ryuichi reflects upon his life as he creates new, amazingly
beautiful music. Every Christmas, Mr Lawrence was not only Sakamoto's first film
score, but his acting debut too. I started the two new things at the same time,
you know, composing film music and acting. Did it help you to compose the music
because you had been in the film? Yes, it was done after the shooting, right?
So, of course, you know, I was into the subject. You know, I got the needed
feeling when I was on the set. So it helped a lot. And it helped a lot because
right after the shooting, I saw the rough cut, first cut of the film footage.
And my acting was worse than I expected. The only thing I could do is writing
better music to compensate my bad acting. Do you enjoy acting? Yes, I did. But
after acting job was over, you know, I was really... You realized you were
really a composer? Yes, I must be a composer, not an actor. I suppose because
of... because that soundtrack is so famous and the film is so famous, you became
known around the world not just as a composer, but as a Japanese composer
because of that film, particularly. Is that a problem for you? Because I know
you live in New York City and I figure you are very much an internationalist. I
want to be, yes. I want to be Cosmopolitan, yes. In everyday life, you know, I
don't remind I'm Japanese or whatever. I don't care. When I travel like this,
you know, because I'm on the tour and I've been through many different countries
and at the airport, I have to show my passport. That's the only time I remind,
oh, I am my nationality. Otherwise, you know, I don't care. Many people still
say I'm doing Japanese music and it's the biggest question. What is it? Japanese
music. Because people don't ask, let's say, Peter Gabriel and he's doing British
music or whatever. So why do you ask me when I'm doing Japanese music? I think
you yourself find these relationships between different musics even when there
isn't necessarily a historical or geographical reason for it. That you hear, I
read somewhere Japanese pop music and it sounds to you Arabic. Yeah, like some
intonation, that kind of thing, you know, that reminds me. Not necessarily
Arabic music, maybe Irish music. My fantasy is there might be hidden underground
pipe or relation in all these ancient days between many different cultures, you
know, from Far East through Asian countries that might have reached to the end
of the other side of the continent or the other side of islands. That's my
fantasy. He's a very wide ranging composer, isn't he? I mean, it's not just film
music and it's not just concert music. There's also the synth pop of the Yellow
Magic Orchestra from the 1970s. He's quite a hard man to pin down. Oh, yes. I
mean, literally he's all over the map and he has a very wide variety of musical
styles that he works in. He's called Kyōju in Japan, which means professor. And,
you know, he's formally trained at the conservatory. He has a master's degree in
film composing and he studied musical anthropology as well. So he's really, you
know, he's well versed in, of course, Western music and Japanese music, but also
he understands, you know, musical tonalities from all over the world in
different styles and cultures. And he incorporates all of that into his music. I
sense that you make very little distinction between classical music, which is
your background. I mean, you mentioned Satie and Wihi, you played Bach, I think,
and Debussy in that film. And not to mention your own compositions, but between
classical music and pop, do you see a dividing line there at all? Obviously, no.
There are no categories or no genres, no genre borders in my head. But of
course, historical contents are different, like for Bach and great music. But to
me, it's hard to tell, but to me, let's say I like gardens, all kinds of
gardens, English garden, Japanese garden, Chinese garden, the French garden.
They're all gardens. To me, it's like music like gardens. So there's classical
music, baroque music, Greek music, but I like them all. The fact that Bach is a
sort of mainstay is interesting, because I remember that Tore Takamitsu always
used to listen to this Matthew Passion before starting work on a new
composition. Exactly, yes. So is that Japanese tendency? I don't know, being
drawn to Bach, especially like minor, minor, minor tunes of Bach. It's a sample
of two. Oh, okay. Alright, fell good-okay. Rewichi Sakamoto, Solari, from the
album Async, who died last week at the age of 71. You're listening to the music
show. Easter weekend means the National Folk Festival is back in Canberra and
one of the groups making a return to the festival grounds is Apotomy Compania.
They're a band who hail from Greece, well actually they're originally from
Melbourne's Greek community and made their way to Athens where they're based
now. Their music stems from the Greek folk tradition called Rebetika and we're
going to have George from Apotomy Compania explain all of that in a moment. But
first here's a taste of what we're talking about. This is a song called Living
Apart. Yaggate pono ke kaimo evrethi ke votani Ma o kaimo stu horizmu deni pori
na giani Ii pikras ke ta vasana Meto kero xehniondi Ii pikras ke ta vasana Meto
kero xehniondi Ta matia omos pa'gapas pote delizmo nionde Ii pikras ke ta vasana
Meto kero xehniondi Ta matia omos pa'gapas pote delizmo nionde Ii pikras ke ta
vasana Meto kero xehniondi Ta matia omos pa'gapas pote delizmo nionde Mine
kardia mu erosti mesi trofia to klama Mine kardia mu erosti mesi trofia to klama
Na agapithis na horistis pes pos to eki stama Na agapithis na horistis pes pos
to eki stama Galeatsos on the line from Athens here on the music show. Hello
George. Good afternoon. Are we talking about 1984 when the group came together?
That's right. I mean you've done your homework well. You were going around
Melbourne listening to all sorts of folk bands mainly to Dan O'Connell going to
festivals and at some stage we thought there's not really any dance or musicians
playing traditional Greek music or for the matter, Rebetika. So we decided we're
not start playing that style of music and we saw that there was an audience for
us, especially at Melbourne University and the Trout. So we started playing
little gigs there and gradually made a real name for themselves and when we were
invited to play at the National Freight Festival in 1996 in Melbourne. We're
talking about a massive diaspora in Melbourne at the time of course. It's I
think the largest outside of Greece. So I take it you had a ready audience.
That's right. There was. And I suppose there was a gap there that we just filled
in. And how important was it for folk who had those connections to Greece to be
hearing music like that in Australia? At the time most of the bands that played
Greek music in Melbourne were bands that played for reception halls for social
events like weddings and stuff. And there wasn't really a band or musicians
around playing say concert style or small venue style settings. And I think the
forum did that. You've mentioned the term Rebetika and we should explain what
that is. It's a style of Greek folk music but it has a really interesting
history. Take us back into that. Well the origins are lost in history as I say
but basically it's a style of music that originated on mainland Greece. It's a
style of music that really tanked a focus in the 1920s especially after the Asia
Minor catastrophe of 1922 where we had an influx of about a million and a half
refugees from Asia Minor who came to Greece. And with them they brought their
style of music especially from Zmuga in Prostantinople where the music was
basically played on such folk instruments like the fiddle, the gunoy, the
sandoori. And this became the Greek popular music of the 20s and 30s just before
the Second World War when of course the political situation changed in Greece.
There was a dictatorship, a Metaxas dictatorship who actually at the time
thought that they could purify Greek music by banning any references to Eastern
modes. So what Metaxas did, they imposed a censorship on any style of Greek
music that used these Eastern modes, you know the various matams as we say. So
most of the music was, musicians were forced to compose only in the major and
minor scales. I like that at the same time in Turkey Kemal Akatürk tried to do
the same thing and he also banned that style of music particularly the vocal
improvisational style called amines. So it was a tragic really. The origins are
sort of urban in the cities, city centres and the slums of the cities. You could
only say that's what the composing could be made there. Possibly for the same
reason with tango. George I'm looking at photographs of Apotome Kampania's
history. There's a lot of bazookas here but there are other instruments that pop
up over time as well. Take me through what some of the changes have been over
the years for your group. Well we started off with playing the batika. The basic
instruments is the bazooka, that long neck to the root style of instrument. It's
the jurra. A jurra is similar to a bazooka, it's slightly smaller with a smaller
body and a shorter scale length and then the baglama which is the small one
which is much smaller and has to say two octaves pitch higher than the bazooka
and that blends in well with the jurra and the mainline bazooka and accompanied
by the guitar. And what about the role of vocals in this style of music? We do
have the smooth neck style of singing which incorporates very chromatic use of
the voice, singing in the various makams. One particular aspect of that style is
the amanir which is what we call more improvisation. The song is sang to a de
capedacilibus. De capedacilibus is just a verse of 15 syllables as we say.
That's why it's called de capedacilibus and it's basically improvised in that
particular mode, in that particular scale. The singer bases takes the lyrics and
improvises as he goes along. And we should mention that there is often dance
involved in this too. Of course. It's the classic zebequico, it's the nine
eighths as we say. But there are variations of these nine eighths. You know how
it's spread up into, it could be spread out to two, two, two and three or it
could be two, two, three and two or it could be two, three, two, two and two. So
that might sound a bit confusing but in the end it all adds to a variation of
dances. So what about the relocation for Apotome Kompania? You started in
Melbourne these days, you're based in Greece but you still have a very strong
connection to Australia, you return most years. That's right. We've made a sort
of a pilgrimage to come back every three years or so. That's because we want to,
we've got friends there, we've got relatives, you know we've spent quite a large
part of our lives there so this has been one of the reasons why we've been
coming back because it's an opportunity to reconnect, to play music and to meet
up with friends and relatives and basically have a nice and good time. Well
let's put the dancing shoes on now and enjoy a little more Ribidica from your
group Apotome Kompania and we look forward to seeing you on the ground in
Australia soon George, thank you. Thank you very much for having us. Thank you.
Thank you. Thank you. Thank you. Thank you. Thank you. Thank you. Apotome
Kompania and a song that translates as playful or flirtatious widow. They're
playing at this year's National Folk Festival in Canberra. Details on our
website. That's all for today. Next time the author and silent film composer
Philip Johnston is on the music show to reveal the secrets of writing new music
for very old films and even how you write music for a graphic novel. We'll leave
you today with new music from the British folk singer Shirley Collins. The first
single from her upcoming album Archangel Hill. This one's called High and Away.
Pulled up another clean out of its footings, dropped it and smashed it to
matchwood I swear. But in the hen house it worked up to the treetop, the hen was
still sitting not an egg broken there. High and away, tell me the tricks that
your nailer can play. High and away, tell me the tricks that your nailer can
play. In the path of a twister there's nothing will stand, bowl a card down the
street like an empty tin can. A little post off his mountain view Arkansas
carried away by a twister oh lord. The letters returned to us down from
Wisconsin showed just how far the mail fluttered abroad. High and away, tell me
the tricks that your nailer can play. High and away, tell me the tricks that
your nailer can play. I once had a box and a little book in it with all of the
words to the songs that I knew. Then one day it was lost and I hadn't the heart,
no I hadn't the heart for to write them again. How far would you follow someone
to the other side of the world? In Earshot's new seven part series we're telling
confessional stories of following a doomsday cult leader, a death match
wrestler, die hard music fans and a mother trying to keep her daughter sane and
safe online. I just thought what could we do? That was a moment of sheer terror.
I was drinking to pass out. Come follow Earshot and lose yourself completely.
Saturdays at 2 on ABC RN and anytime on the ABC Listen app.