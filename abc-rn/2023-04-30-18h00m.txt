TRANSCRIPTION: ABC-RN_AUDIO/2023-04-30-18H00M.MP3
--------------------------------------------------------------------------------
 did replace the US as the dominant power in the Indo-Pacific. Hear what you
need to know and why on ABC RN. Hey, where do you get your best big ideas? In
the shower? Walking the dog? Maybe making dinner? I do all of these listening to
RN's big ideas. Deep thinking, sensational minds. Big Ideas is your front row
seat to the best live forums and festivals across Australia and the world. And
I'm your new host in 2023, Natasha Mitchell, with you Monday to Thursday, 8pm on
ABC RN or on the ABC Listen app. Feed your mind, one big idea at a time. ABC
News with Andrew Calupis. A consignment of medical aid has reached Sudan the
first since violence broke out between the country's army and the rival rapid
support forces two weeks ago. Here's the BBC's Will Ross. On board the plane
that landed at Port Sudan were eight tonnes of aid, including medical supplies
to help those wounded in the fighting. The International Committee of the Red
Cross says it hopes for security guarantees to send more humanitarian relief to
the capital Khartoum and Darfur. Aid agencies, including the UN, have come under
criticism for the lack of help that's reached Sudan, where fighting has now been
taking place for two weeks. In Khartoum, where there's been heavy bombardment,
people are running desperately short of basic supplies, including food and
water. Tens of thousands have fled the city, but millions remain trapped by the
fighting. The federal government has announced a review of the multi-billion
dollar infrastructure investment program. Here's political reporter Dana Morse.
A three-month review of the infrastructure investment program will run the ruler
over more than 700 projects currently slated. The government says it remains
committed to its election promise of delivering $120 billion in the
infrastructure pipeline over the next decade, but says the investment program
needs to be unclogged. Rising material prices, skill shortages and supply chain
issues are also understood to be forcing up project costs across the board. The
government says it will follow through on any projects already under
construction. A strong jobs market, still solid consumer spending and strong
population growth has allowed all states and territories to enjoy strong
economic growth, according to this quarter's ComSec State of the State report.
Tasmania is leading the economic rankings with Queensland and South Australia in
equal second position, followed by New South Wales. Chief economist at ComSec,
Craig James, says full employment across the country is helping drive growth.
Now we've got a strong job market and there's a lot of economists as well as
businesses and consumers bemoaning the fact that we've got such a tight job
market that I think it's something to be celebrated rather than commiserated.
The New South Wales Pharmacy Guild says the state's new prescription trials will
help a large range of people access medications. Over a thousand pharmacies will
be able to prescribe urinary tract infection medication to women under 65 from
today and contraceptive pills from July. The Queensland Government tried a
similar plan in 2020 and extended the program last year. Vice President of the
Pharmacy Guild in New South Wales, Catherine Brogner, says the trial will help
alleviate pressures on the strained health system. There's huge value to it. We
know there's value in patients not having to go to hospital, right, when these
UTIs become so severe that they're actually in the hospital. There's also value
to patients who can't access the GP. The Pope has urged tolerance from migrants
and others as he addressed tens of thousands of people in an open air mass in
the Hungarian capital. From Budapest, here's BBC's Nick Thorpe. In warm spring
sunshine, Pope Francis arrived in Kosciut Square in Budapest to celebrate mass
with a congregation of tens of thousands. This was the climax of his three-day
visit and his main opportunity to speak to the Hungarian public. In his sermon,
he urged his listeners to open their doors to those who are foreign or unlike us
towards migrants and the poor. It was a bold message in a country where
government policy has made migrant a pejorative word. Rangers will continue
searching for a 65-year-old man who's feared to have been taken by a crocodile
in a national park on Queensland's Cape York. Police say air and water crews
have been scouring the area since Saturday afternoon. Friends of the man say he
was fishing on the bank of the Kennedy River when he disappeared amid a loud
splash. Forestry officials in southern India have captured a wild elephant that
has killed seven people and destroyed houses in the state of Kerala. More than
150 personnel were involved in the operation to tranquilise the 30-year-old
animal and then transport it to a nature reserve. Villagers have held protests
demanding action saying the rogue elephant was destroying their crops. Wildlife
experts blame rapidly expanding human settlements around the area for an
increase in human-animal conflict in some areas of India. That's the latest from
ABC News. For more news at any time you can head to our website or download the
ABC Listen app. This is Duet on RN with Tamara Anna Chisłowska. My guest today
was born into a family of academics and studied at the London School of
Economics before deciding to dedicate his life to music. These days he's a
global phenomenon credited with single-handedly redefining the image of the
harp. Xavier de Maistre, welcome to Duet. Hello. You've single-handedly done
this to the harp. Well it's always nice to hear. You know when I was studying my
teachers always told me, you know that it's not possible to become a soloist as
a harpist. The best thing you can do is to get into an orchestra. So and I
always had that on my mind that the harp had this potential. So I'm when what
I'm doing now I wasn't even dreaming of it. So it's really nice to be able to
change the image and to open new doors also maybe for the other harpists. So who
said to you there are a lot of great lawyers or businessmen but there's only
going to be one harpist like you. Who said that? It was the great Nikanar
Tsabaleta was maybe the most famous harpist of the 20th century. I met him when
I was competing for a contest in Israel. And I wasn't sure because at the time I
was going to Sciences Po and then I went to London and people were saying what a
shame you could become one of the best lawyers or do a great career. What are
you going to do with music? And so he encouraged me and I of course I was like
admiring him a lot. And so this gave me the motivation and maybe like the last
push to really try my way. Tell me about the Caprice by is it Labar? Yeah Labar
was quite I mean he brought lots of difficult studies and exercises for the
harp. He was a French harpist. Don't think he wrote anything for piano did he? I
don't think so. So Zabaleta is playing tell me what characteristics are we going
to hear in this Caprice? It's quite a virtuoso piece and of course it's not the
most interesting piece from the harmonic point of view but it's very showy for
the harp and Tsabaleta always has a great style when he interprets this piece.
It's never cheap it's always like a very subtle and so well I'm looking forward
to listening to this piece again. So so do so And you just heard, what was it,
Xavier? The Caprice by Labar, played by the great harpist, Nicanor Zavaletta.
And you and I are here on the stage of the City Recital Hall, Angel Place in
Sydney. This is almost becoming like your second home. It seems every year
you're out here. Almost. I was here two years ago. And the amazing thing with
the Brandenburg is that we can play six times in the same hall, which is very
rare. It shows how much you're loved. I mean, the Brandenburg is loved, and I'm
happy that I was accepted by the audience here in Australia. And I loved this
experience so much that I couldn't wait to be back. So when Paul offered me to
come again, it's not easy because, of course, when you come to Australia, you
need to find like three weeks in your schedule. But well, it was the only time I
could make, and I was happy. It was in your summer. And thank you for putting
out a piano for me. That was so kind, so hospitable of you. And you've brought
your harp out onto the stage. Is this the one you travel with all the time, or
do you have a few? No, unfortunately not. I mean, I had three harps in different
places, and we tried to ship the harp, which is the closest to the venue I
perform. But when I travel overseas, I usually borrow a harp from a colleague or
from an orchestra, literally like the pianists. And I always ask for the same
brand. So in my case, I play on American harps, which are built in Chicago. And
we have to adapt pretty quickly to the instrument. Savi, tell me about
auditioning for and becoming a principal in the Vienna Philharmonic. It was my
dream as a child, because I was watching every year of a New Year concert. And I
was thinking one day I'd be there, and everybody was laughing. And so this is
what Zabaleta had sort of predicted for you. Yes, exactly. Yeah. And you have to
imagine that there are very few positions. I mean, at the Vienna Philharmonic,
you only have two harpists. And usually you only have one harp position per
orchestra. So when you are a student, you know you won't have so many chances. I
mean, like there are maybe two or three positions in Europe per year, and the
competition is fierce. So do you remember the audition? Was it nerve-racking? Or
was it a breeze? Well, you know, when I was already the harpist with the
Bavarian Radio Orchestra in Munich, and actually Lauren Mazel already spoke with
the Vienna Philharmonic and told him, OK, I know the guy you're looking for. But
still, I had the audition. But I had quite a lot of experience. And I remember,
like, because you have to play in front of a screen, you don't see, the people
don't see you. It's like an anonymous procedure. And of course, when I got the
job, I was the luckiest guy in the world. Tell me about playing Strauss with the
orchestra and with conductors like Riccardo Mutti. I think he's one of your
favorites. Yeah, Riccardo Mutti, he's one of my idols. He supported me a lot.
After hearing just a few chords, he just talked to the whole orchestra in Vienna
and said, I've never heard such a harp sound before, which of course made me
very happy and gave me a very good stand in the orchestra. And then he offered
me to conduct a solo concerto in Paris. And this was my big debut in Paris with
the Orchestre National de France. And how was it being in your first New Year's
concert, the first one you ever did? Quite exciting. I mean, I can remember the
night before, like putting so many wake-up calls because I was so afraid of
being late because this is like the concert which is broadcasted in so many
countries. And of course, playing this repertoire, all these Strauss, Polkars
and Waltzes, I think no other orchestra in the world plays this repertoire with
this style. Yes. And you heard the Vienna Philharmonic under Riccardo Mutti and
Joseph Strauss's The Dragonfly. I don't think we heard Xavier de Maistre in the
orchestra then. Did we hear you there? No, because I was playing every second
year and this was my year off. So tell me why you had a certain sense of even
disappointment somehow after a while being in this orchestra. There was
something missing, wasn't there? Yes, especially because you know as a musician
you enter the orchestra. I was 24 when I got the job and then after a few years
I was thinking, oh that was it and there I couldn't imagine doing the same thing
for the next 40 years. And also as a harpist you have to imagine that you spent
90% of your time just waiting and counting bars and I'm not a patient person so
I realised that... Counting bars is a nightmare for me. It's just a nightmare.
And also when you play a long opera and you have sometimes 20 minutes without
playing a note and sometimes you wait the whole rehearsal and you don't play. So
you're freezing. And maybe does the instrument ever go out of tune while you're
waiting? Well I mean colleagues would say that the harpist spends half of the
time tuning and the other half playing out of tune. So of course like the harp
you spend a lot of more time tuning maybe than actually playing in the
orchestra. But it's an important colour and I learnt a lot because I was working
with the greatest conductors and also you have to know that as a member of the
Vienna Philharmonic you play in the Vienna State Opera and I love opera. The
opera repertoire for harp is rich. Yeah it's really interesting. It's like
Puccini and Wagner and Schwarz you have the most interesting parts. What were
some of your favourite solos or accompaniments in the... I think Boheme is the
most interesting part because you play all the time beautiful things we always
like coming through and this has always been also my favourite opera. It must be
so wonderful being so close to the great singers and learning about phrasing and
things like that. Yeah, because in Vienna I was like in the pits the harp is
very high so I had contact with the whole stage and the voice has always been my
favourite instrument and to see how the great singers shape a phrase, how they
breathe, how they do a beautiful legato, I learnt actually I think the most from
the singers. If I do this... What does that bring up for you Xavier? Well my
favourite composer Debussy and it brings also a lot of my childhood back because
my grandmother used to play the piano and Debussy was her favourite composer so
every summer she would start the day with a moonshine, Claire de Lune and this
is always a piece I was dreaming of playing and when I signed my recording
contract with Sony I immediately said my first album will be dedicated to
Debussy. I've got a quote here Xavier where you've said as far as Debussy's
piano music is concerned one has the impression that the very existence of the
hammer mechanically striking the strings should, as it were, be negated. Now I'm
translating that into language I can understand which is that you have this
beautiful sonority that he creates especially in this very ugly instrument,
let's face it, it's a barbaric instrument. Well this is one of the only
advantages we have in the harp is that we don't have hammers in between that we
can really shape the sound with our fingers so which gives us maybe more
flexibility and more possibility for colours and especially the ones you need in
Debussy which where you should avoid any verticality and I think the sound
should come from the air. Some of the preludes are so beautiful aren't they I'm
thinking of, I wonder if we could play that together what do you think is our
chance of doing that together at the end of the hour? Yeah we definitely should
try. Can we try a little bit of it? Yeah of course. A little bit of it? So,
Vier, was that one of the pieces that you stole from the piano so that you could
play yourself? Yes, one of the many pieces I stole. Did you steal the arabesques
as well? I did because I think for the, especially for the first one, I'm really
convinced that it sounds better on the harp than on the piano. And actually, you
should know that Debussy motivated the harpist Pierre Jamais to play this
repertoire on the harp. It really encouraged him, so I think we had the
authorizations. So I'm very happy now to present my recording of the two
arabesques by Claude Debussy. Thank you. And that was the Arabesques 1 and 2 of
Debussy performed by Xavier de Maistre and he would have you believe it's much
better on the harp. You pretty much convinced me actually. Thank you. Now
another quote from you Xavier, you've said... I think I shouldn't say too much.
You shouldn't say so much because I've gathered it from all over the place. Your
advice to younger musicians is to forget your instrument. First of all what does
that mean? And you've also said that you want people to be leaving the concert
and bringing the emotions home with them. Yeah because I don't see the harp as a
goal in itself. The harp is an instrument and it's there to achieve colours,
give emotions to the audience. So of course I love the harp and I think it's a
beautiful instrument but I want to the people to forget it and the most
beautiful compliment you can make is like, oh you know the mini conductors told
me I didn't know it was possible to sing on the harp and you sound like a whole
orchestra. I feel this is what we all try to achieve is to sing on our
instrument. This is the reason why I like to accompany voices. Diana Domra has
been a regular partner of yours hasn't she? Yes I met Diana when I was playing
at the Salzburg Festival and she wasn't so famous at the time and she was
covering Natalie D'Ossay and so she was singing backstage the voice of, how do
you say him, from the sky in Verdi. Don Carlos. Don Carlos and I was playing the
harp backstage and then we met and we became friends and she loves the French
repertoire and so we started to think okay maybe we could do a programme
together and we're starting to put ten pieces together and very soon after that
she became one of the greatest sopranos of our times. So you're saying that you
launched her career is what you're saying? I was going to say we met before this
big turmoil because now if you want to book Diana you have to be five years
before and so the schedule is completely full so I was lucky enough to meet her
before and we became friends and we love to do music together and we've been
touring for the last ten years. I think every second or third year we have a new
programme and we learn a lot from each other and this is quite unique because I
think each concert is a special musical journey. Xavier you've said that the
most beautiful instrument for you is the voice and we're going to hear Diana
Dameril now singing Strauss and this is I think Xavier it's the very last song
Malvin. Exactly from the four last songs and she sings this time not with me but
with Helmut Deutsch on the piano. Oh All in one, Luftlos und ummedes Pormors
Glut, Wie ein verweiltes, lazes Gesicht Unter dem golden, emmischen Licht. Und
dann verweht, Leise im Wind, sehntliche Blüten, Sommers Gesang. Sommers Gesang.
Helmut Deutsch. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.
Sommers Gesang. Sommers Gesang. Sommers Gesang. Sommers Gesang.