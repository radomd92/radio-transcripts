TRANSCRIPTION: ABC-RN_AUDIO/2023-05-09-20H00M.MP3
--------------------------------------------------------------------------------
 Arden telling the story of his own great grandfather, James Arden. That's the
music show after the news. ABC News with David Rowlands. The Federal Budget
forecasts that real wages won't grow until next year and inflation will remain
uncomfortably high for Australian households. With the details, here's political
reporter, Cayman Goch. Real wages continue to decline but are forecast to turn
around early next year and grow by 0.75 per cent. Treasury warns bringing
inflation down remains the Government's biggest challenge, but it's expected to
fall to 3.25 per cent next financial year. High inflation paired with rising
interest rates means more Australians will continue to struggle with the cost of
living this year. Treasurer Jim Chalmers. We face a global economy that is
slowing and an outlook that is more uncertain with more downside risks. There
has been a $126 billion reduction in forecast deficits over five years due to
high commodity prices and increased tax revenue. The Government has announced a
$14.6 billion cost of living relief package to help combat the challenging
economic circumstances ahead, with unemployment expected to rise to 4.25 per
cent over the next financial year. And the Federal Government is raising income
support payments as part of a tranche of targeted cost of living measures
included in this year's Budget. Political reporter Dana Morse explains. Over a
million Australians will benefit from a $40 a fortnight boost to JobSeeker,
AusStudy and Youth Allowance. Older Australians relying on income support have
been recognised, with the Government dropping the threshold for the existing
higher rate of JobSeeker for the over 60s to those over 55. Renters on welfare
support will also gain a boost, with the Government increasing the maximum rate
of Commonwealth rent assistance by 15 per cent. Changes have also been flagged
to help curb the cost of the National Disability Insurance Scheme, including
almost $430 million over the forward estimates to improve the scheme's
efficiency through its administrative body, the NDIA. Military officials believe
the changes will save $59 billion over the next decade, but the Budget papers
don't spell out exactly where the savings will be made. There's also a $5.7
billion package aimed at strengthening Medicare and re-incentivising bulk
billing at GPs. More than half the spend will go towards making bulk billing
more available for patients under 16, pensioners and concession card holders.
New line items will also be added to the Medicare benefit schedule for longer
consultations targeted towards those with chronic and complex conditions.
Finance Minister Katie Gallowhurst says the announcement will help cover costs
for many families. For parents with children under the age of 16, that's an
important cost. Anyone who's had kids and had to take them to the doctors knows
that, and often when one kid gets sick the rest go down as well. To other news,
that administrators have been appointed to try to find a new buyer for Jenny
Craig Australia and New Zealand. News reporters were informed yesterday that FTI
Consulting had been brought in as voluntary administrators of the weight loss
company a week after the US business filed for bankruptcy. Founded in Melbourne
in 1983, Jenny Craig quickly expanded globally by providing pre-prepared meals,
snacks and weight loss coaching. However, dietitians Australia President Tara
DiVersi says the company has been unable to keep up with modern trends. Some of
those types of systems like your dieting systems that had that one style fits
everybody approach really just doesn't work for today's consumer or what people
want with making sure they can have that level of personalisation. Protests have
erupted in several Pakistani cities after the former Prime Minister Imran Khan
was arrested. Police used water cannon and tear gas against Mr Khan's supporters
in Lahore and Karachi. Earlier, dramatic video footage showed dozens of
paramilitary troops whisking Mr Khan away from the court complex in the capital
Islamabad. In a video message filmed before his arrest, Mr Khan said he's ready
for what lies ahead. If someone has warrants to arrest me, then come to me with
warrants. My lawyers will be there. I am ready to go to jail myself. Making a
scene would be wasting resources as if the country's big criminal was coming.
Japan says it's working towards opening a NATO liaison office in the capital
Tokyo. The Japanese ambassador to the US has confirmed reports that there are
proposals about opening the first Asian bureau for the US-led alliance. China
has responded to the report saying it needs to exercise vigilance in the face of
what it calls NATO's eastward expansion. The reports haven't been confirmed by
NATO. And the conviction of an American sports doctor for supplying performance
enhancing drugs to Olympic athletes has been hailed as a landmark moment for
international sport. Eric Learer faces up to 10 years in prison after being
found to have supplied drugs to a Nigerian sprinter. ABC News. Welcome to the
music show. I'm Robbie Buck. That's So Young from the album Jammu Dreaming and a
warning that this interview uses the names of First Nations people who've died.
This is from Archie Roach's second studio album Jammu Dreaming and is the only
track on the album not written by Archie. It was actually written by and
performed with Gugatha and Gundijmara musician David Arden. David joins us later
in the show to talk about his latest project which tells the story of his
family, tracing his own story from his great grandfather James Arden. Do you
Good Hiding seek for little children Trying to find me So young And so full of
life So young And so full of life So young And so full of life So young First to
composer Andre Greenwell. With the impending world premiere of her opera Three
Marys, Andre brings us into the world of myth, music and yes, marys. Before we
chat to Andre, let's listen to some music from an earlier work called Laquium,
Tales from the Morning of the Lackwomen. Happily it'll ye, you might die with a
nice kiss like this. O'er the land of the free and the home of the brave Thou
art exalted, saviour, thee, pandula. That's Persephone Sleeps Amidst the
Softened Blossoms from Laquium, Tales from the Mourning of the Lackwomen by
Andre Greenwell, who is my guest on the music show. Hello. Hi, Robbie. Thanks so
much for having me. Now that goes back a few years for you. That was a short
film that you wrote the music for and directed. Well, yes. To begin with, it was
a bit like a contemporary oratorio meets concert project that was in fact the
first work of mine that was presented at Sydney Opera House. And I think it was
all the way back in 1998. Then I went on to make a short film from excerpts from
that project, Laquium. So let's move forward because you're heading back to the
Opera House. Yes. And that's for a brand new opera called Three Marys. And the
reason that we can't hear that just yet is it hasn't premiered. So we don't have
any of it. It's all in the making. So where are we heading with this new opera
compared to what we heard just there? Three Marys is it is much more a dramatic
work. I've worked with many different kinds of singers in the projects that I
create from popular music, rock, experimental, classical concert. But this work,
it's more of a contemporary opera for sure. We're telling a story and the
instrumentation would be a bit bigger. In fact, it's definitely my most
ambitious work to date. Is it? Yeah. There's an ensemble of eight players,
flute, violin, cello, double bass, tuba, wood, electronic keyboards. There's a
sound design element as well. And we also have a choir of 16 singers coming from
the New South Wales Arts Unit and a cast of four. So it's quite a number of
people on stage for an independent work. A lot of moving parts. Yes, a lot of
moving parts. What about the story of the Three Marys? Look, I had an
opportunity to develop a chamber opera through a workshop at Opera Queensland.
And it was really important to me to find a subject matter that's appropriate to
the form. And also working into that form, I know I'm going to be living with
the subject material for a very long period of time. So I did quite a bit of
research. I can't explain it. It's almost like the story found me. But there's a
beautiful medieval myth that the three biblical Marys sailed in a boat all the
way from the Middle East to the south of France, where it said that Mary
Magdalene wrote a gospel. And in fact, the Romany people gather in the south of
France at Saint-Maurier-de-la-Mer or Saint Mary's of the Sea, a little town, and
they venerate Saint Sarah as Carly Madonna, the Black Madonna. In our story,
it's almost like our own version of the myth and that there are many accounts of
how many people were in the boat. But I found one version that seemed quite
useful with the idea that Mary, the mother of Jesus, Mary Magdalene, and the
daughter of Mary Magdalene were in that boat. So we're calling her Sarah Marie.
And the exploration is an intergenerational experience of these women in exile,
travelling across waters. So why that particular story? Well, I was raised
Catholic and surrounded by silent women on the walls in school and on pedestals
and in the biblical text. Those women rarely speak. And going back to that
question about what's appropriate for opera, I could see that would be a really
terrific vehicle to give voice to those women in literal and metaphoric ways
through the form, through the sung voice. And surviving at Sea, which is at the
core of this work, it's an age old theme, but it obviously has modern resonance
too. That's another aspect to the work. I would say we're not presenting a
religious perspective at all. It's reviewing a myth with contemporary resonance.
I do recall growing up, there was one priest, I asked the questions of how
Jewish are we? And this idea of shared history, of shared stories. So I was kind
of interested in this cultural meeting place somehow in our telling of this
story. And in fact, in the choir, we have a featured vocalist who sings in
Arabic at times, and the choir sing mostly in English, but with French at times.
I wanted to introduce, I suppose, an approximate geography about the narrative.
It's not authentic, because we're telling a myth, but it's to sort of energise
these contemporary resonances. You know, people are still travelling so much,
the world over and to our own country, in need of better lives and protection.
There's a very interesting character that the librettist Christine Evans, who's
a magnificent writer, has introduced to that end, I think to really spark off
the contemporary resonances. And his name is Mazug, and he's inspired by a real
fisherman, Champs-Édine Mazug, who until recent times was burying and caring for
the bodies of refugees who had washed up on the shores of Tunisia. So in the
opera, without revealing too much, you know, there is sadness and grief richly
expressed in our work. Well, it is an opera. It is an opera. Yeah. Can I ask you
about your writing for an opera like this? And let's talk about the vocal
writing first and the cleanness of your vocal writing, which you could consider
quite modern, but it has sort of almost plain chant quality to it. And I wonder
what your thoughts are about how you write for vocal. It's not fixed for me, but
you're absolutely right. Those references are there. I think I draw on many
stylistic approaches depending on the narrative purpose. But plain chant is
really important to me, you know, composers such as Hildegard. I would also say
the very early opera of Monteverdi. You'll hear that in it. But then at times,
you know, there's a song that in an operatic way, I feel like it's Jacques
Brelnik cave in it. You know, there are also people that I've listened to a lot
in my work and there's moments where I also here. Ravel in my head. So there's
many things going on there. It's very much driven by voice. Certainly in this
work, I've shaped the music structure around and I really want to allow space
for the writer's intention to be heard, not to be too clever as a composer. It
is very rich in its vocal expression and vocal textures and harmonic design.
Yes, that's true. Do you write the vocal lines first? I'm, you know, I'm a bit
of a closet singer. I love to sing. I'm not trained really. And I work with a
computer as most people do these days. But for this process, I just sat at my
piano with an old fashioned piece of manuscript and a pencil. And I'd map out
the architecture of songs from the libretto and then I'd take it back into
software and, you know, tease it out from there. And then I would have
conversations with Christine and our dramaturge, Angela Taplin, you know, about
things that I might want to expand in certain pieces. Yeah, it's actually come
from the old fashioned way and teased out. André Greenwell is with us today on
the music show and we're talking about her new opera about to be premiered
called Three Marys. You mentioned Christine Evans, who's the librettist and who
is based in the US, which I guess in this post-COVID world that's perhaps not as
unusual as it might have been in the past. Look, I think we started the project
in the middle of 2019. Well, Christine came onto the project officially then we
raced towards this workshop at the end of 2019 at Opera Queensland. Then, you
know, it was everyone was so excited and then the pandemic happened and the wind
was really taken out of our sails. And I thought I had, you know, this project
with such magnificent potential and we had no idea where it was all going to go.
So the focus on the libretto was pandemic enforced. I think it was a gift to us,
you know, with Christine in the US, Angela as dramaturg in Queensland, myself in
Sydney. Those meetings almost got me through the pandemic. You know, it was a
long period. Is it fair to call her a more narrative driven writer? Ooh, what
would Christine think of that? Yeah, well, the reason I ask is that does that
then inform the way that you write the music to it? Yeah, definitely. I've
worked with a lot of poets, but I discovered along the way for this project that
I really needed a dramatist, someone who's really on top of managing a lot of
dramatic themes. It's quite complex planning out that happens before, you know,
you really get into doing the music in a serious way. Saying that, you know,
Christine is a well-known dramatist, but she writes novels often in quite a
poetic language. And there's a lot of poetry in her style and she can be at once
beautiful and acerbic, which is great fun for me. In the past, you've done
musicals that have been described as retro indie post-punk with the hanging of
Gene Lee, with singers who are, you know, more rock and roll singers, for
example. Is the choice of opera for it, is it dictated by those other factors
we've talked about here? I think so very much. Personally, I think the idea is
working quite beautifully in this. It's almost like it feels a bit contemporary,
early opera to me. I feel that quite strongly in its realisation. It's very
intimate as well as highly emotional. You've written a lot of music for screen
as well as for the stage. As a composer, do you tackle those different outcomes
in different ways? I do feel the headspace of screen is quite different. It has
a lot to do with awareness of being part of a score and that means sound design.
And so I think there's differences, say in theatre where the resources are
different, that music might be doing a lot in a theatre score and with less
resources, whereas screen, the music is, it's part of a space that you're
looking into. Does that mean you can be more sparse when writing for the screen?
Oh yeah, I think so definitely. And in screen, you know, there's often not that
much music. Well, in this case in opera, it's, you know, the music is the text.
Well, with that in mind, let's perhaps take a listen to Orpheus' song from
Gothic, which marries those worlds together. Orpheus' song is from a project,
sort of an interdisciplinary concert project I made called Gothic. And I thought
it would be really fun to make a work of different songs that explored different
expressions of Gothic from medieval to contemporary times. I used a number of
existing texts in that project, but I also commissioned a few contemporary
writers. And one of them is the amazing writer, poet, critic, Alison Croggan.
Running before me, running behind me. And, you know, the Orpheus' story is very
well known for its Gothic tropes. And I was also thinking about the resonances
of Jean Cocteau's film in making this project in Gothic. There was also sound
design. I should also say I went to film school for a little while and studied
sound design, and that's something that, you know, creeps into my work. So
sometimes there'll be things drawing from screen practice in a live performance
space. Andre Greenwell, thank you for joining us on the music show. We look
forward to seeing and, more importantly, hearing Three Marys being premiered at
the Sydney Opera House. But in the meantime, let's have a listen to Orpheus'
song from Gothic. Thank you. Thank you, Robbie. And as she walked behind me, her
steps as light as rain, her grave clothes scrambling round her, stumbling on the
path like a woman in a dream, her blood waking in her bruised flesh, and it hurt
her, and she moaned, and I heard her, and she moaned, and she was crying. I
fixed my gaze forward on the waves as my longing reached behind me, how I longed
to comfort her, how I longed to hold her close. At last I saw the daylight world
right before me like a blessing, and in my joy I turned to speak, in my joy I
looked behind me. And as she walked behind me, her steps as light as rain, her
grave clothes scrambling round her, standing on the path like a woman in a
dream, the blood pooled in her bruised flesh, and I stood alone. Orpheus' song
from Gothic by composer Andre Greenwell to words by Alison Croghan and sung
there by soprano Julia County. And Andre's Three Marys, premieres at the Sydney
Opera House, details on the website. You're listening to the Music Show.........
Free Music 1 by The Free Music, a group led by Libyan composer and producer
Najib Al-Houche. Their music fuses disco, reggae and funk to create their own
distinctive sound. The Free Music self-financed and released ten albums in the
70s and 80s, but their success wasn't felt much outside Libya during the Gaddafi
regime. But now a German label called Habibi Funk have collected some of the
Free Music's best known songs and reissued them in a compilation called Free
Music Part 1. We'll let you know if Part 2 arrives. You're listening to the
Music Show. In the language of Boonarung and Woiwurrung, Yirrumbuoi means
tomorrow, and it's the name of the festival that showcases the voices and
stories of local First Nations artists through art, theatre, music, dance,
spoken word and interactive events. One of these local artists is David Arden.
Over the last 40 years he's worked with plenty of names you'd recognise,
including Archie Roach and Ruby Hunter. This year David's released a new album
called Mirta, The Ballad of James Arden, which tells the story of his great
grandfather who fought for the rights and freedom of the Gunjjmara people. Music
has always been the way David's delivered messages and told stories, and we're
going to start today by hearing from a much younger David Arden. He's the front
man of Koori Youth Band and a song from 1986 called Our Future. This is all our
originals in Australia. Our future is dedicated to them. Our future. Tell me now
Can nobody know the concept And where from here now And where we go Nobody knows
Our future Our future The Koori Youth Band and a song called Our Future, which
is taken from a 1986 EP recorded as part of the Rock Against Racism concert. And
the front man of that band is David Arden. Welcome to the music show, David. Ah,
hello Rob. Hey. Take us back to that band and to a very young David Arden. You
were still a teenager back then. Yeah, yeah, well, I was 18. Basically my father
was part of the Charcoal Lane, living in Fitzroy in Collywood, but basically in
Fitzroy and in the park there. And we went to a youth centre. It was called
Fitzroy Community Youth Centre. And there was a whole lot of street kids. And
one was my brother. This is Wally. Yeah, this is Wally Arden. Wow. And Earl Mao
and Luke Mao and a beautiful Pacific Islander. And there was Jimmy Rees,
nicknamed Tippi. And just prior to that Rock Against Racism concert that we
heard you performing there, am I right, were you in Alice Springs in 1985 on
that massive occasion when Uluru was handed back to the traditional owners?
Yeah, we were. And you were involved in music at that stage too? Yeah, I was
involved in music. Well, it was in a band called Hard Times and they come from
Chasm. Great older brothers like Peter Roddimer, Ian Johnson, Bear, Wayne Thorpe
and Chooka, yeah, which was Wayne's brother. And they were in that same music
university as, you know, No Fix the Dress, Us Mob. And then they come back and I
got in playing drums and I learnt drums there and then I went to finish that
learning music there with the older brothers and then I started my own band up.
Before that we were hanging out at FCYC. And then we went, the whole lot of us,
including my wife who was Arenda, part of the Hughes Franey Mob from the
Caterpillar Dreamin' Mob, you know, and there was a fellow there called Greg
Colin and he was one of the youth workers there, being urban blackfellas,
learning about, you know, the desert blackfellas. And we went on that quest and,
you know, he took us up there and we started understanding the hand and back of
Uluru, you know. And then from there we heard there's a concert happening in
Alice Springs at Bazzo's Farm. Before then, you know, I was a groupie for No Fix
the Dress. Bart Willoughby and Salman Burns who was the lead guitarist was
Coloured Stones. We grew up and started playing music together and then for a
freaky thing I ran into them. And there, there, Goanna's there, Rumpy Band's
there, and Bart come up to me and said, look, Dave, would you like to play drums
for me on the way we have survived? And I said, well, you know, this 18 year old
young man. Yes, thank you. I would love to, you know. And these were my mentors
in all my life. Thirty years later we found out me and Bart shares the same
fifth grandmother from Gugathur, you know. So, you know, these things happened
for a reason. They took it away. They nearly, you know, made us extinct and the
elders are connecting us in some way or another. Speaking of taking away, you
mentioned Charcoal Lane and of course your involvement with the late, great
Uncle Archie Roach and Aunty Ruby Hunter is a terrific story too. Yeah. So tell
me that story. Tell me about your connection with those two. My father, his
favourite cousin was Lawrence Roach, which was Uncle Archie's brother. And they
grew up on Framlingham. And it was later in life, you know, I think after Jumul
Dreeman and Uncle Archie quoted me a line from one of the Framlingham books that
it was everyone on the mission went to school except two young kids and they
wagged. And, you know, they wagged in the way of walking the land of
Framlingham, of Kerowarran, of the Gundjumara country and their names were
Lawrence Roach and Wally Arden, senior, you know, my father. And the connection
from there, they come down to Fitzroy and they kept together. They stuck
together and they were part of Charcoal Lane. So they were the first founders of
Charcoal Lane and then Uncle Archie come to search and find himself, you know,
and then run into my father because he was always with Uncle Lawrence, you know,
to find his brothers and his sisters. That's how that connection was. And then
like you were saying with Koori Youth, we did a lot of land right rallies and
death and custody rallies, you know, and a lot of funeral funds. And at the same
time Uncle Archie and Aunty Ruby were playing Country Western, playing covers
and we always watched each other and they watched us. Later in life we sort of
got together and said, look, let's get together and join a band together, you
know, and it became Archie Roach and All Together. And you not only played with
Archie but you also wrote for Archie too. Well, wrote a song that Archie
included on his album. Yeah, yeah, Jummo Dreamin', yeah, yeah, which is the
funniest thing, you know, through Uncle Archie's song book, yeah, no one ever
sang their own song or read a song of their own and put it on that album. On his
album I was the only one. So I think that's an honour for me and we share the
same great, great grandfather. So that's how Uncle Lawrence and Dad were very
close, you know, they were family. It's my uncle in Western way but in tribal
way it's my uncle father. Like Aunty Ruby, my Guggenstein side is my mother's
sister. She's my Aunty Mum, you know, and I didn't know that until I started
learning and resimulating myself within my bloodline cultural way. David Arden
is with us today on the music show. Let's talk about the way that music can be
used to heal and tell stories and also tell truth too. You've had a really
interesting arc, as you said, you've been in music for most of your life, pretty
much all your life, but you've also been learning about who you are and where
you've come from as part of that arc and so how do those two things meet? I was
a commissioned flat kid. I come from Collingwood and Fitzroy, you know, and my
Aunty Mum, my father's older sister, brought me up, me and Wally, and, you know,
we went down roads that weren't too good. And then I found music, you know, from
football I found music. I heard a story, David, that you found music after being
a bit naughty and a teacher gave you a guitar. Yeah, yeah, yeah, well that's it.
You know, it was that naughty I ended up going to what they call Parkville now,
which we called it back then, was Beltara and Chirana and I got locked up for a
year, yeah. What started it was my mother was a big fan of Chris Gofferson, you
know, Bobby McGee because her brother was Bobby Arden and her favourite artist
was Patsy Cline, your cheat in the heart. You know, the Love Dummry Wrong songs,
which blackfellas love, you know. You know, we love them Love Dummry Wrong
songs, especially when you have a charge, you know. I bet. Yeah. And they're
healing. Sometimes, you know, they're dark moments but they're healing moments.
So I learnt how to play them two songs and I met this beautiful lady and I tried
to find her and she passed away now, God love her, and she gave me my first
teaching of how to pick a guitar and from there I come out and went back to
Collingwood and there was a youth centre there and I did my first show back in,
I think, back in the 70s, late 70s, you know, and my mum come and sang Your
Cheating Heart. She was shaken. First time I'd ever seen nerves. She was shaken
but she can sing, I tell you. And it just, it blew me out and I think we
connected within that sense and from there that world opened up, you know, and I
ended up finding Bart. With Sal and Burns and Tony Lovett, who are the three of
us, you know, become the groupies of No Fixed Address Countdown, No Fixed
Address. Well you mentioned No Fixed Address and you mentioned Coloured Stone
and there's Wurrumpi Band and there are so many others, yourself included, and I
wonder what you think the role of those musicians, when it comes to explaining
what Australia is, what the role of those songs have been. It's about
acknowledging and it's about re-earthing who we are as First Nation people, you
know, living in this Western Aussie world and, you know, bringing that to the
forefront of not being lost anymore and bringing these things to the forefront
where we can talk about. It's great to see society, you know, these institutions
like the AFL and the Rugby, that plays a part towards acknowledging First
Nation, you know, existent within this country. And I think that's what we tend
to do. For me, and to see what they've done in the past, the older brothers, the
uncles, you know, the Boy and McClouds, the Uncle Jimmy Littles, the Aunty
Audrey, Uncle Jimmy Chai up in Broome and these great storytellers that talk
about their culture and the country that they're from and the connection of
cultures that are the make-up of who we are now. These are the Irish Scottish,
you know. For me, I've got Irish Scottish German English in me, you know, and
they're my ancestors. No matter how it connected, they're still the connection
of me. First and foremost, I'm First Nation, you know, but when I go to them
other countries, then I'm that as well. But that's the make-up of telling that
story and the stories that I've learnt of what they've done to where I am and
trying to find my way of deliverance. Yeah. Well, one of those methods of
delivery was the great era that you spent in the Black Arm Band and you were the
musical director for that group. For those who may not have experienced the
Black Arm Band and I was lucky enough to see you on stage and it was just a
terrific experience, but just give us the background behind the group. I went
there with Kuj Edwards, my jambi, which is my brother-in-law, my wife's brother,
and then I met all the mob that I'd played with over the last 30 years and
Shane, who I'd known since I was 16, me and my wife. This is Shane Howard. The
legendary Goanna man, because he knew that I knew all the histories and all the
songs and, you know, all these artists and I'd performed with him and that, you
know, and the importance of having a music, a First Nation music director in
this great forum, you know, which is called Black Arm Band. He said, Dave, you
should be a music director, you know, with me and I said, well, let's go music
director within that sense. So when we did that, having a chance to go down the
journey with all my uncles and aunts and aunts and aunts my sisters and stays
and nephews was what we met. You know, there was a beautiful man called Stephen
Richardson and the arts hub that paid a big part towards him, you know, helping
us bring this to the forefront of this country and the world, which was a really
good thing. You played all over the Globe. Yeah, we played all over the globe
with it, you know, honour and a pleasure. But the whole concept of addressing
our and dove elections, you know, the endurance of these beautiful songs that,
you know, paved the way and made a mark on this country's country's journey of
acknowledging civic rights and human rights and land rights. Well David you're
still using your music to tell stories of your family and your history and your
place in this country as well and we're going to listen to a track off your most
recent release. This is Call It Out. All of my life I had to fight Against the
things that was not right People can be so unkind When they live their life
around spite Call it out and make that change Cause there's no place for racism
in our lives When do we start to work things out And help us find a way for
common ground Try not to let things cloud your mind On your journey through your
life Call it out and make that change Cause there's no place for racism in our
lives Call it out, out, out We must respect David Arden and Call It Out, our
guest on the music show today. It's a track from his most recent release, Mirta,
the ballad of James Arden and yeah that last name is the same as David's. Give
us the story here David. The story is about my great grandfather, James Arden.
My aunty mums would talk about him so a lot of it was oral when I was a kid and
I was learning as much as I can and listening as much as you can as a young
person you know. It was later in life when I became you know around about 19
towards my 30s I started having an understanding about who he was and the
journey that I took to understand being an urban black fella towards oh hang on
I'm actually I am a traditional person as well between my mother and my father's
side so okay then and resimulating myself you know within that sense of living
in two worlds identifying you know this connection to my Gunditjmara you know my
Gugatha within this case the Gunditjmara side. Kure Warren and where he come
from and he was a man that come from his mother she was Barbara Winters and they
were the keepers and the guardians of Garywood which is the Grampians in
Victoria and on one side and the Harrison's were on the other side and then they
got forcibly removed from there and ended up in Condor and Framingham. For James
his father was Timothy and Barbara and then James lived his life with his wife
and you know he was fighting against the odds you know to be respected as a man
you know as a human. And he was something of an activist as well? Yeah he was an
activist. When I just started doing researches and that I seen that there was an
article in Melbourne here that him and Billy King come down two Aboriginal
tribal men come down to Melbourne and they started doing activism down here for
the treatment of Aboriginal people in missions and they did a tribal dance and
that was in one of the articles down here. So I learnt that and then I learnt
you know he served time in war he was a light horseman so he was a horse breaker
you know back in the days Aboriginal people couldn't serve but they become the
breakers and there was moments where you know there was poison in the water so
he got ill in the army sometimes you've got hard defects or something they stop
you from going to war because you're not fit enough to go to war so that's what
happened to James but he still served within that unit he still registered but
he was horse breaker and then he had conflictions with the missionaries in the
missions and then he had to stand up and fight against them you know because he
was a man with money now and the song tells the story of that. His grandson was
Captain Red Saunders the first enlisted officer. And so David what is META? Well
it's the story it's the story of David Arden it's the story of five generations
back I've written songs within tells the story of them of over them five
generations between the Gunditjmara and as well as the Gugata you know and it
talks about my song line the story is my song line and that was the great thing
when I listened to Aunty Ruby you would hear her song lines you know between her
Pidgin Jara and the Gugata you know of her clan but also on Kalachi between the
Bunjilung and the Gunditjmara but then he would go into another world towards
singing songs about other countries you know other peoples that's what Uncle
Archwood's got. For me it's telling my song lines and enlightening people to my
world. And what does it mean to you to have this work's world premiere at the
Yurrumboy Festival this year? It is an honour, Yurrumboy, to have the chance to
Yurrumboy in a First National you know festival and it's an album launch as well
for us. And you worked with Daniel Harraghi? It's a really honour to be able to
do this beautiful album with this great man who's you know who it's in heaven's
sand he's you know Venezuelan free time Grammy Award winner and played with a
lot of beautiful legendary artists that's lived in this country for about five
years now and now he's working and culturally you know he's a First Nation from
that country but you know we're linking and we have an understanding not just
you know verbally but musically and the product that you hear deliverances that.
Two beautiful you know sisters of mine you know Jill Morgan and Violet Arden you
know my wife you know who's helped me administrate and take this we're working
with you know the VCA, the MCM and the Willian Centre mob down here so you know
which is this beautiful six piece orchestra that's going to work with us you
know. Well let's have a listen to another track from Myrta this is the Ballad of
James Arden and David Arden thanks for joining us on the music show. Thank you
very much Rob thanks for having me. James Arden was a tribal man from the
Gunditjmara Nation. He came from Garywood where his bloodline began. Where the
Gunditjmara people lived off the land. He's law, he's dreaming and his culture
rules. Myrta, Myrta, Myrta, We're your Guarantor. James was 43 years old and a
father of six. He joined the military in the First World War to fight for
freedom, to become his own man. James worked as a horse breaker for the light
horse picking. Mead top, mead top, wing a wong a. Mead top, mead top, wing a gar
a top. In 1916, against all odds, against the missionaries and the station
manager who is to control every movement of the Gunditbari clan, James made a
stand to fight for what is right. Mead top, mead top, wing a wong a. Mead top,
wing a gar a top. The title track from David Arden's Mead-a, Rise Up, the ballad
of James Arden. It's also being premiered at the Arts Centre Melbourne as part
of the Year and Boy Festival and details are on the music show website. The
music show is produced by C. Benedict and Kezia Yap. Technical production by
Russell Stapleton and Brendan O'Neill on Gadigal land and Wurundjeri Woi Wurrung
country. James lived out his life with his wife Christine Arsten. Now his tribe
will survive on the land where he was born. Mead top, mead top, wing a wong a.
Mead top, mead top, wing a gar a top. Next time, English folk legend Shirley
Collins is with us to talk about Archangel Hill, the third album she's released
since her re-emergence from a 36 year hiatus back in 2016. And Carina Canellakis
on the career of a modern orchestra conductor and the release of her new album
with the Netherlands Radio Philharmonic Orchestra. Hi, Patricia Carvellas from
RN Breakfast. And I'm Andy Park from RN Drive. It's budget time. This is a
responsible budget that is right for the times and readies us for the future. So
what does this mean for you? Both Andy and I are in Canberra to drill down into
the details. Making sense for the numbers to find out what this budget changes
for the country and your family. So catch the newsmakers on Brekkie from 6am.
And from 4pm with the best analysis on Drive. All this week here on ABC RN.
What's it like to be conned? You feel a little dirty?