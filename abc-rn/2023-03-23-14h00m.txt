TRANSCRIPTION: ABC-RN_AUDIO/2023-03-23-14H00M.MP3
--------------------------------------------------------------------------------
 or response. It's vaguely licorice sort of phenyl taste. The great
unretirement. Analyzing the data. Just hang on tight mate. Stay informed and be
entertained. ABC RN. ABC News with Tom Melville. The Prime Minister Anthony
Albanese has revealed the wording of a referendum that proposes enshrining an
Indigenous voice to parliament in the constitution. Talia Roy reports from
Parliament House in Canberra. The question that will be put to Australians later
this year is, a proposed law to alter the constitution to recognise the first
peoples of Australia by establishing an Aboriginal and Torres Strait Islander
voice. Do you approve this proposed alteration? Mr Albanese became emotional as
he sought support for the change. All of us can own an equal share of what I
believe will be an inspiring and unifying Australian moment. However, opposition
leader Peter Dutton says he still has reservations. How will it deliver
practical outcomes for Indigenous Australians? And if you can't provide that
detail, then he needs to explain why. Mr Dutton says the Liberal Party will meet
to discuss its position. Independent Senator Lydia Thorpe has been thrown to the
ground by police after trying to interrupt an anti-trans rights protest on the
lawns of Parliament House in Canberra. Peter Lustead reports. Senator Thorpe
yelled, you are not welcome as she approached UK anti-trans rights activist
Kelly J. Keen-Minschel, who was giving a speech to her supporters. Police
blocked the Senator before pushing her to the ground. She then got up and walked
away. The Australian Federal Police say they are aware of the incident and it
has been referred to the AFP's Professional Standards Command. Around 50 people
attended the protest, as did 400 trans rights supporters 30 metres away. Ms
Keen-Minschel's speech at the Victorian Parliament on Saturday also caused
controversy with 30 men performing Nazi salutes. New South Wales Treasurer Matt
Keen says he will always stand up to bigotry when asked if his government would
ban the Nazi salute. An anti-trans rights rally in Melbourne on Saturday was
addressed by a British activist and was also attended by a group of neo-Nazis
who raised their arms in the Nazi salute, sparking outrage. The Victorian
government says it may consider amending its laws regarding hate symbols. Asked
on Triple J's hack program if the New South Wales government would ban the Nazi
salute, Mr Keen says there is no place for intolerance anywhere. I have been
very outspoken on the need to be inclusive and respectful and to stamp out
intolerance, hatred and bigotry across our community and I will always be a
strong voice for those things. A marine ecologist says conservation efforts need
to be strengthened as a new study reveals strong population declines in reefs
around Australia. A team of researchers has found more than half of the 1,000
reef species it monitored have seen their populations decline over the past
decade. The University of Tasmania's Professor Graham Edgar is the lead author.
He says stresses on marine life need to be limited. In general to try and reduce
global warming but secondly we need to look at those other stresses in the
system, the pollution and the fishing pressure and those and reduce the local
stresses which are all compounding to cause population declines of many species.
Large numbers heading overseas now where large numbers of people are again
taking to the streets in France on another day of nationwide protests and
strikes against legislation to raise the retirement age. The BBC's Hugh
Schofield reports. This ninth day of protests since January against the pension
reform is starting off much like the others with many schools and universities
shut and major disruption to public transport. The difference is that after
President Macron's decision to force the pension bill through the National
Assembly without a vote on the text, the atmosphere is much more tense. In the
morning protesters briefly blocked the main access road to Charles de Gaulle
Airport in Paris and others have invaded the tracks at the Garder-Lyon train
station. Police are on maximum alert because of fears that the day's
demonstrations could turn violent. The Bank of England has announced an 11th
consecutive interest rate increase despite concerns about troubles in the global
financial system. Britain's central bank has boosted its key rate by a quarter
of a percentage point to 2.25%, a day after the US Federal Reserve approved a
similar move to tame inflation that's slowing economic growth. Many analysts had
expected rates would be kept on hold following the recent collapse of two US
banks and turmoil at Switzerland's Credit Suisse. And to sport, where in the
NRL, Parramatta has exacted revenge over Penrith with a thrilling golden point
win in the grand final rematch. Fresh off signing a new long-term deal for the
Eels, Mitchell Moses slotted a field goal in extra time to give aside a 17-16
win. And in the AFL, Carlton have withstood a last quarter Geelong fight back to
post a memorable eight point win at the MCG. 90 points to 82, five points, five
goals to Charlie Kerneau led the way for the Blues. You're up to date with the
latest ABC News. This is Duet on RN with Tamara Anna Czyszlowska. Today I'm
joined by a renowned performer and composer of the king of all instruments. He
was born in Cairo and raised in Australia and he's become synonymous with an
instrument as ancient as it is beautiful, evocative and as he's proven time and
time again, incredibly versatile. Joseph Tewatros, welcome to Duet. Thank you
for having me. It's so exciting to be here. Do you like that intro? What an
intro. Synonymous. I love that. I love being synonymous with things. I wish
people could see what you're wearing today. Yeah, I'm not synonymous with
anything else except the oud. Because I'm audio, I'm sound to most people. So
the people that know the oud and the oud in Australia, I guess I've been lucky
to try and forward it in some way in this great land. You've said that the oud
is like your refuge, your comfort, your true friend. Yeah, because have you seen
my friends? I'd rather go and play an instrument. That's how bad it's become
over the years. But no, look, I think it's very important. I mean, there's a
definite nurturing of the spirit and I think that's something important that we
have to celebrate in a way. And we've been very lucky as musicians to have that
option and to know the benefits of music nurturing the soul. And I think and
hope and pray that youngsters coming up see the importance of music and how good
it is for them in all sorts of areas. Permission to evaporate. Yes. Tell me
about that. That's what we're going to hear first. Well, I mean, it's a very I
mean, the title sounds quite interesting, but yeah, it's one of great depth,
actually. I wrote permission to evaporate in a tough time when my parents had
both passed away. My mother passed away in 2012 and my father a year later. And
so it was a tough time and it's one of confusion. And that is a perfect time
where the oud and music were, you know, refuge and somewhere to take some sort
of comfort. And that was permission to evaporate by Joseph Tawadros. And you
also heard Christian McBride, James Tawadros and Matt McMahon. And that was your
that was your bro, Joseph. Yeah, that was James. James plays percussion. He
plays all sorts of Middle Eastern percussion. On that recording, he was playing
the frame drum, which is called the bendir. Lots of people in your family have
played musical instruments. We all dabble. We love music. My family have always
loved music and, you know, we're brought up on it. So yeah, it was only natural
that, you know, other players in the family played it. But James is actually a
biomedical engineer. So he's he's got a proper job, apparently. And he comes and
plays, you know, frame drums when he can. Your uncle played trumpet. He played
trumpet. Well done, Tamara. This is this is amazing. Extensive. Oh, man, I can't
believe that you've gone into this. This is this is kind of like ASIO. Yeah, he
played trumpet. And then I yeah, I really wanted to play the trumpet, but my
family said, no, it's not a great idea. They were anti trumpet. And so I saw the
Oud and I really loved it. And they said, OK, Oud sounds all right. And it was a
link to my heritage and I was very keen on it. And I love it. I still love it
now. Didn't your grandfather play as well? Well, I never met him. But yeah, I've
got photos of him playing and he died in 1957. But I have photos from the 1930s
with his band playing the Oud. And it's a really inspiring thing to see as a
kid. And it was something that really made me want to learn the instrument,
learn more about the culture and the history of the place where I was born. So
I'm a big fan of traditional music, traditional Arabic music. And that was the
music that I was brought up with. My mother used to record these radio shows
when she was in Egypt to learn the old repertoire. So I had this big library of
cassette tapes, which I still have. And I listened to and learned some of the
old repertoire. And that was my journey with the instrument. And so my parents
were very big influences on it and I had many traditional influences. And that's
what I was brought up on. I still listen to it today. There was a singer that
was a particular influence on you. Yeah, Umuk Al-Sum. I mean, there's really two
or three massive singers in the Middle East. One is Umuk Al-Sum, the biggest
diva ever known in the Middle East. She's just as popular now as she was back
then. She died in 1971. She had a very popular career in the 20s and 30s. But at
the same time, there was also Abdel Wahab, who was a very big composer and
singer. And the movie industry was really, it was the golden age of Egyptian
cinema. And all these singers and composers became movie stars. And the third
one was a little bit more modern and introduced electric guitar and accordion.
It was more popular. It was Abdel Halim. So between those three, you have most
of Egypt's popular music through that whole period. And so Umuk Al-Sum and Abdel
Wahab were rivals. In the 60s, they joined forces because of the president
suggesting it. And so then they created an amazing era of these songs where
Abdel Wahab composed for her. But she has a, the amazing thing about the golden
age was that they have the most amazing lyricists and composers. And most of the
music that's played on the oud or on the Arabic instruments are from that
period. What about piano? Did piano enter your life at any point? Well, I love
piano, actually. Piano is a great instrument. And I always find that the oud is
the king of all instruments in the Middle East. But the piano is really up
there. And the parallel is that piano is the instrument of choice for composers.
And the oud is in the West. And oud is the instrument of choice for composers in
the East. So they're very similar in that sense. But they couldn't be further
apart in the way they are. You've got black and white keys that can't play
quarter tones. And whereas the oud is a fretless fingerboard that shares a lot
of quarter tone music and scales with quarter tone. So when playing with piano
players, you always try and find a happy medium between the both and the right
sort of intonation so it's not kind of all over the shop. So I shouldn't have
tuned the piano. Well, I tuned it to quarter tones earlier. You didn't realize.
I just got in with my mallet. I haven't played it yet. Rachmaninoff. Yeah. The
Prelude in C sharp minor, the really famous one that starts... Do you want to
play at the beginning? I want to. I'm not a big, you know... I'll give you a C
sharp. So have you ever played that on piano? I've never played it on piano. I
just gave it a crack for the first time now on the oud. But I find his pace and
his soul, Rachmaninoff, it's such... That passion which I think a lot of the
Arabic music has. And I also am a big fan of, you know, performers that are
composers. And that's something that I always look up to and see what they're
doing and how they challenge themselves on the instrument. Coming up. You You
Rachmaninoff's prelude in C sharp minor opus 3 number 2 and it was performed by
Vladimir Ashkenazi. It was the choice of Joseph Twardros who's with me on duet
Joseph you met Vladimir Yeah, I met him, you know backstage and in the BBC. We
were both doing interviews, you know I had to warm up the stage, you know that
sort of thing. No, just kidding. He was a lovely guy We talked about about wood
and he was very interested in the instrument and yeah, lovely guy Tell me about
your interest in Khalil Gibran I was really I love his book the prophet the one
he wrote in 1923 very famous book And I received that as a kid and I hated it I
was like, what's this guy going on about? but then you know you go through some
things and then his words kind of resonate with you a bit more and I think
That's the great thing about him His words do resonate with a lot of people and
provide a lot of comfort for people and so wonderful quotes One of them I
remember about you know, if your heart's like a volcano, you can't expect
flowers to grow Wow, all these beautiful little wisdoms. I've never I've never
heard that but you know, it's good point Don't don't don't don't do that with
flowers or volcanoes All these you know, they're quite philosophical the idea of
your thoughts being like leaves that love to sway in different directions That
reminds me of you. Oh, I don't know. Well, thank you very much I'm just I
thought you know reason and passion are the rudder and sails of your seafaring
soul we can do all that But I wrote three albums based on his words one of which
was the hour of separation Which was you know, his quote was love knows not its
own depth until the hour of separation Another album called the prophet which
was actually inspired by his words and the third album concerto of the greater
sea Which was kind of you know more words of a return and more comforting Joseph
I brought in just a little Carlisle Gibran volume because I it's like parables
and yeah things like that There's one there that I think is really appropriate
to you. It's about the earth. Yeah, do you see it? Why do you think it's
appropriate to me? I because You'll see. All right. Okay, I'll read it for
everyone the red earth Said a tree to a man my roots are in the deep red earth
and I shall give you of my fruit and the man Said to the tree how alike we are
My roots are also deep in the red earth and the red earth gives you power and
bestow upon me of your fruit And the red earth teaches me to receive from you
with thanksgiving What do you think I mean, it's a lovely lovely quote, I don't
know if it sounds like I don't know I've never talked to a tree for starters
Connection to the ancient connection to the earth. No, it's true. It's charming
being our common language What is I mean, I've always been strong believer in
honoring the ancestors and the music that's cut before us because you know I
think music is constantly evolving and we have to know at what points it was
evolving You know, it's it just doesn't drop from the sky There's what efforts
made by musicians and I think that's really important to really honor the roots
but move forward With you know, some pioneering things. So I think we're
listening to the next one self-knowledge, which is a solo wood piece from You
know the pieces based on his works You You You You You you Joseph Twardros there
with his own self-knowledge from The Prophet, which is music inspired by the
poetry of Khalil Gibran. And we mentioned that commonality of musical language
and I've read, you've talked about that a lot. In fact, that seems to be one of
your, maybe one of your missions? Yeah, well, I mean, it's just there. I mean, I
was brought up in Australia, I was born in Egypt and loved the Egyptian culture
and Arabic music, but there's so much great music out there and you can find
always similarities between music and it has to be done well. And I think great
collaboration comes from meeting points rather than just having two things run
at once. And you've seen those collaborations where it's just, you know,
everyone applauds on the end, but no one really gets it. Whereas, you know, it's
really important to understand about the musicians that you deal with, their
style, their upbringing, and then to find a meeting point, which is, you know,
that we both can understand rather than just having two things run at once.
You've talked a lot about labelling and how labelling certain genres and
musicians as world or ethnic, et cetera, kind of undermines that commonality of
the musical language and the musical ideas. Well, I don't actually mind the
labelling. I mean, you're going to get labelling you don't like or, you know,
people putting you in certain boxes. But I do think there's many more
similarities than differences. And, you know, audiences are almost shocked
about, you know, these music that they do like and how they've liked a certain
collaboration. But I don't think it's that hard. It's just that you have to
listen and you have got to have musicians that are prepared to learn about the
other genres and to learn about the other musicians and to find a really good
meeting point. And that's what I've always tried to do is find the right
musicians to undertake that. You know, Richard Tonietti was one of them. We
worked on arrangements with the Australian Chamber Orchestra and that was a very
successful tour. And we then did one again based on Vivaldi and Music of the
Baroque, which showed the similarities between some of the things that I do and
Baroque music and the progressions and the way it moves and the intervals. So,
yeah, I think there's much more similarities and it's not that hard to find. And
I think playing an ethnic instrument or labelled as ethnic, I think shouldn't
deter people from listening to the music and loving it. Do you think Mozart is a
great example of this? I wouldn't call it the common touch, but it's something
that speaks to everyone, isn't it? Mozart's a great... I mean, I love Mozart and
there's a lot of... I mean, there's been experiments with Arabic musicians.
There is a CD floating around called Mozart in Egypt where they get a lot of
Mozart's music and have Egyptian musicians play along with it. But this is why I
picked the Symphony No. 25, the first movement in G minor, because it really
feels like an Arabic piece. I really, you know, feel that, you know, it's like
an Arabic folk song with some harmonies over the top. And I performed with the
Sydney Symphony and they did this piece and my brother and I were able to do a
little version of it on the oud and the percussion just to show what we talked
about. Do you remember how it went? Could you give us a little first? The
opening, I can't really remember. Off the top of my head, it's something like...
That'll do, I think. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Oh, that was lovely, Tamara. Excellent
stuff. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. And that was the first movement, Seafarer from the Concerto of
the Greater Sea, in fact from the Greater Sea Suite in A by Joseph Tawadros,
who's with me in Eugene Goossens Hall clasping the oud. Mm. Ready to perform.
Who was playing with you there? I was playing with Joseph Tawadros on Bendir,
which is the Egyptian frame drum. You've always composed, Joseph, haven't you?
Yeah. I mean, since the beginning, since the first album, I've always composed
my own music and that was a part of the challenge. There was just no repertoire
to play with other musicians, you know, and it was so exciting. And I think
throughout the years I've been very lucky to collaborate with some of the great
Australian musicians here and musicians from around the world and to create
works for them and to be along and play along with them as well. And, you know,
I've done something that I'm very proud of that doesn't get much light. Speaking
about Gibran earlier was, you know, a project with the Song Company, which had
SATB voices. So that was cast upon me. And then most recently was the oud
concerto, which I find very important in the development of the oud and in the
development of where I'm going as a composer. So how do you go about composing
for a symphony orchestra? I mean, do you do it at the piano or at the oud or
sitting down? No, it's always at the oud. And the thing is, oud players, and
again, I kind of dream of being in Egypt and loving Egyptian music and all that
sort of stuff. Most oud players are composers, and most of the oud players do
write their own songs. So I felt like that was my kind of duty as an oud player
to be also a composer and an improviser. And I think we played Rachmaninov
earlier. It's that kind of getting into that mind space, you know, what you want
to achieve with a certain piece. And whenever I compose for musicians, I try and
get into their brain and compose to their strengths. Because it's important to
compose to collaborator strengths so the collaboration can be strong. I'm not a
fan of these give musicians stuff they can't play just because, you know, let's
see what happens. It really has to be done very well so you have a strong
collaboration. And with the concerto, I wanted something which would challenge
me. Most of the music that I wrote, I couldn't play. But that's a thing that
composers do for other musicians. You know what I mean? You know, a composer
will compose a violin concerto not knowing how to play the violin. So I didn't
find it too far off. But again, it's that challenging, you know, trying to
create a work that was, you know, that would hold the test of time. Now our last
track is Vivaldi. And I think I'm right in saying that you're the only
Australian composer that the Academy of Ancient Music has performed. Yeah, I
know. I mean, that's by default because they had to play one of my pieces.
Because they only play old music. But it was a real honour. They actually sent
me a letter saying, well, yeah, because they don't play modern music. But in
this program, again, Richard went over and we did a program, again, the same
program we did with the Australian Chamber Orchestra, which was, I spoke about
it earlier, with the Baroque music and my music and finding similarities between
that. Because Vivaldi, you know, there was a lot of happening in Venice and a
lot of trade in Venice. And so a lot of the music was affected and a lot of the
art was affected. So that's what we're trying to bring to the foreground. And
I've always loved Vivaldi. It was my in. It was my gateway composer into music
of the West. And I really enjoyed his music. And I found his music very close to
that of the Middle East. And this violin concerto, we actually played together
in that series. And so it just, for me, it sounded like an Arabic folk song. So
there has been a diving nature to it. And that was Vivaldi, the violin concerto
in A minor, you heard the Academy of Ancient Music. Elizabeth Wilcock, violin
and directed by Christopher Hogwood. It was the choice of Joseph Tuadros. You've
brought something a little bit challenging, haven't you? Well, I thought if I
was going to challenge anyone it would be you, Tamara. And that's why I brought
a piece called Give or Take, which was originally as a duet for double bass
player John Patatucci on the Arab Separation album. So when I found out that he
was going to be on the album, I wanted to challenge him and write this piece,
which is kind of a show piece. And we then later did it with the Australian
Chamber Orchestra and in violin duets. And so now it's going to be new with
Sartu Vanshka. Yeah, good old Sartu. She gives it a really good go, this piece.
And so I thought it would be great to hear it in its, you know, this is a world
premiere on piano. No one's ever played it on piano with me. Okay, so give me
some tips. Remember, play fast. Just play fast. Just smudge. Smudge is your
friend, you know. And the other thing is, you know, a lot of the listeners are
going, oh, that smudgy Middle Eastern stuff. Don't worry, get away with it. It's
all right. This is Duet on Rn with Tamara Anna Czyszlowska. Welcome 미야 returning
to school. Joseph to Wydros, I've been forced to count to four and beyond. Yet
again, thank you so much for the duet. Oh, thank you very much for having me.
The World Science Festival Brisbane is back. New voices from ABC RN are geeking
it up all week with brilliant brains from all over the planet. Fran Kelly
searches for a net zero future. Jonathan Webb, that's me, looks at Stone Age
medicine. Paul Barclay asks whether science can save the world. Saturday night,
Teagan Taylor, that's me, posts our science soapbox, Occam's Razor. If you're in
Brisbane, come join the fun and keep listening to ABC RN to hear lots more from
the World Science Festival Brisbane. Which concerns you more, China's military
might or its enormous communications offensive? It is complex,