TRANSCRIPTION: ABC-RN_AUDIO/2023-04-20-15H00M.MP3
--------------------------------------------------------------------------------
 life for you. And heavy metal in the Islamic world. With the author of We'll
Play Till We Die, Mark Levine. That's the music show. ABC News with Andrew
Calupus. Federal Treasurer Jim Chalmers won't say if he's considering raising
the Job Seeker Allowance as recommended by the Economic Inclusion Advisory
Committee. The report makes the case that several Australian welfare payments
are too low, including Job Seeker, Youth Allowance and the Commonwealth Rent
Assistance rate. On the ABC's 7.30 program, Mr Chalmers said the government
needs more time to consider the recommendations. We will work through the
recommendations in a methodical way. It's no use pretending that we can
implement all of these in one budget. I don't think the review panel even
expects that. We'll help people where we can. We'll try and do that in a
responsible way that doesn't blow the budget. An independent review recommending
major changes to the Reserve Bank of Australia has gained bipartisan support.
The review has laid out 51 recommendations, including increasing the number of
RBA board members and splitting the board into an advisory monetary policy board
and a governance board. The head of the Australian Council of Social Service,
Cassandra Goldie, says creating a more transparent and independent RBA is
important. It's critical that we get greater diversity in the decision-making
structures of the Reserve Bank, including in the boardrooms. We are really going
to be pleased to see much greater diversity and we hope those processes deliver
that outcome. Independent Senator Lydia Thorpe has accused the Prime Minister of
being opportunistic after he labelled her involvement in an altercation as
clearly unacceptable. Political reporter Shandell Al-Khuri has more. When
footage aired on Seven News, Senator Lydia Thorpe was seen in an altercation
with a group of men in Melbourne at the early hours of Sunday morning. Senator
Thorpe alleges she was standing up for herself after she was provoked and
subjected to racism. Prime Minister Anthony Albanese yesterday urged her to seek
support for health issues. She says suggestions she needs mental help continues
what she described as a racist and misogynistic narrative used to discredit and
silence outspoken women, particularly Indigenous women. Three people are dead
and three others are in hospital after a crash involving a truck and two
vehicles in Victoria's far north on Thursday afternoon. Courtney Withers
reports. Police say two cars and a truck collided on an intersection of the
Murray Valley Highway near the border town of Strathmerton. At around 2.30,
three people died at the scene, they're yet to be formally identified. Three
others were taken to hospital with non-life-threatening injuries. The truck
driver wasn't injured and stopped at the scene. Police want to hear from any
witnesses or anyone with dash cam footage as they try to determine the exact
cause of the crash. The Netherlands and Denmark are buying 14 Leopard 2 tanks to
send to Ukraine. In what the Dutch Defence Minister says is a show of support
for Kiev's resistance against Russia's counter-continued attacks. With more, the
BBC's Anna Hologhan. Leopard 2 is considered one of the world's finest tanks
with a greater range, firepower and targeting system than its vintage
predecessor. The 2 is also faster, heavier and better protected, making it more
valuable and resilient on the battlefield. The consignment will cost about $180
million, with Denmark and the Netherlands splitting the bill. To sport and in
rugby league, South Sydney has scored a rare win over Penrith in a thriller at
Stadium Australia. News ABC Sports Andrew Moore. Penrith had won 11 of their
last 13 games against South Sydney, but it's the Rabideaux who scored twice in
the last five minutes to snare victory from the jaws of defeat. Penrith looked
like they had it wrapped up when Steven Crichton scored his third try in the
71st minute to give Penrith an advantage of 18 points to 10, but Latrell
Mitchell scored in the 75th minute from a contested kick and then a stunning
team try, finished off by Rabideaux centre Isaiah Tass in the 79th minute, gave
the Rabideaux a 20 points to 18 victory before a vocal crowd of 19,548. Andrew
Moore, ABC Sports Sydney. American golfer Brooks Koepke says he can't wait for
the party hall at the Live Golf event in Adelaide starting tomorrow. The Par
312th hole at the Grange will be turned into a stadium hole to create a raucous
atmosphere. Four time major winner Koepke says he's excited. I love when the
fans get a little rowdy. They're screaming, they're booing you when you hit a
bad shot and cheering you on when you hit a good one. I think that's going to
make this hole exciting. I think it'll definitely bring a different atmosphere
as well, which will be cool. Most fans we've had yet thus far, so I'm excited.
You're up to date with the latest from ABC News. For more news at any time you
can download the ABC Listen app. Think bigger. RN. This is the music show and
I'm Robbie Buck. We are listening to Ruth's song for the sea off Kit Down's
first solo album recorded with ECM, Obsidian. Later in the show we'll sit down
with Kit for a chat and a live performance from the band. Thank you. Let's dive
into some heavy metal. We'll Play Till We Die is Mark Levine's second book and
explores the revolutionary music cultures of the Middle East and the Islamic
world. Before, during and beyond the waves of resistance that shook the region
from Morocco to Pakistan. Mark is a rock guitarist and professor of history at
the University of California Irvine. Lucia Sobera is the senior lecturer and
chair of discipline of Arabic language and cultures at the University of Sydney
where she studies the colonial and post-colonial history of the Arab world and
Africa. Let's start with some music coming out of Morocco. Palestine. This is my
rock and roll, my rock and roll Ah, la, la, la, la, la This is my rock and roll,
my rock and roll I'll crush him, I'll crush him I'll crush him for no reason,
I'll crush him now and only half of your fave HoAR combo Cold and allez-vous
andLes East Obviously peak is on my Kashmiri booty on the passport on holiday
Catch the bunny catch the bunny catch the There is El Cahid Motorhead by Hobba
Hobba Spirit, one of the groups that's referenced in the book Will Play Till We
Die and we have the author Mark Levine with us today on the music show. Hello
Mark. Hi, how are you? Great to be here. And also joined by Lucia Sorbera who's
a senior lecturer and chair of Discipline of Arabic Language and Cultures at the
University of Sydney. Hello Lucia. Hello. Alright, the book is subtitled
Journeys Across a Decade of Revolutionary Music in the Muslim World and that
decade is an interesting decade. Let's go to the start of it where things were
quite tumultuous, Mark. Well this book is in fact a follow up to the first book
I wrote about music in the region called Heavy Metal Islam. My research there
started in 2002 so I've been researching this for over 20 years. That book kind
of predicted that something big was going to be happening soon. It was really
clear if you were working with young people, especially artists on the avant
garde that something was going to happen, that things were just too repressed
and this huge youth culture was just too explosive. And so when it happened it
wasn't really surprising to me in late 2010, early 2011 that so many of the
people who were involved as the most important activists in the region came out
of the scenes, the music scenes I was looking at which was the heavy metal
scenes, then the emerging hip hop, hardcore punk scenes, the same skills that it
took to create an underground music scene it turns out are very useful for
creating an underground political scene and breaking it big. It's like breaking
a band in a way. So that's how this book really came to be is seeing all the
friends I had made over the previous 10 years out on the streets in the lead of
these uprisings and I wanted to just follow their stories and how their lives
evolved and then bring in a lot of new artists and new activists who I'd met
along the way. And Lucia your perspective on the cultural output of that very
vibrant time? It's very multifaceted. I focus mostly on women's movements and
women's political activism in Egypt and what I have seen over the years it's a
growing participation of women in the broad political spectrum of all political
formations and especially the social movements that emerged in the 90s. So you
know women have one century of political activism in Egypt. They have been in
all the political movements, the underground Marxist movements, the Islamists,
the liberals, the democratics, the students movements. So it was not surprising
that they were at the forefront of the 2011 revolutions because they were
already there and especially young women were very vocal and they raised topics
that you know until then were taboo. Some of the most high profile activists for
the freedom of political prisoners today are some high profile women, lawyers,
writers and activists. Mark you mentioned your first book which was Heavy Metal
Islam. Give us some insight into that particular genre and how it played a role
and indeed how that attitude grew into different genres too. You know I guess
when I was growing up my best friend was from Yugoslavia, the one that was still
Yugoslavia and he used to tell me how they used to sneak, this is in the 70s,
into the woods and play all these band heavy metal albums like Black Sabbath and
Nazareth and all these old 70s bands because for them it just represented a kind
of freedom. You know we all know in the former Soviet Union also rock and roll
and especially metal was really big. So similar thing here. The difference is as
you know so many people have said to me when you look at an Iron Maiden album
cover and you see Eddie you know with skeletons and corpses and tanks you know
and it's like a fantasy but for so many people in the region that's their
reality. So of course metal is going to make sense if you grow up in highly
authoritarian societies with a lot of violence and even war, metal makes sense,
really makes sense. And also the cathartic element just says for kids all over
the world especially in these kind of societies it's a release that you really
can't get anywhere else especially if you're already not someone who fits into
the norm. And what about in Egypt Ligia? It's very broad in Egypt, the scene is
very diverse and it's also reflected in the broader cultural scene. You know if
you look at the literary sphere there has been for instance an explosion of
comics and graphic novels and pop cultures more in general which comes before
the revolution and in many ways some of the themes that were discussed in these
graphic novels predicted the revolution as well. They all discussed you know
very deep and important political and social issues that were then put at the
forefront of the revolution like social justice, the fight against corruption,
the fight against the violence of the police. Similarly if we think of the
lyrics of metal songs or also early hip hop songs when it was much more
political, groups that were big were you know really the hardcore metal like
deicide or cannibal corpse or napalm death you know these kinds of really
extreme metal genres became the ones that were really popular and the lyrics
make sense and the other thing is for young people in the region especially
where you're heavily surveilled and monitored to be able to sing brutally in
English where no one really can understand you unless they're like really fluent
in metal brutal singing it's a very safe way to say things about regimes that
wouldn't be very happy if they knew what you were singing. But these days it's
not just metal is it that gets such a representation and I take it it's the
attitude not just the sound of what's being played here. You've also got as you
mentioned hip hop there's what is it maqra ghanat? Yeah maqra ghanat it's a
wonderful genre of music that emerged out of hip hop and maqra ghanat means
festival in Arabic it started at weddings street weddings blasting sort of what
we call shabi or popular music with very fast and danceable beats and then
people would start rapping over it and it just evolved into its own genre. I've
attended some of the performances and I and you know there's also a broad
scholarly literature about that now you know there are ethnotropologists who are
studying those and from multiple perspectives generational gender perspectives
and they are all emphasising how these genres are politically very engaged and
it's played an important role in mobilising people. So what's the reaction been
like in places like Egypt and elsewhere in the Middle East to these kinds of
musical forms which as we know can be viscerally rebellious? The scenes
especially the metal scene which came first became really popular first across
the region we're talking Morocco to Pakistan right straight across every country
had a decent metal scene and Egypt one of the best but certainly not the only
one. By the mid late 90s as it became popular it started getting a backlash just
like happened in the US the decade before and people's kids started getting
arrested they were prosecuted for being Satan worshippers a lot of the scenes
went underground for another five or six years but by the mid 2000s especially
as these authoritarian governments which really couldn't do much for their
citizens except arrest them and torture them they really couldn't give them what
used to be known as the authoritarian bargain you know better education social
development healthcare jobs that all was falling apart by then so there was kind
of a liberalisation culturally going on similar to what happened in China in a
way and so you know by then people were like it really doesn't matter it's not
really against Islamic law to play certain forms of music even Saudi Arabia now
has a thriving open metal scene when I first started this it was mostly
underground now even Iran you know depending on the week sometimes you can have
a metal show sometimes you can't so in most countries though music has become
accepted once again because music's always been at the centre of Muslim cultures
for 1400 years there was a short period and you know that it hasn't been but now
most places other than Afghanistan perhaps don't really have any limits on music
as long as it's not explicitly anti-religious. If you turn on the TV and look at
the Middle East a lot of the reporting of course is looking at the Palestinian
territories and the situation there has deteriorated in many ways what's
happened to the musical activity whilst this has been going on Ligia? Music is a
space of resistance and of assertion of existence of people so there are there
continues to be festivals of music but also of literature in May the Palestine
Festival of Literature which is an itinerant festival organised by the writer
Ahdaf Suhaif who is Egyptian and she's an advocate for Palestinian rights and
Robert Homer Hamilton they try to bring world literatures across the Palestinian
territories and their idea is as the people can't travel we travel with books
and writers. Palestine I always tell to my students is a miracle of culture
because there is not I think other people in the world that could keep alive
such an important literary musical theatre culture under these circumstances and
they managed. Yeah also there's the Palestine Music Expo which very much is
built on the same framework as the literary festival which brings in artists
from outside and yeah the hip hop scene of course is very big and very important
the first video ever Arabic language hip hop video ever to get a million views
on YouTube which I know today seems like nothing but back in 2006 to get a
million views in Arabic that was something was the band called Dom which was
actually a band of Palestinian citizens of Israel and since then Palestinian hip
hop has become perhaps the most important hip hop scene in the Arab world the
most political hip hop scene certainly right up until a couple of years ago
during one of the Israeli invasions of Gaza when there were protests against the
border a rapper named MC Gaza literally was filming in the midst of the fighting
a music video which he then released to the world and to people who were
participating in the Palestine Music Expo showing which was really one of the
most powerful videos I've ever seen as the smoke and the tear gas and bullets
are flying he's rapping so it's very much powerful but also the first Arabic
metal band to actually sing brutal in Arabic is a band called Halas which means
enough which came out of Haifa and Akko or occur so Palestine has always been at
the forefront because it is literally the crossroads of the region and because
so many Palestinians are in the diaspora and so many people come in and out and
also the influence of Israel and the Israeli metal scene the Israeli hip hop
scenes and the Israeli electronic scenes also some of the most avant-garde and
best DJs and electronic dance music people also coming out of Palestine. Mark
you come to the age-old argument about protest songs from an interesting
perspective you studied at the conservatory for a little while you're a
professional musician you're also a scholar of Hebrew and Judaism too and you
come to this world of music with a lot of on-the-ground experience. From your
perspective seeing what you've seen and being in these areas of the world what
is the role of music for the protest? I can give you two quotes that I think
capture it the first is from Fela Kuti you know the great Nigerian founder of
Afrobeat who said you know music is the weapon of the future it's such a
powerful cultural force that you can actually use it as a weapon against
authoritarian regimes because it can tell the truth in ways that no other
artistic genre can as viscerally and as powerfully connecting to people. The
other quote that I come back to almost every day in my life is by this African-
American playwright and documentary filmmaker Tony Kade Bambara who said the
role of the artist is to make revolution irresistible and that's really what art
does it gives you a conception it helps you go from just a subculture to a
counterculture to a revolutionary culture articulating a new vision of the
world. You can't win you can't defeat bullets just with music but if it helps
make the idea of a different future not only possible but urgent then it's
playing a very necessary role I don't think there's been a revolution that
didn't have a music or a form of art that was associated with it so in that
sense I think it's necessary it's crucial but of course so many other factors go
into whether it actually works in the long run especially as Bob Dylan's career
would show us you know how easily the artist and the music can become co-opted
commercialized commodified monetized and which point happened to hip-hop
especially you know it loses its revolutionary impetus. And Lucia from your
perspective the role of women in all of this? They're everywhere it's crucial
you know maybe we should not even talk any more about the role of women because
you know they're just there and of course they're you know with their bodies
with their presence they are they are at the forefront of a fight Farid Al-
Nakash she's a veteran of journalism in Egypt and she's been you know a high
profile political activist told me once in an oral history interview women are
attacked brutally because if you take away from the square if you control art
for the population you control all the population yeah and therefore there is
this tendency by authoritarian regime to attack women first because they they
are absolutely crucial to generate you know identities and education and space
the way we live the space if you make the city unsecure for women the city is
unsecure for everyone and that's exactly why in the in the phases where the
regimes were more brutal or where revolutions had to be crushed the first to be
attacked were women. I would just say also you know the first great metal band I
saw a thrash band was an all girl band who were playing at this festival this
DIY festival in Morocco called the Boulevard Festival in 2005. There have been
you know women rappers you know metal artists EDM artists etc punk rockers from
the very beginning in this region they've been at the forefront of it when they
weren't out front on the stage they've been behind as some of the main
organizers of concerts of festivals so women have been at the avant-garde of the
scene of all these scenes and when you go to a show like in Egypt I organized
one of the first metal shows to be held in Cairo after the satanic metal scares
in the mid 2000s and you see a bunch of teenage girls all of them wearing head
scarves head banging next to a bunch of guys you know right next to them like in
a real mosh pit kind of situation squeezed together and you think even I thought
you know that's not supposed to be there right that's not supposed to happen but
of course it's just normal for them that's why I think music's so powerful
because it forces you to re-examine our you know long-held prejudice about what
it means to be a woman or a girl or how people interact in reality it's so much
more complicated than we imagine and the interesting thing is that with women
activists they suffer so much violence from regimes but these regimes are also
really smart so women artists they like to co-opt so in Morocco that band I
mentioned this band of young women the king's daughters became fans of them so
that they started getting money and of course once you get money are you going
to say anything about the king of course not right so if you're an artist you
can be co-opted a lot more and that's really the tension also that artists face
in the region where they're so desperate to have some support yet they also want
to maintain their integrity and the political grounding of the music and you
know women have usually been the best at negotiating this well let's go out with
a song this is God Allow Me Please to play music who's this by oh they're so
powerful and so wonderful voice of basser pro is an Indonesian all-female metal
band same thing you know people said why are these young women playing metal boy
can they shred that's all I can say I wish I played guitar like them and I've
been playing 30 plus years so they're a great way to go out. Martin Fine and
Lucia Sobera thank you so much for your time. Thank you. Thank you. Thank you.
Thank you. Thank you. Thank you. Thank you. Thank you. Thank you. Thank you.
Thank you. Thank you. Thank you. Thank you. Thank you. Thank you. Thank you.
Thank you. Thank you. Thank you. Thank you. Thank you. Thank you. Thank you.
Thank you. Thank you. Thank you. Thank you. Thank you. Thank you. Thank you.
Thank you. Thank you. Thank you. Thank you. Thank you. Thank you. Thank you.
Thank you. Thank you. Thank you. Thank you. Thank you. Thank you. Thank you.
Thank you. I actually enjoy it because you don't really have the CPU brain space
or the energy to question yourself. I teach a little bit and a lot of the things
I end up talking about with students that are struggling with certain things is
this self-judgment, the inner critic, the kind of devil on your shoulder. And
that is definitely the enemy of improvisation. When you're improvising it
doesn't even necessarily need to be good, whatever that very subjective term
means it just has to be present and the intention has to be articulated. And
beyond that it's very possible to self-sabotage if you start making judgements
moment by moment. So I guess you're trying to get in that frame of mind where
you're allowing things to happen and you're focused on the moment at hand, not
thinking about where you're going in the future and not thinking about what just
happened. So when I was playing then I didn't know that I would incorporate a
little bit of a Hungarian folk tune, which I did, and I kind of just mashed that
into like a three bar section of a tune of mine from a band that I used to play
in about ten years ago. So those things were not, they just happened to be in
the same key and I happened to arrive at one idea and it suggested itself in the
back of my brain and I thought it was a good idea. Whether it was, I don't know,
but it's a very honest representation of what was going on in my head at least.
You were here playing the grand piano but you mentioned that other instrument
and that is the pipe organ, which isn't synonymous with jazz necessarily and
that was the start for you with your explorations when it came to improvisation.
My first instrument was cello and then I started singing in a choir and I heard
the organ getting played and up until that point I'd been playing written music
always. I mean I'd say up until that point I was eight or something like that.
But it had always been about reading music and theory led and then the role of
the organ in the church service is often, you can play a little filler
improvisation while the priest takes the cup to the right table and stuff like
that and you can use themes from the service so you can use a hymn tune or a
psalm chant or a piece from whatever. And I loved the freedom that the organist
had. I'd go up and watch the organist sometimes. I'd pretend to be sick and then
you were allowed to go up and watch the organist from the back of the organ
loft. And I'd see how free they felt definitely in comparison to the other stuff
they were playing which was heavily notated and demanded a lot, especially at
the organ which is an instrument that demands a lot of concentration anyway
because there's so many variables, so many sounds. You've got four keyboards
stacked on top of each other and one that you play with your feet extra. There's
a lot going on. And I was seeing how free the organist who's my teacher as well,
Catherine Deenish, could feel when they were just taking a piece of often very
old melodic material from a psalm or a hymn and just twisting it through
different harmonies, making little rhythmic cycles. And I was like, that's for
me. I love that approach. And when my mum cottoned on that I liked that, she
gave me Night Train by Oscar Peterson. Then I kind of disappeared into a jazz
rabbit hole for about 15 years. And then as my tastes grew towards something a
little more close to new music and experimental music I was looking for
different sound sources because I was feeling a little bit limited by the piano
and I'd never really warmed to keyboards. I mean like I'm a technophobe and...
You mean electronic keyboards? Yeah, sorry, like synthesizers and things like
that. I would always manage to break them somehow just by looking at them.
They'd fuse in some way. So I saw the organ as like I could reunite with that
instrument which had the benefit that I was never really taught at college that
so it felt like a version of being self-taught in some way. I felt very free on
it and didn't have any of the baggage that maybe I have on the piano just
because you're taught for... I was taught eight years higher education on piano
so you have a lot of rule sets that you have to get out of your system once you
leave. And I didn't have that with the organ. I just had this childlike
fascination with it still. But I had all this stuff that I could bring to it
from my studies in jazz and things about rhythm and harmony and different
reference points and of course improvising. So about 10 years ago I started
trying to develop a language that felt personal on the organ and the deeper I
got into that the more I relished the challenge of... I mean it's such a
challenge anyway. You're always on a different instrument in a different room
that sounds completely different. The organ always speaks differently in
different rooms. There are different sounds, different ways that it's built
mechanically. There's so many variables each time you play that it's... That's
fascinating to me. I love that. And I kind of genuinely embrace chaos in all
forms in my life but particularly that felt like... It doesn't sound like the
most sort of punk thing to do play church organ but there's something about
really not knowing how it's going to go and whether the instrument might work or
cut out and doing that in front of paying public unbelievably is really
appealing. But it's a way of... It's some kind of antidote to the quite strict
formal music education that I had and it feels cathartic in that way. So what
happens when you come from an experience like that playing church organs as a
jazz musician and then come back to a piano? Yeah, I think organ helped me feel
a little less that I had to do impressions on the piano of my favorite pianists.
I felt like I had this little home on organ that was always going to be there
and when I sort of came and then played piano, I was like... I ended up
developing a different set of harmonic fascinations and melodic patterns and
things because of the organ that then weirdly through the sort of long journey
of the circle came back onto piano again. And so the nature of resonance on the
organ is that things speak differently in different textures because the way you
build sound is by putting one sound on top of another sound. So it's additive.
Additive, exactly. To get a very loud note on the organ, you're essentially
having 70 versions of middle C at the same time to get a loud middle C which
will have a slightly different timbre. But that can make it feel very muddy. So
voicing wise, you have to be careful and because also it follows the harmonic
series, things like open intervals, fourths, ninths, fifths stacked of all of
those things. So things like... With those kind of distance between the notes
feel really good on organ. Things like this... Already sound smudgy on piano,
but they get very, very smudgy on the organ. From playing all of this organ, I
ended up with all of these new voicings that would fit the organ well that I
can't help but now play on the piano. So in fact that thing that I was playing,
the sort of pattern that I ended up locked into in the improv that I just did,
this... It's all built on fifths, so it's coming straight from that concept of
open intervals. One road leads to the other. Yeah. Well, let's talk about a
recent release of yours, Famillion, which has Peter Eld on bass and James Madrin
on drums. Tell me a little bit about that experience. You've got a producer like
Manfred Eicher, who is the label owner who's produced over a thousand records.
Done. Yeah. So what's the experience like for you as a jazz musician going into
that realm? It was interesting because my route to ECM was sort of slightly
through the back door. I played on Thomas Stronen's record, who's a Norwegian
drummer, and Sun Chung was producing that for ECM, and he heard me there, and we
got on, and I suggested the idea of doing some solo organ stuff. So Obsidian and
Dream Life of Debris, two organ albums that I made for ECM, followed, but they
were both with Sun Chung, who was a producer working under Manfred within the
ECM fold. And then Sun left ECM, and we'd been talking about making a Tria
record, so Manfred did the Tria record, and that was the first time I'd worked
with him. And before that, that band with Petter and James was called Enemy, and
we played quite different. It was very rhythmic, lots of quite complicated
meter, polyrhythmic things, and quite an aggressive aesthetic, kind of a lot of
beats. And we changed, of course, that for the... It was a collaboration with
ECM rather than for ECM, because to fit with that body of work, of course, you
know going in that it's a certain experience that you're getting. You know
there's going to be reverb, you know there's going to be four seconds of silence
at the beginning, and you know it's Manfred, and he has what he likes. So it's
almost like taking on a fourth band member, really. But knowing that going in,
which we did, and we're really excited about, because we've all listened to ECM
records since we first listened to jazz. I said Night Train, Oscar Peterson was
my first jazz record, Cologne Concert was my second one. And the sound of his
records is in my ears, definitely. And he's still producing. Yeah, yeah, he's
still producing, and he's great at it. He has a very strong idea of what works,
and it takes the pressure off you to existentially worry about how good you're
playing is or not, and whether it works or not. And you can relax on that level,
because he's really guiding the ship. And you'll come out with a record that is
not the one you would have made on your own, but that's a good thing. That's
what you go into collaboration for, is to hand some of the control over, and
then it means that you can reach areas that would have otherwise been
inaccessible, because of your own preconceptions of how things usually flow.
It's basically two days, you go in and record on one day, you mix down on the
next day, and so you've got to hit it right? We're kind of a live band, we've
done a lot of gigs together, so we like that. That plays to our strength,
really. Having it so quickly take some of the pressure out, when you know that
we always have a rule of three takes a day for each tune, maximum, and hopefully
it's just one or two kind of thing. I mean, that's kind of how it used to be
done. So there's no reason why it can't be done as well. Obviously, with really
tricky music, and when a band hasn't played much before, that can be really
tricky, but that wasn't the case here. And it was also, we recorded it in
lockdown. I mean, all three of us came from different countries, to a different
country, plus Manfred from a different country as well, all coming to
Switzerland somehow. Whilst lockdowns were happening, and people had to come
through strange routes in order to not have to quarantine. We had to come
through the Italian border in like a van with blacked out windows, like one in
the morning, it was incredibly dramatic. And we managed to get there, and it was
such a miracle that we were all in the same place. And it was the first time I'd
played and seen my friends in that band for like two years. The main
consideration was being present and enjoying the moment rather than worrying
about the outcome, which is definitely the best possible mindset. You were
saying what mindset are you in? That's the good mindset to be enjoying the
moment that you're in. Well, we're glad that you're present today on the music
show, Kit Downs. Thanks for joining us. And considering you're sitting in front
of a beautiful grand piano, maybe we could push you for one more improvised
piece. What's floating through your head at this stage? Anything we should know?
Six am flight. I'm a bit hungry. This is the end of my trip, so yeah, I actually
have an idea to start with. And it's because I've never been to the Sun and the
Hemisphere before, so I've never seen the stars from this angle. Well, I just
straight up haven't seen some of those stars, and I've never heard some of these
bird calls that you have here. So that's sort of in my head. All right, Kit
Downs, thank you. And all right, take care. with an improvisation inspired by
Australian Birdsong and Jetlag. Next time on the music show, the life and music
of Australian composer Margaret Sutherland, via her biographer Gillian Graham,
with plenty from the ABC archives. A pivotal figure in the architecture of
Australian music, both metaphorically and literally. The wait is over. The new
season of Short and Curly has arrived. The perfect podcast to get children and
adults talking about life's biggest questions. Like, is it okay to kill insects
that annoy us? Is it rude to stare? And should children be allowed to swear? I
would never swear in front of my parents, but I do at school. That's Short and
Curly. Hear it now on the ABC Listen app.