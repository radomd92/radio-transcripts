TRANSCRIPTION: KZOO_AUDIO/2023-03-09-12H00M.MP3
--------------------------------------------------------------------------------
今でもさらわれてゆく歌詞・音楽復髑されヒネくれて膝をかかない手で 肌散って 過ぎてゆく 時を身をくる窓の外 吹き荒れぬ
風はないでいる二方の人生と飛ばされぬようOh...Oh...You are gone in the wind心には 積むじ風の扉へ続くコリドーンひたまりに
髪をとかず 今でも君は そこにいる降り向いた坂道 涙さえ見せないでさていた 遥か遠い日も 後ろ姿なんて優しく 蘇れるOh...愛は砂のように 奥の指の隙間を
溢れていたけれど今も心には 積むじ風の扉へ続くコリドーン遠い日々へのコリドーンYou are gone in the windYou are gone in
the windYou are gone in the windYou are gone in the wind泣いたりしないでね 今日は大切な日だから
ちゃんと見ていようね心の音が消えるまで でもね もういつでも会えるからねうれしい時も 寂しい時も 胸の奥層を信じて 望むならばこの風に溶けて あの歌に乗って
気付けばあなたを包んでる懐かしい匂いがしたら いつものように 笑ってね今空に向けて ただ安らかに 羽ばたくこの背中をご覧よ右の羽根には さようなら
左側にはありがとう出会う度 あなたは 優しさひとつ 分けるんだよ別れの旅に また強さひとつ もらうんだよでもね ずっと あなたはあなたのままよ旅立つ朝も
つまづく夜も 私の中眠ってた あの日のままこの風に溶けて あの歌に乗って 気付けばあなたを守ってるあるの雨に 夏の川に 秋のちばに 冬の窓辺に 今空に向けて
ただ安らかに羽ばたくこの背中をご覧よ右の羽根には さようなら 左側にはありがとう右の羽根には さようなら 左側にはありがとう間違えているかな 今行ったこの道は
身溺れのない空が 人が出る川沿いの街に 風が吹く 心にも風が吹く流されているかな 知らない頃地に 愛せているかな あの頃みたいに寂しくさせて 生きしたとしたら
いつかきっと取り戻すからそれがたった一人でも どこかで いつも誰かが見ていればただその想いだけを この胸にしまって ずっと走ってゆけるんだもし今すぐ何かを
今すぐ出会われたら僕はその時何を残すあんだろう どんなにつまらなく見えたとしても譲れないものがあるんだここから見える街は まだ変わろうとしてる
何かを捨てようとしているんだ君は僕に何を望むだろう やって遠く日は落ちるそれでもここは風の街 いつでも同じ風が吹いているそれぞれの想いも 優しく包んで
明日もこうして風が吹く橋を渡ってゆく電車の 長いその明かりだけが今は闇に包まれた川に映っている 細やかなこの人生はどこへ続いてゆく明日は他だ誰もいない
それでもここは風の街 いつでも同じ風が吹いているそれぞれの想いも 優しく包んで 明日もこうして風が吹くそれがたった一人でもどこかで
いつも誰かが見ていればただその想いだけをこの胸にしまって ずっと走ってゆけるんだそれでもここは風の街 いつでも同じ風が吹いているそれでもここは風の街
いつでも同じ風が吹いている誰も君に見せたいものがあると 思い出したからタイヤを見ながらスカワーム 海のルートを外れて目指すのさスケーセンまで見えるオープン
熱い風の風を引き裂くように走る掴み合い君の全てを揺らしたくて 今の二人の心は波のようで想いを寄せたり返したり今度もっと心揺らして
もう飾らないまなざしを売ってもっともっと指の先まで 恋気で感じたい不安ばかりはそんな二人じゃなくて 熱い風に生まれたらタイヤもほんとなら いくつぶるのきらめき
そんな二つらな人の僕を見ている君は何かの痛みを隠してる 身体を寄せたり 癒したりもっともっと夢中になって もう迷わない口づけをんでいてもっともっと
燃え上がるほど素肌で感じたい傷ついても強く抱き合えたんだら 愛し会えるままつのでもっともっと夢中になって もう夢中になって もう恋を抱き合えたんだら
愛し会えるままつねもっともっと 心揺らして もう飾らない まなざし呼んでいれもっともっと 指の先 まだ頬気で感じたいもっともっと 夢中になって もう迷わない
口づけ呼んでいれもっともっと 燃え上がる鼓動 優れ感じたい傷ついても 強く抱きあえたなら 愛しい 逢える真夏で燃え上がる鼓動 優れ感じたいOh...愛し合える
愛し合える 愛し合える人は誰も ただひとり 旅に出て人は誰も ふるさと 振り返るちょっぴりさびしくて 振り返ってもそこには ただ風が 吹いているだけ人は誰も
人生に つまずいて人は誰も 夢破れ 振り返るプラタナスの枯れ 浜う 冬の道でプラタナスのチル音に 振り返る帰っておいでよと 振り返ってもそこには ただ風が
吹いているだけ人は誰も 恋をした 切なさに人は誰も 耐えきれず 振り返る何かを求めて 振り返ってもそこには ただ風が 吹いているだけ振り返らず ただひとり
一歩ずつ振り返らず 泣かないで 歩くんだ 歩くんだエンディング半島名の ゴミ袋を持ってドアを開けた空は少しはべた 目を隠しながらいつもと変わらない 原子で
駅に向かうよ昨日 君にあてなかった きっと今日も泣いてた元気になったよ 冬原はじゃ 恥ずかしいからまめあえるをすぐに 自信を持ってゆくめに クライクライクライ
いつだって自分らしくあきらめないよ 立ち止まらないよなに強い風が 吹いててもラッシュワーの 電車に 輸られながら君の言葉 思い出してた無視今ここで
諦めてしまったらあの時 失くして 恋も意味もなくなってしまうよと見失ってることが いつも 気を吐かせてくれるんだ愛だと何回いても 足りないくらい
感染してるよ自信を持って ゆくめに クライクライクライ いつだって顔を開けていつもSmile Smile Smile 今度きらめ あの時は
笑顔で揺られるようにGอย compared 汗ため 自信を持って ゆくめに クライクライクライ いつだって自分らしくあきらめないよ
立ち止まらないよどんなに強い風が吹いて自信を持って 夢にTry, Try, Tryいつだって顔を開けて君がいると
音の歌い方で心はこんなに強くなれる使い降りしてる バクバク一つを持って西から東へずっと 当てもなく流れた長距離の音バス 窓から見える景色はひたすらに続いてる
青空と土凹に胸に移って 歩いたりはどう時からしたい 今この空へキラクに行こう Make it easyかもないよ Girl 誰にだって未来だわからないさIn
my way 風の中で転んでもまた立ち上がればいいDrawing in the windDriving the wind in
me出された薄いコーヒーが焼けにうまく感じていたそれもこの風のせいI love you, lady, you
know僕、D-Landの歌声も意味さえわからないけどこの胸に響いた恋を空に花やたら明日の追い場所答え遅れたもっと遠くTake it easyかもないよ
Girl 誰にだって描きたい愛がRowing my way 風に届け悩みさえ 小さいつもり変わるRowing down inYeah, yeah,
cool恋を空に花やたら明日の追い場所答え遅れたもっと遠くTake it easyかもないよ Girl 誰にだって未来なわからないさMy way
風の中でこの胸もまた立ち上がればいいRowing down inLet's do itNo, no, no, no, noYeah, yeah,Rowing
in the windYeah, yeah,Rowing in the windYeah, yeah,Heat will beSing
hereYeah遥か遥か彼方のことを教えて
誰か君を向くままに遥か今は一生なら見上げた空に君の足跡を探す遠くからやってくる未来光のソフトで色めいとテレパシーだから 火事になって魚になって
君も思うよ愛しき人よ傷とって 悲しみくれ今
目をつむって祈りでもって言える言葉を愛しているの?いつも家にそれはここだけの話遥か遥かの山を越えて若葉の頃に会えたらいいな想い出は
雪の真珠光水背の向こうへと舞い上がることのハートだのに 闇になって獣になって 君も思うよまだ見る人よそのまま 気をなさずにねぇ 君といつか裸になって
眠る世界にもたじの微笑みそのまた別の話風になって 魚になって君を思うよ愛しき人よ傷とって 会えるのだろう今 目をつむって祈りでもって言える言葉を愛しているの?い
つも家にそれはここだけの話それはここだけの話何かは始めようかまずのような目を見つけたらいいな何かが始めようか何かが始めようか何かが始めようか何かが始めようかまず
のようなメロディの方がそうするよ今すぐで勝てようか溢れそうな涙の方が確かめれた遠く広がる空を似ていたそう君と生きていたきっと夢から覚めても明日はあよる君がいせる
のここまでも愛を風のようにきっと夢から覚めても僕らは歩き出せるそうさ 終わらない歌よもう一度いつのことだけの隠れてる願いはもう 知ってるここから旅立とう終われそ
うな昨日にもさらだろう遠く夜明けの空を見ているそう僕は見ているだから出会いと僕らの自由を受け止めたらどこまでも愛を風のようにだから出会いと僕らの未来を受け止める
そうさ 遥かな道を行う声きっと夢から覚めても明日はあよる君がいせるのここまでも愛を風のようにきっと夢から覚めても僕らは歩き出せるそうさ
終わらない歌よもう一度Can you feel it?Low it everywhereCan you feel it?Low it everywhereCan
you feel it?Low it everywhere潮風に君を感じて銀色も波に二人溶けてしまいそう
このまま時間よ止まれと街が遠く小さく見えるよもう離さない君に決めたよ輝いた 季節に辿り着いた未来に迷わず降りし落ちて風のララララ溢れ出しそうな不安に涙を隠した昨
日に良い想い出なら見える風のララララ教えせる見えない不安あきらめかけていたただ一つのことが何音 心を揺らすよ寄せて返す君への想いが気持ちを満ちす光に変わるそれっ
て言ったあの頃君と聞いた砂浜いつまででも風のララララ繋いだ手握りしたくなった気持ちを感じて季節な日から見える風のララララ見つめる笑顔になぜか急に抱きしめた暗い風
に揺れてもう一度も今熱い想いを乗せて君に決めたよ輝いた季節に辿り着いた未来に迷わずに心を振って風のララララ溢れ出しそうな青い海道を隠した色にきり想いが風のラララ
ララ君に決めた風のララララ溢れ出しそうな青い海道を隠した未来に返すこの代わり当てた大地の空白に言葉を失って立ち尽くしていた何から先に手をつけばいい絶望の中に光を
咲かすどこかに神がいるならもう一度新しい世界をこっちに開かせてくれそれでも未来へ風を吹いている頬に感じる命の居吹きそれでも私は強く言っていく逆たひとす連覇をする
ことから始めようか記憶の傷口は重たったりなって痛みの中に優しさを生んだ誰か先に抱きしめればいい温もりの中で夢を語ろう溢れた涙の分だけ何かを背負わせてほしいよ冒険
者にはならないそれでも未来へ愛は続いでる人と人とが求め合っているそれでも私は一歩歩き出すそこに忘れられた希望を拾って始めようかそれでも未来へ風を吹いている人と知
れば感じるはずさ確かに未来へ風を吹いている全てを知らないどうにくれても確かに私はここに存在する前を塞いでる誰かを伸ばして今も生きるもしも風が止んでしまっても風が
消えた世界はないんだどんな時も時をしてるように今日と夕日がそう辛い一日でもできることを始めようか生きるままに恋をしたりモデルしたりオシャレが大好き私らしくあるた
めにも絶対外さない時間手との途中大変銀行光が響く愛の風空を駆けていく行かなくちゃ正義の魔法影に運ぶ夢どんな時だって自分の力信じかけつける夜時はやる決めたこの月の
坊主この月の坊主6位の変身ピンクで決めたラジュモンを唱えてアドバイスよドキドキ愛しぎドキドキドラブルへこむ時もあるだけどそれが強くなる大道になっていくの向かい風
負けたりはしない諦めないよだって仲間がいる希望の地ほら勇気だせばどんな困難だって乗り切るよ落ち込んだ時はチャンスラジョンラップのポーズ生徒の途中大変緊急コールが
響く愛の風空を駆けていく行かなくちゃ正義の魔法かけに運ぶ夢どんな時だって自分の力信じかけつけるやる時はやる決めたこの決意のポーズでもう一度と戻れないとしゅう涙手
に散った道を歩いて本当は少しだけ見えていた明日を気にしたありふれるその言葉だけど不意に何かを信じて未来といつかまた巡り合うのだとひとかに違う気がつけばもう見える
おもかげを探す一度なくした日々はどこどこ先ほどの命の花そしてこの心がきて月暴すあの鳥風あの日を落ちるように流れて明日へWOW手探りで掴んでは消える毎日の儚さにも
気付いて曖昧なあたしを壊して静かに目を閉じる光りゆく希望でそれをしても朝日を見たいの今強く生きてゆくのだと確かに願う人と心にとどまる怖さを見つけて踏み出した時君
はどこどこめぐりゆくはこの心遂かず身の見付きやそより声と流る涙消えずにひとひらと風を纏い流れてきてどこふわり浮かぶ風今は見えないけど確かに見えたの人への方と君が
何か伝えたの私が今伝えるの欲しかった答えは今今きっときみちて見えたこと今ゆくよ変わらないあたしで日々逢う声ことが光を晴れぬように届かないままきっとこれ命の花人と
してこの心を焦がして吹き止まずは残り風あの日を落ちるように流れて明日へどこどこどこ