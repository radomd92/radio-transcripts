TRANSCRIPTION: KZOO_AUDIO/2023-03-07-14H00M.MP3
--------------------------------------------------------------------------------
地味たしみだと あなたはから買うけど二人で映すプリクラは 何よりの宝物カニザの女の子で どこか少し大胆さりげなく腕を絡めて 公園道路を歩くもっと知りたい彼のこと
ライバルにさをつけてそっと耳も染めささやく 外す企業と恋が始まる予感 あなたも感じるでしょ街で書いたラブレター 渡すその5秒前たとえ言う前に日が暮れて
さよならの時が来るずっと狂うセリフ探して 黙り込んだ今度はいつ会えると ふいに聞いたあなたが街でキスをくれたのは 門源の5秒前街で今夜眠れない
真夜中の5秒前真夜中の5秒前さよならありがとう
声の限り悲しみよりもっと大事なことさりゆく背中に伝えたくて温もりと痛みに間に合うようにこのまま続くと思っていた僕らの明日を描いていた伸び合ってるから 光がまだ
胸の奥に熱いのに僕たちは燃え逆る旅の途中で出会い手を通りそして話した 未来のために夢がひとつ叶う度
僕は君はまるだろう強くなりたいと願いないだ月の花の毛に懐かしい想いに囚われたい残酷な世界 泣き叫んで大人になるほど増えてゆくもう何一つだって
失いたくない悲しみに飲まれ落ちてしまえば痛みを感じなくなるけれど君のこと
君の願い僕は守り抜くと違った音を捨てて崩れ落ちてゆく一つだけのかけがえのない世界手を伸ばし抱き止めた 激しい光の束輝いて消えてった
未来のために託された幸せと約束を超えてゆく振り返らずに進むから前だけ向いて叫ぶから心に群れを灯して 遠い未来だってReady or K2Ready or
K2121のインハノルハワイI say yeahいつまでもあるのだろうか 俺の真上にある太陽はいつまでも守りきれるだろうかな
キワライ起こる君の表情をいつで全てなくなるのならば 二人も出会いにも感謝しようあの日あの時あの場所の奇跡は また新しい奇跡を生むだろう愛することで強くなること
信じることで乗り切れること君が残したものは今も胸に ほら輝き失わずに幸せに思い振りあえたことは 俺の笑顔取り戻せたことはありがとう 溢れる気持ちいた
キスの道に花びらのように散りゆく中で 夢みたいに君に出会えた奇跡愛し合って喧嘩して 異国な壁二人で乗り越えて
生まれ変わってもあなたに会いたい花びらのように散ってゆくこと この世界で全て受け入れて行こう君が僕に残したもの
今という現実の宝物だから僕は精一杯こきて花になろう花はなんで枯れるのだろう 鳥はなんで食べるのだろう 風はなんで僕のだろう月はなんで明かり照らすの
なぜ僕はここにいるんだろうなぜ君のここにいるんだろう もうぜ君に出会えたんだろう君に出会えたことは 俺の笑顔取り花びらのように散りゆく中で
夢みたいに君に出会えた奇跡愛し合って喧嘩して 異国な壁二人で乗り越えて 生まれ変わってもあなたに会いたい花びらのように散ってゆくこと
この世界で全て受け入れて行こう君が僕に残したもの 今という現実の宝物だから僕は精一杯きて花になろうI love, believe itI love,
believe itいつでも君の感情に愛をしやがつも願うことが愛 夢を見れば恋をすれば 誰にでも悩める日が来るからI wish, I wish, I wish
for youI love, believe itどうせ自分を騙さない失まないたいよ駆けない月のような世界 守りたい 救いたい
目線のようにいつでも君の感情に愛をしやがつも出会うことが愛 夢を見れば恋をすれば 誰にでも悩める日が来るからI wish, I wish, I wish for
youI love, believe it望むのなら叶うまで全ているより走り出していくような 心には光がさすほどないつでも君の感情に愛をしやがつも願うことが愛
夢を見れば恋をすれば 誰にでも悩める日が来るからI wish, I wish, I wish for youBaby
今が全てじゃないことだとすでにしているだろう結びゆくState of mind ここにあるDo I know why?それじゃ見えない 辿り着けない
まずは君が願いをかけてRight Face, Right Time, Get Set, Go!いつでも君の感情に愛をしやがつも願うことが愛 夢を見れば恋をすれば
誰にでも悩める日が来るからI wish, I wish, I wish for you唯一の君のために愛をしやがつも願うことが愛 夢を見れば恋をすれば
誰にでも悩める日が来るからI wish, I wish, I wishI wish, I wish for youWe don't wanna stay
with youCould you let me be a side forever君はいつも僕の薬箱さどんな風に僕を癒してくれる笑うそばからほら
その笑顔を泣いたらやっぱりね 涙するんだねありきたりだといい どうかしてるかな君を守るため そのために生まれてきたんだ吐き出るほどに
そうさそばにいてあげる眠った横顔 震える子供 胸のハートWe don't wanna stay with youWe don't wanna be a side
foreverいつかもし子供が生まれたら世界で二番目に 好きだと話そう君もやがてきっと 恵み合う君のままに出会った 僕のようにね見せかけの恋に
嘘を重ねたかも失ったものは みんなみんな埋めてあげるこの僕に愛を 教えてくれた温もり変わらない朝は 小さなその胸 エンジェーハート見せかけの恋に
嘘を重ねたかも失ったものは みんなみんな埋めてあげるこの僕に愛を 教えてくれた温もり君を守るため そのために生まれてきたんだあきれるほどに
そうさそばにいてあげる眠った横顔 震えるこの胸らよWe don't wanna stay with youWe don't wanna stay with
youWe don't wanna stay with youWe don't wanna stay with youWe don't wanna stay
with youNo, generally, I'm the aside for whether you like me or not見慣れた街並み
いつもの仲間が離れてたって旅立っていくなんて自分を探して 孤独に怯えて 共にそうした最高の回遇 つな日々 まっすぐに目を見れずゆらゆら揺れてる
それぞれの道に刺す光の波を超えていく 終わったはずの夢がまだ僕らの背中に迫る 刻まれた想い出が騒ぎ出す限られた愛と時間を
両手に抱きしめる責めた今日だけは消えないで周りを気にして 見た目も気にして何が本当か わからず歩いてるんだ悩んだ理由は 忘れてしまった僕という通報
誰か止めてくれないか日々は目指す 暗移動 大事にしまとんで大げさに果たしてた 心の隙間 褪めていた止まった時間は歪め 僕らの未来を照らす二度と戻れない夜の中で
いつまでも語り続ける永久希望の歌を 例え今だけとわかっていても俺が留まるところじゃないから青い想いを抱いていたのか習った効果
十年以上かこのままずっごなんて考え動き始めた列車の中に いつでも君はいるからあの時やの場所でまた会えるかな知って続けた瞳の奥に
いつでも僕はいるかな海辺に咲いた花のように 託話しくずっとこのまま光を 僕らの未来照らして高く舞い上がれ 終わったはずの夢がまだ僕らの背中に迫る
刻まれた想い出が騒ぎ出す 限られた愛と時間を 両手に抱きしめるせめて今日だけは綺麗 また時間はゆるべ僕らの未来を照らす
二度と戻れない夜の中でいつまでも語り続ける 永久希望の歌をたとえ今だけとわかっていても 僕たちの気持ちが振り返って耳に扉に合うくらい
明かさ絡まらん心払う誰もであわたすぎた 誰もであわたすぎたOhきっときっと誰もが 何か足りないものを耳に期待しすぎて 人を傷つけている会えば変化してたね
長く見過ぎたのかな虹を貼れば直さら 隙間広がるばかりキスをしてり 抱き合ったり 多分それでよかった当たり前の愛し方も ずっと忘れていたね信じ合える喜びも
傷つけあう悲しみもいつかにのままに愛せるようにDarkness burnいつもの悪い時には いつも言い訳してたそうね そんなところは 二人
欲にいていたね安らぎとか 真実とか いつも求めてたけど言葉のように簡単には うまく伝えられずにもう一度 想い出して あんなにも愛したことありがとうが言える
時が来るまでSake burn残された傷跡が消えた瞬間に本当の優しさの 君がわかるよ きっとすぎた日に背を向けずに 夕暮り時を感じていつかまた笑って
会えるといいねDarkness burnAh...Darkness burnDarkness burn手のひらで震えた
それが小さな勇気になっていたんだ手文字は苦手だった だけど君からだったらワクワクしちゃう返事はすぐにしちゃだめだって
誰かに聞いたことあるけど駆け引きなんてできないの 好きなのよAh...恋しちゃったんだ 多分気づいてないでしょ星の夜 願い込めてShirley
指先で送る君へのメッセージSakuraが咲いている この部屋から見えてる景色を全部今君が感じた 世界と終了を取り替えてもらうより本の一挙でも叶わないんだ
君からの言葉が欲しいんだ嘘でも信じ続けられるの 好きだからAh...恋しちゃったんだ 多分気づいてないでしょ星の夜 願い込めてShirley
指先で送る君へのメッセージ甘くなる果実がいいの何気ない会話から育てたいAh...恋の始まり
胸がキュンと狭くなるいつまでも待ってるから春の冷たい夜風に預けてメッセージ恋しちゃったんだ 多分気づいてないでしょ星の夜 願い込めてShirley
指先で送る君へのメッセージMEMOMEMOは少し長いけどでも最後まで読んだような一人ぼっちが寂しなってしまなかったIt's the past of
Jolainian GraceAs I submerge through your radiant spaceMy lady in lace, hallowed
in paceTelling my waist, you hated shading, embraceThe dreamy it tastes, the
fine foods, a hard laborWe want to speak some of God's graveYou're wanting me
loving, my behavior seems crassWe're learning you as you passSo we can bond,
wave beyond, treat smokeThe kind to get your love, but y'all remind youAnd to
black it outS stronger, an early time 이 Ledgesik集まってIt CampusIt formed a free
songTime flies, so strongEnough to zipsA fast, it's王の狂衢It starts the sailNo
matter how we tryStay safe flow so downEvery last dayもうなんて見たくないようならJust love
愛した人でも 眠ってていいよでも 最後まで言うね
ようねあんな優しい思い出がれたのは初めてだったさあってゆくけど新しい彼女のことを私のように愛さないでGoodbyeGoodbyeGoodbyeI don't
ever want to let everything about it to youStay true and I'ma never think it's
out of youCause you're the one I choseLet's grow together in a sight point
planeWho the love of my life worthもうちょいとして きっかけで花がくつづいたOhね 二人
ふざけあった時間だけ知らない間にどんどん好きになってたさようならどこで会うことなんてないんでしょうさようなら二人の部屋でも 少しだけいけどでも 最後まで言うね
ようね一人ぼっちで寂しいなんて知らなかったわ私のレモリー分かれるのがつらいことあなた愛して ラララGoodbyeGoodbyeGoodbyeでも
破って捨てていいよでも 最後まで呼んでようねそんな優しい恋がれたのは初めてだってさあってゆくけど新しい彼女のこと私のように愛さないでGoodbyeGoodbye
GoodbyeGoodbyeGoodbyeGoodbye何度も着信のチェックしてみても言うもないうほう こうちがあって聞けてもいいけどMy Bryant's
in my wayMy Prince, give me time, babyLike a night, too 時なYou're my best, so
much to see youThere's a chance, what I should beLet me down,
疲れる君も忘れていいほどに無かす予感が 合わせるでSo, so 僕よに切ない今の日に何にしないように狂って 踏むつつ 言いさせてDanger, no
近づけるの意味が咲いて 寒くて回し続けて 回りたくないから止めないでYeah, come on, yeah, come onHah, bring it to
your life直通これAstle 知れてるはずも言わそうEver since 初登場 耳から離れることでNo, no, like a soul, no,
yeah10段とスニー アレいる空間 アレいる周波数Invases, tell me, I'm your fun and gagerSo, tell me
who died6万ドルの キューマンGenerama, 空と夢中だM.Doke is a shoe downKeep in close,
もうそうしやるでUpro, キューマン ドカクてくぜ 未知風作Burn the ass, rock it up, ask for the
shoePentate, the end to end, 黙らすなく撮り巻き 喰っちゃう ジローとアクトWhy are you, why are youThis
force, my mindWhy are you, why are youThis body change is mineWhy are you, why
are youThis world is mineWhy are you, why are youこんなに 真果てでも 意味にされるしもうつまらない
関係ならばWhy don't you let me goIt's a little bit different, but it's a little
more冷たくなくてやさしいIt's special, you know, it's a treatYou must find a way to
goだってそれが 難しいI'm gonna be with youI'm gonna be with youYou know, you're gonna be
with meYou're gonna be with meYou say you're the songSong, song, it's a
song素直に言うよりSong, 僕たちからの感じままいて 続け続けてDjの力はゲットもっと熱く 響かせてさまでもワンチ続けて 帰りたくいかな止めないでCome
on, yeah, girlCome on, 遅れないよWant that, get down on the floorParty people, beat
up, beat to the beatReborn, reborn, come on, rush to AYo, say talk to the free
styleWant stop, boi, boi, s style, shotAri dake, yo die, check the sneak, I know
so dieWani ga wa, I'm the man of the hourKiri dachi, piti, fly jean, and not my
loverShaba daba, it's class 11, yoFreeze, baby, suicideWakuna, sarayu, styles,
cheesy and gamesChi karma, I play chachet through your cityKyo wa sata, day, we
get stronger, hey踊り続け続けてDjの力はゲットもっと熱く 響かせてねえ, 朝までは私続けて帰りたくみ方 止めたいでFreeze, baby,
suicideWani stay, I'm the man of the hourFreeze, baby, suicideWani go, I'm the
man of the hourFreeze, baby, suicideWani go, I'm the man of the hourFreeze,
baby, suicideWani go, I'm the man of the hourFreeze, baby, suicideWani go, I'm
the man of the hourKiri dachi, piti, fly jean, and not my loverHit the hit,
don't go, we areMise et yo, the song, songSuna Monty, yori song二人から 何か感じNo, I'm
a ninja踊り続けさせてDjの力はゲットもっと熱く 響かせてねえ, 朝までは私続けて帰り続けたらきれないでI can't say it行きたいことならどれく
らいあると分からなく溢れどる私心はお喋りだわ行きたいことならあなたには後から後から溢れてる私以外のお喋りだわなのにいざとなると不思議になる遠い場所から何度も話し
かけてるのに目と目でつじ合うかすかに色っぽい目と目でつじ合うそういう中になりたいわ無もり口なしね無もりさしないね行きたいことならあの日から誰にも分けずに溢れてる
私気持ちはわらわらだわなのにいざとなると不思議になる書いた手紙もしまい込んで誰も知らない目と目でつじ合うかすかに色っぽい目と目でつじ合うそういう中になりたいわ目
と目でつじ合うかすかに色っぽい目と目でつじ合うそういう中になりたいわ明日少し行き途絶して視線投げてみようかしら目と目でつじ合うかすかに色っぽい目と目でつじ合うそ
ういう中になりたいわ無もり口なしね無もりさしないねいえらいのよ温かい砂を上を歩き出すよ悲しい知らせの届かない海で君がいなくても太陽が覚えるの新しい一日の始まり今
日を選んだ編みたくしの線が横に続く川は分からない生けもらう私が毎日はたらく理由両手に空を腕に嵐を君にお別れをこの海辺に残されていたのいつも起きてがみ夢の中でも電
話としても声を聞きたいよ言葉が押すのが苦手な君はいつも起きてがみ忙しいと連絡たまに忘れちゃうけど誰にだって一人とはあること今日を話した徒中への人は一人でも大丈夫
だと言う居昔が私はまた考えている途中花に名前を星に願いを私にあなたをこの窓辺に飾られていたのいつも起きてがみ少しだけでもシャツの上でも君に触れたいよ覚えている最
後の一度必ず帰るよあたす楽場所を夢に続きを君にお帰りもこの世界のどこかから私を贈り続けるよ夢の中でも電話の地でも誰を聞きたいよ言葉が押すのが苦手なら今度急にいな
くなるの君は何もいらないよ君は何もいらないよ君は何もいらないよI'm down with you夜空に輝く月影は青くこの世を流れる歌声は話し何日の出るされその岩
影から聞こえてくるのが日投げしの祈り電話をささやく境界の跡よお前は知らない近づく私を気を立たない罪も戦い争されいつも組みに行くのも命がけのことInshallah
InshallahInshallahInshallah徹地にとらわれ戦場に眠る夜空の涙でおりぶわ昨日島によくぎる鉄城の思いは頂上が踏まって笑おうななんて子供らは
憂れて喋っているのにどこへ行ってしまったのイスラエルの向かみはInshallahInshallahInshallahInshallahInshallah嵐がすべ
てを葉をした葉を新たな勇気が虫を切り開くの逆へ歩みすがえるその岩壁は祈りを捧げる日投げしの姿ささやかない鳥にみんな声を合わせ歌声は踊りゆく晴れた空高くInsha
llahInshallahInshallahInshallahInshallahInshallah