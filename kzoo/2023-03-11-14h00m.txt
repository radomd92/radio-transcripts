TRANSCRIPTION: KZOO_AUDIO/2023-03-11-14H00M.MP3
--------------------------------------------------------------------------------
何もないように振る舞うから決して一人にはしないから何十年先も君を友達って思ってる夢の続き何度も話してよいい言葉かりじゃないこれまでの僕らの毎日に今だからきっと言
える本当ありがとう友よ言いたいこと言って言いたいだけ言ってわかりたいから争ってたがりの気持ちわかりあって次会う時はまた皆で笑ってたいから夢の中でも謝っておくの何
十年先も君を友達って思ってる辛い時は何でも話してよいい言葉かりじゃないこの先の僕らの毎日にこれだけはずっと言える本当ありがとう友よ夏の落としもの君と過ごした日々
洗い立てのシャツのような笑顔今も忘れられない真夜中声が聞きたくなって無意識にダイヤに回すだけど話す勇気がなくて切なさなきしめた翼を広げて旅立つ君にそっとエレルを
贈ろう誰のためじゃなくただ君のため愛してたよないさで二人サービスター飲みかけの感銃数肩を寄せて夢語り合ったあの日を見つめてたフライデナイン君の部屋へといそういつ
もの僕は無い騒ぐ人影窮屈な道街は無数さ翼を広げて旅立つ君にそっとエレルを贈ろう誰のためじゃなくただ君のため愛してたよ翼を広げて旅立つ君にそっとエレルを贈ろう誰の
ためじゃなくただ君のため愛してたよ君のため愛してたよはあ一日目には大所に立つの好きな料理を残す並べてあげたい二日目には見しもうも心残りは置いていかない三日目には
片付けをする明日の工夫とももらいておきべなくちゃ四日目にはドライブしたりは懐かしい景色がきっと新しいいつか有る目にはパーティーを開く家族の誕生日一年分は止めて向
いかめりは女子会をするの東の高ガールストップお酒も飲んだ何日かの自由を私にください神様最後になのか感動へ普通の幸せしか望むないのにそれが一番難しいのね七日目には
あなたと二人きり心満ちたりと私は旅に出るまた会えるそのいつかのためあなたの指に指をそっとつないで何日かの自由を私にください神様最後になのか感動へ普通の幸せしか望
むないのにそれが一番難しいのねあなたを守りたい窓を揺さぶ海鳴りは見れ心の叫び声見慣れた神様直せば他人に戻る濡れきゃ素肌を重ねてもなぜか心明日の今は愛されたのは眩
しいですかあなたは神様熱く揺らめく人間水にそっとおのみを投げたいのあなたに溺れしねたら幸せだから何もいらないもいやなき帰らないでと泣きながら手を伸ばしてもすり抜
けてゆく儚いから合わせ鏡の悲しみは痛みくさ恋しさ裏表約束なんて男のずるさの一つ会えば会うほど会いたくて今の終わりが怖いから愛することは幻ですかあなたは神様子供た
ちが空に向かい両手を広げ取り役もや夢までも掴もうとしているその姿は昨日までの何も知らない私あなたにこの指が届くと信じていたそらと大事な振れ合う身体過去からの旅人
を呼んでる道あなたにとって私ただの通りすがりちょっと振り向いて見ただけのいい方々も一番行く人の波に体を預け石畳の街角をゆらゆらと彷徨う祈りの声 歪めの音歌うよう
なざわめき私をお気軽に過ぎてゆく白い朝時間旅行が心の傷をなぜかしら埋めてゆく不思議な道さよならだけの手紙迷い続けてたきあとは悲しみを持てあますいほうじるあとは悲
しみを持てあますいほうじる始まりは突然嵐になる月をせられてく君の瞳始めて感じた衝動を全部心の底いてみたくなるまた見たことのない本当の心教えてよいくつもの星の名前
知って君と僕は出会った幼い日空にあげては祈ってた僕は飛ぶこの胸に生まれた想い力に変わる君とならどこまでもいける未来導く光になれ間違えることが怖くなって逃げ出した
悪な理想が時はこの手が覚えてる握りた太陽を胸に刻んで走り出して傷をつけよう見に行こう新しい世界何が待っていても生きてゆく意味を知ったからいつだって命のみたいな誓
い答えなんかなくて手探りで迷ってばかり無可能中に進んでく幼いあやまちの背負って傷跡もいつの日か強さに変えて未来導く光になる何か正解で間違えたそんなこと誰にも分か
らないけど自分の胸に生まれたこの想いは嘘じゃないいつだって命のみたいな時代答えなんかなくて手探りで迷ってつまずいて壊して僕ら生きてくこの胸に生まれた想いが何か変
わる君とならどこまでもいける未来導く光になる未来導く光になる高く燃える孤独な道を彼のもとでもない髪を旅かせ道の先にはシンヒロあの日を殺したくてどうしたんだろう悲
しい気持ちないわけじゃない遠い昔に失くしてきたの限りない喜びはあるが遠く前に進むだけで精一杯柔らかな想いはあそこにしまって夜は薄めに色の夢を見て朝は希望のぐらい
の開けることなくせめて体だけを綺麗に可愛い 可愛い寂しくはない夏の海とか 冬の街とか想い出だけが 正観体風が取りあわせ眠った初の魂が燃えるOh,
Me止めない鳥は鳥残されて胸や背中は大人だけれど限りない喜びはあるが遠く瞳離すだけで精一杯柔らかな想いにはあそこにしまってOh,
Me初めて出会った日のこと覚えてますか過ぎゆく日の想い出を忘れずにいてあなたが見つめた全てを感じていたくて空を見上げた今はそこで私を今持っているのを教えて今
痛いあなたに伝えたいことがたくさんあるね愛いたい 愛いたいし傷でもぼかで探して悲しくてどこにいるの抱きしめてよ私はここにいるよずっともう二度と会えないことを知っ
ていたのが辛いだ手をいつまでも離さずにいたここにいてとそうすまもなく泣いていたなんだ今もあなたは変わらぬまま私の隣で笑っているかな今
痛いあなたに伝えたいことがたくさんあるね愛いたい 愛いたいし涙がふれて 時はいたずらに過ぎたねえ 愛いたい 抱きしめてよあなたを想っているずっと永遠にか
伝えられなくても伝えたいことがある戻りたい あの日あの時に叶うのなら何も言わない今 痛いあなたにしてほしいことをいっぱいあるね愛いたい
愛いたいどうしようもなくて 全て呼べと願ったこの心はまだ泣いているあなたを想っているもう一度もう一度もう一度もう一度もしも願いが叶うなら吐息を白いバラに変えて会
えない日には部屋中に飾りましょうあなたを想いながらDarling I want you 会いたくてときめく恋に駆け出しそうなの迷子のように
立ちすくむ私をすぐに届けたくてダイヤに回して 手を止めたI'm just a woman falling in loveIf my wishes can be
trueWe need to use my timeThrough all this wild rosesWe're waiting for
youThinking about you every nightI'm fine away I amI am not living in your
heartDarling I need youどうしても口に出せない願いがあるのよ 今夜の夜と日曜のあなたがいつも欲しいからDarling 回して
手を止めたI'm just a woman falling in loveDarling you love me
今すぐにあなたの声が聞きたくなるのよ両手で本を抑えても心にくれる夜が嫌いダイヤに回して 手を止めたI'm just a woman falling in
loveWhen you were hereWe were thinkingWe were calling fireI'm better than some
bad weatherDarling I need a heartDarling I need a heart of oneCan't stop
youCan't hold youCan't wait no moreI'm just a woman falling in loveI'm just a
woman falling in loveDarling 回して 手を止めたDarling 回して 手を止めたStuff 星崩れ 髪を飾りStuff 優しい目を
待つを塞い月 月 切なく 震える胸No more 無しでもいい 逢いたいのにHalloweenあなたは来ない 私の想いをジョークにしないでDraw meYou
are you are sleepingYou are you are dreamingI know you love youDarling 回して
手を止めたLove is 花からの 水着だけがLove me 目立ちすぎて 泣きたくなるのJilly ジリ 焦げてる この痛みも冷たい水背に
そっと浮かべてHalloween泳ぎ出すけど あなたの夢には追いつけなくてI can't let go of me 苦しいのになぜなぜあなたじゃ
泣けたらなのLove meYou are you are sleepingYou are you are dreamingI know you love
youDarling スタートHalloweenあなたは来ない 私の想いをHalloween泳ぎ出すけど あなたの夢には追いつけなくてサミタレマ 緑色
悲しくさせたよ一人の午後は 恋をして 寂しくて届かぬ想いを 温めていた好きだよと言えずに初恋は 振り交際区の心この過去の方程を 走る君がいた
遠くで僕はいつでも君を探してた朝い夢だから 胸を離れないゆうかえは暗い色 帰り道一人 口笛吹いて名前さえ呼べなくて 囚われた心
見つめていたよ好きだよと言えずに初恋は 振り交際区の心風になった花びらが 皆も見出すのに 愛と友人 書いて見ては 震えてたの頃朝い夢だから
胸を離れない他の方程を 走る君がいた 遠くで僕はいつでも君を探してた朝い夢だから 胸を離れない胸を離れない 胸を離れない今も離れない
胸を離れない花のように咲いて 花のように散るのならただ心のまま 愛したかった風の弱い春が 辿り着いたこの場所でずっと探していた
たったひとつのものを巡り目を打って また会えたから 貴方と恋に落ちて行きたい何かも越えて 素直に乗って その瞳を添えさずに花のように眺めて
出会い分かれていくならこの涙はね 何処へ行くの 雨になっていつか 出会う誰かの語りそっと触れるように 落ちて行けたらいいあなたがくれる このまま私を
失うことが怖かった心の奥のこの出来事 秘密つけていたんだよすみざり式の夢 熱い想い きっと最初で最高の恋生まれ変わって
申し上げたなら貴方とまた恋に落ちて行きたい 涙も越えて 素直に乗ってもう一日を空さないあなたがくれる その優しさが 壊れてしまっても
構わない心の奥の秘密といらは あなただけに開くから心の奥の秘密といらは あなただけが開けるから心の奥の秘密といらは
あなただけに開くから予見だけ無理を見つめながら 当てにならない明日を振らない場いつも呼ぶは少し出しなはず 泣き出しそうな空にしたのでもI wanna make
it tonight 終わらないようにYou have no weak to cry 終わらないメロディを足早についてく時の中
揺れていくのは嘘の数だけ寂しついた夢にもたれながら 平たちを隠せずにいたけどI wanna make it tonight 終わらないようにYou have
no weak to cry 終わらないメロディをLong we are 必ずロール前のすびは トルレを見ちゃいLong we night
もう二度と見せないでLong we are 必ずロール前のすびは トルレを見ちゃいLong we night もう二度と見せないでLong we are
必ずロール前のすびは トルレを見ちゃいLong we are 必ずロール前のすびは トルレを見ちゃいLong we night もう二度と見せないでLong
we are 必ずロール前のすびは トルレを見ちゃいLong we night もう二度と見せないでLong we are もう二度と見せないでLong we
night もう二度と見せないでLong we night もう二度と見せないでLong we are 必ずロール前のすびは トルレを見ちゃいLong we
night もう二度と見せないでLong we are もう二度と見せないでI will keep you more style空色に光る銀の鐘を
駆き鳴らして歩いてゆくパレードがじまる ねえそこからついておいてよドキドキ夢見ること もしも今おくるならSmile 笑ってる君だけに 後たかな風が吹くよ
pleaseGive me something more kissGive me something more smile この胸にもうっと kiss my
princeあなたが生まれた瞬間に この世界に笑顔が増えた愛すべきその人に 出会えた時 わかるでしょうドキミキ夢溢れる 明日も同じ気持ち 人Smile
笑ってる君だけに 後たかなその声は この胸に日々 クアイの PrinceI will keep you more kissCan I take you
up?生きますのWhat the PrinceGive me something more smileI will be with youI will be
with you