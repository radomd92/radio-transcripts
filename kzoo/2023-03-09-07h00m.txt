TRANSCRIPTION: KZOO_AUDIO/2023-03-09-07H00M.MP3
--------------------------------------------------------------------------------
どこか どこかもっと遠くへ行きたいそんな気がして 君に触れた指先ふわり春は迷い降りてくる言いたいことの その半分さえも伝えられず過ごした昨日
なのに不思議でどこか気持ちに気づいてしまう とてもすぎだった想いで作り 重ねていく 今日があるのならいつかまた来る この場所に帰る日がきっとあるはずね明日
違う世界が見えるただかの私のまま 行こう 踏み出したら夜空に昨日は降り注ぐよ恋することも自分だけで勝手に難しくしてたね きっとあなたのことを 甘いよりもっと
もっと深く分かりたいなのに私の心 揺れているよ 流れる景色に素直で痛い だから急ごう 始めての街今目指して 明日 明日扉を開けて新しい旅に出よう
そして記者が走り出したら夢だけを見つめてるどこか どこかもっと遠くへ行きたいそんな気がして 二人手にしたそのキープに桜は降り続くよあなたのことを 甘いよりもっと
見つめてるどこかもっと 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを
甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと
遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと
遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと
遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたいあなたのことを 甘いよりもっと 遠くへ行きたい桜の花よ 幸せですか
それともきれいに咲くだけですか若くはないけど やよいの空に 私も春を咲かせます桜桜 悲しい恋もありました桜桜 死にたい恋もありましたこの世で最後に落ち会った
桜の花の木の下でああ 日の下で桜の花よ 切ないですか それとも一人で知るだけですと 泣きたくないけど 別れ泣きたら私も涙 敷らします桜桜 幼い恋もありました桜桜
大人の恋もありました生まれる前から 探してた 桜の花の木の下でああ 日の下で桜桜 悲しい恋もありました桜桜 死にたい恋もありましたこの世で最後に落ち会った
桜の花の木の下でああ 日の下で日の下で2809に申し込んでください 私たちの手で危険なドラッグから子供たちを守りましょう海岸 列車には
いつしか人影もまばら街を離れ 山には今も 薄雪の名残別れのその朝に 私はただ泣きながらドアの名札を 外すあなたを 黙って見ていた叫ばけ叫ばけ桜
滲む涙も鮮やかにしれしれ桜 痛む心に降り積もれ人生は春の夜 全てが 夢の中海岸 列車には やがて私一人きりでも行きなくちゃ また明日から
思い出に変えて誰ほど愛を 二人重ねてちぎってもひとつになれる 少先男と女では優しさを ありがとう さよなら手を振って叫ばけ叫ばけ桜 滲む涙も鮮やかにしれしれ桜
痛む心に降り積もれ人生は春の夜 全てが 夢の中愛を 二人重ねてちぎっても桜の花の 薄紅のかすみの中をさまようえばめぐり愛し わが恋の花びらの音
束ぬれん花びらの音 束ぬれん桜 花桜 初夏の思いつのりで恋地獄 命と思う恋なれど不意味いつと人の言う桜恋しや コハルには悲しき恋を 忘れたき儚き夢が
幻かはげしき思い 残感桜の下に 口果て真白き雪に うずむれて果てなき恋の 道雪のじょうど隣しや 雪の下桜の花の 薄紅の始まりの朝はいつでも
くずついた空模様なれない制服も 黒い靴も破り捨てたいくらいに思う僕の目の前に ただ広が闇と無理やり 描き出した光メモーバスが出る時間だよ君の家の前をとっていつも
の駅を過ぎたい昨日とはまるって違う風景みたい誰にも見せない心は小さな痛みを抱いた美しくあろうと明日けど憎しみに震えてしまった僕の新しい手帳のページはまるであの日
を書き消したみたいにねえ何人だろうねえ何一つ消えちゃいないよ硬化した奥振り抜けて誰もいない程にそって僕が選ぼっとしている未来へ桜並木をくぐって心にずっと陽が差し
た時刻してしまうと数んでしまうから硬化した奥振り抜けて誰もいない程にそって僕が選ぼっとしている未来へ桜並木をくぐって心にずっと陽が差した時刻してしまうと数んでし
まうからねえ何一つ消えちゃいないよ硬化した奥振り抜けて誰もいない程にそって僕が選ぼっとしている未来へ桜並木をくぐって心にずっと陽が差した時刻してしまうと数んでし
まうから僕がそばにいるよ君を笑わせるから桜並木季節数え君と歩いて行こう僕がそばにいるよ君を笑わせるから桜並木季節数え君と歩いて行こう眩しい朝はなぜかせつなくて理
由を探すように君を見つめていた涙の夜は月の光に震えていたよ二人で僕がそばにいるよ君を笑わせるから空のない街抜け出し虹を探しに行こういつもそばにいるよ君を笑わせる
から柔らかな風に吹かれ君と歩いて行こう君と歩いて行こう君がいるいつもそばにいるよ手のひらからこぼれて舞うよ見上げれば昨日の雨に打たれてても先誇る花凍えるような孤
独の空で泣き濡れてた切ない日々に微笑みがほら蘇る桜が笑う涙よ花びらになれ心の中に今桜が咲いた生きてる切ない想い心の中でほら桜が笑うひとひらひら輝いてるひとひらひ
ら今を生きる薄紅がハラハラと舞う目の前には花びらの道気がつけば昨日の風に散らされても優しい花よ途切れるような道の途中で諦めてたあの日の夢に喜びがほら蘇る桜が歌う
涙よ花びらになれ心の中に今桜が咲いた生きてる切ない想い心の中でほら桜が笑うひとひらひらきらめいてるひとひらひら命が咲く涙よ花びらになれ心の中に今桜が咲いた生きて
る切ない想い心の中でほら桜が笑うひとひらひら輝いてるひとひらひら今を生きる愛を染める桜と書いて愛で桜今あなたに習って書いたあれはいくつの春のこと白い白い桜が今年
も咲いて空に舞う愛でどんな色してるのと聞いたわね あの日今の答えはわからないまま明日私とつぎます愛を染める桜今あなたに習って書いたあれはいくつの春のこと愛と友人
何度も書いて書いてあけした今あなたは恋していたの今は知るすべないけれど白い白い花びら天から届く海ですかお前の色で愛せないと言ってくれますか背伸びしないで歩いてゆ
けと背中押してくれますか白い白い桜が今年も咲いて空に舞うお前の色で愛せないと言ってくれますね桜吹雪に包まれながら明日私とつぎます桜吹雪に包まれながら明日私とつぎ
ます青書の伊沢書人は心配ごとが増え時間に言われると心の寄りどころ神の存在への意識が薄れますが心の窓を開き、青書の言葉に耳を傾けてみましょうチャールズ・スタンレー
博士のインタッチ放送は月曜から金曜の午後2時15分からですあっておいでよ高校しておいでお前は預かりもんだからかえさにゃならん涙の笑顔で夕日との丸い背中を夕日が染
めるままよ、せいせいままよ、さいちゅうありがと海のおやごにあったらこの花片のに降りたと言ってたと伝えておくれ想いを振り切るためなのかうりに奇情なそぶりを見せたま
まよ、せいせいままよ、さいちゅうありがと春になったら桜が咲くそうな日本の様子や花がよりよこしておくれ何にも言うこともないけれど無事に達者に暮らせと泣いたままよ、
せいせいままよ、さいちゅうありがとさらにままよ、せいせいままよ、さらにおかえの隅で男なき一人酒を飲みながら友よなぜに先に行ったおまえの笑顔を思い出す心受け継い
未来を見つめ俺の気持ち 狂いたつ桜のように 桜のように咲かせる それが夢桜のように 桜のように咲かせる それが夢海を眺めて 波の音語り合った
あの頃の方友を思い懐かしむおまえの空を 見つめるおまえの姿 忘れないしぬき羽根上げ 悲しみぶつけ空に向かって 目を閉じる桜のように 桜のように優しく
鮮やかに桜のように 桜のように優しく 鮮やかにどこまで続く 道しるめ上りつめる 楽しさよともとともに 走り続けおまえの彼方を 見つめるおまえの魂
居あどしてる力にあぎる いくつもの壁祈りを込めて 立ち向かう桜のように 桜のように毎日る いつまでも桜のように 桜のように毎日る いつまでも人たがび
勝負に出た弱音なんか 儚いでそうよ そうです この世のことは不器用なけば 見せたがないわ苦しい時こそ わばらっていきましょう笑顔に咲き回す 幸せ咲きだ片割れ
好きで 永遠はずかもすれば 丸く収めて知らぬ彼方をそうよ そうです この世のことはわばらってしたって 始まらないわ春夏 秋冬 順番遠い笑顔に咲き回す
幸せ咲きだ大きな望みも 持ってるだけじゃただの恋も 持つ想いだけそうよ そうです この世のことは筋がきどりに行くはず 舞いはどこまでやれるか試してみましょう
努力に咲き回す幸せ咲きだきらめく日差しに 影のが燃えて 道のく 春が澄みひとり咲いて 咲いて 咲いて 咲いてただひとすじに 誰を待つのか 抱き桜帰らぬ恋を
懐かしく思い出させるみはるのさとよはらはら 舞い散る 花びらをそっと両手で 受け止める風に揺れて 揺れて 揺れて 揺れてただひたすらいい 何を思うか
抱き桜花の命の 儚さに 涙こぼれるみはるのさとよ流れる月日の 映いに姿変わらぬ 紅しだれ千年咲いて 咲いて 咲いて 咲いてただひとすじに 何を見つめる
抱き桜雪時計 水の 星せらに 心やすらみはるのさとよ