TRANSCRIPTION: KZOO_AUDIO/2023-03-07-12H00M.MP3
--------------------------------------------------------------------------------
あの日の空の二人のいそば屋のテレビが映してた知らげた人生で 誤魔れて初めて 終わり橋本ステが震えてた平変に溺れつと 彼は叫んだけど 信じた者は皆
埋めっきが剥がれてくI will never cry この世に何を求めに生きている 叶わない夢だろう追いかけるオールや文字やな 悲しくて泣いたら
幸せが逃げていちまう一人寂しい夜 涙こない手にしたI will never cry この世に何を求めに生きている
叶わない夢だろうまた地震に浮かれたあの子の思い出すもう一度あの日に 迎えたいあの子の 空が腐っている 色づいた水辺を生まれ変わってみても 英語の男にゃなれない
お願い聞けるじゃけ?お待ちきる汗を 追いでいく彼方は 愛も知らずに満足が喜びをなれかと わがちがうのが人生した優しさをありがとう 君に惚れちゃったろう
立場があるから口に出せないけど サガヤの褒めが出る呼んだうちでさ 足が触れたのは わざとだよ半月が都会のトペイルの谷間から このチョコチョイと 俺を睨んでいたI
will never cry この世は 弱いものにを詰めたいの終わりなき旅路を 明日天気にしておくと 恋人に出逢えた色渡る場所をいつで出そう 命はずむけるように
可愛いあのことにしてI will never cry この世は 弱いものにを詰めたいのI will never cry この世は 弱いものにを詰めたいのTill
soon you shoot the moon and it's completely 今夜は 真っ白なのに 見えないAnd now you left to
face the blue 今夜は 真っ白なのに 見えないThe empty room once smelled sweetly 今夜は 真っ白なのに
見えないOf all the flowers you plucked in the fall and late you blew the razorWhy
you had to reach bill only was it just the seasonAnd now the fall is here again
You can't begin to give in, it's all overWhen the snows come rolling through
You're rolling too with some new loveWill you think of times you told me that
you knew the reasonWhy we had to reach bill only was it just the seasonOh, oh,
oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, ohWill you think of times
you told me that you knew the reasonWhy we had to reach bill only was it just
the seasonOh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, oh, ohはやりの歌も歌えなくて
ダサいはずのこの俺おまえと離れ1年が過ぎ いい男になったつもりがそれでもこの年まで俺が育てた肌がの心はおしゃれをしても 車帰っても
結局変かもないまま早く忘れるはずも ありくれた別れをあの時のメロディが 思い出させるすでとで夢と おまえ抱いてた頃ふたらないことだって
2人で笑えたね今夜も風の香りは あのことと同じで次も恋でもしてる 辛くないのに夜道みたいな始まりが 2年も続いたあの恋初めておまえだいたい夜空
俺の方が震えてたね恋は石ころよりも 溢れてると思ってた涙やモンドより 見つけられないすぐるって後で2人 涙ついてた頃どっちから別れ話
強いかかけてたあの頃に戻れるなら おまえを離さないすでとで夢と おまえ抱いてた頃ふたらないことだって 2人で笑えたね今夜も風の香りは
あのことと同じで次も恋でもしてる 辛くないのに始まりの朝はいつでも 崩した空模様慣れない制服も黒い靴も 破り捨てたいくらいに思う僕の目の前にただ広が
闇と無理やり描き出した光でもパスが出る時間だよ 君の家の前をとっていつもの駅を過ぎたい 昨日とはまるって違う風景みたい誰にも見せない心は
小さな痛みを抱いた美しくあろうと明日けど 憎しみに震えてしまった僕の新しい手帳のページはまるで あの日を書き消したみたいにねえ何ひとつ消えちゃいないよ
硬化した奥降り抜けて誰も居ない程度に沿って 僕が選ぼっとしている未来へ桜波記憶ぐって 心にずっと陽が差した 時刻してしまうと
数んでしまうからねえ何ひとつ消えちゃいないよ 硬化した奥降り抜けて誰も居ない程度に沿って 僕が選ぼっとしている未来へ桜波記憶ぐって 心にずっと陽が差した
時刻してしまうと数んでしまうから桜波記憶ぐって 心にずっと陽が差した 時刻してしまうと流されはしないからなんて 初めから飛ばし君気にしなきゃ怖いものなんて
向こうからそれてゆく上にあるのはいつもの空だけ 特に何も変わらないんだけどただ一人の野郎 引き入らないそう手にした跡に隅にいけるよう
常に監視してたい気分だよなくさないように胸に誓うでも 大事なのはそう 君と揺れていたいよこれを忘れてはいけないのだ 触れてみたいよそれはまだ早い
その前にやることが山積み人は恋で変わるんだ 僕がその中の一人君が小さく揺らついて 幕が上がるストーリー天ぼやクリズムを歩くように
一つ一つを噛み締めながら君と僕の野郎 一人来たりそう幸せが一きくのはまりにも 矢をなことだとわかっているけど確かめた時は起こらないでね
君とはどんな時は笑ってないよ これものそかにしてはどうだあらしてたいよ 同じ時間を過ごすなら 直さなそうだ君と揺れていたいよこれを忘れてはいけないのだ
触れてみたいよそれはまだ早い いいじゃない 全てに期待をかけるのはさすがに思いけど二人に未来を これくらいがちょうどいいまだ見ぬ君を 先に探しに行こうNo,
No, No, No, No, No.回る時代 終わる予感 変わる風吹き古くねど 指さされ 笑われても長い旅を知った 人にじゃなかったそばにいたくれた
君たち信じているWe are forever, yeah, yeah, forever冷た闇に歌えWe are forever, yeah, fighting,
never諦めはしないWe are forever, yeah, yeah, forever僕らと並び上げるまだ生きる 生きてやるWe are forever,
yeah, yeah, yeah, yeah止まる未来 詰まる言葉 閉まる扉に辛い時と 道はずれ見失っても黒い雨の群れ 力見せてやれ消えかかっていても
命を信じているWe are forever, yeah, yeah, forever希望の歌を叫んでWe are forever, yeah,
fighting, never食いたりはしないWe are forever, yeah, yeah,
forever僕らに比べあげる不思議上であるためにForever, yeah高い夢を見て終わらない悪の守り抜く肌を今でも信じているWe are forever,
yeah, yeah, forever冷た闇に歌えWe are forever, yeah, fighting, neverきらめはしないWe are
forever, yeah, yeah, forever僕ら空にあげるまた生きる 生きてやるForever, yeah不思議上であるためにForever,
yeahForever, yeahForever, yeah無理なんだいた気をよく聞く順調騎士せんめい教えて何を迷っているの?カットバセイ!作にもないとしょもな
い独却のドラマをね走ろう誰も真似はできない吐き潰した靴ほうり投げて太陽をときす7秒 数えたら始まるスペシャルレス虹の星に飲まって空高く 愛への未来だって動かすよ
振り返るサイン見つけたら全然が踏み出すよ独りじゃないAre you ready? OK! Come on! Let's go!行きよよ
浮き生きて天移も本音がミッションまだまだ眠っているはずだよ本気ださどうすればどうなるの?不安はね
進回の回歩だよ星を生み出していく細胞に広がる新しいリズムを感じて7秒 数えたら始めるFree Down
Room虹のリズムの空を進め抜け飛び切りの明日だって突っ越え出す不利きさFunding One More戻らないそういけない場所なってない手を取ってAre
you happy?I will make it together全霊そんなのいらないからさ突き進もう虹の灯に乗って空高くありえない未来だって無事達表ブルーに変
わるくさい見つけたら全部不全開踏み出すよがもしられるキュメらしくAre you ready?Baby okay?Come on let's goFunding
One More時は過ぎてゆくWell Well Well One Moreもう何やってんだHoly Holy Holy遅れるなよさクラクラ響いてないでほらマジ
時間がないどけいなこと考えないでそうわけまくろう昨日までの愛を忘れてやり直そう耳ビントのハパット心臓独特そうセクシーな運動を感じて僕とShake it
upOver and Over Tonight今夜は終わらないOver and Over and Over明日は舞ってないOver and Over and
Over and Overだだだだだ歌えOver and Over and Over and Overみんな騒ぎ踊ってNo No No いるのは誰?No No
No 隠れてないで見つかったらじっとしたてキミをキープするからどんどんキミにハマってくしもうキミを話したくないし秘密メインPinky
Promise一夜だけの恋Soul
Related乗り方が分からないなんてマジウソだろう適当でもいいから僕とほら踊ろう耳ビントのハパット心臓独特そうセクシーな運動を感じて僕とShake it
upOver and Over TonightOver and Over TonightOver and Over TonightOver and Over
Tonight音は鳴り止まないOver and Over and Over and Over誰も止められないOver and Over and Over and
Over燃え上がるのは今だOver and Over and Over and Over足りないなら叫べただ過ぎ去る時間を輝かせたい全てをかけてでもJust
Play All DayOver and Over TonightOver and Over TonightOver and Over TonightOver
and Over Tonightこんなは終わらないOver and Over and Over and Over and Over
and明日は待ってないOver and Over and Over and Over and Over andタタタタタタ歌えOver and Over and
Over and Over and Over andみんなさわけ踊れ最高の場所ハイテンション用意したさあ出かけよう夏のエリッキーラブチャンス描いたページめくのW
owこの夏こそはあげていかなきゃおしゃれもしないでどうするつもり肌もいつもより出ててもいいんじゃないグロスだってたいよ聞かせてくれ失敗が怖くて何ができる温泉怖が
っていちゃい何も始まらないオカマラシで行こう最高の場所ハイテンション用意したさあ出かけよう夏のエリッキーラブチャンス描いたページめくのうせをラブストーン叫んでみ
つけこの浜辺で海には女どうし質十文り上がると夏の集団自由研究の手間つまり恋調べてみよう白いワンピースニング上目遣いとっておきの作戦当たってくだけどそんな時にあな
たの瞳に覚えて一言先に踊って気をつけたのはあなたも消えない先行の花火のような小さなゆだねじゃない打ち上げ花火のような大きな音が鳴った研究結果がましたハーピュー答
えはあなた判決はユーザイだからって言えたらいらそうな夏ワンワンワン最高のファッションハイテンション用意してさあ出かけよう夏のエリッキーラップチャンス描いてページ
めくの研究結果がましたハーピュー答えはあなた判決はユーザイだからって言えたらいらそうな夏ワンワンワンエリッキーラップチャンスアイテムアイテムアイテムアイテムアイ
テムアイテムアイテムバーブル言葉を読むミックスミックスでユニカをチェックアイテムアイテムアイテムアイテムアイテムアイテムアイテムアイテムVoodooVoodoo
VoodooThis wayもう何も言わないで癒せないこの痛みも聞きたくないな 臭めWith all my heartWith all my
soulすぎたことは忘れようWith all my mindWith all my strengthさみだ流れてゆくわI've been waiting for
so long時と共に消えてゆくのもWake up from this silence甘い言葉に誘われてYeah, yeah, yeah,
yeahストローライン上地再発生よ何戦と完全もやつでIt's tragic, 何戦モノラプルスタメコCall this the banquetsLow MCs
best look outSkull cap, めっすRemember meI'm the one you used to
dieでもそこらの奴とは毛が違う今現在過去の話とは永遠ないYou heard it's N-I-K-AIt's payday, サケーケーMy gay mate
is verbalI say K-designIt gets one day play子供も騙し 動かすゲーI'm not dead and
vied透明で流れる光のようCome in your way空がするカスカスと並ぶ調校そう見る過去 無音を停留Let me rock your
cradleYou never figure outThis may not knowYou're not rockin'色はK-GoI've been
waiting for so long絵本人にそれでいいのもThings like broken
promises甘い言葉に誘われて心の絆でいつまでも忘れられないのでしょうUnheard is the word真実までないI guess
why我がのサープをゲット君は劇かなThat's right心の奥に聞かせてくれよI'm not the oneそう言うよ
叶えてくださ入れてつけならいらないのどこからどこなく聞こえてくる男From the underground to a sceneThat is minus 10
degreesI'm me, we hear it all限りないこのflowUnforgettableいつまでもYes yes on and on to
theこれもくなしWhen I flush down your couchWe do it thisHit or askお先を飛び入れてTouch with
me, yeahYo what's your station?IDWhat's that same combination?With all my
heartWith all my soulWith all my mindWith all my strengthWith all my heartWith
all my soulWith all my strengthYeahI've been waiting for so
longねえ本当にそれでいいのTWICERocking promises甘い言葉に誘われてYeah, yeah, yeahI've been waiting
for so long時と共に消えてゆくのRocking promises甘い言葉に誘われてYeah, yeah, yeahComing back fast
to the labSo let's have funCheck it outComing through, coming throughComing
through, coming throughあの空を越えて生まれゆく魂よNo, no, no翼なんかないで気付いてからひと強くなれた気がする大地を消えて走り出
そうきらめくあなたの姿についていける人になりたい昨日抜き捨ててもう一度夢を見よう今こそOh, match you
up!愛を受け止めてどんな時だって私はいつもそばにいるよ壊れそうだった日も譲れなかったあの夜さえも誇れる時がいいさOh, toss and rollあの虹を越え
て響き合う魂よ誰にも負けないハーブレイク踊り続けてた朝の風が虚しかった帰るとこなんてなかった日々悪いことばかりじゃないよね街のさよならが道でも今あなたに会えて生
まれ変われそうだよいつでもOh, match you up!信じていてね自分以外に大事と思える人見つけたからこの色一級で消えそうな暮らし負けたと思ってたあたしに
は何かができるよねあの虹を越えて響き合う魂よ一つになりたい愛こそ全てだねOh, my love!今こそ Oh, my true love!愛を受け止めてどんなと
きだって私はいつもそばにいるから壊れそうだった日も譲れなかったあの夜さえも誇れると綺麗さを訪れるあの空を越えて生まれゆく魂よ誰にも負けない自分に負けないで傷つい
てもいつでも自由さ二人で旅立った未来へOh, my love!Oh, my true love!Oh, my love!Oh, my love!Oh, my
true love!Oh, my love!Oh, my true love!Oh, my true love!勝ち、助い、
massage,打てたくなったさ、大会に俺らを見てどんなには言えないねえ みんな もう もうココの カラー ココの カラーおほりのりの オリのりのけはろきろひ
アロキロキロキロおらえとおはひび アイケクおはひびアイケクおはひび エホンランリーおえいけくおろんど アオヘリキランランマイコリポリポ
エホンリポリポおかあがとをしてベあちマイコリポットセリフリāクエル国あんのあんのか井のシュージ以前にうたし 将来Coco no
KalaOlimpolinoAloghi loghi hohohoIkku ahibiIkku ahibiAko o namiOikiku alonoAho
helikilalaMakulipulipoEho o nipulipoOkalaa作詞・ RoflYou Didn't Knowササ・건You better
go easy on meCome on baby,L wowCome on baby,L wowCome on baby,L wowCome on
baby,L
wowんmemememememememememememememememememememememememememememememememememememememe