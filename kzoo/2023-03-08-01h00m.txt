TRANSCRIPTION: KZOO_AUDIO/2023-03-08-01H00M.MP3
--------------------------------------------------------------------------------
なんですよねだからこの収支席が今度もしロシアを公式訪問するとですね安価な中国製ドローンがまた一気にロシアに流れ込む可能性があるそうすると戦争はさらに義化するとい
う可能性が出てくると思うんですよねどうなんでしょうかこれ中国としては今のように知らんかをしながら流れてる方がいいのか国としてもう共有するよと言うのかこの辺どうで
すこのね国として共有すると言ってしまったとたんにやっぱりアメリカとか政法との関係は決定になるのですよね今ロシアは明らかに悪者になっているのでそっちに交信に加担す
ることはできないのでこのグレーン像をうまいことを利用してあのさらに事実上のロシア支援をするとロシアにオンを売るということをやると思うんですが気をつけなきゃいけな
いな要するに大量にこういう安いのが出回るのは犠牲者としては便利かもしれないけど民間人の議世は間違いはかけますかそうですよねそのことやっぱり論理倫理的にねそうです
からこの中国もやっぱり倫理的な責任とはついてもありますこの時間のニュースです今日は国際女性でですがイギリスの雑誌エコノミストは先進国を中心とした29カ国を対象に
女性の働きやしさんを指標化したランキングを発表し日本は下から2番目の28位でした再会は韓国で日本と韓国は少なくとも2016年から7年連続で同じ順位となっています
ランキングは給与や教育の水準それに労働参加率の断除格差など10項目をもとに順位をつけましたガン細胞を生み出す元になるガンカン細胞が大腸がの再発につながるメカニズ
ムを突き止め細胞内の特定の遺伝子を除去すると再発を抑制できることが分かったと九州大学の研究グループがアメリカの科学雑誌に発表しました研究グループは従来は不可能だ
ったがんの完全な治療につながる可能性があるとしています自動車が事故や故障で走行できなくなりインターネットで検索した業者にデッカー車での権威を頼んだところ妨害な料
金を請求されるトラブルが相次いでいますある村外保険会社に入った報告ではタイヤがパンクして動けなくなったドライバーが現場に到着したレッカー業者に10万円の前払いを
請求されました関係者によりますとレッカー移動の料金は時間帯や距離で異なりますが平均23,000円程度です内閣府が実施した世論調査で日本が戦争に巻き込まれる危険が
あると答えた人はどちらかといえばお含めると合わせて86.2%で回答の選択肢が今回と同じになった2009年以降で最も多くなりました担当者らは中国の同行など国民を取
り巻く安全保障環境が一層厳しさを増したという認識の現れではないかと分析しています適用法です今日の関東地方は晴れて天気の崩れば大でしょう空気がやや乾燥しそうです最
高気温気能より高く今日は21度くらいでしょう4月下旬波の温かさになりそうです明日はおほぼ晴れて南風が強まるに込み明日の最高気温22度くらいになりそうです森本田け
ろスタンバイスタンバイトークファイル皆さんのメールをご紹介します今日はですねコロナワクチンの接種高齢者などは年2回それ以外の人は年1回これでいいと思いますが効果
信用できましょうかということで伺いましたはい最多県川口市の藤本さん38歳の女性の方コロナワクチン減らしすぎじゃないんですか先月近所の内下で3回目のコロナワクチン
を接種した時にドクターは3ヶ月相手いれば次が受けられますと言っていましたそれと比べると1年に1度ではワクチンの効果は下がりすぎじゃないかと思いますねインフルエン
ザと同じご類に引き下げてもインフルエンザのような流行期が限定されているわけではないのにその点考慮されていないんじゃないかというふうに思いますなかなかねーそう思い
ますよ普通ねインフルエンザと全部一緒にしちゃっていいのかいっていうねー満たかし30代の女性から家族がコロナワクチン接種したのにコロナにかかってしまいました私はワ
クチン接種は自病があるので見送っています結果1回コロナにかかりましたけれども重症化することはなくて加分症等区別がつかなかったああんですワクチンも本当に効果に意味
があるのか疑問に思いますねこちらの方はまあワクチン本当に効果があるのっていうそういう方ですねそれからこちらは気を接種の鈴木さん67歳夫婦2人とも子どもを支援する
仕事なので効果を信じて2回接種したいのですが有料化したら接種は諦めます年齢だけでなく仕事に応じて無料化も考えてほしいと思います今の予定ではこの無料は2024年来
年の3月までということになってますけれどもどうなりましょうかねー国立市の55歳の男性の方周りでも感染された方をたくさん見てきましたが皆さん決まって言っているのが
複数回の接種をしておいて良かったという話です医療に生通しているわけではありませんけれども感染された方たちがかかりつけ医から言われたのは複数回打っていなければもっ
と大変でしたよということでした新しいルールでは私は年齢的には年1回しか打てなさそうですがそういう話を聞くとやはり複数回を希望したいですね立川市の50代の男性十分
だと思いますよインフルエンド並みにするっていうのはもうかかっても仕方がない重症化は防げないということでしょう毎年夏葉と冬から春にかけての年2回山が来ていたのでそ
の前に打って重症化だけ防ぐ今はそういう風になるんだと思いますねー千葉県市川市の53歳森川かなこさん納得しませんねむしろ打たなくてもいいんじゃないんですかこれまで
4回も5回も打たせたのに結局何度も感染の波が来ているんですよそんなにたくさん打っても聞いたが聞かなかったかわからないものをまた1回だ2回だというな納得いきません
ねなぜ私こんなに怒ってるかというと去年受けた3回目の接種で未確以上と全身の激痛を追ってしまったからなんですよ報告困難になって一時はついば必要になほどでした日常生
活に指標を来たすほどの副反応があるのに効果をきちんと明示せずにワクチンワクチンというのはもう疑問だと思いますよ私周辺でも副反応に悩む人たくさんいますよ神奈川県安
嶋市の40代の40代の女性の方失礼効果が十分かどうか私には分かりません去年までは3ヶ月で効果が落ちるから3ヶ月経ったら次の接種をと言われましたよねそれが年一回で
すか若い人は12ヶ月間開けても大丈夫という説明聞いたことがありませんよそろそろ専門家の出番じゃないんですか本当ですねちょっと話が違いすぎちゃうからね 世田が役の
50代の男性50代というのは高齢者じゃないので年一回に分類されますよただ50もすぎればだいぶ体に型が来て基礎疾患のようなものも感じますよそのような立場から言わせ
てもらえば年にかい打たせてくださいよ一回じゃ不十分ですよアダチクの島田さん55歳の男性の方 信頼できるもできないもその判断に必要な情報全くないでしょう政府は何か
というと専門家の判断でなんて都合のいいように専門家持ち出しましたが今回どのような科学的なエビデンスがあるのか説明もありませんねご類に移行の件もマスクの件もなんと
なくという噴色しかなく全くもって科学的ではありませんなのでマスクと同様ワクチンについてももうみんなやりたい人だけやるとなって収束なんて遠い夢物語のような気がしま
すねもう政府が信用できないという話になっちゃうわけですね国分自身の糸をしげる3男性の方1回目は80%あった接種率も会が進むにつれてどんどん下がっています今更ね1
回とか2回とか議論してもしかだがないんじゃないんですか世の中もうインフラエンザ並みに感じてる人が多いんでしょう群馬県高崎市の40代の女性効果あるのかなぁ今まで言
った言っていた話と違うんですよ何かワクチンの扱いが雑になってしまったねそうした空気の影響も悪くもしれませんが私もう歌なくてもいいのかなと思うようになりましたそも
そも副反応つらくてできれば打ちたくないと思ってたんですよこの程度のものならそんなに無理しなくてもいいんじゃないんですか茨城県62歳の男性根拠はどうなのか詳しい説
明何ですか国民にわかるように説明できないから盛りかけ問題みたいにうやむやにして幕引きしちゃうんですか命にかかわることなんで心配ですよみんななんかねーあやふやだな
あいまいたなぁという気持ちのもやもやもやもやしてると人生ですねtbs ラジオこの後はパンサー無界のフラットです福島県の60代の男性厚労省の専門部会で決めたことで
しょうけれども当時かがありますね厚労省は年2回の接種でいいとした理由これまでの接種でモデルなファイザーアストロゼネかどれが良かったかその結果今後どれを打ったらい
いのか国民にわかるように説明しなさいよ不説明のまま上位下達では3年間コロナに本労されてきたを国民に失礼でしょ怒っている方もいらっしゃいましたtbs ラジオ
行く島広市のおはよう一着船森本竹ロースタンバイをお送りしましたこの番組はブルクベーカリーハワイ チャーリーズタクシーエルダース栄養科学研究所 広市電気関西山と
佐藤制約takeout ケータリングの立つオズ メディケアスペシャリスト小林木和子以上各社の提供でお送りしました ladies meこれが唐它春の 長さを
あなたと行くの砂に 足跡を 残しながらはじめて 私の うちに 行くのよ恋人が いつか できたらば うちへつれて おいでと 言っていた 父 夢に
見てたの愛する人と いつか この道 通る その日をお茶を 運んだ 生地の外に父と あなたの 笑う声が聞こえてきたのよ とても 明るく幸せな癖に
なぜなけてくるの母の 微笑み 胸に 染みたは 帰るあなたを 見送る道はおぼろ 月夜の 春の 彩いんだのケイスに いちに いちまる 銀鋒の ハワリー何かを
決めるとき どんなことを 基準に しますか今日の インタッチで 考えていきましょうキリストのうちにあるものは 何を基準にして
判断をしたらよいのでしょうこの世の知恵でしょうか それとも神の知恵でしょうかキリストにある 確かな 歩みをしたいなら 神の知恵により頼むべきですでは
古臨扉人への手紙第1 2章6説から16説のチャールズスタンレアの メッセージをお聞きください古臨扉人への手紙第1 2章6説から16説をお開くくださいしかし
私たちは 聖人の間で知恵を語りますこの知恵は この世の知恵でもなく この世の過ぎ去っていく 支配者たちの知恵でもありません私たちの語るのは
隠された奥義としての神の知恵であってそれは神が 私たちの栄光のために 世界の始まる前から 争め定められたものですこの知恵を この世の支配者たちは
誰一人として悟りませんでしたもし悟っていたら 栄光の主を十字架につけはしなかったでしょうまさしく聖書に書いてある通りです目が見たことのないもの
耳が聞いたことのないものそして人の頃に思い浮かんだことのないもの神を愛するもののために 神の備えてくださったものはみなそうである神はこれを見たまによって
私たちに敬意されたのです見たまはすべてのことを探り 神の深みにまで及ばれるからです一体人の心のことは その人のうちにある例の他に
誰が知っているでしょう同じように神の見心のことは 神の見たまの他には誰も知りませんところで私たちは
この世の例を受けたのではなく神の見たまを受けましたそれは目組みによって神から 私たちにたまわったものを私たちが知るためですこのたまものについて話すには
人の知恵に教えられた言葉を持ちいず見たまに教えられた言葉を持ちいますその見たまの言葉を持って 見たまのことを解くのです生まれながらの人間は
神の見たまに属することを受け入れませんそれらは彼には愚かなことだからですまたそれを悟ることができませんなぜなら見たまのことは
見たまによって分きまえるものだからです見たまを受けている人はすべてのことを分きまいますが自分は誰によっても分きまえられません一体誰が主の見心を知り
主を導くことができたかところが私たちにはキリストの心があるのですパオロは
むなしいこの世の知恵というものがあることを述べていますけれどもこれから語る知恵はこの世の知恵とは異なる知恵ですそれは私たちのうちに住まわれる 神の見たまからのも
のです私たちに例的な動作力があるとは私たちのうちに働いておられる神の見たまがある特定の状況についての真理を私たちに明らかにしてくださることです例えばある状況にお
かれ突然どうすればよいのか分からなくなるとき主に教えてくださいといなります主よ今あなたからの動作が必要ですこの状況の中で私はどうすればよいのですかと誰かから選択
を迫られるときもあるいはアドバスや知恵や相談を求められるときもあなた方のうちにおられる見たまは全てをご存知です彼らの中でどんなことが起こっているかもご存知ですそ
の見たまが何をしてくださるかというとあなた方に動作を与えてくださるのですつまり見たまがあなた方のためにそのことを明らかに示してくださり神の知恵はどう考えるかを教
えてくださるのです私たちは例による動作のないままに何かを求め失敗することがよくあります皆さん神の人は焦って決めてはいけませんまたパールはこの箇所でクリッシャンで
あれば誰でも例によって分きまえることができると言ってはいませんこのことを理解しておくことは大切です彼は見たまが彼のうちに働かれることの結果とこの世の知恵とを大比
していますこの世にも知恵はありますが神はその知恵を愚かなものにされたとパールは言います14節には次のように書かれています生まれながらの人間は神の見たまに属するこ
とを受け入れませんそれらは彼には重かなことだからですまたそれを悟ることができませんなぜなら見たまのことは見たまによって分きまえるものだからですけれども私たちの知
っている多くの人々は私たちとは全く異なる立場で全く異なる原則に基づいた判断をします私たちは世の人々が見ていないことを見て判断をしますパールは6節で何と言っている
でしょうかしかし私たちは聖人の間で知恵を語りますこの知恵はこの世の知恵でもなくこの世の過ぎ去っていく支配者たちの知恵でもありませんしかし私たちは知恵を語りますと
あります私たちは物事を神の視点で語ったり告げたり説明したりする特権や力が与えられています人が神の視点から何かを語ることのできるただ一つの方法はうちに済まわれる精
霊によって脳みなのですパールは例による動作力とこの世の知恵を逮捕させています例による動作はこの世の組織とは愛いれません私たちが生きているのはこの世の組織の中です
私たちはESが来られるか主が私たちを呼び寄せられるまでこの世から出ることはできません私たちをこの世の組織の一部ですそして私たちが目されるかそれはESが来られるま
でここに留まっているのですさらにパールは7説でこのように言っています私たちの語るのは隠された奥義としての神の知恵であってそれは神が私たちの栄光のために世界の始ま
る前から争かじめ定められたものですここで注目をしていただきたいことはまず奥義という言葉です奥義というのは人間の理解力を超えたものですそれは掲示によって理解を与え
られて初めて分かるものです自分で考え出すものではありません隠されていた奥義ではこれですキリストの十字架は全ての人類の希望であったしまた今もそうです人が十字架とい
うシナリオを作り出すことは不可能です神の一人後によってなされる救い十字架で神の見込みが血を流し信仰によってYesキリストの死と放無理と復活を信じ受け入れるだけで
全ての罪が許されるというような計画を人が考え出すことは決してありませんそのようなことは意味をなさないのですこの話を誰かにするとそんなことは信じられないと言うでし
ょうどうして信じられないと思いますかそれは人には全く意味をなさないからですこれは神の知恵です私たちが神の見心を知ることのできるただ一つの方法は神が掲示してくださ
り神が私たちに知恵や悟りを与えてくださることによってのみです私たちには理解できないことを悟り理解することができるように神が見た間によって私たちに動作を与えてくだ
さるのですここで皆さんにお話ししておきたいことがありますクリッシャンであれば誰でも神の見心を知ることができ礼的な動作を得ることができると言っているのではありませ
んそのようには言われていません皆さん皆さんが肉によって生き神に生きしたがわず罪に身を委ねて恋に神に染むき神の立方かしかつ自分の行いを正当化するそのようなことをし
ていて神の見心を分きまいしていることができるなどとは思わないでください見玉によって愛むとは神の見心を完全にご存知の私たちのうちに自分でおられる見玉に従順にしたが
って愛むことですコリントビトリの手紙へ戻りますがパールは2章11節でこのように言っています一体人の心のことはその人のうちにある礼の他に誰が知っているでしょう同じ
ように神の見心のことは神の見たもの他には誰も知りません皆さん愛うんでいるときに私たちは素早く意識決定をすることができるのでしょうかそれはクリスチャン一人一人の心
の中に住んでおられる見玉と同じ保障で調和して一致して愛うんでいるときですパールは12節で言っています私はこの世の知恵のことを話しているのではありません私たちはこ
の世の礼を見たまではなく神の見玉を受けましたそれは恵みによって神から玉わったものを私たちが知るためです私たちは見玉によって神から与えられた知恵を語ります私たちに
神の見言葉が与えられているのは特権です神の見言葉は神の知恵ですけれども見言葉が与えられていることと見言葉を私たちの頃にとどめある状況において見言葉をどう適用すれ
ば良いかを知ることは別のことですもう一度12節のパールの言葉を聞いてみましょうところで私たちはこの世の礼を受けたのではなく神の見玉を受けましたそれは恵みによって
神から私たちに玉わったものを私たちが知るためです愛する皆さんよくお聞きください目組みによって与えられているものが聖書には本当にたくさんあります私たちは誰でも自由
にそれを受けることができますいける神の変わることのない誤しのない知恵が書かれている書物はただこの聖書だけです僕べきことではありませんかこの聖書の中に神の知恵が書
かれているのです神が礼によって脇巻えることができるようにしてくださいます礼的な悟りが得たいなら私たちは神に十分に下顔なければなりません皆さんにお聞きします何かを
決めるときに神の見心がわかるようになりたいですか人々が何か困ったことがあってあなたのところに来てあなたに心を開いて相談をするそのようなクリスちゃんになりたいです
か神は何が問題かにズバリ焦点を合わせることができるようにしてくださいます神が考えておられるように考え神が見ておられるように物事を見る神がついてくると人々があなた
のところへ問題を持ってくるとき何が問題なのかを神の心理によってサトロクトができるようになりますクリスちゃん一人一人のうちには神の見玉が住んでおられ見玉は心理を分
かりやすく教えたいと願っておられます次にパウドが13説で語っていることを見てみましょうこの玉物について話すには人の知恵に教えられた言葉を持ちいず見玉に教えられた
言葉を持ちいますその見玉の言葉を持って見玉のことを解くのですこれは見玉だけができることですですから私たちは見玉の奥にを理解する必要があります見玉の奥にを理解でき
なければ例によって分きまえることができませんでは次の14説です生まれながらの人間は神の見玉に属することを受け入れませんそれらは彼には愚かなことだからですまたそれ
をサトロクトができませんなぜなら見玉のことは見玉によって分きまえるものだからですここでは生まれながらの人間のことと失われている人のことが語られています生まれなが
らの人は神の見玉に属することを受け入れませんどうしてでしょうそれは神の見玉に属することは生まれながらの人には愚かであり理解できないからですそれは例によってサトル
ことなので分からないのです見玉が真理を明らかにして教えてくださらなければ誰も分かることはできませんですから失われている人は神についての深いことを理解できないので
す続けて次の15説を見てみましょう見玉を受けている人はすべてのことを分きまいますが自分は誰によっても分きまえられませんここではパオルは何を用としているのでしょう
かクリシャンは誤ちをおかわさないとは言っていませんすべてのことを分きまえるというのはあらゆることを判断するということですパオルは私たちのうちには見玉が住んでおら
れるのだから見玉によって逢いむとき私たちの判断はすべて動得的な聖書の原則に戻すと言っているのです何かの意識決定をしなければならないときその意識決定の過程でたいず
神はどう言われるだろうかに立ち替えるのです例的な人はすべてのことを判断する例的な人はすべてのことを神の視点で捉えたいと願うという意味です15説の後半を見てみまし
ょう自分は誰によっても分きまえられません判断されたりしませんこの部分は私たちは他の人のことを捉うことができるけれども他の人は私たちを捉えすことができないという意
味のように思われますしかしそうではありませんパオルは次のようなことを言っていると思います私たちはイエス・キリストにすべてを委ね見玉による思いを持っているので見玉
によって歩み例の原則に従って判断をします私たちはこのような言い方をしますそれで世の人と対立することになります私たちはこの世の基準に従って判断されないしこの世の判
断に固質しませんそれで世と対立が承知ることがあります私たちはどのような鳴り雪になっても人を怒らせることになっても人から理解されなくてもどうしても例の基準に従った
意志決定をしなければなりません理解できない人は私たちとは全く違う基準や判断で動いているからですあなたは生じくした例的な判断ができますか自分で試してみてくださいあ
なたの心が正しく見た間によって歩いているかどうかを次のことによって確かめることができますある状況である人と間に何か問題があったやかみは数に何が問題かをあなたに示
してください何が間違っているのか何が問題なのかがわかってもそのことが起こる前と同じ愛と憐みをその人に対して持つことができているそれはあなたが見た間によって歩いて
いるからけれどもあなたから厳しい批判的な言葉や真立な言葉が出るならあなたは見た間によって歩いてはいませんしかもあなたは間違った判断をすることになるでしょう15
16 説でパールは言います見た間を受けている人はすべてのことを分きまえますが自分は誰によっても分きまえられません一体誰が主の見心を知り主を導くことができたかとこ
ろが私たちにはキリストの心があるのです私もこれまでどうしたらよいのか全くわからなかったことがたびたびありましたそれなとき主をどうしたらよいのか分かりませんと言い
ましたそして神ははっきりとどうすればよいのか教えてくださいました私たちの成すべきことは私たちに与えられているものを主張することですあなた方の頃に反抗心がなく罪が
なく見た間によって歩んでいるなら次のように祈るのです主よ私にはキリストの心がありますあなたがそう約束してくださっています今キリストの頃をください神様からの知恵が
必要です礼による動作が必要ですあなたがご覧になっているように見る必要があるのです祈ったら神があなたに示してくださることを疑わないでくださいそして主に祈るときあな
たに分かる範囲でできる限りあなたの心に罪がないようにしてください最後にお話ししておきたいことは神は身言葉に反することを決して言われませんそれらのことは絶対にあり
ません皆さんは次のことに思いを向けたことがありますか私たちはこの世界で歩んでいますこの地上でこの血肉の体の中にキリストの心があるのです私は理解はできませんがこの
ことは真理だと知っています考えてもいてくださいキリストの心を持つことよりも価値あるものが他に何かあるでしょうかキリストの心を持つとはどういう意味でしょうかそれは
あなたの人生で神の目的に従ってクリスタン生活を送るために知る必要のあることを神の定められた時に知ることができるということですあなたはキリストの心を持ち見玉を持っ
ているのです見玉によって歩みキリストの心を持ち神の知恵を持つことはとても価値あることだと思いませんかあなたはどんな人生を生きているかこのことにどれほどの価値をあ
なたが見出しているかを転検してみてくださいもし罪の中を歩んでいるならキリストの頃価値あるものとは見ていないことですキリストの頃を持っていることを知ることほど価値
のあることは他には何一つありませんキリストの頃を持つ時どのような状況の中にあってもその状況についての心理をあなたが教えてくださいところで私たちはこの世の例を受け
たのではなく神の見玉を受けましたそれは恵みによって神から私たちにたまわったものを私たちが知るためですこのたまものについて話すには人の知恵に教えられた言葉を持ちず
見玉に教えられた言葉を持ちいますその見玉の言葉を持って見玉のことを解くのです神への情熱と人々への愛で世界に触れるチャールズスタンデーの教えをインタッチミニストリ
ーズがお送りしています日本の今起こっている最新のニュースをはじめスポーツ、栄能情報、すぐに役立つ健康情報など、厳選した情報を満載でお送りいたしていますこの番組は
サトウ製薬、テイクアウトケータリングのタツオズ、メディケアスペシャリスト小林木和子、以上各社の提供で月曜日の日続かれていますあわき光絶にわかんで塩も影の神情けあ
ふるる涙のつぼみからひとつひとつ香り始めるそれはそれは空を越えてやがてやがて迎えにくる春よ 遠き春よ瞼閉じればそこに愛をくれし君の懐かしい聞こえがする君に預けし
わた心今でも返事を待っていますどれほど月日が流れてもずっとずっと待っていますそれはそれは明日を越えていつかいつかきっと届く春よ
まだ見る春迷い立ち止まるとき夢をくれし君の眼差しが果たったを抱く夢よ
朝日夢よ私はここにいます君を想いながらひとり歩いていますまがずる雨のごとくまがずる花のごとく春よ 遠き春よ瞼閉じればそこに愛をくれし君の懐かしい聞こえがする春よ
まだ見る春迷い立ち止まるとき夢をくれし君の眼差しが果たったを抱く春よ 遠き春よ瞼閉じればそこに愛をくれし君の懐かしい聞こえがする春よ まだ見る迷い立ち止まるとき
夢をくれし君の懐かしい長さを心に光をあなたの心に光をお届けする心に光をの時間です心の窓を開いて清らかな光をお受け取りください皆様こんにちは日本協会の日ごぶの僕氏
ジョンバロウと申します今週心に光を担当させていただいていますどうぞよろしくお願いいたします今日も心に光を聴いてくださり心から感謝いたします今日は子育てのテーマで
す皆様と子育てのことに関して考えたいと思います今日も見つけた子育ての喜びの本を表彰しながら考えたいと思います子育ては本当に大変ですよね私は今ですね3人の子どもが
いますけれども妻と一緒に子育てをしつつ本当にいろんなことを学んでいます今日のテーマは子どもの自立を助けるために子どもの自立ですね皆様と考えたいと思います今日の主
題は知恵によって訓練され選択する自由を与えられ良い結果や悪い結果を体験している子どもは懸命な決断をするすべを知った責任がある大人に成長するようになるそうですね子
どもを自立させることこれは本当に重要だと思います自立をするということを考えると子どもが18歳20歳になったら自立をするというイメージかもしれませんけれども子ども
の時からですね小さい時から本当に自立とは何なのか自立を育むことができるように今日皆様と考えたいと思います親として子育ての目的は子どもがいつの日か自立した責任ある
大人になるように育てることです親が顔ごになり子どものためだと思って子どもがやるべきことを何でもやってしまうとその目的は達成されないでしょうそれ以外子どもが自分で
決断したり選択できるように徐々に訓練することがとても大切になりますそのために子どもが大人になる年齢に達するまで寝ごとに自由と責任を少しずつ与えていきますそうすれ
ば子どもは貸しこい選択をするための技能や知恵を身につけるようになるでしょう生徒の中にはこのように指示されています新元22章の6説若者をその行く道にふさわしく教育
せよそうすれば年をいてもそれから離れないとあります若者をその行く道にふさわしく教育せよそうすれば年をいてもそれから離れないとてもいい箇所ですねはいその行く道にふ
さわしく子どもを教育するとはそのふさわしい道へ子どもを導くという考えだけではなく子どもの個性や才能に従って成長するように助けを手助けをするという意味も含まれてい
ます子どもは一人一人違った能力とユニークな才能を与えられた存在として作られたのですだからそれを頭に入れて教育すべきですほとんどの子どもは大きくなったらなりになり
たいかという夢を持ちますねその夢はよく変わるでしょう私の子どもたちのことを思います本当に大きくなったらなりになりたいということを聞くと毎回違う答えが出てきますね
警察官とか消防者とか医者とかいろんな答えが出てきますね子どもをいろんな夢を持ちますけれどもその子どもを出望させるよりもその子どもの能力に似合った夢を持ち続けるよ
うに励ますべきです子どもはしばしば自分に与えられた才能や自分自身の夢を追い求めるよりも親の知いたルールの上に歩むようにというプレッシャーを感じますですから親の夢
を知るよりも自分で長所を見つけ出しそれを伸ばせるように助けるべきですなぜ親にとって子どもを自立させることが難しいのでしょうかいろんな理由があると思いますけれども
例えばですね子どもにさせるよりも親がやってしまったほうが簡単で早い時があるから親としてアドバイスをするのがどうぞだと思っているから子どもを苦しみや痛み、羊から守
ってやりたいと思うので子どもに失敗をさせたくないという気持ちですね自分が必要とされていると感じたいからそして最後に自己中心だから私たちは長年子どものことを心配し
たり計画したりラグサメ愛することによってたくさんの犠牲を払っていますですからそれを止めて子どもを手放すことが難しいのは当然なことですそうですね本当に自分のことを
思いますけれども子どもを自立させる本当に難しい理由がたくさんありますね私はですねこの理由の中で一番同感するのはどうですかね自分が自分がやった方が簡単で早いってい
うのがありますねやっぱり子どもがやるとすごい時間がかかってしまう子どもがやるとやっぱり間違ったりしてしまうということもありますねあと今思いつきましたけれども私の
息子が幼稚園ですね自分に初めて入ったとき自分で教室に行くようになりますね車で送って自分で車から出て自分で教室に行くようになりましたけれども本当にすごく警戒しまし
たね自分の息子が本当に一人で教室に行けるのかっていうのをすごく自分の心の中で疑って本当にすごい心配をしましたね子どもを手放すことが本当に難しいですね他にもありま
すけれどもどのようにしたら子どもを自立させることができるでしょうかまたどのようにしたら成長の拡発立つ段階において次への備えができていると確信を持つことができるで
しょうか一つはですね自分のことは自分でさせ選択する自由を与えるどんなことでもできる限りたくさん子どもに選択させる機会を与えます子どもがすると時間がかかったり完璧
にできないかもしれませんしかし大切なことは子どもにやらせてみることですやり続けることで自信につながり練習するうちに以前よりも上手にできるようになるでしょうもし子
どものときに練習をさせなかったら大人になったときにどのようにものことを決めて良いか分からなくなるでしょう子どもも何かを自分で選択する機会があればより嬉しいと思い
ますたとえササイなことでも子どもに選択させることは決断する練習のチャンスになります例えば野菜を食べないといけないけど人参と放練祖どっちを食べるピアノの練習はいつ
するのがいい夕食の前それとも後かしらなどありますね選択をさせる自由を与えるということですね子どもに与える選択肢は年齢にあって内容にすべきです子どもはまだそんなに
多くのものことを決める判断材料や能力を持っていませんとして洋服を選ぶときの組み合わせの方法や選び方はまだよく分からないでしょうしかしこのTシャツとこっちのTシャ
ツどっちがいいというふうに二つのうち一つだったら選ぶことができると思いますですから子どもの連例と能力にあった選択の機会をできる限り与えましょうそうですね例えば子
どもがティーネージャーであって高校を卒業しますよねそして大学に入学することが多いですよねそのときにどの大学に入学するかという決断をその子どもに与える大切さがあり
ますね小さな子どもだったら本当に決断力というのは本当に少ししかないので二つから一つを選ぶとかですね洋服でもいいし遊びでもいいし本当に簡単な選択をすることができる
ように子どもは徐々に選択する力自立のできるようになりますねとても良いアドバイスですね子どもが成長するに従って授業のこと部活のこと友達のこと何か正しく何が間違って
いるのか将来どの大学に行くのかどの仕事を選ぶのかそして誰と結婚するのかなど決断していかなければなりません簡単な問題について自分で決断する機会が与えられていれば複
雑な問題に関しても決断できるようになります従来の子どもにとって自分で選択する自由が与えられることは特に重要でありそうでなければ子どもの気持ちが親から離れていった
り感情的にも肉体的にも反抗するようになるでしょうまた自分でものことを決める上で親に信頼されていると思うことで子どもは自信を持つでしょうすべてのことについて子ども
に選択させる必要はありませんけれどもある程度のことについては子どもが選択できるとしたらこれらの人生の中で重要な決断をし問題を解決していくために必要な責任感を癒し
なことになるでしょうそのとおりですね本当に小さい時からそういう決断を剥くまないと本当に大きな大きくなって大きな決断をする時そういう子どもの時から学んだことを適用
することができるんでもっともっと大きくなると問題っていうものは複雑になるので本当に問題っていうのは難しくなるのでそういう問題をよく解決できるために小さい時から自
立させて本当に決断力というものを価値観として子どもに学ばさせることですねとても重要ですね最初に及びした選手箇所をもう一度及びしたいと思います新元の22章6説若者
を行く道に不鮮しく教育せよそうすれば年をいても離れない本当にその通りですね子どもが自立することができるように懸命な決断をすることができるようにですね責任ある大人
に成長することができるように本当に小さい時からこういうことを教えてこういう技能教えを身につけて子どもは決断することができるようになります自立することができるよう
になります本当に子どもを自立させることは本当に難しいことですねいろんな親として自立させたくないという気持ちがいっぱいですけれども子どもは大人になるのでそういう選
択する自由を与えて自分のことは自分でさせることができるように子どものことを成長させますありがとうございました仮美ユニオン協会の25分の僕氏ジョンバロウでした今日
も心に光を聴いてくださりありがとうございましたよー1日をおすくしくださいありがとうございましたこれがこの動画はいろんな人が私が彼が私が彼が彼が彼が彼が彼が彼が彼
が彼が