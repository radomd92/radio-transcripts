TRANSCRIPTION: KZOO_AUDIO/2023-03-02-13H00M.MP3
--------------------------------------------------------------------------------
'llLet us伝えたくて あなたを見つめにけどつながれた耳では 誰よりも優しくほら この声を受け止めてる眩しい朝に
気がわらいしてさあなたが窓を開ける舞い込んだ未来が 始まりを教えてまたいつもの街へ出かけるよ目とぼこなまま 積み上げてきた二人の淡い日々はこぼれた光を
大事に集めて今 輝いているんだあなたの夢が いつからか二人の夢に 変わっていた今日だって いつか大切な思い出青空も 泣き空も
晴れ渡るようにありがとうって伝えたくてあなたを見つめるけどつながれた耳では 真っ直ぐな想いを不器用に伝えてるいつまでも ただいつまでもあなたと
笑ってたいから信じたこの道を 確かめていくように今 ゆっくりと歩いていこう喧嘩した日も 泣き合って日もそれぞれ色逆せて真っ白な心に 描かれた未来をまだ
書き出していくんだ誰かのために 生きること誰かの愛を 受け入れることそうやって 今もちょっと絶かされて喜びも 悲しみも 渡し合えるように思い合うことに
幸せをあなたと 見つけていけたありふれたことさえ 輝きを抱くよほら
その声に寄り添ってくんだね愛してるって伝えたくてあなたに伝えたくてかけがえの泣いてをあなたとの取れからを私は信じたんだなありがとうって言葉を今
あなたに伝えるから繋がれた身に手は 誰よりも優しくほら この声を受け止めてる私は今 どこにあるのと踏みしめた足跡を何度も見つめ返す彼方だけ
開き巻く窓目にかじかんだ指すぎで 夢を描いた翼はあるのに 飛べずにいるんだ一人になるのが怖くて 辛くて優しい日だまりに 片寄せる日々を越えて
僕らこの苦な夢へと歩くさよならは悲しい言葉じゃないそれぞれの夢と僕らを繋ぐエールともに過ごした日々を胸に抱いて飛び立つよ一人で次の空へ僕らはなぜ答えを assa
yえて当てのない暗い日々自分を探すのだろう誰かをただ思う涙も真っ直ぐな笑顔もここにあるのに本当の自分を誰かの言葉で尽くしたことに逃れて迷ってありのままの弱さと抜
き合う強さを掴み僕ら初めて明日へと駆けるさよならを誰かに告げるたびに僕らまた変われる
強くなれるかなあなたとへ違う空へ飛び立とうとも答えはしない想いよ今も胸に永遠などないと 君からの日も強く胸に刻まれてゆくだからこそあなたは
いや無駄なでもない叫び声をあげて 私を生きてゆくよと約束したんだひとりひとつ道を選んださよならは悲しい言葉じゃないそれぞれの夢と僕らをつなぐえるいつかまた巡り合
うその時まで忘れはしない誇りよともよ空へ 僕らと彼が散る言葉がある心から心へ声をつなぐえる共に過ごした日々を胸に抱いて飛び立くよ ひとりで次の空へ永遠などないと
君からの日も永遠などに 君からの日も永遠などに 君からの日も永遠などに 君からの日も目やんではない 切なく痛む時もあるNo one's still in
love 迷いはしないもしもこの先に希望という光があるのならば出逢えたことがきっと生きてゆく強さになる愛した日々 愛された時あの温もりは消えないI can't
say anything to youどんな言葉も 全ての伝えきれないから会いたくても 会えない夜は眩れないへやりたくねI still believe in
loveもう一度 諦めないで信じることが make them a futureShow me the way to the days of
yesterday投げ出してしまえば 絶やすいことだけどこの我がまを 受け止めてくれた優しさに 答えたいから人並みの中で
居られてもな変わらないものがある立ちのまたびによみがえるあの夢が 知るしだから飽きてるほど 愛に触れてゆく頼りがいい この自由に流されないで
心を繋いでゆくから会いたくても 会えない夜はあなたの声が聞きたいのI still believe in love離れても 包まれてると信じることが make
them a future諦めないで 信じることがMake them a futureKZOO KZOO 1210AM Honolulu, HawaiiKZOO
KZOO 1210AM Honolulu, HawaiiKZOO KZOO 1210AM Honolulu, HawaiiKZOO KZOO 1210AM
Honolulu, Hawaii孤独なドアの向こう その一人きり踏み出したらさ強がりなまなざしは 誓い 腕はしない逃げないことが いつものも自由より大事だと I
knowわかっていたから今 for you 戦うよ君が祈る時は その手包み込むよ同じ熱に生なされShake it down 信じた道を走る I'm here
for my own life 傷つくなら奥までなじく生きぬ その果てなら 赤く燃えていいFight down 希望はいつの時はSelf-shine この胸の中
探し出してCan you feel it? 失くすことを恐れるより愛を一つ抱いて 闇の向こう 騒を抜こういくつかの出会いさえSo far
遠い記憶に揺れるだけ曖昧な別れより too hard 傷をつけて寂しさに沈まないのか 捨てられない誓いだらぶもて遊ぶ運命には for you 逆らうよ君のもとへ
いつか 変える時が来るよ信じている明日をFight down 貫き通す願い I'm here for 忘れない真実ならさ 肌で
君を誇る自分だけはいつもここにいるSelf-shine この答えはいつの時をTurrent 心が叫ぶ 背を伸ばせばいいCan you touch it?
夜に濡れる儚さより愛を一つ抱いて 遠い光 So I wanna goShake and 信じた道を 走る I'm here for 迷わない傷つくなら 奥まで
らしく生きるその果てなら 赤く燃えていいFight down 希望はいつの時もSelf-shine この胸の中 探し出してCan you feel it?
失くすことを 染めるより愛を一つ抱いて 闇の向こう So I wanna goShake and 信じた道を 走る I'm here for 忘れないI'm
here for 迷わないFight down 希望はいつの時もSelf-shine この胸の中 探し出してI'm here for 迷わないThere you
are in the darkest nightNowhere to run, just sitting there lonelyCan you feel
like you're so far awayCan't find your way homeCrying out, but no one's there
for youAnd you feel like you wanna escapeDon't close your eyes, no need to hold
your breathShouldn't be afraid to make another stepDon't give up, don't give
inThere's always an answer to everything in lifeOnce you fall, you will riseJust
listen to your heart and say it outI know I can make it, I'll step up againEven
through the rain, through the nightI'll be my strongerCause I'm gonna make itAnd
nothing can stop me from holding tightTo my fate, I'm on break of dawnLong, long
way, that's how far you have comeNever look back, cause you sacrificedDon't
hesitate, just keep on walkingYour way, there is nothing in this worldThat you
can't reach, deep insideYour heart, you wanna let the world knowWhat you can't
really doYou can try, one more timeYou just need to stand tall and sayI know I
can make itI'll keep going forwardTo change my life, get the lifeI've always
dreamed ofCause I'm gonna make itAnd no one can tell me it's impossibleHere I
am, I'm on break of dawnI'm on break of those sticks and stonesBreak my bones,
but I will stand strongSticks and stones, break my bonesBut I will stand strongI
just wanna tryTill I fall down againI know I can make itI'll stand up again,
even through the rainThrough the night, I'll be my strongerCause I'm gonna make
itAnd nothing can stop me from holding tightTo my faith, I'm on break of dawnI'm
on break of dawnI'm on break of dawnFull in 止まない 雨は今日も誰かの解けない笑顔を曇らせてる傷つく度
壊れてゆく絆をそばで見てるだけじゃもどかしくて争いの涙で 溢れるこの世界声のない悲しみに 気付くことできたなら変わる未来があるよきっとYou're my
hero 手を取り心を一つにしよう信じぬ君こそが 希望に生じた光過ぎる時間の中で何を今 残せるだろうかけがえのない人 守るパーティー嘘が信じつから
瞳そらさないでおたやいの痛みさえ かばうことできたなら恐れるものなんてないHow may your hero 届くな その胸 開いて包む伝わる温もりが
僕らをつなぐ私You're my hero 愛のもん 見えたそうYou're my hero 愛のもん 見えたそうYou're my hero 愛のもん
見えたそうYou're my hero 愛のもん 見えたそう泣き虫だった小さい頃から少しは変われたからクレヨンで描いた未来の自分懐かしい匂いがした一人ぼっちじゃ
今だって上手くいかないや私の中いつも声にならないよ本当は伝えたいのに別に寂しくなんてない
そう言ってサンダンスよがあった大人ぶっての誤解しまってあった想いを言葉にできたら きっと きっと踏み出せ真面目で偉いねって
褒められたのになんか嬉しくなかった優しい嘘に気づいては心傷ついて果てしないさて 探りで見つけたいの壊れてしまう前に
作り笑顔で隠した不安が嬉しいはずでる自由は曖昧でも足りなかった途切れそうな想いでも そっと
そっとつないでく私の中いつも声にならないよ本当は伝えたいのに誰もわかってくれてない
そう言ってサンダンスがあった明日になれば動き出せますように途切れ思い届けばもっと今を好きになる長い夜 静かに明けてゆく空またひとつ近づいてく昨日のリピートのよう
なスクランブル交差点で青すぎる空を見てた雨なら隠せることもあるのにな物が溢れる街で何か満たされないままレンタルシャン あのエリカも未来ままで 変え尽きがしたねえ
今あなたは私を覚えているねえ
今私はあなたを思い出せた声はずっとここにあってなのにずっと響かなって大事だって思うのがないわけじゃないから帰りたいと思ってたって帰りたいと言えなくて強がらって
寂しかったそれでも明日を探してる笑ってまたね 手つけて初めての中央線も気づけば慣れた足で人ゴミを避けて歩いているねえ 今あなたは私がどうも見えるねえ 今私はあな
たに会えないかもしれない見もしないテレビの灯りを眺めて手にした経理台見慣れた名前に繋いだ向こうで人の気も知らないで笑う声に雨はずわらった声はずっとここにあってな
のにずっと響かなくてわかるよって 嘘でしょって歯ぐらかしてだけどなんか嬉しくってだからきっと
朝を待ってリピートって思う今日に期待をする私はずっとここにあってそれがきっと今になって迷いだって
孤独だって当たり前にあるけど帰りたいと言いたかった帰りたいと言わなかった強ががって 寂しかったってそれでも私は歩いてく見せないあの映画も明日帰りゆこうBaby
touch my heart函言は好きじゃない眩しくて目が痛い見たくないもの死にたくないものこの部屋にだってたくさんあるよ一切合切
忘れてしまいたいんだ山積の仕事も流行りの漫画夜中のバラエティ番組は薄らがおにしてくれるけど月の日になれば忘れてくまたリトウどうしようもない
今も逃がしたい逃げ切りたい いつだってこんな夜はわけもなく会ってくる生まれない隙間に月が一つぶる涙を落とすBaby touch my
heart気に入っていた馬鹿ももう今年は来ない気がしてる何度も繰り返し切ったアルバムは最近じゃメッキーに人も街も変わってくみんな遠くへ向かい走ってくなのに
感じんなところだけ感じんなところだけDay by day積もってくパセティクトに関してGaze by gazeちょうどいいね ライフスタイルを教えて眠りに落ちて
く5秒前明日何すればいいのかさえわからないまま夢の世界へいつだってこんな夜はわけもなく会ってくる大丈夫まだ誰かを好きになれる気がするいつだってこんな夜はわけもな
く会ってくる生まれない隙間に月が一つぶる涙を負ってSame and babyOh babyBabyBest to be a babyBest to be a
babyBaby babyBabyOh babyBabyBest to be a babyBest to be a baby窓を開けたら冷たい風が
夏限を揺らした窓を開けてやっと時が流れて 心を揺らした窓を開けてやっと時が流れて 心を揺らした窓を開けてやっと時が流れて 心を揺らした窓を開けてやっと時が流れて
心を揺らした窓を開けてやっと時が流れて 人情が無く叫びたい心臓のベクベクスピードアップ 明日になりきっとFade away一生でハズってきるチャンス
そうそうI'll keep it勝利期限は長くない 今を食べつくFadeDarling Fade 本能のまま 千歳逆で走ってゆけCherry La La
ジャリラルリ 実際の日々を振り切ってHigh Don't Bice 納粛 復活を 揺れるウリルスムージャンプさせてWon't I dance? 泣いまーす
アリハーバー 希望はきるな悲憎な思いを Boys and Girls 喜びにゃひとれない心境はまだまだ Disatisfaction 絶対に無いで
待ったFade away幾千の頃を絶えず 拝い ケガヨテラ Dance or Fade勝利期限のギリギリまで 味わい尽くせばいいDarling Fade
明日になれ 生きあわらのEverydayCherry La La ジャリラルリ 実際の日々の始まりさ散々ちゃんで 悩んでる
人生大体そんなものさ一瞬光る喜びのために生きてんだDarling Fade 本物のまま 戦争力で走ってゆけCherry La La ジャリラルリ
逆軒船も追い越せDarling Fade 明日になれ 生きあわらのEverydayCherry La La ジャリラルリ 実際の日々の始まりさI'm done
by some sugar 僕はBed release もうJumpさせてWent night
afternoon,<|ps|><|transcribe|>叶えましたおowDarling Fade 明日に一 discDarling Fade
会場や行動でも食べる窓際で見とれてた 平に迷う雪ガラスに映った 私へと願って気がついた
不思議でしょ?人はみんな自分の笑顔すら見えなくてそばにいる誰かに見守られてるそうやって交わって生きてくのかな運命の人よどうか真っ直ぐに 私を見つめてね泣き顔
想い顔も 全て預けたいどんな瞬間も 私のままですっとすっと 側にいたいLing Dong, Ling DongLing Dong, Ling DongLing
Dong, Ling Dong不安とか 孤独とかいつも 抜く痛くて何度も 確かめる悪くせ好きだって 真実を傷ついて すれ違って愛の形に導かれる与えられるものじゃ
なく感じるものいつだって寄り添って信じればいいありふれた言葉よりも真っ直ぐに心を見つめてる照らないで無情気な笑顔を見せたよ好きも愛してるもん
何もいらないすっとすっと 離さないで運命の人よどうか真っ直ぐに 私を見つめたね泣き顔 想い顔も 全て預けたい幸せの金を 空に放つよスタッフと 剥ぐらぬようにリン
トリントリントリントどれくらい感謝したって足りないから花灯を全身で見つめ返す太陽の光を浴びて輝く夜空の月がそりしてるみたいに雲の空がくれるように彷徨う私
光をつけてくれたね一人きり閉ざした心 こじ開け私の全てを受け止めてくれたんだ誰かを頼る心 強く信じる心きっとあなたに出会ったからすらを担えたんだ愛を知って輝き出
すんだ人もみんな世界を照らしてく夜空の月のようにどんなにも輝いてるよ見えるかな?逃げないで強くなってくあなたに笑って欲しかったこんなにね 見て光よ不思議だね
笑顔の奥で泣いてたこれのあたしにさよならできたんだありがとう だから苦しい時にはあたしの光で守ってあげたい誰かのためになりたい
誰かのために生きたいきっとあなたに出会ったから生まれ変われたんだ愛を知って輝き出すんだ嬉しいよ
不器用なあたしだってまだ小さい光だってどうかずっと見守っていたねいつまでも無限に繰り返し心に抱く想いよ飛んでゆけありがとうどうしよう
どうしたらいいこんなにも誰かを愛せるって涙が溢れ出す愛を知って輝いてるよ迷わないで世界を照らしてく夜空の月のようにこんなにも輝いてるよほら
この空で見ぼしかりあだから最初からあなたとそりとげたたったんじゃない始まりは約数年前今から話すストーリーYou're readyYou're the one
and only綺麗感で持ちそろってて全て手にしてたくわらってそんな女が嫌いあいつの殻に罠を仕掛けてゲーム開始私のものになったらどうするどう出てくるのこの人生全
てゲームいいところまで行くだけで語らきゃ意味ないじゃね泳がせてそうなきっかけるそのタイミング待つの恐れるがいいさあどうぞタイム話しと