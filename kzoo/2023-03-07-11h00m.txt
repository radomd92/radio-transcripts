TRANSCRIPTION: KZOO_AUDIO/2023-03-07-11H00M.MP3
--------------------------------------------------------------------------------
在茂サザ男団どこにあたりを切り替える…歌展開�ONm 本をルールはわいいまあ I think about that particular name
エロヒムアイティーを like God shouts out of that nametrust me trust me I'm powerful enough
trust me trust me trust me trust me in thebeginning trust me he created the
heavens in the earth trust me he's gonna sendthe saviour trust me God Elohim he
says trust me I'm supernaturally able and I amtreatable to my world
私の言葉にとってはwelcome to in touch the teaching ministry of Dr. Charles Stanley and
his continuingseries the character of God a lot of people formulate their own
impression of who orwhat God is but a flawed understanding can be dangerous and
even destructivetoday's message reminds us that the only true and accurate
understanding of Godcomes from the Bible stay with us to discover more about his
incomparable namewhen you read the Psalms you find over and over and over again
references to the praiseof God's name Psalm 135 says praise ye the Lord praise
ye the name of the Lord praise him oh ye servants of the Lord ye that stand in
the house of the Lord in the courts ofthe house of our God praise the Lord for
the Lord is good sing praises unto hisname for it is pleasant Psalm 145 the
psalmist begins I will extol thee my GodOh King and I will bless thy name
forever and ever every day will I bless thee andI will praise thy name forever
and ever and the last verse of that chapter says mymouth shall speak the praise
of the Lord and at all flesh bless his holy nameforever and ever no study on God
would be complete without us looking at his nameand as you go through the Bible
there are many many names that refer to God andsome of them you could recall but
as I began to study the names of God to seewhat was really involved not only did
I get my heart blessed in the study of itbut I got my heartchallenged in what I
believe these names require in theirvery essence now today people named their
children by certain names because ofcertain things they for example if a person
has a name that they like best ofall sometimes they'll name their son and
daughter by that name or sometimes it'sto commemorate their parent their mother
their father their uncle or someone inthe home or sometimes it may be some of
the reasons which they give a child aname in the Old Testament they never gave
names in that manner but there wasalways a specific reason for naming a child
for example Hannah when she namedSamuel said I call him Samuel because I asked
the Lord for him so there was adeep desire in her heart and Samuel was the
result of that desire when Pharaoh's daughter pulled him out of the water at
least the his sister did and present him to Pharaoh's daughter she said I'll
call him Moses because we drew him outof the water when you go to the judges and
you find Samson Samson was called bythat name I believe because God knew the
work that would be before him look atthe life of the Lord Jesus Christ and look
at his name in fact you can goall through the Old Testament and find people were
given specific names fora very definite reason but there was something else
interesting to me thatsometimes God would call the name for example it is God
who gave John the Baptist his name they want to name him Zachariah or probably
had lots of names they want to call him but his mother said no his name is to be
John it is God the father who named the Lord Jesus Christ because the angels
said that will shall call his name Emmanuel God with us thou shall call his name
Jesus for he shall be the savior of his people God changed Abrams name to
Abraham hechanged Saul's name to Paul he changed Sammon's name to Peter when I
began tolook and see the very importance that God placed upon the name and how
henamed men and how he changed the name of men whom he chosen for great task
it's easy for me to see that when you come to the name of God that God didn't
get hisname haphazardly that it's not just the name for deity but God's name
everysingle name for God in the Bible is full of rich and beautiful meaning for
us Iwant us to take first of all the name Elohim the name Elohim if you're
writing notes that's e-l-o-h-i-m it is the word found in Genesis chapter 1the
very first verse it is found over 2700 times in the Bible 32 times in the first
chapter of the Bible Elohim in the beginning God created the heavens and the
earth and you see it's not just some name that man has given to it but you see
because God is God God saw to it in the inspiration of the scriptures thatthe
names by which men would know him would be names that are full and rich with
meaning now if you break that name down here's what you'll discover Elohimthe
first part of that means strength power might Elohim the last part of thatname
comes from the root that means to swear or covenant keeping or a promise so when
you and I read the name Elohim what do we find when it says inthe beginning God
what does that word mean what does the word God mean itmeans simply this Elohim
means he who is infinite in power and absolute in faithfulness Elohim in the
beginning God he who is infinite in strength infinite in power infinite in might
and he who is absolutely and unswervingly faithfulabsolutely faithful now when I
began to look at that and begin to think interms Lord what does that mean to us
personally well I thought about norfor example you recall it can Genesis chapter
8 if you will you recall thatGod told him to make an arc and the Lord said to
him I want you to go on the arc and carry a tool this and tool that until the
other and he did exactly thatand then it says in chapter 8 verse 1and God Elohim
remembered nor not onlydid he have the infinite power and strength to deliver
him in a flood thatcovered the world but he was faithful to remember nor whom he
closed in the arc when I began to understand what that word meant I began to
think how in theworld can any Christian ever call upon God and ever doubt that
he's able tofulfill his will you see every single promise God has ever given us
theverainame of God itself assures us every single promise in the scripture
thatis ours the name Elohim is God blaring out to us he who is infinite in power
isalso absolutely faithful whatever he's promised he has the power the power
toperform and whatever he has the power to perform he is faithful to keep it
yousee when we talk about God and we use his name and we carelessly throw
itaround we need to stop and ask ourselves who is this God about whom we
speakand how often we sing I want you to listen to this with that definition in
mind Martin Luther wrote he said a mighty fortress is our God a ball worknever
failing listening a mighty fortress infinite in power unlimited in strength a
ball work never failing absolutely faithful our helper he amidthe flood and then
you begin to read did we in our own strength confide our striving would be
losing we're not the right man on our side the man of God's own choosing this
ask who that may be Christ Jesus it is he Lord's about his name from age to age
the same and he must win the battle why because he'sinfinite unlimited in his
power and I believe if God's people would begin tounderstand who this God is
we're praying to when you get on your knees andyou say dear God what are you
saying are you talking to somebody who's weak areyou talking to somebody that
you do not know personally to whom are youaddressing that prayer you're saying
to you to the one who is infinite in powerto you faithful absolutely faithful to
every promise listen you ought to beable to get up off your knees with total
confidence that God is going to answeryou prayer you and I are worshiping and
praying and singing and praising apersonal God whose very name is a testimony
and a proclamation to usinfinite in power absolutely in his faithfulness and we
ought to walk inthat kind of content that thing hit me all of a sudden God just
convicted meof something something that he'd ask of me to do that I had not done
and I saidto him that I would do it but I didn't know how to do it and so I
guess I justflowed it along asking Lord show me how and maybe there was some
doubt in mymind of how he would work it all out when I read that passage of
scripture I foundmyself flat on my face before God weeping and I'll tell you why
I wasweeping to think that I could get on my knees before God and call his name
in prayer and ever doubt for the very nature of God that he would not fulfill to
thevery infdegree exactly what he promised and I found myself worshiping God
andpraising God and hallelujahing to God and confessing that to him how could
Iever turn to Genesis chapter 1 again and my friend you think of the
promisesthat God's given you in the book how can you ever turn to Genesis
chapter 1and say to an unbeliever listen in the beginning God you can't say that
unless you believe he's infinite in his power absolute in his faithfulness and
that's the reason you and I can say to a lost man if you'll turn your life
overto Jesus Christ he who is infinite in his power he who's faithful to his
wordhe'll save you on the spot I've told you my friend we talk about him as if
weknow him and we don't even know what his name means I want you to get a hold
ofthat Elohim he is supernaturally powerful and absolutely faithful and I'lltell
you when I think about that I can shout for the simple reason that meanshe whom
we worship is adequate sufficient and faithful to meet every single need of our
life and you need to get that into your kid's heart in their mindswho is this
God when I think about that particular name Elohim I feel likeGod shouts out of
that name trust me trust me I'm powerful enough trust me trust me trust me trust
me in the beginning trust me he created theheavens in the earth trust me he
created the son trust me he created man trust me he's gonna send us a savior
trust me God Elohim he says trust me I'msupernaturally able and I am faithful to
my word there's a second word for Godand that's the word Jehovah the word
Jehovah was the most sacred name in thewhole Hebrew religion it was the most
sacred name for the whole Hebrewsociety the word Jehovah in fact was so holy and
sacred that when they read the10 commandments and if you recall the 7th verse of
Exodus chapter 20 listenGod said thou shall not take the name of the Lord thy
God in vain thou shall nottake the name of Jehovah in vain in fact the Hebrews
were so conscious of thataround 300 BC they decided not to even speak the name
because of the penaltyof blaspheming the name of God now I want us to look at a
couple of passageshere let's go back to Exodus for just a moment and I want us
to see first ofall in chapter 3 and you recall that here God is speaking to
Moses and in the10th verse of chapter 3 listen to what he says so let's begin in
verse 10 comenow therefore and I will send thee unto Pharaoh that thou may
spring forth mypeople the children of Israel out of Egypt Moses said unto God
Elohim who am Ithat I should go into Pharaoh and that I should bring forth the
children of Israel out of Egypt and he said certainly I will be with thee and
thisshall be a token unto thee that I have sent thee when thou has brought forth
thepeople out of Egypt ye shall serve Elohim upon this mountain Moses said unto
Elohim behold when I come unto the children of Israel and shall say unto them
the Elohim the supernatural one absolutely faithfulinfinite power of your
fathers had sent me unto you and they shall say to mewhat is his name that was
very important he says now when they say what's hisname what am I going to say
and listen to what he says what shall I say to themand Elohim said unto Moses I
am that I am and he said thus shall thou say unto the children of Israel I am
have sent me unto you what is he saying that is adescription of Jehovah
everlasting eternal ever existing never a time when he didnot exist when they
ask you what is the name of your God which God sent you youtell them that the
great I am have sent you Jehovah God the eternally existingone has sent you well
when you look at that phrase I am listen verse 15 and God said moreover unto
Moses listen Elohim God said moreover unto Moses you say tothe children of
Israel the Lord God Jehovah of your fathers the God ofAbraham the God of Isaac
the God of Jacob had sent me unto you this is myname forever and this is my
memorial unto all generations verse 16 go and gather the elders of Israel
together and said to them the Lord God Jehovahhave sent me unto you now what
does that word mean it means self-existent itmeans everlasting that says
something about the majesty of God it sayssomething about the grandeur of God it
says something about the wholespectrum of life because you're not not serving a
God who has a beginninglisten when people say where did Jesus began in Bethlehem
no eternal he is and if you recall in John chapter 1 look in John chapter 1 for
just a moment inJohn chapter 1 the scripture says in the beginning was the word
now that is inthe beginning of creation was past tense past tense something is
going on in the past he says when you stand on the threshold of the beginning of
time he says you can look back before time and what will you find you'll find
that the word that is Jesus Christ was the word was with face-to-face
relationship with Elohim and the word was Elohim supernatural in his power
absolute in his faithfulness and if you take a Greek Bible and you will begin to
trace some words it'll be interesting in well let's go to book of John in John
chapter 8 there's several verses here I want you to notice something look if you
will in verse 28 then said Jesus of them when you have lifted the Son of man
when you've lifted up the Son of man then shall you know that I am when Jehovah
identified himself to Moses how did he identify himself he says I when they ask
you who sent you you tell them I am have sent you you can go all the way through
in fact there's several verses here in the book of John let's take verse 56 of
the same chapter look there for just a moment verse 58 of the 8th chapter Jesus
said to them their liver last saying to you before Abraham was what I am before
Abraham was I am when he said that those Jews like the rip their clothes off
because that was about as blasphemous as they could ever hear that here was an
itinerant Nazarene carpenter walking around saying before Abraham their sacred
father was Jesus said I am because he was and is Jehovah the everlasting ever
existing one his total existence is to be found in himself when you now read the
book of John what do you find you find the Lord Jesus Christ saying I am what I
am bread of life I am way the truth in the life I am door of life I am good
shepherd all the way through what do you find you find the ever-present God in
Christ saying I am he is the sum total of the revelation of who Jehovah is and
that's why when men say I believe in God but don't believe in Jesus you cannot
when men say I believe in God but I do not believe in Jesus ask them will you
please identify and define to me the God in whom you believe all they say I
believe in the God in Genesis chapter 1who created the heavens and the earth or
you do then let me tell you who he is he is the one who has supernatural
strength absolutely faithful or you believe in in the Lord in the Old Testament
God always that right his name is Jehovah and he is the I am and one of the
person said I am and his name is Jesus and the man who tries to avoid John
chapter 14 and verse 6when Jesus Christ said I am the way the truth in the life
no man come into thefather but by me you know why I can't come to the father
because he and thefather are what they are one Jehovah everlasting God and
that's why the Bible says that Jesus Christ is the same yesterday today and
forever whybecause he's never been in need he never came into being he's always
been he always will be Jehovah the great I am his God and it is
absolutelyimpossible for a man to tell you and to know what he's talking about
that he believes in God but he does not believe in Jesus Christ ask him to
define for you who is the God in whom he believesdoctor Stanley's message help
to see the nature of Jesus Christ that he'sGod and he wants you to be in
relationship with him believe that hissacrifice for you on the cross canceled
the penalty of your sin entrust yourlife to him and be welcomed into the family
of God if you'd like to listenagain click today on radio at in touch.org and
that's where you can find outmore about who Jesus is and what's available to
those who trust in himto order a copy of today's complete message his
incomparable name open thestore page it's also included in our teaching set the
character of God ourweb address again is in touch.org or call one eight hundred
in touch if youprefer you can write to us at in touch post office box seventy
nine hundredAtlanta Georgia three zero three five seven a favorite Old Testament
storyabout an underdog illustrates the reverence we should have for God
today'smoment with Charles Stanley is just ahead at times life can be stormy and
each one of us will face many trials questions are asked and doubt may setin the
answers are found through faith in Jesus he is within every believeralways ready
to offer us comfort hope and peace in times of great trial so youcan trust him
with any circumstance to order doctor Stanley's book how to letGod solve your
problems call one eight hundred in touchor go to in touch.orgstore are you
living a life of preferences or one that's based on God's wordto enjoy a strong
life one that makes an eternal impact we have to break freefrom the factors that
make us weak anddoctor Charles Stanley's bookstanding strong believers will find
encouragement to construct a lifebased on the strength of enduring faith built
on uncompromised biblicalconvictions to order your copy of standing strong call
one eight hundred in touchor visit in touch.orgyou're listening to in touch the
story of David's faith as he faced Goliathis legendary let it enhance your
perception of God here's a moment withCharles Stanley.when David took his five
stones and a sling he went out to meet old Goliathhe knew who God was listen to
what he said he says thou comest to me Goliathwith a sword and with a spear with
a shield but I come to thee in the name of theLord of Host the God of the armies
of Israel whom thou hast defied this daywill the Lord deliver thee into my hands
and I will smite thee and take thinehead off from thee young David knew who
Jehovah was he knew that he was the provider of strength that when he wound that
sling it was God who would guidethat stone he knew God and when you read the
Psalms there is a holy awesome reverence about his concept of who God is a lot
of years have passed society has changed and the fact that men can joke about
God throw his name around and use it in profanity and blasphem it says our
society knows little or nothing aboutthe reality of who God islearn more about
living the way a person would who reveres God with our onlineresources at
ntouch.orgtomorrow on ntouch the Old Testament leader Joshua needed to learn a
lessonabout God's authority we'll learn about that as we hear a
practicalexplanation of the name of God Tuesday on ntouch the teaching ministry
of Dr.Charles Stanleythis program is a presentation of in touch ministries at
Lata Georgia andremains on this station through the grace of God and your
faithful prayers andgiftsyou're listening to Keizu Keizu001210am Honolulu
Hawaiiatt고damereまだ続くんだろうどの道今が大切なのさかぶしゃないなんて 見落としてきたもの例えば誰かの優しい声にも永遠を知ればどんな絡みも
痛みもいつか消えてそうやって今は私を汚してずっと昔みたい天空の中に いつかは辿り着ける真実の歌を道してにして永遠を知ればどんな絡みも 痛みもいつか消えて真実の歌
はこの胸に抱かれもっと今以上に私を汚してほんの少しだけ私を汚して真実の歌を道してにして抱きしめて欲しいよ本当はね今すぐに素直になれるなら全てが君にいつだって本当
はずっと I wanna say I love youでも戸惑うばかりだ
過ぎてゆくな時間だけ君の全てに涙くって信じてたいの僕も声聞こえてるなら強がって笑顔で隠し不安は君にはもう気づかれてるかな気になって言えなくて I want
you by my sideEvery moment in my
life傷つくことが怖いから次の約束もできないまままたねって手をふっと泣きたいくらい切なくなるのはなぜいつだって本当はずっと I wanna say I
love youでも戸惑うばかりだ
過ぎてゆくな時間だけ君の全てに涙くって信じてたいの僕も声聞こえてるなら夢だってる今この瞬間にも君は何を想っているの知りたくて聞けなくてもYou make me
lose controlEvery time you touch my
soul着信の度に作りの中探してるのは君だけであったいの一言で痛いくらいに私を満たしてくいつだって本当はずっと I wanna say I love
youでも戸惑うばかりだ 過ぎてゆくな時間だけ君の全てに涙くって信じてたいの僕も声聞こえてるなら離れてると寂しくてそばにいると苦しくてどこにいたって何をしてたっ
てただ募ってくよ君への想いだけ来らんようのない気持ち溢れ出してゆくI wanna give it all to
you君の声を聞くだけで見つめられるだけでいつまでも何があっても話したくないからLove you君へ走り出すいつだって本当はずっと I wanna say I
love youでも戸惑うばかりだ 過ぎてゆくな時間だけ君の全てに涙くって信じてたいのI wanna give it all to youI wanna
give it all to youずっと I wanna say I love youでも戸惑うばかりだ
過ぎてゆくな時間だけ君の全てに涙くって信じてたいの君の声聞こえてるならOh my lifeI always dream to be with youOh my
heartIt's like you're a sweet dream of some true抱きしめてほしいよ本当はね
今すぐに素直になれるならすべてを君に残らない 何を誇りはしない口にムーンを交わせばYou're the best途切れない
高ぶ向こうの感情が過ぎ出す世界は飛ぶ星がこぼれ落ちたみたいに光るYou're the best
無邪気なその瞳の奥はどこまでもミステリアスYeah乾いた言葉じゃ君に届かない見慣れたコリュームで愛をばらなく超える
またたくまもなくすべてを止める時を止めたLove Love Harriker心奪われる 背中で騙らせるくぎ付けさ Love Love
Harriker黙る語らない 変わりのその仕草偽りな調節さ地味地味地味 時間続ける辿れば 見上るのは簡単さ重に安心なんて無い君に 君に
全てに恋きじゃ言葉は僕に届かないそれでも構わないさ 夢の向こうYou know超える 少しの隙もなく美乳を突き始める割れ焦がした Love Love
Harriker落ちては前へがぬ欲望は限りなく求め合えば Love Love Harriker見つめ合う度 必然のように時線その先 吸い込まれてゆく見つめ合う度
必然のように時線その先超える 温かくはもなく全てを止める 時を止めたLove Love Harriker心を奪われる 背中で騙させる空気づけさ Love
Love Harriker少しの隙もなく 色を突き始める割れ焦がした Love Love Harriker落ちては前へがぬ欲望は限りなく求め合えば Love
Love HarrikerLove Love Harriker間違ったって優勝がない人間だろう毎日が最高じゃないはずなのきっとEverybody on a
shibunHappy one more day共感する日じゃないだけ顔を開けて水は立てる問題ないもっと強くグツグツ煮込んでちょうどいいはずなのシャブでスリット
なだけじゃつまんないよ回るのお月様はいつも赤いのホップステップチャンス二段飛ばしていくよここからは景気よく天下やただ愛想は言わくたい baby大きく口を開けて
Smile and once more smileYou and me just do it地球は回るよ晴れる夜だから何処までも歩いていこう私はね聞いて呼ぶるん
な時真っ赤なリップで唇色付きりぬちょっとしたおまじないで世界は変わってゆくのさ今だよアクションをこそるよLove and this are the door
to our new worldどこかで繋がってるYeah yeah yeahただ涙のぼんだけがGoodbye baby大きな声で歌わないYou and me
forever地球は回るよ晴れる夜だから何処までも歩いていこうI'm a mind rhythmに身を抜かせて足を見とはじけるかGoodbye
yeahただ愛の命は苦手 babyOh keep 息を吸って always be this wayYou and me I like
it地球は回るよ晴れる夜だから何処までも歩いていこうEverything's gonna be alrightEverything's gonna be
alrightEverything's gonna be alrightEverything's gonna be alrightEverything's
gonna be alrightEverything's gonna be alrightEverything's gonna be
alrightEverything's gonna be alrightEverything's gonna be alrightEverything's
gonna be alrightEverything's gonna be alrightEverything's gonna be
alrightEverything's gonna be alrightEverything's gonna be alrightEverything's
gonna be alrightEverything's gonna be alrightEverything's gonna be
alrightEverything's gonna be alrightEverything's gonna be alrightEverything's
gonna be alrightEverything's gonna be alrightEverything's gonna be
alrightEverything's gonna be alrightI'm coming home just coming home急げ
君の住む街へと夜明けば 手の空をかける銀色の翼よ思えば いつでも Sweet Girlfriend変わらぬ君の愛今はじめて
気づいたのさ優しいぬクロリにFlying to your heart 旅立つのさ眩しい予感の中Flying to your heart
巡り逢えたかけがえのない愛を見つめてFlying to your heart 旅立つのさ離れて 出したローリーテインもう 迷いはしない生まれた街
輝く空そして君の音へFlying to your heart 始まるのさきらめく二人の日々Flying to your heart
巡り逢えたかけがえのない愛を見つめてFlying to your heart 旅立つのさ眩しい予感の中Flying to your heart
巡り逢えたかけがえのない愛を見つめてFlying to your heart 始まるのさかとえて言えば Long
train風切り咲いて走るように未来に向かってマッシングランド突き進めば望みは叶う立ち止まらない振り返らないやるべきことをやるだけさ会いたくて
会いたくて離らないから旅に出た会いたい人は君だけの君なんだけど それだけじゃない知らない街で出会いたい本当の自分とI got the truth nowBe
ambitious 曲がっともよ冒険者よBe ambitious 旅立つ人よYou said they are thereBe ambitiousかとえて言えば
Long train夜を貫き走るように光に向かってマッシングランド突き進めば奇跡も起きる立ち止まらない振り返らないやるべきことをやるだけさ会いたくて
会いたくて離らないから旅に出た会いたい人は君だけの君なんだけど それだけじゃない両手を担げ 抱きしめたい輝く夜明けをI got the truth nowBe
ambitious 曲がっともよ冒険者よBe ambitious 旅立つ人に永久を割れBe ambitiousBe ambitious 輝く冒険者よBe
ambitious 旅立つ人よYou said they are thereYou, I got the truth nowBe ambitious
輝く冒険者よBe ambitious 旅立つ人に永久を割れBe ambitiousI got the truth nowBe ambitiousI got
the truth now