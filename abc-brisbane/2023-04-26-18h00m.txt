TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-26-18H00M.MP3
--------------------------------------------------------------------------------
 And in the plan this morning, well, one of the plans this morning is to take a
look at movie taglines. Oh yeah. Which is a bit like, if you were to do
taglines, I guess it's a bit like being a sub-editor. You mean like, just when
you thought it was safe to go back in the water? One of the greatest taglines of
all time. Because Jaws was, you'll never go in the water again. Oh was it? Yeah.
And then just two or three years later, just when you thought it was safe to go
back in the water. That was Jaws 2. It was Jaws 2. Oh, okay. It was very clever.
Yeah, that's very good. So we'll talk about those great taglines for films, the
ones that you've seen. And maybe we can just take a look at some of the films
you've really enjoyed and see if we can find what the tagline is for that film
as well. Yeah, that's a great idea. Excellent. Alright, coming up, Trevor, you
have a great show as always. We will. And also take a look at International
Affairs with Keith Souter later on as well. Excellent. Thanks, Trev. Trev will
have a brilliant program as always and you and I will reconvene tomorrow night.
In the meantime, here's the news. ABC News with David Rowlands. There are
concerns that any moves made to control rent rises will only exacerbate the
problem, as the Greens harden their opposition to a housing fund proposed by
Labor. The $10 billion Housing Australia Future Fund bill aims to provide 30,000
social and affordable homes over the next decade, but the Greens argue it
doesn't support renters enough. They propose to phase out the negative gearing
tax scheme, allowing investors to deduct net rental losses from their income
tax. Property investor Ajana Paliwal claims renters won't benefit from policies
stifling investment. Putting a temporary pause on things is just creating a
slingshot effect. We had the same thing happen in COVID where we're putting lots
of stimulus in one direction. What did it do? It pumped things the other way
with inflation. Here we are 10 interest rate increases later. A Sydney charity
says the rising cost of living is putting support services under immense
pressure. Despite Australia's inflation rate falling from its December peak, the
annual rate has come in at 7 per cent, down from 7.8 per cent in the previous
quarter. But prices for most essential goods and services have continued to
rise, leaving many struggling to make ends meet. Lighthouse Community Services
founder Gandhi Sindhian says his service is in a difficult position. You sort of
support families with what's donated. Money is spent on families where you need
to be very careful with tailoring what you purchase to their needs. Because at
the end of the day you can't just have an open checkbook. It's not just about
food, it's about a lot of other essentials. Australia's Age Discrimination
Commissioner is calling for a national register for power of attorney documents
to prevent the financial abuse of the elderly. The move was recommended in 2017
by the Law Reform Commission to prevent vulnerable people from losing millions
of dollars to dishonest relatives and friends. All Australian jurisdictions have
differing laws and regulations which cause a significant confusion for many
older people and service workers involved in the process. Dr Kay Patterson is
urging the nation's Attorneys-General to commit to improving the system. We've
got to educate people like managers of aged care. We need to educate the call
community about what their rights are. Look, this is one of the worst forms of
elder abuse and it can be fixed if the attorney generals get together and make a
decision they're going to do it. Sudan's army and the police say they have
launched an operation to bring back prisoners who have escaped during fighting
between the army and a rival paramilitary force. Both sides are blaming the
other for the release of thousands of inmates from prisons. Among them is a
former Sudanese government minister who has been accused by the International
Criminal Court of crimes against humanity and war crimes. Despite already
serving a lengthy sentence on charges widely acknowledged to be politically
motivated, Russian opposition leader Alexei Navalny is now being investigated on
terrorism charges. The BBC's Sasha Schlikta reports. Two years ago Alexei
Navalny's Anti-Corruption Foundation was declared an extremist organisation. The
FSB Secret Service also says it, together with Ukrainian intelligence, was
behind the bombing of a cafe in St Petersburg in early April which killed the
prominent pro-war blogger Vladlen Tatarski. Many view these charges as utterly
absurd. The opposition leader appeared in a Moscow court today saying he was now
being investigated on terrorism charges. He said he'd been told by his
investigators he'd be tried by a military court. It's not immediately clear
whether the bombing and the new charges are linked. Back home an Atiwi Islands
elder and mayor of Melville Island says he hopes other communities are inspired
to take action against major oil and gas companies after traditional owners
filed a human rights complaint against top super funds over investments in gas
projects. Roxanne Fitzgerald has the story. Traditional owners travelled
thousands of kilometres to the Tiwi Islands to lodge the complaint arguing the
super funds are failing to meet international human rights standards by
investing in Santos' Barossa gas project. It comes after they sent grievance
letters to a dozen banks and won a federal court case against the gas company
over their lack of consultation. Tiwi elder Pidawangi says he wants super funds
to divest from the gas projects and he hopes other communities feel empowered to
take action. Santos has been contacted for comment. And in basketball, the
Adelaide 36ers have announced the signing of Isaac Humphries on a one year deal
for the NBL 24 season. The 2018 Rookie of the Year returns to the 36ers after
spending last season with Melbourne United. ABC News. Weather for Queensland for
Thursday. Brisbane, the Gold Coast and Sunshine Coast. Showers expected with
tops of 25 degrees. Bundaberg a shower or two in 26. Rockhampton partly cloudy
in 28. Macay a shower or two in 27. Townsville mostly sunny 30. Kansas shower or
two in 29. Longreach mostly sunny 31. Partly cloudy in 31 for Mount Isa. Roma
mostly sunny 28 degrees. Toowoomba partly cloudy in 21. On ABC Radio, you're
with Trevor Chappell. Good morning. It's six minutes past two and it's six
minutes past midnight in Western Australia. We're taking a look at taglines from
films. Taglines aren't necessarily great quotes from films. They're taglines
that are set up to explain the film in a sentence, I guess we can say. So it's
like sub editors who do headlines in newspapers. It's the idea of being able to
give the film a line to put on its poster to go, this is what it's all about.
Thank you for the person that sent the text in. I haven't seen the Predator one.
The tagline for Predator is, if it bleeds, we can kill it. Summs up Predator
quite nicely. 1300, 800, triple two, we will discuss those things a little bit
later on when we have a chat with Tom, our overnight movie fellow. Well so, this
started because we had a listener last week. Not last month when we spoke to Tom
who said the best tagline that she'd seen for a film was, if you run the beast
will catch you. If you stay, the beast eats you, which was for a film called
City of Gold, which I had a look at. Great film. So we'll take a look at that as
well and you joining us of course as we take a look at those taglines. I just
love the tagline for Jaws 2, just when you thought it was safe to go back in the
water. Oh, wasn't that the case? Keith Souter will join us later on to take a
look at International Affairs, so keep that in mind. Keith will be on in a
couple of hours' time. What tends to happen when we talk to Keith Souter is we
have a chat, which is good, and then we'll get people to ring in with any things
that they want to talk about. What happens is that people tend to ring in like
10 minutes before Keith is about to leave us. So if you want to ask any
questions about International Affairs, stories happening overseas, a little
explanation or you just want to pass comment, then do it nice and early please
so that we can make sure we can get your calls in. Otherwise, people miss out
and we don't want that to happen. So think about that. We'll also find out about
something called Orange Sky, which has developed over a period of time. At
start, I think I spoke to the people at Orange Sky maybe eight years ago, and it
has progressed something shocking from two vans with a couple of washing
machines in it to something far greater, which is now moving overseas. So we'll
take a look at that with them, and of course right now though we play. And to
play you need to ring 1300 800 222 to text through the theme because all of
these questions are connected. Text through the theme on 0437 774 774. It was
suggested that the opening songs are a little too easy. So I thought this one
may be a bit more of a challenge. I'm very sure it's doable, but it's a little
bit more of a challenge. I should also point out that it's possibly the longest
snippet of all time. So Predator 2, Just in Town with a Few Days to Kill, says
David Mount Martha. 1300 800 222. Come and play because there are five questions
to answer. You can have a go at any of them. Have a listen to this song and tell
me the name of artists or artists that are performing the song. 1300 800 222.
Have a go. family When she walks to the sea, she looks straight ahead, not at
him. Oh, but she says so sadly, How I wish I could be with you. Oh, but she says
so sadly, How I wish I could be with you. How can we tell her he loves her? Yes,
he would give his heart gladly, But it's day when she walks to the sea, She
looks straight ahead, not at him. Oh, but she says so sadly, How I wish I could
be with you. Oh, but she says so sadly, How I wish I could be with you. Oh, but
she says so sadly, How I wish I could be with you. Oh, but she says so sadly,
Oh, but she says so sadly, How I wish I could be with you. I'll go with Stan
Gets first, but that one was, I don't know how to pronounce the name, Astrid or
Astrid Gilberto? Yeah, yeah, yeah, and Stan Gets, Astrid Gilberto and Stan Gets,
very good. It's a great song. Yeah, there's a new one, two years ago, a
Brazilian woman sang it too. That's a lovely song. Very good. So that's what it
is, a girl from Ipanema, Astrid Gilberto and Stan Gets. Now, because you sorted
that out, we have a who, an in, a when, a where, I have to change that one,
where and a how. You do all hard ones. I do, they're all doable, Joy. Tonight,
I'll pick the riff and have to leave you soon. I've got to tell you, the riff
today is the longest little snippet that we've ever had. All right, I'll try
how. How. No, it's not the snippet, but are you ready? Yeah. How does Ernest
Guavera travel from Buenos Aires with the intention of reaching Peru with his
friend Alberto Granado? Oh, God. Oh, dear. Maybe the snippet would have been
better. Yeah. Can you repeat the names again? Yep. How does Ernest Guavera
travel from Buenos Aires with the intention of reaching Peru with his friend
Alberto Granado? By motorcycle? Yes, by motorcycle. It's Che Guevara, isn't it?
Yeah, it's about Che Guevara when he was younger and his travels, which is, do
you know the name of the book film? The Motorcycle Diaries? Yeah, from the
Motorcycle Diaries. Very good. We have a who, an in, a when and a where. Where.
Where. Is a little snippet. Did you nearly swear, Joy? Yeah. I've never heard
you swear before. I have, but you didn't catch me. Have a listen to this. It's a
very long snippet. Have a listen to this. I want to know the name of the song.
I'll play it again. What's that from? Oh, I know it. Can you play it one more
time, please? Yeah. That sounds like God saved the king. No, it's not right. No,
it's not that. No. Okay. Toodle-loo. Toodle-loo off to you. Sam knew that it was
by motorcycle. Very good work. Let's see what Jeremy can help us out this
morning. Hello, Jeremy. G'day. Jeremy, have a little listen to this. What's that
from? What's the name of the song? Can you play it again? My Bluetooth speakers.
Here you go. There you go. Okay. Could I have it one more time? No, we've heard
it dove enough, Jeremy. Oh yeah, but I can't have it because I was just putting
my earphones on. One more. I don't know. Ave Maria? Not Ave Maria. It's not a
bad choice, but not Ave Maria. Yes, Peter. No, Ginger. Hello, Ginger. Yes, Dave
in Mount Martha. But not quite the title, but I'm presuming you do know the
actual title. Hello, Kim. Good morning, Trevor. I reckon you know this. Things
are getting too easy. Oh, too easy? Oh, my word they are. And the last singing
of the opera, Iponema, was a little bit below the note. A little bit below the
note. A little bit below the note. And I would like to say good morning to June
from Portland. You've done exactly that. Now, if I play this for you, what song
do you think it would be? I think it might be Don't Cry For Me, Argentina.
That's true. Is Julie Covington ever just a little bit below note? I can't say.
I would have to hear her. Well, you will in just a second. Well, more than just
a second. It is Don't Cry For Me, Argentina. Carolyn's worked out the theme.
Nicely done, Carolyn. Lots of people have worked out the theme now. Evita, Don't
Cry For Me, Argentina. Now, Kim, we have a who, an in and a when. As my memory
is very poor, I would say a who, please. A who. Who travels to San Theodorus to
rescue their friend Bianca Castafiore, who has been imprisoned by the government
of General Tapioca? My immediate thought was David Crockett. It wasn't David
Crockett. Thank you. Actually, when you hear the answer, David Crockett is a
very, very interesting one. But thank you. Hello, Glenn. Hi, Trev. How are you?
Hi, Glenn. Not bad. Who travels to San Theodorus to rescue their friend Bianca
Castafiore, who has been imprisoned by the government of General Tapioca? That
would be Tin Tin. That would be Tin Tin. Hey, that's very good. How did I go
with the pronunciation of their names? Oh, very well, mate. I wouldn't have an
attempt to announce it. I remember reading it at school many, many years ago.
Yeah, it was Tin Tin. Oh, it's work. I thought that would have been a little bit
challenging, but obviously not. Oh, I don't want to concur with the previous
call, but I got the other two pretty easy as well. So, I thought you'd make it a
bit harder. So, you're suggesting that it's all too easy today? No, no, no. I
know because I now make a fool of myself. Okay. Well, if you're going to make
those sorts of suggestions, we'll see how we travel next week as far as the
questions are concerned. If we have an in and a when. I'll go with a when.
Actually, this is quite easy too. You see, I thought the Tin Tin one was going
to be a hard one. And I also thought that the motorcycle diaries may be a little
bit difficult too, but obviously not. When my baby smiles at me, where do I go?
You go to Rio. Rio, did you know that? Okay. I don't think it's going to be
harder next week, Glenn. Boy, is it going to be harder next week. Last question.
Are you ready? I think so. In which Brazilian location was Season 6 of American
Survivor set? Yeah, so that's where I'm no chance, mate, because I don't think
I've ever watched an episode of Survivor. No, well, I can't even think of a
Brazilian. Brazilia, the capital. No, but thank you. You've done well. So it's
all a little bit easy today, is it? Hello, Al. Oh, good morning, Trevor. It's
all a little bit easy today, apparently. Well, apparently. I haven't known any
of the answers yet. And I don't know this one. I can't even think of a place in
Brazil. I can't. So in which Brazilian location was Season 6 of American
Survivor set? I haven't got a clue, Trevor, so it's a good night for me. And
it's a good night for him. I think it was also the first season that they split
the gender within the series as well in Survivor. Donald said, oh my God, it's
like all the questions are in foreign language. Well, not really. Not really.
Lots of people have worked out the theme. Jason's worked. No, Jason has said it
was Don't Cry For Me, Argentina. Alfie's worked out the theme. George has worked
out the theme. It wasn't Danny Boy, but thank you, Jane and Mudgy. Dave in
Olympia knew that it was Don't Cry For Me, Argentina. Lots of people have worked
out the theme this morning. Claire, not quite, but you're not bad. This is
Overnight on ABC Radio. Hi, Kate O'Toole here. Planes, trains and autos. If it
has a motor and gets you from A to B, it's being celebrated at the Planes,
Trains and Autos Festival at the Workshops Rail Museum in Ipswich. And I'll be
there too. So come along or listen out for fabulous stories that will really
take you places. That's this Saturday, right here on ABC Radio Brisbane. Jeffrey
from the Gong, we've had a look and we've actually taken it down to seconds. You
beat Carolyn by a smidge. What was it? About five, ten seconds. You beat Carolyn
by. So Jeffrey from the Gong, send in a text with a song on it and we'll play it
for you in just a little while. Rosa down on the corner knows exactly what we're
talking about. Ginger from La Trobe knows exactly what we're talking about as
well. Pete's in Dunsborough. Hello, Pete. Hello, Trev. How are you going? Good,
Pete. In which Brazilian location was Season 6 of American Survivor set? Damn,
mate. I've like all the questions before and then I've got no idea. I'm just
going to go with the Amazon. Where? The Amazon? Yeah. That's exactly where it
was. You're joking. Yeah, it's exactly where it was. They set it in the Amazon.
I mean, really, if you're going to set Survivor somewhere in South America,
you'd choose the Amazon, you'd think. Yeah. My father's pathway always used to
call it the amazing. The amazing. The amazing. Yeah. Which it would be. I'd love
to go to the Amazon. Anyway, Pete, that makes you a winner for the morning. Have
a chat with Pav and we will send you out a magnificent prize. No matter how hard
you think your questions are, Trevor, there will always be someone that knows
the answer. That is very true, Claire. That is very true. And the theme, yes,
would be South America. That's the theme this morning. The film that we're going
to take a look at, City of Gold, was set in South America, which takes a look at
the gangs in Rio in the 60s, 70s and 80s, child and teenage gangs in Rio, in the
Rio de Janeiro slums. So we'll take a look at that film. We will also take a
look at the great taglines of all time. So keep them in mind. 1300 800 222 if
you want to join us in that conversation with Tom in just a little while. We
haven't heard anything like Don't Cry For Me Argentina in a very, very, very
long time. It's a beautiful song. It won't be easy. You think it's strange. When
I try to explain how I feel. That I still need your love after all that I've
done. You won't believe me. All you will see is a girl you once knew. Although
she's dressed up to the nines. At sixes and sevens with you. I had to let it
happen. I had to change. Couldn't stay. Down. Looking out of the window. Staying
out of the sun. So I chose. Running around. Trying everything. But nothing
impressed me at all. I never expected it to. Don't cry for me Argentina. The
truth is I never left you. All through my wild days. My mad existence. I kept my
promise. Don't keep your distance. And as for fortune and as for fame. I never
invited them in. Though it seemed to the world. They were all I desired. They
are illusions. They're not the solutions they promised to be. The answer was
here all the time. I love you and hope you love me. Don't cry for me Argentina.
The truth is I never left you. All through my wild days. My mad existence. I
kept my promise. Don't keep your distance. Have I said too much? There's nothing
more I can think of to say to you. But all you have to do is look at me to know.
That every word is true. Don't cry for me Argentina. It's a beautiful song.
Julie Covington and Don't Cry For Me Argentina. City of God is the film that
we're talking about. I've got a couple of texts talking about City of Gold. The
film is City of God which was made in 2002. No it's not a current film Terry.
The reason we're doing it is because last week we had a text from someone saying
that City of God even though it's subtitled as an excellent film I recommended
to my parents and they equally enjoyed it based on the organised drug crime
scene in Brazil. The tagline is if you run the beast catches you. If you stay
the beast eats you. Says Rachel last time we take a look at film. So we're
taking a look at that film and any other films that you want to talk about the
taglines and how the taglines work so particularly well. Hello Patricia. Hi how
are you? Thank you so much for the beautiful music. I listen to you people all
the time. Thank you. Can you play Harry Belafonte, Jamaican Farewell I think
it's called. I think that we should because it's important to still recognise
the incredible life that Harry Belafonte had both musically and also as a
spokesperson for human rights. He was a very significant person was Harry
Belafonte. Absolutely I followed his career you know his time and I would always
admire his music but he's also his work that he done you know in the community.
True we will play that song. Thanks Patricia. Oh thank you so much thank you bye
bye. Toodle-oo. My heart is down my head is turning around I had to leave a
little girl in Kingston town. Down at the market you can hear ladies cry out
while on their heads they bear. Ackee rice salt fish are nice and the rum is
fine anytime a year but I'm sad to say I'm on my way. Won't be back for many a
day my heart is down my head is turning around I had to leave a little girl in
Kingston town. Sounds of laughter everywhere and the dancing girls sway to and
fro. I must declare my heart is there though I've been from Maine to Mexico but
I'm sad to say I'm on my way. Won't be back for many a day my heart is down my
head is turning around I had to leave a little girl in Kingston town. Down the
way where the nights are gay and the sun shines daily on the mountain top. I
took a trip on a sailing ship and when I reached Jamaica I made a stop but I'm
sad to say I'm on my way. Won't be back for many a day my heart is down my head
is turning around I had to leave a little girl in Kingston town. Sad to say I'm
on my way won't be back for many a day my heart is down my head is turning
around I had to leave a little girl in Kingston town. Harry Belafonte and
Jamaican Farewell. Jeremy Langkove says thanks for all you do and check out City
of Men, a spin off TV series from City of God. The film which is a semi-
autobiographical story about the photographer. It's true, it's a terrific and
City of Men, a spin off series, thank you. If you want to take a look this
morning at taglines in movies and you want to point out what are some of the
best ones that you know or doesn't even have to be some of the best but some of
the films that you've liked that have got good taglines. 1300 800 222 text us
0437 774774. And what makes a good tagline? Does it need to be able to sum up
the film? Does it have to be specifically summing up the film or just something
about the way the film feels? Give us a ring 1300 800 222 we'll talk to Tom
shortly. Two Brisbane teams, two Brisbane wins and what about that comeback by
the Dolphins. The Dolphins are in front. The traffic 26 plot in the first half
to round nine of the NRL and the Broncos host the bunnies in a blockbuster at
8pm on Friday night. Well the Dolphins head to Wagga to take on the Raiders at
3pm on Saturday. We've even got the Reds in action at half past seven that
night. Our local footy teams on our local station. So make sure you stay tuned
to ABC Radio Brisbane. This song for Jeffrey who worked out the theme. We're
talking about a chosen place. You want to sell it in a marketplace. You know.
About the door. People. The. The. The. The. The. The. The. The. The. The. The.
The. The. The. The. The. The. The. The. The. The. The. The. The. The. and they
don't know where it comes from. A lot of people don't know where it comes from.
I guess it's really a quote, but it sums the movie up, I think. That's what
they're doing. They're on a mission to God. That's exactly what they were. I
wonder what the tagline was, though, for Blues Brothers, and whether or not that
was actually the actual tagline for the film. Because there's plenty of quotes,
you've got to say. There are plenty of quotes, but whether or not that's the
actual tagline from the film. If anyone knows that, by the way, give us a call.
1300 800 222. Text 0437 774774. Because that's the difference. There are some
terrific quotes for films, but does that necessarily mean that it is the
tagline? Alright, there we go. The Blues Brothers tagline. They'll never get
caught. They're on a mission from God. The most dangerous combination since
Nitro and Glycerin. That's a very good tagline. My favourite tagline from a film
is from The Castle. This is going straight to the pool room. Very good example,
Jen, of a great quote from a film, but not necessarily the tagline that went
with it. And we'll keep on, we'll take a look at those, the difference between
the two, and whether or not the tagline is actually better than the quote, or
whether or not the quote's a bit in the taglines. Give us a call. Gary. Hello,
Gary. G'day, how you going? Good. That's the way. Look, I've got two quick ones.
The first one is, well, they're first, the original and the sequel, but I'll do
the sequel first. It's Evil Dead 2, and the tagline was just one word, groovy.
Groovy? Groovy. That's what it was, because when his arm, his hand got cut off,
somehow fashioned with just one hand, a way to put the, what do you call it,
chainsaw onto his arm, and as soon as he clicked it on, he looked at it and just
said groovy. So do you need to have seen the first one to understand the tagline
for the second? No. No, you don't. Because the first one was actually, the
tagline was, I think, I think it was a way because the movie was going to become
a flop until they saw what Stephen King had wrote in an article in the paper
saying the most ferociously original movie of the year. So they used that as a
tagline and put Stephen King on there, and it became a hit. Wonderful. Thank
you. So groovy. Groovy was for two. Great taglines. Hello. Yes, for a great
tagline. Now is it a quote or a tagline? Tagline. If you build, Field of Dreams
is the movie. Yep. And it's, if you build it, they will come. It's a great line
and it's a great tag, because it really does describe the film well, doesn't it?
It does. Yes. It's one of my favourite films, that one. Same here. Yeah. If you
build it, they will come. Beautiful line. And it does. It sums it up the film
quite nicely for Field of Dreams. If you haven't seen it, it's worth a look.
It's a bit of a... Well, it is a ghosty film, but maybe a nice ghosty film.
1300, 800, triple two. Hey, Donald. Hello, Trevor. A tagline. Can I give you a
three? You can give me three. OK. How about the obvious is a long time ago in a
galaxy far, far away. Which is a very good one for Star Wars. For Star Wars. In
space, no-one can hear you scream. Which is one of the greatest ones of all
time, you'd have to think. Yes, yes. From Alien, of course. The first Alien
movie. Alien 3 had the bitches back. That's a really good one too. Alright.
Alright. And my final one is from the Jeff Goldblum, Gina Davis, The Fly. Be
afraid. Be very afraid. Thank you. How often does be afraid, be very afraid get
used? And I wonder whether or not people know exactly what the tagline is for
that as well. They blow the good taglines, the bad ones are better. The man with
the iron fists, you can't spell Kung Fu without an F and... Without an F. Thank
you. And Bridget Jones's baby, we're going to need bigger pants. So perhaps, and
that's a good point John, perhaps some of the bad taglines are just as good. If
you want to give us some bad taglines, you're more than welcome. 0300 800 222
text 0437 774774. Good morning George. Good morning Trevor. Do you like a good
tagline? Oh well, they're so important. I mean, so the movie companies spend
lots of money and time getting the tagline right, because for that very reason
the previous gentleman said that the right tagline or the right quote will get
people in the door. Do they tend to use them that much anymore because we don't
really have the posters, so are they bothered with the taglines? Oh, they still
have them, yeah. They're still in the evening with some wine stuff. And Jimmy
has just spoken his voice over by the guy whose name I've forgotten now who used
to do all the movie... He's the best....in the world. In a world where no one
has any... That guy. I'll do it. I was going to do the Shawshank Redemption and
the serious ones. I'll see if I can do it in that voice. Can I get it right?
Here we go. Fear can hold you prisoner. Hope can set you free. That's a great
tagline. I like that one. But of course, as you know, I like to go from the
sublime to the ridiculous. So a couple of the other ones that I like, some of
them, there's multiple ones in some cases, but I wanted to mention Deadpool, as
we know, the rather voice-cracking kind of anti-hero. With great power comes
great irresponsibility. I like that. And then another one, which I really like,
which is one of the phrase in three parts, you know, bang, bang, bang, bad ass,
smart ass, great ass. And I'll just go over Toxic Avenger, which is the guy who
sort of becomes a superhero when he falls into a ball of toxic slime and becomes
this kind of triggered kind of... So he's actually a guy who's cleaning this
toxic tag. He's now 98 pounds of wiggling, so it goes, he was 98 pounds of solid
mud until he became the Toxic Avenger. Thank you, George. Toxic Avenger. I did
like, they were seven and they fought like 700. If you can work out what film
that is. Tagline from Superman, you'll believe a man can fly. Lovely. You see,
don't get confused with quotes from films and the taglines. And a really good
quote from Jaws being, we're going to need a bigger boat. However, the tagline
from Jaws was very different. The tagline was, you'll never go in the water
again. The tagline for Jaws 2 was just when you thought it was safe to go back
in the water. If you want to join us, 1300 800 222. This has been Steve Austin,
weekday afternoons from 3.30-ish on ABC Radio Brisbane. 1300 800 222 as we take
a look at great taglines. What makes a great tagline? Give us a call. 1300 800
222. Tom Cushing, Film Fellow, joins us to take a look at taglines on the
program this morning. Manager of the theatre Royale. Good morning, Tom. Good
morning, how you going? Do you like Royale better than Royaled? Here's the
problem. I do. And as previously discussed, I also say Castlemane. So I'm a
complete fraud. Royale in Castlemane. Because technically speaking, I believe I
work at, you know, I manage the theatre Royale in Castlemane. I'm constantly
talking about the theatre Royale in Castlemane. But tell me it doesn't sound
better. Hey, I'm saying I'm with you. Totally with you on that one. Do you like,
because we need to take a look at the difference between a tagline and a quote.
Because they get mixed up sometimes. Yes, and often the tagline is made by a
completely different team and can be bizarrely disconnected from the movie. But
I think a poster and a tagline are almost a separate art to making a great
movie. So the poster and the tagline, what are you saying are completely
separate? No, no, well, I'm saying often, not always the case, but a filmmaker
will be in charge of the film. And then obviously it gets sort of shipped off to
advertising and they go, how do we sell this? Now, there are some famous people
who are filmmakers and very much advertisers like Schluckmaster, you know,
William Castle or something. Oh, see, I just said Castle there. And that their
kind of film will be built into the marketing. But often, like in the case of
Jaws, I don't think Spielberg had a lot to do with, you know, just when you
thought it was safe to go back in the water. It's such a good tagline. It is
good. It's so good it almost makes you forget the terrible tagline of Jaws the
Revenge. This time it's personal. A big shot of a shark. So they're really a sub
editor. So the film gets sent off to the advertising. So basically it's a sub
editor who needs to take, do you reckon they actually watch the film before they
work it out? Sometimes they definitely don't. I mean, I think these days that
they almost always do. But there are definitely like examples where like my most
loathed tagline ever was for Royal Tenenbaums. A very sort of specific, somewhat
tweeze I would say. But like, you know, the tone is like very, very specific.
And the tagline for it is family is not a word. It's a sentence. And you're
like, you know, you never watch this movie. You had to come up with something
about five seconds before you walked into pitch. Overnights on ABC Radio. You
can join us on 1300 800 222 as we investigate taglines. The good ones, the bad
ones and see how they operate. Andrew, good morning. Good morning. The tagline.
And do you think it works well? It does indeed at certain times. But run for us
run. Is that the tagline from the film or is it a quote from the film? Oh, I'm
not sure. I'm pretty sure it's what is a tagline now anyway. It's in movie
history. Yeah, it's a famous quote to be a tagline. There's got to be a poster.
It's going to be a poster for Forrest Gump and it's going to say the words run
for us run. Is that what the yeah. And so I mean, I'm loving the I'm loving the
quote, Andrew. And possibly go. Sorry, go on. I love the smell of napalm in the
morning. Great quote as well. What is the tagline? I wonder for a pop. Yeah, I
don't think it's could be it could. I think often they're not quotes is the
thing because you have to understand why that is. I think is an amazing quote
run for us runs. Another good example. You need to have watched the movie. Now,
a tagline is a is a question. So interesting. Only the movie can answer it,
which is kind of a separate thing. A tagline is supposed to lure in a person who
has no context for the movie, but it's like I've got to understand how to answer
that question. You know, the thing, you know, man is the warmest place to hide.
No one says that in the movie, but you hear that you hear man is the warmest
place to hide and you think what the hell is that all about? I go see that. And
then in that question is answered by the film because man is the is the warmest
place to hide is from the thing, which is such a good quote, because that's
exactly what it is. And it's a great example of that tagline and that that
poster, the iconic poster was made before anyone had actually seen the movie.
The thing about a quote from a film is that people don't know that that line
from the film is going to be a memorable quote as well. Because for the people
who are just trying to work out what they're going to do for a tagline, they're
not going to be able to pick out a line from the film and then make that,
although I possibly possibly do with some if it's really obvious. Yes, I would
think if I was the guy who sat down to watch Forrest Gump and had to come up
with a tagline that pulled from it, my guess would have been life is like a box
of chocolates. And apparently off the end of that, it's life is like a box of
chocolates. You never know what you're going to get. And when I watch that movie
with no context, I don't know if I would. The story of a lifetime Gump happens,
Gump happens here. There's a whole stack of ones for that apocalypse. Now we
were just trying to find one. And I don't think there is one for apocalypse now
because that's the thing is that some films just don't have one. Well, here's
the thing. They called it apocalypse now. Such an insanely good title. It's like
I'm not sure you need to add anything else. That's the other thing is the title
itself, a tag, which will bring you in. Yeah, I mean, like I think even things
like psycho or whatever, like that just sounds so good. I think psycho
technically, because I know that there are posters with psycho head had like
check in, relax, take a shower. Which is just not because I saw that for psycho
for the tagline for it. And to me, check in, relax, take a shower doesn't really
tell me anything about the film. Only works as like a weird joke if you've seen
the film, which seems to somewhat defeat the purpose, which is my favorites are
the ones that like the lower budget horror films that are just trying almost too
hard. Like if you look up the original poster for Last House on the Left, it
has, I have to say, at least three taglines that form a paragraph in like I
think as many different fonts because it's got Last House on the Left. And
you're like, OK, good title. It rests on 13 acres of earth over the very center
of hell. Great. Sounds good. Oh, hang on. No, there's another one. Marie 17 is
dying. Even for her, the worst is yet to come. She lived in the last house on
the left. And you're like, OK, that's a lot. And then it's got the now famous to
avoid fainting. Keep repeating. It's only a movie, only a movie, only a movie.
So clearly the thought was like, look, we're aware someone's going to cast Naia
with this poster. And they may not stay on it for too long. We've got to have
three different taglines ready to rock and roll. You've almost got to stand
there for about three minutes and read it all. Yeah, totally. It's like some of
them are so dense. And I find like it tends to be those sort of shocky genre
ones that have that. Maybe I don't know if it's a lack of confidence or it's
just a style thing. But you get these like little mini novellas. Glen, good
morning. Good day again, Trev. Good day, Tom. How are you going? Good, how are
you going, man? I was in the early 80s called Blood Beach and their tagline was,
just when you thought it was safe to get back to...