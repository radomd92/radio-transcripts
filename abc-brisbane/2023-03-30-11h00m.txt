TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-03-30-11H00M.MP3
--------------------------------------------------------------------------------
 program for today. Thanks for joining us. Good night. ABC News. Good evening.
I'm Michael Boush. State Parliament has passed new laws expanding the search
powers of Queensland Police. Toby Jers Lewis has details. The legislation
broadens a wanding trial that's been running on the Gold Coast since 2021. It
means officers will be able to use handheld metal detection wands to search
people for weapons until 2025 in all Queensland safe night precincts, at public
transport stations and on vehicles. It's called Jack's Law, after teenager Jack
Beasley, who was fatally stabbed on the Gold Coast in 2019. His parents, Brett
and Belinda, have campaigned for the changes. It's very humbling. It's all a bit
overwhelming, to be honest, and we're super proud. The legislation will be
presented to the Governor for Royal Assent on Sunday, what would have been
Jack's 21st birthday. A 27-year-old man has appeared in a ballon at court in
northern New South Wales accused of assaulting two teenage girls on the Gold
Coast. Dominic Hansdale reports. Police allege that just after midnight on
Tuesday, a man offered a 17-year-old girl a lift before driving erratically
around Palm Beach for an hour while touching her thighs. They say the girl
jumped from the car in hidden bushes before the man drove away. About four hours
later, police allege the same man grabbed another 17-year-old girl around the
waist and back at a popular park before she ran away. The man is currently in
custody in New South Wales. And police are continuing work to extradite him and
are asking anyone with information to come forward. Australia's chief medical
officer has warned that fresh waves of COVID are on the way and booster rates
need to increase before winter. Lexi Hamilton-Smith reports. Figures show more
than a million people, mainly over 60, had a booster shot last year. A new ad
campaign is being rolled out to try to lift that number, which Professor Paul
Kelly says is critical for the health of the nation. He says COVID cases have
been rising in recent weeks, but at this stage it's more of a ripple than a
wave. Meantime, the subsidised COVID antiviral, Paxlavid, will be more widely
available from April 1. The federal government says people aged 60 to 69 with
one risk factor for severe illness instead of two will now qualify. More than
160,000 Australians are in that category. Meantime, the federal government is
set to wind up its remaining pandemic leave payments, but there's concern the
replacement scheme is far too narrow, as Stephanie Dalzell reports. Workers in a
range of high-risk settings, including aged care, disability care, Aboriginal
health care and hospital care sectors, can currently access financial support
from the federal government if they contract COVID-19 and don't have paid leave.
However, from Saturday, that scheme will be replaced by a new commonwealth
program which is only available for aged care workers. NDIS Minister Bill
Shorten says COVID is still affecting people in the disability sector. A lot of
disability carers are casually employed. They don't have a lot of leave. Mr
Shorten says he'll discuss the decision with the sector and government. Well,
after months of negotiations, one of the federal government's key climate policy
measures has passed through parliament. Tom Lowrie reports from Canberra. The
new laws will strengthen the safeguard mechanism, a scheme that caps the
emissions of Australia's heaviest polluters. It's a key plank of the
government's climate policy, and its passage required lengthy negotiations with
the Greens. Climate Change Minister Chris Bowen has told parliament it's a
significant achievement. What the parliament has done today is safeguard our
climate, safeguard our economy and safeguard our future. Shadow Climate Change
Minister Ted O'Brien has argued it will hurt businesses and consumers. This is a
tax that will see prices go up. Under the plan, emission caps will be lowered by
roughly 5% a year through to 2030. To finance and on the markets, the ASX200
gained 72 points today. It closed at 7,122. The Australian dollar is worth 67.11
US cents. To the weather for Queensland, scattered showers and storms tomorrow
for the northern inland and the central coast, mostly fine elsewhere and less
humid in the southeast. Brisbane's temperature range, 16 to 29 degrees. The
Sunshine Coast, 16 to 28. The Gold Coast, 17 to 27. Ipswich, a low of 13 with a
top of 30. Mostly fine for the southeast on Saturday, a few showers for Sunday
and Monday. On Moreton Bay, west to northwesterly winds 10 to 15 knots, seas
below a metre. This is ABC News. This is ABC Radio Brisbane and Queensland with
Joel Spreadburer. Thursday. Thursday is here. It's lovely to be with you. I get
particularly excited, whether I'm doing on this show, producing this show,
working on this show, presenting this show or even just listening to this show
because Thursday means just one thing here on ABC Radio Brisbane and Queensland.
I don't know the words properly, but gee, I wish I did. Quentin Ellison is here,
host of Friday Now on Four Triple Z, an absolute musical maestro. I'm going to
go out there and say it straight away, mate. Thank you. I love the maestros out
there. There's a conductor who could be a maestro as well. They know more than I
do. But look, I can't reach the high peaks of that. I'm trying to think who that
was singing there, but surprise, surprise. So it's good to be back. It's been
almost a month. It has been a month. The last time you came, the last time I did
one with you, we did the first time we met, which I think was a lovely theme
because it was pretty much the first time we met. We did. And since then, you've
been very busy. You're a busy guy. I'm a busy guy. You know, we work a lot. We
do all of that. Never a dull moment. You know what? There's a theme in that. I
did think that and that's why I threw it at you. I started this one tonight. You
did. Because so we are talking about, well, you don't know what we're talking
about. So I came up the idea of jobs because I just started a new job at the
time when you were asking me to come on a Thursday. I was like, ah, I can't
because I'm actually working. I remember the email. It's like, I've got this
song, then I've got this and this and this. And then there's that that clashes
with that. I was like, wow, Quentin, you are very busy. I've got about five jobs
going and I'm sort of going for another one at the same time. What happens when
you're casual and don't own a house, you don't have kids. So you just become
carefree and Busy yourself. And careless. Busy. Yeah, sure, sure. It's a good
theme. So look, it's songs with job titles in them. That was our original one.
And you've come straight back with an absolute ripper list of songs with job
titles in them. I've, look, taken a couple of liberties and expanded a couple
that don't necessarily have a job title in them, but very much are around the
theme of work. So I've expanded the theme to job titles and songs about work.
Yes. Is that okay? With employment in it. Yes, exactly. A loose theme. But we're
centering around employment, work, being busy, all of that. And look, I think
it's really good if you're listening at home and you have some suggestions or
some thoughts on what you hear. Always happy to hear from you 1300 222 is the
phone number 0467 922 612 on the text line. That shall be the case all night.
But Mr. Quentin Ellison from 4 Triple Z and four other jobs as we speak. Yes,
including the unpaid one and this one as well. This would be number six because
I get paid for this one, but I'm still working. I can give you a fridge magnet.
You have to do quiz though, those rules. I know the answer. So you're going to
say something. I'll make it about anything but music because you'll blitz it
straight away if it's about music. What are you going to get us underway with
tonight? That's a good question. I'm just trying to figure that out. I don't
have the sheet on me. That is completely okay. Okay, just put people, you did
not hear that. The magic of radio. Jinks, we did that in stereo. Okay. Now this
one, number one, number one that I chose is Jackson Brown, The Loadout. The
reason I put this one in because I do driving bands like we just did, not fest,
slipknot was in Brisbane. And I'll do other things like country music channel
was driving country music stars and metal and whatnot. And at the end of the
night, you see the roadies and the loaders and the riggers at some ungodly hour
till when the sun comes up, they pack the whole thing down and they leave for
the next one and don't sleep. And that's a job in itself. You have to tip your
hat out to the riggers and roadies who do that kind of thing to entertain us.
Oh, 100%. And the magic wouldn't be possible without them basically. I think
it's a good choice. It's a strong choice to get us underway on ABC radio,
Brisbane and Queensland Thursday. Happy hour. I'm Giles Bradbury. Quentin
Ellison is here as well. Here's Jackson Brown and The Loadout. I'd like to do a
song I never played in public before. It's a brand new song. It's sort of a
tribute to the friends of mine that come out here on the road and to you too.
Now the seats are all empty. Let the roadies take the stage. Pack it up and go.
Pack it up and tear it down. They're the first to come and the last to leave.
Working for that minimum wage. They'll set it up in another town. And that was
sweet. I can hear the sound of slamming doors and folding chairs. And that's a
sound they'll never know. Roll them cases out and lift them amps. And haul them
trusses down and get them up them ramps. Cause when it comes to moving me, you
know you guys are the champs. But when that last guitar's been packed away, you
know that I still want to play. So just make sure you got it all set to go
before you come from my piano. But the bands on the bus and they're waiting to
go. We gotta drive all night into the show in Chicago. Or Detroit. I don't know.
We do so many shows in a row. And these towns all look the same. We just pass
the time in the hotel rooms and wander around backstage. Till we hear that crowd
and we remember why we came. Now we got country and western on the bus. R&B. We
got disco and eight tracks and presets and stereo. And we got rural scenes and
magazines. And we got truckers on CB. And we got Richard Pryor on the video. We
got time to think of the ones we love while the miles roll away. And we got the
time to think of the ones we love while the miles roll away. And we got the time
to think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we love while the miles roll away. And we got the time to
think of the ones we want while the miles roll away. It's different every time.
Now, Michael Stkorof菜喘, who went up to Miami, picked up his guitar and was
having a competition with Yoga for the accident. He said, you know, Michael, I
recommend you have the guitar looks and, you know, appreciating what you did so
driving to a gig one night and he heard a radio guy say, well, good news folks,
we're taking care of business, which was apparently his catchphrase, and he
goes, wait a sec, here we go, White Collar Worker becomes taking care of
business, we make a few tweaks to the melody and the chorus and the rest is
history, it becomes a hit, it's their longest standing single on the US
Billboard charts. It's a ripper of a song, it's another real big pop culture
song, it pops up here, there and everywhere in film, TV, advertising, all of it,
it's a good number, Quentin. Yeah, sometimes when I hear that I keep thinking
primal screens, get your rocks on, get your rocks off, if you hear the chugging
of it, you put it together, if you know the song, let's sing along, let's put
paperback writer to the lyrics to this one as well, see how it sounds. Let's
have a sing along on ABC Radio Brisbane in Queensland, happy hour Thursday, the
theme tonight is songs with job titles or songs about working, and here's
Bachman Turner, Overdrive, taking care of business. They get up every morning
from the long clocks, wanting to take the 8.15 into the city. There's a whistle
up above, a people pushing people, shoving at the girls who try to look pretty.
And if your dreams aren't time, you can get to work by nine, it's Bach's
sleeping job to get your pay. If you ever get annoyed, boot with me, I'm self
employed. I love to work at nothing all day, and I've been taking care of
business, every day, taking care of business, every way, I've been taking care
of business, it's all mine, taking care of business, and working overtime. If
this was easy as fishing, you could be a musician. If you could make sounds,
laudamello, get a second-year in guitar, chances are you go far. If you're
getting with the right machinimello, people see you having fun just lying in the
sun, tell them that you like it this way. It's the work that we're affording,
we're all self-employed, we love to work at nothing all day, and we've been
taking care of business, every day, taking care of business, every way, we've
been taking care of business, it's all mine, taking care of business, and
working overtime. Take good care of my business when I'm away every day. They
get up every morning from the long clocks, wanting to take the 8.15 into the
city. There's a whistle up above, people pushing, people shoving at the girls,
they're trying to look pretty. And if they trade on time, you can get to
overnight, and start your slave and chop to get your pay. If you ever get
annoyed, look at me, I'm self-employed, I'm self-employed, I love to work at
nothing all day, and I've been taking care of business, every day, taking care
of business, every way, I've been taking care of business, it's all mine, taking
care of business, working overtime. Taking care of business, too. Taking care of
business� That's Barkman Turner overdrive there with Taking Care of Business, an
appropriate theme I think for ABC Radio Brisbane and Queensland. On your
Thursday evening Taking Care of Business with Quentin Ellison from 4 Triple Z.
We'll be back to continue the theme of job titles and songs about work in just a
tick. ABC Listen. Rampaging Roy Slaven and HG Nelson. I note that some people
don't use log books anymore. What? Yeah. They don't know cos, sine, all
that's... Yeah, cos, sine, cosine, cotan. They're all great. Lunching on the
blindside. Why don't they do it? I don't know. I don't know. I think Royal
Commission. What happened to the log book? I loved it. I start to love it. Could
we get the Landis to drag the log book? Keep screaming back at our universities.
Podcast available now on the ABC Listen app. You're with Joel Spredver on ABC
Radio Brisbane and Queensland. You are indeed. You're also with Mr Quentin
Ellison, host of Friday. Hello. It's lovely to have you here. It's good to be
back. I'd love some more context on just how busy you are. It's a real privilege
to have you in the studio. You're leading the charge tonight. Now we're up to
song three on the job titles and work. What have you got for us, Quentin? Okay.
Well, American Psycho quoted this group so many times and I can understand why
because it's my favourite band from the 80s. That's Huey Lewis and The News.
1982 was...pictured this album. It was two minor hits, I guess, Do You Believe
in Love and Working for a Living. So I think that might have been a side as well
from back then. It references a number of occupations and one of the songs we
may play later on is also featured in that song, in this occupation, and it's
the world's oldest occupation amongst bus boys, bartenders and the ladies of the
night. And if you know the song, it is a quick three minutes, 20 or something
like that? I think it's a short one. And if you know the song and you listen to
the program, it's actually a dirty dozen staple as well in 1964. So it's there.
It's been around in the last couple of weeks. Okay. Great song. So this one is
Working for a Living. And if you know the song, Sing Along, because I would sing
along in the car with this one when I had a car at the time. But it's a good
little number, this one. You know, one of these days I'm going to sneak your
microphone back on during a song so that all the listeners can hear your
wonderful sing along too, because it's a bit of a treat. I'm going to Blutes in
a couple of weeks, apparently, of a friend's birthday and we might do some
karaoke there. So you might drop on in on the six. Oh, I've given away the
details. Sorry, Kerry. Look at the excuse to get up on karaoke. Michael from New
Farm says, apparently, Backman Turner Overdrives taking care of business was a
favourite of Elvis Presley. So there you go. Yeah, hence his rings. Oh, OK,
joining the dots here. Good on you, Michael. Thank you. So let's move on. ABC
Radio, Brisbane and Queensland, Huey Lewis and the News as chosen by Quentin
Ellison. It's Working For Eleven. Working for Eleven, they're having a working.
I'm taking what they're giving cause I'm working for Eleven. Hey I'm not
complaining cause I really need to work but hitting up my buddies got me feeling
like a jerk. Hundred dollar condo, two hundred rent. I get a check on Friday but
it's already spent. Working for Eleven, working, working for Eleven, working,
working for Eleven, they're having a working. I'm taking what they're giving
cause I'm working for Eleven. Working for Eleven, working, working for Eleven,
working, working for Eleven. Bus Boy bought ten ladies of the night. Grease
Monkey X, junkie winner of the fight. Walking on the streets, it's really all
the same. Selling souls, rock and roll, any other game. Working for Eleven,
working, working for Eleven, working, working for Eleven, they're having a
working. ABC Radio Brisbane in Queensland, Hughie Lewis and the News working for
a living, the theme tonight being job titles, songs about work. Quentin Ellison
from Four Triple Z is here. And Inspired Choice, you're a big fan of Hughie
Lewis. And working. And working. He has six jobs. Yeah, yeah. Hey, this is a lot
of fun tonight and thank you for everyone that's participating. I've got a
couple queued up here and I'll give a shout out quickly to Marie in Manly who's
texted him with a couple of good suggestions. I've got a, I don't know where to
start here Marie. I think we might go a bit of Elvis Costello because you've got
in and suggested watching the Detectives by Elvis Costello. What do you reckon,
Hugh? Should we follow this rabbit trail? I reckon we should. Look, it probably
has a different meaning to what we think but, you know, when I think about it,
you know, Detectives up all night watching, staking, stalking, stake out, that's
the word, you know, for God knows how much money they'll probably earn, probably
nothing, but not getting sleep, eating bad food even, you know, and that is an
occupation. And, you know, you've got the firms out there who are trying to bust
people who think they've maybe rorting the system. So yeah, that's a pretty
strenuous and hard occupation to be in but interesting at the same time. A very,
very interesting one. It's a 1977 release. The album My Aim Is True from Elvis
Costello, also the title of a film I believe from early, early noughties I
believe the movie was. Cannot remember who was in it but anyway. Cillian Murphy,
that's who it was, the guy that's in, I'm having a moment now. Yes, the guy who
was in, yeah, that one. Peaky Blinders. Peaky Blinders, thank you so much that
one. Anyway, I digress, Marie, thank you for your text. Let's hear it right now.
It's Costello on ABC Radio, Brisbane and Queensland watching The Detectives.
Last girl's that one with the deep fix, telepathic rat so correct. With her dogs
under illegal legs. She looks so good that he gets down and fixed. She's
watching The Detectives. Ooh, he's so cute. She's watching The Detectives. When
they shoot, shoot, shoot, shoot. Beat him up until the tears drop start. But he
can't be wounded cause he's got no heart. Long shot at that jumping sign.
Visible shivers running down my spine. Cut the baby taking off her clothes.
Close up of that sign that says we never close. You snatch a junior match a
cigarette. She pulls the eyes out with her face like a magnet. I don't know how
much more of this I can take. She's filing her nails while they're dragging the
link. She's watching The Detectives. Ooh, he's so cute. She's watching The
Detectives. When they shoot, shoot, shoot, shoot. They beat him up until the
tears drop start. But he can't be wounded cause he's got no heart. You think
you're alone until you realize you're in it. Now fear is here to stay. Love is
here for everything. They call it instinct just went past the legal limit.
Someone's scratching at the window. I wonder who is the third detective from The
Jack-O'-Billy. You belong to the parents who are ready to hear the rest about
their daughter's disappearance. So it nearly took a miracle to get you to stay.
It only took my little fingers to blow you away. Just like watching The
Detectives. Don't get cute. Just like watching The Detectives. Don't get so
angry when the tears drop start. But you can't be wounded cause you've got no
heart. Watching The Detectives. It's just like watching The Detectives. Watching
The Detectives. Watching The Detectives. Watching The Detectives. Watching The
Detectives. That's Elvis Costello watching The Detectives. ABC Radio Brisbane
and Queensland. It's Thursday, it's happy hour and it's about work. It's about
job titles, Quentin Ellison, that text. That request coming from Marie in Manly.
I tell you what, Marie has sent through three or four absolute rippers. What
else did she send? So we've got Lawyers, Guns and Money by Warren Zevon. Zevon,
yeah. And we've got Delivery Man by Cruel C. Another Cruel C number that I've
got in a separate request is of course, Better Get a Lawyer. That's a ripper. I
think Tex Perkins does some singing in that one as well. He growls in this one.
He says you better get a lawyer son. You better get a real guy on one to get out
of this one. Let's see how we go. I tell you what, you're remiss not to include
an Aussie in a famous Aussie. And I think this popped up on your original list.
We won't play all of it but let's just give a quick shout out to the great John
Farnham and this classic number. I tell you what, he sounds so young. It's a
very different sounding voice there, isn't it? Again, we were talking about this
months ago, the evolution of an artist who went from there to when they become
like this icon. And that one there is, yeah, I have to tip my hats to the
cleaners out there. I used to clean Suncorp Stadium and the Moon Bar back in the
early 2000s and the disgusting nature of the toilets and the clubs. It's, yeah,
I've been there and I tip the hats to the cleaners. So if you're cleaning out
there, you are doing us a good service. So thank you. Yeah, absolutely
beautifully said. And there's a good reason to mention that. The next song that
I'd like to introduce tonight is along those lines as well. I'm going to 1983
Donna Summer. Now this is She Works Hard For The Money, an out and out classic.
It's from the same album of the same title, I should say. Three weeks at number
one on the R&B Billboard charts in the US. It got to number three on the Hot
100. Now it's about a hardworking blue collar lady and Donna Summer, she was
inspired. She went out one night in 1983 after the Grammys and she went to an
after party and encountered a restroom attendant by the name of Onetta Johnson
who was so exhausted from working such long hours that she was fast asleep in
the corner of the bathroom. And Donna Summer said it, I looked at her, my heart
filled up with compassion. I thought to myself, gosh, she works hard for the
money, copped up in this stinky little room all night. And then I thought about
it and I thought, she works hard for the money. She works hard for the money.
This is it. This is it. I know this is it. And on it goes and becomes a song in
tribute to hardworking individuals out there, as you beautifully illustrated.
And an occupation like that can be ignored. But I think I've seen people
actually rallying for the people in that occupation. Like when you go to an
opening or whatever, you've got the bar people out there too serving, you don't
have to be an idiot to be, because you're in that status, you just politely and
be nice to workers, retailers especially as well. Especially in difficult times
when everyone is a little bit afraid, a little bit stressed. And look, I know
it's done the time in hospitality, it can be a rather thankless line of work as
well. A little bit of kindness goes a long way. So let's give a shout out right
now to the great Donna Summer with 1983's She Works Hard for the Money on ABC
Radio Brisbane and Queensland. She works hard for the money, so hard for it
honey, she works hard for the money so you better treat her right. A little
there in the corner stands and she wonders where she is. And a praise to her,
some people seem to have every claim. From 9am on the hour hand, it's a waiting
for the bell. And she's looking real pretty, she's waiting for the clientele.
She works hard for the money, so hard for it honey, she works hard for the money
so you better treat her right. 28 years have come and gone and she's seen a lot
of tears. Of the ones who come in, they really seem to need her there. It's the
sacrifice working day to day. A little money just exists for pain but it's worth
it all to hear them say that they care. She works hard for the money, so hard
for it honey, she works hard for the money so you better treat her right.
Already knows she's seen a bad time. Already knows this is a good time. She
never sell out, she never will, not for the love in her heart. She works hard
for the money, so hard for it honey, she works hard for the money so you better
treat her right. Donna Summer there, 1983. She works hard for the money on ABC
Radio, Brisbane and Queensland. My name is Joel Spraburra, Quentin Ellison from
4 Triple Z. And many other lines of work is here as well. We're celebrating
work, we're celebrating jobs, employment and I'm having a great time. I'm having
a great time working here for nothing. I'm working hard for my nothing here. You
are working hard for your nothing. I'm going to look Fridge Magnan on offer,
quiz time coming up, just saying. It's lovely. Now Susie from the Sunshine Coast
was on the phone a very good evening to you Susie, having a chat to Lizzy Orley
who's producing tonight and Susie said, why don't you play a bit of opera?
There's lots of opera that could work and Lizzy said, I don't know, what do you
reckon? And she said, I don't know, you think of something. And I want to give a
shout out to the wonderful Lizzy because Lizzy absolutely did go and think of
something. And this is for you Susie, a little excerpt from The Barber of
Seville. Now forgive me if I'm mispronouncing but I believe Giovanni Pesillo
behind that one. Here's a little taster just for you Susie. There you go. Just a
taster, just a taster. The Barber of Seville bit of opera as well, Quentin
Ellison. Good choice Susie. I really got into that. Thank you Susie. One of
these days I will turn your mic on because your singing, sir, is extraordinary,
if I may. I was really miming it actually. He wasn't singing out loud that time.
When he does though it's very good. Let's go back to you. Now the Moody Blues
Quentin, why have they made the list? Well, just the singer and the rock and
roll band, what else? I mean you can say guitar man. What other songs out there
to depict? I mean we did The Load before but we can just talk about a person who
plays a particular instrument in the band or someone who sings and this one is
just a rock and a roller, this one from start to the end. It's one of those,
where else is it going? It just travels along. Go to woe. Go to woe. It goes to
twelve. Not one of those special events, it goes up to eleven. That's right. It
goes all the way to twelve. Well new technology, it cranks it up to twelve. It
goes to twelve. Let's go to twelve and a half. On OVC Radio, Brisbane and
Queensland, happy hour Thursday. The Moody Blues, just a singer in a rock and
roll band. Mr. One Ring on the bass and the guitar, beating so many people who
are trying to be free. And while I'm driving I hear so many words, the technique
barrier is broken, now we're bound to key. And if you want the wind to change,
you know about you, and you're the only other person to know, don't tell me. I'm
just a singer in a rock and roll band. A thousand pictures have been drawn from
one word, only who is the artist, we gotta agree. A thousand miles can be so
many ways, just to know who is driving, what a help it would be. So if you want
this world of yours to turn about you, and you can see exactly what to do,
please tell me. I'm just a singer in a rock and roll band. How can we understand
bias by the people, for the people who are wanting to be smart and selfless? Now
when you see a private person who is blinded by the people who are crossing this
earth. Crossing this earth. I'm just a wanderer on the face of this earth,
teaching so many people who are trying to be free. And while I'm driving I hear
so many words, the technique barrier is broken, now we're bound to key. And if
you want the wind to change, you know about you, and you're the only other
person to know, Don't tell me. I'm just a singer in a rock and roll band. How
can we understand bias by the people, for the people who are only destroying
themselves? Now when you see a private person who is blinded by the people who
are crossing this earth. Crossing this earth. I'm just a traveler crossing the
world, teaching so many people who are bridging the seas. I'm just a singer in a
rock and roll band. I'm just a traveler crossing the world. Keep the applause
going. Moody blues. I'm just a singer in a rock and roll band. Quentin Ellison,
you catch your breath. Wow. Can you imagine us in the 70s listening to that?
We've just got so far out, man. The drum flourished towards the end there. I was
going to sort of bring it down a little bit earlier, but I thought, no, no, no,
I can't. You just couldn't do it. That's a sin. It's addictive, that song. Yeah.
Yeah. Go back and listen to it later, everyone. Yeah. Good, good choice to see
our ABC Radio Brisbane in Queensland. Happy Hour continues, another one of your
suggestions right after this. The latest issue of ABC Organic Gardener magazine
is filled with practical solutions for a greener life. You'll learn all about
veggies you can plant now, from crunchy kohlrabi to easy to grow Asian greens,
as well as tasty apples, perfect for cooking. You'll also discover low-tox
options for a healthy kitchen and key steps for chook care in the cool season.
ABC Organic Gardener magazine, available from newsagents and
abcmagazines.com.au. This is ABC Radio Brisbane and Queensland with Joel
Spreadburer. It sure is having a lot of fun doing job titles, work themes
tonight. Quentin Ellison is here as well, an inspired song list so far. And
thank you for your input from home, Graham from the Sunshine Coast. A song has
come to mind for Graham. Everybody Wants to Work by the Uncanny X-Men, Quentin.
You've immediately said, let's get it here. Yeah, I remember this song. I think
it might have been 1984 from memory serves. The film clip, I think Brian Mannix
was on Countdown. He was on top of all these beams or something like that.
That's my earliest memory. Yeah, wow. Yeah, it was pretty much a big hit back in
the like 84, 85 or something like that. OK, I'll have a look at that and let's
get it in there. ABC Radio Brisbane and Queensland. And my thanks to Graham for
the suggestion. Everybody Wants to Work, Uncanny X-Men. I could drive a taxi, I
could drive a horse. Tell the truth things could be worse. When I get too young,
life's too good to work. I'll tell you something, I'm learning. Everybody Wants
to Work. No, no, not me. Everybody Wants to Work. No, no, not me. Everybody
Wants to Work. No, no, not me. Oh, not me. Oh, I can sleep till lunchtime,
nothing else to do. Perhaps I'm better off than you. Cause I'm not getting
older, life's too good to work. I'll tell you something disturbing. Everybody
Wants to Work. Oh no, not me. Everybody Wants to Work. No, no, not me. Everybody
Wants to Work. No, no, not me. Oh, no, not me. Dr. on the Earth who baked the
food Dr. on the Earth who baked the food And everybody wants to work Na na na me
Everybody's up to work Oh no, not me Everybody has to work Na na na me Oh not me
Everybody goes to work Na na na me Everybody lives to work Na na na me
Everybody's up to work Na na na me Oh not me Everybody's up to work Everybody's
up to work Very true, very true words from Uncanny X-Men. Everybody wants to
work, everybody's got to work. I think the latter is more the case, Quentin
Ellison, I'm sure you'd agree. Well yours is the no no not me. Pretty much.
Yeah. ABC Radio, Brisbane and Queensland Thursday, happy hour. Now a couple of
texts, Angie's texted in, said thanks for the shout out to Opera before and
thanks again to Suzy for bringing that into the conversation. Francis as well
has said I'm surprised that Dolly Parton's 9 to 5 hasn't been suggested yet. In
our work these days it's more like 9 to 9. Francis still working away while
listening tonight, lovely to have you along Francis. And you know what, just for
you and for you too Angie, here's a little bit of Dolly. Working 9 to 5 What a
way to make a living, where they getting by It's all taken and no given, they
just use your mind And they never give you credit, it's enough to drive you
crazy and you levy 9 to 5 For service and devotion, you would think that I would
deserve a fair promotion Want to move ahead But the boss won't seem to let me in
I swear sometimes that man is out to get me The great Dolly Parton, the one and
only Dolly. Great earworm for the night when you're riding home. It sure is and
that goes out to you Francis, you Angie and everyone else, everyone that's a
Dolly fan and everyone that's working hard, 9 to 5, 9 to 9, whatever the hours
may be, we all do it we all make a crust to get by in this world so thank you
very much. Now Quentin, it's been an absolute pleasure, thank you. Thank you,
it's good to be back here again on the ABC. The singing, the dancing, all of it,
it's been an absolute hoot so thanks for giving up your time and bringing your
expertise because it's vast, I really appreciate it. I hope everyone enjoyed it
too and yeah, get those tunes in for the next happy hour. I think we might even
put the theme out a bit early and I think we're onto something here because you
bring, really really bring it. Now I've got one more to squeeze in, 1961, Please
Mr Postman, the debut single by the Marvellettes. Now this is the first Motown
song to reach the number one position on the Billboard Hot 100 and it became a
number one hit in 1975 when the Carpenters covered it. Earlier than that it was
covered by none other than the Beatles in 1963 on ABC Radio Brisbane in
Queensland. To take you to the news, thanks again Quentin Ellison for your
company and for everyone else out there, thank you for being part of it. Here's
the Marvellettes, Please Mr Postman. Thank you. Thank you.