TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-27-13H00M.MP3
--------------------------------------------------------------------------------
 I've never been a man of faith, but I feel heaven came up for much now. How
much better can it get than when you're holding the love of your life? Cos baby,
it's just you and I. Isn't it gorgeous? Kyle Linehart featuring Morton. And
babe, it's just you and I on ABC Radio Brisbane and Queensland. And we'll talk
about Australia's shipwrecks after 9 o'clock, and then songs you love and
dedications. The ABC Listen Up makes listening to your local ABC radio stations
wherever you are as easy as, well, ABC. Tap our logo to stream us live and free.
Good evening. Rachel Hater with ABC News. The federal government has been forced
to undergo further consultation with the aged care sector on its pledge to have
nurses in every residential aged care facility 24 hours a day, seven days a
week. The sector has warned it's already facing severe workforce shortages, with
some aged care homes granted extensions beyond the July cut-off date. The
government says it's looking further at the design of the program. Prime
Minister Anthony Albanese says the government wants practical outcomes for older
Australians. Overwhelmingly, aged care homes are on track to meet that 24-7
target. Well known, weeks ago, a number of homes that have applied to have that
extended have had that extended. The Municipal Association of Victoria says
disruption from extremist groups is the biggest challenge local government has
ever faced. Police were called to a Monash Council meeting last night after it
was disrupted by more than 100 people protesting an upcoming drag storytime
event. Bullying behaviour has forced the closure of the public gallery at Yarra
Rangers Council, while Whittlesea Council has hired security. Association
President David Clark says moving meetings online is the best way forward.
Online is not perfect, but certainly it still allows people to certainly view
council making its decisions in a very public and transparent way. The biggest
step for us to look at things like security and partitioning and those types of
things, I'd be really, really reluctant to do that as the first step. An inquiry
into the convictions of Kathleen Folbig for killing her four children in the New
South Wales Hunter has heard she's been treated like Lindy Chamberlain, court
reporter Jemele Wells. A second judicial inquiry into Kathleen Folbig's
convictions has heard a rare gene variant might explain the deaths of her two
daughters and other medical issues have been suggested as the cause of the
deaths of her two sons. The lawyer for the DPP said the new genetic evidence and
new expert evidence about Kathleen Folbig's diaries changes the way the case can
be viewed. Kathleen Folbig's lawyer said she's been wrongly convicted like Lindy
Chamberlain who was released years after new evidence was found. The inquiry
heard there was significant public agitation after the new evidence in the Lindy
Chamberlain case came to light. A former coalition defence minister says large
salaries paid to former US military officers to work in Australia is no big
deal, arguing top lawyers often charge higher rates. Pentagon documents reveal
dozens of retired American officials have worked for Australia in the past
decade, including an Admiral who charges $5,000 US dollars a day when working
for defence. Former Defence Minister Christopher Pyne says the cost is justified
if it improves Australia's military. I thought, so what? I mean, honestly,
$5,000 a day, US, is less than almost any barrister operating in the Sydney bar,
and I think it's a great big beat up. The public is being urged not to become
COVID-complacent heading into winter as a new sub-variant of the Omicron virus
becomes more prevalent in New South Wales. The Arcturus strain was first
detected in the state in February and is now responsible for almost one third of
new cases there. Professor Michael Tull from the Burnett Institute says it's no
more deadly than previous variants, but better at staying in and infecting our
bodies. Any sub-variant that does take on and increases means it's better able
to avoid the immune response of the body from either a vaccine or a previous
infection. So it just means it's just getting more and more efficient. And
comedian Cal Wilson says having her portrait nominated for an Archibald is
exciting, but winning is a complete surprise. Artist Andrea Hewlin has won the
Archibald Packing Room Prize for her oil painting. Cal Wilson says she can't
believe she's gotten away with winning. I just love the fact that so much time
and talent and effort has gone into immortalising something as ridiculous as
this. I put in a lot of hard work, so I'm just thrilled that that's been
recognised, that I did such good sitting and staying still, so it's great. It's
such a thrill. I have not stopped beaming. ABC. You're with Kelly Higgins-DeVine
on ABC Radio Brisbane and Queensland. You sure are. Good to have you along
tonight. Australia's waters have a lot of shipwrecks in them, around them. You
know what I mean. The earliest dates back to 1622, so we're going to hear more
about them from a marine archaeologist who will tell you about the shipwrecks
that we have around this great big country of ours. A little bit later as well,
songs you love and dedication. So if you want to give Joel the super producer a
call now to put in your song request, you should do that 1300222612, 1300222612,
to tell us which song you'd like and give Joel the chance to find it maybe if
it's not one that we would normally play. We do try our best to track them. When
I say our best, Joel does his best to try and track them down for you. I just
sit here and listen to the music, to be quite honest. So Morse Code Day, by the
way. April 27 honours the inventor of the Morse Code, Samuel Morse, who was born
on this day in 1791. It also celebrates the pioneering method of communication
and the invention that was first used to transmit encoded messages, the electric
telegraph. And Morse Code is still used by US Navy intelligence specialists,
journalist, amateur radio operator, aficionados, who formed the International
Morse Code Preservation Society, and aviators who communicate abbreviated
identifiers via Morse Code. And Sid the Yachty saying Morse Code got him into
trouble once. A kid with very bad acne said to me, read my lips. So I did. Then
I gave him a clip behind the ear. I thought he was swearing at me. OK. I'm not
quite sure what the acne has to do with that. But anyway, just a bit of detail
there from Sid. You're on ABC Radio Brisbane in Queensland. Give me a call. Tell
me what songs you'd like to hear tonight for songs you love and dedications. And
while you're doing that, Steve Winwood is going to do this. ABC Radio Brisbane
in Queensland. Are you still free? Can you be? Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. Oh. Oh. Oh. Oh. Oh. Oh. Oh. Oh. Oh. Oh. Oh. Oh. Oh. Oh. There
you go. While you see a chance. Steve Winwood on ABC Radio Brisbane and
Queensland. And the song I played just before the news, Juliet Grace Meir.
Interested in the title? You might be as well. I was on a date with Martin and
it was called, Babe, It's Just You and I. You're listening to ABC Radio Brisbane
and Queensland. Australia's waters are littered with shipwrecks. The earliest
dates back to 1622. But what do we know about what happened to the survivors of
these wrecks? And how do archaeologists piece together the story behind them?
Graham Henderson is a marine archaeologist. His book is called, Swallowed by the
Sea, the Story of Australia's Shipwrecks. So I asked him how many shipwrecks we
have around Australia. Yeah, they're in the thousands. I mean, it depends on
what you call a shipwreck. Is a boat a shipwreck? A boat is, that would be a
boatwreck, I guess. But if it's a registered vessel, then it tends to be classed
as a shipwreck. Now let's start with a ship called the Batavia, which was
wrecked in 1629. Now, of course, before the arrival of the First Fleet, what was
this ship doing in Australian waters? Well, it shouldn't have been in Australian
waters. It was on its way from the Netherlands via Cape Town to Batavia. That's
current day Jakarta. And it should have turned north a bit before it did. And so
it sailed too far west and hit Western Australia. Part of the reason for it
sailing too far east, sorry, not west, part of the reason for that was because
there was a bit of argument going on on board. So it hit the Abrolhos Islands in
1629 and all hell broke loose. So once you know you've hit the Abrolhos Islands,
you've gone too far. Yeah, yeah. The Abrolhos Islands are 40 mile off the coast
of Western Australia, so close enough for them to be in the wrong waters. Now,
there were 322 men, women and children on board and more than 220 survived. And
I understand there's quite a story about what happened between the survivors.
Take us through that. Well, the people on board included the commodore of the
fleet, essentially, the man in charge of all the vessels, Francisco Pelsart, and
the skipper, Adrian Jacobes, who was in charge of the navigation. But there was
another chap who was in charge of the commercial side. And the Dutch at that
time were very, they placed commerce at a very high priority. So Geronimus
Cornelius was the businessman in charge. When the shipwreck took place, Pelsart
and Jacobes took off to get help from Batavia. And they left Geronimus Cornelius
in charge, and he did the wrong thing in a big way. So this was a group of
people who were at the mercy of this man. Is that right? Well, there were 220 of
them on the island. During a period of time, he decided that he wanted to kill
those who were not necessary for his intended venture in piracy in the East
Indies. And so he killed by torture and whatnot 125 of them. And he would have
killed more, but he had marooned a group of soldiers under the command of a chap
called Webby Hayes on another island. And he thought that that would get rid of
them as a problem. But when a rescue ship came back, Webby Hayes got to the
captain of that ship, and warned them about the mutineers. So the mutineers were
rounded up and duly given their punishment. Graham, how was the wreck
discovered? The wreck was found after a number of people had been searching for
it for some time. And the record seemed to indicate for a while that it was the
southern Abrolhos. But then some writers got onto the job of establishing that
it really indicated the central group of the Abrolhos Islands. So people
searched in the right area. A crayfishman over there had actually seen anchors
in the water, and he took a group of divers out to that spot, and there it was.
Was it under a lot of silt? Oh, not silt, but lovely coral. So when in the 1970s
the West Australian Museum started excavation of the wreck, the curious thing
was that there were a lot of big stone blocks on top of the rest of the
wreckage. We raised these blocks. They turned out to be a gateway that had been,
or an intended gateway, for a replacement for one at the Fort of Batavia.
Removing those blocks, what we discovered was that underneath the blocks were
preserved a good part of the hull of the ship. They wouldn't have had the chance
in hell of surviving in that warm coral waters if it hadn't have been for that
cargo of stone blocks being on top of them. But when we removed those, there was
this wonderful hull remains underneath. Now another ship with a pretty
remarkable story behind it was the HMS Pandora. Where was it wrecked? Well, it
was wrecked off Queensland on the Great Barrier Reef. It was a part of the story
of the Mutiny on the Bounty, because in 1789 Fletcher Christian had challenged
William Bly, tossed him into a little boat, and William Bly, I guess, was
expected to die on the way home. But he sailed this little boat 6,700 kilometres
across to Timor and eventually got back to England. At that point, the British
government, being rather horrified about Mutiny having taken place, sent Captain
Edward Edwards on the Pandora to find the mutineers. He didn't find Fletcher
Christian and a number of the others, because they'd gone to Pitcairn Island, a
very remote place. But he did capture 14 of them, put them in a tiny little
cell, which was generally referred to as Pandora's Box. The Pandora started to
head back to Britain, but it was wrecked on the Great Barrier Reef. Just four of
the mutineers drowned, together with 31 of the crew. But generally, most of the
stories have been about the four mutineers who drowned. But then they got back
to England after having done a 2,000 kilometre journey to Timor from there. You
were involved in the initial inspection of the wreck Graham Henderson. Can you
describe what it's like to uncover a wreck for the first time? Well, a chap
called Steve Dom and another one called Ben Crop, the well-known Ben Crop, had
found it in 1977. And at that point there was legislation covering shipwrecks.
So the federal government asked the Museum in Western Australia to provide
someone to do an inspection. I was the lucky one who got to do an inspection in
1979. Oh, it was a splendid experience to hop into the water on this magic site,
a bit over 30 metres in depth. Brilliant clarity. You could see the shipwreck on
the seabed from the surface. It was a lovely wreck, in very good condition. And
I had the task of giving a clear identification of the wreck. I found things
like the number 24 on one of the rudder fittings, and it was a 24-gun ship. So
that rating was marked on the vessel. There was also the name of the metal
founder who put his name on the fittings as well. So we had a very clear
identification of the ship, not just because of the position and the size of the
wreck, but with actual words, which are always a very difficult thing to get
with inspections. Now, you tell the story of a woman called Eliza Fraser, who
was one of the several survivors of the wreck of the Stirling Castle in 1836.
What happened to her after the wreck? Well, she and her husband, James Fraser,
and some of the crew set out for Fraser Island, of course. They got there and
met up with the Aborigines on the island and lived with the Aborigines for a
period of time. Her husband, James Fraser, he died. Then finally people got to
Sydney, and she was taken back to Sydney, and then went back to England and said
a lot about what had happened at that point. She, I think, exaggerated quite
considerably some of the travails that they'd gone through and talked about the
Aborigines in ways that were, I think, not at all pleasant at all. But that
exaggeration meant that the Aborigines were considered to be nasty people,
whereas they had in fact helped the survivors from the ship. It's Graeme
Henderson, marine archaeologist, author of the book Swallowed by the Sea, the
story of Australia's shipwrecks. Mr Dulac, how long have you been dead? Two
immortal classics from Anne Rice. I offer my life story. The newsreaders Sam
Reed and Jacob Anderson from Game of Thrones in The Interview with the Vampire.
I can swap this life of shame, swap it out for a dark gift. And Alexandra
Dodario from White Lotus in Mayfair Witches. You don't know what I'm capable of.
Both premiere Friday May 5, streaming all episodes on ABC iview. You're with
Kelly Higgins Divine on ABC Radio Brisbane and Queensland. Indeed. Look, we're
going to kick off Songs You Love and Dedications a little bit early tonight.
1300 222 612 if you'd like to hear a particular song. This is one I didn't get
to when we were doing Happy Hour a bit early and I thought, you know, throw out
some songs if you want to hear them. This one I think came from Marie who wanted
to hear a bit of Trans Vision Vamp and Sister Moon. 1300 222 612. Oh, I've been
missing you. Now, if you take my heart, you can take my hand. I'll be with you.
Now, sister, do you remember all of those days? All those crazy nights seem less
than a haze. But every new moment, that's a brand new start. When you've got the
nature down deep in your heart. And as the mountains move on the winds of
change, there's no fear of the end. When you've got the world running in your
veins. Groove on, sister, groove on, Sister Moon. Groove on, my baby, groove on,
groove on, Sister Moon. Now you've got the skies and you've got the stars and
you've got the power to break the young heart. Yeah, you've got the power and
you've got the life and you've got the love to make it all right. Hey, Sister
Moon, are you coming round soon? Hey, hey, and Sister Moon, in your midnight
blue. Groove on, sister, groove on, Sister Moon. Groove on, my baby, groove on,
groove on, Sister Moon. When you've got the stars shining down on you. Groove
on, sister, groove on, Sister Moon. Groove on, my baby, groove on, groove on,
Sister Moon. What you, what you want, what you want, Sister Moon. Trans-vision
vamp. Kicking off songs you love and dedications tonight, 1300222612. You're
listening to ABC Radio Brisbane and Queensland. One of my favourite people, Gil
from Imbil. Hello. Hello, Gil. How you doing? Mighty fine, thanks. I'm excited.
Now you've got the big Imbil rodeo happening, the Mariba rodeo. Yeah, well,
we've got a little bit far from Mariba, but we've got round two of the Imbil
Buckle series coming up on Saturday night. Excellent. Are you out there getting
on a Bronco or some such thing? No, I'd rather eat them than ride them. Fair
enough. I'm with you. I'm with you. So is it a big weekend? Are you expecting
lots of people to flood into town? Well, yeah, they've put a cap on about 4,000
people. OK. And I'm just a newbie to the area, so I'm just trying to help out a
bit. Good stuff. The PNC at the Mary Valley State College puts this on, so they
put on three a year. Yep. And this one's round two and it's part of the Buckle
series, so everybody, all the points count towards the NASDAQ rodeo titles.
Fantastic. So a bit of a family event, is it? There'll be rides for the kids and
all that sort of stuff as well? Oh, yes. Oh, yes. We've got free rides for the
kiddies in the kids zone. Yep. They've even got one of those walls that's climb
up too. Ah, very good. They're lots of fun, the climbing walls. Yeah, I hope the
ambulance is on. I hope the ambulance is on there. It's like those, you know,
the big blow up slides that they have at some of those shows, and I've watched
kids, like little kids stand at the top and they've just got that, and they
don't sit down and you just go, oh, no, and tumble over they go. It's like, oh,
crikey. That's it. The parents are in terror. The kids are like, no, this is
great fun. Exactly. So when does it start, Gil? Three o'clock on Saturday, it
kicks off, and they've got markets, free rides, kids zones, food stalls. A
little bit of everything going. Yeah. Oh, and don't forget the band Junction
Road. There's a local band. They'll be playing there. All right. After the rodeo
and after the fireworks and look out, anybody who lives close to the high school
there. All right. That sounds like a lot of fun. Glad we could give it a
mention. Now, I think we need to play some Slim Dusty, don't we? Oh, that'd be
nice, yeah. But when he says Mariva, just insert Imble there, right? Imble
instead. Good stuff. Thank you. Thanks, Gil. In the north of Australia where
they hold a Bushman's Gala, everyone will go. The town's really humming for the
champions are coming to the big Mariva rodeo. When the sheriff gives his ruling
and the big parade is moving, it's gonna be a mighty show. The horns are tooting
and the boys are rooting, tooting for the big Mariva rodeo. They'll be coming
from the south, yes, coming from the west. Riders from the north are ready for
the test. There'll be some fun and battle with the horses and the cattle at the
big Mariva rodeo. There's a spill and a buster, it's a fair dink of mustard,
deep north in Queensland skies. Dust clouds are flying for the boys are really
trying for the prizes, good and high. A rider takes a landing but the pickup men
are handy, they keep things on the go. Cute gates fly open for a record time,
they're hoping at the big Mariva rodeo. They'll be coming from the south, yes,
coming from the west. Riders from the north are ready for the test. There'll be
some fun and battle with the horses and the cattle at the big Mariva rodeo. When
the rodeo is over and the champions in clover, trophies all on show. Prizes are
given with sashes and ribbons for the big Mariva rodeo. There's one thing I'm
knowing that next year I'm going to take back my travel and show. Old friends
are meeting and there'll be a hearty greeting at the big Mariva rodeo. They'll
be coming from the south, yes, coming from the west. Riders from the north are
ready for the test. There'll be some fun and battle with the horses and the
cattle at the big Mariva rodeo. At the big Mariva rodeo. Ah, you've got to love
it, Slim Dusty, Mariva's rodeo. Get along to the Imble rodeo if you're anywhere
near Imble this weekend. You're with Kelly Higgins-Divine on ABC Radio Brisbane
and Queensland. Songs you love and dedications tonight, 1300 get 222612, give me
a call. Or you can text with whatever song you'd like to hear. Just tell me your
name and where you're from and why you want to hear it. 0467 922612. Let's head
to Brisbane and Andrew. Hello. Hello, Kelly. How are you tonight? Good. How's
your week been? It's been pretty busy, actually. We're getting into the semester
at university. Yes, that's right. Near the end of semester. That's when the
students all start to panic and it's like, I don't know, what am I supposed to
be doing? It's like, did you read the rubric? Did you read what you're supposed
to do? No? It's like, step one, bossum. We're getting it done. Step one. We're
nearly halfway through our 5,000-word submission. That's right. It's due in two
days, is it? Excellent work. 20, 21 days, I think it is. That's OK then, that's
forever. We've got the entire weekend coming up. I know, but I was going to go
to the rugby on Saturday afternoon, go watch the hospital cup. You can still do
both. I think you could still do both. What's the essay on? Parkinson's Disease.
Oh, OK. It's Parkinson's Awareness Month, actually. It is Parkinson's Awareness
Month. What are you writing on? Can I ask? Just the effectiveness of exercise.
Oh, and what does it say? Are you up to that beat yet? We are doing our risk of
bias assessment now, grade assessment at the moment. It looks good. It looks
like it's a... The great thing about exercise is anyone can do it kind of
anywhere. Yep. You don't need to come in for... Something like transcranial
stimulation, electromagnetic stimulation, you need to go into a particular
facility for that. And medications, you need to go to the pharmacist and get
them filled. But exercise is one of these things that you can just do. Yes, you
can. Motion is the lotion. Motion is the lotion. Oh, very nice. Now, Andrew,
you've asked for a great song tonight. What do you want to hear and who would
you like to hear it? Who would I like to hear it for? Yeah, that's it. I didn't
put that very well, but yes, thank you. California Girls by the Beach Boys.
Excellent. Who's it going out to? It's going out to Kendall, who is on her way
back to California as we speak. I got to see her last night for the first time
in a couple of years thanks to border closures and flight restrictions and the
fact that they actually live in California. Yep. But, yeah, Wednesday nights
were always fun when Kendall was there. I mean, they still continue to be fun.
Yep. But she was definitely missed for the last few years, and it was really
nice to see her again last night. So it's going out to Kendall from Andy.
Thanks, Andrew. Thanks, Kelly. Have a great night. Well, East Coast girls are
hit by a really dick-closed styles they wear and the Southern girls with the way
they talk, they knock me out when I'm down there. The Midwest farmers' daughters
will even make you feel all right and the Northern girls with the way they kiss,
they keep their boyfriends warm at night. I wish they all could be Californians.
I wish they all could be Californians. I wish they all could be Californians,
yeah. The West Coast has the sunshine and the girls all get so tan. I dig a
fridge bikini on the way and I'll cross by a warm drink in the sand. I've been
all around this great big world and I've seen all kind of girls. Yeah, but I
couldn't wait to get back in the States, back to the cutest girls in the world.
I wish they all could be Californians. I wish they all could be Californians. I
wish they all could be Californians. I wish they all could be Californians. I
wish they all could be Californians. I wish they all could be Californians.
There we go. Andrew from Brisbane wanting to hear that one for Kendall tonight.
Here's mate who's just flown back to California. This is ABC Radio Brisbane and
Queensland. More songs you love and dedications in a moment, 1300 222 612. If
you want to hear a particular song tonight you can text me 0467 922 612. ABC
Listen. Rampaging Roy Slaven and HG Nelson. I note that some people don't use
log books anymore. What? They don't know cos, sine, cosine, cotan. They're all
great. Lutching on the blind side. Why don't they do it? I don't know. I think
Royal Commission. What happened to the log book? I loved it. I opened it up.
Could we get the Landis to drag the log book? We'll be back in a while. You're
going to be on the news. Podcast available now on the ABC Listen app. You're
with Kelly Higgins-Divine on ABC Radio Brisbane and Queensland. A little bit
earlier we were talking about shipwrecks and mentioned the Pandora. Wendy texted
in, thanks Wendy, to say apparently King Charles III took interest in the
Pandora. The coronation happening on Sunday, are you going to watch it? I'm kind
of a fence sitter when it comes to the Royals. I like to watch on, but if we
decided not to have them, it wouldn't make me sad, it wouldn't make me happy to
just be a thing that happened. But I think I'll watch just because the last time
it happened I wasn't alive. And well, depending how long Charles lasts, I may
not be alive for the next one, so may as well enjoy it while I can. But
apparently King Charles III took interest in the shipwreck of the Pandora. Thank
you Wendy for sending that in. 0467 922 612 to give me bits of fabulous trivia
like that, thank you. Or to request a song tonight, this one came in from Karen
who asked to hear this to remind Sharon that she's always there for her. Okay.
Sometimes in our lives we all have pain, we all have sorrow But if we are wise
we know that there's always tomorrow So lean on me when you're not strong And
I'll be your friend, I'll help you carry on For it won't be long till I'm gonna
need somebody to lean on Please swallow your pride if I have pain You need to
borrow for no one can feel The love you need that you won't let show You just
call on me brother when you need a hand We all need somebody to lean on I just
may have a problem that you'll understand We all need somebody to lean on Lean
on me when you're not strong And I'll be your friend, I'll help you carry on For
it won't be long till I'm gonna need somebody to lean on You just call on me
brother when you need a hand We all need somebody to lean on I just may have a
problem that you'll understand We all need somebody to lean on If there is a
load you have to bear that you can't carry I'm right up the road, I'll share
your load If you just call me, call me, if you need a friend Call me, call me,
if you ever need a friend Call me, call me, call me, call me Bill Withers and
Lean On Me on ABC Radio, Brisbane and Queensland with Kelly Higgins-Divine for
songs you love and dedications tonight. Lauren at Dachshund has asked for one
from the Rolling Stones tonight. She says it's on the Beggar's Banquet 50th
anniversary album going out to her brother Mark, Factory Girl. Take me to the
station and put me on a train I've got no expectations to pass through here
again Once I was a rich man, now I am so poor But never in my sweet short life
have I felt like this before Your heart is like a diamond, you throw your pearls
at swine And as I watch you leaving me, you pack my peace of mind Our love was
like the water, the flashes on a stone Our love is like our music, it's here and
then it's gone So take me to the airport and put me on a plane I've got no
expectations to pass through here again Music Lauren's just sent a message
saying that was nice, it's not Factory Girl but I still enjoyed it. We have no
idea then what Rolling Stones song that was. Sorry about that. But I hope you
enjoyed it anyhow. And the Bill Withers song. I've got a text on that one too.
Thank you James at Bribey. He says I first heard that song when I was walking
home from a school social in 1968. My mate had a transistor radio and I'll never
forget Bill Withers, his song and that moment. I thought it was the best song
ever. You're listening to ABC Radio Brisbane and Queensland. You certainly are,
you're with Kelly Higgins-Divine. Another request tonight. This one coming in
from Linda who says could you please play Baby When You're Gone by Brian Adams
for my husband who's no longer with us. The radio is coming Queensland. Music
Music Music Music Music Music Music Music Music Music Music Music Songs you love
and dedications tonight. Brian Adams and I think that was Sporty Spice. Baby
When You're Gone and for her husband who's no longer with us. You're with Kelly
Higgins-Divine this evening and thank you to Jill from Gimpy who cleared up the
song we did play for the Rolling Stones. No expectations apparently and
obviously no expectations of us getting it right tonight would be best. There's
only eight minutes to go, seven minutes to go so we're nearly there. I want to
play this one now going out to Judy from Shane, a bit of Doctor Hook. ABC Radio
Brisbane in Queensland. Music Music Music Music Music Music Music Music Music
Music Music Music Music Music Music Music Music Doctor Hook and years from now
on ABC Radio Brisbane and Queensland. Look I'm going to throw in a Captain's
Pick for our last song for the week, one that I hope you'll enjoy. A bit of
Adele to make you feel my love. Music Music Music Music Music Music Music Music
Music Music Music