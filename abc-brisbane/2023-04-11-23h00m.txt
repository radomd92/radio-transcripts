TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-11-23H00M.MP3
--------------------------------------------------------------------------------
 And at home, as I say, 30 games undefeated is a massive, massive result. Full
strength both sides. Sam Kerr leading the way, creating goals, saving them. And
as I said before, Mackenzie Arnold in goal, stopping two penalties and a barrage
of other shots. She was extraordinary. Joel Spredberg, thank you so much for
keeping us on top of the Matildas. Now get back to work. Thanks, Larry. News is
next at seven o'clock. I do get my news sometimes on the radio. There are enough
people out there who are doing good for the whole world. I do follow ABC radio.
ABC radio. Yeah, there's hope. Yeah. In this bulletin, former liberal Ken Wyatt
speaks out against the party's decision on the voice to parliament. Authorities
set to scour garbage dumps in the search for the body of a Brisbane woman. An
airstrike on a village in Myanmar kills more than 50 people. And acclaimed
Australian artist John Olsen dead at the age of 95. ABC News. Good morning. I'm
Matt Eaton. Former Minister for Indigenous Australians Ken Wyatt doesn't believe
the liberals were ever truly considering supporting the indigenous voice to
parliament proposal. Matthew Doran reports from Parliament House. Ken Wyatt tore
up his liberal membership last week after the party decided to formally oppose
the plan to enshrine a First Nations advisory body in the nation's constitution.
Liberal leader Peter Dutton says there are problems with how the constitutional
amendment is worded. But when asked whether he believed any changes would have
won Mr. Dutton over, Ken Wyatt was clear. No, I don't think so. The former
minister also disputed Mr. Dutton's criticism the voice would be made up of
inner city elites. That's far from the truth. Yesterday, Shadow Minister for
Indigenous Australians Julian Leeser quit the liberal front bench over the
party's stance, saying he wanted to campaign in favour of the referendum. A top
Chinese diplomat will today hold talks with Australian officials in Canberra.
Here's Foreign Affairs reporter Stephen Jedgetts. China's Vice Minister Ma
Xiaoxu will meet with senior Australian officials at the Department of Foreign
Affairs and Trade. The talks are another sign that Beijing's intent on
sustaining high level contact with Australia, despite persistent tensions over
the Albanese government's push for nuclear powered submarines, and more
recently, Australia's decision to ban TikTok from government devices. The
discussions are expected to focus heavily on trade disputes, as well as broader
political disagreements between Australia and China. Chinese state media says
the Chinese Navy continued with actual combat training around Taiwan yesterday,
a day after Beijing announced the end of drills. Isadora Bogle has the story.
China began the exercises on the weekend after Taiwan's President Tsai Ing-wen
met with high level US government officials in Los Angeles. Chinese state
television reported that several warships continued to carry out actual combat
training in the waters around Taiwan to test the combat effectiveness of weapons
and equipment. Taiwan's defence ministry said it had spotted nine Chinese ships
and 26 aircraft carrying out combat readiness patrols around the island on
Tuesday. The South World Islands government has denounced the drills. China
claims Taiwan as its own, which Taiwan's government strongly disputes.
Authorities will begin searching two landfill sites for suspected Brisbane
murder victim Leslie Trotter later this week. Rebecca Hyam reports. The 78-year-
old went missing a fortnight ago from her unit at Tawong in Brisbane's Inner
West. Police say her body was placed in a wheelie bin which was collected on the
28th of March. That collection was among more than 20 other loads compressed at
a waste facility before being taken to landfill sites at Rochedale and Swanbank.
Detective Superintendent Andrew Massingham says the search will begin by the end
of this week. The area where we believe Ms Trotter's body is has been
quarantined at each of these dump sites and we need to make a search for that
body. He says the massive scale of the operation is likely to prove challenging.
The first public hearing of a parliamentary inquiry into vaping in Queensland
will be held in Townsville today. More from Rachel Merritt. The inquiry will
consult the community about ways to reduce e-cigarette use, particularly among
children and young people. The committee will examine the effectiveness of anti-
vaping education campaigns in schools and the harmful effects of nicotine use.
This afternoon, the inquiry will hear from the director of the Townsville Public
Health Unit, a vaping retailer and e-cigarette users. A second hearing will be
held on the Gold Coast tomorrow. The final report is set to be released in
August. More than 50 people are believed to have been killed in one of the
deadliest airstrikes by the Myanmar military in the country's ongoing civil war.
Southeast Asia correspondent, Mzoey Ford reports. Communities in the Sagai
region in central Myanmar have been openly resisting the country's military
junta by forming militia groups and running their own schools and medical
clinics. Tuesday's military airstrike was aimed at a new militia group office,
but also struck villages. At least 50 people died, according to reports by
survivors to various media outlets. Myanmar's military rulers have increasingly
used airstrikes to crush dissent since seizing power two years ago. The junta is
promising an election this year, but opponents say they don't believe the vote
will be free or fair. Russia has acted to make the army draft for the war in
Ukraine more difficult to avoid, with the lower house of parliament approving
legislation to move call up notices online. The BBC's Sascha Schlickter has
more. The Kremlin is keen to avoid last year's chaos when young men would often
be picked up in the street or in shopping malls. With the Ukrainian offensive
now imminent, it needs a steady supply of recruits. Under the new law, an
electronic summons will be deemed to have been served as soon as it appears in a
user's personal account in a special government portal. If he doesn't use the
portal, he'll still be deemed to have received the summons after seven days. If
a draftee ignores a summons, he will not be able to leave Russia or make
property deals and will lose his driving licence. A man has been run over and
killed south of Brisbane early this morning. Police say the man was lying on
Holtz Road at Logan Village just before five o'clock this morning when he was
struck by a four-wheel drive. He sustained critical injuries and died at the
scene. The road remains closed. The number of women forced to transfer from
Gladstone to Rockhampton to give birth is expected to halve from today as the
city's maternity unit moves to the next phase of its reopening plan. Rachel
McGee reports. Gladstone's maternity unit has been on bypass since July last
year, with women instead having to travel to Rockhampton to give birth. The
Central Queensland Hospital and Health Service says from today, birthing will be
available at the Gladstone Hospital where there are no risks identified by the
maternity team. It says it's been reintroducing birthing services in phases and
is working to restart low risk births in Gladstone, which will be possible as
soon as a sustainable roster of suitably qualified staff has been recruited.
It's hoped this will be done by June. One of Australia's most successful and
celebrated artists, John Olsen, has died at the age of 95. Vanmire Navida
reports. The oldest winner of the highly prestigious Archibald Prize, John
Olsen, passed away on Tuesday evening. Known for his vivid landscapes, the New
South Welshman won many awards during his long career, including the Wynn and
Salman Prizes. In 1977, he was awarded the OBE for his services to the arts, and
in 2001, he was appointed an officer in the Order of Australia. His paintings
hang in several of Australia's major art galleries, including the National
Gallery in Canberra and the Art Gallery of New South Wales. His work is set to
be projected on the sales of the Sydney Opera House next month during Sydney's
Vivid Festival. To sport now, and in the NRL, Cowboys forward Griffin Neame will
be sidelined for two months after being hospitalised with a throat injury. The
22-year-old suffered a cricoid fracture during the Cowboys-Dolphins game on
Friday night. He was discharged from hospital on Sunday. A club spokesman says
his recovery is expected to take a minimum of eight weeks. England's Test
cricket captain Ben Stokes says they've been asked, or they've asked curators
for flat, fast wickets for this year's Ashes series against Australia. The five-
test series gets underway in Edgbaston in mid-June, with Australia holding the
famous urn since the 2017-18 series. But with Stokes as captain, England have
won 10 of their past 12 matches and will enter the series in winning form.
Stokes has told Sky Sports they want to continue with their aggressive playing
style. Especially the ground staff around England about what type of wickets we
want. And they've been very responsive to us, which has been good. We want fast,
flat wickets, which is something that we want, because we want to go out there
and score quickly and it brings their guys in. If they've got fast wickets to
bowl on, then they'll be happy with that as well. Now a reminder of the top
stories for this morning. Former Minister for Indigenous Australians Ken Wyatt
has spoken after quitting the Liberal Party, saying he doesn't believe the party
room ever truly considered supporting the Indigenous force to Parliament.
Brisbane authorities will begin searching two landfill sites for suspected
Brisbane murder victim Leslie Trotter later this week. And the first public
hearing of a parliamentary inquiry into vaping in Queensland will be held in
Townsville today. Queensland's weather, sunny and dry across most of the state,
partly cloudy about the southern interior near the border, scattered showers and
isolated storms about the northern peninsula and Torres Strait and the odd
shower about the north tropical coast. Cairns and Mackay heading for a top of 32
today, Townsville and Rockhampton 33, Bundaberg 30, Mount Isa 37. In the
southeast, sunny today with light winds strengthening it by lunchtime in
Brisbane. Brisbane Logan and Ipswich a top of 29, the Gold Coast 28, the
Sunshine Coast 29, Toowoomba 24. ABC News. Friendly. It's a great place. Good
weather. You're listening to ABC Radio Brisbane. Brisbane! ABC Radio Brisbane.
Coffee. Much warmer than Melbourne. Much warmer than Melbourne, is it? Well,
it's a bit cool. Can you hear that noise? Something's going on with the blinds
in the studio. I think they're broken. Can you hear it? Oh, I think it stopped.
Great. We pressed a button and it stopped, but I think they're now broken. Thank
you, Joel. Spreadbra. Gee, trouble follows you wherever you go, Joel. Thank you.
It's been 12 months in the job for one of the key figures in the organising of
the Brisbane Olympics and Paralympics 2032, Andrew Liveris AO, in the studio
next on what we should look forward to. And also this half hour, you're going to
be transported into the beautiful world of ballet. Giselle starts this Friday
with Queensland Ballet and wait till you meet the young woman who is playing the
lead role. ABC Radio Brisbane traffic. What's happening, Brad? Let's tell the
truth, Loretta. That wasn't the blinds. That was your teeth chattering. It was.
No, it was a really terrible noise. It was bang, bang. I did hear it flicking
over there. Could you? See, what happens is you press a button to put the blinds
down because the sun shines in and I can't see anything. And so we press this
button, but we can't find the right button. So we press all of them. It's like
being at home with remote control, isn't it? That's right. So I think the blinds
are broken. I think it was my fault. OK. What's happening on the roads, Brad?
Haven't heard a thing about the blinds. Brackenridge, we did have a minor crash
on Depot Road eastbound. That's got cleared very quickly before the Gateway
Motorway is a little bit of delay through there. But I think the sun glare
really probably created that crash. So it's very bright through there. So take
it easy with that sun glare this morning. And Clontar for crash. Elizabeth
Avenue southbound before the bridge. Traffic's back to Duffield Road and also
reports for crash at Rosewood. Carribin Rosewood Road at John Street. Lots have
been on Flash, Bar, Scrub, Hyne Road and Discovery Drive. Busy on the Inch Road,
Centenary Highway this morning, also the Pacific Motorway and the M1. We have
seen a lot of heavy traffic, though. The Brutus Highway from Deception Bay to
Bord Hills after early crash. And a lot of traffic as well on Strathpine Road
this morning from Gympie Road and Jamaica Way through Bord Hills. And Anzac
Avenue has been slow from Kalanga through to Strathpine. I'll have more details
at half past seven on ABC Radio in Brisbane. Thank you, Brad. 15 degrees in
Brisbane at the moment. We are heading for a top of 29, Bayside 13. It's 14 at
Logan. 8 at Ipswich, the Gold Coast 14 and the Sunshine Coast 11 degrees. With
the ABC Listener, you can take the footy with you wherever you go. Off to walk
the dog. Take the footy. Go in camping. Take the footy. Picnic in the park. Take
the footy. ABC Sports expert coverage of the AFL and the NRL. Every goal, try,
mark and tackle, live and commercial free. So whatever you're up to, take the
footy with you on the ABC Listener. Brisbane. I think it's a friendly city. Just
an exciting place to live. Fantastic. This is ABC Radio Brisbane. Well, it is an
exciting place to live and it's getting more exciting by the day. The 2032
Brisbane Olympics and Paralympic Games might sound like a long way off, but for
those in charge of organising it, there's no time to waste. Andrew Liveris AO,
the former chairman and CEO of Dow Chemical Company, has just done his first 12
months as president of the Games Organising Committee and he's in the studio.
Good morning, Andrew. Good morning, Loretta. Gee, does it feel like 12 months?
Blink of an eye. Honestly, if someone told me it was 12 months about two weeks
ago and I said, you've got to be kidding me, it's that long already? But then it
was just so fast. We've done so much in 12 months. Yeah. It's an amazing time
actually for Brisbane. Well, I want to ask you about what you've done. But
firstly, when you go back 12 months ago and you were offered the job, what went
through your mind? Oh, look, I actually almost didn't understand what I was
being offered. It was so out of the blue for me. I was asked earlier to look at
maybe being on the board, but to be president was an honour I wasn't expecting.
So the Premier surprised me, Thomas Bach surprised me. I guess the one that
surprised me the most, I think I've told this story a few times, is my wife. She
said, why are you hesitating? Because she knows how busy I am, but she said,
this is the job you've been waiting for. It's giving back to your country, your
state, your city. I mean, there's a lot of things nice about it. But this is
probably one of the few institutions around in the world that's still revered,
you know, and Australians love our sport. And so we look at those five rings and
look what it did for Melbourne, look what it did for Sydney, and just imagine
what it's going to do for Brisbane. Yeah, well, when you think about organising
Brisbane, do you look at those other cities and what's been done in the past?
Absolutely. In fact, first 12 months and going by in a blink of an eye, that was
one of the first things I did. Three months almost of going to cities around the
world. I'd been there in my previous job as DAO. We were a top sponsor. But now
I was looking at it from a different vantage point. And then I went to LA and
Paris to see what they are doing. And of course, very apropos, Paris, which is
literally just around the corner. We talked a lot. I benchmarked. We looked at
what's the comparators. It's really interesting to come out of all that, saying
every one of those cities had something great, but a lot to learn from. And of
course, our model here in Brisbane, regional games as they are, this is Brisbane
Queensland Games, as the Premier likes to say, and I think she's dead right, I
mean, this is not just the city, it's the region, and it's all of Queensland.
And there's no games that have been given that sort of mandate. We are what they
call the new norms. Yeah, because by the time you get to 2032, we feel like it's
a long way away. But where are you at with planning? Well, so, I articulated
about a year ago, just my first speech almost, was the 3 plus 3 plus 3 plus 1
model. That sounds a bit complicated, but the math adds up to 10. The last year
is all implementation. You can imagine what Paris is going through right now.
But the first three is a luxury that only really LA and Brisbane have been
afforded, which is to plan. And why would you plan? You plan because there's no
way the future is predictable. Look at world events, look at supply chains, look
at building costs, look at labour issues. So you've got to expect the best, but
actually plan for the worst. And what you've really got to do is use the
planning timeframe, which we are doing in a big way, to anticipate all the
things that will happen and actually go into it with a mindset that whatever the
budget has been allocated, both the budget of the organising committee, which is
$4.9 billion, or the budgets associated with infrastructure and venue, those
budgets are probably going to move around on you, and therefore you've got to
plan your way through optimisation, which is a big movement at the IOC. How can
we learn from Tokyo, how can we learn from Beijing? We're bringing all those
work streams into our planning process. So when we go to the second three and
the third three years, third three is big mobilisation, OK, year four before the
Games. Second three is Games delivery, which is really all the planning. You now
start procuring all the services you need, you name it. Federal government is
providing services, state government, of course, and local councils. But if we
have to build a team, we're at 11 people right now, by the time we get to year
six or seven, we will be up at 2,000, 3,000 people. So imagine what you've got
to do to get that organised and actually put the Games delivery in place. So
we're planning for all that now. You've talked about the budget. Now, that's one
that gets a lot of attention when we look at what's being built around the city.
Yes. How do we make sure that we're not feeling the impact of that in the
future? Yeah, look, the state government and the federal government have got the
Intergovernmental Funding Agreement all sorted out, which is the $7 billion,
which, of course, speaks to the epicentre of the venues that need to be done,
which is the Gabba and the rebuild and the Brisbane Arena. Those are the two big
ones, but there's a lot of other venues, 16 others. And then the infrastructure
is associated with transport, the Transport Ministry as well as the Ministry of
Infrastructure, which is the Deputy Premier. These sorts of activities are
project managed. I've spent my whole life building things. You know, Dow's
biggest projects were $30, $40 billion. And what I know about the front end of
projects and the government is doing this, is to project manage them and try
really hard to freeze the scope. So we're at that stage. And I think all the
noise that goes on about numbers is a little bit more of a view that we have an
estimate, but we've got to fine-tune those estimates. And that's natural in a
project. And by the way, fine-tuning means not necessarily going up, but going
down. How do we optimise the cap expand? And that's what we're spending a lot of
time with our games delivery partners. And the people appointed to the jobs in
the state government in particular are very instrumental to the delivery of
those projects on time and on budget. Because you've stated your reputation on
this, haven't you? Yeah, I do. I have and I feel it. You know, some say, you
know, you're accountable for just delivery of the games. I don't see it that
way. At the end of the day, we're delivering not just two-by-two-week events
here. We're delivering a change in the way this part of the world appears to the
world. I think I know a little bit about what global business looks like. I know
a lot about what Brisbane and South East Queensland appear to the rest of the
world. And apropos to, you know, the sun and sunshine, we have an environment,
an aesthetic, a natural beauty, a welcoming, a hospitality that is very unique.
And we've got to put through the Olympics our lifestyle on display. So this is a
growth part of the country. And if the whole country looks at Brisbane,
Queensland, and says, wow, that is the most brilliant place in Australia, and I
know that's a bit of a compete with Sydney and Melbourne, I don't mind. And my
reputation, yes, I put it at stake. 20 past 7 on ABC Radio, Brisbane. Andrew
Liveris, AO, is with us, the former chairman of CEO and Dow Chemical Company,
and, of course, the president of the organising committee for the Games, and
done his first 12 months in the job. Brisbane is one of the smallest host
cities, though, in recent times. Will that make a difference to how we're
viewed? Absolutely. I mean, Barcelona, you asked about benchmarking earlier, is
probably my best benchmark. We've seen what they've been able to do. You know,
there was a term used, tier 2 city. So small doesn't mean you're small. Small
just means relative to a London or an LA, you're not a tier 1. So how do you put
on Games, you know, tier 2 city Barcelona showed us? In fact, Barcelona is now a
destination of choice in Europe. If you think of going to Europe, you'll think
of Barcelona like you'll think of Rome or you'll think of London or Paris. So we
are going to do that with Brisbane. I mean, this will be destination southern
hemisphere. If you want to go and have a wonderful life, come live here. If you
don't, you'll come and visit. And I think putting Brisbane on the map as a brand
is part of our branding strategy. So we see the legacy piece of this as key, the
10 plus 10 as the state and the federal government, of course, talked about it.
We, I've talked about the planning, the 3 plus 3 plus 3 was 1. But that's 10
plus 10. Where are we beyond the 10 years? Economically, socially, inclusion,
diversity, incorporating our First Nations, integrating everyone culturally,
making this a presentation, not just of the greatest show on earth, literally,
but of the location. What makes an Olympic Games work and what makes one fail?
Well, if I can cut to maybe the quickest way of answering that from John Coates
and AOC leaders, how many gold medals we win is a big indicator of success for
the home team. And clearly, we won a lot of gold medals in Tokyo. And I don't
know about you, but watching the Games in Tokyo as, you know, as changed as they
were, delayed and no-one in the stands, we celebrated our athletes, our heroes
and heroines. And we looked at that and said, there's excellence. So, look,
putting our athlete on display at the core of this is the athlete, the para-
athlete and the Olympian. We have got to figure a way to actually make that
endemic to society. What do athletes do to be Olympics and Paralympics gold
medalists? They train, they focus, they look after their bodies, they look after
their minds, they're supported by their family, they're supported by their
neighbours, by their clubs, by their ecosystem. The whole supply chain of being
an athlete is actually the supply chain for us all. Health, wellbeing, nutrition
should be part of everyone's active lifestyle. So, putting that at the
centrepiece of these Games, i.e. winning the gold medals, but actually for that
seven-year-old who's looking at Prisma 30-2032 and when they watch Paris,
they'll say, I want to strive to be that hero or heroine. And for the rest of us
who won't be able to partake in the Games and compete, our city will be healthy?
Yeah, look, the change in Brisbane from my time to the times I've been visiting
to now the times I'm coming now is dramatic. I think, you know, where we're
sitting right now and what the river looks like from, you know, this vantage
point, this river can be the spine of the entire Olympic Games. This centre,
i.e. urban centre, can be a new city, a smart city, can elevate itself. We can
plan that back to planning. There's no question that putting Games away from
where the people need to come, I love, you know, like LA and SoFi Stadium, it's
right in the middle of LA. So, people now go to downtown LA. If you look at what
Paris is doing, Paris is using its icons and the river as the centrepiece.
That's what we'll do here. We're going to use this river, we're going to use
this location, the city of Brisbane. Now, of course, not to forget the regional
side, Sunshine Coast, Gold Coast, Darling Downs, up in North Queensland, we'll
all get their share of being part of these Games. But this city will change
forever. Andrew Liveris, thank you so much for coming in. 12 months into the
job, got a way to go yet? We do. A race to the finish line. Race to the finish
line, maybe become a marathon runner. Thank you so much for coming in. Not at
all, Loretta. Andrew Liveris, President of the Brisbane Organising Committee for
the 2032 Olympic and Paralympic Games. ABC Listen. It's hard not to love Bluey.
Whatever your age, it's the biggest little show on Earth. But have you ever
wondered what goes into making an episode? Well, there's a new podcast called
Behind Bluey that takes you right behind the scenes. Each week, you'll hear from
the show's creator, Joe Brum, and his creative team, explaining exactly how all
the magic is made. The new podcast Behind Bluey, only on the ABC Listen app.
Brisbane. Brisbane. I think it's a friendly city. Just an exciting place to
live. Fantastic. This is ABC Radio Brisbane. 25 past 7. Keep your breakfast DJ
songs coming in. Songs about sunshine. Some songs to warm you up today. That's
what we want to hear. 0467 922 612. Keep those songs coming in. But when you
think of ballet in your head, probably imagining something that would look a lot
like Giselle, maybe. It's a romantic ballet, that's one.