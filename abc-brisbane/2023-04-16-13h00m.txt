TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-16-13H00M.MP3
--------------------------------------------------------------------------------
 realised perhaps that's what I want to do. Alright, that's it for another Songs
and Stories. Hope you enjoyed it as much as I did. I want to thank Mick James
behind the glass there, Sterling Job as always. I want to thank my two guests,
Felice Arena and Pip Williams, a couple of authors. Next week, another two
fabulous guests. And of course, your stories and your songs. You take care and
have a great week. Look after yourselves. Hello, ABC News with David Rowlands.
The No Campaign Against an Indigenous Voice to Parliament is set to launch its
advertising campaign tomorrow. Later this year, Australians will vote on whether
an independent advisory body for First Nations people should be enshrined in the
Constitution. Leading No Campaigner, Warren Mundine, says the focus is on
ordinary Australians against the voice, not on media personalities or elites.
We're just ordinary Australians. This is about us. That Constitution affects us
as Australians and affects us as Indigenous Australians. And so that works a lot
in our favour. A woman who was fatally stabbed in Darwin on Friday night had
been visiting from out of town to see her son. Lillian Rangier has more. A
56-year-old man has been charged with murder after allegedly stabbing his
51-year-old partner with a knife in Darwin's CBD on Friday night. The ABC
understands the woman had travelled to Darwin for the court appearance of her
son, who was in jail. Her final action was to seek help in the nearby Hilton
Double Tree Hotel, where bystanders provided assistance, including Indigenous
Affairs Minister Linda Burney, who comforted the woman's family. Today, Labor
Senator Malindiri McCarthy told Sky News the incident was a reminder of how many
women are killed in Australia every week. The alleged offender is expected to
appear in court tomorrow. International calls for a ceasefire in Sudan have so
far gone unheeded, as fighting continues into a second day. Larry Butrose
reports. UN Secretary-General Antonio Guterres is leading calls for the fighting
to stop. Doctors say 56 people are now known to have died, with nearly 600
wounded. Battles have spread from the capital Khartoum across Sudan. The clashes
follow months of tension between the military and its paramilitary partner, now
turned deadly rival, the Rapid Support Forces Group. The tensions have delayed a
deal to get Sudan back to its short-lived transition to democracy after the coup
of October 2021. China has launched a weather satellite as civilian flights
altered routes to avoid a Chinese-imposed no-fly zone to the north of Taiwan,
which Beijing put in place due to the possibility of falling rocket debris.
Taiwan's Transport Ministry says Beijing initially notified Taipei that it would
impose a no-fly zone from Sunday to Tuesday, but it later said that period was
reduced to just 27 minutes after Taiwan protested. The no-fly announcement has
again stoked tensions in the region, as it follows shortly after China staged
war games around Taiwan. Beijing views Taiwan as sovereign Chinese territory,
while Taiwan asserts its independence. Victoria's Premier Daniel Andrews says
the government is looking closely into the Porter-Davis collapse and is prepared
to support affected customers. Construction work was halted on 1,700 new homes
across Victoria and Queensland when the firm went into liquidation late last
month, leaving homeowners in limbo. Liquidator Grant Thornton has written to
Porter-Davis customers to advise it received 80 submissions from builders who
are willing to take over construction works and has assessed 15 of them as
possibly being suitable. Mr Andrews says he doesn't have any announcements to
make just yet about how the government will help victims of the collapse. We
just have to go through a proper, orderly process, work out what's happened,
take action against anyone who's done the wrong thing, support people who are
worse off because of that. I certainly don't rule that out. I just can't
quite... I'm not quite ready to make an announcement about that yet. Two AFL and
fixtures for next year's Gather Round are expected to be announced in the coming
weeks amid news that the event will stay in South Australia. The SA government
and the AFL have agreed to keep games in the state until at least 2026, due to
the success of this year's event. Over 220,000 local and interstate fans are
estimated to have attended the four-day festival of football. League boss
Gellermagloklund hopes the quick decision will give organisers ample time to
follow up with another blockbuster. I think we can say now it has been an
unbelievable success. All games sold out and just incredible energy. It's been
an amazing weekend where people have pulled this together in four and a half
months. I know now with a three-year commitment with the state government, we
can actually plan with our clubs, our supporters. And New South Wales has won a
thrilling top-of-the-table Super W rugby match, beating the Queensland Reds by a
solitary point at Concord Oval. Elsewhere, the Western Force were too good for
the Fiji and Drua, 23-10 in Perth. ABC News. Aboriginal and Torres Strait
Islander listeners are advised that the following program contains the names and
voices of those who have passed. Welcome to Speaking Out. Mainly discussing land
rights and economic empowerment. Aboriginal enterprises in mining,
exploration... I just want to talk a little bit about Indigenous constitutional
recognition.....with Larissa Barrett. It's a fresh view coming on ABC Radio. It
refers to matters in relation to the powers, functions and procedures of the
voice. And in doing so, it clarifies that it's the intention that parliament
have the capacity to make those laws which bound what the voice does. It's not a
boundless body which can go into any area it chooses and the parliament will
make laws and adjust them. Constitutional recognition through an Indigenous
voice to parliament. And remembering Aboriginal leader, Unupingu. Land rights
were born in 1963 at Yirrkala in Anum Land, where I, too, was born. And my
father, Mungurawi, and other clan leaders, lodged a claim on behalf of the
Guma'ch people and our mother clanned the Urejingu people, whose leader was
Milirpum. It was a petition written on bark to the parliament in Canberra. It
was rejected at that time. And so, in 1971, was our appeal in court before
Justice Blackburn, when we claimed the minerals under our land. This is Speaking
Out. I'm Larissa Barrett. Before the end of the year, Australians will be asked
to vote on the proposed Indigenous voice to parliament, which would advise on
matters relating to Aboriginal and Torres Strait Islander people. The wording
for the upcoming referendum was recently revealed by the federal government.
Prime Minister Anthony Albanese also confirmed the body would be able to advise
executive government as well as federal parliament if the referendum were
successful. A public awareness campaign is set to take place over the coming
months in order to answer common questions about how the process will unfold.
Opposition leader Peter Dutton has formally opposed the proposal, announcing he
is set to campaign against it, despite unrest within the Liberal Party. The
decision has seen Shadow Minister for Indigenous Australians and Attorney-
General Julian Leeser resign from the front bench, and former minister Ken Wyatt
resign from the party. An Indigenous voice to parliament is a proposal which has
been met with a wide range of responses from Aboriginal and Torres Strait
Islander people. Those who disapprove of the idea claim, among other things,
that it will fail to lead to practical outcomes and is an added layer of
bureaucracy. However, Minister for Indigenous Australians Linda Burney says the
voice is a way for First Nations people to directly advise all levels of
government about laws and policies that affect their lives. It is about
recognition and listening. And there is absolutely nothing to fear from this
referendum. It is about finally recognising the incredible history that we have
in this country of 65,000 years. And it's about making sure that we put
something in place that is going to mean a change in the life outcomes of First
Nations people, Aboriginal and Torres Strait Islander people in this country.
Well, I am sure that over the next six or seven, eight months, that we are going
to see lots of misinformation, lots of disinformation and potentially scare
campaigns. What I can say to our watchers this morning is that this has been a
rigorous process. We have sought and received advice from the best legal minds
in this country. And the cabinet approved the working groups' preferred set of
words. And they are very close to what the Prime Minister advanced at GAMA quite
almost seven or eight months ago now. So I can assure everyone there is
absolutely nothing to fear from this voice. It will mean better outcomes for
Aboriginal and Torres Strait Islander people. And every single Australian
understands that a 10-year gap in life expectancy is not acceptable in this
country. It will also mean something for everyone. Every single Australian will
be able to walk prouder and taller the Sunday after referendum day. This is
Speaking Out. That's the key to it all. Keeping connected to country on ABC
Radio. Well, we see the issues that impact Indigenous people every single year
in things like the Closing the Gap reports. And for far too long, we've seen a
lack of progress on really key issues like health and education and employment,
the wellbeing of our young people. So these are issues that many Australians,
Indigenous and non-Indigenous Australians feel a great deal of frustration that
there hasn't been the pace of progress and the level of progress despite the
good intentions and goodwill of governments across the years. So, look, there is
a long list of issues that the voice will be absolutely focused on. There will
be a high expectation from Indigenous people and non-Indigenous people that the
voice will be focused on those issues, particularly around Closing the Gap. And
there are many years of work before we even start to consider any other issues.
This will absolutely be focused on those things that have a particular impact on
Aboriginal and Torres Strait Islander people. You're listening to Speaking Out.
It just comes down to showing, sharing, you know, respecting. The world from an
Indigenous perspective on ABC Radio. You apply another layer to the decision-
making process. It will dramatically change the way, and I don't believe for the
better, that our country operates. We believe in enshrining in the Constitution
recognition for Indigenous Australians. We believe in a local and regional
voice. Put it into legislation. Stop the excesses of a Canberra voice. We don't
want a Canberra voice having a say on defence, on the RBA decisions around
interest rates, on environmental approvals and outcomes, on every element of a
government decision making under this proposal by Mr Albanese is going to be
affected. It will cost billions and billions of dollars. It will require
literally thousands of public servants. This is Speaking Out. That's the key to
it all. Keeping connected to country. On ABC Radio. The Liberal Party believes
in conscience and freedom. Unlike almost any other party in the Parliament, the
Liberal Party gives backbenchers the freedom to champion the ideas they believe
in. I want to exercise that freedom because I intend to campaign for a yes vote.
I've loved my work as Shadow Attorney-General and Shadow Minister for Indigenous
Australians, and I hope I've done some good. I resign without rancour or
bitterness, and I remain a loyal Liberal fully committed to the leadership of
Peter Dutton. I've worked with Peter Dutton for many years. As the Shadow
Minister, I've probably travelled with Peter Dutton more than any other
colleague. I've seen him listen to and engage with Indigenous communities and
Indigenous leaders, and he has a genuine desire to improve the conditions that
Indigenous Australians find themselves in. Peter and I had a conversation about
my position last December on The Voice, and we've talked several times since.
They've been respectful conversations. Peter did everything he could to keep me.
I respect that, and I respect him very much. But it was clear on the day before
Shadow Cabinet and the party room meeting last week that I was in a different
position to a majority of my colleagues. At that point, Peter and I agreed that
I would take the Passover and Easter break to reflect on my position, which I've
done. You're listening to Speaking Out. It just comes down to showing, sharing,
you know, respecting. The world from an Indigenous perspective on ABC Radio. I
think that those voices are entitled to be heard, and I think it doesn't take
away from the consensus position that was achieved at Uluru and the significance
of this moment if we get it right. And that means a voice with power. The
mandate is a First Nations voice. The current amendment that needs to be
finalised must be one with power and not limiting taking away, you know, what...
Let's not talk about what it can't do. Let's talk about what it can do. We want
to improve our nation and the political process and the decisions that are made
about us. So I absolutely think, you know, there is still... There is enough
detail there. There will be more detail. There will be a process before a
referendum and a process after a referendum. And it's a politician's job to
articulate that to the nation so that people are assured and guaranteed that
when they go to the ballot box, they know absolutely what they're voting for.
This is Speaking Out. That's the key to it all. Keeping connected to country. On
ABC Radio. So the discussion we had with the Solicitor-General and the Attorney-
General, without going into all the detail of the discussion, was more around
whether there was a need for some additional words which were being considered
at that stage. And they've been referred to in the media as the seven additional
words. And those words were to the effect that Parliament could make laws with
respect to the legal effects of the representations. And we had some discussion
around that. By the time we had that discussion, the question of whether... The
words that the Prime Minister brought to the public are words which allow the
Parliament to ensure the parliamentary supremacy. It clarifies the position, the
powers, functions and procedures of the voice. And in doing so, it clarifies
that it's the intention that Parliament have the capacity to make those laws
which bound what the voice does. It's not a boundless body which can go into any
area it chooses and the Parliament will make laws and adjust them. If there are
representations made to a person who is a decision-maker in executive government
and that decision isn't taken into account or isn't received, then there are
procedural fairness responses that are available to any person who makes such a
representation. What would happen is the normal administrative law would apply.
And the words that are now before the public and will form part of the
Constitutional Alteration Bill allow for Parliament to regulate that. Any fear
that any person has had in the past should be allayed by those words. That's
Arista, Tony McEvoy, SC. You also heard from Minister for Indigenous Australians
Linda Burney, former Minister for Indigenous Australians Ken Wyatt, Director of
From the Heart Dean Parkin, former Shadow Minister for Indigenous Australians
Julian Leeser, Opposition Leader Peter Dutton and Referendum Engagement Group
member Tila Reid. Speaking out with Larissa Barron. The knowledge, the culture,
the arts, the language, the law and customs of Indigenous people. On ABC Radio.
This is Speaking Out on ABC Radio, Radio National, Radio Australia on podcast
and the ABC Listen app. I'm Larissa Barron and if you like what you're hearing,
why not rate us on your app and that way other people can find us and hear our
stories as well. As you heard earlier, the proposed wording for a referendum to
enshrine an Indigenous voice to Parliament in the Constitution has been revealed
by the federal government. Meanwhile, the South Australian government has
established its own advisory body. So what can be learnt from this state-based
approach? More on that shortly, but right now some music from the late Chris
Phillips. MUSIC MUSIC My people, my people Aboriginal people Don't forget what
we're fighting for We are Aboriginal people Of this land Custodians My people,
my people Aboriginal people Across the land Don't forget what we're fighting for
They took us away from families Set us up on mission camps They now set us up to
prison camps Saying this is best for your well-being My people, my people
Aboriginal people Don't forget what we're fighting for We are Aboriginal people
Of this land Custodians My people, my people Aboriginal people Across the land
Don't forget what we're fighting for Now, well, it's come so far One or two more
will come along But the Indians and Nannas were so long With their language and
dancing and their cultural songs My people, my people Aboriginal people Don't
forget what we're fighting for We are Aboriginal people Of this land Custodians
My people, my people Aboriginal people Across the land Don't forget what we're
fighting for How to run is our future By saying our knowledge to every
generation We keep us strong and live our own With our language and dancing and
our cultural songs My people, my people Aboriginal people Don't forget what
we're fighting for We are Aboriginal people Of this land Custodians My people,
my people Aboriginal people Across the land Don't forget what we're fighting for
Don't forget what we're fighting for We are Aboriginal people That's Chris
Phillips with My People, My People. South Australia has set up an Indigenous
voice to parliament. The South Australian Government recently passed the bill
creating the advisory body which is expected to be running before the end of the
year. South Australian Attorney General Kya Ma believes the advisory body will
help decision makers make enduring, positive change for Indigenous people and
communities. On a grey and wet day in Adelaide, thousands of people gathered in
front of Parliament House to watch history be made. The first Indigenous voice
to parliament. Really monumental, I mean I've, this is like the first major
historical event that I've actually attended in person. This is about 35 years
of my work towards amplifying Aboriginal voice, one of the most significant days
for South Australia. I know I'll go away and I'll shed a few tears of happiness
later, you know, I've held myself by and you know, very thankful day, you know,
thankful for those that have come before us, you know, for those here today and
thankful for you know, what tomorrow looks like, Aboriginal or non-Aboriginal,
you know. Stolen generation survivor Pillerwook White was taken from her family
in the Northern Territory when she was two and adopted by a White family in
Adelaide. Oh it just gives me goosebumps, you know, I'm nearly in the grave and
it's an important part in history and 44 referendums in the past, so they're not
easy. It's a day of celebration for supporters of the voice, but the upcoming
national referendum isn't far from anyone's mind. Look, there's always that
trepidation given there's a different process with a referendum and we
understand there is a lot of misinformation. We also understand that there's a
lot of First Nations people with different viewpoints and that's completely
welcome and embraced. I am really hopeful though and as more information comes
out and is being shared in a positive note, yeah, really excited to see what the
next few months brings. How do you feel as we head towards the next few months
of what could be sort of uncertain times? It's not uncertain, it's written
already there by the ancestors, I'm really excited. The country has an appetite
for change. What would you say to someone who is a bit uncertain, doesn't quite
know what they are voting for? Get on the right side of history, that's what
I'll be saying. The South Australian model will see 12 representatives from the
state's First Nations groups elected by the state's indigenous population to
advise state parliament. Much like the national model, the SA voice will be non-
binding. Any advice can be ignored by parliament. Because the SA voice was set
up by legislation passed through parliament, it can also be dissolved by a
future parliament. That's where it differs from the national model, which would
be enshrined in the constitution to advise the Australian parliament and
government on policies and laws that impact the lives of First Nations people.
Andrew Plasto is the head of Nuru College, an independent school which looks to
incorporate Aboriginal knowledge into the curriculum. He believes the SA voice
will offer a map for the rest of the country to follow. I think we'll have the
opportunity of getting the voice set up in South Australia before the referendum
and in doing so be able to sort of show that a yes vote will not be the end of
the world, it will actually be the beginning of incredible growth for this
nation. Representatives for the SA voice will be elected in coming months. It's
expected the body will be running before the end of the year. And in the moments
after the legislation was passed, the crowd outside parliament delivered a
simple message for the rest of the country. Earlier this month, one of
Australia's most influential Aboriginal leaders, Unupingu, passed away aged 74.
A trailblazer in the fight for land rights and constitutional recognition,
Unupingu was part of the first Australian legal case which tested the native
title rights of First Nations people. He helped set up the Northern Land Council
and Yothu Yindi Foundation. In 1985, he received an Order of Australian Medal
for his services to the Aboriginal community. He went on to advise successive
governments and was celebrated as a singer, artist and promoter of Indigenous
culture. Identification, this is the Press Club luncheon, recorded ex-Canberra
on the 10th of November 1977. End of identification. Our speaker today is Mr
Gulleroy Unupingu, Chairman of the Northern Land Council and a traditional
tribal leader of the Goolmuch people, owners of the land where bauxite is now
being mined on the Gove Peninsula in Arnhem Land. We are going to hear a great
deal of Mr Unupingu in the coming years. As Chairman of the Northern Land
Council, the influence he and the Aboriginal peoples he represents will exert in
the development of the North has yet to be appreciated or understood by most
people. The Northern Land Council, comprising 57 members representing 33
Aboriginal communities and groups in the northern part of the Northern
Territory, has the task of negotiating on behalf of Aboriginal people with those
who wish to use their land, such as mining companies. We look forward to hearing
Mr Unupingu's views on the development of the North, the restraints which he
feels should be imposed and the benefits which he feels should flow to the
peoples he represents. Mr Unupingu. In 1963, Land Right was established in
Arnhem Land. The Government of the Northern Territory was established in 1971.
Justice Blackburn was the court. I spoke this in Gumaich, in Australian, in one
of the languages of Australia. I spoke it like this for my own people, the
Gumaich people and other people of Australia who will be listening today. Now
let me speak it again in English language, which is only my second language and
it is difficult for us. Land Rights were born in 1963 at Yirrkala in Arnhem
Land, where I too was born. And my father, Mungurawi, and other clan leaders,
Lodja, claimed on behalf of the Gumaich people and our mother clanned the
Urejingo people, whose leader was Milirpum. It was a petition written on bark to
the Parliament in Canberra. It was rejected at that time. And so in 1971 was our
appeal in court before Justice Blackburn. And we claimed the minerals under our
land as well as our land and tried to spoil the mining and spoiling of our land
by Nabalco. So that was in English for our European Australian brothers and
sisters who have given us so much of our land at last. The Parliament,
Aboriginal Land Rights Act of 1976, gave us more land in the Northern Territory
than the whole state of Victoria. It was a big advance in our government's
thinking. But now as chairman of the Northern Land Council, I must tell you that
the government has failed to do what the Parliament told it to do almost 12
months ago. In law, we still have no land. We have no title to any land. People
we don't like come onto our land and stay on our land and we cannot get them
off. How would you feel if your home was invaded by strangers and you couldn't
get rid of them? We are bitterly disappointed by the government's laziness and
inefficiency. More than three years ago, Judge Woodward said that Aboriginal
land should be owned and looked after by Land Trust. He said he had accepted our
council's advice. More than six months ago, Judge Fox said the Land Rights Act
should be amended to allow Aboriginal registered title to their land, even
though its boundary were not surveyed. But the Parliament has risen and the Act
has not been amended. More than six months ago, we wrote to the minister about
this. Four months ago in Darwin, the minister said he would act within two
months. He still hasn't acted. The land is still not ours. If the government
will not act, then the Northern Land Council demands money from the government
to hire surveyors to get on with the job. For example, I took two land owners of
the Ranger country where uranium is being mined or will be mined. The two
brothers, Toby Magendi and Jimmy Gengeli, across to Gove where bauxite is being
mined already to show them what mining is all about, because they were told that
mining will take place in their country which involves uranium. I took them
across to Gove and here when they actually saw the damage that Nabalco has done
to the land and the holes and the pollution and the big buildings and the noise
of the heavy vehicles, they were shocked. They thought that the hole were going
to be small, but when they actually saw it, it's too big. Last month,
helicopters from Teperary Station, which is owned by Sir Frederick Sudden, a
motor car dealer who lived in Sydney, trespassed on Aboriginal land at Daly
River and took away several thousand head of cattle which belongs to an
Aboriginal company called UNIA. I would call that stealing and so would you, I'm
sure, but the land is not yet legally ours, so it is not stealing. The law
cannot help us, only our friends in Darwin Trades and Labour Council who have
put a ban on sudden motors and all cattle from Teperary. So the laziness and
inefficiency of the government is damaging and vigorous company of Aborigines,
led by Aboriginal Harry Wilson, who left Daly River Mission about five years ago
to set up an outstation called Pebamunati. So far, this Aboriginal company has
branded 9,000 cattle, trucked hundreds to the meatworks in Darwin and Catherine
and exported 400 live to Hong Kong. I have told you this story because you must
understand how we feel when our efforts are being frustrated by the government,
which can act with vigour to meet the needs of Darwin people after the cyclone,
Tracy, and to meet the needs of miners on our land. Why can't it be acted with
vigour to meet our need? Of course, we will get the title eventually, but
remember how much our people have suffered over the years from trial and broken
promises. It is any wonder that they are anxious and fearful now, still with no
land. So families and clans suffer, but now I can feel the rising spirit of our
people, the same spirit which moved the Gurindjis to walk off Wave Hill in 1966.
We are patient people, but we are determined. For this reason, I believe that
the Northern Land Council is well qualified to do its statutory duty, which is
to research and then present land claims to Aboriginal Land Commissioner Judge
Tuohyi, who will make his recommendation to the government. For example, the
judge is just finishing in Darwin the hearing of land claims for 1,190 square
miles in Burululla area of Gulf Country. Our other statutory duty is to
safeguard Aboriginal land from developers, or when necessary, negotiate with
mining companies, not only to protect Aboriginal land, but only to protect the
long-term interests of European Australians too, because Aboriginal land is part
of Australia. We live on our land. We love it. We are nothing without it.
Government can give away land for short-term gain, financial, economic, even
political. We intend to protect that part of Australia, which has been entrusted
to us, writing in agreement the strictest environmental safeguards, and we will
be there watching to enforce them. We didn't want uranium mining or any mining
on our land, but of course, Nabalco has been mining, and treating bauxite at
Gulf for some years and PHP has been mining manganese on Groote Island since
1960s. And now, people are trying to force us to accept that mining, uranium
mining, will go ahead, but we insist that we don't want uranium mining, and I
illustrated the feeling and the reaction of traditional owner when I told the
little story about Jimmy and Toby. The Northern Land Council has started its
negotiation with Ranger. I was at the first meeting in Darwin on October the
5th, and we will be meeting again in Darwin soon. We have already submitted to
Ranger our long and very detailed draft agreement, which has all Judge Fox's
recommendation in it. It is the best mining agreement in Australia. However, it
is not better than agreements which are being written all the time in other
parts of the world. But this draft agreement is, of course, confidential. But I
will reveal the text of another confidential document, which reports a meeting
in Canberra on September the 9th between the Secretary of the Department of
National Resources, the Secretary of Atomic Energy Commission, and uranium
company representatives. I reveal it as a strong protest because the Northern
Land Council should have certainly have been represented at that meeting as a
body empowered and obliged by Act of Parliament to negotiate with the mining
companies. Nothing should be hidden from the council. The report said, and I
quote, the government will shortly issue guidelines for the exploration of stage
two of Kakata National Park. The goal of these guidelines will be orderly
exploration. The guidelines will be developed within a month or so. It is most
probable that there will be a new and special form of exploration licensing. It
is highly probable that tendering will be used to allocate exploration in this
area. That is the end of one quote. So there will be an exploration on land
which we claim. But the Northern Land Council is not being consulted about this.
Finally, this confidential report said, and I quote, it is obvious that the
producers have split ranks on issues of development plan and that there is a
spirit of fierce competition between them. I envisage that the unity shown by
the forum will disappear as soon as the formal development plan gets underway.
So it is to be fierce competition to explore and exploit our land far more for
more and more uranium and our Northern Land Council is not being told about it.
What kind of people do they think we are? Two months ago I met Father Momas,
John Momas, at a conference of Pacific people in Oneyatta, Solomon Islands. And
he invited me to Papua New Guinea where I met ministers of the government in
Port Moresby. Father Momas is a traditional owner of Vulcanville Island where in
1963 Consigriento signed an agreement negotiated by Australian government to
mine copper. In 1974, after independent, the agreement was renegotiated by the
new Papua New Guinea government with the advice of international experts. The
new agreement extracted from the company far more justice in money, control and
safeguard for the people of Papua New Guinea. Today the Northern Land Council is
employing one of these same international experts, Mr Stephen Zorn of New York,
who was with us in Darwin last month in ranger talks. One of our honorary
consultant is Dr Ross Cano, an Australian in National University of Canberra. Dr
Cano is a development economist who worked for the Minister of Finance in Port
Moresby and took part in Vulcanville Negotiation. Another honorary consultant is
Dr Nicholas Peterson, also of National University. Dr Peterson is a Cambridge
trained anthropologist who was research officer to Judge Woodward in his land
rights inquiry. Let me remind you that Aboriginal people would prefer to be able
to do this professional work for themselves. But for the reason which you might
think about, we don't have our own experienced professionals yet. We believe
that Aboriginal determination and the kind of expertise I've just described will
protect the long term interests of all Australians, European and Aboriginal. As
we negotiate with the mining companies wanting to work on our land, we want not
only royalties but a fair share of profits once these rise above an accepted
level. In this way, at the expense of big companies, we will be able to pay for
better housing and other necessary improvements in our lives with less expense
for our fellow Australian taxpayers. We insist that there be no work whatever on
our land by any mining company until the Northern Land Council has signed with
the company a firm and comprehensive agreement. This is a statutory obligation.
We also insist that the companies already mining our land, Nabalco and PHP,
accept that it is reasonable and in our long term interest that they too should
renegotiate with us the agreements that they have made with the government.
These were made without our approval years ago when Australian public opinion
was very different. This is not a statutory obligation for them but it is a
moral obligation which was strongly recommended by Judge Woodward. PHP should
understand that ownership of the land at Groote Island gives real power to make
its mining difficult, if not impossible by denying its workers access to beaches
and other recreational areas, shutting them up on their mining and special
purpose leases. We don't like doing this to our friends. We do it only because
we are unhappy about the existing mining leases, church leases, grazing leases,
special purpose leases of all kind in middle of Aboriginal land. They are an
affront to our new stater and strength within Australia. They remind us of our
old dependence and weakness. They allow others to keep their influence on us
Aborigines who should be thinking and acting for themselves. The Northern Land
Council does not want to hunt PHP and Nabalco off Aboriginal land but it does
expect to renegotiate the terms of their existing leases on our land. After this
we will be able to live together as equal. We must warn PHP that the traditional
owners of Groote Island like Nanyawara Amagula are determined as the Gurungji
were determined. In the middle of last month we learnt that Ranger was starting
works of quite substantial kind of its mining side and we protested to the Prime
Minister by telegram. His answer on October 18th disturbed us. He wired us and I
quote, consistent with the government's decision and intention to consult with
Aboriginal people. The government believes that it is important they should
continue. That is to say he told us the work must continue and of course
consultation is now protection. Did he deliberately leave out any mention of
Ranger's legal obligation to sign an agreement with Northern Land Council before
any mining go on? Our field officers told me today that the work has now
stopped. We have acted and we have been successful. We will stay watchful. On
August 25th when the Prime Minister announced that uranium mining would allow on
our land, Mr Viner recalled that all just foxes' recommendation about Aboriginal
land has been accepted and he said quote, the government's decision will ensure
that Aboriginal people themselves can exercise effective control over matters
affecting their interest. End quote, we know that this is not happening and I
have given you the evidence. The Kokadu National Park will be an Aboriginal land
and we wanted this only because we thought the management of park would help us
control intrusion on our land. As Fox said, the Northern Land Council should
take part in this management but what has happened in July this year the
Northern Territory Legislative Assembly passed an ordinance setting up its own
Park and Wildlife Commission with an advisory council. No one invited the
Northern Land Council to be represented on these bodies and when we applied for
membership it took five weeks for our letters to be acknowledged and then we
were told on August 23rd that no decision would be made for some weeks. Four
months after the ordinance was passed we still didn't know if we would be
represented on these bodies then only last week I was invited as chairman to
join the advisory council but we expected seat on the commission itself which we
asked for. We believe that decisions are being made in secret which we ought to
by law to be discussing. So it goes on, these are difficult days for all of us,
Aboriginal Australians and European Australians as we learn to live together in
a new way with real equality at last. We Aborigines want to share this land with
you and we ask you to share it with us openly and without fear or secret
dealings. We ask you to be as responsible as we have been for 40,000 years in
preserving our heritage and environment. I would like to thank the President and
the members of the National Press Club for inviting me to speak here today as
chairman of the Northern Land Council. All my councillors and all Aboriginal
people appreciate the honour and opportunity. May I also thank the Australian
Broadcasting Commission for making it possible for my words to reach Aboriginal
and European Australians everywhere. Thank you. That's the late Aboriginal
leader, Yuna Pingu, who passed away earlier this month aged 74. Gently on the
breeze, journeyed long to find me. From cool rivers and tall trees, to where the
ocean and sky meet. The scent of summer's gone, stopped me and I stand, calling
me back, a message from the land. Realise what I've done and I've forgotten
them. Is it too late to start again? They're waiting for me to remember them.
Just a phone call away, to come up and stay. Going home, going home, going home.
Cedric's waiting at the station, see him smile, oh how I've missed him. Where
he's in the kitchen, cooking tea. Potato salad and trifle just for me. Long
talks, a long ride, into the bush and the riverside. Ghost stories left and
right, they keep me up all night. Sleeping in my room, with the bright green
walls, it's so quiet here, no sound at all. I'm home. Going home. Going home. To
this beautiful woman, who inspired me to sing. To this kind, gentle man, who
taught me to cherish the little things. You know I'll never forget when I come
home. Summer's gone, stop being nasty. I'm home. That's the show for this week.
Join us again next week for more stories from Indigenous Australia. This episode
of Speaking Out was produced by Jay McAllister and Manel Creed. You can email
the program speakingoutatabc.net.au and find us on social media via ABC
Indigenous. For B.S. I'm Larisa Barron. You're listening to ABC Radio.