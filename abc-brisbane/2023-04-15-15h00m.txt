TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-15-15H00M.MP3
--------------------------------------------------------------------------------
 these films now because they're dad movies. You shouldn't. I don't know. I feel
like you've said this isn't your genre. Well no, no, no, no. It's not that you
can't see them and enjoy them. I mean by all means see Master and Commander.
It's terrific. I'm just saying that a way to define them is to think of, you
know, the Hollywood movie executive in your head and think who he's trying to
get the bucks out of. It's funny, like a film like Back to the Future, even The
Raiders of the Lost Light, now we talk about it obviously they're directed at
men but I'd never really sat down to think about the the movie execs going this
is for the men. CJ, we're actually out of time. Thank you. Thank you so much for
coming in. CJ will be back with us before too long for another movie topic. This
is Nightlife on ABC Radio. Dummy Im will be my guest after the news. ABC News
with Chelsea Hetherington. A man has been charged with murder following the
stabbing death of a woman outside a hotel in Darwin's CBD on Friday night. Basil
Hindale reports. The 51 year old woman ran to the Double Tree Hotel on Darwin
Esplanade for help after allegedly being attacked in a nearby park. Staff of
Indigenous Affairs Minister Linda Burney, who were there, assisted before she
was taken to hospital where she later died. A 56 year old man was arrested at
the scene and is believed to be the woman's partner. Police have described it as
a domestic related incident. He has been charged with murder as the
investigation continues. Miss Burney has offered her heartfelt condolences
saying she comforted the woman's family before paramedics arrived. Paramedic
Stephen Touja who was fatally stabbed in Sydney has been described by his family
as an exceptional man whose compassion touched the lives of many. The 29 year
old former nurse was stabbed in front of his colleague while taking an early
morning break outside a McDonald's in southwestern Sydney. A 21 year old remains
in custody after facing court charged with his murder. Emmer Gedge who
previously worked with Mr Touja says he always helped anyone in need. He fought
so hard for his patients and for his colleagues. If he saw something that wasn't
right he would try and fix it. He was just such a lovely guy, such a good, a
genuinely good person. He was an unsung hero. A man has been arrested after an
explosive object was thrown into a crowd in western Japan where Prime Minister
Fumio Kishida was due to give a speech. North Asia correspondent James Oton
reports. Prime Minister Fumio Kishida was at a fish market in the city of
Wakayama to campaign for a local by-election. He was on his way to give a speech
when a loud bang was heard. Eyewitnesses say a cylindrical object was thrown and
white smoke came out after the explosion. Reports say two local fishermen as
well as police officers tackled a man as bystanders ran from the chaos. Fumio
Kishida was evacuated and is unharmed. There are no reports of injuries. James
Oton, ABC News, Tokyo. The United States, Japan and South Korea are discussing
holding regular missile defense and anti-submarine exercises in a bid to contain
North Korea. In a joint statement, the three countries have urged North Korea to
stop destabilizing activities and warned any nuclear tests will be met with a
strong response. It comes after a series of recent missile tests by Pyongyang.
Sudan's capital Khartoum has been rocked by heavy gunfire and explosions as army
units have clashed with a paramilitary unit known as the Rapid Support Forces
who claim they have captured the international airport. The BBC's Emmanuel
Ogunza reports. It's not yet clear what has triggered the fighting but tensions
have been high in Sudan following a standoff between the two factions. On
Thursday, the RSF, which is led by Sudan's deputy leader Mohammed Habdan Dagalo,
deployed forces near military base in the northern town of Marowe. Sudan's
leader, General Abdel Fattah Al Burhan, has said he's willing to talk to his
second-in-command to resolve the dispute over who will lead a unified army in a
proposed civilian government. Western powers and regional leaders have urged the
two sides to de-escalate and to return to talks aimed at returning the country
to civilian rule. Legal challenges are expected to complete a ban on the popular
video sharing platform TikTok, now awaiting the governor's signature in the US
state of Montana. Larry Butrose reports. Immediately after the Montana
legislature approved the ban, TikTok complained it was a case of egregious
government overreach. The Chinese company also reiterated its claim it does not
share users' data with the government in Beijing. Montana would become the first
US state to place a total ban on the app across all devices. The new laws would
fine TikTok if it tried to continue operating in the state and also find third-
party platforms such as Google if they don't remove it. A TikTok spokesperson
says the matter will now be pursued in the courts. Port Adelaide has come from
behind to beat the Western Bulldogs by 14 points in driving rain at Adelaide
Oval. Cody Waiterman kicked four goals as the Bulldogs led by eight early in the
final term, but the power kicked the next four to record a 10 goals 1070 to 8
goals 856 victory. Earlier, Essendon stunned Melbourne with a 27-point win and
Brisbane thrashed North Melbourne by 75 points in Mount Barker. And the Brisbane
Broncos have bounced back from their only NRL defeat of the season to record a
43 points to 26 victory over Gold Coast at Rabina. The Broncos trailed 14-10 at
half time but flew out of the blocks in the second half. ABC News. ABC Radio
weather update. Tonight for the southeast, a mostly clear night with overnight
temperatures in the mid-teens. For Sunday, a sunny day, the chance of fog in the
early morning, mainly inland. 30 degrees expected for Brisbane. It will be 31 in
Ipswich, 28 on the Gold Coast and 29 on the Sunshine Coast. Looking ahead, cloud
and some coastal showers are set to return from Monday. Daytime temperatures
will remain in the high 20s to early 30s. You're listening to Nightlife with
Suzanne Hill on ABC Radio. You are indeed. Hello, welcome to the second hour of
Nightlife on this Saturday night. Suzanne Hill with you in just a moment. My
guest will be a dummy-im singer, someone who came, she came second at the
Eurovision Song Contest back in 2016. She's got a new single, she's going to
play that for us but she's also going to try to convert you to jazz pop. She's
actually got some amazingly beautiful and classic songs to play for you tonight.
I do hope you will stick around. And then next hour, we will have the pop quiz.
I understand that there was no hard montage over the last few weeks. The hard
montage is back tonight, as is, I have to say, the punishment song. That'll be
Barry Manilow. So look forward to playing the pop quiz next hour. I do hope that
you will join in. But for now, it is time for Conversion. Conversion. All
tonight's guest in Conversion is someone I rightly think deserves the moniker as
Australia's Eurovision queen, Dami Im, who represented us back in 2016 with the
song Sound of Silence. Look, Dami came second that year, really should have come
first. But Dami placed the highest of any Australian act in the Eurovision Song
Contest. I think we should be proud of that despite the still bizarre fact that
geographically speaking, we really shouldn't be there. Dami won the X Factor in
2013. Since then, she's released a series of albums and chart topping singles,
including a new single that you'll hear later this hour. And Dami's got a new
album coming out later this year. She has joined you tonight to convert you to a
style of music she calls jazz pop. Hi, Dami. Welcome to Nightlife. When you hear
Sound of Silence, are you sort of back on that Eurovision stage? Yeah, I am. It
always gives me this adrenaline hit for some reason, you know, the nerves, you
know, really, it was months and months of preparation and then really intense
two weeks of just rehearsing and rehearsing for those three minutes of
performance. And it had to be perfect. So it always gives me that natural
reaction where I go, here we go. How was that? I mean, I can imagine as a kid
growing up, you would have never imagined you would be on the stage at
Eurovision as an adult. When you, was it surreal thinking back? Yeah, I remember
as a high school kid going over to my friend's place and just watching it and
going, wow, this is so bizarre, but so fun. You know, it was just pure
entertainment and music and it was just nothing like I'd ever seen before. So to
then have me on that stage, it's like, yeah, unbelievable. It's like, insert
yourself into this, like a random movie, crazy, you know, most extravagant movie
scene that was happening. Yeah. Wow. Now tonight you're playing us a jazz pop.
Why have you chosen this particular genre? Well, so when I decided that I wanted
to sing the first genre that I really listened to and sort of studied myself as
a singer was jazz pop. So I finished my classical piano degree and then I went
on to study jazz voice and that's where it all began. So, you know, I still love
these songs and this genre, but you know, like jazz, when you say jazz, you
know, there's so many different types of jazz, but I really like the ones that
are quite accessible and you know, fun to listen to and nothing too crazy
because then, you know, you just get lost. So if you came to this basically
because you were studying, what had you been listening to and did that change
when you moved from Korea to Australia at the age of nine? Had you been
listening to very different music in Korea? I listened to all sorts of music,
like just so random. So, you know, K-pop, you know, Korean pop music. And then
my mom was trained in opera singing. So there was a lot of classical music in
our house. My dad loved old pop songs, you know, Simon and Garfunkel, The
Beatles, all that. And then growing up in the Korean church, I listened to a lot
of Korean gospel and worship music. So it's just all this mix. And then jazz, I
fell in love with when I was, yeah, I decided to study. And so, yeah, it's just
all this amalgamation of all this very random, different types of music. And
tell me what, what switched you from the piano that you had been trained in to
deciding you were going to sing? Yeah. So I played piano. I was, I started
lessons when I was like five years old. And when I came to Australia, that was
what I was good at, even though I didn't speak English and I felt really
different. So piano was something I did because I was good at, I could show off.
And I didn't love classical music. I just did it because I liked showing off and
winning competition. And I wanted to sing. I really enjoyed singing in my
bedroom. I started to record myself as a, you know, I think like 10 year old
girl. And I became obsessed with singing, but it wasn't a public thing until a
bit later when I started to sing at church. And yeah, going and studying jazz
singing was when I actually admitted and sort of told everyone and my parents
that I wanted this to be something more serious than just, you know, kid wanting
to sing in her bedroom. So many of us have that being a kid singing in the
bedroom, but that's right. So a few of us actually can turn that into a career.
So I know we are going to hear some piano in the songs that you've chosen for us
tonight. In fact, particularly the first one, Jamie Cullum, Don't Stop the
Music, a cover of the Rihanna song, but you wanted to play the piano like Jamie
Cullum, didn't you? Oh man. Yeah. This was, you know, like I played piano all my
life, but I wanted to play like Jamie Cullum. So bad, all these solos and it
was, you know, it was piano, but it was cool. And it was impressive and nothing
like Chopin or Rachmaninoff that I had practiced. So yeah, love this version of,
his version of Rihanna's Don't Stop the Music. So this is our first track for
you tonight. Jamie Cullum, Don't Stop the Music. Yeah. Maybe I'm a say R is
incredible. If you don't have to go, So, do you know what just started? I just
came here to party Now we're rockin' on the dance floor Acting naughty, hands
around my waist Just let the music play We're hand in hand, just a chess Now
we're face to face Cause I wanna take you away Let's escape into the music DJ,
let it play Just come and fuse it Like the way you do this Keep on rockin' to it
Please don't stop the music I wanna take you away Let's escape into the music
DJ, let it play Like the way you do this Keep on rockin' to it Please don't stop
the music Please don't stop the music We Don't Stop The Music We Dont Stop The
Music Please don't stop, please don't stop, please don't stop, please don't stop
the music. I said baby, baby are you ready? Cause it's getting close. Don't you
feel the passion ready to explode? What goes on between us no one has to know.
This is a private show. You know I just started, I just came here to party. Now
we're rocking on the dance floor acting naughty. Your hands around my waist,
just let the music play. We're hand in hand, just a chance. Now we're face to
face. Cause I wanna take you away. Let's escape into the music. DJ let it play.
I just can't refuse it. Like the way you do this. Keep on rocking to it. Please
don't stop, please don't stop, please don't stop the music. I wanna take you
away. Let's escape into the music. DJ let it play. Like the way you do this.
Keep on rocking to it. Please don't stop the music. Please don't stop, please
don't stop, please don't stop the music. Please don't stop the music. You're
listening to Nightlife on ABC Radio. That's Jamie Cullum, Don't Stop the Music.
The first song that my special guest Dami Im has chosen for you tonight. She is
converting you to jazz pop, a style of music that she listened to a lot when she
was at university. Basically learning how to be a jazz singer. Dami if we're
gonna talk jazz and play jazz, we really can't go past Nina Simone, can we? Oh
man, yeah. Nina, I remember watching her documentary that came out a few years
ago and just finding out about her life and all the difficulties she faced with
racism and just the frustration. And I think she started off learning classical
piano as well, which was, I didn't know that. I just thought that's her genre,
what she does. But yeah, so when you, like now I appreciate her songs even more.
You sing them sometimes too at your shows, don't you? Yeah, I do. Yeah, I love
particularly singing Feeling Good. It's such a beautiful bluesy, just such a
classic. Yeah. All right, Nina Simone, My Baby Just Cares For Me. My baby don't
care for cars and races My baby don't care for high-toned places Miss Taylor is
not a star high And even Lana Turner's smile Something he can't see My baby
don't care, who knows My baby just cares for me My baby don't care, who knows My
baby just cares for me My baby don't care, who knows My baby just cares for me
My baby don't care for shorts And he don't even care for clothes He cares for me
My baby don't care for cars and races My baby don't care for He don't care for
high-toned places Miss Taylor is not a star And even Liberace's smile Something
he can't see Is something he can't see I wonder what's wrong with me My baby
just cares for My baby just cares for My baby just cares for me You're on Night
Live with Suzanne Hill on this Saturday night. I've got Dami Im in studio with
me. We are converting you to jazz pop. I'm going to hark back quickly to
Eurovision Dami. It's only a month away. Do you now watch it every year? I do. I
do. I can't not watch it now that I've experienced it once. Would you like to go
back again? Never say never. I don't know. Possibly. It was such an incredible
experience and it led me to meet the most amazing fans globally. As a musician,
there's nothing better than getting the opportunity to showcase your music to a
wider audience. Did you notice that actually ended up translating in you getting
sales in all sorts of countries where people might not have known you
beforehand? Yeah, absolutely. Sound of Silence did chart all over the world in
40-something countries. I don't know the numbers. A lot. But yeah, it was just
incredible and I had fans fly all over to Australia to see my shows. Even in
remote locations in Australia, they flew in from Poland or France. Really? For
example, what kind of location would we be talking? Like Rudy Hill, for example.
Yeah, yeah. You were there in your Rudy Hill RSS. A certain guy from Poland's
there. Yeah, I flew in just to see you because I became a fan during Eurovision
and it would just blow my mind. That's incredible. So have you caught up with
this year's entry? What's it called? Promise by Voyage? Yes, I've listened to
their song. I think it's a really strong song. So yeah, I mean with Eurovision,
you just don't know how people are going to react and every song is so
different. So yeah, I can't wait to watch what happens. Yeah, oh, I'm excited
too. Now, Madeleine Peyrou is your next pick to us as a cover of the Leonard
Cohen song, Dance Me to the End of Love. This is a version from 2004. What is it
about this song that appeals to you? It's just this something about Madeleine,
her voice and the timbre of the whole song is so just so beautiful and raw. And
I, you know, when you put it on, it just puts you in this kind of sexy mood and
it's just awesome. All right, everybody, sexy mood time coming up. Madeleine
Peyrou and Dance Me to the End of Love. Dance me to your beauty with a burning
violin. Dance me through the panic till I'm gathered safely in. Lift me like an
olive branch and be my homeward dove. Dance me to the end of love. Dance me to
the end of love. Let me see your beauty when the witnesses are gone. Let me feel
you moving like they're doing Babylon. Show me slowly what I only know the
limits of. Dance me to the end of love. Dance me to the end of love. Dance me to
the wedding now. Dance me on and on. Dance me very tenderly and dance me very
long. We're both of us beneath our love. We're both of us above. Dance me to the
end of love. Dance me to the end of love. Dance me to the children who are
asking to be born. Dance me through the curtains that our kisses have outworn.
Raise a tent of shelter now though every thread is torn. Dance me to the end of
love. Dance me to the end of love. Dance me to your beauty with the burning fire
in. Dance me through the panic till I'm gathered safely in. Touch me with your
naked hand. Touch me with your glove. Dance me to the end of love. Dance me to
the end of love. Dance me to the end of love. Dance me to the end of love. Jazz
pop is what we're playing for you tonight. Now, Shani Russell, you came by. Now,
I'm not familiar with Shani Russell. Tell me about it. So she is a very
respected jazz singer in Australia. She's also a great pianist. She was also my
vocal teacher when I was doing my masters at university. And, you know, before I
got to study with her, I started to listen to all her music and it was just so
amazing. And I used to just listen to her CDs all of like over and over again,
just trying to learn how she can just, you know, manipulate these existing tunes
into sounding like her very own. And yeah, I really think she's a special
Australian artist. This is Shani Russell, you came by. You came when the rain
was falling. You came when the skies were gray and stormy. And all my pain was
cutting rivers deep into my heart. Then you came by. You came, but I did not
hear you. You came and for reasons not so clear you stayed with me. Although I
didn't want to know why you came by. You had a way I couldn't say what made my
heart feel so. You warmed all my days like the sun. You loved me well, I gave
you hell. You're crazy, don't you know? But maybe it's me who's crazy. I waited
so long to love you. But patience is yours, it's been forever. And you gave the
treasures of your heart to me. And now I see why you came by. You came when the
rain was falling. You came when the skies were gray and stormy. You had a way I
couldn't say what made my heart feel so. You had a way I couldn't say what made
my heart feel so. You warmed all my days like the sun. You loved me well, I gave
you hell. You're crazy, don't you know? But maybe it's me who's crazy. I waited
so long to love you. But patience is yours, it's been forever. And you gave the
treasures of your heart to me. And now I see why you came by. Shani Russell, you
came by. Dummy Im is here. Dummy, someone I know you're a big fan of, that's
Norah Jones. Yes, Norah Jones. What do you love so much about her? Norah Jones,
when I found her music, I was in high school and I didn't know that had this
element of jazz. It was kind of the first time I heard it and I went, what is
that? What is that sound? I think she's real, that jazz, but it's still pop and
you can just appreciate it. She's so easy to listen to and also an amazing
pianist. And I respect artists who have such distinct sound and I think Norah
Jones is definitely one of them. Dummy, have you had the chance to meet some of
your heroes since you've embarked on this career? Yes, I had a chance to. Well,
the first person that I got to meet, a really, really famous person was of
course Danny Minogue. And then I got to meet Kylie Minogue soon after and that
was the first time I was like, oh my gosh, I can't wait to tell my friends. This
can't be happening, you know, in front of my eyes. Yeah, but I've been lucky. So
yeah, whenever I get to meet, you know, these artists that I admired for so
long, John Farnham, when I got to meet him. Oh wow. I'm really bad at
recognizing people's faces. So I remember being at this event and I finished
soundcheck and I came off stage and this gentleman came up to me and said, that
was really great. And I thought, oh, thank you. That's really kind. Who was
that? And then my manager told me, that's John Farnham. So that was that moment
for me. And I was starstruck after that happened and he would already, you know,
I was in the room anymore. Oh, you lost your moment to say something to him.
Yeah. I was just like, oh my gosh. Did you literally, when you met Kylie
Minogue, actually bring up your friends or start texting them going, oh my gosh,
I just met Kylie Minogue? Yeah, I did. Yeah. Were they impressed? Yeah. Yeah.
Yep. They were very impressed. They freaked out and yeah, it was the greatest
moment. Let's hear Nora Jones, Come Away With Me. Come away with me in the
night. Come away with me and I will write you a song. Come away with me on a
bus. Come away where they can't tempt us with their lies. And I want to walk
with you on a cloudy day in fields where the yellow grass grows near high. So
won't you try to come? Come away with me and we'll kiss on a mountain top. And
I'll never stop loving you. Come away with me. I want to wake up with the rain
falling on a tin roof while I'm safe there in your arms. So all I ask is for you
to come away with me in the night. Come away with me. This is Night Live on ABC
Radio, Suzanne Hill with you on this Saturday night. I've got Dami Im in the
studio with me. She's playing you some of her favourite jazz pop songs. Oh,
we've got Ella Fitzgerald and Louis Armstrong, Cheek to Cheek. I love this so
much because it was in the soundtrack to The English Patient, this amazing scene
in that which is my favourite film. But how did you come to it? Yeah, so I love
this tune, obviously, Cheek to Cheek, but I actually got to perform it with a
very special singer who is a very world renowned Korean soprano, her name is
Soomi Jo. And I got to sing this as a duet with her when she came to Australia.
So that was really, really special. And, you know, Kylie Minogue, when I met
Kylie Minogue, that was special because I could show off to my friends. But
Soomi Jo, I could show off to my mum who, you know, classically trained. Guess
who I get to sing with? Soomi Jo. And what did your mum say? Well, she was
impressed. Good. Were you ever tempted by opera? Oh, no, my mum growing up, she
used to say, oh, your voice is a bit too hoarse to be a classical singer. So I
don't think I'm like maybe disciplined enough to, I think with classical
singers, they're a lot more careful with their voices, whereas I'm just too
tomboyed to, you know, whereas like pop singing, you can have a little bit of
scratch and... Or a rasp or... Yeah. That adds to the character. Yeah. And I
think I just I'm not like that delicate kind of person who looks after, you
know, a piece of delicate thing. I'm just like, yeah, here and just yell and
shout. Is opera something that you like though? I mean, did your mum induct you
into loving it or is it something that you don't really engage with that much? I
appreciate the art form and how much work gets put into it. But to be really
honest, when I was studying classical piano, I didn't love that you had to put
in hours and hours and years of work. People practice, you know, eight hours a
day since they're a kid and then no one really appreciates it as much as that
work, you know. Whereas I love pop because you get instant reaction. You know,
people just go, you sing a big note and people go, yeah, yeah, that was good. So
I just I'm too impatient for classical music. That's that's that's me. Let's
listen to Dummies latest pick, which is Ella Fitzgerald and Louis Armstrong,
cheek to cheek. I find happiness. I see. When we are together. Dance cheek to
cheek. Yes, I'm in heaven. And the kids around me. Through the week. To finish.
Like a streak. When we are together. That's a cheek to cheek. Oh, I love to
climb to mountain. It's still how you speak, but it doesn't thrill me. Have as
much as dancing cheek to cheek. Oh, I'd love to go out fishing. And a river or a
creek. But I don't enjoy it as much as dancing cheek to cheek. Now, mama, dance
with me. I want my arms about you. Chums about you. Will carry me through. Yes,
heaven. I'm in heaven. My heart beats so that I can hardly speak. And I seem to
find happiness. I see. When we are together. Dance cheek to cheek. Take it,
Ella, swing it. Heaven. I'm in heaven. And my heart beats so that I can hardly
speak. And I seem to find happiness. I see. When we are out together. Dancing
cheek to cheek. Heaven. I'm in heaven. And the cares that hung around me through
the week. Seem to vanish like a gambler's lucky streak. When we're out together.
Dancing cheek to cheek. Oh, I'd love to climb a mountain. And to reach the
highest peak. But it doesn't really have as much as dancing cheek to cheek. Oh,
I'd love to go out fishing. In a river or a creek. But I don't enjoy it half as
much. As dancing cheek to cheek. Come on and dance with me. I want mom about
you. That charm about you. Will carry me through to heaven. I'm in heaven. And
my heart beats so that I can hardly speak. And I seem to find the happiness. I
see. When we're out together. Dancing cheek to cheek. Dance with me. I want mom
about you. That charm about you. Will carry me through to heaven. I'm in heaven.
And my heart beats so that I can hardly speak. And I seem to find the happiness.
I see. When we're out together. Dancing cheek to cheek. Cheek to cheek. Cheek to
cheek. Cheek to cheek. Ella Fitzgerald, Louis Armstrong, cheek to cheek, one of
the great jazz standards. Oh, surprise surprise, Jamie Cowan's on the list
again, dummy. Had to put him in twice because he's just so, so good. I could
listen to his albums over and over again since, yeah, I was a teenager. And
yeah, I picked this next track because. Which is Fascinating Rhythm. Yes. And I
got to sing this as part of my exam recital because it's a tricky song. Because
it's called Fascinating Rhythm and he does it in a way where he changes around
with the time signature. And it's a little nerdy thing, but for someone who's
studying music, you want to kind of explore what you can do with these nerdy
time signatures and keys and little, you know, it's hard, hard to play. And that
was really interesting. But Jamie Cullen makes it sound not just tricky, but
real fun to listen to and exciting. You know, he jumps up on the piano and he,
you know, jumps like that showmanship is something I really enjoyed watching and
wanted to learn as a classically trained pianist. Jamie Cullen, Fascinating
Rhythm. Got a little rhythm, rhythm, rhythm. The pitter-pats through my brain.
So darn persistent. The day isn't distant when it'll drive me insane. Comes in
the morning without any warning. And hangs around all day. I'll have to sneak up
to it. Someday I'll speak up to it. And hope it listens when I say. Bad bad.
Giddyup esper… Fascinating rhythm, you got me on the go Fascinating rhythm, I'm
on a quiver What a mess you're making The neighbors want to know Why I'm always
shaking Just like my grandmother Each morning I get up with the sun To find at
night no work has been done I know that once it didn't matter But now you're
doing wrong When you start to patter I'm so unhappy Won't you take a day off
Decide to run along Somewhere far away off And make it snappy Oh how I long to
be The man I used to be Fascinating rhythm, why don't you stop peeking on me
Music You're on Nightlife on ABC Radio, Suzanne Hill with you, I've had Dami Im
in the studio with me this hour. Now Dami, you do have a new single out called
Collide, tell us a bit about it. Yeah, so this song is about facing new seasons
in your life, you know, seasons change and you can't, there's nothing you can do
about it, you know, it just comes to you and you've got to accept it and run
along with it. And I actually wrote this while I was pregnant, I was feeling so
much anxiety and I was just scared about the future, how it was going to change
me. And I finished the song after the baby arrived and it, you know, it became
this song where I decided that I would accept this new life and everything was
different but I was going to collide and just embrace my new fate and new season
and just go with it. It's a fascinating way of putting it because I mean, once
you do have a child, life has changed forever. But is the old, I mean, did you
have to give up much of the old life? At the start, definitely, I had to give up
a lot of sleep, a lot of freedom. And I guess after I decided to embrace it, I
think I just got used to it. And I'm enjoying it now, you know, he's 11 months
old and it's so beautiful, I love it. And, you know, I was complaining so much
at the start because I was just so confused. But once you embrace and you sort
of get used to it with the new rhythm of things, you start to enjoy and see the
bright side of it and there's so much brightness here now. So we're going to
hear Collide in a moment. What else are you up to for the rest of the year
because you've got a new album coming out later in the year, don't you? So I
have been working on this whole EP while I was pregnant and after birth. So this
whole series of songs will come out in the middle of this year and hopefully
I'll be able to announce a tour as well. All right, well, everybody keep your
ears peeled for a tour from Damien. Damien, thank you so much for coming on to
Nightlife and joining us tonight to take us through your favourite jazz pop
songs. Thank you, Susanna, for having me. I really enjoyed going through all
these favourite songs of mine. So Damien and this is her new single Collide.
What? I believe time to give up the problems and give them all up to Collide
with every moment. Just throw it all in, dance in the rain, don't look back,
don't be afraid to collide with every moment. Get it all out, never back down,
love like the one love again. Ran into the drama, into a fall, caught in a web,
come out round the moon. I tried to be a piece but fell like a wall. So I gave
up the problems and gave them all up to Collide with every moment. Just throw it
all in, dance in the rain, don't look back, don't be afraid to collide with
every moment.