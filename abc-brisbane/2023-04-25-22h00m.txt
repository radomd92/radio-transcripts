TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-25-22H00M.MP3
--------------------------------------------------------------------------------
 Yeah, very good morning Craig Loretta. At the moment of Tinkalpa, we've got
reports from a crash in the Gateway Northbound near Wynnum Road. Delay is a back
to Belmont. It's been heavy on the Bruce Highway this morning. Wet roads as
well, so do drive with care. Deception Beta Griffin after an early crash near
Dollies Rock Road. So a heavy start there this morning. I'll have more details
after 6 on ABC Radio Brisbane. So we have a Nundana, we have a Cooparoos, and a
Scotian, part of the team. Now our other producer Caitlin is from Yarra Bilba,
so we need the term for anyone who lives in Yarra Bilba. Yarra Bilbian? Yeah,
that actually works, doesn't it? What about for your suburb? 1300 222 612 or
text 0467 9222 612. We'll take your calls after the news. And then talking solar
panels, when was the last time you got them checked? When do you need to get
them checked? We'll talk to a local installer. News is next on ABC Radio
Brisbane, 6 o'clock. Hi, it's Kate O'Toole here. Do you have a question about
your garden? We're from 6 on ABC. Saturday morning horticulturist Annette
McFarlane is here to help. Join ABC Gardening Talk Back from 6 o'clock Saturday
morning right here on ABC Radio Brisbane. ABC News, good morning, I'm Maria
Hadzakis. The Federal Government is facing growing public pressure from its
backbench to lift the rate of the job seeker unemployment benefit in the May
budget. Political reporter Chantelle El-Koury has more. Labor backbenches are
among more than 300 signatures on an open letter to the Prime Minister urging
him to deliver a substantial boost to job seeker, youth allowance and related
payments. Last week, a group appointed by the government to review income
support payments labelled them seriously inadequate and said an increase should
be a first priority. The Federal Government is set to reject the recommendation
to lift the rate to around $68 a day. The open letter has been co-signed by
academics, business leaders, economists and politicians, including some Labor
MPs. Millions of Australians will be able to buy 60 days' worth of medicine for
the price of a single prescription from September under a major shake-up of the
pharmaceutical benefit scheme. The Federal Government will today announce it
will double the amount of medicine some people can collect with each script from
one month to two, effectively halving the cost. The policy will be targeted at
people with chronic illnesses such as diabetes and heart disease and will
include 320 medicines on the PBS. Consumers Health Forum of Australia CEO Dr
Elizabeth Devaney says the changes mean more Australians will be able to afford
vital medication. Consumers are telling us that they can't afford their health
care. We see this change to prescribing as one way to reduce the costs of health
care. This will allow many Australians to afford the health care that they
actually need. A Queensland parliamentary inquiry examining support provided to
victims of crime will sit in Cairns in the states far north today. Kristy
Sexton-McGrath reports. The public hearings are sitting across the state and
will review the support being provided to victims of crime in Queensland,
including compensation through the Victims of Crime Assistance Act. Today's
hearing in Cairns will hear from a local crime and justice action group, the
Cairns Sexual Assault Service, Queensland Indigenous Family Violence Legal
Service and several victims of crime before an open session this afternoon. The
hearing moves to Townsville tomorrow. US President Joe Biden has formally
announced his running for re-election in 2024 with Vice President Kamala Harris
as his running mate, with the latest his mandisami. Joe Biden is already the
oldest president in US history. If he's re-elected, he'll be 86 years old after
a second full term in 2029. In a video to launch his bid to retain the White
House, he cast the next election as a fight for democracy and personal freedom.
That's why I'm running for re-election, because I know America. I know we're
still a country that believes in honesty and respect and treating each other
with dignity. Shortly after the video launched, the US President and Vice
President filed their campaign paperwork with the Federal Election Commission.
The American singer, actor and political activist Harry Belafonte has died at
the age of 96. Isadora Bogle has more. Harry Belafonte was the first to sell a
million records. Many still know him for his signature hit, the Banana Boat
Song. He won three Grammy Awards as well as an Emmy and a Tony Award. By the
1960s, he had decided to make civil rights his priority, campaigning with his
friend Martin Luther King. In the 1980s, he worked to end apartheid in South
Africa and became the driving force behind the hit song, We Are the World, the
1965 all-star musical collaboration that raised money for famine relief in
Ethiopia. In sport, the Melbourne storm has produced a second half comeback to
defeat New Zealand's Warriors 30 points to 22 in the NRL's Anzac Day evening
clash. The Warriors led 18 to six early in the match before the storm rallied.
Fullback Nick Meany scored two tries and kicked five from five conversions. To
Queensland's weather, showers in eastern districts, the central coast and the
North Tropical Coast, showers and isolated storms about the far northern
peninsula and the Torres Strait, mostly cloudy elsewhere, Cairns and Townsville
29, Mackay 27, Rockhampton 28, Bundaberg 26, Bountyser 31. In the southeast,
partly cloudy with a high chance of showers, Brisbane, Ipswich, Logan, the Gold
and Sunshine Coasts 25 to Wumbat 20. Looking ahead, partly cloudy with showers
tomorrow. ABC News. G'day, welcome to AM. I'm David Lipson coming to you from
Gadigal land in Sydney. Millions of Australians will be able to buy 60 days
worth of medicine for the price of a single prescription under a controversial
shakeup of the pharmaceutical benefit scheme in next month's federal budget.
Doctors and consumer groups are welcoming the change, which the government says
could save patients up to $180 a year, but pharmacists have been staunchly
opposed to it, political reporter Nabil Al-Nashar explains. At least six million
Australians will soon be able to collect a two-month supply of their medicine
rather than one effectively having the cost. We really welcome the government
listening to consumers who are saying we can't afford our health care. The CEO
of the Consumers Health Forum of Australia, Dr. Elizabeth Devney, says it will
help people afford vital medicines that they might otherwise have delayed
getting. We hear all the time from ordinary Australians who say I had to make a
decision between groceries, the rent or medicines, and I didn't get my medicine.
We hear from parents who say we get the medicines for our kids, but we don't get
them for ourselves because we can't afford both. The federal government says the
change is targeted at people with chronic illnesses like diabetes and heart
disease and will cover more than 320 medicines on the PBS. It'll be up to your
doctor to decide whether to write the two-month script. The decision about which
drugs should be on this list was made by the government and their advisory
committees, but I understand that what they've tried to do is look at those
Australians who take lots of medicines for chronic diseases so they regulate the
pharmacy. Not one medicine, but maybe four or five or every 15 that they take
every day. The government estimates it'll save patients more than $1.6 billion
over the next four years, and it's a change that AMA president, Professor Steve
Robson, has been pushing for. If you think about the chronic conditions
Australians have, they're typically blood pressure, diabetes, cholesterol,
mental health conditions and inflammatory conditions like arthritis. So we're
expecting to see a lot of medications that pertain to these conditions because
they're generally stable and managed over the long term. He says it'll also mean
fewer visits to the GP. That's going to free up a lot of GP appointments, and we
know that seeing a GP in Australia is a challenge. We think this will have that
spin-off benefit of opening up a lot of appointments and making it easier for
families to go and see a GP if they need to and get an appointment when they
need to. The Pharmacy Guild has been staunchly opposed to the shift, and
speaking before the government's announcement, ACT pharmacist Samantha Curtis
said there would be a huge financial cost to pharmacies. We can't afford to keep
our doors opening, offering the same services when this comes through as what we
are now. There will be cuts. There will be cuts where anticipating one third of
the pharmacy employment sector will lose their job. That's pharmacists, nurses,
pharmacy assistants. The total cost of the policy change to government will be
unveiled in the May Budget. That report from Nabil Alnashar and Emmy Groves.
Already the oldest president in US history, Joe Biden, has officially confirmed
he'll seek a second term in the White House. The announcement came in the form
of a slick video that warned Trump supporters were in fact threatening American
freedoms and asked voters to let President Biden finish the job. North America
correspondent Barbara Miller compiled this report. Hail to the chief. Please
help me welcome the President of the United States, Joe Biden. Joe Biden strides
onto the stage of a union event, his first public engagement since announcing he
will run for a second term. Here at least, that news was greeted
enthusiastically. No one's surprised by Joe Biden's 2020 forerun and some
Democratic voters welcome it. I don't think he's had enough time yet to do what
he can do. But for many, there's no getting over the fact that if elected Joe
Biden, already the oldest US president in history, would be 86 by the end of his
second term. I would love to see a younger, more dynamic Democrat run for sure.
The announcement came in the form of a three minute video, which began with
scenes of the attack on the US Capitol on January the 6th, 2021. It didn't so
much tout Joe Biden's achievements in office, but cast the president as the
defender of freedom and democracy. When I ran for president four years ago, I
said we're in a battle for the soul of America, and we still are. Barring a
major upset, the question of who the Democratic nominee will be now seems
settled, and his running mate will again be Kamala Harris. Atima Omara is a
strategist focusing on women in politics. It would have sent a terrible signal,
I think, to a key portion of the Democratic party base, which are black and
brown voters. If he had removed a vice president who has done a good job in
being loyal to the administration in a role that's very clearly defined as the
number two. For the moment, Donald Trump, aged 76, is the most likely
challenger, despite the fact that today a civil trial has begun in New York over
a historic rape allegation by the writer, Egean Carroll. Democratic strategist
Simon Rosenberg says even setting aside Donald Trump's legal wars, he has a big
hill to climb. It's always hard to defeat an incumbent president, and they're
running somebody who lost last time, and their candidate is not a spring chicken
either. There's more to do, so let's finish the job. Gone are the suggestions
Joe Biden might be a transitional president who hands over to a younger
generation. He waited a lifetime to take the top job, and today he's signalled
he's hell-bent on holding onto it. This is Barbara Miller in Washington
reporting for AM. The federal government is facing growing pressure from its
backbench to increase the rate of job seeker unemployment payments in next
month's budget. Four Labor MPs are among the 300 signatories to an open letter
to the Prime Minister calling for an urgent and substantial boost to the
benefit, warning that people doing it the toughest can't be left behind. Isabel
Massali reports. First term Labor MP Louise Miller-Frost has been hearing
stories of poverty for a long time. When I was door knocking in the campaign in
2021 and 22, I was coming across people living in garages and living in vacant
blocks and camping under trees because they just couldn't afford them. This will
be families as well as singles, and they just couldn't afford to find a
property. I hear about people who are skipping medications or choosing which
medication to have because they can't afford them all, or skipping meals so they
can feed the kids. Once the head of the St Vincent de Paul Society in South
Australia, the member for Boothby has joined fellow Labor backbenchers Alicia
Payne, Michelle Ananda-Rajah and Kate Thwaites, signing a letter calling on
their own government to substantially increase the job seeker payment. There are
people across the strata of society saying we're a wealthy country. We need to
look after the people who are doing it tough. The right thing to do, the humane
thing to do is to look after people so that they can then go on, look after
themselves and look after their family. Liberal MP Bridget Archer has also
signed, along with independents, Greens, business and union leaders and
community advocates. They say the payment needs to rise to address structural
injustice and increase deprivation. Last week a government appointed committee
recommended that job seeker increase from $50 a day to a rate equal to 90% of
the aged pension. That's about $68 a day. Louise Miller-Frost isn't pushing for
a specific figure but she's hopeful the Albanese government will listen. As far
as I'm aware, no decision has been made at the moment. I know there's been a lot
of speculation and I know that me lobbying and I know a number of my other
colleagues are as well making it clear within the party that this is what we're
hearing from our electorates, from our communities. This is not a fringe opinion
by any means, this is a mainstream push. The Australian Council of Social
Service coordinated the letter and CEO Cassandra Goldie won't be drawn on
whether she'd be satisfied if the government raised job seeker by a smaller than
recommended amount. It isn't too late. It certainly will be too late if they
don't do it for people who are really in financial distress now. And the
government has said this will be a federal budget about the cost of living. You
cannot seriously tackle the cost of living and miss out the most important
measure that is needed for people in the very lowest incomes which is a
substantial increase to job seeker. In a statement, Social Services Minister
Amanda Rishworth says the government is considering the committee's advice and
there will be measures in the budget to address disadvantage. Isabel Massali
reporting there. In Finance, economists will be watching the latest quarterly
inflation figures out today for signals on whether the freeze on interest rate
rises will continue. The Australian dollar is lower at 66.2 US cents. That's the
program for this morning. I'm David Lipson. It's a quarter past six. You're with
Craig Zunke and Loretta Ryan on ABC Radio Brisbane. It's looking a little gloomy
outside on this Wednesday. It's the 26th of April and hey we need your help. As
we discuss, what do you call people from your suburb? Yeah, collectively. You
know when you talk about people, for example, I'm a Nundana. And I've got the
t-shirt because a local pub was selling them. Oh was it? Yeah. And it's
fantastic. I love it. I'm a Nundana. Okay, from Nunda. What are you from your
suburb? So this was prompted by a couple of things we saw on community Facebook
pages. Marookians and Tarragindians from Coorparoo have been dubbed Coorparoo's
from Alan Marie Elliott, our producer of Breakfast. What about for your suburb?
What is the term you use to describe your people from your suburb? Your people.
Your people. 1300222 612 or maybe you're after a name and a little nickname for
your suburb residence. 04679 222 612. You can also send us a text. We'll look at
that this morning. Also before half past six... Solar panels. When was the last
time you got them checked? You're going to hear from a local solar installer
about getting a solar checkup. What you should do, what you need to be aware of.
That to come. APC Radio Brisbane traffic. I'll check the roads first. Hi there,
Brad. Yeah, very good morning, but not a great morning on the roads.
Unfortunately, there's been quite a few crashes and damp conditions. So do drive
for care. Headlights on as well for safety. Bulling Gap, we've got a current
motorcycle crash. Ipswich Road, South Bend left lane after Stanley Street.
Emergency services have arrived on site. Upper Mankivag, got a crash. Pacific
Motorway North, left and right shoulders after Clump Road. There has been some
minor delays. After an early crash at Tinguow, but that's been cleared on the
Gateway Northbound near Wynnum Road. Delays have eased. Wars back to
Bournemouth. We are seeing general peak conditions though on the Gateway
Northbound from Oatmile Plains to Mackenzie. And a hitch in the heavy on the
Bruce Highway, Burpingarra to Griffin after an early crash near Dollies Rocks
Road. And also a lot of traffic on the Ipswich Motorway and the Warragah Highway
eastbound from Carolies through to Goodnum. Australian Traffic Network. I'm
Brad. More details at half past six on ABC Radio Brisbane. Brad, where are you
from? I'm from the Valeo. The Valeo. The Valeo. The Valeo. The Valeo. The Valeo.
Cause that's really the Valeo. Alright, the Fortitude Valley, isn't it? So if
you think of something better. Fortitudean Valeo. I just thought the Valeo. The
Valeo. I like that. We could describe it as a work in progress. That's right.
For the morning too. Thank you, Brad. Thank you, Brad. 16 degrees in Brisbane at
the moment, Bayside 17, 14 at Ipswich, Logan 16, 18 on the Gold and Sunshine
Coast. A few more shows about today. Looks like tomorrow and over the course of
as we get into the weekend as well. We'll get detail from the Weather Bureau at
10 to 7. You're with Craig Zunka and Loretta Ryan on ABC Radio Brisbane. And
your suburb names. Let's go to Leanne on 1300 222 612. Leanne, where do you
live? We live in Buckingham. So what do you refer to yourselves as? Where the
Buccaneers. Oh, that's brilliant. That is, I reckon that's the best one yet. The
Buccaneers. And do you say it often? Occasionally, we say we live in Buckingham
and people go, pardon? Pardon. Where the Buccaneers. I reckon we really need to
embrace these terms. I think so. We do. Yes, we'd have so much more fun. Yeah,
exactly. That's brilliant. We've got to put together the definitive list this
morning. We need your help to do so. Lana, where do you live? I'm in Forest
Lake. So what are you known as then? We're the Flakers. Oh, the Flakers. But is
that written anywhere? Is it, you know, say on the community Facebook page or
something like that? Yeah, it's a lot quicker and easier. You know, hey,
Flakers, as opposed to, you know, hey, Forest Lakers. Yeah, it is. I think
everyone should have the t-shirt. Lana, all right, that's a good one. The
Flakers. So whenever we get callers from now on, we're going to say, oh, you're
a Flaker. Hi, how are you? Yeah, that's for sure. Your suburb 1300, 62612. Bill,
good morning to you. Good morning. Where do you live, Bill? Well, I'm at
Cinnamon Park. But what I was ringing about was that I grew up in Coorparoo. And
my dad used to refer to us as Coorparoosters. I like that. I don't know whether
that was a name he invented or whether it was used by...