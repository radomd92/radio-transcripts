TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-05-02-19H00M.MP3
--------------------------------------------------------------------------------
 So, for example, you know, if you have a spaceship falling into a black hole,
it reaches what's called the event horizon, where it looks like it's coming
slowly to a stop. And if you were to peer inside the spacecraft, you would see
astronauts frozen with their mouths open and between heartbeats and that kind of
thing, from us as a stationary observer watching them fall into it. Now, what
they experience is entirely different. They're experiencing, because it's
stationary to them, the world around them is moving and they're stationary. So,
they're just living their lives and it appears that the rest of the universe has
come to a stop. So it depends on the observation, you know, which then, of
course, you know, points directly at this being a construct and not something
that's actually real. But physicists argue about this all the time. It's a
really hotly debated subject. If you have any questions about this, 1300 800, I
know it's a big subject and I know you might be sitting there spinning out a bit
going, what? If you have a question, 1300 800 222 or text us on 0437 774. So
it's 774. After the news, I'll ask Bill if it really matters. I wonder. News
time on Overnights, three o'clock, one o'clock in WA. ABC News with David
Rowlands. Financial experts have urged people under pressure from interest rate
rises to seek help from their lenders. The Reserve Bank has lifted the cash rate
to 3.85 percent, making it 11 rises in the past year. Peter White, managing
director of the Finance Brokers Association of Australia, says there are options
for people who are struggling to repay loans. If you look like this is going to
create hardship for you, the first thing you must do is ring your bank, have a
chat to them. They have special divisions to look after this. Don't be shy to
reach out for help. It's perfectly fine. Many people do and these things are set
up to help you within the banking systems. There is help there if you want it.
Don't let it pile up on top of you. The incoming Qantas chief executive has
committed to improving the airline's customer experience, which she's
acknowledged has slipped in recent years in the wake of the COVID pandemic.
Vanessa Hudson will become the airline's first female CEO when Alan Joyce steps
down at the end of the year. She's been with the company for almost three
decades and is currently chief financial officer, but says that Mr. Joyce will
be a hard act to follow. I have spent a lot of time with Alan and I can see how
he weighs up decisions and what is going to make me sleep well at night in
making the tough decisions is that we make the right decisions and we make the
right decisions for the organisation, for our people and our customers. The
president of the peak body for GPs, Dr Nicole Higgins, says the federal
government should fund programs to help drive down the high rates of young
people with smoking addictions. The health minister, Mark Butler, will increase
taxes on tobacco products by five per cent a year over the next three years. The
measure is tipped to rake in an extra $3.3 billion in the federal budget over
four years. Dr Higgins says there's a desperate need to stop young people from
becoming addicted to vapes. Children are increasingly taking up vaping, but we
also know that they've got three times more chance of becoming cigarette smokers
if they do so. The NSW ambulance service has been convicted and fined almost
$200,000 for failing in its duty of care regarding policies and procedures for
restricted drugs after a late Macquarie paramedic ended his life. Bindi Bryce
has the story. Hunter paramedic Tony Jenkins died in 2018 after admitting to
tampering with ambulance fentanyl stocks using the opioid as a sleeping aid.
TechWork NSW investigated and found at least 44 vials of fentanyl had been
tampered with, dating back to 2018, but it did not say how many vials Mr Jenkins
was linked to. Investigators found NSW ambulance breached workplace safety laws
with the NSW District Court fining it $187,000. The fine would have been
$250,000 but was reduced by 25 per cent to reflect the service's guilty plea.
The head of the United Nations says Afghanistan represents the largest
humanitarian crisis in the world today. Secretary-General Antonio Guterres made
the comments after a meeting of envoys from more than 20 countries in Doha to
discuss a common international approach to the country. Mr Guterres says the UN
will stay in Afghanistan to deliver aid despite the Taliban's ban on the UN's
female staff, which he says is a violation of human rights. He's warned that six
million Afghans are one step away from famine-like conditions. Hong Kong's pro-
Beijing leadership has dramatically reduced the number of directly-elected seats
on its local district councils from 90 per cent down to 20 per cent. The seats
are the last major political representative bodies chosen by the public. In
2019, the pro-democracy camp won a landslide victory in local elections at the
height of the anti-government protests, although a few of those councillors
remain in those positions. The next district council election is expected to
take place later this year. Acclaimed Canadian singer-songwriter Gordon
Lightfoot has died aged 84. Elizabeth Cramsey looks back at his life and career.
Gordon Lightfoot was regarded as a poetic storyteller, known for his evocative
lyrics and his guitar-driven folk style. His hit songs included such classics as
If You Could Read My Mind, The Wreck of the Edmund Fitzgerald and Sundown. The
height of his popularity was in the mid-1970s, and his extensive touring saw him
retain a loyal following in Canada and the US. He battled ill health at times
during his career and died in a Toronto hospital. Weather for Queensland for
Wednesday. It's going to be sunny or mostly sunny across much of the state.
Brisbane a top of 26, tops of 25 degrees for the Gold Coast, Sunshine Coast,
Roma and Toowoomba. Brockhampton a top of 29, Bundaberg mostly sunny and 26,
Mackay sunny and 27, mostly sunny and 30 for Townsville. Longreach a top of 29,
29 for Mount Isa as well. Cairns partly cloudy and 30 degrees. Good morning, six
minutes past three, six minutes past one in Western Australia. We're
investigating time. I know it's a big question because last time we spoke with
Bill we investigated how we measure time and the history of the measurement of
time. But today we're going that little bit deeper and we're asking the question
if time actually exists or is it just something that we constructed as people?
And what are examples of how time does exist if you have some examples of that?
1300, 800, triple two. And if it's all a little bit mind boggling and you've got
a question you'd like to ask, believe me, there is no question that is too silly
or too smart. There are just questions because there are a whole heap of silly
ones that I'm going to ask. So if you want to join me in asking those questions,
1300, 800, triple two, our number. Bill, Dr Bill Lott, former Professor of
Chemistry and Physics. Bill, what does it matter? I know that matter might be an
interesting choice of terms, but what does it matter whether or not it exists or
not? We've constructed a measurement so why don't we just live with that? Yeah,
well, first of all, why did we construct it to start with? And we need time in
our life. What we're actually doing is measuring a change between two points. In
fact, I was thinking about this when we were going to talk about this subject,
about just really talking about measurement in general, how we measure
something. Because distance, and in space time you have space, distance as well
as time. And distance is always measured relative to something else. So we look
at the difference between where something starts and where it ends in terms of
length, and that makes perfect sense. The same is true for time. So what we're
actually measuring is the, when we measure time, is the difference between two
events. And then we choose these events either because they're convenient or
because they're important to us. So for example, something like a generation. A
generation is a unit of time. Used to be defined as like 20 years. I think
they've changed it now. But basically it's amount of time for somebody to be
born, grow up and have kids. That's a generation. And that term, that term for
that period of time was important to us for clearly biological reasons. Also,
things just like growing older. If you took a picture of yourself from the day
you were born every day until you died, you could clearly see the passage of
time. That actually pulls in concepts around thermodynamics. So things like
entropy or how is time related to the entropy of the universe, which is a
measure of randomness and disorder, increases over time. And so if you're
watching something decay, that's increasing entropy. So if you have a brand new
plum that you just plucked off the tree and you set it on the counter and you
took a photograph of it every day, and you would watch it slowly change and
decay. So I think in our lives, when we're looking at things that change, we
want to be able to measure that, which allows us then to predict the trends in
the future, which are then useful in our lives. So that's why time is important
as a concept. As a real thing, it enables physicists, when we start exploring
the universe and we run into these problems, we have to create a model that will
allow us to explain the thing that we're seeing. And I've got a couple of
questions about aging. And someone said, we age over time, so aging is a
physical proof of time. And also, however, can you please ask your guest where
time stands as far as aging in the human body is concerned? So yes, you can see
the passing of time as we get older from baby through to when we die. However,
we needed to put a measurement on that time so that we get an idea of how long
it is that we live for, perhaps. Sure. And if you think about other animals,
this is one of the differences between humans and other animals is that we know
we're going to die in the future. So if birds flying around the sky, they're
happily unaware of this sort of thing. So do birds have a sense of time? And of
course, they do, because the sun rising and setting is important to them, which
gets us back into the measurement of time. How do we measure? We usually measure
it through some repetitive event or some sort of oscillation, like the sun
coming up or the earth going around the sun or the resonance of an excited
cesium atom, the atomic clocks that work that way. So yeah, it's a yeah, that's
the best answer. That's good. Without drinking something. John, good morning.
Good morning, Trev. Yes, John. I guess there'd be an easy way to explain time.
If you pop on time by Pink Floyd, you wouldn't have popped down the road for a
bit of breakfast to come back and see what the consequences are. Well, that's
true, and it'd probably take that amount of time to actually play it as well,
Bill. What is that, the disco version? Yeah, yeah, it's true. John, thank you.
But music is an interesting one as well, because you have time sequences in
music. You also have a distance of time in a note with music. So that is a
measurement of it. But that music exists outside of measuring it as well. Yeah,
it does it though. I mean, you have a piece of sheet music with all that written
on there. So you've recorded it, the timing of it and everything. But you're
absolutely right. I mean, time is the musician's palette, isn't it? In addition
to pitch and note. But for example, one of my favorite sort of lack of music is
in Beethoven's Fifth. It's that pause and it makes entirely the difference in
the entire piece of music. It's true too. It's a da da da da. And it creates
that sense of anticipation, doesn't it? And that space is not a note, it's
purely a time element that has become part of the music. And so yeah, time had
to be constructed in order for people to have rhythm. But we had rhythm before
we had the timing of the music, which is also a musical timing in itself. Well,
did we? I mean, the Aboriginal peoples were dancing to music since they've been
humans. So I think time is a conscious construct, but it was a necessary one for
human activity. So that's one of the basic overriding theories. It's interesting
because Frank Zappa was an incredible mathematician as well and involved a lot
of mathematics in his music. Anyway, we'll go back to calls. We've got lots of
text to do as well. Hello, Donald. Good morning, Trevor. Good morning, Bill.
Good morning. A bit of a heady subject for the CR of the morning. Never too
heady, Bill and Donald, never too heady. Actually, this is the time of day that
it's best to ponder these questions, I reckon. Bill, I grew up reading Asimov
and Arthur C. Clarke and having them try to explain time in novels or use it to
produce short stories. And then of course, HG Wells did the time machine and
that got represented in the movie. And I think the best part in there was where
he was in the machine going relatively slowly forward and watching the dress of
the mannequin in the window change over a period of time where the event time
was compressed together, which really showed you. So my real question to you is,
did you see the movie Interstellar? And did you think that it represented time,
black hole, event horizon well? It did. It was very, very, very good. So it
brought in, you know, we're talking about time and in the relativity aspect of
the thing, we're really talking about space time. So it's not simply time. And
then there's mass associated with that as well. So you know, hugely massive
objects like a black hole or a planet even warp the fabric of space time, so it
creates little dents and bumps and rolls. And so I really express that well.
Bill, I might just explain to people for people who haven't seen Interstellar.
Interstellar is a film about the discovery of aliens. So it's an alien film.
Jodie Foster was in it. And what happens is that after that interaction with
aliens, the aliens then give an idea about how to get to where they are. And a
lot of that involves time and traveling through black holes. Is that a fair
assessment? Yes, that's fair enough. Yes. That's good. And it's the what I liked
about that movie, and it was very, very, very accurate. So that's the answer to
the question. What I liked about the movie was it was not just about time. It
was about time and space and mass and how those things interact locally with
each other in order to change the perception of time depending on where you are.
And they did a terrific job showing that. It's a good film, too, if anyone's
interested. It was a little bit out there in the very end, but it was a good
film. Andy, good morning. Hey, good morning. Apparently, when we go into outer
space, we age more quickly. I'm just wondering if there's any way further in the
universe—