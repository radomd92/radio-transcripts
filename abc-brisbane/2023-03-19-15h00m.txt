TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-03-19-15H00M.MP3
--------------------------------------------------------------------------------
 to cut it there because here's the news for you. ABC News with Chelsea
Hetherington. South Australia's Premier Peter Malinowskis is treading carefully
around suggestions of where nuclear waste from future submarines should be kept.
The Federal Government plans for nuclear-powered submarines to be built in SA
over 30 years as part of the AUKUS deal. A dump site is currently being prepared
at Kimber on the Eyre Peninsula to store the country's low to intermediate
nuclear waste. Speaking in London, Mr Malinowskis says he hoped a decision on
where the submarine waste will be disposed of will be made thoughtfully and
maturely. The Kimber nuclear waste facility is entirely different in nature to
what will be required from the Federal Government in regards to the AUKUS
nuclear waste. So conflating the two is a risky proposition. The Federal
Government is of course the body that is responsible for both so they will have
to make those judgements in due course. The Victorian opposition has backed
calls for state laws to be changed after a group of protesters performed Nazi
salutes outside state parliament on Saturday. Anti-discrimination groups have
called for the state government to extend a ban on the Nazi symbol to include
the Nazi salute. Deputy Opposition Leader David Southwick says the behaviour
needs to be stopped. That is offensive, that is inciting hate and violence and
we need the laws to protect those people from this kind of outrageous behaviour.
Emergency talks are continuing in Switzerland in an attempt to rescue the
troubled Swiss bank Credit Suisse before the opening of financial markets on
Monday. The BBC's Mark Ashdown reports. Credit Suisse is one of 30 banks
worldwide which is seen as too important to the system to be allowed to fail.
But after a turbulent week, a takeover is now being seen as the most likely
option. The Swiss government is understood to be overseeing emergency talks
which could see UBS, a bigger bank also based in Switzerland, take Credit Suisse
over. That could mean a restructuring and possibly job losses. A swift
resolution is seen as vital in order to stabilise banking stocks and the wider
markets before they open. Egypt is hosting talks between Israeli and Palestinian
officials in an attempt to end violence between the two sides ahead of the
Muslim holy month of Ramadan. In previous years, clashes between Palestinians
and Israeli police have increased during the holy month. The talks, backed by
the US and Jordan, are being held in the resort city of Sharm el-Sheikh. The
conference follows talks in Jordan last month where pledges to de-escalate
tensions were quickly derailed when a new burst of violence erupted on the same
day. The authorities in the Indian state of Punjab have extended the suspension
of internet for another day as a police operation to detain a Sikh separatist
leader continues. Almost 80 suspected supporters of Amritpal Singh have been
arrested. Mr Singh, a preacher who is demanding a separate homeland for Sikhs,
was at the centre of a massive protest last month. The Queensland Council for
Civil Liberties says sharing surveillance footage online shouldn't be permitted.
A number of high-profile crimes have led to a spike in demand for security
cameras in Queensland and seen an increase in the amount of footage of minor
crimes being spread online. Civil Liberties Council President Michael Cope says
it's a breach of privacy for those captured on screen. People should not be
allowed to put videos that they take or recordings they take inside their
private property up on social media unless they have the permission of the
person who is in it. The Canberra Raiders have recorded their first win of the
NRL season with a 24-20 victory over the Sharks at Canberra Stadium. Tim Gavel
has the details. In front of 14,000 fans in hot conditions, the Raiders held on
to win against a fast finishing Sharks. The Raiders led 24-10 midway through the
second half before the Sharks scored two late tries to Jesse Ramian and Britton
Okora. The Raiders had to hold on in the dying moments to secure a 24-20
victory. A sour note was the broken jaw suffered by Raiders hooker Danny Levi.
Early the Bulldogs held on to defeat the West Tigers 26-22. And after three
rounds, the Broncos and the Dolphins are on top of the ladder and they meet at
Lank Park on Friday night. Tim Gavel, ABC News Canberra. MacArthur have held
league leaders Melbourne City to a one-all draw in the A-League men competition.
MacArthur took the lead at Campbelltown Stadium through Jason Romero before an
own goal levelled things up in the second half. Earlier goals from Bruno
Fornaroli and Fernando Romero helped Melbourne victory to a 2-0 win over Central
Coast. And Australia has thrashed India by 10 wickets in the second one-day
international, levelling the three-game series at one-all. Mitchell Stark took
five wickets as India were bowled out for just 117 runs. And you're up to date
with the latest from ABC News. Weather for the South East. A clear night,
overnight temperatures in the high teens and low twenties. For Monday, a mostly
sunny day. The chance of fog in the early morning, mainly inland, and the slight
chance of a shower in the north at night. 32 degrees expected in Brisbane, 34 in
Ipswich and 30 on the Gold and Sunshine coasts. Some cloud and showers expected
to creep in for Tuesday and that will continue into the later part of the week.
Daytime temperatures set to hover around the high twenties to low thirties.
You're listening to Nightlife with Suzanne Buell on ABC Radio. It is five past
one? It's five past midnight in Queensland, five past ten in Western Australia.
Gee, American Pie really does have the ability to divide, doesn't it? I've got
Nikki. Thank you. What a classic. Thank you. Terrific. Anne said American Pie
was the perfect trucker to have a shower, but blimey, I had a shower, I got out
and it's still playing. Deborah, I need a general anaesthetic now, Suzanne. Oh
no, the torture song. What did we do to deserve this, says Gary. Yes, well, this
I think is why it was a perfect torture song, wasn't it? Because it really is a
punishment song. One person's punishment is another person's pleasure. Anyway,
look, we got there, we did the history quiz, we had time to play American Pie.
We're going to talk about a new development in batteries for electric vehicles
in just a moment and later this hour, Michael Sun, who's the cultural writer
with The Guardian, is going to be bringing you this week's new music just before
we catch up with Trevor Chappell. And I am anxious to see how many questions
Trev got right in tonight's history quiz as well. But you know, the average
electric vehicle battery can drive about 350k on one charge. So if you lived in
Europe, that would be a lot more than you'd probably be likely to do in a day.
But of course, here with our big open spaces and long distances, the
introduction of EVs is seeing the rise of what they like to call range anxiety,
that concern you just won't get enough charge out of the battery to get you
where you're going. That 350km on one charge means, for example, if you're
driving between Melbourne and Sydney, that you'd have to recharge your vehicle
two and a half times. That's 877km. I think a lot of us might also not be sure
if there are or will be enough charging stations when and where we want them.
The battery in an EV is already pretty big, but imagine if the EVs of the future
might be able to drive 1600km without needing to be recharged. That'd be pretty
amazing, wouldn't it? Well, the key is getting more energy into batteries. And
in the US, Argonne National Laboratory, which is a US Department of Energy
research agency, has come up with a way of getting more energy into batteries in
conjunction with the Illinois Institute of Technology. Now, Larry Curtis is the
senior chemist at Argonne National Laboratory, and he joins me now from the
States. Hi, Larry. Thanks for being with us on Nightlife. Hi, Suzanne. It's good
to be with you. Am I right in thinking that you've just created the most energy-
dense battery to date? That's correct. We've done it in the lab. It has not been
scaled up so it could be used yet. That's going to take a number of years more.
But yes, in the lab we have done that. So is that of all types of batteries or
specifically the kinds of batteries that you're going to be using in cars? This
can have lots of different applications. This new type of battery can have lots
of different possible applications, including aviation and long-haul trucking
and things like that. So why the research that you've been doing to create a
more energy-dense battery? So the research that we've been doing is we've been
looking at a new type of battery that involves lithium reactions with oxygen and
as opposed to lithium where lithium reacts or gets inserted into a metal oxygen
cathode material which has heavy metals like cobalt and magnesium. That's why it
can't go as far because we're using a lot lighter metals in it, lithium and
reacting with oxygen. So the key to this battery is the lithium air component.
So can you explain how that works? Okay, how that works is that in this battery,
lithium cations or lithium reacts with oxygen and electrons to form a lithium-
oxygen compound. And like I said, this is much lighter than what's currently
used in lithium ion batteries and that's why it's much higher. And what we've
discovered in my collaboration with people at Illinois Institute of Technology
is that we've created one that uses a solid-state electrolyte. So all the
previous work has been based on a liquid electrolyte. This uses a solid-state
electrolyte which allows for the formation of a more dense form of lithium oxide
that involves four electron reaction instead of two electron one when you use
the liquid electrolyte. So the key to this has been development of a new solid-
state electrolyte which has advantages over a liquid electrolyte. So this
battery though, it doesn't have to be lithium-based, is that right? That's
right. So we are actually working on one that's based on sodium, which would
avoid lithium. So there's plenty of sodium, salt available as opposed to
lithium. You're dependent on sources of lithium, which is a problem. It can be a
problem if we have a lot of electric vehicles and run out of lithium. I mean,
what are the deposits like across the world in terms of lithium supplies? Are we
saying we can't basically get everyone in the world an electric car and still
have lithium left? That's probably true. I mean, if we went completely to, right
now, to completely to lithium-ion based batteries, we would run out of lithium.
And as we develop ways to, I mean, people are working on ways to recycle the
lithium, which may help also. So there are ways to get around it by recycling.
But right now, it'd be difficult if we went to 100% electric vehicles. Yeah. And
so practically it's much better for future development of EVs if we're not
relying on lithium. That's right. And also the other thing is cobalt. So the
current ones, and cobalt may be a more severe problem, depend on cobalt. And
there's a lot of limited sources of cobalt. So the one that we've developed does
not depend on metals such as cobalt or manganese. So I'm talking to Larry
Curtis, Senior Chemist at Argonne, the Argonne National Laboratory, which is in
the US. So is this new battery likely to be more expensive to manufacture? No, I
don't. We have not really done a complete economic analysis, but no, it doesn't.
The metals, the elements that it uses are not going to be as expensive as the
ones in the lithium-ion battery. It doesn't depend on, like I mentioned, cobalt
and manganese. So it'll probably be actually less expensive. So the solid
electrolyte aspect of this battery, I understand, removes some of the safety
issues that currently exist with lithium-ion batteries. So what are the
advantages of the new battery? Yeah, so that's right. All the current batteries,
most of them in electric vehicles, use liquid electrolyte. And that's the source
of the fires that you hear about once in a while, especially that occurs in cars
or laptops, because the liquids are more dangerous than a solid-state
electrolyte. A solid-state electrolyte can avoid these possible reactions that
lead to the fire, the occasional reactions that will lead to the fire. So yeah,
so a solid-state, having a solid-state one is going to be much more safe. So
what are you seeing, what, a few years before you can actually get this to
market? Are lots of people excited about it? I mean, you've come up with it in a
lab, but is the real world looking at what you're doing and going, wow, this is
really going to change everything? Yeah, so we've had a lot of interest in this
from various companies. It's going to take, we figure, maybe two or three years
to actually scale it up. So right now, it's actually the size of this cell,
battery cell, is the size of a dime. And we have to make that about a hundred
times larger than that. And so that's what we're going to be doing in the next
two or three years. After that, then once we achieve that, then we have a number
of companies that are, would be very interested in using this, such as for
aviation or automobiles and other applications. So say your battery is ready to
go in a few years' time, would you see that there was any advantage of sticking
with the kind of batteries, the lithium ion batteries that people are using now?
Would you see arguments for staying with that? Or would you think that what
you've created in the lab there is sort of a no brainer for people to put into
their EVs? Well I think it's going to take, I mean, it'll take a while because
lithium current technology is well established. So it will take a while for
that. Assuming we're successful, it would take a while to develop these
applications because each one of these applications, such as for aviation or for
automobiles, requires a different type of battery packs. And that, so that it'll
take a number of years for that to happen. So this is looking into the future.
But it is a big development. So would this have potential for long distance
trucks that might be using diesel at the moment to use these batteries,
particularly somewhere like Australia where, you know, our truckies are
literally going thousands of kilometres? Yeah, so that's what we, we look in one
of the applications, we long haul trucking, aviation is another one, planes with
this type of energy density, planes within a continent. You could have planes
flying within, say, continental United States with a type of range of when you
get out of this type of battery. If the battery is more energy dense, does that
make it heavier? No, so that, no, that means it's going to be actually lighter.
You can get more energy out of it for the same amount of weight. Oh, okay. So
you can actually make smaller batteries if you need to, but I mean, a plane's
going to need a jolly big battery, isn't it? Yeah, right. Yeah, right. Yeah. The
other thing is, the other nice thing about this battery is it can run on air so
one doesn't have to store containers of oxygen on board, whatever, wherever
you're using it. So it can run on air. So just explain, I'm not quite sure I
understand what you're saying, run in what way? Okay, well, so one needs a
source of oxygen, this battery that has a reaction between lithium and oxygen.
So one needs a source of oxygen, which could be right now, we're running these
on containers of oxygen, but it can also run on just air. So it pulls the oxygen
out of air and one doesn't have to store the oxygen in a container, which has
problems because it adds weight and also it will, other dangers because oxygen
is explosive. So if you're using air, it's safer and saves weight. Right. So you
could be sitting there on the plane, it's running on one of these batteries and
it's literally just sucking in the air all around it. Wow. Right, yeah. So what
are the big challenges that you think lie ahead? Is there any sort of really big
hurdle that scientifically you need to overcome that you can see in the next few
years? Well, yeah, there's quite a few challenges to make this into a larger
battery. First of all, it has to work on a larger scale. Another thing is faster
charging. There's always a need for making these batteries so they charge fast,
so one doesn't have to wait a long time to get it charged. So that's something
we'll be working on to make it work so it's faster charge. So those are the main
things that have to be, scaling it up so it's 100 times larger and that's
sometimes difficult, but we hope to do that in a couple of years. Well, sounds
like in a few years' time it could be a real game changer for electric vehicles.
I think a lot of us in Australia would like to have a car that we know could do
a good 1,000 kilometres before we have to think about recharging. Right, yeah.
Larry, thanks so much for joining us on Nightlife to tell us a little bit about
it. Yeah, thank you for having me. That is Larry Curtis. He's a senior chemist
at Argonne National Laboratory in the United States. It's actually perhaps a
little bit like the CSIRO in that it's actually part of the US Energy
Department. It's one of their research agencies. So certainly people there have
been working very hard to try and get this breakthrough to produce this, well,
the most energy-dense battery that has been made to date. Interesting stuff,
isn't it? Yeah, Robert says, Elon Musk has already said Tesla won't be using
lithium batteries in a decade. Sounds like there's not enough lithium for us all
to have those EVs somewhere. It might be a good thing if we don't have to use
all those resources up. 20 After One, this is Nightlife. How about some Tori
Amos, this is Cornflake Girl. There was a cornflake girl, thought that was a
good solution, hanging with the raisin girls. She's gone to the other side,
giving us a yo-yo. Things are getting kind of gross and I go at sleepy time.
This is not really, this, this, this is not really happening. You bet your life
it is. You bet your life it is. Oh honey, you bet your life. Let's peel out the
watchword. Just peel out the watchword. She knows what's going on. Seems we've
got a cheaper feel now. All the sweet tears are gone. Gone to the other side
with my hands like a baby hair. They must have paid her a nice price. She's
putting on a stupid look. This is not really, this is not really happening. You
bet your life it is. You bet your life it is. Oh honey, you bet your life. Let's
peel out the watchword. Just peel out the watchword. And the man with the golden
gun thinks he knows so much. Thinks he knows so much, yeah. Where'd you put the
keys, girl? I'm a robber. Where'd you put the keys, girl? I'm a robber. Where'd
you put the keys, honey? I'm a robber. Where'd you put the keys, girl? Tori Amos
and Cornflake, a girl 24 after 1, 24 after midnight. A couple of more comments
about batteries. Lou said the French are more practical. They're developing a
towable auxiliary battery. What for the EV? That means you have to take it with
you? I'm not sure, Lou. But Lou's also saying good luck to them with the
battery. Oxygen in the air is low concentration. Liz says, thank heavens this
new EV battery doesn't rely on cobalt. Yeah, Larry mentioned cobalt there.
Interesting. Apparently it's the Democratic Republic of the Congo has 74% of the
world's production of cobalt at the moment. It's become a Chinese economic
colony for the extraction of raw materials. And apparently there are, it's
disgusting, 40,000 children working in toxic conditions in all of these small
mines scattered around Congo trying to extract the cobalt. An ecological and
humanitarian disaster. The UN is describing that. So yeah, Liz, your point about
not needing the cobalt would certainly be good news for people who are concerned
that the current production of batteries means having to access this cobalt from
the DRC. So look, a lot of really interesting things happening. Jan says, leave
the lithium to the manic depressive. Jan. Now, as you know, we have a lovely
podcast here at Nightlife. One of the few local radio shows that actually has
our own podcast. So we like to pop as many things up on it as we can. For
example, from tonight's program, I think the chat about moss. I found that very
fascinating. And our first hour's going up online on the podcast this week in
history, all about that trial about the teacher who was caught teaching
evolution. That's going up on the podcast as well. Last night, that conversation
about the history of gin, our exploration of the Corsican language. They're all
available as podcasts for you now. And of course, lots of things that Mr. Philip
Clark does during the week as well. For example, on Tuesday, Phil had a
fascinating conversation with Professor Lucy Frost, who's an emeritus professor
of English at the University of Tasmania. Professor Frost has spent much of her
career researching and writing about 19th century women and children. She was
telling Phil about the hundreds of convict orphans as they came to be known.
They came out to Australia with their convict mothers. They ended up in
orphanages under state care. And then they ended up being used as free labor.
Here's a bit of that interview to give you a taste. Do you talk about being far
from home? These children, I mean, if you came here as an adult, you must have
thought, well, that's it. I've been blasted to the other end of the universe.
I'm never being able to go back home. For children, it was supposed to be even
worse. I'm sure that it was. I can't imagine what it would have been like to be
a 10 year old, for instance, who sailed on the convict ships with your mother.
And you knew what you were leaving behind. It's not as though you were an
infant. You knew your father was left behind, and you might never see him again.
You might have brothers and sisters who are still there. And all of your
friends, the whole world that you knew and the way the world was put together,
you know, what you ate, where you went, how you lived, how you entered a room,
everything was turned upside down when you went, first of all, into the prison
ships where you slept with all the other women below decks, and then on arrival,
separated from your mother at the harbor and carted off to the orphan schools.
Yeah. So what's that? Women were not allowed to keep their children. What about
in terms of feeding them and so on? I mean, because some of these children, if
they're born on board, were still breastfeeding, I imagine. That's right. So
children who were not weaned went with their mothers to the Cascades female
factory on arrival. This is in Hobart you're talking about? This is in Hobart.
Yes. This is the big women's prison, called a female factory, as they were also
in New South Wales. And there was a nursery there where women breastfed their
children until the children were six months old. And then they were forcibly
weaned. The mothers were sent to go out to work, and the children survived or
did not survive in the convict nurseries with other women who were there with
their tiny infants supposed to look after them. But, you know, it was pretty hit
and miss. And as you can imagine, with lots of children crowded together in a
small space, there were many illnesses like diarrhoea that went from one child
to another. So the mortality rate in the nurseries was very high. So if you
survived to be three years old, then you went off to the orphan schools. How
many children are we talking about here? Is there even an accurate number to
document? There is actually. We know that between 1828, when the male and female
schools opened, and 1879, when they closed, more than 6,000 children went
through this institution. Okay. I mean, there were children, of course, on board
ship as well, apart from the infants who were born on ship and born soon
thereafter, there were also small children in some instances on the ships
themselves, weren't there? Yes, there were. So the children who were on the
ships, and there were more than 2,000 children who sailed with their mothers,
were sometimes quite tiny, but sometimes they could be well into their teens.
And getting onto the ship seems to have been quite a haphazard matter. When I
was working on women who were transported from Scotland on a particular ship,
the Atwick, which sailed in 1838, and I discovered on that ship that one woman
brought six children with her all the way from Aberdeen, whereas the women from
London, by and large, could not bring any of their children with them. So we
know if they had children before they went onto the ships. And I think that in
this case, the Scots were trying to get rid of these children, you know, extra
mouths for the community to feed, and that if they got all the way down to the
Thames, that they let them on board. Now, during the famine in Ireland, the
Irish were quite keen to get rid of as many children as possible. So the number
of children who sailed with their mothers on the ships that came directly from
Ireland was quite large. On the Blackfriar, for instance, there were 261 women
and 59 children. Imagine that. Imagine that. Dear, oh dear, oh dear. That was
Professor Lucy Frost, Emeritus Professor of English at the University of
Tasmania, speaking to Philip Clark about Australia's many convict orphans. I
hadn't heard that before either. If you wanted to hear more of that interview,
it is available as a podcast on the ABC podcast. Also, if you head to our
Nightlife website, you will find it there as well. abc.net.au forward slash
nightlife will get you there. There's always the full four hours of the programs
from the last week available for you to listen back to, which I know is how a
few of you like to listen to the quizzes the next day and then the highlights of
various interviews. So make that your destination if you want to hear any of
Nightlife that you have missed during the week. And just back on that, batteries
for EVs. One of my texters says, my hubby Longhalls expresses brizzy to Sydney,
drivers don't have time to wait for recharge. You're quite right, Julie. You're
able to go 1600 in one go. Sydney to Adelaide, and you'd have a couple of
hundred Ks to spare as well. I'll get some new music for you in just a moment.
Should we have some slightly older music? Just looking through my list, see what
we might play tonight. I think I'm going for Pete Murray, Better Days, and then
we'll be joined by Michael Sun, who's the Guardian's culture writer, and he will
bring you some new music. I know I'll be stronger and I know I'll be fine for
the rest of my days. I've seen better days, put my face in my hands, get down on
my knees and I pray to God, hope he sees me through till the end. I noticed most
things, but I didn't notice the change. It was hot in the morning, then it
turned so cold towards the end of the day. There was no conversation, I just
felt like I was in space. I needed my friends there, I just turned around, they
were gone without a trace. I've seen better days, put my face in my hands, get
down on my knees and I pray to God, hope he sees me through till the end. Now I
have just started and I won't be done till the end. There's nothing I have lost,
there was once placed in the palm of my hands and all of these hard times have
faded around the bend. Now that I'm wiser, I cannot wait till I can help my
friends. I've seen better days, put my face in my hands, get down on my knees
and I pray to God, hope he sees me through till the end. I've seen better days,
put my face in my hands, get down on my knees and I pray to God, hope he sees me
through till the end. I've seen better. Pete Murray and it's called Better Days.
New music for you in just a moment. That's good. Weekday mornings from five on
ABC Radio Brisbane. New music. Now we have some new music for you on Nightlife
as we are want to do this time on a Monday morning. Michael Sun is joining us
this week. He's the culture writer with the Guardian newspaper. Hello Michael,
welcome to Nightlife. Hello and thank you for having me. And you must check out
a lot of new music every week. Look, I feel like I am just constantly head of
the clouds, headphones on at all times. So I definitely relish any and all
opportunities to actually be able to talk about it. So I'm glad to be here.
Great. All right. Now you have chosen three tunes for us. Now the first one is,
you might have to say her last name, is it Policek, Caroline Policek? It is
indeed. It's a new track by Caroline Policek. So you might actually know her
from the kind of 2010s indie duo, Chairlift. She was one half of that duo and
they kind of gained a lot of success in the New York scene early on with a song
called Bruises, which is the iconic song from that Apple ad. But since then,
she's actually gone solo and she's had this amazing solo career in creating
these really evocative, quite complex art pop tracks. She had an album called
Pang a few years ago and she's just come out with a new album called Desire I
Want to Turn Into You, released very aptly on Valentine's Day. This track, Blood
and Butter, comes off that album and like the rest of the record, it's all about
the heady rush of falling in love. So it's a very beautiful love track,
dedication of course, but then you also get this quite grotesque imagery, I
would say. It's a track that's also about how love can be obsession and how love
can kind of entail giving yourself over to someone else so completely that you
want to swallow them completely or you want to be fully inside them. And of
course, if you listen your way through the entire track, which I'm hoping you
do, you get rewarded with this gigantic bagpipe solo near the end, you know,
really staying true to her art pop roots and using some quite experimental
instrumentation here. So take a listen. It's called Blood and Butter. Alright
everybody, stick with it if for nothing else than the bagpipes. Why did you come
from you? Liberty with the critical mass, your body has the body power. Lying at
the foot of a limb, done in the navel ring, invented you. Why did you come from
you? You, what I wanted to walk beside you, needing nothing but the sun that's
in our eyes. Paint the picture in blood and butter, holy water, fire in the sky.
Let me dive through your face to the sweetest honey. Call you up, nothing to
say, no I don't need no entertaining. When the world is at bed, here's my green
and whipping red, I don't wanna get closer than your new tattoo. Look at you on
my psychological Wikipedia, yeah yeah, look how I forget who I was before I was
the way I am with you. Why did you come from you? And what I wanted to walk
beside you, needing nothing but the sun that's in our eyes. Paint the picture in
blood and butter, holy water, fire in the sky. Let me dive through your face to
the sweetest honey. Call you up, nothing to say, no I don't need no
entertaining. When the world is at bed, here's my green and whipping red, I
don't wanna get closer than your new tattoo. Since I can't sway, Why can't I say
land... For now... Let me go, You know, Michael, I wasn't too sure with the
opening bars, but that really grew on me. I'm glad it did. And how good are
those bagpipes? They are. They kind of seamlessly slot in there. It's not
suddenly some jarring bloke in Tartan who's jumping in there. They really fit
nicely. Exactly. Probably the first time I've ever appreciated bagpipes
intruding into my life like that. Not the bagpipes from John Farnham's You're
the Voice? You make a good point. You make a good argument. All right. Michael's
son is with us, who is the culture writer at The Guardian newspaper. Now, next
we have Paranul. Tell me about Paranul. Yes. So Paranul is actually a South
Korean musician who lives in Seoul. But he's kind of put out three albums with
his kind of shtick, I guess, being that to this point, he's still an anonymous
artist. We know very little about him. It's kind of like international man of
mystery. And on his third album, titled After the Magic, it actually opens with
this quite beautiful, sweeping sci-fi track, I would say. It's called Polaris,
and it's named after the star, Polaris. Similar to the kind of Karol Polacek
track we just heard, it really builds on you, I would say. It kind of opens with
these acoustic, simple strums. And then as it builds further and further, we're
journeying into the unknown. If you really listen hard, I swear you can almost
hear stars twinkling. You can kind of hear the bleeps and bloops of this
technological meltdown. And if you've watched any anime at all, this is kind of
like a wild reference, but if you've watched any anime, it almost reminds me of
this track of a Japanese anime intro. There's just such a wall of sound that
this song builds to. There's a shoegaze breakdown in there, and it's just so
head-spinning and maximalist. You can almost imagine the kind of sci-fi
adventure that it might soundtrack. So what did you say? A what breakdown? It
sounded like shoegaze to me, but... Yes, shoegaze! So that's the kind of genre
that you might associate with an act like the Cocteau Twins, for example, where
you get these stunning walls of sound that kind of just wash over you and these
hazy guitars. It's perfect for just like closing your eyes and letting it all
wash over you and basking in the music. So why is it called a shoegaze? You're
meant to be staring at your shoes while you're taking it in. I believe that's
where the term originated. Maybe it was their fans just being, you know, so
introverted that you're just looking at the ground the entire time. I have
literally never heard of that term, Michael, so thank you. I have definitely
learned something new already tonight. All right, so this is Paranoul. It's
called Polaris. this Woo woo woo Woo woo woo Woo woo woo Woo woo woo When the
dawn breaks I look out the window first Even when everyone is asleep I stand at
the end of the road Everyone listens My own conversation I try to imagine
Nothing is impossible When tomorrow night comes I will look at you from the top
of the mountain I make a wish and accept it Your smile is my source of energy I
couldn't deny it My dream, my dream, my life Woo woo woo When the dawn breaks I
will look at you from the top of the mountain When it becomes night, I will
forget I will not dream, I will not laugh I cast a spell I walk on the road I
cast a spell I walk on the road I cast a spell I walk on the road I cast a spell
I walk on the road I cast a spell I walk on the road I cast a spell I walk on
the road I cast a spell I walk on the road I cast a spell I walk on the road I
cast a spell I walk on the road I cast a spell I walk on the road I cast a spell
I walk on the road I cast a spell I walk on the road I cast a spell I walk on
the road I cast a spell I walk on the road I cast a spell I walk on the road I
cast a spell I walk on the road I cast a spell I walk on the road I cast a spell
I walk on the road I cast a spell I walk on the road I cast a spell I walk on
the road I cast a spell I walk on the road I cast a spell I walk on the road I
cast a spell I walk on the road I cast a spell I walk on the road I cast a spell
I walk on the road I cast a spell I walk on the road I cast a spell I walk on
the road I cast a spell I walk on the road I cast a spell I walk on the road I
cast a spell I walk on the road I cast a spell I walk on the road I cast a spell
I walk on the road I cast a spell I walk on the road I cast a spell I walk on
the road I cast a spell I walk on the road I cast a spell I walk on the road I
cast a spell I walk on the road I cast a spell I walk on the road I cast a spell
I walk on the road I cast a spell I walk on the road I cast a spell I walk on
the road I cast a spell I walk on the road I cast a spell I walk on the road I
cast a spell I walk on the road I cast a spell I walk on the road I cast a spell
I walk on the road I cast a spell I walk on the road I cast a spell I walk on
the road I cast a spell I walk on the road I cast a spell I walk on the road