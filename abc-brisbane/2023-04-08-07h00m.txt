TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-08-07H00M.MP3
--------------------------------------------------------------------------------
 ["S Buckle Sambo Som Small S fetch Many part개� virtual ABC News with Satyam
Weinstein. China has launched military drills and live fire exercises near
Taiwan in response to the Taiwanese President's trip to the United States. East
Asia correspondent Bill Bertels reports. China's army says it has commenced
combat readiness patrols around Taiwan on top of the coast guard patrols already
taking place in the Taiwan Strait. The military has also announced several live
fire exercises near outlying islands that Taipei controls. China's leader Xi
Jinping has vowed a resolute response to this week's meeting in the US between
President Tsai Ing-wen and the House Speaker Kevin McCarthy. The pair emphasised
stronger ties, angering China which wants to end Taiwan's democracy and put the
Taiwanese under Beijing's communist rule. Beijing has also sanctioned a top
Taiwanese diplomat by banning her from going to China, prompting ridicule from
Taiwan's Foreign Minister. North Korea says it's conducted another test of a
nuclear-capable underwater attack drone. The weapon's designed for sneak attacks
in enemy waters. It comes just a week after Pyongyang tested another version of
a similar purpose underwater drone. An Italian tourist has been killed in a car
ramming attack in Israel that left seven other people injured following days of
unrest in the region. As the BBC's Yolanda Nell reports, earlier in the day two
British-Israeli sisters were also shot dead in an ambush in the occupied West
Bank. This was on the road in the Jordan Valley in the north of the West Bank.
It seems that a gunman opened fire on a car which had these two British-Israeli
sisters. They came from another settlement to the south of the West Bank. They
were driving with their mother, we understand, on holiday, for a Passover
holiday to the Galilee in the north of Israel. Their mother has been seriously
wounded. Now, there's been no immediate claim of responsibility for either
attack, although there have been praise for these attacks from Palestinian
militant groups in the Gaza Strip. A Supreme Court justice in the United States
says he doesn't think he broke any disclosure rules by accepting free luxury
trips from a billionaire Republican donor. The non-profit news organisation Pro
Publica is reporting that Justice Clarence Thomas has been accepting holidays
from a real estate mogul for two decades. Pro Publica's Justin Elliott says
there's no independent oversight of the US Supreme Court. The fact of the matter
is about the Supreme Court, there's no higher authority, at least in the
judiciary, so they're basically left to police themselves and there's questions
around whether there's any capacity or mechanism to do that. It's all incredibly
opaque. The Liberal Party's retained ride, the last seat to be decided after the
New South Wales state election. ABC election analyst Anthony Green has confirmed
the result, with Liberal candidate Jordan Lane ahead by about 60 votes. There's
been a swing of 8.6% to Labor and Ride, previously held by retired MP Victor
Dominello. The final result leaves the new Labor government holding 45 seats,
the Coalition 36, the Greens 3 and independent candidates 9 seats. A man's in a
serious condition in hospital after being shot multiple times west of Brisbane.
Police say the shooting happened this morning at Kuminya in Queensland's
Somerset region. Crime scenes have been declared while investigations are
underway. Police believe there's no ongoing threat to the community. WA police
say an alleged sexual assault that occurred in the Goldfields city of Kalgoorlie
on Thursday night has been treated seriously by forensic detectives. They say a
woman was walking in the area of Kingsmill Street in Victory Heights after about
half past 10 and half past 11 when she was sexually assaulted by an unknown man.
Some personal items were also stolen. The alleged offender has been described as
being dark skinned, about 20 to 30 years old and approximately 160 to 170
centimetres tall. Goldfields aspirant superintendent Stephen Thompson says
incidents like this are rare, but sexual assault detectives arrived yesterday to
assist local police. Now this would have been a horrendous and traumatic
incident for the woman involved and we were doing all we can to help her through
this. Everyone has the right to walk around the community freely and without
fear. In golf, play has been called off for the day during the second round of
the US Masters due to wild weather at Augusta National. Three large pine trees
fell near spectators. Commentators on Fox Sports described the incident. Just
looking up three trees fell just off the 17th tee. We're happy to report no one
was hurt. I mean, very fortunate. The players on the green at 16 looking over
just couldn't believe it, but fortunately all the patrons were able to move out
of the way. ABC News. You can get more news at any time by downloading the ABC
Listen app. Live interviews, entertainment and music. Weekends with Andrea Gibbs
on ABC Radio. Moving house is always a stressful time, but imagine having to
move 12 million specimens housed in the country's national research collection
into a brand new building. We'll meet the man who has that big job this hour.
And look, you might be on the road this weekend travelling to see family,
travelling to a favourite beach perhaps or a festival. Maybe you travel for
food, for culture, a good fishing spot. Look, there's plenty of reasons to go
somewhere in particular, but I wonder, would you travel to see a solar eclipse?
An eclipse chaser will be sharing her passion for the dark sky and why she hits
the road for it. And I wonder, what would be the picture you would want people
to see if they googled your town, the place that you live? Would it be canola
fields, perhaps the beautiful buildings down the main drag? Maybe that great
fishing spot, the crystal blue water. Tell me, what would be the picture you'd
want people to see if they googled your town? 1300 800 222 or text 0448 922 604.
Haywire winner Blake from Berchip in Victoria knows his town and the Mallee
region is beautiful. He doesn't just wish those beautiful landscapes were the
first things you see when you Google Berchip. He's working to make that happen.
His story in just a moment. And look, I was talking earlier onto a mushroom
expert because it is mushroom season now until about June. Which ones are okay
to pick in the wild and which ones you should probably just get from the shop.
Thank you for the person who text messaged in. Make sure you leave your name
when you text in. This listener says, I would be more worried about the crazy
bull in the paddock. I just get them from the shop. Yep. Good idea. I think that
is a lot safer a lot of the time to get them from the shop. But just admire all
of the mushrooms popping up around your neighborhood. You're on weekends. I'm
Andrea Gibbs. Plenty coming up. Let's get it on with T-Rex. Get it on, bang the
gong, get it on. Get it on, bang the gong, get it on. Get it on, bang the gong,
get it on. Get it on, bang the gong, get it on. Get it on, bang the gong, get it
on. Get it on, bang the gong, get it on. Get it on, bang the gong, get it on.
Get it on, bang the gong, get it on. Get it on, bang the gong, get it on. Get it
on, bang the gong, get it on. Get it on, bang the gong, get it on. Get it on,
bang the gong, get it on. Get it on, bang the gong, get it on. Get it on, bang
the gong, get it on. Take me. Take me. From 1971, would you call that glam rock,
maybe boogie rock that is T-Rex with Get It On. It did feature in the movie
Billy Elliot. Gee, that movie was from a long time ago, but it was a good one,
wasn't it? Billy Elliot, it was the year 2000. Gee whiz, does that make you feel
old? 23 years ago. ABC Listen. Rampaging Roy Slaven and HG Nelson. I note that
some people don't use log books anymore. What? Yeah. They don't know cos, sine,
all that. Cosine, cosine, cotan. They're all great. Luching on the blind side.
Why don't they do it? I don't know. A big royal commission. What happened to the
log book? I loved it. I started loving it. Could we get the Landis to drag the
log book kicking and screaming back into our universities? Every year Haywire
encourages young regional Australians to share their story with the world, like
this year's winner from Birchip Blake has. A new generation of storytellers.
Blake Birchip Victoria, Wachabalek Country. When you search Birchip on the
internet the first thing you see is old photos. Black and white images of the
old railway. What looks like a 40 year old photo of the public hall in all its
1980s brown splendour. And a lot of photos of varying quality of the malleable
statue in the main street. It doesn't make my town look very appealing. Growing
up in a small town of 700 people there's not much more than footy in the winter
and cricket or tennis in the summer. When I wasn't training or playing I had
lots of free time on my hands which came in very handy when on my 12th birthday
I received a camera. Something clicked and a new passion began. I love to
capture the beauty of the malle through my lens. Our town looks best in spring.
The footy oval is set for finals. The freshly painted lines stand out against
the freshly cut green grass. The pink sun sets over the lake with the yellow
canola crops in the background look amazing. In grade 6 I was asked to make a
video to showcase Birchip's incredible landscapes. I used the school drone to
show the town from another angle. After hours of learning and practising I
decided to share my photos on social media and I don't just get likes online.
Blake! Those sheep photos look amazing. Keep it up mate. It makes me feel pretty
proud when people stop me in the street to talk about my photos. I feel as
though I'm doing something for the community. I volunteered to take photos for
sporting clubs and other community groups. And I even got to shoot my first
wedding last year. I think my town and region is beautiful and there's always
something amazing to capture even if it's just the malle bull in the main
street. I'm going to keep on taking photos of my town and sharing them. Until
one day when you search for Birchip it'll look as amazing to people online as it
does to the people who get to live here. ABC.net.iu slash haywire. That was
Blake from Birchip in Victoria, Wuchabalack country with his haywire winning
story and you can find more and see other great stories by young regional
Australians at ABC.net.au forward slash haywire. And I wonder what would be the
picture you'd want to come up when people googled your town is it the canola
fields like Blake said? Maybe it's a cow in the middle of town and perhaps some
beautiful buildings. I just searched for my hometown growing up and there's a
bridge, some apple statues and bottles of wine and they all look like pretty
beaut photos. A lot different to when I was growing up down there and we
actually, someone said that we were the ugliest town in Australia but I tell you
what, that lit a fire under us and we have become very pretty since I'm proud to
say, proud to be a Donnie Brooker. You're on Weekends with me, Andrea Gibbs.
This is Dead Star. I'm a flyin'. It looks so good in a mirror. I'm a flyin'
done. I'm heading out where the water is much deeper. I'm a flyin' done. I'm a
flyin' done. I'm a flyin' done. I'm a flyin' done. I'm a flyin' done. I'm a
flyin' done. I'm a flyin' done. I'm a flyin' done. I said myself, I said you.
I'm a flyin' done. I'm a flyin' done. I'm a flyin' done. I feel myself, I'm a
flyin'. It looks so good in a mirror. I sell my soul for a record. Yeah, for all
we've said and done. Cause I'm heading out where the water is much deeper. I
said myself, I said you. I'm a flyin' done. I'm a flyin' done. I'm headed out,
I'm goin' there, I'm gonna make it. I said myself, I said you. You're tuned to
Weekends with Andrea Gibbs here on ABC Radio. On April 20, a total solar eclipse
will be visible from X-Mouth in WA. It's pretty exciting. Eclipses are rare
astronomical events, ones that many people haven't seen in their lifetime.
You've got to be in the right place at the right time to catch one. And
sometimes that means travelling kilometres. You've got to chase them. We are
here for the lunar eclipse. It's going to happen any moment now. The moon's
going to turn red and I'm so excited. We actually have clear skies for once. I'm
Kirsten Banks. I'm an astrophysicist and science communicator. So I'm a proud
Wiradjuri woman. I've always known that I was Indigenous from when I was very
young. My dad would always tell me, you're Aboriginal. That's something to be
proud of. Knowing a bushland was a great way to help me connect with culture. It
allows me to come along and see the land, how my ancestors would have and
appreciate the land. Aboriginal peoples here in Australia have been looking at
this and studying the stars and the planets for tens of thousands of years. And
that's something that I'm just incredibly proud of. The first time I saw the
Milky Way in all of its grandeur was a huge turning point for me in realising
that yes, this is exactly what I want to do. My parents and I stayed at the
Nullarbor Roadhouse, which is the middle of basically nowhere. And the sky was
just on fire. There were so many colours and so many stars that I was so
overwhelmed that I cried. And it was from that moment where I was like, whoa,
space is really cool. I need to learn more about space and astronomy. Kirsten
Banks, otherwise known as AstroKirsten, loves astronomy. She loves the sky. She
also loves catching an eclipse where she can. If she can, Kirsten, you're an
eclipse chaser. How many have you managed to catch in your lifetime? Well, I'm a
pretty bad eclipse chaser because I haven't seen any total solar eclipses yet.
Well, let's begin with what is an eclipse and why are you so fascinated with
them? Yes, so a total solar eclipse is when the moon perfectly covers the face
of the sun. So it completely blocks the light from the sun going directly to the
earth in a very particular part of the earth, makes a very, very small shadow.
And I think it's really fascinating because the moon is much, much smaller than
the sun, 400 times smaller than the sun, about thereabouts. But it's also 400
times closer to the earth than the sun is. And because of that, that perfect
alignment, the moon perfectly covers the sun so we can actually see this. If the
moon were a little further away, we wouldn't get total solar eclipses. This is
just insane that it's just so divinely designed, so to speak, that we live in
this moment of the universe where the moon is the perfect position and the
perfect size and our sun is the perfect position and perfect size that we get to
see this. This is why it is such an incredible and rare event to see. Is it a
case of one minute you're in bright daylight and then it suddenly changes to
this twilight feeling? More or less, yeah. So for this particular solar eclipse,
only people in the very, very small part of X-Mouth will actually be able to see
the total solar eclipse on mainland Australia. But even so, the rest of
Australia will be able to see partially the solar eclipse. So this moon won't be
completely covering the sun, but it will partially cover the sun. But if you
don't know about it, you really won't notice anything unless you're in the path
of totality where it actually covers the sun perfectly. Now as an eclipse
chaser, you haven't managed to see one yet, but how do you chase them? Do you
just kind of keep an eye on what's happening up there in the sky? So there are a
lot of really great tools and websites to use to find out where is the best
place to go looking for solar eclipses. So for example, this one that goes
through X-Mouth, if I go to a website like timeanddate.com, that's one of my
favorite ones to go to, to check out what all the details are for solar and
lunar eclipses, it will tell you what the path is, where to go, what time to go
there as well. Because the eclipse will pass through X-Mouth, it'll start, the
totality will begin at 1.29 p.m. and 41 seconds, and will go until 1.30 p.m. and
40 seconds. So it's a very, very small moment where you have to be there at just
the right time in the right place to see this. Is this why they are such elusive
things? They really are. They only happen like maybe on average once every 18
months. And even so, they happen in different locations across the world. And I
believe that you won't be able to see the same solar eclipse again in the exact
same spot for like 6,000 years or so, I think, if I remember that correctly.
Wow. Now, can it be dangerous? Is this the one time that it's okay to look at
the sun or do you need special equipment or just, you know, with sunglasses
suffice? Okay, so you do need to be very careful. Do not look directly at the
sun. I know we need to say it sometimes, but yes, don't look directly at the sun
unless it is actually in totality. Because even if the moon is covering the sun
like 99.5% or something, there is still very harmful and very bright sun rays
that will get through and damage your eyes. So it really is only during totality
where you can safely look at the sun. Well, you're looking at the moon then
really because it's covering the sun then. But anytime outside of totality, you
must be very careful. You can use solar glasses, but also again, be very careful
that there aren't any tiny scratches in your solar glasses because any sunlight
that gets through that is going to cause permanent damage. Or you can use a
pinhole projector. So even if you get a piece of paper and just poke a little
hole in it with a toothpick and hold it up at the sun and let sunlight come
through that little hole and project on the ground below, you'll be able to see
the sun. Oh, cool. And that's a pretty budget friendly way to do it as well,
isn't it? Very much so. So I mean, astrotourism, traveling, especially to see
something spectacular in the sky, it's becoming more popular, isn't it? It is.
And it excites me so much because when it comes to astronomy, I find that that
science is one of the most accessible sciences out there because all you really
need to do is just go outside and look up and it's there ready for you. And
looking up into the sky, what can that teach us down on the earth? I mean, I
know so many Indigenous stories play out in the night sky here. Is it the same
for other First Nations people in other places? I'm sure there is. Like I said,
the night sky is just so accessible and it's a big part of our world, whether we
really realize it or not. And like you said, in Australia, there are countless
stories and cultures that talk about the night sky. Like, for example, in my
Wiradjuri heritage, we talk about the emu in the sky, which we call Bugomun, and
its position in the night sky indicates when is the right time to go looking for
emu eggs. And so it's a very intricate way to look at the stars and relate it to
what's happening in the world around you at that time. Where are the best places
in Australia to see the night sky? Because we've got quite a few great
destinations where as soon as you, I guess, you get out from the city lights,
you can check out some pretty spectacular happenings up there. Oh, absolutely.
We are very lucky living in Australia that we don't have a lot of bright cities
throughout the country. It's really just in those coastal areas. And if you go
out bush just a little bit, you can really start to see the night sky in all its
glory. But the best place that I've ever seen the night sky from Australia was
at the Nullarbor Roadhouse. That place is just a pub, a service station, a motel
caravan park, a golf halt as part of the Nullarbor Golf Course, and one single
street light for hundreds of kilometres. And the night that I was there, it was
just so bright and colourful. I cried. It was incredible. Yeah, not much else to
do out there at the Nullarbor Roadhouse but to look up at the sky in awe. Is
there is somewhere that you haven't been that you are absolutely gunning to go
to check out the sky anywhere in the world? I would love to go to Paranal
Observatory in Chile because it's very high up. You don't get a lot of clouds up
there because it's so high. And I just want to see what the night sky looks like
from one of the biggest telescope facilities in the world. Yeah, are clouds. I
didn't think about clouds. Could that be a problem for people trying to check
out the one in Exmouth in WA on April 20? Look, I really hope clouds won't be a
problem. I have every single finger crossed for everyone who is there. But it's
not out of the realm of possibility that could be clouds. But yeah, I just
really hope not. Yeah. Worth the worth travelling out there anyway for people
who live nearby. Do you think it's a pretty good community, isn't it? The Astro
community? Absolutely. They're so welcoming and everyone is just so passionate
about their work and what they do. It's just a wonderful time. Kirsten Banks,
she's an astrophysicist. If you want to check out all the wonderful things that
she does, I recommend her Instagram page. It is at AstroKirsten. Kirsten, thank
you so much for your time today. My pleasure. Weekends with Andrea Gibbs on ABC
Radio. Kirsten mentioned Emu eggs there. And I wonder if you've had enough egg
stories this Easter. Well, I've got another one, but not a chalky egg variety.
How do you move the country's fragile egg collection to another building without
breaking them? You'll meet the man who has that big job right after this. It's
Janet Jackson. Like a moth to a flame, burned by the fire. My love is blind.
Can't you see my desire? That's the way love goes. Like a moth to a flame,
burned by the fire. That's the way love goes. My love is blind. Can't you see my
desire? That's the way love goes. That's the way love goes. My love is blind.
Can't you see my desire? That's the way love goes. That's the way love goes.
That's the way love goes. That's the way love goes. My love is blind. Can't you
see my desire? That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. That's the
way love goes. That's the way love goes. That's the way love goes. ABC Listen.
Your little ears can discover so much.