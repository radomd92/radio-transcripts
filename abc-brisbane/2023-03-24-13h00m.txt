TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-03-24-13H00M.MP3
--------------------------------------------------------------------------------
 ABC News with Tom Melville. A member of the referendum working group on an
Indigenous voice to Parliament says the proposed final wording of the referendum
question was the result of months of informed discussion and debates.
Australians will vote this year on whether a voice, which would act as an
independent advisory body for First Nations people, should be enshrined in the
Constitution. However, there are concerns the proposed inclusion of the words
executive government will result in legal challenges should the government not
consult the voice on a decision. The chair of advocacy group Uphold and
Recognise, Sean Gordon, says the working group came to the right decision. We
had the discussions, we understood each of the concerns that were raised from a
legal perspective but also from a practical perspective on our communities and
we formed the position that it was absolutely vital that executive government
remain in the amendment. Two Brisbane women have briefly appeared in court
charged over their alleged involvement in an international cybercrime outfit.
Peter Quattrocelli has more. The two women, aged 27 and 34, have both been
granted bail on strict conditions after appearing in the Brisbane Magistrates
Court this morning. They were arrested and charged along with a Melbourne man
and an Adelaide man yesterday. Police allege the group is responsible for 15
significant cybercrime incidents between 2020 and 2023. It's also alleged they
set up 80 bank accounts with stolen identities to transfer nearly $2 million to
criminal associates in South Africa. Victims' losses ranged from $2,500 to
almost $500,000. New South Wales Premier Dominic Perrottet has plugged his track
record of 12 years in government on the final day of the state election
campaign. Mr Perrottet started the day at a pre-polling booth in the Sydney
suburb of Chatswood in the seat of Willoughby, previously held by popular former
Premier Gladys Berejiklian, before campaigning in the neighbouring seat of North
Shore. Both seats are held by the Liberals but they're under serious threat from
independent candidates. Mr Perrottet says tomorrow's election is a clear choice
for voters. It's not about the Liberal Party, it's not about the National Party,
it's about the great people of New South Wales. And I say to the people of New
South Wales, it is only the Liberals and National with that long-term economic
plan to take our state forward. I say to the people of New South Wales today,
back our plan, protect you, protect your family. Heading overseas where a former
cyber security adviser to US President Barack Obama says Western governments
have a right to raise concerns over the popular video sharing app TikTok. Its
CEO, Shouzi Qiu, has been questioned by a US congressional committee as pressure
mounts for an outright ban of the app. The company has been dogged by claims
that its Chinese ownership means user data could end up in the hands of the
Chinese government. Michael Daniel is now head of the Cyber Threat Alliance. He
says governments, including Australia's, should review the risks but isn't
calling for an outright ban. I certainly think that any government needs to be
making a risk-benefit trade-off. Are the benefits of, for example, having TikTok
or any other social media platform, is the risk of that worth the benefits that
they bring? King Charles will postpone his first official state visit to France
due to widespread anti-government protests. Harry Sekulic has the details. Hours
after France's interior minister fronted national television to say his country
was ready to welcome King Charles in what he described as excellent conditions,
the monarch has pulled out of the trip. There were concerns about the violent
protests erupting across France with people turning out in droves to rally
against the government's reforms to the pension system which will lift the age
of retirement from 62 to 64. King Charles was meant to visit the south-western
city of Bordeaux but the town hall has been set on fire. It's not yet clear
who's responsible. Israel's Prime Minister Benjamin Netanyahu has arrived in
London to fierce protests. Mr Netanyahu shook hands with his British counterpart
Rishi Sunak outside No. 10 Downing Street as a wall of demonstrators jeered and
shouted nearby. Jewish and Israeli organisers rallied under the banner Defend
Against Democracy to voice opposition to Netanyahu's judicial reforms which
would increase the PM's powers. In the NRL, the Broncos have claimed bragging
rights in the Battle of Brisbane. Downing Newside the Dolphins 18 points to 12.
The Broncos led 8-0 early in the second half before two quick tries saw the
Dolphins leap into a 10-8 lead. The Broncos went back ahead through Kurt
Gapewell before Ketone Staggs scored a length of the field try to seal the six
point victory. Earlier the Storm ended a two game winless streak with a 24
points to 12 victory over West's Tigers in Melbourne. You're up to date with the
latest ABC news. For more news at any time you can download the ABC Listen app.
Nightlife with Renee Crosh. Indeed it is. Hello. Great to have you here on
Weekend Nightlife with me Renee Crosh filling in for Suzanne Hill. We've got
lots of great things coming up for you. Did you hear in the news just there
about TikTok? Gee, they've got a grilling today in the US. We're going to find
out more about that with Andrew Romano who will join us in a couple of hours
from the US to find out what's going on between the US government and TikTok
which of course is owned by a Chinese outfit. Lots of those stories including
the latest on Donald Trump and also in half an hour or so this incredible 50
year old mystery. Now it's a townsfolk bloke called Sid Ramsey who was 32 at the
time. He went missing at sea sailing from Cairns to Hong Kong on his 50 foot
cutter, the Southern Maiden. What happened to him? Well, the intrigue and
speculation has fuelled a 50 year old mystery and we're going to find out for
you shortly, well in half an hour or so. But first up in this hour, a really
fantastic read that I think you will really enjoy if you like crime fiction and
if you like Australian stories. The book's called Lenny Marks Gets Away With
Murder and Victorian police officer Keren Mayne is our writer tonight. Stick
with me. This is Weekend Night Life. Night Life on ABC Radio. You love a crime
fiction, right? We all do. And tonight I have just the book for you. Police
officer Keren Mayne, now turned author, lives in Melbourne with her husband and
four very young children, including twins. And during her recent maternity leave
during lockdown, she took the brave decision to stop dreaming of becoming a
writer and actually do it. And the result is her debut novel, an irresistible
read, I've got to tell you. It's both funny and deeply moving. It's titled Lenny
Marks Gets Away With Murder. Or does she? Keren Mayne, hello. Hello. Thank you
for such a gorgeous introduction. Thank you for being here on Weekend Night Life
with me, Renee Crosh. Congratulations on really for your debut novel. It is such
a satisfying read. There's so many surprises and developments, particularly at
the end, which means you really can't put the book down, which was my problem. I
just couldn't put it down. And I'm rapt to hear it. So yeah, fantastic. And it's
a hard one when now going on to promote the book because there are twists and
turns in it. And so, you know, I've got to be careful what I say or who's in my
audience because some will have read it and some won't. So but yeah, I'm rapt to
hear it. You've lost sleep sitting there reading my book. That's fantastic. What
a thrill. Essentially, the novel's based around one character, Lenny, and she's
a young woman who is on a journey, I guess, of life discovery about finding her
identity, her place in the world and understanding her extremely complex life.
But she's so relatable in an Australian context, isn't she? I think so. And it's
funny because, I mean, you want to write characters that are realistic. I didn't
think she'd be quite as relatable as she was because some of her quirks and, you
know, things she does to calm herself and stuff, maybe she maybe they're a bit
exaggerated than what most people would have, which, you know, it's a character
in a book so that that happens. But there's just like people seem to resonate
with something, you know, in there, like whether it's the love of the TV show
Friends, like the Friends fans have come out of the woodwork or, you know, that
she does the anagrams in her head to calm herself or that she collects the same
copy of a book. And yeah, so there's as well as like actual personality traits.
Yeah, she does seem to be relatable to a lot of people on many levels, which I
love. Yes. It makes me happy. It's such a difficult thing to achieve such a rich
character that has so many traits that you think, yes, I kind of I can relate to
that person and I feel like I know them and I know them really well by the end
of the story. Let's talk about Leni and her character a little bit later,
because I do want to ask you your decision to become a writer. Okay, so you've
been a police officer for the past decade or so. Before that, I think you were a
wedding photographer. Yes, I was wedding and kids photography. So I used to do
pixie photo photography, you know, kids in plant pots and suitcases with teddies
and stuff. So, yeah, I've done that. And now you're becoming a writer. What a
career trajectory. I know it's but it doesn't seem as jumpy when you're in it as
it does from perhaps the outside. Like, I don't know, it just seems to have
been, I guess, a natural progression or maybe not natural, but sort of the, you
know, the communication skills in wedding photography came in handy for the
police force. And then the stuff I know from the police force has come in handy
for the writing. So it's kind of just, you know, bumped along fairly organically
from the inside. What drew you, Keren, to becoming a police officer? So I was
looking for something to do outside of photography because I didn't really want
to keep working for myself. And it was probably, unless my bosses are listening,
it was a bit lazy of me because I didn't really want to go and study at uni. I
wanted a job that would pay me to train and that I didn't have to, you know,
accrue a huge hex debt with the, you know, no job guaranteed. So I got into the
police force, which interested me anyway. And then, I don't know, in a blink of
an eye, 16 years later, and I'm still doing it. So it's worked out pretty well
for me. But then you say you had this compulsion to start writing because you
wanted a creative outlet. So that leads us to you writing a book, which is
really a crime fiction, really, isn't it? Yeah. And it's funny because the more
people I talk to about it as it's gone on, like I've used the term crime
adjacent a couple of times, which I did, to peel off another writer, because the
crime, and this was always my intention, I really wanted to write a story about
a person. But of course, you've got to do awful things to your people in books,
not in real life, because otherwise they don't have a character arc. And, you
know, so there's always got to be some sort of conflict and some sort of, you
know, villain, whether it be, you know, in your own mind or a real one. So I
wanted it to be a story about the person and that person was Lenny and I just
adore her. I'm still very fond of Lenny, even though I've read the book as many
times as I have to get it to this point. And but yeah, but it's definitely got
the crime theme as well. And one bookseller actually said to me, she's moved it
out of crime and put it in general fiction because she doesn't think it's your
typical crime novel. And I said, sorry about the, you know, the extra shelf
moving. But okay, that's fine, whatever works for you. So I think, yeah, it's
not like your gritty crime police procedural, and you don't have a sort of a
detective point of view in there or a race to, you know, the whodunit or
anything like that. But yeah, definitely there, it was always going to revolve
around some sort of crime with my, with my background, my day job. Yeah. Jess
With your day job. I understand with your police background that you set out to
write a very different novel. Originally, you were going to write, you were
going to write something about a female detective in the Melbourne suburbs
solving a crime. But I read that you shelved that because you said that's too
boring. Jess Yeah. And that was me. Like I thought I was being super creative.
And maybe that was just like me dipping my toe in the water because, you know, a
lot of people want to write books and it's, it's hard, you know, like writing an
easy read is as hard as writing, you know, anything else. So I did that because
I think it was writing something that I was pretty familiar with, didn't have to
do as much research. So there's me being lazy again, lazy because I didn't want
to study, lazy because I didn't want to do research. But anyway, I wrote, so I
wrote a full book, which is a police procedural, but it doesn't have like, I
find or I found what naturally happened when I was doing Lenny Marx was that
there's a fair bit of dark humour in it, which is a bit of me. There's a bit of
dry wit. So that's a bit of me injecting myself into the book. Whereas the
police one, I think it was just, I felt too much like I was still just doing
work stuff. It didn't feel like my creative outlet was giving me free enough
reign. I was still trying to put the boundaries around it of, oh, well, that
wouldn't happen. Or, you know, oh, well, in Australia, we don't have those sort
of shootouts. And, you know, nobody's pulling out a, you know, secret gun from
your ankle holster and stuff. So I just I think I got myself too caught up in
being realistic. So writing as a police officer, from a point of view of a
police officer, just wasn't as much fun. I had so much more fun writing as a as
a schoolteacher in the Dandenongs. That was that was brilliant. Which is which
is what Lenny's character is. So you shifted your focus, Kerrin Mayne, you
shifted it from writing about crime to really writing about the person who's
affected by a crime. Yes, yes. And because I love that about books, when you
walk away from a book or a TV show or a movie, and you've got that character
with you, I really like that. There's a lot of crime books that you can read.
And there's a lot of crime books that have brilliant characters and detectives
you love. And those authors absolutely nail it. But I really wanted a character
that that came with you. So as much as you loved her story and the plot, you
know, the character is what keeps you thinking, oh, I miss Lenny. I wonder what
you know, what would she make of this, etc. What's she doing now? You're
listening to Renee Crosh. And this is Weekend Night Live. Kerrin Mayne is with
us and her book, which is just out. It's her first book. It's called Lenny Marx
Gets Away With Murder, or Does She? Kerrin, I do have to acknowledge you have
four very small children, twins, in fact, that are only several months old. It
cannot be easy to find the time to write. No, so full admissions, I only had two
children when I wrote Lenny Marx. So just the two, which at the time seemed
busy. And now in hindsight, they were pretty cruisy days. Because now I've
doubled that. So yeah, it is hard, but I just love it. And it's one of the only
things that I do for myself. And I should certainly, you know, probably go to
the gym and I should probably, you know, take up tennis again and all that sort
of stuff. But the writing I can do anywhere. I've got my laptop, usually in my
bag with me. I can sit and have a coffee, write a few lines, and I feel like
I've done something for me for the day. And there's just stories that I don't
know, they sort of burn to get out of me. Like Lenny, I really just, I was so
motivated to do it. I found that time. And likewise, my household situation is
really good in that my husband and I are very, we share the load. So whatever
time I could take, he was all for it. And he was, you know, it rapto got
published and that it took off. You know, it doesn't take away the guilt, the
mum guilt that you have when you step away, but it certainly made it easier
because he was all for it. He was so supportive of me doing exactly what I
wanted to do. Oh, that's, that is great. That's great to hear a modern family. I
love how you say you sent off the publication to Penguin, you know, one of the
greatest publishers in the country. They accept it. No rejection after rejection
letters like you hear from lots of writers. But you said the day I signed my
contract, I was all abuzz. And then my four year old was kind enough to bring me
back to earth by vomiting in the lounge room. That's right. Because children
have no sense of occasion, of course. So yeah, that's right. And to be honest, I
did have many, many, many rejections because the police book, which I talked
about, the police procedural, I ended up finishing and submitting out quite
widely to agents here and in the US and got maybe 70 rejections for it in that,
you know, you either didn't hear back or you got a form rejection. I had a
couple of manuscript requests, but they didn't go anywhere. And that was fine
because I really learned the process and I really got my head around it. And it
also made me really resilient to rejection. So I think when I went to put Lenny
out there, I was like, well, what's the worst that's going to happen? They'll
say no. You know, it's, I wasn't sort of precious about it anymore. So, you
know, any writers listening, if you, you know, if you're, if you're sitting on
something thinking, should I, or shouldn't I just throw it out there? Like the
worst that could happen is somebody publishes you, like, you know so yeah, I did
have the rejections. And then actually, as well as the vomiting from my daughter
on the same day, I, yep. My editor at Penguin said, yep, let's, let's do this.
Let's have Lenny come and be published by Penguin. I also got a rejection from
an agent. I submitted it too. So it just shows how fickle the business is, you
know, like, oh, we all know that we know, but I'm just so thrilled that Lenny
Marks gets away with murder has got up. So let's talk about Lenny Marks. She's a
great character. We've said she's a school teacher in primary school. She's a
methodical, hardworking young person. She prefers, this is a quote, this is from
your book, contentment and order over the unreliability of happiness and the
messiness of relationships. That's nice, isn't it? We relate to that. She's
socially awkward. I think she has 36 copies of the Hobbit. Your favorite book,
obviously, Karen. Yeah, yeah. 37. 37. Yep. I think you've only got 11 copies
though. Is that right? I'm 12 now. I've got 12. My husband likes to find them
for me now. I've given him a, I've given him a go-to present to buy. So I'm up
to 12. And she loves, she loves Scrabble and she loves to play with anagrams.
Yeah. Yeah. Is that something that you love? Yeah, look, I always would buy the
weekend papers simply for the puzzle pages. So I sort of push the news aside and
I'd rip into the puzzles and I loved letters and numbers that, you know, the
sort of the serious version of eight out of 10 cats does count down. So I would
always see if I could, you know, get the longer word than the real contestants.
I mean, some of them are brilliant, Karen. Here's one of the anagrams. There's
hundreds in the book. Underestimate, determinate, interested, remediates,
terminated. Yeah. So it's funny, I was talking about this last night. So I did a
book event last night and one of my friends said to me, he's finished the book
and he said, I didn't read them. He goes, I just skipped over them. And I said,
and that's fine. And then the other friend said, but there's clues in them. Like
there's things in there you can learn about her mood and her mindset, which was
my intention. But I also thought if people just skip over that, what I want you
to take away from it is that's what she's doing to calm herself. She's having a
moment where she needs sort of that little internal meditation, you know, just
give herself a bit of a brain space. So whether you read it or not, and there
are a couple of little terms in there, which I won't say what they are, but that
sort of hint at what has happened or what will happen. Yeah. I just want, I
wanted it to be, that's her on the page doing the thing that keeps her sort of
running at an even keel. Well, I think it's very, very clever and it's a lovely
insight to a character that we grow to really love. So let's get to the rub.
Here's the thing. Lenny Marks has had a very traumatic life. I don't want to
give too much away from the story, but her mother herself and her little brother
are victims of domestic violence. And it deeply resonates, I think, with all of
us, Keren, because we know so much now about the severity of domestic violence
in this country. We know from all the statistics that have come to light that a
woman on average in Australia is murdered every week by her current or former
partner. Yeah, it's horrible. Yeah. And so this is really the story that you
unfold and the real kind of turning point is in the book is when Lenny Marks one
day receives a letter from the adult parole board, which she doesn't want to
open and her life sort of unravels from that point. But the domestic violence
that you write about so cleverly in this book is something that you sort of
makes the book, it gives it real gravitas. Yeah. So it's interesting because I
knew when I thought of this idea that Lenny had had a very traumatic past and it
wasn't clear to me why at the start. And it's funny because this book, everybody
else knows what's happened to her, but her. So it's kind of a mystery that
unfolds for herself only as she comes to terms with what's going on. And it is
hard to talk about because there's a few spoilers if I go into the detail too
much. But what I really admire in my job is that there's the amount of people
that have had horrific things happen to them. And of course, if there's a
murder, then that primary victim's not there anymore, but it touches so many
people. There's so many people impacted by violent and horrible crimes and then
they have to get up and they have to keep going on with their lives because
there's that moment where it's important to everybody and it might have media
attention. It might be going to court or whatever's happening and then everybody
moves on and they get on with their life. But you are still living with that in
your life. And I just think the resilience and spirit of people to carry on and
remain positive. And if they don't, I'm not saying that that's not still a
perfectly viable thing to do because I don't know how more people don't just
fall down over those things. But yeah, I really wanted it to be a story about a
person that had survived a traumatic event and not so much about the offender or
the offense. Yes. Yes. And you definitely achieved that. I mean, your little
Lenny Marks, when she's just 13 in the book, she really has seen a lot and been
through a lot. She's a spunky little girl who takes matters into her own hands,
that's for sure. But there's one stat that I've looked up, Kerin Mayne, if I can
share, is that more than two thirds of mothers who have children in their care
when they experience violence from their partner said their children had seen or
heard the violence. That's two thirds of mothers have children that witness
these sorts of crimes. That's now in Australia. Yeah. And I think the impact
that must have on people and kids especially in those formative years and must
be so confusing because it's quite often their other parent that's perpetrating
the violence. So you've got these two people that you love and care for in your
life and they're doing horrible things to each other. So just the depths and the
psychology of it all, obviously, I'm not a psychologist. That's not one of my
careers or previous careers. There's so much fallout to it. And it's not as
simple as going, well, it just shouldn't happen because it shouldn't. But that's
not the solution, obviously. Curiously, the police are not the heroes in your
novel, are they? So they're not the ones to solve the crime. They're not the
ones that serve justice. It's actually Lenny that does that. Yes. Well, and I
don't know how far I can go without it being a spoiler. I think if anybody's
listening that hasn't finished it yet, maybe you need to avert your ears. But
yeah, the police pop up in a very benign fashion at the end, which was my
intention. Not because I don't think the police do a good job, but just because
I really wanted Lenny to be her own hero sort of thing. I wanted her to take
charge of her life and take control of what's happened to her. And without
revealing too much, she ends up in a very similar position than that she was in
when she was a much younger and not as confident and strong version of herself.
And I mean strong both physically and mentally. So yeah, it's sort of like a
big, you know, everything does a big 360 and there she is back looking in the
face of exactly where she's come from and she's got to make some big decisions.
Lenny Marks Gets Away With Murder is the book we're talking about. Keren Mayne
is the author and you're on Weekend Night Live with me, Renee Crosh. So some
people have likened the book to Eleanor Oliphant is Completely Fine by Gayle
Honeyman. What do you think about that? Well, look, I love that book. I loved it
too. I adore it. Yeah. So I'm more than happy to have that as a comparison, but
I think as much as it does in some ways sort of hit some of the same vibes, I'd
say it's quite a different story in that I guess I would say Eleanor Oliphant is
not a crime novel and I'm not sure where it was ever shelved, whereas I do think
Lenny Marks is still in a crime category. So I think, yeah, we sort of have a
few similarities, sort of rotate around some of the same sort of ideas and
themes and then fall in slightly different places. Probably not that bad for
your marketing though, is it, to be likened to that? I'll tell you what, like,
you know, I wouldn't mind selling as many as Gayle Honeyman. That'll do. Thanks.
Bit of a hard act to follow though. I'm assuming that's why she's only written
one book because I've been waiting for a follow up from her. I'd love to read
it. Yeah. Yeah. So your characters, you know, it's not just Lenny Marks. There's
a range of characters there, Keren, and they're all so beautifully defined from
Zanny to Maureen to Ned to Faye and all the school teachers in the lunchroom.
Any school teacher that reads this book will completely relate to Amy, that
character in there, and then the lovable Kira. So you really kind of nailed it,
didn't you? You really kind of got inside these characters and were able to sort
of bring them to life for the reader. Well, I hope so. And thank you. I have
apologised. So my oldest child is five and she just started prep or foundation.
So I call it prep in the book and had a panic on the first day of school because
the book had already gone to print. And I messaged my teacher friend and I said,
do they still call it prep? And she said, yeah, yeah, some places do because at
my kids school they call it foundation. Anyway, off on a tangent, I told her
teacher, if she does read my book and I did actually give her a copy of it, I
said the teachers aren't based on the prep teachers aren't based on any real
life prep teachers. Just because I thought I'd hate it for her to think that
there was some traits I've based on, you know, the teachers I've met over there
at the little local primary school. Well, I have been in a teacher's staff room
for lunch and I think you have nailed it because I think, I think there are
those personality types in there. Fantastic. I'm so, I'm so glad to hear that.
That's brilliant. I think some of the staff room things are like universal staff
room things, regardless of your occupation, like people stealing your food
items. I think it has to happen everywhere like that, unless you work by
yourself, you know, it's going to happen everywhere. So. And stealing, stealing
tea bags. Yeah, yeah. Doing, you know, really annoying things like that. So I
think that happens everywhere. But yeah, the dynamic and the personalities I had
a lot of fun with and Ashley and Amy, the prep teachers who Lenny wants to
befriend, but aren't particularly nice to her. I think everybody can see it long
before Lenny sees it. That was so much fun to write. Like a villain is good fun.
I bet. I bet. Well, what's next for you? You've got your debut novel out there.
You're still writing, you're writing a second novel? I am. I am. I had another
idea burning to get out of me. It sounds like I need to go to the doctor, but I
don't. So an idea burning to get out of me. And I've I'm sort of drafting that
now. So I'm just sort of working through it in the in any scraps of time I can
wrangle between feeding babies and putting them to sleep. They're very good
babies, actually. They quite often sleep at the same time. So it means I get a
little bit of time. So, yeah, that one hopefully will be, you know, my next
novel, my next published novel. And yeah, I'll just keep writing anyway, because
I love it. Is it another crime? Crime fiction? It is. This one's a bit crimier.
That's a good word for an author. I'll just make one up. It is a bit crimier.
It's not a sequel. Lenny Martz does not reappear in this book. I'd never say
never. But Lenny's story, I'm pretty happy I told that in that book. She is a
hard character to follow up, though. I am, you know, trying to work in a
character that I like as much as her. But I think with time and they sit with me
for a little while, I'll like them as much. Yeah, it's another crime one set in
Melbourne and yeah, a few different elements. So I will be sure to let everybody
know as soon as I've got something in good condition. Well, congratulations.
It's an excellent, an excellent achievement. I don't know how you do it with
four little children, twins. You're still like looking after them. They're only
several months old and you've got a five year old and what a three year old or
something. Yes, exactly. And you're still working as a police officer. You're
writing. You are the quintessential super mum. So thank you for doing what you
do. I loved the read and keep on it. Thank you so much. Thank you for the
interview. It was wonderful. Sharon Mayne, she is the author of Lenny Marks Gets
Away With Murder or Does She? I'm sure you'll love it. ABC Listen. Our minds are
powerful and complex. Join Sana Kedar to explore everything mental about the
mind, the brain and our behaviour. As you scale this technique up, you can
memorise all kinds of things. From negotiating to addiction to AI, the history
of brainwashing, emotional intelligence, rudeness and humour. How does that
feel? All In The Mind with Sana Kedar. Podcast available now on the ABC Listen
app. You are with Renee Crosh filling in for Suzanne Hill tonight on Weekend
Nightlife and I'll be here for the next three weeks. Thank you for choosing the
ABC and thank you for being here with me tonight. Coming up, a 50 year old
mystery of a Townsville bloke. Sid Ramsey is his name or was his name because he
went missing at sea 50 years ago. Sailing from Cairns to Hong Kong on his 50
foot cutter, the Southern Maiden. What happened to him? And all the intrigue and
speculation that has gone on since his disappearance. We're going to find out
from Tim Elliott, the writer from Good Weekend magazine next up. But given that
we're talking about Australian stories, I thought why not play this great song
from... Of course, you know it. It's Icehouse. It's Great Southern Land. It's
got to be the most iconic Australian song, doesn't it? I think we played it a
lot on Australia Day, but why not give it another round tonight? It's Icehouse.
It's Great Southern Land. It's Icehouse. It's Great Southern Land. It's
Icehouse. It's Great Southern Land. It's Icehouse. It's Great Southern Land.
It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's Icehouse. It's
Icehouse. It's Icehouse. It's Icehouse.