TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-15-09H00M.MP3
--------------------------------------------------------------------------------
 ABC Radio, it's coming up to your news. Stay with me. Chances are you've used
an AI chatbot for something like maybe changing a flight or speaking to your
bank. But many people are turning to AI and chatbots for therapy. But how safe
is that? Having that conversation after the news plus fictional bands. Who's
your favourite? The Monkeys maybe? Let me know. You're on ABC Radio. The news is
next. The Prime Minister was arrested after an explosive object was thrown into
a crowd in western Japan where Prime Minister Fumio Kishida was due to give a
speech. North Asia correspondent James Oden has the latest. Prime Minister Fumio
Kishida was at a fish market in the city of Wakayama to campaign for a local by-
election. He was on his way to give a speech when a loud bang was heard.
Eyewitnesses say a cylindrical object was thrown and white smoke came out after
the explosion. Reports say two local fishermen as well as police officers
tackled a man as bystanders ran from the chaos. Fumio Kishida was evacuated and
is unharmed. There are no reports of injuries. James Oden, ABC News, Tokyo.
Treasurer Jim Chalmers says an independent review of the Reserve Bank will
deliver meaningful recommendations. The review was ordered by the Treasurer last
year and looks at the bank's approach to monetary policy as well as its
accountability and culture. The RBA has been under pressure since Governor
Philip Lowe was forced to apologise for inaccurately predicting interest rate
rises or interest rates were unlikely to rise until 2024 and they then started
to increase much earlier. Mr Chalmers has told the report or has held the report
since last month and has told Bloomberg he plans to release it shortly. Well I
think in order for it to be worthwhile the recommendations need to be robust and
meaningful and they are. And so when I release it, whether it's in the next week
or two, certainly before the budget on May 9, people will see the thought that's
gone into it. Indigenous Australians Minister Linda Burney has offered her
condolences to the family of a woman who died after being allegedly stabbed
outside a Darwin hotel where the minister was staying. Emergency services were
called after receiving information an injured woman had entered the hotel
seeking help. She died a short time later in hospital and a man was arrested at
the scene. In a statement Ms Burney says her staff provided assistance to the
woman inside the hotel while she comforted members of her family. She says her
heartfelt condolences go out to the woman's family and loved ones. A man charged
with the stabbing murder of a paramedic in Sydney remains in custody after
facing court. Leah Harris reports. 21 year old Jordan Finiangonofo did not apply
for bail in Parramatta bail court this morning and it was formally refused until
his case returns to court on June 28. He is accused of fatally stabbing a 29
year old rookie paramedic outside a McDonalds in Campbell Town yesterday. The
paramedic was sitting in the ambulance with a colleague on a break after a night
shift before the alleged unprovoked attack. Finiangonofo's lawyer Jarvid Faiz
told media outside court his client is being treated for mental health issues.
The Victorian Government has failed to rule out delays to Melbourne's planned
airport rail link due to budget pressures. A state government spokesperson won't
confirm reports the project will be delayed but says the upcoming Victorian
budget will take into account rising interest rates and inflation. The
spokesperson says the state government is working in partnership with the
Commonwealth on the rail link. Deputy Opposition Leader David Southwick says the
Andrews government is struggling to contain Victoria's growing debt. We are in a
financial crisis here in Victoria. Our debt has blown out to the largest in the
nation. We're the debt capital of the nation and the big build has become the
big budget blowout that is grinding major projects to a halt. Coles is advising
its credit card holders that historical data has been impacted as part of the
Latitude Financial hack. Coles says it's yet to hear details of the number of
impacted customers or what exactly has been breached. Latitude Financial was a
former service provider to Coles. Coles says credit card holders who have been
impacted will be contacted by Latitude. The cyber incident does not affect Coles
customers, only those who held Coles credit cards prior to March 2018. Legal
challenges are expected to a complete ban on the popular video sharing platform
TikTok, now awaiting the governor's signature in the US state of Montana. Laurie
Butrose has more in this story. Immediately after the Montana legislature
approved the ban, TikTok complained it was a case of egregious government
overreach. The Chinese company also reiterated its claim it does not share users
data with the government in Beijing. Montana would become the first US state to
place a total ban on the app across all devices. The new laws would fine TikTok
if it tried to continue operating in the state and also find third party
platforms such as Google if they don't remove it. A TikTok spokesperson says the
matter will now be pursued in the courts. And you're up to date with the latest
on ABC News. You're listening to ABC Radio. Hello, Lisa Pellegrino with you.
Hope you're doing well. Do you engage in the world of AI? Maybe you used AI
technology to style up your profile pic, you know, those ones that were going
around for a while, where people's faces were slightly tuned into art pieces
that were circling and circulating around online, or maybe you've engaged with a
chatbot along the way, changing a flight, speaking to a financial institution.
You may be surprised to learn that growing numbers of people are turning to AI
chatbots for therapy, but is it safe? And what possible dangers could lurk in
such an interaction? Also coming up on the program in the next hour? What band
is your favourite? Let me rephrase that. What fictional band is your favourite?
The Monkees, one of the most successful fictional bands. Well, fictional bands
are the ones that turn real. They started off in a TV series, became super
popular, and now are a band in their own right that tour, sell lots of albums.
Do you have a favourite fictional band from a movie, TV series or a book? Having
that discussion very soon, it's inspired by the growing popularity of the TV
series Daisy Jones and the Six. Maybe you've seen it. It stars Elvis's
granddaughter, Riley Kehoe. That's all coming up. Plus a little bit later on, I
want you to meet a beautiful guitarist who has a long relationship with the Hush
Foundation, creating music for hospitals and healthcare workers. And his latest
album is a thanks to all the health workers who worked so hard and continue to
do so during the pandemic. This music is played in theatre, in operating
theatres and in hospital wards. And I jumped at the opportunity at the time, I
think, as the first vaccine was being rolled out. I just couldn't believe
watching the news and seeing medical staff being verbally abused and physically
abused. There was so much anger in the community. I thought the last people who
need to be in this position are the healthcare workers. I wanted to say thank
you to them musically. All of the pieces that I wrote for this album, in a way,
they're like individual kind of letters of thanks to all of the extraordinary
health workers around the world that have helped us through these last three or
four years. You hear more of the music and the life of Slava Grigorian, a
stunning guitar player and his latest offering of gratitude to healthcare
workers who continue to be on the front line during the pandemic on ABC radio.
Right now, it's Nairi with Moonshine after that AI therapy. I am a night sky,
deeper blue and blue dye. Some days you can hear me cry. Some days I'm okay but
not all right. I'm shaking fists at these long labor lists, wondering where the
future goes and what I could miss. Oh baby, sweet pie, sweet, sweet pie. Remind
me when I'm used to right. None of us will check the time cause the moonshine
drag up all of my life. Love is shade in a land, in a land not too tight. This
isn't paradise, it's white noises and red eyes. Some days I'll be rolling dice.
Some days I'll be high on my supply. I get my kicks like getting hit by a brick.
Rolling round in circles wondering if this is paradise. None of us will check
the time cause the moonshine drag up all of our lives. Oh baby, sweet pie,
sweet, sweet pie. Remind me when I'm used to right. None of us will check the
time cause the moonshine drag up all of our lives. Love is shade in a land, in a
land not too tight. This isn't paradise, it's white noises and red eyes. None of
us will check the time cause the moonshine drag up all of our lives. Love is
shade in a land not too tight. This isn't paradise, it's white noises and red
eyes. None of us will check the time cause the moonshine drag up all of our
lives. Love is shade in a land, in a land not too tight. This isn't paradise,
it's white noises and red eyes. None of us will check the time cause the
moonshine drag up all of our lives. Love is shade in a land not too tight. This
isn't paradise, it's white noises and red eyes. None of us will check the time
cause the moonshine drag up all of our lives. Love is shade in a land not too
tight. This isn't paradise, it's white noises and red eyes. None of us will
check the time cause the moonshine drag up all of our lives. Love is shade in a
land not too tight. This isn't paradise, it's white noises and red eyes. None of
us will check the time cause the moonshine drag up all of our lives. Love is
shade in a land not too tight. This isn't paradise, it's white noises and red
eyes. None of us will check the time cause the moonshine drag up all of our
lives. Love is shade in a land not too tight. This isn't paradise, it's white
noises and red eyes. None of us will check the time cause the moonshine drag up
all of our lives. Love is shade in a land not too tight. This isn't paradise,
it's white noises and red eyes. None of us will check the time cause the
moonshine drag up all of our lives. Well you say pseudo therapy but are the
people receiving this advice or having these interactions, are they seeing it as
pseudo or as quite legitimate valid ways to engage with a therapist? Well first
of all we have to understand that it is not a therapist. There is no person or
actual therapist behind these AI chatbots. So unfortunately what's happening is
these chatbots are gathering information from various sources. Some might be
evidence based and unfortunately some might not be and then they're using and
synthesizing this information to then give back to the people who are using
these chatbots. So we don't know whether or not the information that is being
given to these users is actually going to be healthy or helpful or even in some
cases safe. With these AI chatbots, particularly things like chat GPT, there
wouldn't really be any monitoring or guidelines around this. There are some
chatbots that will have statements written by their producers and developers who
are maybe putting it out there that you know please remember that these chatbots
are not people, they are not therapists, they can't give actual advice but in
some cases including chat GPT there is no statement as far as I'm aware in
regards to mental health care. Why are people going to AI chatbots? What have
been some of the reasons? From my understanding it's either for some people when
they have not accessed a psychologist or an adequately trained mental health
professional and maybe they're wanting to dip their toe into getting some sort
of help but it isn't really through some of the more conventional channels. I
think a lot of our lives at the moment are really merging into this online space
and this seems to be just another way of accessing a service through this online
space. What we're also seeing in a lot of the reports coming out now is people
who are unable to afford adequate mental health care particularly in places like
the US are turning to AI chatbots because they don't feel like they could invest
the time or the money into receiving the mental health care that they need. So
as a mental health professional you touched on some of the concerns but what are
the other concerns that you have about people using AI chatbots for their mental
health concerns as therapy? So the main risk that we see as mental health
professionals is that AI does not have the ability as it is at the moment to
triage risk. So if the AI is being used by someone who might be presenting with
let's say self-harming or suicidal thoughts and they're typing this into the
chat service the AI is not then able to refer onto a local adequate mental
health service who might then be able to support that individual. And in fact
we're not sure what kinds of responses that AI chatbot might be giving to people
who are in quite a lot of distress. So if there is a risk we're concerned that
they might not actually be receiving the kind of help that they need. The risk
is a massive issue. Yeah what if a chat box gives information or advice that is
potentially harmful or even fatal? Well at the moment I believe there has been
at least one case of this occurring and what is happening at the moment is a lot
of these developers are needing to review their protocols about how these
products and services are being used and there's a lot of legal liability that
might actually fall on the developers of these AI chatbots when they are
potentially giving out risky or even dangerous advice or information. Where it
sort of fits in in Australia at the moment is if there are people who are using
these sorts of services we would always recommend that there be a warning label
or some sort of recommendation label that says if you are experiencing distress
that you go and see a GP or you go and see a psychologist who is then able to
triage that risk. And you have to be quite careful don't you because I was
reading an article I think it was about how somebody was engaging with an AI
chat box and for therapy reasons and they were asking something along the lines
of are you a professional or do you know what you're talking about and the chat
box said back oh I have a couple of masters degree and so forth but then there
was a disclaimer at the top saying these characters everything they say is
fictional. Absolutely and I think that you know we never really read all of the
terms and conditions when we're going into any kind of site or purchase
agreement or any kind of service agreement and it becomes really tricky when
someone is relying on a service that perhaps they don't actually fully
comprehend the terms and conditions of that service. So if they truly believe
that they are talking to an individual or perhaps they have been speaking to
this AI chat bot and it becomes almost a pseudo personal relationship with this
chat bot then it becomes quite murky territory and we need to make sure that
these sorts of disclaimers are put front and centre so that people can
understand that this is actually just automatically generated words that may or
may not have any veracity. Do you think that AI could never replace a human
therapist? I would say that there could potentially be scope for this and I know
that there are several platforms that are available at the moment whether they
are counselling services or coaching services that are utilising AI chat bots
but what I would say is that there needs to be quite a lot of checks and
balances and most importantly quality control and assurance. So for instance
there are services such as Beyond Blue and Headspace who do chat therapy and
that might be over SMS or that might be over a text, a chat service online but
there is always another person on the other end of that and so if there is a
sense of risk the therapist or the counsellor on the other end of that can then
refer that person on to the manager or the team leader who is then able to
provide additional support in the cases of risk. And with a human as well there
are so many nuances you do pick up when you are speaking with somebody it could
be their tone, if you get to see them face to face or even through online with
video, a lot of counsellors and therapists can go through videos online and you
get a sense of one another. Absolutely and you know what at the end of the day
humans are social creatures, we need to have that emotional connection, we need
to be able to really see each other and understand each other not just through
our words either verbally or on a page or on a screen, we need to be
understanding things like tone of voice and facial expressions and matching
those sorts of things with what the person is saying so if the person is saying
something that is you know I'm fine and everything's okay and my life is okay at
the moment but their tone of voice might be sad and they're not making eye
contact and their facial expression might be indicating that something else is
wrong then we need to be putting all of those clues together in order to make an
accurate assessment of what that person might need and the kind of supports that
we're able to provide for them. Sarah, what's your advice for anyone needing
mental health support and is navigating that perhaps for the first time? I would
absolutely suggest that the person contacts first of all their GP in order to
get a referral and mental health care plan to come and see a psychologist. I
would also say that absolutely it is really challenging at the moment to either
access a psychologist or even afford sessions for a lot of people so we do need
to be mindful of the cost factors. So getting that referral and that mental
health care plan will absolutely make it easier by having those Medicare
subsidized sessions. If we are not able to access and afford mental health
support then we do need to be looking at who else is around us who are able to
provide us with the kinds of support that we need. So for instance trusted
friends or family members who we're able to talk openly with and even going and
using phone services like Lifeline and Beyond Blue so that we can have that
experience of having an empathetic understanding human on the other end of that
line. And some of the numbers for those organizations you mentioned Lifeline 13
11 14 if you'd like to contact them. There's Kids Help Line as well 1 800 55 1
800 for young people aged up to 25 years old and of course BeyondBlue.org.au you
can also call them on 1300 22 46 36. Sarah Odawati is a practicing psychologist
and the director of the Australian Association of Psychologists Incorporated.
Thank you so much for taking some time out and I can assure everybody you are
not a chatbot. You are definitely a human that we've called up on the phone. Can
you guarantee that for us? Absolutely. Thank you so much for having me. Thank
you very much. Take care. You're listening to ABC Radio. Lisa Pellegrino with
you. Hope you're having a lovely Saturday. Coming up very soon fictional bands.
So bands that got their start on a TV show, maybe a movie, they might even be a
fictional band from a book you love. I would love to hear your absolute
favourite. You may be watching the new TV series Daisy Jones and the Six
following the rise and fall of the most influential band of the 70s Daisy Jones
and the Six. Have you heard of them? Well no you haven't because they don't
exist. Daisy Jones and the Six are a fictional band with a familiar rock and
roll story but despite not being a real band the actors they actually had to
undergo months of band camp where they learnt to sing and play their
instruments. We all got cast just before the pandemic and at that point we all
had a different distance to go. Like some of us had no experience, some of us
knew a little bit, some of us knew a lot and in all honesty I think one of the
best things to have ever happened to this production was the fact that we had
like a year and a half delay and it meant that we could all get up to speed and
get to the same page. So when we kind of finally came back together for like
this intensive sort of three months band camp before we started filming that
consisted of band rehearsals where we'd have individual lessons and then we'd
all come back and have a jam and sort of play through the songs and then me and
Riley would be singing. I think the first time I'm remembering now that I heard
the music the context was you need to sing this so it was just anxiety. I was
literally about to say exactly the same thing. This is too high. I was like this
is really, I was like how do I, I don't know if I can do it. I remember having
that conversation once going okay well we'll think we'll get someone else to
maybe have a stunt vocalist is what they said. Yeah and do it for us or
something. I was like what does that mean? And I think that that really inspired
me to work harder. They wanted us to work on our singing chemistry very early
on. From day one. I think from day one and so we just kind of met and then we're
like put face to face and they're like singing at each other. Lead actors and
lead singers of Daisy Jones and the Sixth that's Riley Keough and Sam Cleffelin.
We'll learn more about that series and other fictional bands throughout history
in just a moment but before that one of the most successful fictional bands
turned real life amazing artists who are very popular to this day. It's the
Monkees. Daydream Believer on ABC Radio. Oh I could hide beneath the wings of
the bluebird as she sings. The six o'clock alarm would never ring. But it rings
and I rise. Wipe the sleep out of my eyes. No shaven razor's cold and it stings.
Cheer up sleepy G. What can it mean to a daydream believer and a homecoming
queen? You once thought of me as a white knight on his steed. Now you know how
happy I can be. Oh and our good times start some day without dollar one to
spend. But how much baby do they need? Cheer up sleepy G. Oh what can it mean to
a daydream believer and a homecoming queen? Cheer up sleepy G. Oh what can it
mean to a daydream believer and a homecoming queen? Cheer up sleepy G. Oh what
can it mean to a daydream believer and a homecoming queen? Cheer up sleepy G. Oh
what can it mean to a daydream believer and a homecoming queen? Oh I love that
song. It always just makes me feel so good. I hope you love it too. A Daydream
Believer by The Monkees chatting about fictional bands turn real life success
stories next. We are one. But we are many. And from all the lands where we come.
We share a dream. As thing with one heart. I am. You are. We are Australian. We
are Australian. You are. You are listening to ABC Radio. Social media is buzzing
over the new TV show Daisy Jones and the Six. Based on the best selling book by
Taylor Jenkins Reid and loosely inspired by Fleetwood Mac, Daisy Jones and the
Six follows the rapid rise and fall of a fictional six piece band in the 70s.
The band is fronted by Daisy Jones played by Elvis's granddaughter Riley Kehoe
and Billy Doon played by British actor Sam Claflin. Look I know that I said that
I would tell you everything. But how much of everything do you really want to
know? You regret me and I regret you. You couldn't handle your liquor and you
can't see the end of the truth. You write songs about who you wish you were, not
who you are. What if he wrote songs about the guy that maybe wants things that
he shouldn't? Who'd want to hear a song about that? I think everybody would. Go
ahead and regret me but I always want you to regret you. Go ahead and regret me,
go ahead and regret me, go ahead and regret me. Go ahead and regret me but I'm
beating you to a dude. The fictional band have released a very real album called
Aurora. It's been composed by music industry heavyweights like Blake Mills,
Phoebe Bridges and Marcus Mumford and the songs are performed by the cast. But
how's this? The album has shot up the music charts peaking to number one on the
US soundtrack chart, number two on the Billboard Americana folk album chart and
it's also the first fictional band to top the iTunes chart. Now production
wrapped up months ago and yet the cast or band should I say are continuing to
rehearse sparking rumours that Daisy Jones and the Six are getting ready to
tour. But would you buy tickets to see a fake band perform live? Well it's not
the first time a fictional act has gone on to succeed in the music business.
Patrick Lenton joins you. He's the deputy arts and culture editor at The
Conversation talking about fictional music acts turn real. Hello Patrick. Hello,
thank you so much for having me. Have you watched Daisy and the Six yet? Oh yes,
I loved it. I was a big fan of the book originally and then I was very keen to
watch the TV show as well. I wonder how many people have done both because when
I didn't know of the book before but I saw the series and then I started looking
on social media on YouTube and there's a long history of people adoring the
book. Yeah, I think the book was so popular that it's off the back of that that
the TV show kind of became. What's the premise of the show? It's about a band in
the 70s. It's a classic kind of rise and fall, you know, talented kids, are they
going to make it? But then it is loosely based off Fleetwood Mac. So as you can
imagine, it's not just hooray, they're very successful. They're very successful,
but there's a lot of inter-band drama, romance, scandal and it really explores
that and gives it a very unique or 70s vibe, I guess. And who's in the cast? Oh,
we've got Riley Keough as who plays the titular Daisy Jones and then Sam Claflin
as Billy Dunn who's the, I guess the love interest really. And Riley Keough, a
lot of people are very excited she's in it because not only is she a great
actress but she comes from rock and roll royalty. Does she? Yes, she's Elvis's
granddaughter. Oh gosh, I had forgotten about that. Yes. But for a second there,
because she looks just like Florence from Florence and the Machine. Yes. And I
got so confused there, I thought maybe she is related to Florence. She's Elvis's
granddaughter and it's been interesting, I've watched a lot of the chat show
interviews that her and the cast have been on and some people they focus on that
fully but there's quite a few that don't even mention it and I think she really
appreciates that in that interview space when she's concentrated on because
she's an actress, she's a director, she does a lot of her own stuff, well all of
her own stuff. And interestingly, this is actually her first entry into singing,
as well as the other singer in the band, they've never sung a note before. Did
you know that? Yeah, I did know that and it's fascinating because they're
excellent, they are really good. Yeah, can you tell us about the band camp that
the cast had to go into? Well, from what I know, they, because they really made
a decision with this show to actually focus on the music which I think is not
just smart but necessary because the, you know, it's all about this band
becoming super super famous, super super successful. And I remember in the book,
I remember thinking oh it's so smart that, you know, that this book has written
about, you know, a fake band because all they have to do is say it was the best
song that anyone had ever heard, and then they don't have to actually show that
because it's writing. Whereas with the TV show they were like right, well we're
actually going to have to have the music in this show and it's got to be good
otherwise we're going to look ridiculous. So they did actually put a significant
amount of time taking them off to a band camp to work on, you know, not just
their own vocal performances, but like singing in character, which I think is a
really interesting challenge for an actor. Yeah, the two lead characters and the
two lead singers, they both were not singers at all. Riley Keough says strongly,
I could not sing at all before. And you see them, they're singing, it's actually
them, they're amazing. The other band members, they're all musicians but some of
them I understand that the instrument their characters are playing is not their
usual instrument so some of them even have to learn another instrument. That's
right. They, I believe they spent around 18 months working on just the music,
which is crazy and yeah and they all learned, like fully learned how to play
their instruments so drums, bass, all that, it's amazing. Here's a taste of that
very music. I don't know who I am. Is it out of our hands. Tell me tell me tell
me We made it this far. Patrick, are you surprised to hear of the band and the
soundtracks chart success I mean my goodness it has gone so well. I, I'm
pleased, I think that if I think that it's a sign that they focused on the right
things with this show. I have been surprised by how massively successful it's
been I've even seen people online, talking about loving some of the songs and
being like, who's the like who's the band, and then finding out that they're
actually interested in a fictional band you know it's very it's very interesting
meta textual. Yeah well Daisy Jones and the Six they're not the first fictional
band to turn real are they though there's been quite a few before them that have
become really popular. Oh yeah absolutely I mean, I guess we could talk about
Spinal Tap. The Blues Brothers. I think the band that was almost famous, you
know, had a bit of success as well. Oh did they I never actually heard any of
their music. I mean it was before social media was what it is today and and
Spotify I hadn't heard their music making it out there. I think it was more that
it was sort of pulled out as a, as a good example of like authentic depictions
of music you know rather than, you know, doing the charts like, like the
fictional Daisy Jones and the Six has. But a good example is the TV show
Nashville did you ever watch that? No. Oh, fantastic, highly recommend as a long
running sort of you know drama soap ish sort of style TV show that finished in
2018, and it was set in the in the Nashville country music sort of world so we
had like Hayden Panettieri playing a kind of almost like a sort of pseudo Taylor
Swift style figure a little bit more of a diva, and, and they do all their music
in the show and created so many soundtracks I think something close to like 12
soundtracks for the show, all of which did really really well and have
consistently, you know, done well on Spotify and even had songs chart on
Billboard. And they've also toured the music and are still touring the music the
cast is still out there even though show finished in 2018 still out there
touring the songs from the show. Well it sounds like Daisy and the Six are
looking to tour I'm only guessing because they're still rehearsing from what I
hear and I even saw in an interview with Riley Keough, Daisy Jones, and she said
when she was auditioning for the part they said to her, Would you tour? And she
said yes. And then she said to the interviewer, you know because you lie to get
jobs, but it seems like they might actually be touring. There's one band, of
course, a fictional band who I know they're a fictional band but they've become
such a huge music success that I find it hard to call them fictional. The
Monkees, they are a band, they were created for a TV series but I mean, the
Monkees have stood the test of time. Oh, so much so that I remember it was,
well, I was about to say a few years ago but probably a decade ago that I was, I
was listening to that song and had no concept that that it was from a fictional
band, I just assumed that they were a band, because you know I was, I never saw
the show or anything so how was I meant to know? Yeah, and I understand that in
the early days though, when they first were releasing music, they weren't taking
that seriously and people would make fun of them a bit. I think it was their
vocals and session musicians and, you know, people weren't buying it. Yeah, and
I think that there was a, like, they were considered almost like a bit of, not a
parody but a facsimile of, you know, the Beatles. And so, so there was kind of
this idea that they were maybe a bit of an Aldi version. Speaking of parodies,
there is one band you mentioned. The vinyl tap sets out to be a fictional band.
You know that it's a mockumentary, but as you said they got a bit of success
too. That's right. I love how they're, they characterise themselves as one of
England's loudest bands. Not even the best, not even the best, just one of the
loudest. Now, you were talking about other bands and like the Nashville show
bands that have really got success but there's quite a few that haven't as well.
There's a lot of TV series. I remember watching a lot of those American Saved by
the Bell type shows in my early days and every now and then one of those shows
the members would form a band, Degrassi Junior High, Zit Remedy, for example.
Yep, yep. There was Zach Attack, which was Saved by the Bell. I thought, am I
imagining that they had a band? They did have a band. Yeah. Yeah, and what do
you think it was about those bands that didn't quite make it? I mean, I think
that probably knowing how those shows sort of worked, which was, you know,
running off the smell of an oily rag and kind of, you know, filming as many
episodes as they could so that their teen cast members didn't suddenly turn into
adults and make it all look ridiculous. I think they just didn't have the time
to really, you know, invest in good music and, you know, writing really, really
good songs and having good performances. Well, if you ask me, this particular
song from Degrassi, the Zit Remedy, Everybody Wants Something, is a song that I
think was a missed opportunity for the world not to adore and to carry on. They
even did a film clip in the time before TikTok and YouTube. They did it on VHS.
Everybody get ready and get into gear. Degrassi's sensation's the one and only.
Zit Remedy is here. ABC presenter Jacinta Parsons and I have talked for years
about actually performing this live one day. Yeah, that's a good idea. She's got
the guitar, though. She's got musical talent behind her. That's the only
difference with me. So with Daisy Jones and The Six, how likely do you think it
is they're going to be announcing a tour? I think it's really likely, to be
honest. You know, there's been so many clues and hints that this is something
that they're going to consider doing. And I think that there's almost a bit of a
sunk cost fallacy going on here where they've spent so much time becoming good
musicians and working on the music for the show and they have this amazing
producer who co-wrote all of them and Marcus Mumford involved. I think that they
have to do something else with it and take it down the road just for the sake.
Out of all the fictional bands you mentioned today, is there a personal
favourite with you? Well, I still listen to the Nashville soundtrack quite a
lot, actually. It's become like a regular part of my listening rotation. And
they have two of the cast members. They're called Maisie and Lennon. And people
might know them outside of the show because that's their real names. And they're
successful musicians now. They sort of got their start on Nashville. So
technically, when I listen to their folk music, which I really enjoy, I'm kind
of listening to a fictional band. I'm even thinking Glee. We didn't mention
them. Can we say they're a fictional band in a sense or a fictional singing
group? Because that TV show, they toured, they have songs that you can listen on
Spotify and all the rest of it. Yeah, that's true. There's a lot of people who
still prefer the Glee version of some songs rather than the original, because
that's the first time they heard it or the most times that they heard it because
they only do covers, really. So yeah, I reckon we could count them. Let's just
add them into the mix. Patrick Lenton, Deputy Arts and Culture Editor at The
Conversation, thank you so much. Thank you very much. And here is one of the
songs from Daisy Jones and the Six, Honeycomb, Look at Us Now on ABC Radio. Tell
me, tell me, tell me How we made it this far Did we unravel a long time ago Is
there too much we don't wanna know I wish it was easy but it isn't so Oh, we
could make a good thing bad Oh, we could make a good thing bad Now where do we
stand Baby, baby, baby No one knows who you are And if this was your plan Tell
me, tell me why You've been crying in the dark We unraveled a long time ago We
lost and we couldn't let it go I wish it was easy but it isn't so So we could
make a good thing bad So, baby Oh, we could make a good thing bad Oh, we could
make a good thing bad Oh, we could make a good thing bad Oh, we could make a
good thing bad How do we get here How do we get out We used to be something to
see Oh, baby, look at us now Oh, baby, look at us now This thing we've been
doing ain't working out Why can't you just admit it to me Oh, baby, look at us
now Oh, baby, look at us now Oh, baby, look at us now Oh, baby, look at us now
Oh, baby, look at us now How do we get here How do we get out This thing we've
been doing ain't working out Oh, how do we get here How do we get out We used to
be something to see Oh, baby, look at us now Baby, look at us now This thing
we've been doing ain't working out Why can't you just admit it to me Oh, baby,
look at us now Baby, look at us now How do we get here How do we get out We used
to be something to see Oh, baby, look at us now Baby, look at us now This thing
we've been doing ain't working out Why can't you just admit it to me Oh, baby,
look at us now Baby, look at us now Oh, we can make a good thing bad Oh, we can
make a good thing bad Oh, baby, look at us now Baby, look at us now Your picture
perfect blue Sunbathing on the moon Stars shining in your bones illuminate First
kiss just like a drug Under your influence Take me over you with magic in my
veins This must be love No silver or no gold Could just be up so clear Hear the
glitter in the darkness of my world Just tell me what to do I'll fall right into
you Going under cast a spell just say the word I feel alone Boom clap sound in
my heart the beat goes on and on and on and on Boom clap make me feel good come
onto me Come onto me now Boom clap sound in my heart the beat goes on and on and
on and on Won't grab the thing that's good Come onto me, come onto me now You
are the light that I will follow You let me lose my shadow You are the sun, the
glowing halo You keep burning me up with all your love Boom, clap, sound in my
heart The beat goes on and on and on and on Boom, clap, make the thing good Come
onto me, come onto me now Boom, clap, sound in my heart The beat goes on and on
and on and on Boom, clap, make the thing good Come onto me, come onto me now Now