TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-17-00H00M.MP3
--------------------------------------------------------------------------------
 Brad, what's going on? At Lovewich, no delays, had an early breakdown, Lovewich
Road outbound, that's just been cleared from the right turn lane at Norman
Avenue. Lights are out, I should say, at Ashgrove, Stewart Road and Elemanta
Drive and the M1 Northbound has been recovering from all-mode to Logan Home
after a really crash before the Logan River Bridge. Bruce Highway has been slow
from Deception Bay through to Mango Hill, had an early crash in the right
shoulder southbound near Boundary Road back at 8.30. And don't forget the school
zones, the start of Term 2 today, this Monday the 17th of April. News is next,
it's 8 o'clock. Have you downloaded the ABC Listen app yet? Make sure you tap
the heart and make ABC Radio Brisbane one of your favourites. We are one of your
favourites, aren't we? ABC News with Matt Eaton. A mass shooting at an Alabama
birthday party has killed four people and injured at least 15 teenagers. Here's
North America correspondent Carrington Clark. Investigators are gathering
evidence at an Alabama dance studio after a mass shooting at a teenager's
birthday party. The attack took place at around 10.30pm in Dadeville. It is
unclear what the motive was. The White House says President Joe Biden has been
briefed on the attack. The shooting occurred within weeks of two high-profile
mass shootings in the nearby states of Tennessee and Kentucky, which prompted
local leaders to call for tight gun control measures last week. It's the 163rd
mass shooting in the United States this year, according to the Gun Violence
Archive. Carrington Clark, ABC News, Washington. The aged care industry's peak
body is asking for more leniency from the Federal Government as it enforces new
staffing rules in aged care homes. From July 1, all aged care homes will be
required to have a registered nurse on duty 24 hours a day, seven days a week.
The Government's already acknowledged some homes will miss the deadline and
exemptions are being offered to very small facilities in remote areas. But Tom
Simonson from the Aged and Community Care Providers Association says broader
exemptions will be needed. We need the Government and the regulator to be really
cognisant of the fact that we've got a workforce shortage across this whole
country, across every sector, not just aged care. So conjuring registered nurses
out of the air is not something that we can do. A Sunshine Coast aged care home
says it has up to 20 positions vacant because would-be staff members simply
can't afford to live in the region. More from Owen Jarks. St Vincent's Care
Maroochydore supports 130 residents and has a staff of more than 200. Chief
Executive Lincoln Hopper says more staff are needed but many can't afford a
home. Currently got about 100 vacant shifts there per fortnight that we just
can't fill. A major report from the Everybody's Home campaign this month found
essential workers on the Sunshine and Gold Coasts were spending more on rent
than any other part of the state. Report author May Azizis says workers are
struggling to survive. They'd be spending upwards of 80% of their income just to
get the average unit rent. The campaign calls for more federal investment in
affordable housing. A Senate inquiry reports into a bill that would freeze
billions of dollars in student debts will be tabled in Federal Parliament today.
Lexi Hamilton-Smith explains. More than three million Australian graduates owe
more than $74 billion in HECS loans. They could potentially face up to a 7% hike
to that in June this year due to spiking inflation. The Australian Greens have
proposed a bill to abolish indexing on HECS debts saying it's unsustainable. A
Senate inquiry report into the bill is set to be tabled in Parliament today. The
inquiry received more than 60 submissions from student bodies and graduates
detailing hardship. The Federal Government says it's not considering a pause or
freezing of HECS indexation ahead of the May budget. Education advocates are
calling for better regulation of a $170 million Commonwealth scheme designed to
support Indigenous children's study away from home. An ABC investigation has
unearthed allegations of abuse, assault and neglect at one student hostel in
Outback Queensland. Around 280 private hostels and boarding providers around the
country receive funding through AB study but there's no mandated qualification
for workers who supervise children and providers are not required to meet
legislated standards of care. Wiradjuri woman and Queensland University of
Technology researcher Jessa Rogers says the lack of oversight is concerning.
When you consider how long a young person is actually away from home during that
formative time of their youth it is a massive responsibility and it is up to
those facilities and boarding schools and boarding homes to communicate with
families to take care of those kids and to provide an excellent experience for
those young people. In finance the local share market is expected to open higher
with the ASX SPY200 futures up 0.1 of a percent. That's despite a drop on Wall
Street in the US on Friday with the S&P 500, Dow Jones and Nasdaq all ending the
week lower. The Aussie dollar's lower at 67 US cents. The weather isolated
showers south of Mackay. Shows and storms in the central and northern interior
widespread at times over Cape York and the tropical east coast north of Bowen.
Cairns, Mackay and Bundaberg are top of 30 today. Townsville 29. Rockhampton 31.
Mount Isa 28. In the southeast partly cloudy with gusty northerly winds and
showers developing on the sunny coast. Brisbane and Logan 30. Ipswich 31. The
Gold Coast 27. The Sunshine Coast 29. ABC News. Good morning, welcome to AM. I'm
David Lipson coming to you from Gadigal land in Sydney. On the program today
Aussie is losing more money than ever to scams. Encouraging people to part with
large sums of money to make what is a scam investment that is where we see the
highest level of losses. And relief in WA's northwest as the cleanup from
cyclone Isla continues. We lost a few tanks and solar pumps over close to the
coast and on the Pardoo boundary but apart from that the buildings are fine.
It's just garden damage. You know it's a huge relief and we've absolutely dodged
a bullet. Thanks for your company. The groups campaigning for a no vote in this
year's referendum on an Indigenous voice to parliament are launching an
advertising campaign today. They oppose enshrining an independent advisory body
for First Nations people in the constitution. Here's a snippet from one of the
ads. Will an Indigenous voice to parliament create less violence and crime? Will
it provide better educational outcomes? Will it help overcome poverty? And will
it assist in closing the gap? Vote no. Vote no. The launch comes as another high
profile yes campaigner, marathon runner and former federal Liberal MP Pat Farmer
sets off on a 14,000 kilometre run around Australia to build support for the
voice. The Prime Minister is farewelling him in Hobart this morning and I spoke
to Anthony Albanese earlier. Prime Minister, thanks for your time. The no case
has launched its advertising campaign with the slogan don't know, vote no. Is
that a sign that the yes side has a lot of work to do explaining how the voice
will actually help Indigenous people in practical terms? Well it's a sign that
the no campaign will be negative. What we are doing is promoting a very positive
campaign and today Pat Farmer's run at the launch with some 14,000 kilometres
he's going to run between now and the 11th of October, 80 kilometres a day in
the run for the voice to be launched by myself and Tasmanian Premier Jeremy
Rockliffe is one of the positive ways in which we'll be getting out there on the
ground to the community and it will be spooked by members of the community,
members of the Indigenous community as well as people of goodwill such as Pat
Farmer the former Liberal member for MacArthur and I pay tribute to him for his
commitment to the voice. You've acknowledged Julian Lees's principled decision
to quit the Liberal front bench and campaign for a yes vote. He's still
concerned though the referendum could fail unless there's some changes to the
proposal. Will you consider any changes? Well we have the parliamentary process
which is considering the evidence before it but on the first day it heard from
eminent lawyers such as Brett Walker and others who believe that the wording
that has been promoted in the legislation together when read with the second
reading speech and explanatory memorandum make it very clear the legal soundness
of the proposition which has been put forward but we'll consider the
parliamentary process it's unfortunate that the National Party decided last year
before they even knew what the question was that they'd vote no and the Liberal
Party pre-empted the process as well of course and I do pay tribute to Julian
Lees for the principled stand that he has made. He is someone who's been a long-
term advocate of the voice and he'll be a powerful voice out there in support of
a yes vote come the end of the year as will Ken Wyde of course the former
Minister for Aboriginal Affairs. So there will be this parliamentary process as
you say can you rule out the prospect of the reference to executive government
being removed? Well we'll have the process which is there but of course
executive government has been spoken about for a long period of time including
by Mr Lisa and some constitutional conservatives themselves always spoke about
that. In Australia's system of government of course it's very different from the
US system. In Australia the executive derives its power from the parliament. We
have a Westminster system of government in this country and what the reference
to parliament and executive government is about is making sure that you can get
early representation made. Of course it doesn't change the fact that the
parliament is primary. That's very clear in the third clause that's been put
forward and it's also very clear that what this is about is just recognition and
consultation that we should recognise First Nations people in our constitution
and secondly that where matters affect them they should be consulted because
that will lead to better outcomes. The voice is the means to the end. The end is
about closing the gap in health, education, economic outcomes, incarceration
rates, all of those areas which show that Aboriginal and Torres Strait Islander
people are the most disadvantaged group in Australian society and we need to
address that in order to move forward with reconciliation. In the shorter term
though will the government look into the concerns raised by Peter Dutton and
Jacinta Numpagimpa Price in Alice Springs last week about the alleged abuse of
Indigenous children there?