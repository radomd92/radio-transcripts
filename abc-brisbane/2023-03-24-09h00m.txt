TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-03-24-09H00M.MP3
--------------------------------------------------------------------------------
 ABC News, good evening, I'm Toby Jers-Lewis. A new chapter in Queensland
sporting history begins in Brisbane in just under an hour when the Broncos and
Dolphins clash for the first time in the NRL. The so-called Battle of Brisbane
has officially sold out Lang Park with more than 50,000 people expected to
attend. The teams sit first and second on the NRL ladder and both are undefeated
this season. Sparks are expected to fly but Dolphins CEO Terry Reader says the
NRL's newest franchise is more than ready. When we run out tonight in front of
52,000, every seat sold, you know, the atmosphere will be brilliant and we can't
be any more excited about it. Brisbane City Council has approved the
construction of a controversial 15-storey luxury apartment tower on the same
grounds as one of the city's oldest homes. The high-rise building will be built
next to heritage-listed Shaftesdon House on the Brisbane River at Kangaroo
Point. The estate was brought by a property developer for $15 million in 2020.
Some local residents and historians have campaigned against the development but
it's now been approved by both the State, Government and Council. Experts are
divided over the potential for the referendum question on an Indigenous voice to
Parliament to lead to High Court challenges. Some are concerned the proposed
inclusion of the words Executive Government will result in legal challenges if
the Government were to not consult the proposed voice on a decision. Australians
will vote this year on whether a voice should be enshrined in the Constitution.
Constitutional lawyer Professor Anne Toomey says she was advised by the
Attorney-General that the proposed final wording has the same intent as previous
iterations. To allow Parliament to make laws with respect to how those
representations are dealt with by the Executive, whether or not they're required
to take them into consideration or not. New documents have been released
revealing former Liberal staffer Bruce Lambin told his former employer, British
American Tobacco, he didn't think he was the person referred to in interviews
with Brittany Higgins when she alleged she'd been raped at Parliament House.
Elizabeth Byrne reports. The documents were released by the Federal Court which
is considering if it should grant Bruce Lambin an extension of time to lodge
defamation action against media groups and journalists who ran the interviews.
Mr Lambin's trial over the alleged offence was abandoned and there are no
findings against him. He has maintained his innocence. The released documents
include notes of a meeting with Mr Lambin on the day the interviews were
published saying he read the story but had no idea it was referring to him.
After the meeting Mr Lambin was suspended on full pay. Overseas. A fugitive
cryptocurrency boss behind the failed TerraUSD and Luna tokens has been arrested
in Montenegro. US authorities have accused DoKwon of major securities fraud
resulting in nearly $60 billion of losses. South Korean police had issued a
warrant for Mr Kwon's arrest last September. Officials are reportedly working on
repatriating him to South Korea. There are calls for Europe to immediately ramp
up the production of military equipment to help Ukraine repel invading Russian
forces. The EU has pledged to provide Kiev troops with one million rounds of
artillery in the next 12 months. The European Commission President Ursula von
der Leyen says there can't be any more delays in providing help to Ukraine's
frontline forces. We will continue to provide all support we can to Ukraine.
With that we help keep Ukraine running while it is defending itself and we are
once again increasing our military equipment support to Ukraine. Back home.
Australia's drugs regulator says an overwhelming number of health groups and
researchers support tighter border controls for nicotine vaping products. The
Therapeutic Goods Administration has published nearly 4,000 submissions relating
to the products. Emeritus Professor Simon Chapman from Sydney University's
School of Public Health says vapes are too easily bought from convenience stores
and online marketplaces. This is why it's failed, the prescriptions scheme has
failed, because the other side of it, they're cutting down, bringing stuff into
the country and at industrial levels has not been addressed. So that's
absolutely essential to making the scheme work. To Queensland's weather.
Isolated showers along the exposed tropical east coast tomorrow. And isolated
showers and thunderstorms in central and southeast Queensland, possible severe
thunderstorms near the New South Wales border. Brisbane 21 to 31. This is ABC
News. This is ABC Radio Brisbane with Steve Austin. Broadcasting live from the
northern end of Lane Park. Ahead of the inaugural battle for Brisbane. Broncos v
Dolphins. Five minutes past six on ABC Radio Brisbane. I'd give you the
temperature but I can just tell you it's perfect. The sun's gone down, there's
just a little breeze in the air, just enough warmth to let you know it's warm,
just enough humidity to let you know it's Queensland and just 55 minutes before
kick off for the battle for Brisbane. In just a moment, Australian Rugby League
Commissioner Kate Jones is our special guest. But let's check the roads around
the city first of all. ABC Radio Brisbane traffic. Thank you for that Steve. Got
a multi-vehicle crash northbound on Onaga Road just after Newmarket Road at
Newmarket. Slight delays there and also delays on Waterworks Road at The Gap
with reports of a crash heading westbound there. Very heavy going inbound from
Woolloongabba through the Riverside Expressway. I'd say many off for the 40
Broncos and Dolphins. And intercity bypass got 10 minute delays as well. Got
some very heavy delays still on the M1 up through Yatla from Hellensvale on the
Gold Coast. Centenary Highway northbound Darra up to the Jindalee Bridge. Still
a 10 minute delay there. Meantime the Bruce Highway southbound Mango Hill,
little bit under speed and Gympie Road Chermside's in busy conditions also. From
the Australian Traffic Network, I'm Jason McLean with your latest traffic and
other update in 20 minutes on ABC Radio Brisbane. Jason, thank you very much.
Well I can tell you the crowd is pretty good here outside the precinct area of
the northern end of Lang Park. People are in a good mood. There are families
out. Lots of Dolphin families. I think there's a fair bit of the Redcliffe
contingent down here. Do they call them pods, Steve? Is that what we call a
family of dolphins? Maybe it is. But a pretty good atmosphere building up for
kickoff which is at about 56, 54, 55 minutes from now. With us is Australian
Rugby League Commissioner Kate Jones. Kate Jones, thank you so much for giving
us your time. I know you probably want to get in there with friends and family.
How involved were you in getting the dolphins into this code, into this round,
into this season? Look, Steve, it was a decision of the Commission, united. We
had a united decision on this. But I guess as a Queenslander, and you know, the
one commissioner that lives in Queensland full time, the weight on my shoulders
felt really significant. And I was just saying to Zane before, they got a lovely
text message from Peter Volandi, just saying you should be really proud tonight.
So it's great just to see all the families coming out. And you know, it's quite
emotional standing here. It's been a good start for the new licence holder. Very
good start. I don't think if you and I placed bets last year, Steve, we would
have picked the Broncos and dolphins if we wanted to. So it's a good decision
about the dolphins licence? Absolutely. And as I said, it weighed heavily on our
mind. We've got great supporters of our game and Ipswich, you know, and I've got
a real soft spot for the Western suburbs and the Ipswich connection from back in
the day. So look, but at the end of the day, the dolphins had a plan. They had
the financial support and they've got a great, rich history. 14 years to get the
17th team. What about an 18th? Well, there's lots of discussions about an 18th
team, even 20 teams. But look, you know, we've got a very clear focus to grow
the game. We think that, you know, and we've seen how fast and fierce the footy
has been this year. We've had the closest start when it comes to competition
points that we've seen since 1975. And, you know, I think that the fans are
rewarding us both with their listening, watching and also turning up in droves.
So I think there is room for expansion. We've got to get that timing right. Big
question, big question, Kate, is where is that expansion going to come from?
Because sometimes when we get a taste of something, we want a bit more of it,
but then we get a little bit of a glut and we don't want it anymore. I have a
feeling if you go to the well again in Brisbane, it will dilute what we've got
here. There's been talk of the Ipswich, there's been talk of that Eastern
Corridor or Western Corridor I should say. This talk about a Pacifica team, how
real is it? And also the fact that if we need to go to 20, whether Perth is a
reality. Can you talk around that for us? Yeah, thank you. And look, absolutely,
it's real. We're having a number of discussions with our key partners,
including, of course, the Hunters and PNG, as well as the Australian government
and both the Queensland Rugby League as well, who have been fantastic. And I'll
say even today I got a call from the Mayor of Cairns, Bob Manning, saying, Kate,
if you bring a team in, I want it in Cairns. So look, I think, obviously, we're
very mindful of the Cowboys as well. So like we had a very deliberate and long
decision about the Dolphins, we won't rush it. We want to make sure we get it
right. But there's so many fans of our game in the Pacifica area that it makes
sense to me that that would be a clear pathway forward to grow the game and to
support another team. In my mind, it's about sort of three things, I guess,
funding, fan base and future plan, in my mind, to make it successful. And player
development with players, like sourcing players. Absolutely. And I think, you
know, so obviously there's work that we can do with other teams in regards to
player development. We're also working very closely with the government already
in PNG about creating those pathways to grow talent. And similarly, one of the
outcomes of the Olympics will actually be a 20,000 stadium in Cairns. So there's
a lot of things pointing in their favour. But once again, you know, we'll make,
the Commission will make a deliberate decision about that. I've been talking to
Shane Morris, who's head of player development at the Redcliffe Dolphins, or
Dolphins, and he was in charge of the Hunters at one stage. And he's talking
about the fact that when he lived up there in PNG Port Moresby, he could live
there, but his wife couldn't. And his kids couldn't go to school past grade six.
So from your perspective, is it important that you have a Pacifica team based in
Cairns, and would they live in Cairns? And could you actually play those games
in the Samoas and the Tongas and PNG when they're based in Cairns? I mean, it's
logistically a nightmare. It is logistically tough, but also Perth is tough, of
course, for our game as well. But those are all the issues that we're working
through quite closely, as I said, with all of our partners, including the
federal government. But I think there is an opportunity, particularly when it
comes to fans, that that sort of mix between the Pacific Islands, as well as
Cairns being the home base, is a pretty good solution to get around some of
those issues that you've raised. Back to the Dolphins here. Is the Dolphins good
or bad for the Broncos and Titans in terms of sponsorship, poaching players and
the like? How do you feel? Because, you know, the Dolphins obviously grabbed a
few of the Broncos players. Look, yeah, you're right, five. But I think what we
really looked at, Stephen, it's a really good question and was part of our
evaluation, was they had to demonstrate to the commission that they weren't
poaching existing sponsors. That of course, you know, with players coming off
contract in November last year was a different story and they had to compete in
that open market, which was tough for them. But you know, they pulled together a
great team. So look, I actually think they came with $10 million worth of
sponsors when they came to the table. One of their sponsors is one of the
largest sponsorships that the game has and already one of the most financially
secure teams in the league. And that's because of their rich heritage and
history. So I actually think it grows the game. If Dave Donahue was on radio, he
would say this rivalry. We've seen record membership for the Broncos this year,
record membership for the Dolphins. Who doesn't love a good local derby? I want
to know, you're going to be in a corporate box this evening. Who are you there
with? Who are you with? Who's trying to get in your ear? Who's trying to
schmooze you? Well, actually, this is a home game for the Dolphins. So they have
hosting rights. So I am a guest of the Dolphins. So I'll be on my best behavior.
There you go. So in the Dolphins box. Can I just say this to you, Kate? Who was
it that made the decision that whoever won the license, they had to have Wayne
Bennett as the coach? Who on the board made that decision? Was it a PBL
decision? Because whoever made it, it was genius. It was a PBL. PBL insisted
that in his view, if they didn't have Wayne Bennett, then that was the linchpin
for him. We needed Wayne Bennett because we knew not only would it attract the
fans, but it would also attract the players. And players that were taking a
gamble on their career, like we see the Bromwich Brothers, you know, make a big
decision to come up here. But they're getting another stage in their career.
Their leadership is on show. And certainly tonight, the Dolphins will be relying
heavily on that experience. Now, I have to ask you about AFL. This sounds
unusual, but Zane Bojack has a theory. His theory is that the AFL guys put on
their game tonight deliberately to try and draw a bit of that, get a bit of that
tension going in terms of where the support base is. How do you see that, Kate?
Oh, look, look around here. All I can see is Fins and Broncos fans. Look, there
might be another game on the other side of town in some weird shaped stadium,
but I'm not paying attention to that. Aren't you glad you're not in government
anymore? I can see that. Can I just say something to you? Once upon a time, I
heard a little dicky bird that said to me that you were going to be the next CEO
of the Brisbane Broncos. So I've had to live that down for a while. But since
you've gone on the ARL Commission, you have been an absolute star. So you know
what, Kate Jones? I think the Broncos missed out. Yeah, caught that day. No,
look, I'm just I'm so happy for Kevi. I'm a look, of course, I've lived in
Brisbane all my life. Mad Bronco supporter. So excited about the Dolphins. But
you know, I'm so happy for Kevi. And I just think, you know, everything's coming
together. And let's face it, at the end of the day, we're going into round four
with the Brisbane Broncos and the Dolphins on top. Kate Jones, thanks very much
for coming along. Pleasure. Hang on. Who are you backing? You're in the Dolphins
box. You're in the Dolphins box. I am. I am. I'm an independent commissioner.
But of course, you know, but you can't you have to back your own team. My team's
always with the Broncos. So obviously, in my heart says Broncos. I want to make
sure it's a good game, a close game, and that'll be a great game for the fans.
80 minutes of good footing. Thanks very much. That's a good segue, Steve. Kate
Jones saying that she's going to be supporting the Broncos tonight because it's
been an interesting build up to this game, Steve. It's been an amazing run by
Kevin Walters' team, the Broncos. They started off this season with a win
against the defending premiers, the Penta Panthers. Let's have a listen to what
they've been able to do over the past four weeks. Last tackle, Walters goes left
to Reynolds. Reynolds got a pass away to Farnworth. And Steven Crichton got
pulled out badly in defence. Herbie Farnworth scores for Brisbane. And they hit
back in the 16th minute. And now Walters threatened to kick out a dummy half and
he manages to beat the first and then got a pass away as well to Ezra Mann. Look
out, he's away. He beats one-two. Steven Crichton is having a shock. I missed
it. A kick downfield. Farnworth in pursuit. Farnworth and Cole. And it's Herbie
Farnworth. He gets a touch to it. It's going to be a try, I reckon. Ten metres
out from the goal line. Adam Reynolds just by the by. Setback on the 20 metre
line. Walters goes left to Reynolds. Field goal from 18 metres out. Reynolds
strikes it, likes it, kicks the field goal. Brisbane by seven. Brisbane's going
to win. They lead 13 to 12. Eight slow to his feet. Sirens. Whistle. Long
whistle. Game to the Broncos. Brisbane has beaten the Premiers. Penrith first
up. Pretty special, yeah. Especially in round one to be against the Premiers and
winning in that fashion. Just massive shout out to our middles there working
unbelievably hard. So all the boys made a note of it watching finals footy,
thinking that we should have been there. Well, knowing that we should have been
there. Everyone's worked super hard, staff included. Especially in our Ds, it's
been a massive, massive highlight for this pre-season. So I think it showed
tonight. 20 from touch on the western side on halfway. Played to Pikes, passed
centre field and left. Reynolds to Mamm. Here's Walters again at the line. Oh,
he draws three defenders. Puts Holtz away down the wing. Up to the 20. Rubber in
field. Straight to Asra Mamm. Gathers and scores. And yet again, yet again, the
architect is the new number one. Kick downfield from Townsend. Again, just the
value of his knowledge there on the back foot. He puts a boot to it. Taken by
the goalie. And he's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it. He's got it. He's got it. He's got it. He's got it. He's got it.
He's got it.