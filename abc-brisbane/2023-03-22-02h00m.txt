TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-03-22-02H00M.MP3
--------------------------------------------------------------------------------
 ABC News, good morning, I'm Maria Hatzakis. Water police are assisting with the
search for a woman reported missing from Corumbin Beach on the Gold Coast. A man
raised the alarm about half past nine last night, but when emergency services
arrived at the scene, he was gone. Police are urging the man to contact them as
the search for the woman continues. The Federal Government has struck a deal
with the Coalition on legislation setting out how this year's referendum on an
Indigenous voice to Parliament will be managed. Matthew Doran reports from
Parliament House. Congress made a concession in allowing an official pamphlet to
be distributed across the country outlining the respective yes and no cases, but
it has not agreed to the Coalition's demands for Commonwealth funding for both
sides of the debate. A national, neutral, civics campaign will be run to ensure
voters understand the referendum process and the meaning of the proposed
constitutional change. The deal effectively sidelines the Greens and Senate
crossbench who had also been proposing amendments to the machinery bill. The
legislation could be dealt with by the Senate as early as today. Latitude
Financial says it's uncovered more large-scale data theft impacting past and
present customers. Business reporter Amelia Tozon has more. The provider of
credit cards and buy now pay later products in major retailers hasn't said how
many more of its customers' data has been stolen. It already revealed last week
that around 330,000 people's information had been hacked and taken, including
driver's licences and a small amount of Medicare numbers and credit card
details. The company has almost 3 million customers and an unknown number of
formal ones. It's apologising to its customers and says that its call centres,
which closed last week after the cyber incident, are operational again.
Overseas, Bali's Governor Wayan Kosta has joined growing opposition to Indonesia
in the inclusion of Israel's soccer team in the upcoming Under-20 World Cup,
with some matches scheduled in Bali. Indonesia correspondent Anne Barker
reports. Governor Kosta has written to Indonesia's sports minister demanding he
ban Israel's team from taking part. He says it's out of respect for Indonesia's
diplomatic support for Palestinians. The move comes after conservative Muslim
groups staged a protest and march in Jakarta this week against the Israeli team.
Indonesia has no formal diplomatic relations with Israel, but a foreign ministry
spokesman says it's FIFA, soccer's world governing body, that decides which
teams participate. Though he says despite Indonesia's position as host, its
stance on the Palestinians will not waver. Anne Barker, ABC News, Jakarta. Back
home, a multi-million dollar Paralympic Centre of Excellence is set to be built
in Brisbane ahead of the 2032 Games. Laura Lovell reports. The new facility at
the University of Queensland's St Lucia campus will be a training centre for
para-athletes. The state government and UQ will each put forward $44 million in
funding. The federal government will be asked to match that contribution. The
facility will cater for 20 of the 23 Paralympic sports. Premier Anastasia
Palaszczuk says the centre could be a long-term home for Paralympics Australia.
Construction is expected to begin by 2026 and completed by 2028. Australian
households spent a record $63.8 billion online shopping last year, making up 18
per cent of all retail sales. Australia Post has released its annual online
shopping report showing that over four in five households went online for their
goods last year. Home and garden was the category with the most spending, while
food and groceries also saw big increases. To the weather for Queensland now.
Scattered showers along the east coast, tending widespread about the north
tropical coast. Scattered showers and storms across the Gulf Country and
peninsula, tending widespread about the eastern peninsula. Isolated showers and
storms in western Queensland, mostly sunny elsewhere. Cairns Townsville and
Rockhampton 31, Mackay and Bundaberg 30, Mount Isa 34. In the southeast, cloudy
with a chance of showers. Brisbane 28, Ipswich and Logan 29. The Gold and
Sunshine Coasts 27, Toowoomba 24. Looking ahead, partly cloudy tomorrow, mostly
sunny on Friday and partly cloudy on Saturday with a medium chance of showers
about the southern border ranges. ABC News. Suddenly I felt normal. I started
crying. I knew it was my blood. You roll out your swag. And I lashed out. My
mind opening up like a trap door. Majestic vindication. It was such a relief. So
to your question. Life. In everyday moments. Really strange but really
awakening. Conversations with Richard Feidler on ABC Radio. Most of us are
familiar with the creaky streets of the south. But there's a lot of people who
are not. And I think that's a good thing. I think that's a good thing. I think
that's a good thing. I think that's a good thing. I think that's a good thing. I
think that's a good thing. I think that's a good thing. So imagine the impact
that the world's first big public aquarium had when it opened in London in the
mid-19th century. For the first time, people were exposed to the gangly
weirdness of an octopus and to lobsters and sea urchins and the school of fish.
John Symons is Emeritus Professor at Macquarie University and he lives in
Hobart. John has written about all kinds of things in his distinguished career.
He's written about medieval manuscripts, chivalric romance and the history of
cricket. More recently, John has dived right into the history of animals, into
the lives of the animals that became stars when they were put on display in
Victorian England in front of thousands of fascinated gawkers. John's latest
book is about the invention of the aquarium in the Victorian era and the craze
that it sparked for marine life and his book is called Goldfish in the Parlour.
Hello John. Hello Richard. You had been a medievalist for most of your career.
What got you interested in the lives of animals? Well I was always interested in
animal welfare and animal rights in a personal way. And there came a point in my
career when I achieved certain things and I realised that I didn't really have
to worry too much about publishing in medieval literature anymore. So I thought
I'd start writing about the things that I really cared about and that was animal
rights and animal welfare. But of course as a historian I do that by telling
stories about the history of animals and let people understand through that the
issues around welfare and so forth that emerge. I've often talked on this
program about trying to imagine myself into the life of an animal, you know just
from having a pet in my life and how hard it is to do that because we're always
inclined to project human attributes onto the animals in our lives. Is that what
you were looking at? Were you kind of interested in what might be going on in a
certain animal's head? To a certain extent although as you rightly say it's
actually very difficult and actually you know often I think quite dangerous to
start to make speculations about what animals might or might not think or might
or might not feel. I think it's better to assume that they do think and they do
feel and that because of that they have certain rights to reasonable treatment.
So I think I'd take it from that perspective more. I mean there have been some
very interesting writings about the life of animals but anthropomorphizing them
too much is a difficult path to go down. I wonder if we could actually sort of
put ourselves inside their heads given that they have a sense of sight that's
either radically better or radically worse, a sense of smell that's radically
better or radically worse. We'd have this wholly completely different sensory
field that would make the world look utterly different I think wouldn't it? I'm
sure it does. There's been a lot of recent work about octopuses which are quite
extraordinary creatures and particularly their sensory apparatus and their
cognitive powers which appear to be quite different from ours and quite
remarkable. When you were looking at some of these star animals from the
Victorian era that I mentioned before, which animal did you start with John?
Well I started with a wombat called Top who was owned by the pre-Raphaelite
painter Dante Gabriel Rossetti who in fact owned a whole menagerie of exotic
animals. Rossetti had a pet wombat? He certainly did. Called Top is that right?
Called Top. It's called Top after William Morris. And how did an English
painter, pre-Raphaelite painter come to be in the possession of an Australian
native animal like that? Well it wasn't as hard as you might think Richard. The
East End of London, particularly around the St Catherine docks area had a number
of traders in exotic animals and sailors, ship's captains and tourists would
have these animals and they'd bring them. You know the famous picture of a
sailor with a parrot is a good example of that and then they would sell them to
these dealers. So Rossetti went down to the East End one day with a five pound
note in his pocket and came back with a wombat. And what do you know about the
life of Top the wombat in this garden in London? Well it wasn't very long. Most
of Rossetti's animals died pretty promptly because he had no idea how to look
after any of them really. And Top lived for just a few months. He lived in the
garden in Chelsea. He also lived in the house and there are anecdotes of what he
got up to in the house. He appears though to have had sarcoptic mange which
is... That doesn't sound good. No, no and it's still endemic in wombats and that
probably carried him off quite young. Probably he only lived until he was eight
or nine months old unfortunately so he was quite a small wombat. What kind of
interest did a wombat excite in London in those days? Oh well a lot of interest
because at about just slightly earlier than the arrival of Top London Zoo had
acquired its first wombats and they were extremely popular especially since they
bred. So for the first time wombats were... new wombats were appearing in
Britain. In the course of the 19th century, I mean really from the first fleet
onwards, there's a steady trail of Australian animals to England and to Europe
but mostly to England. It starts with kangaroos which are incredibly robust
animals and were relatively easy to support, to transport and get them there
alive. And there were several groups of kangaroos in England even by the turn of
the 19th century. The most difficult one was the koala because as you probably
know koalas have a very, very specific diet and they couldn't keep them alive
long enough on fresh eucalyptus leaves as they crossed, as they went on the long
sea voyage. And they finally did get one to London Zoo but unfortunately he then
got tangled up in a towel, in a towel rack in a laboratory and hanged himself.
Oh God. I know. So... Poor thing. Well, most of my research is rather like that
I'm afraid Richard. Most of it ends badly for the animals I'm afraid. Yeah.
Which is part of the point of doing it, you know. Yeah. I remember reading a
story that the Habsburg Emperor Rudolf II had this huge menagerie in Prague
Castle. He did. And one of them included a cassowary from New Guinea. Now this
is in the 1500s he had a cassowary. So this has been going on for a while this
tribe, hasn't it? Oh yeah. There are some strange things. There's also a very
early manuscript which clearly has a sulphur crested cockatoo in it, a European
manuscript. I think we underestimate quite how much transaction there was with
what we now think of Australia in those days. I mean although at the European
end they weren't quite sure where these things were coming from. You know there
were transitions through what is now Indonesia and China which made their way,
which of animals which made their way into Europe. There is some thought that
the Chinese had some kangaroos quite early on too. You've also told the story of
a baish, the hippo. A hippo. How did this hippo end up in Victorian London?
Yeah. The London Zoo was set up as basically a private zoological society for
members only. And that business model was badly failing by about 1850. So they
got in a guy called David Mitchell as what we would now call the CEO. And he hit
upon a business model which involved getting what he called a star animal each
year, which would revolutionise the fortunes of the zoo and bring in many more
paying customers. So they mounted an expedition. They did a lot of diplomacy
with the government of Egypt and they mounted an expedition down the Nile to
capture a hippo and bring it back alive to London. Now capturing a hippo is no
small feat. I mean hippos don't like you coming too close to them I understand.
They're extremely dangerous. Extremely dangerous. So what do you know about how
they caught this hippo? Well it was very sad. It was only a baby hippo when they
captured it. So I mean I'm afraid what they actually did was shot its mother and
then they hooked it with a traditional hippo spear which is like a boat hook.
And in fact in the photos of our baish you can actually see the scar on its side
from the boat hook. And then they transported him up the Nile, kept him for a
bit to a climate to recover and to wait for the ship to come from England and
took him off to England where a special train was waiting for him to take him to
London Zoo. How long had it been since hippopotamuses were in Britain? Were they
ever in Britain? Well they were in the days when Britain was tropical forest.
Yeah they were. They were the prehistoric hippopotamuses. But it's probable that
some of them, the hippos, may have been brought to England by the Romans for
gladiatorial games that were held in London. In fact that's probable. But none
since Roman times. In fact none in Europe since Roman times. So what was a
baish's life like?