TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-03-26-05H00M.MP3
--------------------------------------------------------------------------------
 ABC News with Satyam Weinstein. The South Australian Parliament has passed
legislation to establish a First Nations Voice to Parliament. Evelyn Manfield
reports. Hundreds of people are gathered outside South Australia's Parliament
for the passing of the voice legislation. The crowd cheered and waved Aboriginal
and Torres Strait Islander flags as the legislation passed. Premier Peter
Malinouskas has told the crowd this will give First Nations people a voice to
Parliament. It makes SA the first jurisdiction in Australia to have an
Indigenous voice to Parliament. The special Sunday sitting was designed to allow
as many people as possible to attend. The next step will be electing
representatives for the South Australian voice. The Premier elect of New South
Wales says he'll meet with his senior leadership group this afternoon to ensure
plans are in place to start governing. The ABC predicts Labor will govern in
majority, having picked up at least nine seats from the coalition. Labor's gains
include key Western Sydney seats of Parramatta and Penrith. Chris Minns says
their key priorities include getting support in place for essential workers and
protecting essential services. Giving confidence to essential workers in New
South Wales that there's a government in place that will look after them. We're
also preparing legislation to insert Sydney water inside the New South Wales
constitution as a lock against the back door fire sale of this essential public
asset. Outgoing New South Wales Deputy Premier and National Leader Paul Toole
says winning four elections was always going to be tough for the coalition. Mr
Toole has comfortably won his seat of Bathurst. The party is set to lose Monero
with Labor candidate Steve Wan on the verge of a political comeback after a big
swing in this seat once held by the former Deputy Premier John Barillaro. The
ABC projects Mr Wan will finish on 54% of the two party preferred vote. Mr Toole
says overall it's been a strong result for the Nationals. In a fourth term we've
actually increased our margin so far and it's still being counted and we don't
have pre-polls in and we know the pre-polls were quite strong. But we are
looking at increasing our margin by close to 5%. Ms Janelle Safin has received a
resounding endorsement from voters in the flood ravaged electorate of Lismore.
She's held the seat with a majority of less than 2% but that now looks set to
blow out to more than 16%. Ms Safin says it's a vote of trust from the people
she's worked hard to help. A lot of people who would never consider voting
Labor, they voted for me. They know I'm Labor, that's no secret. But I had
people say to me, I've never voted Labor but I am voting that way. I'm voting
for you. I want you to stay around and see us through all of this. Federal
Services Minister Bill Shorten is calling for a further investigation into
government contracts worth hundreds of millions of dollars over potential
conflicts of interest. He's ordered an investigation on the back of allegations
first published in nine newspapers that former Cabinet Minister Stuart Robert
intervened to help France negotiate a lucrative Centrelink contract. The
allegations do not constitute any illegal conduct and Mr Robert has rejected the
act of accusations. The Independent Review looked at Services Australia and NDIA
contracts from 2015 to the present and found 19 had inconsistencies with
standards and good practice. Bill Shorten says the Government will now consider
its response to the report. The report has revealed far lower standards than are
acceptable according to the public service procurement practices across hundreds
of millions of dollars of contracts of taxpayer money. Rescuers are searching
through the rubble of ruined buildings after a series of tornadoes in the
southern United States left at least 26 people dead. In Mississippi, entire
blocks of homes have been flattened by a tornado that touched down for nearly an
hour. Authorities in neighbouring Alabama say a number of communities there have
been badly damaged as rescuers used chainsaws to search through the debris. The
former US President Donald Trump has held a rally at Waco, Texas. He's railed
against a series of investigations into him, telling supporters the thugs and
criminals in the justice system will be defeated. Mr Trump's facing a number of
indictments as prosecutors look into allegations of campaign finance fraud, his
handling of top secret documents and the attempt to overturn the 2020 election.
The choice of Waco as a venue has raised eyebrows with the city marking 30 years
since more than 80 members of a cult were killed after a long standoff with
federal law enforcement. An Australian test, Cody has finished second in the
final snowboard slope style event and the World Cup circuit in Switzerland. It
was Cody's first slope style podium since she won bronze at last year's Beijing
Winter Olympics. You're up to date with the latest from ABC News. On ABC Radio,
this is Weekends with Michaela Simpson. Hello, how are you today? Are you having
a nice Sunday so far? I truly hope so. In a moment, it's been over 50 years
since the moon landing and you'll find out how Australia is preparing for its
journey back into space. And speaking of the skies, today I want to know what is
the best sunset you've ever seen? Where were you? And bonus points if you can
tell me about its colours. You can share your experience with me today by
calling 1300 800 222 or you can send me a text 0437 774 774. Sunsets are one of
nature's gifts that we just get to experience each and every day. What makes
them so extraordinary that sometimes they take our breath away with the
beautiful colours that present themselves this afternoon looking into the
science of sunsets. But first, it's been over 50 years since the moon landing,
but NASA is actually planning on heading back to space. This time, its ambitions
are bigger with the lunar surface to be a launch pad for a manned expedition to
Mars and Australia is gearing up to be part of the action. ABC's Angelique
Donnellan explores what this means. As all the signals from the astronauts on
the moon will be fed through parks, we will be a split second ahead of everybody
else. Half a century ago, an Australian radio telescope in regional New South
Wales helped beam footage of the moon landing to more than 600 million people
around the world. Australia's role in the 1969 moon mission was small but
important. Now NASA is planning a return trip, but this time will play a much
bigger part. We have a long partnership with the US and NASA in deep space
exploration now spanning more than half a century. Australia is one of the
closest partners of the US having to do with terrestrial matters. We want them
to be our partner in matters of space. Space is hard and yet space is the place.
For the first time in a decade, NASA has sent its administrator to Australia to
strengthen ties ahead of a series of expeditions to the moon. The US space
agency is spending $93 billion on the so-called Artemis program. Three, two,
one, boosters in ignition and liftoff of Artemis 1. We rise together back to the
moon and beyond. Last year, NASA successfully achieved its first mission
milestone by launching a spacecraft into the lunar orbit. NASA is now traveling
607 miles per hour. There are plans for a crew to land on the moon in about 2026
with a base to be established in the years after. We are going back to the moon
now after a half century, but it's a different program of going back to the
moon. It's to go and live and learn and work and create and invent. Since the
last time a NASA administrator visited, Australia has set up its own national
space agency. The federal government has committed $150 million to the moon to
Mars mission. That ought to go to the moon. Australia was one of the last OECD
countries to form an agency. We're not tracking a space economy in Australia
that is growing rapidly. We have over 17,000 people working in this sector and
we identify over 600 organizations that are working in space one way or another
from commercial entities to research and academia to government. Today $8
million was awarded to Australian companies to design a rover vehicle which will
be deployed on the moon. So the purpose of the activity is to design a rover
that can operate on the surface of the moon to scoop up dirt from the surface
and move it towards a unit on the surface for analysis to understand what that
material is made of. And essentially oxygen will be extracted from the soil to
help maintain a sustained presence on the moon. For every dollar that we spend
in space exploration, that's money that's spent here on earth in innovation, in
research, in creating jobs and business opportunities. Later this week,
Catherine Ben-Orpeg will head to Europe to be trained as the Australian space
agency's first astronaut. When I studied university in Australia we didn't even
have a space agency which is why I and many of my other colleagues went
overseas. I'm so excited and humbled to be picked as the first person to
represent Australia on astronaut training. It still feels surreal to me. In this
return trip to the moon, NASA is planning to harness lunar resources. We know
there's ice. If there's ice in abundance, then there's water. And if there's
water, there's rocket fuel, hydrogen and oxygen. Making rocket fuel would help
NASA move towards its grand ambition, future crewed voyages from the moon to
Mars. I'm saying 2040. Why Mars? Was there life? Is there still life? And if so,
what happened? And was that life perhaps developed? If it were developed, was it
civilised? That's part of our exploration. Bill Nelson says what's learned in
space will help us better conserve planet Earth. You think of us as a space
agency, as an aviation research agency. We are also a climate agency. Why? All
those instruments up there that are observing Earth. That's giving us the data
of what we are doing to our planet down here. Human society is on the cusp of
the next great space exploration and industrialisation endeavour. And Australia
is part of that. We're part of that already. That is ABC's Angelique Donnellan,
exploring what Australia is up to as it prepares to head back into space. And
you can learn more about that journey at abc.net.au. But look, you don't have to
be an expert in space to be fascinated by what could be lurking out there. For
example, in 1985, Alice Springs man Lou Farkas made a bold business decision. He
purchased a tiny roadhouse on the Stewart Highway in the middle of the
Australian desert. Within a few years, he'd transformed the place into a UFO-
themed attraction, complete with murals, green alien statues, a choo-choo train
and even a man-made barramundi lake. Lou tells ABC's Jack Schmidt the intriguing
story of Wycliffe Well. My name is Lou Farkas. I bought Wycliffe Well in 1985
and left there in 2011. When I bought it, it was just a small shop with a petrol
bowser. But my time here, I converted it into a hollow destination, being a
60-acre, 6-acre grand park, hotel, recreation area, zoo. And the UFO theme was
quite beneficial to be able to produce all that. Yeah, the UFO thing is the key
detail here. This wasn't an ordinary roadhouse. It was covered in UFO and alien
paraphernalia. When did Wycliffe Well become known as a UFO hotspot? Well, it's
always been a UFO hotspot, even since the Second World War. Because Wycliffe was
a staging point for the Army, as they were driving up and down the Stuart
Highway and led to Darwin, reason being because there was a good water supply
there. And so they always, overnight, that's where they stopped seeing coffee
breaks and all that sort of stuff. And there was already quite a few sightings
in those days. It wasn't until, I can't remember the exact year now, but
probably in the early 90s that I personally had sightings. And so I'd have to
have been more noticed of what was going on. And then some of the paintings that
were left behind from the previous owners started to make sense to me because
there was these sort of strange paintings that depicted this sort of stuff, but
there was no story attached to it like that. And then I happened to be talking
to somebody who was driving through from Taylor Creek, and I just mentioned to
him that there's strange things happening at the back of the park. There was
light sipping around the sky. And he went back and told the news media at the
time, the Tenant Times in Taylor Creek about what I said. And so they then came
out to make a story of it and then went out to the side of the building and sure
enough they had a sighting. And lo and behold, they raced back to Taylor Creek
and then put it in the Tenant Creek Times and it became front page news in the
Sunday Territorium. And that was it. That was it. From that point onwards, when
I had that publicity, it became noticed that it was a common theme to this
thing. There was quite regularly things happening out in the skies at the end.
So the roadhouse became a bit of a magnet for people that had witnessed UFO
sightings. Yeah, that's right. It gave them a sense of relief that they could
actually tell somebody or tell their story. So then the next thing I could do
was start changing my tourist items to UFO type items. And I was already heavily
into painting murals all around my walls. But that always had to do with Ayers
Rock and traditional Northern Territory landscapes and all of a sudden I had to
paint over everything and hire a muralist to come from all over the place and
start painting UFO stuff. So you painted stars and UFOs and aliens and all sorts
on the walls? Yeah. So during this period of time, I was also then building a
caravan park. I put in a more motel accommodation and I built a 60-acre lake out
the back, put in a railway line and zoo and all this sort of stuff. What
happened was I actually then converted Wycliffe from a stopping point just for
fuel or refreshments to an actual destination with souvenirs or with the murals.
The murals went into all my motel rooms and throughout the park everywhere,
statues and aliens and UFOs and people just couldn't get enough of it. I mean
the clicks were going with the cameras was non-stop. So Lou, over the years you
kind of created this place that became an internationally known UFO hotspot and
an attraction for people on the Stuart Highway. How much money did you invest in
the place? About 4 billion, so 4 billion dollars. 4 million dollars and that was
in the 80s and 90s, that was a lot of money now, a lot of money back then as
well. Was it a successful business? Well, it was. Of course I kept investing all
the time thinking that my children were going to take over at some point down
the line but that never eventuated and so it was a loss in that respect, a big
loss in that respect. But I mean the thing was that there was actually nothing,
the place was nothing there and then of course when I first went there it was
still a dirt road, the Stuart Highway was still a dirt road and then a year
later they started filling the highway all the way, it became a sewer highway
between Adelaide and Darwin. And of course what happened then was that the
caravan had started coming up and I was just a little shop there at the time and
there were a couple of motel rooms and so I was just on the generators and of
course all these caravans would pull up and they want power and I had all power
lines going through my windows everywhere and the generators going ooooh, so I
thought I better do something quick about this. Well you had a massive
restaurant didn't you, a 300 seat restaurant and you had the dam and the train.
Yeah, I had two restaurants. Two restaurants. Yeah, I had two restaurants and it
was a big stage, you know 300 seat actual sitting part, big bird abies around
the back walls and there was huge big kitties and all that sort of stuff so I
could cater for the 300 easily, it was not a problem at all. And you had a big
dam out the back as well Lou. Yeah that's right, so that was, I think I built a
barramundi lake. A barramundi lake in the desert. That's it, well that was the
whole idea and then around, then I had bridges that ran across to the island in
the middle of it and then across that then I had a, I bought a train from the
Grampians in Victoria. A train. Yeah a train actually, an actual tichy train, it
was a diesel train looped around the lake and then back to the site and all
these were free attractions for the people to stay in the park. Hey Lou, those
years you had at Wycliffe Wells sound like a massive adventure in your life. Do
you miss the place these days? I do, I do. I missed it from day one in the sense
that what I miss in my life is the cut and trust of business and because it was
one hell of a hard job in the sense that, see my children and my family lived in
Alice Springs because they couldn't, we had retired school there and stuff like
that but it didn't work because I got to a phase when I got a message from my
daughter's school that the teacher overheard her saying that she doesn't have a
father you know and so I thought oh that's time to sort of re-evaluate the
situation and that's one of the reasons why I sold cheaply and lost so much in
that respect is because it was either that or hanging out for a price and that
could have gone on for who knows how long. So UFO thing gave me free publicity
for year in year out. I mean I was in everything, magazines, it was just
everywhere and I had all these articles and news programs up on the walls, the
walls and the restaurant was covered in news items and articles definitely. It
was good business and bad business. Green lights, people rushing in saying oh
quick quick give us a swim, we're going off the highway, just beam a light, just
follow us all the way down the highway. So it was good for me that way. In other
ways it was bad because they'd say give me a quick coffee, I'm getting out of
it, you can't drive, I've got to go, I've got to get out of the area. So many
strange happenings in Wycliffe Well, thanks for telling me a little bit about
it. You're not welcome, my pleasure. Yes, if you've ever been to Wycliffe Well
it's a place that you'll never forget. ABC's Jack Schmidt speaking with Lou
Farkas about the intriguing story behind that place and you're hearing it on ABC
Radio. Ready for some fun? The Melbourne International Comedy Festival is back
and it all begins with the gala hosted by Luke McGregor. Thanks for clapping
when I came out, that means a lot. With an all star line up. We're all in this
crazy mixed up world together though aren't we? Now thanks to unisex toilets we
actually are. The Melbourne International Comedy Festival, the gala. Excellent!
Wednesday night March 29 on ABC TV and streaming on ABC Eyeview. Weekends with
Michaela Simpson on ABC Radio. And it is time for another Who Sang It Best. If
you are playing along for the first time, the game is simple. I give you three
different versions of one track and all you need to do is tell me who sang it
best. The song with the most votes will be played in full before the end of the
hour. And today I have been putting this song off but I think it's finally time
we put this track to the public vote. It's Leonard Cohen's Hallelujah. Now I've
heard there was a secret chord that David played and it pleased the Lord. But
you don't really care for music do you? It goes like this, the fourth, the
fifth, the minor, fall, the major, lift, the baffled king composing Hallelujah.
Hallelujah. Hallelujah. The song itself was written and recorded by Leonard
Cohen in 1984 and it featured on his album Various Positions. Cohen's rendition
was released as a single in Spain and the Netherlands but it got very little
attention in the United States. Speaking of the track, Cohen explained,
Hallelujah is a Hebrew word which means glory to the Lord. The song explains
that many kinds of Halleluyas do exist. I say all the perfect and broken
Halleluyas have an equal value. It's a desire to affirm my faith in life, not in
some formal religious way but with enthusiasm, with emotion. However, it's
another version which is arguably the most famous and it is your second option
in today's Who's Sang It Best. Hallelujah. Hallelujah. Hallelujah. Hallelujah.
Hallelujah. Hallelujah. Hallelujah. Hallelujah. Hallelujah. Hallelujah.
Hallelujah. Hallelujah. Hallelujah. Hallelujah. Hallelujah. Hallelujah.
Hallelujah. Hallelujah. Hallelujah. Your faith was strong but you needed proof.
You saw her bathing on the roof. Her beauty and the moonlight overthrew you. She
tied you to her kitchen chair. She broke your throne. She cut your hair. And
from your lips you gave her wings. She cut your hair. And from your lips she
drew the hallelujah. Hallelujah. It really is beautiful, isn't it? Jeff Buckley
is your second option in today's Who's Sang It Best. He heard the song in the
early 90s and he began performing it at his own shows in and around New York
City. He did include it on his 1994 debut album, Grace, but the song didn't gain
widespread attention until after his death in 1997, which sparked renewed
interest in his work. Now last but certainly not least, the brilliant Katie
Lang. But I've seen your flag on the marble arch. Our love is not a victory
march. It's a call and it's a broken hallelujah. Hallelujah. Hallelujah.
Hallelujah. Hallelujah. Hallelujah. Hallelujah. Hallelujah. Hallelujah.
Hallelujah. Hallelujah. Hallelujah. Hallelujah. Hallelujah. Hallelujah.
Hallelujah. Hallelujah. So out of those three versions, I want you to tell me
who sang it best. Is it Leonard Cohen's original? Is it Jeff Buckley or is it
Katie Lang who released and recorded their version of it in 2004? Share your
thoughts with me by calling 1300 800 222 or text 0437 774 774. And now the track
with the most votes will get played in full before the end of the hour. My name
is Michaela Simpson and I am here with you on ABC Radio in a moment turning our
attention to the unassuming animal which for centuries has found its way into
historical art. That comes after this track from the church. It's under the
Milky Way. I think about the loveless fascination under the Milky Way tonight.
Lower the curtain down on Memphis. Lower the curtain down on Rye. I got no time
for private consultation under the Milky Way tonight. Wish I knew what you were
looking for. Might have known what you would find. And it's something quite
peculiar. Something shimmering and white. It leads you here despite your
destination. Under the Milky Way tonight. Wish I knew what you were looking for.
Might have known what you would find. Wish I knew what you were looking for.
Might have known what you would find. And it's something quite peculiar.
Something shimmering and white. It leads you here despite your destination.
Under the Milky Way tonight. Wish I knew what you were looking for. Might have
known what you would find. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Under the Milky Way tonight. Under the Milky Way tonight. Under the Milky Way
tonight. Under the Milky Way tonight. Under the Milky Way tonight. Under the
Milky Way tonight. Under the Milky Way tonight. Under the Milky Way tonight.
Well it goes like this, the fourth, the fifth, the minor, fall, the major lift,
the baffled king composing Hallelujah. Hallelujah, Hallelujah, Hallelujah,
Hallelujah. Well your faith was strong but you needed proof. You saw her bathing
on the roof. Her beauty and the moonlight overthrew you. Well she tied you to
her kitchen chair. She broke your throne and cut your hair and from your lips
she drew Hallelujah. Hallelujah, Hallelujah, Hallelujah, Hallelujah. Baby I've
been here before. I've seen this room and I've walked the floor. Used to live
alone before I knew you. But I've seen your flag on the marble arch. Our love is
not a victory march. It's a call and it's a broken Hallelujah. Hallelujah,
Hallelujah, Hallelujah, Hallelujah. Maybe there's a God above but all I've ever
learned from love was how to shoot somebody who outdrew you. It's not a cry that
you hear at night. It's not someone who's seen the light, the cold and broken
Hallelujah. Hallelujah, Hallelujah, Hallelujah, Hallelujah. Hallelujah,
Hallelujah, Hallelujah, Hallelujah. Hallelujah, Hallelujah, Hallelujah. Oh,
Katie's voice, the strings, it really is amazing. That is Hallelujah, the winner
of today's Who's Sang It Best on ABC Radio. Thank you so much for sending in all
your votes. Michaela Simpson is my name. I'll be back with you right after the
news. Talk to you then.