TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-16-17H00M.MP3
--------------------------------------------------------------------------------
 Yes. There's a Dutch connection with the name Orange as well. Yes, Prince of
Orange I think. It's so good to hear you Casper. I always enjoy hearing you ring
up for the quizzes throughout the week. Sorry, I don't know the answer but I'll
continue listening. And hi to my mum Beryl. Yes, hello Beryl. All right now
Casper, we don't have any time for any more callers but we've got the news
coming up. So we've got a couple of lines free, 1300 800 222 if you would like
to get on board. I've had people waiting for quite a while now so you might need
some patience but we've still got a few questions so you might get a chance to
partake in tonight's quiz. And 300 800 222 will be back with more after the
latest news. ABC News with Chelsea Hetherington. The Sudanese Army says it has
approved a proposal from the United Nations to open a safe passage for urgent
humanitarian cases for three hours every day starting today. The army says it
will reserve the right to react if the rebellious militia commits any
violations. Earlier, the United Nations World Food Programme said it had
temporarily halted all operations in Sudan after three of its employees were
killed in clashes between the Sudanese army and the paramilitary rapid support
forces. Fierce fighting has been raging for a second day with at least 56
civilians killed. Officials in the United States say four people have been
killed and multiple people injured during a shooting in East Alabama. Local
media is reporting the shooting occurred at a 16th birthday party in the city of
Dadeville. The Alabama law enforcement agency has released little information
about the shooting and it's not immediately known if a suspect is in custody. In
a post on social media, state governor Kay Ivey says he grieves with the people
of Dadeville. At least six soldiers are dead and about 30 are missing after
separatist gunmen attacked Indonesian troops deployed to rescue a New Zealand
pilot taken hostage by rebels in the country's Papua province. The soldiers were
part of a group searching for Philip Mertens, a New Zealand pilot for an
Indonesian aviation company who was abducted by the rebels in February. The
rebels from the West Papua Liberation Army said the group's fighters carried out
the attack in revenge for killing two of the members by Indonesian security
forces recently. Coalition Senator Jacinda Nunpajimpa Price says she assumes
allegations of child sexual abuse in Alice Springs made by opposition leader
Peter Dutton have been reported. During a visit to the region this week, Mr
Dutton repeated claims that sexual abuse of Indigenous children is rampant and
that children are being returned to their abusers. The peak body for Aboriginal
and Islander children refuted the claims, accusing Mr Dutton of using the issue
as a political football. Senator Price has been asked whether she passed on any
of the allegations of abuse onto police. If that's what the territory families
know about them, I'm assuming that they have reported that. I know foster
parents have, they've done the due diligence and they have done the reporting.
There's been a surge in road deaths across Australia with a nearly 6 per cent
increase in fatalities over the past year. Kate Ashton reports. There was an
increase in road deaths in every Australian jurisdiction except the Northern
Territory in the year ending March 31st. Around the country 67 more people died
on the roads compared to the year before. There was also a significant rise in
pedestrian deaths, up 23 per cent. The Australian Automobile Association says
the reasons for the rise in pedestrian deaths is not yet clear. It says more
data is needed to better understand and prevent the upward trajectory of road
deaths. 50 people have been arrested after protesters entered a rail corridor
and stopped a loaded coal train near the port of Newcastle. The protesters stood
beside the rail line at Sandgate, forcing the train to come to a halt on
Saturday morning. More than a dozen people climbed on top of a train carriage
where they remained for almost three hours. Newcastle City Police District
Commander Superintendent Kylie Endemi says most people protested peacefully. 47
persons were issued with Field Court attendance notices at that location and
three other persons have been arrested and returned to Warratah Police Station.
Two of those people will be charged with malicious damage and one person will be
charged with an assault on a security guard. Collingwood has handed St Kilda its
first loss of the AFL season, beating the Saints by six points in the final game
of Gather Round in Adelaide. ABC Sports' Clint Wilden reports. Before the game,
SA Premier Peter Malinouskas announced to a massive roar that Gather Round would
return to Adelaide for the next three years. This year's edition climaxed with a
thriller. Collingwood looked set for victory, leading by 26 points late in the
final term, before St Kilda stormed home, falling short by a goal. The Magpies,
though, will miss Dan McStay for the Anzac Day game against Essendon. McStay has
a tendon injury in his finger. Earlier, Geelong beat West Coast by 47 points,
while at Norwood, GWS beat the Hawks by two points. Clint Wilden, ABC News,
Adelaide. New South Wales won a thrilling top of the table Super W clash,
beating the Queensland Reds by a single point at Concord Oval. The Reds led by
five points at the break, but the Warratahs fought back to claim a 31 points to
30 victory in the clash of the competition heavyweights. ABC News. ABC Radio
weather update. Tonight for the southeast, partly cloudy conditions, but no rain
expected. Overnight temperatures in the mid to high teens. For Monday, partly
cloudy conditions continuing. The chance of a shower if you're along the coastal
fringe. 30 degrees expected for Brisbane, 31 in Ipswich, 27 on the Gold Coast
and 29 on the Sunshine Coast. Looking ahead, cloud and coastal showers are set
to continue into the week. Daytime temperatures will range from the mid to high
20s. The godness on the mountaintop. Hello, welcome to the final hour of
nightlife on this Sunday night. Suzanne Hill with you on ABC Radio right across
the country and even in the Netherlands where Casper from the Netherlands has
joined us to co-host part two of the Dutch Australian history quiz Casper,
welcome back. Thank you, Suzanne. All right, ready to resume your co-hosting
duties. Absolutely. I just remembered that I forgot to say something else about
Harry van der Veezybeet. Last week I cycled down to the street where he grew up
just to have a look and feel the connection with this Dutch Australian history
bit. And it was nice to be there and to see because I know that in the cellar
beneath the apartment block where he lived he used to practice with his first
then. So I thought that was nice to see. All right. Now I might say this
question, we're on question 15. John from Irrimpels got it. KJ at Penrith has
the answer as well. Let's see if Cassandra in Shoalhaven can help us. Good
evening, Cassandra. Oh, good evening. What's the question? Hello, Cassandra.
You're a bit faint there, Cassandra. Oh, hello. Oh, can you really speak up?
You're very, very faint. Oh, you're not on your speakerphone, are you? I'm on.
Oh, that's better. That's better. Is that better? That's a lot better. Were you
on speakerphone? Oh, no, I had it upside down. Well, that will do it for sure.
All right, Cassandra. Kasper, I'll get you to repeat the question, please, for
Cassandra. Yes. In 1980, a famous Australian musician married a Dutch woman with
whom he is still married today. They have a house in a quiet rural village in
the Netherlands, nicknamed the Jerusalem of the Achterhoek, due to the rich
variety of Protestant Christian denominations and to its location on Seven
Hills. Who is this legendary musician? Cassandra, who is it? Holy moly. I'm...
Wow. Okay. Have a guess. Australian musician, let's say... It's not Jimmy Bass.
Well, you know it's a man, and he's really famous. That's not your guess. It's
not Jimmy Barnes. No, I'm saying it's not my guess. Definitely not Jimmy Barnes.
Come on, try someone at us. Let's say Ian Moss. No, it's not Ian Moss, I'm
afraid, Cassandra. Oh my God. All right, thank you. Thank you. Good to talk to
you, Cassandra. Henry in Queensland. Hi, Henry. Hello. Hi. Hello, Henry. Henry,
do you know who this legendary Australian musician is? No, before that, I want
to talk to Casper, say that, yes, I've been through the Netherlands on a
European tour one time. We stayed in Amsterdam overnight, and then by a very,
very happy coincidence, and that's all it was, the Floriade was in full cry at
that time, and our tour was ordered to give us a tour a day at the Floriade, and
it was wonderful. I really appreciated that. That's very good to hear, Henry. It
was lovely. I'm happy to hear that you had a lovely time over here. All right,
Henry, try and figure out who this musician was who married the Dutch woman in
1980 and still lives in the Netherlands. Paul Granger, the Australian musician.
Paul Granger? No, no, he's not. Very famous, this person is. It's a funny thing
that I think this, you could actually argue that this is internationally the
most famous Australian musician. Do you think? Potentially. All right, Brian in
Rockhampton, I'll let you have a go at this. Wonderful. Hi, Brian. I think an
Australian musician who's married to a Dutch lady is living overseas, is that
correct? That is correct. Well, I believe he actually has several houses, one in
Australia and one in the Netherlands and one somewhere else. So he's probably
living in Australia as well, part of the time. There's only people that I listen
to are sort of rock and pop musicians, so I'm hoping it's one of them. We'll go
on. Thrice on the notice. Just because he's fairly intelligent in the musical
world with his records and what he does ranging music, let's say someone like
the lead singer of Ivor Davies. That's it. No, it's not Ivor Davies. Well, there
you go. Thanks, Brian. This is almost turning into a West Africa question. Chris
from Coggera, hello. Hello, it's Chris from Eorke, is it? It is. Are you Chris?
Yeah, yeah. I came in late, I'd been asleep and the name I came up with, I
think, Casper was mentioned right after the news, which was Harry Vander. So has
anybody actually mentioned Harry Vander? Now, Chris, Harry Vander was the
answer, was the subject of an earlier question. Oh, yes, well I was just… No,
yes, you said, so I'm going to let you have a go and have another answer. Okay,
a famous Australian musician. So it could be, so we're talking an
instrumentalist rather than a singer, are we? It's a famous Australian musician,
that's all I'm going to say. Very well known. Oh, well look, I'll just say
Johnny Farnham. No, it's not Johnny Farnham. It's alright. Okay, then. Thank you
very much, Chris. Errol. Hello, Errol. Hello, Suzanne. Hang on, we've got to
work this out. You have been waiting a good hour there on the phone, so look, I
reward you for your patience or I thank you for your patience. Hello, Errol. How
lovely to talk to you again. Yeah, you too, Casper. Before we go on to this one,
which I'll fail on, tell me, in 1942, what were the letters ABDA mean to you?
ABDA? 1942. 1942? I don't know. I know ABBA in 1974. That's ABBA in the
Eurovision contest. That was the Australian British Dutch, no, American British
Dutch Australian command that was set up to take charge of the World War II, in
case it's Japanese. I thought you'd know that. I didn't know. I know that there
was an organisation like that and I also know where it was, but don't say that.
But I didn't know this letter combination. Alright. I mean, I know you've waited
a long time, Errol, but we do need to get an answer, please. Only an hour.
That's fine. I don't care. When is that musician? I don't know whether... The
famous Australian musician, Errol. Just find someone famous. I don't know
whether it's actually a classical musician or not. It's not a classical
musician, okay? Okay. He married in 1980. Okay. So, well, we'll go... He was
relatively young at that time. The other member of the Easy Beat, Dutch
Heritage, I'll go for him. No, Errol. No. No. More famous than that. Okay.
Alright. Goodbye, Errol. Bye. Mike in Oak Park is up next. Hello, Mike. Hi,
Suzanne. Hi, Casper. Hello, Mike. Hello, Mike. How are you? Moto in Oak Park has
this. Good. Just before I'll get my two bobs worth in, many years ago I was
touring with musicians through Europe and worked at the Jap Eden Haller in
Amsterdam. Also, a couple of nights at the Milkveg, which I'm sure many people
would... Oh, really? Yeah. That was with the band... Milkveg, famous place,
yeah. Yeah, that was with a band called Camel, but I think I'm probably a bit
older than you. Anyhow, I think the answer to the question is Angus Young, by
the way. Get on one of the Yes, well done Mike in Oak Park. It was indeed Angus
Young from ACTC. Yay! Woohoo! I've got question 16 for you. In 1957, Dutch
immigrants in Australia founded the Hollandia SC. Hollandia SC is the
predecessor of which current team in the A-League. Central Coast Mariners, I'll
just take a guess at that. No, it is not the Central Coast Mariners, I'm afraid.
Okay, well thanks. Nice talking to you guys. Take it easy. See you. Thank you,
goodbye. Thanks very much Mike. Let's talk to Tony in Ballarat. Hey Tony. Hello
Suzanne, hello Keffra. Hello Tony, how are you? I'm pretty fine. Just<|de|><|tra
nslate|><|de|><|translate|><|de|><|translate|><|de|><|translate|><|de|><|transcr
ibe|> a wonderful picture about the world. No, it is not the Western Sydney.
Thank you. Thanks very much, Tony. Good to chat to you. Pat in Canberra. Hello.
Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi.
Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi. Hi my I heard that.
Yes, I do text quite regularly while I'm working. Fantastic. Alright, Rebecca,
let's say you've rung in in time for the final question, so you could be the
winner of the Dutch Australian History Quiz, which I think would be apt because
I guess you're Dutch Australian now. We want to know what the job of Van Diemen
was in 1642 that meant that he was important enough to have Van Diemen's land
named after him. We know he wasn't a navigator. He wasn't a Dutch politician.
What did he do? Oh, God. I do recall this learning at school. I'm either
thinking he's an explorer or a botanist. That's the only two things that's
coming to my mind. Well, which one of them are you going to pick? I'm going to
go botanist. No, not a botanist and I'm going to exclude explorer for everyone
else as well. Rebecca, I'm sorry it would have been so apt if you had been our
winner tonight, but it wasn't to be. Absolutely. Yeah, nice to hear from you.
Maureen in Bentegei. Hi, Maureen. Hello, he was the governor of the Dutch East
Indies. Well done, Maureen. You are the winner. I didn't say hello to cats. No,
no, you can do that now. I just wanted to get the applause in sort of
instantaneously. You can say hi to cats for now. Hello, Maureen. Congratulations
on winning the quiz. Yes, I just sprang up. I've been listening most of the
time. Excellent. All right, Maureen, have you been to the Netherlands? No, no,
but I have been to Indonesia. Where did you go? I went to Bandung in Java. For
vacation or? I had relations living there at the time. All right, Maureen, well,
I will pop you back on hold and we will get the details so we can send you out
this fridge magnet. And Kasper, for you, well, looks like I've promised Tim
Tams, haven't I? So we'd better go and get you some Tim Tams and get them posted
out to you. Well, thank you. So Kasper, thank you so much. You put a lot of work
into this history quiz. You did a lot of research on the questions. When we went
to fact check them, they were absolutely spot on as we would have expected. So
thank you so much, Kasper. It's been fabulous. I know a lot of listeners have
really enjoyed this chance to interact with you. I really enjoyed it. Thank you
very much for giving me this opportunity. And it's really a special experience.
And thank you. Thank you, Australia. And I really enjoyed it. All right. Thank
you, Loughlund as well, of course, the producer. Yeah. Thanks, Lachie, for
everything you do. Good night, Kasper. Good night. Thank you. Well, that was
Kasper in the Netherlands. And I'm sure you'll hear Kasper again as a contestant
on the quizzes here on Nightlife on ABC Radio. Thanks so much to everybody who
texted in and took part in the quiz tonight. You, my friend, are a stone cold
maniac. And let's get ready for some fun. It's extreme comedy gold. I know. And
you can stream it all for free on ABC IQ. New music. For new music, we are
joined by the wonderful Bridie Tanner, who is currently presenting the Breakfast
Show at ABC Radio in Lismore. Hello, Bridie. Welcome back to new music. Suzanne,
great to speak to you again. And I think this is the first time we've talked to
you since we solved your problem. The last time you were with us, you'd had that
piece of music in your head for about, what, 20 years? Yes. And the Nightlife
audience helped you out. Tony Childs. It was indeed Tony Childs. And I played it
the next day. And then Tony Childs came to Lismore. No. Yes. And did you tell
her? I didn't see her, but apparently she was in this seat that I'm sitting in
right now, recording something for Sydney. Let's have a little listen. Got to
the beat. Got to the beat that had been in your mind for years and we finally
figured out what that was for you. And I'm not surprised at all why it was stuck
in my head for so many years. She does that hook about 60 times in the song. But
because she's growling in her voice, you can't quite make out that she's saying,
don't walk away. Yeah. Anyway, our audience was old enough, Bridie, that we
could solve that problem for you because we all heard it the first time it came
out. I'm forever grateful. As you should be. Thank you, Nightlife audience. Now,
instead of older music, we have new music that you're going to bring us tonight.
What have you been listening to that you want to share with our audience? Yes, a
triple threat of new Australian music from some of my favourite artists. And
we're going to start right here in the Northern Rivers. Byron Bay's answer to
Jack Johnson is Bobby Allou. His Samoan roots come through in his music with
beautiful ukulele strumming, drumming. I'm slightly in love with him. But he is
such a chill guy and he's written, he's just released two new singles. This is
one of them. Take Time to Take Time Out. The thing I love about this song is
that it reminds me that when you're packing up to go away, you know, Anzac Long
Weekend or Easter Long Weekend and you're in this, you know, kind of frustrated
frazzle of like we've got to get there, we've got to get the ice in the esky,
where's the directions, we're out of reception. All that stress that builds up
just before you're about to have a getaway or a holiday. This is about taking
your time and just going with the flow and actually making that time to have a
break is such an important thing. So it's really going to chill you out. I think
it's the perfect way to kick off our three new Australian music, Triple Threat
tonight on Nightlife. Thank you, Suzanne. This is Bobbie Allou with Take Time to
Take Time Out. Take Time Out. Take Time Out. Take Time Out. Take Time Out. You
are enough. Always the right time to take time out. Making a good life. Over the
ranges, a clear night sky, something wonderful. You are enough. You are enough.
You are enough. You are enough. You are enough. You are enough. You are enough.
You are enough. You are enough. You are enough. You are enough. You are enough.
You are enough. You are enough. You are enough. You are enough. You are enough.
You are enough. That has a lot of energy, Bridie. Cat Empire, their new song,
Thunder Rumbles. Don't you wish you were in a festival audience without going
off and everyone just dancing their bums off? I do. Maybe not right now at 1.30.
I suppose it might be the time to be doing it. I reckon for anyone driving the
roads this late at night, that would have had a few bums wriggling in seats, at
very least. Now, Bridie, before we move on to your third tune, you have, because
Bridie used to work with us in Sydney, now she's working in Lismore, and you've
very much been embracing local life to the point that you and everyone else at
the station are trying to figure out what kind of entry you might be able to put
into the local show which is coming up. Tell me more about this idea and this
project. Yeah, perhaps inspired by the Sydney Royal Easter Show, I want to win a
ribbon in the show. I want a best in show ribbon for something. I don't know
what it is, but I want something. And the Grafton show is coming up, so is the
McLean show here in the North Coast. And I reckon there's a category, or maybe
we make a new category, that I have a shot in heck at winning. Now, knitting,
crocheting, cooking, I could give it a crack, but there are people who have been
entering the show religiously for many years who are just going to absolutely
mop the floor with me. What is a new category, or maybe just a little bit of an
underground category in show entries that I have a chance at getting a ribbon
in? That is my mission at the moment on ABC North Coast, to find out what that
category is, or just straight up make a new category. So I don't understand.
What gives you the idea that you can just make up a new category? You can go,
like, I look at this paper mache I've made, I'm going to win the paper mache
category if they don't already have one. How are new categories created at the
show? Well, one of my colleagues here at ABC North Coast, Eloise Farrow-Smith,
had a chicken that she entered in the Bangalow show. And in the poultry section,
they're quite specific about breeds and purebred chooks. So when the judges came
up to her and quietly said, Eloise, I think you've got a mongrel chook here.
They must have felt bad for her, Suzanne, because they ended up making a mongrel
chook category in the show, and her chicken won first place. First of only one
place? Yes. So you're trying to play on potentially the sympathy card. The
sympathy card. Or maybe I can get into the children's lane, go into a
competition that mostly children enter and mop the floor with some kids. I don't
know. I'm thinking decorated vegetables could be my calling. Not a category
already, one you want to invent? I'm going to have to find out. I'm going to
have to get back to you on that one. But I think, you know, I don't see many
decorated vegetables. You see big vegetables. But who's decorating them? This is
one of the suggestions that came through on our text line. Somebody entered a
cucumber porcupine with little toothpicks as the spines of the porcupine and won
the show with that. Now that was a child's entry, but I reckon if I can come up
with something truly exquisite, decorated vegetable-wise, I might have a shot at
a ribbon at the Grafton show this year. Right. So this is now on your bucket
list. I think this is, I mean, I just think relying on the sympathy card to
invent a new category, I think maybe the underground, the sleeper category is
what you want. So maybe people know what might be in those sleepers. The ones
that hardly anyone enters, right? So you can basically be the sole entry in that
category. Yeah, I'm not going to win a ribbon for fruitcake. No, no. Or scones.
But maybe there's some obscure photography competition or something that I can
enter in the show and have a chance at winning. I don't know. I want help
though. Well, look, if you've got an idea for Bridie, you can SMS it in and I'll
pass it on. 0467 922 702. And Bridie, if you can't win that ribbon, I mean, are
you going to be okay? I'm going to keep trying. There are many shows throughout
the year and I'm a bit of a show junkie. So Casino show, Bangalore show, McLean
show. I mean, there's plenty of opportunities to keep trying. But I don't know.
I've just got to, I just, I just, I really want it. You know, I want a ribbon. I
don't care if it's best in show or second place or runner up. I just want a
ribbon from a show to add to my portfolio. It's my new life mission. You know
what? My new life, well, not a new one, but the one thing I've got on my bucket
list is I want to be on a jury. It's never happened, but that's the thing I
just, but I can't make it happen. I just have to wait for the magic moment.
You're going to get that letter next week, I reckon. Then I'll have to take time
off. So maybe, I don't know. Just, you know, I just want to know that it's going
to happen one day. That would be good. You're wishing for it. Thanks, Bridie.
All right. Maybe we should play the final new music piece that you have for us
tonight. Who's the group? The group is called Ripple Effect Band. I was a fan of
them and then I saw them live and I didn't realize this, but it's an all female
rock band from Arnhem Land and they absolutely rock. They're from Manningrida,
which apparently has these beautiful cliffs looking out to the sea. Now this
song, Waleah, is all about the healing experience of standing on those cliffs
and looking out to the sea. I'm really keen to show you this because the song
was originally written in 1990, recorded onto a cassette and they've kept a bit
of that original recording at the very start of this track. It's Waleah by
Ripple Effect Band. Well, Bridie, thank you so much for joining us for new
music. Good luck with the show. I guess next time we chat to you, we will find
out how you went. Sure. Give me your suggestions as well for category center.
Help out Bridie, 0467 922702 and here's the Ripple Effect Band and Waleah. Yeah.
Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah. Yeah.
Yeah. Yeah. Yeah. Yeah. Yeah. Yeah.