TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-16-11H00M.MP3
--------------------------------------------------------------------------------
 Oh yeah, but I like musical theatre and I've been, you know, I've been, you
know, I started off in musical theatre and performed overseas in musical theatre
and they said, well, give us a couple of bars. And so I sang and then they
called me in to audition and that was the song. Brilliant. Absolutely brilliant.
Felice Arena is my first guest. As I mentioned before, Pip Williams is my
second, but we do have to just have a little look at the world view right now.
Felice will be back with two more songs. ABC Radio. There's lots of possibility.
There's a lot of amazing people here. I love being here because I learn
something new every single day. Great place to live, wouldn't have anywhere
else. That's just lovely. You almost get a bit teary to be honest. This is ABC
Radio. ABC News with David Rollins. Heavy fighting continues on the streets of
Sudan as the military and arrival paramilitary group battle for control of the
country. Larry Butrose has the latest. A human rights advocate in Khartoum says
heavy fighting is raging there for a second day. He says there are fierce
clashes around the military headquarters, Khartoum International Airport and the
state television center. This follows yesterday's battles described as all-out
war in residential areas. Doctors say 56 people are already dead and nearly 600
wounded. UN Secretary General Antonio Guterres is urging a ceasefire, but
there's little sign of that happening at present. Indeed, there are reports of
fighting spreading to other parts of Sudan. Sudan's Prime Minister Fumio Kishida
has denounced yesterday's smoke bomb incident at a port in western Japan. A man
was arrested after allegedly throwing a smoke bomb into the assembled crowd not
long before the Prime Minister was due to speak and a second device was found
later. Mr Kishida says Japan must not allow acts of violence that strike at the
foundation of democracy. The suspect is so far refusing to answer questions
about the incident. The Northern Territory Police Minister says the Coalition
has failed to report its repeated claims of Indigenous children in Alice Springs
being returned to their abusers. Peter Dutton and Senator Jacinta Nambachimba
Price have been accused of playing political football over claims that sexual
abuse of Indigenous children is rife in the area. The peak body for Aboriginal
and Islander children has refuted their claims. Kate Worden is urging Senator
Price to back up claims with evidence. You can't just claim these things and
then walk away. This is classic drop a bomb, walk away. She needs to come
forward and make some declarations around what evidence she has that supports
that view. There's been a surge in road deaths across Australia with a nearly 6%
increase in fatalities over the past year. Kate Ashton has the story. There was
an increase in road deaths in every Australian jurisdiction except the Northern
Territory in the year ending March 31st. Around the country 67 more people died
on the roads compared to the year before. There was also a significant rise in
pedestrian deaths up 23%. The Australian Automobile Association says the reasons
for the rise in pedestrian deaths is not yet clear. It says more data is needed
to better understand and prevent the upward trajectory of road deaths. WA
Premier Mark McGowan has condemned a riot at Perth's Casuarina Prison overnight
which resulted in several custodial officers being injured. Three detainees were
engaging in recreational activities when they assaulted the female supervising
officer, hitting her in the head with a makeshift weapon. They then took her
keys and opened up several accommodation units. Up to 13 detainees retrieved a
ladder, escaped into a roof cavity and climbed onto the roof. Mr McGowan says up
to seven guards were injured, with one sustaining a broken foot. 50 people have
been arrested after protesters entered a rail corridor and stopped a loaded coal
train near the port of Newcastle. The protesters stood beside the rail line at
Sandgate, forcing the train to come to a halt this morning. More than a dozen
people climbed on top of a train carriage when they or where they remained for
almost three hours. Newcastle City Police District Commander Superintendent
Kylie Endemi says most people protested peacefully though. 47 persons were
issued with Field Court attendance notices at that location and three other
persons have been arrested and returned to Warratah Police Station. Two of those
people will be charged with malicious damage and one person will be charged with
an assault on a security guard. Three-time Olympic equestrian Vicki Roycroft is
in intensive care at Sydney's Royal Prince Alfred Hospital after a suspected
heart attack. The 68-year-old collapsed just before she was due to compete in
show jumping at the Sydney Royal Easter Show on Saturday. Her sister says she
has a tear in her aorta and has had an eight-hour surgery. Australian golfer
Grace Kim credits Carrie Webb for helping her to a maiden LPGA Tour win in
Hawaii. The 22-year-old rookie birdie the final two holes for a four-under-68
final round forcing a three-way playoff with China's Youlu and Korea's Yu
Jingsung. She joins a list of Australian LPGA Tour winners which includes the
Queenslander Webb. Kim says it's a huge milestone in her career. I think really
just kind of puts the cherry on top of my career. I know it's just the beginning
but seeing how much they've really motivated me to get to here is really
exciting. This is ABC News. With the ABC Listen Up you can take the footy with
you wherever you go. Off to walk the dog. Go in camping. Picnic in the park. ABC
Sports expert coverage of the AFL and the NRL. Every goal, try, mark and tackle.
Live and commercial free. So whatever you're up to, take the footy with you on
the ABC Listen Up. On ABC Radio this is Songs and Stories with Brian Nankervis
where music and passion are always in fashion. Songs and Stories is the show,
Brian Nankervis here, Felice Arena. Over there, Felice, those stories of you
being in London auditioning for the West End. So you got into hair. I did and it
was probably the best thing for me to do really because I was on air with
neighbours twice a day right. So everyone knew my face, knew who I was but I
didn't know a soul really. And I needed some sort of foundation, some sort of
base and friends and really that came through theatre and I finally felt like
I'd met my tribe or people. Tribe is the perfect thing. But the music of hair or
the sensibility of hair, it's a fair way from John Farnham. It is, it is and
especially when my character had to sing the holy orgy song and it was kind of
embarrassing. He played wolf, he had a couple songs but it was like, oh my
goodness right and we do have to get naked in this show and there were 900
people in the audience but they invited 100 people to come on stage, if you paid
a little extra of course. And I had my parents, I flew them over for opening
night and sent them back to Italy. They hadn't been back since they emigrated,
this was a big moment for their first born to fly them over to see their son
perform in a West End show. And I thought right, they're in the front row, I
won't get naked in front of them, I'll turn around and face the people on stage.
There's only 100 of them. Not realising that you could see every expression on
their face. I think I mumbled through it from the second night onwards I was
back to the main audience again. Of course. Of course, of course. And so being a
part of the West End, I mean that's exciting too. It was and you know what, it
was a world away and it was probably the best thing for me. I only had a year
contract on Neighbours and to come out of that I could have gone a lot longer in
that but I think this summer was looking out for me because I went back to my
first love of theatre and performing live and from here I went on to Godspell
and then I did a rock and pop concert tour of What a Feeling with Irene Cara and
you know growing up in Qyabrum I watched Flashdance and Footloose and all those
songs and singing to What a Feeling with Irene Cara and watching her in Fame.
All of a sudden I'm co-starring alongside Irene and back then a lot of people
didn't know what was happening back home, family and friends. I just did this.
You're on the road with Irene Cara. And we toured up and down the UK and we came
into the London Palladium. The London Palladium, I mean I pinch myself now and
think who was that person? I mean so long ago and I know it was me but it feels
like someone else you know and but again the 90s, being in London in the 90s was
an exciting time and it was a sort of a small community in the arts in the West
End and pop music. There was Take That and the Spice Girls and everyone was
around and we all did smash hits, photo shoots and we all got around and met
each other and there was one particular time during hair, I mean if you're doing
a West End show a lot of celebrities seem to come in. I never saw myself as a
celebrity. I just saw myself as someone who was pretty lucky I'm doing something
I really love and thinking oh my god Robert De Niro is in the audience or
someone else is in the audience or whatever is in the audience and you would
hear whispers in the wings and of course sometimes it wasn't. It was just you
know people getting overexcited but there was one particular person I think that
leads up to my the second choice of my song. I was in hair it was a wonderful
cast and I'm still in contact and still friends with that cast and it's coming
right up to 30 years and you think oh my god I can say 30 years. I performed
with a girl called Pepsi. Pepsi and Shirley were the backing singers of Wham!
They were the backing vocals right and they had their own pop career as well and
in that cast there was Paul J Medford and Sunita who had So Macho a pop star
with the Stockwaterman Aiken group as well and it was a wonderful cast of all
backgrounds have come together. One night in my dressing room so I had a
dressing room I would always be in Sunita's and Pepsi's dressing room talking to
them you know get there a couple hours before the show have a cup of tea.
Routine. Routine. Going through the rituals right. One night I went in there and
they weren't there but sitting there was George Michael right and you go oh
hello oh oh oh wow ah George Michael oh what's an Australian doing in this show?
What? Oh what? Yeah well you know neighbours here here I am talking to George
Michael. I've got to go just borrowing a brush thank you very much and off I
went back into my dressing room going holy cow. So was there a little bit of
attitude there? No no attitude he was being flirty, flirty, flirty fun and you
know his best mates with Pepsi and they I went back in there again the second
night he's there again third night he's there again. Just during previews so two
weeks of previews leading up to opening night by the second by fourteenth day
you go good day George good day Fleeche how I move on and it was that sort of
represented what the 90s in London was all about I mean I'd end up at nightclubs
and there were supermodels and tennis stars and you go oh my goodness and I try
not to point I tried to act very cool. A young boy from Kyabra. I was still
trying to give it my best cool but just smiling away and you know George and I
got to talk and a lot and talk about music and the whole thing and throughout
that whole period of the 90s for me and singing with fellow cast members there's
nothing like it's such a high I mean it's something to sing solo right when
you're singing solo you're in another state it's an out of body experience but
to sing with a cast together a collective group to sing people who sing in
choirs know about this it's so uplifting and moving so this particular song that
George sings or he sang it was one of my favourite George Michael songs
represents not only what London was for me because he was a part of that but it
was the soundtrack of my 90s London days but the choir the gospel choir that
kicks in throughout the middle of the song that takes me back to my fellow cast
members in God's Bell in hair in what a feeling I miss them all we were all it's
youth it's young we're all gorgeous looking great we're jumping up and down
that's what you should be doing in your 20s and I love it and I still have fond
memories to this day. And it must have broken your heart when you heard that
George had passed away. Tears just dreaming tears tears because it's a moment in
time again right when someone you know or you've met or something or they
represent a time in your life you might not know them but they represent that's
what music does they represent a time in your life. Absolutely George Michael
with Mary J Blige and it's a cover of I believe a Stevie Wonder Stevie Wonder
that's right and I love the Stevie Wonder version I really do but the way George
does it if you don't if you haven't seen the video for a long time go and have a
look at the video because it is so cool and it really does it really does
symbolize London in the 90s. You can't reveal the mystery of tomorrow but in
passing we grow older every day this is This is all as born as new, you know why
I say it's true, but I'll be loving you always. Until the rainbow burns the
stars out of the sky, until the ocean covers every mountain high, until the day
that it's hot, until the day that it's warm, until the day that it's the day
that I know. Oh, did you know that true love asks for nothing? Her acceptance is
the way we pay. Did you know that life has given love a guarantee to last
through forever, another day? As today I know I'm living for tomorrow, could
make me the past that I mustn't fear, cause you're here. Now I know deep in my
mind the love of me I felt behind, and I'll be loving you until the rainbow
burns the stars out of the sky, until the ocean covers every mountain high,
until the dolphins fly and parrots live in the sea. Loving you until the dream
of life and life becomes a dream, now ain't that loving you? Until the day is
light and night becomes a day, until the trees and seas just up and fly away,
I'll be loving you forever. Oh, did you know that true love asks for nothing?
Her acceptance is the way we pay. Did you know that life has given love a
guarantee to last through forever, another day? As today I know I'm living for
tomorrow, could make me the past that I mustn't fear, cause you're here. Now I
know deep in my mind the love of me I felt behind, and I'll be loving you until
the rainbow burns the stars out of the sky, until the ocean covers every
mountain high, until the dolphins fly and parrots live in the sea. Loving you
until the dream of life and life becomes a dream, now ain't that loving you?
Until the day is light and night becomes a day, until the trees and seas just up
and fly away, I'll be loving you forever. Until the rainbow burns the stars out
of the sky, until the ocean covers every mountain high, until the dolphins fly
away, until the day is light and night becomes a day, until the dolphins fly
away, I'll be loving you forever. Until the day is light and night becomes a
day, until the ocean covers every mountain high, until the day is light and
night becomes a day, until the day is light and night becomes a day. George
Michael and Mary J. Blige. It was from 1999. They covered the song for Michael's
greatest hits album, Ladies and Gentlemen, the best of George Michael and my
guest Felice Arena. They were pals and that's a beautiful... I don't know so
much about pals, but we crossed paths and we chatted and danced and it was the
heady days, right? Of course. And I was just grooving along then. Can you say
grooving in this video? Of course you can. I was. And of course it takes you
back instantly in the first couple bars. You're right there. I'm back there at
the Bank nightclub at opening night. We had the party there and everyone was
there. I put mum and dad in the cab. They went back home. Of course. It's going
to be a big party mum and dad. See you later. It was in London, if I've got this
right, that you sent a story which then led... I mean because you had this
incredible career as an actor on Neighbours performing in the West End, but I
reckon most people probably see you as the author of... Yes, of course....Specky
McGee and all these incredible books. And you were in London and you wrote a
story that sort of connected you back to country Victoria. I did. And I think
people are surprised. That's why I often say it's to the parallel lives of
Felice Arena, I think. So I see that as my youth and then most people would know
me for Specky McGee or writing books. My downtime after being on stage and
performing and being showman Felice, downtime, I could ground myself by just
writing and drawing. And until this day, that's how I relax. You said it earlier
on in the show, you know, rituals, finding somewhere to sort of come down and
just be still. And we're so fragmented these days, more fragmented than ever,
that sometimes I have too many tabs open. And the only way I can close those
tabs is to be able to just sit down and write. And that's always been with me.
Maybe that had come full circle from my primary school teaching days, the fact
that I loved story and I loved performance and read aloud to my students when I
was an emergency teacher for a year after Bendigo Teachers College and then
neighbours took off. That's always been there. I didn't know it was going to
grow into what it was. I mean, if you had come to me in 1996, I said, no, this
is my path. I am performing. This is it forever now. But the words start to take
over that other side of of me started to grow. So I wrote a story, sent it into
a literary agent. Literally, I actually wrote it during performing in God's
Bell. I wrote it longhand, a page each night, a page each day in my dressing
room on the road. I then had it and it was about a dolphin and a swimmer set in
Australia. It sort of hark back to my days of growing up in country Victoria,
feeling nostalgic, feeling like, could I do this? Could this is this good
enough? I don't know. Swimming in an irrigation canal. Oh, you've done your
research. Yes. Swimming was my sport growing up. I mean, I played footy and
soccer, of course, but swimming was my sport. And when the pool closed early in
the summer, I would train in the irrigation channels from Bridge to Bridge,
pretending I was a dolphin or some sort of sea creature to get myself through it
using my imagination. So you sent the story to an agent. Sent the agent and the
agent looked after adults who just wrote for adults, not kids. He was expanding
his books. He was in advertising. An older, pompous old Englishman. But he was
he was lovely. He read it. He said, I think you have something here. I think I
mean, I was out of neighbors and he knew from from me on West End. And that
obviously helped. But he said, I think you might have a talent for writing. And
he didn't know that I had a teaching background. I said, well, I used to read
out loud and write stories for the kids. And he said, I think you might have
something. But we'll type it up. I mean, I didn't know how to type at the time.
So they typed it up, sent it off. And I was offered in 48 hours a two book deal
with one publishing company and another book. That literary agent at the time
took on someone else six months later. OK, yeah, this is one of those sliding
door of moments, right? Took on J.K. Rowling with Harry Potter. And it was
bizarre because Harry wasn't the beast as we know it as when it was first
released. My book about the dolphin was released. Harry Potter was released. It
didn't become the beast until the Americans got hold of Harry Potter a year
later. And then Warner Brothers bought the rights that it exploded. It was a
great thing for children's books and children's writers right around the world.
Because back then, the Children's Department was at the back of the publishing
house because of Harry Potter. It brought it to the to the fore. And it was good
for all of us. I they offered me I went for the two book deal and didn't know
what the second book was going to be about. So I wrote about growing up in
Caiaprum again, about boys on bikes and writing. And I kept going and I thought
with the acting and eventually I ended up coming back home. And I started
writing Specky McGee and Specky took me down another path. You never look back.
Do you miss the performance? Do you miss the stage? I do. But you know what? As
I said, I haven't stopped performing. I mean, the audiences are just younger.
And so I've had teachers or parents go, have you ever thought of acting? You're
very theatrical, but they don't know this backstory. So I try to bring my
stories to life, you know, bring these words to life. So they only have an hour
with the kids usually. And I'm booked through Specky's agent to speak in front
of large audiences of young kids. It's a performance. It's a performance. It is
a performance. It is a gig. I'll trial out new material like a comedian or
performer and see if that works. I have favorite schools where I might call up
the teacher and say, oh, look, I've got something new. I want to just trial it
out before I head out to the Sydney Writers Festival or something else. I'll do
that. I call it my off Broadway performances. Brilliant. Let's go to your well,
before we go to your last song, we sadly run out of time. We must mention the
new book. It's called Pasta. Pasta. Yes. Is there was there? Did you spend a lot
of time? This is a performance book. It's going back to my performing again,
because it's all the read aloud. It's a rhyming and you can get on a matter of
rhythm, repetition. It begs to be read aloud. So I want parents. I want
grandparents and teachers to perform, to have fun with this. Use all the Italian
words and even if you don't know Italian, the rhyming words, put on the accent
if you have. Bring this to life. It is an ode to the food we all love to eat,
but also to my Italian heritage. And I'm happy. It's so much joy with this book.
I've had so much fun with reading this aloud to at schools and to kids and the
five, six, seven year olds. But even the adults are getting into it. So pasta,
yes, by a firm press. And it's just been a joy. Excellent pasta. All right.
Let's go to song number three, the final one. This one came right out of left
field, but I was very, very happy. Well, you know what? This one, I think for
me, it was just really sums up who I am. And I think for a lot of us, it comes
hand in hand with hope. There's a lot of hope in my stories. Maybe the kid who
dreams of being a footballer or my historical novel characters. There's hope in
that at the end of my stories. I think everyone needs hope. It doesn't matter if
you're five or thirty five or sixty five or eighty five. We still need to wake
up for some sort of hope. And we can only do that with by manifesting or
imagining where we can be. And this one, you know, it really sums up my
childhood, but it sums up my child self as well. And I loved it. And I remember
seeing it. I loved it. And I've always loved it. And it's it's pure imagination
by Jim Walder from Willy Wonka and the Chocolate Factory. Perfect. Absolutely
perfect. Felice Arena, thank you so much. I reckon you should put on whatever DJ
voice you like and I will press the button for this song. Would you like to
introduce it for us? OK, let me take you back to your childhood way, way, way
back where there's hope and love and dreams. Here's Pure Imagination by Gene
Wilder from Willy Wonka and the Chocolate Factory. Thank you, Felice. Ladies and
gentlemen, boys and girls, the chocolate room. Hold your breath. Make a wish.
Count to three. Come with me and you'll be in a world of pure imagination. Take
a look and you'll see into your imagination. We'll begin with a spin traveling
in the world of my creation. What we'll see will define explanation. If you want
to view paradise, simply look around and view it. Anything you want to do it,
want to change the world. There's nothing to it. There is no life I know to
compare with pure imagination. Living there you'll be free if you truly wish to
be. If you want to view paradise, simply look around and view it. Anything you
want to do it, want to change the world. There's nothing to it. There is no life
I know to compare with pure imagination. Living there you'll be free if you
truly wish to be. The final song, pure imagination, Gene Wilder from Charlie and
the Chocolate Factory. It is that time when we invite you to call in. It's
mystery voice time. You know how this works? I play you a little snippet and you
identify that voice and you get to choose a song from the artist. Just have a
listen to this little snippet. Get on the blower now. Hi Ralph. Hey Lover.