TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-03-27-12H00M.MP3
--------------------------------------------------------------------------------
 ABC News with Glenn Lauder. An environmental policy expert says there's no
evidence to suggest a deal between the government and the Greens over the
safeguard mechanism will bring on an energy crisis. The safeguard mechanism aims
to cap the emissions of the nation's highest polluting companies and is seen as
key to Labor's pledge to reduce emissions by 43 per cent by 2030. The opposition
claims it will lead to higher prices for consumers and make the economy weaker,
but Australian National University Associate Professor Christian Downey says he
doesn't believe there's any merit in that claim. Australia is very well
positioned to deal with the energy transition. In fact, there's huge benefits to
come for Australia. We have the most abundant resources of solar and wind, which
are already more competitive with new coal and gas. And so this policy
announcement today is only going to encourage more investment in the sector. As
cybercrime experts concern, companies aren't learning how to deal with data
breaches fast enough. As Latitude Financial reveals, almost eight million
drivers' licence numbers were stolen. The loan providers confirmed another
53,000 passport numbers have also been stolen in the hack. The federal
government says it's working with Latitude to respond. Professor of
Cybersecurity at Monash University Nigel Fair believes data breaches are the new
normal. I think we're doing terribly as a nation. Still to this day, so many
organisations of all sizes think they won't suffer from a data breach. And
because of that, they're not practising good hygiene to start off with, but
they're also not practising incident response, but also the communications
response. Superannuation fund NGS Supers also confirmed it's been hit by a cyber
attack this month, which resulted in a small amount of customer data being
stolen. The fund says it immediately shut down its network after it detected the
unusual activity on the 17th of March and launched several cyber security
protocols to contain the incident. NGS Supers not disclosed how many members
have been impacted by the breach, but says savings haven't been impacted and
remain secure. The NSW Premier and senior ministers in the incoming Labor
government will be sworn in tomorrow as they seek urgent departmental briefings
on the major issues affecting the state. The vote count has resumed with a
number of seats still in doubt and Labor still unsure if it will hold an
outright majority. Incoming Treasurer Daniel Mookie says the swearing in of
senior ministers will allow the work of government to begin. Well, of course, a
big priority for us is to get briefed on the status of Northern NSW in terms of
flood relief and repair as well as that. Equally, we are looking forward to
getting a briefing in terms of the Menindee fish kills is a big part of our
immediate priority as well. The suspended Victorian Liberal MP Moira Demings
refuted claims that she condemned the organisers of an anti-trans rights rally
as part of a deal to avoid expulsion from her party room. Leanne Wong has the
story. Victorian Liberal leader John Pesuto had initially sourced to oust the
controversial MP over her involvement in the Let Women Speak anti-trans rights
rally, which was gatecrashed by neo-Nazis. Instead, she received a nine month
suspension and was sacked from her job as upper house whip. Speaking after the
party room meeting, Mr. Pesuto denied he'd backflipped on his original decision
saying Miss Deming had provided new information, specifically condemnation of
key speakers at the rally. But hours later, Miss Deming tweeted her rejection of
the claim saying she'd never condemned anti-trans rights activists Kelly J.
Keene-Minschel and Andrew Jones. A new report recommended more community led
initiatives in the Northern Territory to help divert offenders away from prison
amid an increase in the number of people being incarcerated. The report by the
Smarter Justice for a Safer Territory campaign found 70% of people surveyed
would like to see more programs tackling the underlying causes of crime over
prison sentences. Olga Havnan from the NT Aboriginal Justice Agreement says
tough on crime approaches have failed to address the issue in the NT over
decades. And I would really love to see the Commonwealth and the Territory
government and community organisations be involved in what I would describe as a
long term plan. That's what's needed. You need a 10 year plan, you know, 10 year
commitment for funding and resourcing and some really targeted allocation. And
departing flights from Israel's main international airport have been grounded
following a strike called in protest against the government's planned judicial
overhaul. The strike was called by the country's largest trade union and could
paralyse many parts of Israel's economy. Tens of thousands of people are
expected to be affected by the flight changes. ABC News. Kelly Higgins Divine on
ABC Radio Brisbane and Queensland. Monday night and that means it's Country
Music Monday. And soon you're going to hear from a bloke who's fronted one of
Australia's great rock bands who went on a detour during COVID while he was
driving between Gunnedah and Mudgy. And he ended up recording a country music
album. It sounds a bit like the locals dragged him in and held him hostage until
he put together the album. No, that's not the way it went. But he will be
touring said album. We'll hear what the band is called and their latest song as
well. Although their latest song is from one of their previous albums, but
they've redone it. I'm interested to hear how it's all turned out and I know you
will be as well. There are some great stories coming your way shortly on ABC
Radio Brisbane and Queensland as well. We will have the quiz a little bit later
to another fridge magnet up for grabs. And this is such a great hour. I'm so
happy for you. Outback Local, you'll be meeting a bloke from Thargaminda who's
using that rugged outback landscape to train for one of the most gruelling
physical challenges on the planet. So there's a lot coming up for you tonight on
the ABC. I hope you'll stick with me with Kelly Higgins Divine. And it's seven
past eight. Oh, he took a cigarette out. And everyone else came down to listen.
He said in winter 1963. It felt like the world would freeze with John F Kennedy.
And the Beatles. Yeah, yeah. Hey, hey. Hey. Hey. Hey. Hey. Hey. Hey. Hey. Oh.
Oh. Watch the water. As we follow. To the station. As the train. Rolls. Hey.
Hey. Hey. Dream Academy on ABC Radio Brisbane and Queensland and the beautiful
life in a northern town. Drive with Steve Austin. You're the property council of
Queensland, Jen. You're the executive director. What you've just laid out to me
is an indication that the state of Queensland at a time when there's a major
housing shortage, they're now going to consider capping what they can charge. In
other words, the state looks like it wants it both ways when it comes to house
and land. It would appear that way. Yes, land tax is going to apply to more
property. Join Steve Austin weekday afternoons from 3.30 ish on ABC Radio
Brisbane. And you're with Kelly Higgins-Divine this evening on ABC Radio
Brisbane and Queensland short leave. We are hoped to catch up with Tim Friedman
from the Whitlam's and also the Whitlam's Black Stump Band. Because it was
during the depths of COVID that Tim decided he wanted to make a country album.
And the Black Stump Band isn't just Tim. It features some of Australia's finest
muses, Rod McCormack, Ollie Thorpe, Matt Fell. So let's have a bit of a listen
to the band's first single. It's called Kate Kelly and features the wonderful
Felicity Urquhart as well. Just keep on drinking and you try to forget How they
strung up Joe Byrne to the jailhouse door He looked just like a marionette He
was dead for two days And I'll tell you no lies With the press all around him
making them money Shooting postcards of him through the flies Close your eyes,
Kate I'll sing you to sleep Close your eyes Your dreams will be sweet Kate
Kelly, Kate Kelly Here I am going to sing you to sleep Ned rose up through the
mist A man made of iron Fighting his way to the smoldering inn Where Joey's
brother was dying Had to shoot out his legs Kate, and if you could sleep You
could forget that they cut off his head For the wardens on paperweight Close
your eyes, Kate I'll sing you to sleep Close your eyes Your dreams will be sweet
Don't linger around here May your soul rest in peace Kate Kelly, Kate Kelly Here
I am going to sing you to sleep Now I do horse tricks In a wild west shore Sharp
shooting Kate The last of the Kellys Now the queen of the rodeo Was Joey your
lover? Did he send you some word? A friend to your brother's all the way to the
end Whereas brothers in arms they would fall Close your eyes, Kate I'll sing you
to sleep Close your eyes Your dreams will be sweet Kate Kelly, Kate Kelly Here I
am going to sing you to sleep Kate Kelly, Kate Kelly Here I am going to sing you
to sleep I just keep on drinking And I try to forget How they strung up Joe
Byrne to the jailhouse door He looked just like a Marionette There we go, the
Whitlam's Black Stump Band featuring Felicity Oertkater and they're doing their
Big City debut tour. They'll be at the Princess Theatre in Brisbane on Saturday
April 1st but you can also catch them on the 16th of June in Yermundi and the
17th of June at the Empire Theatre in Toowoomba. And you're with Kelly Higgins-
Divine tonight. We've got the quiz coming up for you shortly. 1300, 222612.
We're going to do it a bit earlier tonight if you would like to play the quiz.
1300, 222612. There is a fridge magnet. Up for grabs. Keep that in mind. Five
questions and two possible answers that I will give you. One of those answers is
correct. The other one not so much. So for example, today is the 54th birthday
of American pop singer who was once engaged to Australian media mogul James
Packer and is known for her signature whistle register. She can sing very high.
She has like a seven octave range, this particular singer. Is that Mariah Carey
or Beyonce? That's our first question for the quiz tonight. 1300, 222612 to play
the quiz. So get on the phone, have some fun and maybe score yourself a fridge
magnet. 1300, 222612. I did the talk back but the horse is driving nuts in the
nighttime and the sands riders know it's a burst but it's still so magic it's
still so fun if you don't pay too much attention so I say who listens to the
radio? Who listens to the radio? Who listens to the radio? That's what I'd like
to know. Who listens to the radio? That's what I'd like to know. Who listens to
the radio? A guy's got a stranger stuck wrapped around his ear He's oblivious to
the traffic, it's not really clear It crackles, it clicks, it pops, it starts
It's a blessing now the time of 40 yards to wrap AMR, FMR, listen to both of
them Listen to the radio since I don't know when Everyone's up, bitch, yeah,
they want it now They're not too particular, just as I say Who listens to the
radio? Who listens to the radio? Who listens to the radio? That's what I'd like
to know. Who listens to the radio? That's what I'd like to know. Who listens to
the radio? Got sitting in the bedroom doing her work She's a fool with a phrase
but she's gone berserk And one has a cigarette resting out the window The other
one's twiddling with the radio, so I say Who listens to the radio? Who listens
to the radio? Who listens to the radio? That's what I'd like to know. Who
listens to the radio? That's what I'd like to know. Who listens to the radio?
Great end to a song, the sports. And who listens to the radio from 1978? The
quiz up next. What's your answer? Pretty sure it's called Saturday Night and
then in brackets it says Is All Right For Fighting or Something About Fighting
Okay, because we do have to have the exact answer And I know the rules Young
with Saturday, that's your final answer Saturday No, and it's nothing from Elton
John Oh jeez Loretta And Loretta Ryan weekday mornings from 5 on ABC Radio
Brisbane You're with Kelly Higgins Divine on ABC Radio Brisbane and Queensland
And it's time for the quiz It's a little early tonight But that's okay And my
first quiz is Justine from the Sunshine Coast Hi Justine Hi How's it going on
the sunny tonight? It's lovely, lovely weather, nice out drive Oh okay, because
normally on the Sunshine Coast when I say how's it going it's like it's raining
Yeah, no we had a break from it so it's really nice Yay Because I'll be able to
see you Cool And you're out and about, sounds like you're in the car Yeah, I was
on my way to get bread for school lunches and then I heard the quiz and I
thought I'll pull over and give it a crack Excellent Because let's face it you
don't really want to make those school lunches do you? No You don't How many do
you have to make? Just two Oh okay that's not too bad Yeah not too bad My
grandmother on my dad's side had eight of them Eight can you imagine? Eight
lunches, that's a lot of sandwiches Yeah that's, I can't even imagine trying to
organise eight children It'd be like I've forgotten your names, are you all
mine? Exactly Alright first question tonight Justine Today's the 54th birthday
of an American pop singer who was once engaged to Australian media mogul James
Packer and known for her signature whistle register, is it Mariah Carey or
Beyonce? That's Mariah That's Mariah? Yeah Let's find out shall we, let's get a
musical yes or no Can you believe that note? I know it's crazy, as soon as you
said that I thought it's gotta be Yeah just incredible so well done It was
indeed Mariah Carey, 54 Gee she's younger than me, for some reason that
surprises me and makes me just a little bit sad but there we go That list of
people who are younger than me You know every year it just gets longer and
longer doesn't it, doesn't matter how old you are Yeah I agree Okay question two
I was at the Brisbane Lions and the Melbourne Dees game on Friday night What
happened at the Gabba that delayed the game by almost 40 minutes? Were all the
stadium toilets clogged or did the power go out? I reckon it'd have to be a
blackout It was There we were 12 minutes to go, the Lions were playing
beautifully and all of a sudden you heard a bang and bloomp and no stadium
lights We all kind of looked around and I thought well this is going to take a
while isn't it so the players stood out there for a while and then they went in
and came back out and the Lions I think thought yeah we've got this one and the
Dees thought ah I don't think you do and they got very close Well lucky for you
it wasn't the toilet, that's all I'll say Yes that's exactly right, I'd much
prefer the power than the lose, that's for sure Alright here we go, Justine's
doing very well, Justine from the Sunshine Coast delaying the inevitable which
is school lunches Question three, which colour light combination is the most
important for plant growth? Is it A, red and green or blue and red? Red and
green or blue and red? Oh this is a tough one isn't it? Yes, I'm just thinking
Talk me through it, what are you thinking? Well I'm just thinking, I'm thinking
of photosynthesis and then the colours of the plants but I'm not really sure I'm
going to go maybe it's the one that doesn't include green Okay blue and red?
Yeah that one Well done you Woohoo That's absolutely correct, you're blue and
red because the tips of growing plant shoots contain a pigment that's sensitive
to light, especially blue light so you put that on and they go thanks for that
There we go, well that was a very logical line of thinking you've got there Well
done Thank you Alright, question four, Justine from the Sunshine Coast
absolutely slamming through the quiz tonight Day on the Green was hosted at
Sirame Wines over the weekend and the headliner was Sir Rod Stewart But which
American female singer joined him? Was it Cyndi Lauper or Madonna? Who else was
on the bill? Cyndi Lauper or Madonna? Well I have to go with Cyndi Lauper
because I feel that Madonna wouldn't be a co-headliner Yeah I can't imagine that
either Well done Yeah I don't think I see your true colours shine Whereas
Cyndi's just adorable Yeah Madonna's gone a bit, and don't get me wrong I love
Madonna's music like great but she's just gone a bit weird She's different She
is, she is, but you know good on her, living her best life That's right, well
she's happy That's it, worth a squillion dollars Oh goodness, I remember
watching her doco years ago and before music docos were really a thing Good on
her for that too, but they would show us all this backstage stuff with the
dances and the music and does Sean love me, does he not, all that kind of thing
The one thing we were never allowed to watch was her business meetings and she
would shut the camera crew out and I thought that's interesting That's where we
get to see the real girl isn't it? That's where we get to see the savvy Oh, the
fly on the wall That's it, the savvy business woman Yeah but she didn't want you
to see that No way Indeed but she definitely was a savvy business Alright
Justine one more question and that fridge magnet is yours, here we go Okay
Brisbane Broncos defeated the Dolphins 18-12 last week, fins up people Who are
they up against this week? This one I know Go on then It's the Tigers It is the
Tigers, woohoo! Everyone's a winner baby, that's it Are you a Broncos fan? I'm
going to the game so I know who's, yeah So you know who's playing now That
wasn't really an answer to the question are you a fan? You're going to the game,
are you being dragged? I am a fan, yeah I'm going to the game That's how I know,
I was just looking at the tickets and I thought oh this one I've got it alright
Yes, you're up to see the West Tigers play the Broncos and you also will have a
fridge magnet that you can give a little pat on the way out the door Awesome
Thank you so much for playing tonight Justine and good luck with those lunches
What are you going to, what's going to be on the Sangers? Well one's Promite and
the other one's peanut butter Oh okay, are the kids allowed to have peanut
butter again? Or is it a teenager? Well it's sort of a teenager Okay I was
thinking well in primary school they're like no peanuts No that's the Promite-er
Indeed Brilliant Justine, hang on the line and we will make sure you get that
fridge magnet Lovely Justine on the Sunshine Coast, tonight's quiz winner coming
up We will have our Outback Local and this week you're heading way into
Queensland's deep southwest to Thargominda, just over 1000 kilometres west of
Brisbane You're going to meet a guy who's using the rugged Outback landscape to
train for one of the most gruelling physical challenges on the planet For that,
100% pure love on ABC Radio, Brisbane and Queensland I want your love, I want it
tonight I'm taking your heart, so don't you fight I'll be your answer, I'll be
your wish I'll be your fantasy, your favourite dish From the back to the middle
and around again I'm gonna be there till the end, 100% pure love From the back
to the middle and around again I'm gonna be there till the end, 100% pure love
You saw a brand new high, thought that you could fly Did I hear you cry, or did
you like the ride? You call my name again, no it's not a sin I'll show you how
to win, and where I've been I want your love, I want it tonight I'm taking your
heart, so don't you fight I'll be your answer, I'll be your wish I'll be your
fantasy, your favourite dish From the back to the middle and around again I'm
gonna be there till the end, 100% pure love From the back to the middle and
around again I'm gonna be there till the end, 100% pure love You'll never have
to run away Your heart's having a friend to play You'll never go out on your own
In the everJE, you'll find your home From the back to the middle and around
again I'm gonna be there till the end Back to the middle and around again I
wanna be there till the end From the back to the middle and around again I'm
gonna be there till the end 100% your love From the back to the middle and
around again I'm gonna be there till the end 100% your love Oh oh oh oh oh oh oh
oh oh oh oh oh oh oh oh That's such a cool version, years and years, a 100% pure
love Got to talk to our Outback Local in just a second. But look, this is the
last week of the two hour evening show with me from next week because daylight
saving ends down south. You get three hours. Now for some of you that will be
excellent news, for others of you that will be disappointing and I understand
that nobody is universally loved. But to celebrate, because I love being with
you longer, we're going to have an early Songs You Love and Dedications tonight.
So if there is a song you would love to hear, give the lovely Michaela a call
right now, 1300222 612 and we will try to put it on the crystal set before we
hit nine o'clock tonight. 1300222 612. You don't normally get Songs You Love and
Dedications on a Monday night. It will be returning from next week, weekly, or
daily, sorry. So give us a call now and tell us your favourite song and we will
try to put it on the air for you. 1300222 612. You're with Kelly Higgins Divine
on ABC Radio Brisbane and Queensland. And just a heads up, if you're in the
Brookfield area, Rural Fire Service crews will be working alongside a number of
agencies with doing a hazard reduction burn tomorrow near the Gold Creek Dam
area in Brookfield. So there'll be a bit of smoke around, it may affect nearby
suburbs as well over coming days. So keep that in mind. It's time once again to
meet an outback local and tonight's guest calls the town of Thargaminda home.
It's just over 1000 kilometres west of Brisbane and the third town in the world
after London and Paris to have hydroelectricity. Meet Jeff Pike, he's an
adventurer who's currently preparing to tackle one of the most punishing tricks
on earth in support of a very important cause. Jeff, hello. Good day, Michaela,
how you going? Oh good, Michaela's actually my producer. I'm Kelly, that's okay.
Honestly, I should be lucky to be Michaela. She's a sweetheart. That's okay, not
a problem. Now how's life going in Thargaminda? Actually it's cooled down a
little bit out here so we're pretty happy. Ah, so it's been stinking? It has
been quite warm. Yeah, so what was it, 38 today. It's a lot cooler than what it
was. Oh really? How hot's it been? Yeah, oh well over 40, yeah, for quite a
while. Oh that's ugly. Yeah, so that's pretty normal, but not so normal this
late in the season. It was quite a reasonable summer really. A few years ago we
had 40 days over 40 in a row. That's right. That knocks the wind out of you. I
can't even imagine. Surely there's a point at which you think, no, I can't do
this any longer. Yup, exactly right. Like about day five? Yeah, not even. It's
like, what am I doing? The people out here that keep you here, you have a lot
more good times than you do bad times. Yeah. It's just a great little community.
I'm just going to have to get you to talk up a little bit. You're just a little
bit soft and I can't pump your volume any more at this end so I'm sorry. There
we go. That's okay. Gotcha, gotcha. Now you're heading off soon. What are you
preparing for? Yeah, so we're only about three weeks away to go up to Kokoda and
do the Kokoda track. Ten days leading up to Anzac Day. So yeah, it's going to be
pretty full on. It will. What inspired this? Okay, so it goes back to my 20 year
school reunion where a few guys did Kokoda and I told them I'm definitely going
to do it. And so I put my money where my mouth is. But at the same time, since
then, my boys have gone to that school and they're in high school. And I ran it
past one of my mates, we should do it. And it's just sort of come together over
the last 12 months. And in the last probably five months, it started to get a
bit serious. And definitely over the last two to three, it's really, yeah, we've
got things cranking along pretty well now. Yeah, so I'm feeling quite fit.
Probably the best shape I've been in for years. That's good. But it's been
awesome. It has been really good. But we let a few cobwebs out over the weekend.
We had a 40th birthday party here and that's why my voice isn't that good. Turns
out when you're about two o'clock in the morning, you think you can sing a lot
better than anybody else. So yeah, we had a good weekend. What were you cranking
out? Oh, pretty well. What's your go-to song? What's your go-to song? Well, I
reckon it was a bit of Credence at that time. Nice, nice. Yeah, that's a good
choice. I thought you might say Chisel, you know, a little bit of... I think
there was a bit of Col Chisel in there. I bet there was. And then a little bit
of Luke Combs as well. Oh, nice. Good mix, yeah. So when we have that sort of
thing happening in Thargo, the whole town sort of comes together. So it's quite
a nice thing. You obviously love it in Thargo, Minda. How long have you been
there? I've been here 25 years. Yeah. What took you there? Yeah, I moved here to
do a plumbing apprenticeship. So I wasn't from far away, but I was from a little
town called Yulo. It's about 130 km east of here. And when I came here, one of
my mates was a mechanic on the Shire. And I moved here and I slept on a couch in
a caravan for my first few months. And then slowly worked my way up because you
don't get very well paid as an apprentice. I think when I did my first two
hours, I was already behind. I couldn't afford smogo. I couldn't afford lunch.
And then you just dig in. You've just got to do it. And then a few years later
on, I finished my trade and I went into business here as a molly trade company.
And that's sort of the line of work that we've been doing ever since. It's been
quite good. We've got crews up at West of Alice Springs. We did some work down
near Condobolin in New South Wales. We've got a crew up in the Wandoan at the
moment. And then we do work throughout the desert, which is where that's our
backyard. So we try to service the outback with all sorts of trade services.
That's our business. Well done. And I've got Pike joining me tonight on ABC
Radio Brisbane in Queensland. He's your outback local from Thargaminda. And
we're talking about his upcoming trek on the track, the Kokoda track. So how can
you train for Kokoda? I wouldn't imagine, and I could be totally wrong, of
course, because I've never been out to Thargaminda, that Thargaminda and Kokoda,
the area around the Kokoda track, are very different in terms of landscape.
Yeah. So when I started, I was out of shape. And so I needed to strengthen
myself. And so I started just riding. And then I just rode everywhere just with
the pack on. So I'd have like four kilos in the pack and I built that up to like
eight kilos. And then I'd do like 10 kilometres in the morning with the bike.
And then I made this setup where I've got a tyre with a rope behind it. So I'd
ride out 5k's, hook this tyre up around me, and then I'd still have my back on.
And I just built that up to about two and a half kilometres, just towing that
tyre along the road. Yep. Across this Gibbid Hill, actually. And then get back
on the push bike. And that was my warm down back into town. And so I slowly
built that up, built my strength up. And today, so this is quite a bit later
now, today I did a 5k hike with 17 kilos on my back. And then I came back to the
gym and I did 30 stories. So there's a step machine there, 16 steps to a story.
So I did about 500 steps. And then I did a small weight session and went up to
work and started work at 6.30. And then this afternoon, my little girl and I
went back to the gym and I did 50 stories and two sets of… I've got a cramp just
listening to you, Jeff. Oh my gosh. I'll tell you what, my legs are still sore
from all the dancing we did on Saturday night. Dancing isn't part of the
routine, obviously. That's it, there were muscles that you hadn't been using
that you used when you were dancing. Well, that's right. The other weekend I was
in Toowoomba because my boys are boarding school and I didn't think that they
were doing enough. So they had an AEIOU fundraiser and it was a cross-country
fundraiser. So anyway, I didn't think the boys are doing enough. So you know
what, I'm going to run this because I used to run a fair bit when I was their
age. I haven't run for that long anyway. So I'm going to run this and not only
am I going to run this, I'm going to make you guys work. I didn't realise what I
was getting myself into. So I got off about the first 500 metres and my young
fella, he's the oldest one, he just runs past me like a gazelle. How you going,
Dad? See you, Dad. I'm like, good on you, mate. And then in my mind I'm like, as
long as I just beat one of them I'll be fine. Did you beat one of them, Jeff
Pike? Well, technically yes, but what happened was I... I need you to just come
closer to the phone again, Jeff. Sorry, I'm losing you again. Sorry. Yeah,
technically I did because I just ran in front of this young fella and I kept...
Every time I went over the hill I just stopped to a walk and caught my breath.
But by the time I think he got to the top of the hill, I just put on the run
again, making him think I just didn't stop running. Anyway, so I'd run around
and he didn't pass me. And then I came past the finish line, so it did a loop
back on itself. And as I'm running past the finish line, he's laughing at me. He
goes, hey, Dad, catch me. And so he's bloody cheated along the track somewhere.
And so he's ended up in front of me. I couldn't believe it. Nice. There you go.
Good on you. Boy's got Dad's sense of humour, Jeff. Every character's, it's
really good. So when I've been down there at Toowoomba, we've walked tabletop a
few times. The other morning before school, I got them up at half past four and
we went and did the tabletop with a full set of packs. And no one else was on
tabletop at that moment. So we were sitting up at the top of tabletop watching
the sunrise, which is pretty special. Magic. You know, in the lead up to what
we're doing. Yeah, it was good. Now tell us who you're raising money for on this
Kokoda trip. Yeah, so we're raising money for Soldier On. So Soldier On's a
very, very good cause. It's a great charity that focuses on returning service
people to the community and not just returning them to the community. It's about
making sure that they contribute to the community and give everything that they
can and make them feel welcome and not make them feel obscure or left out or any
of that. They really work with the return service people and make them part of
the community. Because when you're in the heat of battle and you've been in that
situation for so long, it's really hard to go to the community because that's
not what you're used to. And that's why they really do put their heart and soul
into what they say they do. And so every dollar counts. Every dollar that we
raise is a dollar they didn't have before. And we know a fair few service people
out this way. And I live across the road from the memorial park here in
Tharkaminda. And on it is the Centertaph, it's the names of all the people that
gave their lives in World War I and II. But since then, there's been a lot of
contributors to the Australian forces from out this way. And if we can just give
that little bit back and that's what it's all for. It'll just be a very, very
special and surreal trip. I can't wait to get to, sorry? No, no, I was just
going to tell everyone who might have just joined us, I'm talking with Jeff Pike
tonight. Jeff is an adventurer, he's an outback local, he's from Tharkaminda and
he's currently preparing to tackle Kokoda and the track. How long have you got
to go? I've got about three weeks, so we fly out on the 15th. We do a debrief
and then we fly up to Popondetta and then we start heading down to Kokoda. And
we start in Kokoda the first night, which is pretty awesome because we get to,
the people that are leading this trip are ex-military. And they're basically
historians. And so some of these guys have done it 39 times, others have done it
11 times. They just love it and they're taking us on a journey with them in
their company. So it's a brilliant thing. Now another one of your passions is
hunting for explorer Ludwig Leichhardt and what happened to him, he and his team
disappeared in 1848 when they headed west. How did this particular interest come
about? Oh, it goes back, it goes back in the late 2000s. What happened was I
wanted to run a flying doctor fundraiser and so I called it the Outback
Explorers Trek. And I got talking to one of my brother-in-law here, Dogger Dare,
and then I got talking to Bomber Johnson who was out at Napmer at the time.
Between Dogger and Bomber they helped me piece together and all the different
explorers that came out through our area. Because I didn't want to just take
these people out on a journey and not know anything about it. So what I pieced
together was at the different places along the trek I could stand up on top of
the edge of a range and say, you know, Burke and Will stood here and they looked
over through the valley. And that's where, if you look over through the valley
you can see the green, that's Coopers Creek. And I wanted to take them on a
journey that they can feel it, they can breathe it, they're in the place where
it actually happened, you know, ground zero and it's something special. So what
ended up happening out of that is we stumbled on the story about Adolf Klassen
who was Ludwig Leichhardt's brother-in-law. And anyway, Klassen was a shipwright
and we found some remarkable things. And one of them is this stone arrow and
it's a broad arrow marked out on the ground with rocks and it faces 270 degrees
west, which is strange. But if you measure the distance between the lead rock
and the side rocks, it's six foot, six foot of fathom and the shipwright, that's
a measurement of depth in water. These little things like that that we've found
along the way, but what we've stumbled on in the last couple of years has just
really opened up the story even more. And we've been lucky enough that we've had
some volunteers come along on the journey. One of those volunteers, Rod
Crossley, he's got a ground penetrating radar and their service company just
deals with finding underground services. That's all they do. Well, a few years
ago, we actually found a few graves with the ground penetrating radar. Oh,
goodness. And then so from those graves, we can't excavate them, of course. But
we approached a number of different archaeological groups. And then we found
surveyors that have come along on the journey with us as well. And they're all
extraordinary people. Some of these people are tied up with the Geographical
Society, the Historical Society of Queensland, and it's we've got this group of
people. And we believe we found Andrew Hume, who actually passed away south in
the Kundra looking for Lycate and he actually got released from the jail down
near Sydney by I believe it was De Four, was the priest down there at the time.
He released him out to look for Lycate and Ohay was with him. Ohay was a VC and
he and Ohay passed away about 70 kilometres south of the Kundra. And Thompson,
the local ringer, he survived and came back and raised the alarm. And then with
that, we've also uncovered this other story on the periphery of our searches,
which is just remarkable. But it's another story about the logging of sandalwood
out here before Burke and Wills came through. It's just, it's really hard to
believe. It's just hard to believe. But if you go through the diaries of Burke
and Wills and Wills in particular, you'll see that they came across wagon tracks
down near Piccadilly Wardhole on the blue. So somebody was already out there.
Yeah, and we've actually we've found some blaze trees. And then last year with
this ground penetrating radar underneath the surface, we found a brush yard
that's basically brush yards made of cut down timber trees stacked on the side.
And this brush yard is about 600 mil below the surface. You can't even tell that
there's a yard there. Unless if you run a ground penetrating radar over the
ground. And then we join the dots with that. And then we found these wagon
tracks where they go between Piccadilly Wardhole over to Winaar, which is quite
remarkable. Yeah, Jeff, this is fascinating. I can tell you're passionate for
it. I can hear it. This is it. Look, congratulations on this Kokoda track that
you're going to be doing. You're taking your two boys with you? Yeah, yeah,
absolutely. Yeah, so one's in year 11, one's in year 9. And then my mate, his
daughter is in year 11 as well. And then I've got another mate at Thargo. He was
my nephew actually, but he was also my first electrician to work for me. He's
been out at Lake Gardiner for the land speed records just north of Port Augusta.
So you might see some photos of him if the story goes that way. Yeah, so he's
been hiking across the salt flats. It's quite remarkable actually that they had
about 800 people down there. So Dog and my brother-in-law, he's been down there
catering for that. So it's a real family affair. It is. I think you'll all be
exhausted, but it'll be just a lifetime memory, isn't it? It's just something
you'd never forget doing. So best of luck, Jeff, we'll catch up with you when
it's done and see how it went. And now if people want to jump on the Soldier on
website to donate to help you out with this fantastic cause, how do they do
that? Yeah, so probably the easiest way is through the, there's a link on my
Facebook. I don't know if that's something that you can do. Yeah, sure. Okay, so
if you look at my Facebook, it's just Jeff Pike. You'll see a person standing
there with his lovely wife and holding a pineapple. Yep. Yep. I'm pretty well
done at it because that was my 40th. It wasn't where I thought that sentence
might go, but there you are. I don't know if you ever heard the story about
copying a pineapple, but I've copied a few pineapples over my days and that's
why the pineapple is there. So anyway, but you'll see a Jeff Pike. Have a look
and you'll see me standing with my wife and I'm holding a pineapple in my hand.
If you jump on there, you'll be able to see that there's a link. It's probably
my last post and it's the link to the Soldier on website. Cool. It's going to be
great. Every dollar counts. That's it. Jeff Pike, it's been fabulous having a
chat with you. Thank you and Chukas for the Kokoda track. Thank you so much,
Kelly. Thanks everyone for listening. That's Jeff Pike, our Outback Local. So
there we go. And also get on the Soldier on website, soldieron.org.au or as Jeff
said, J, sorry, it's not J, it's G-E-O-F-F-P-I-K-E. So Jeff Pike, find him on
Facebook, pineapple and you can find the link so you can donate if you would
like to there as well. You're on ABC Radio, Brisbane and Queensland. Kelly
Higgins-Divine and Missy Higgins, wish she was a relation, it's 4 to 9. Missy
Higgins and the fabulous Steea on ABC Radio, Brisbane and Queensland. Noel from
Moorooka didn't get to your song but I will play it on Songs You Love and
Dedications on Thursday night. Thank you.