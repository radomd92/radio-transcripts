TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-19-13H00M.MP3
--------------------------------------------------------------------------------
 You'll know, but no, it's absolutely right. We know when things have gone wrong
and you know what, I think what we need to do is start calling these people out.
If you're on social media and you see that comment, you go to the troll, back
off. I love it. Just stop. Stop saying it and blokes don't put up with it
because, you know, we need you in there. We need you fighting the fight to go,
no, not acceptable, back off. If you see something safe, something I think it's
a very, has a lot of merit, Kelly. Exactly. Exactly. Thank you so much. This has
been fun. It has, Joel. We've got a bit more serious than we normally do. Well,
I deleted my notes, so I had to sort of make a bit up there. I didn't save my
notes, Kelly. You did very well. Indeed. But I don't care. I don't like this
shit. Oh dear. Indeed. We will have lots more for you after the 9 o'clock news.
ABC News with Glen Lauder. A renewable energy researcher says the Federal
Government's new fuel efficiency standard will incentivise global car
manufacturers to sell more EVs in Australia. The guidelines are part of the
Government's first electric vehicle strategy, which is open for six weeks of
consultation. Deputy Director of the Monash Energy Institute, Dr Roger
Darkerville, says he hopes the standards will increase the number of options for
consumers. Because Australia doesn't have a fuel efficiency standard at the
moment, there's no incentives for the manufacturers to sell their low emission
vehicles here. They've got limited capacity to manufacture the vehicles, so they
will primarily sell them in the markets where they have to sell low emission
vehicles. The Independent Senator David Pocox urging the Government to
prioritise support for vulnerable Australians in next month's budget. A report
by the Economic Inclusion Advisory Committee recommends urgent and substantial
increases to JobSeeker, Youth Allowance and other welfare payments. Senator
Pocox told ABC's 7.30 program he's impressed by the committee's work. We now
have, for the first time, independent advice that the Government is
commissioning and is made public before a budget. And if they don't act on
expert advice, that is setting out what should be done, what the potential
consequences of not doing that are, people get to vote at elections. A new
report reveals the national electricity market would need to triple in size by
the end of the decade if Australia is to reach net zero by 2050. Mechanical
engineer and director at the Melbourne Energy Institute, Michael Breyer, helped
write the report. He says trillions of dollars will be needed to be spent. He's
also warning building the required number of wind and solar farms is only part
of the challenge. There's a lot of work we need to do on transmission, but not
just electricity transmission. We need new forms of energy transmission,
hydrogen transmission, maybe even carbon dioxide transmission. And all of that,
of course, is underpinned by very large investments. The Prime Minister is
urging Senator Lydia Thorpe to seek support for health issues after she was
filmed in an expletive-laden altercation with a group of men outside a Melbourne
strip club. In footage aired on 7, Senator Thorpe seen shouting expletives at
the men after leaving the club in the early hours of Sunday morning. In a
statement responding to the video, Senator Thorpe says that people are using
whatever they can to drag her down. But Anthony Albanese has told the Super
Radio Network the behaviour is clearly unacceptable. These are not the actions
of anyone who should be participating in society in a normal way, let alone a
senator. And Lydia needs to be very conscious of the way in which this behaviour
has been seen. Meanwhile, the Prime Minister says that the country has lost a
great Australian with the death of beloved Catholic priest Father Bob McGuire.
Father Bob's family has confirmed he died in hospital aged 88 after a lifetime
dedicated to the poor and marginalised. Tributes have been paid across the board
and the Prime Minister says that Father Bob was an irrepressibly cheerful
champion for all those battling disadvantage and one who dedicated his life to
brightening the lives of those most in need. The helicopter pilot who rescued a
group of Indonesian fishermen from an island 300 kilometres west of Broom has
described it as a textbook mission. The 11 fishermen were rescued on Monday
after being shipwrecked during Cyclone Ilsa and surviving six days on a tiny
island without food or water. Wayne Thompson says one of his crew was set down
to determine whether the helicopter could land safely on the island. A decision
was made to winch the survivors aboard instead. Mr Thompson says the main
concern was whether they had enough fuel to complete the trip. We thought we
were going to have maybe just over an hour of fuel on site and as it turned out
that was enough for us to arrive, assess the scene and winch all the survivors
back on board, get back to Broom safe and sound. And the presidents of Poland,
Germany and Israel are gathering in the Polish capital to commemorate the 80th
anniversary of the Warsaw ghetto uprising. Hailed as one of the greatest acts of
defiance during World War II. Here's the BBC's Adam Easton. The Warsaw ghetto
was razed to the ground by the Germans. But in 1939, the Polish capital was home
to the world's largest Jewish community outside New York. At its height, the
Nazis imprisoned almost half a million people behind the ghetto's brick walls.
Most were murdered in the Treblinka death camp. Many others died of disease and
starvation. Before the final deportations, hundreds of young Jews decided to
fight. The Germans expected a few days resistance, but the fighting went on for
three weeks. ABC News. You're with Kelly Higgins-Divine on ABC Radio Brisbane
and Queensland. You certainly are. Now, OK, I've got to give you a heads up.
Nationally, the ABC is having a bit of trouble with our phones tonight. But it
looks like we can still get your text messages. So for now, unless I get the
word that the situation's changed, songs you love and dedications is just going
to have to be text messaging for now, anyhow. So if you have a song you would
like to hear and I know that Cam wants to hear nuclear blues. I'll try and find
it, Cam. 0467 922 612. If you've never requested a song before, I would love to
hear from you tonight. I mean, I'd love to hear from you if you have as well.
But if you haven't, now's maybe a good night when, you know, I don't even have
to ring her and talk to her. I can just SMS it and it's all good. 0467 922 612
and songs you love and dedications coming up in about 20, 25 minutes time. And
we just play a song that you would love to hear, be it one you might have a
birthday coming up, an anniversary, something like that. It might be just a
really good friend that you miss and you want to give them a shout out. It could
be that easy or just a song you really love that you want to hear. 1300, well,
no, don't ring that. Ring 0467 922 612 and text your request to me tonight. That
would be excellent. On the way, first, I look, we're going to give the quiz a
miss tonight for obvious reasons. But we will be talking about empathy and
robots and why it is that robots will probably never be able to be really close
companions with human beings because they could probably never have empathy.
That on the way on ABC Radio Brisbane and Queensland. And the Rolling Stones,
they miss you. I'm in a holding house alone. I've been sleeping all alone. Don't
I miss you? I've been hanging on the phone. I've been sleeping all alone. I
won't miss you. I've been holding in my sleep. I've been starving in my dreams.
I've been waiting in the hall. I've been waiting on your call. Your phone rings.
It's just a friend of mine that's in. Hey, what's the matter, man? Bet you'll
come round the 12 with some Puerto Rican girls. It's just us. It's just me and
my friends and me too. It's a real case of why. For a ride you don't have to use
a tow. Everybody waits around. Everybody waits around. Won't you come on? Come
on! I'm walking Central Park singing after dog. People think I'm crazy. Stuffing
on my feet, shuffling to the street. Asking feet, tic, tic, tic. What's the
matter with you, boy? Sometimes I work safe to myself. Sometimes I sing. I guess
I lied to myself. I'm just doing what no one else knows. I won't kiss your
child. You just ripped right down my mind. You fooling on my pride. I'm just
doing what no one else knows. I won't kiss your baby. I'm just doing what no one
else knows. I won't kiss your child. The Rolling Stones. And miss you. ABC Radio
Brisbane and Queensland. You're with Kelly Higgins-Divine tonight. And it's 12
past 9. The other day you heard about empathy and how Botox and other treatments
could affect how we perceive other people's emotions and therefore perhaps be
less empathetic with them. Well, it got me to thinking about robots. Could they
ever become a true companion for humans? My next guest says it's not likely
because empathy is very difficult to manufacture. Professor Skye MacDonald is a
clinical neuropsychologist at the University of New South Wales. Professor, why
do you think empathy is probably beyond a machine? Yes, it's an interesting
issue because people, I mean, I think people are very enchanted with the notion
of robots as companions and certainly there have been movies about it and some
of the current robots that are out, like some of the Japanese robots, are very
endearing. But the notion of empathy is a bit more complicated than currently
being modeled in robots really. The robot, PEPA for example, is a Japanese robot
and it's very good at picking up if someone's happy or sad or basic emotions
like that and it can even respond appropriately. But whether or not that's
empathy is a bit of a different question. What is it about reading somebody's
mood that's different to empathy? Well, for example, if someone's tearful and
crying then clearly they're upset. But I guess empathy requires knowing a little
bit more about why they're upset and what might be an appropriate response if
they're upset. So singing a little song to a child who's born open to hurt
themselves might be appropriate. But if someone's mother's just died, that would
probably be inappropriate. So it's really about being able to put yourself in
the shoes of the other person. And I think that's the sort of thing that would
be quite hard to manufacture in a robot. You believe there are two types of
empathy. Can you describe those for us? Yes, so there's effective empathy which
means essentially feeling the emotions of someone else. So if you see someone
else is tearing up or very joyful then you have a contagion of that yourself.
You start to feel the same emotion. So that's sort of sharing their emotion. The
other kind is cognitive empathy which is where you can put yourself in their
shoes. So for example, if someone can't see something you can put yourself in
their shoes and try to describe what things might look like from their
perspective. Or you just might know how they might think about a certain thing
or feel about a certain thing at a more kind of cognitive level. It's going to
be quite complicated for a robot to master effective empathy, to feel a depth of
emotion when watching somebody else feeling that same emotion. Yes, that's
right. So it could be programmed to do that but it's not going to feel it. It
will only be responding to what it's been told to respond to. Yes, that's right.
So you program a program that's sort of like emotion but it's not that the robot
won't be feeling the emotion. They'll be simulating a response but it won't be
emotion. And that will be at a kind of like a programmatic level. But emotion in
humans is about your heart racing and your sweat glands working and your eyes
watering and your muscles activated to smile at someone else smiling. So there's
a whole lot of physical responses we have to other people's emotions which is
part and parcel of that feeling of effective empathy. Do you think it's
possible, Professor MacDonald, that if artificial intelligence could ever lead
to a robot that truly feels and therefore could have empathy? Yes, well it's
hard to know. There's research, there's people who think about these things all
the time and there's even the notion that the only way you could actually
develop a robot that could know about true empathy and experience it is to grow
a baby robot and have it grow up in a company of humans. Because a lot of what
we develop as empathy comes from being an infant. An infant will look at their
mum when they're in an ambiguous situation to see what her emotional expression
is to work out how to regulate their own emotion. So there's this notion that
you really would have to develop a robot. And we're getting to the point of
saying, well, we're replicating human beings with all the time-consuming nature
of that in order to develop a robot that actually has empathy. I think it's
possible, but... I wonder why we feel the need to make robots in our own image
though and to make them as human as possible. What do you think is going on
there? Why do we even feel it's necessary to do that? We have human beings, lots
of us. Why would we want to make fake ones? It makes, to me, it kind of makes no
sense. Well, I guess it's human endeavour. I think these sort of challenges are
just challenges and people are really intrigued by the possibility. And
certainly there is a lot of interest in care of robots. For example, there are
these little robots that are currently being manufactured for people with
dementia. They're robots, but actually they look like little seals. And when you
pat them, they purr. But they don't have all of the kind of complications of a
normal pet. They don't need to be fed and they don't need to be cleaned and so
on. I mean, that might form a very useful function for people who are with
dementia because they're less complicated than real pets. Why would we want
robots to be companions for people as opposed to other people? I guess there may
be some arguments for that. But there are all these complications of having a
robot that may not actually be able to be truly empathic in situations where
they need to be. That's Professor Skye MacDonald, a clinical neuropsychologist
at the University of New South Wales, giving us a bit of insight into why robots
will probably never be capable of empathy. Conversations. Spend an hour in the
life of someone else. Midcave. I don't have those blinding epiphanies where a
song falls from the sky. Someone who has seen and done remarkable things. Well,
this is going to be easy, this record. These are deceiving ideas. Bathwater of
the last album just sort of hanging around in the pipes. The dirty bathwater,
wow. Hear the latest conversations. Weekday mornings from 11 on ABC Radio. Or
any time on the ABC Listen app. You're listening to ABC Radio Brisbane and
Queensland. Could you imagine living in a tiny house? It's not for everyone and
definitely involves a bit of downsizing. But some Australians think it's the
only viable option amid our cost of living crisis rather than ending up
homeless. But how realistic are tiny homes when it comes to housing those in
need? The ABC's Louise Dixon caught up with a lady who's doing her very best to
accommodate as many people as she can in a tiny space. Well, I was homeless. I'd
been homeless for nearly, what, I think, two years. And so I decided to move out
of Newcastle. If it happens this time, I don't think I'll live through it
because I've had to live through it twice before. And I'm trying not to cry.
It's been ten years since Tabitha moved out of Newcastle to Sesnoch. She was
raising two kids while sleeping rough and had to move away from her two other
children who lived with ex-partners. She had to leave a lot behind, but she also
thought that was the last time she'd be on the street. But with her rent about
to go up, the walls are closing in. I only earn, say, between $1,200 and $1,700
a fortnight, and there's $1,000, $20 gone for rent. And that has to be paid
first. I'm wrecking my brain from the time I'm awake to the time I go to sleep
to see what I can come up with that's not going to cost me and it's not going to
cost somebody else. Tabitha's solution is to think inside the box. She's eyeing
up a two metre by six metre tiny home, which she can put in someone's backyard.
You know, this would probably cost me around $50,000 because it's on wheels and
I can move it wherever I want. You know, instead of paying $350 to $400, $500,
$600 rent. You know, if we can pay that, we can pay off a loan. Most people
would have a home over their heads if they'd be able to pay it off. Tabitha also
looks after a small group on Facebook for homeless people in the Hunter Valley.
Her dream is to one day own a big block of land with lots of tiny homes to help
those in need. Let us have a bit of crown land, put a whole heap of doughs on
them so people have got somewhere safe to sleep over night time. And, you know,
maybe a chaos to help them find a house and go and get it. It sounds like a no-
brainer. A local councillor in Lake Macquarie recently proposed housing the
needy in tiny homes. But there are a lot of grey areas, which usually come down
to local council by-laws. And the rules are different all across New South
Wales. We've got over 50,000 people on the housing waiting list. No available
rental properties in regional or cities across New South Wales. I see this as a
real alternative. From his tiny house up north in Tweedshire, Peter Wigley
represents New South Wales for the Australian Tiny House Association. He's a
passionate advocate for tiny houses, a passion that's not always shared by
others. A fair bit of that negativity comes from a position that tiny homes
aren't consistent with planning in the area. Peter says these plans are
outdated. We've actually had a big change over the last 30 years. We've had, you
know, a large intake of migration coming into our areas. We've had, you know,
COVID, people moving into our areas. So that's putting excessive pressure on
housing and the housing stock. So creating an opportunity, I think we should be
looking at getting our heads together and say, well, what can we do instead of,
well, we can't do this? You know, looking at intentional tiny home community, I
think we need to look at, you know, a pilot program where we do this. When it
comes to individual tiny homes, a lot of New South Wales councils will allow
them to be put on a block where there's a family connection. Typically the tiny
house needs to be mobile or able to be moved along if needed. So it's not
exactly a concrete solution under current regulations. In 2019, there was a
landmark case in the New South Wales Land and Environment Court where a council
not far out of Sydney sought to remove a movable dwelling which was located in a
residential setting. And that was actually determined in favour of the people in
the dwelling. So if we're trying to tackle housing, we don't need everybody
ending up in the Land and Environment Court, I think. Stakeholders need to sit
down with government and start to imagine how we can start to change this.
Because it is a solution, I think it's a really viable solution and one which is
affordable for not only essential workers but young people trying to make their
way in life. With her own future hanging in the balance, I asked Tabitha if she
had any advice for people going through a rough patch. Oh, I don't know. Just
hanging in there, you know, with your tricky brain trying to think a way of
getting yourself out of that situation. But it's hard, it's hard, because
sometimes I don't even know what to say to them. And that was Tabitha chatting
to the ABC's Larise Dixon about housing the homeless in tiny houses. Songs you
love and dedications tonight. The songs you love rolling in on the text line.
You can try tonight. We've already got quite a few, though. So 0467 922612 if
you want to try your luck. First song tonight comes from Peter on the Gold
Coast. He says, Kel, can you please play Stuck on You by Lionel Richie for my
beautiful wife Tanya? We've been together for 30 years after meeting and only
three weeks before moving in together. She is the love of my life and I love her
more now than I have ever loved her. She's the one thing that keeps me going
every day. That is beautiful, Pete. And here's your song. ABC Radio, Brisbane
and Queensland. Stuck on you, got this feeling down deep in my soul that I just
can't lose. Guess I'm on my way. Need any friend and the way I feel now I guess
I'll be with you till the end. Guess I'm on my way. Mighty glad you stayed.
Stuck on you, been a fool too long. I guess it's time for me to come on home.
Guess I'm on my way. So hard to see that a woman like you could wait around for
a man like me. Guess I'm on my way. Mighty glad you stayed. Oh, I'm leaving on
that midnight train tomorrow and I know just where I'm going. I packed up my
troubles and I've thrown them all away. Cause this time, little darling, I'm
coming home to stay. I'm stuck on you, got this feeling down deep in my soul
that I just can't lose. Guess I'm on my way. Need any friend and the way I feel
now I guess I'll be with you till the end. Guess I'm on my way. Mighty glad you
stayed. For Pete and Tanya on the Gold Coast tonight, songs you love and
dedications. You're with Kelly Higgins-Divine and a first-timer who's texted in
tonight, Pete, who's on the Darling Downs and he says, hey, would it be possible
to hear a bit of John Hyatt? I'm a first-timer texting in to get a song and that
song is Crossing Muddy Waters. Baby's gone and I don't know why She let out this
morning Like a rusty shot in a hollow sky She left me without warning Sooner
than the dogs could bark Faster than the sun rose Down to the banks on an old
mule cart She took a flat boat across the shallow Left me in my tears to drown
She left a baby daughter Now the river's white and deep and brown She's crossing
muddy waters Tobacco's standing in the fields Be a-riding come November And a
bitter heart will not reveal A spring that love remembers When that sweet brown
girl of mine Her hair black as a raven Whipped broke the bread and drank the
wine From a jug that she'd been saving Left me in my tears to drown She left a
baby daughter Now the river's white and deep and brown She's crossing muddy
waters Baby's crying and the daylight's gone That big old tree is groaning In a
rush of wind and a river's song I can hear my true love moaning Crying for her
baby child Oh, crying for her husband Crying for that river's wife To take her
from her loved ones Left me in my tears to drown She left a baby daughter Now
the river's white and deep and brown She's crossing muddy waters Now the river's
white and deep and brown She's crossing muddy waters John Hyatt, Crossing Muddy
Waters going out to Pete on the Downs. In a moment, a song to celebrate 43 years
of marriage. Streaming for free every Sunday morning only on ABC iview. Really?
You're listening to ABC Radio Brisbane and Queensland. Songs you love and
dedications tonight. Quite a few texts coming in because, well, the phones are
down across the ABC. Nationally, not just in Queensland. So we're hoping they'll
be back on before we hit 10 o'clock tonight. But you're texting in beautifully.
Thank you so much. 0467 922 612. We've got a good line up, but you never know
your luck in a big city or a small rural town, as the case may be. Di sent me a
message saying, Well, I would love to surprise my hubby of 43 years with a song
by the great Vince Gill. It's called Look At Us. 43 years. What do they say? Get
lost for murder. Congratulations. Look at us All these years together Look at us
After all that we've been through Look at us Still leaning on each other If you
want to see how true love should be Then just look at us Look at you Still
pretty as a picture Look at me Still crazy over you Look at us Still believing
in forever If you want to see how true love should be Then just look at us In a
hundred years from now I know without a doubt They'll all look back and wonder
how We made it all work out Chances are We'll go down in history When they want
to see how true love should be They'll just look at us Chances are We'll go down
in history When they want to see how true love should be They'll just look at us
When they want to see how true love should be They'll just look at us
Congratulations to Dianne and her hubby 43 years married. Songs you love and
dedications tonight and this request came in from Lauren. It's a song by Van
Morrison, the second most streamed song of his. Brown Eyed Girl is the first,
Moondance is the third. It is from his Moondance album from 1970. And a Rolling
Stone review of the album at the time said, This song is the heart of Moondance.
The music unfolds with a classic sense of timing. Guest house drums fading into
watery notes on a piano. The bass counting off the pace. And the lines of the
song and Morrison's delivery of them are gorgeous. I want to rock your gypsy
soul just like in the days of old. Significantly we will fold into the mystic.
ABC Radio, Brisbane and Queensland. We were born before the wind We were so
young girl than the sun The bonnie boat was one as we sail into the mystic I
hear the sailors cry Smell the sea and feel the sky Let your soul and spirit fly
into the mystic And when that fork horn blows I will be coming home And when the
fork horn blows I want to hear it I don't have the fear and I want to rock your
gypsy soul Just like way back in the days of old Significantly we will fold into
the mystic And when that fork horn blows You know I will be coming home And when
that fork horn blows I got to hear it I don't have the fear and I want to rock
your gypsy soul Just like way back in the days of old And together we will fold
into the mystic Come on girl We'll let you stop now That one going out to Lauren
Van Morrison's Into the Mystic. This is ABC Radio, Brisbane and Queensland. It
certainly is here with Kelly Higgins Divine. Songs you love and dedications
tonight. The phones are down across Australia. Well, the ABC phones are anyway.
So you're texting me your requests tonight. 0467 922 612. It's 16 to 10. And our
next song goes out to Cairns Tim. He says it's almost 10 years with my perfect
partner. And this was a song that we had an interesting connection to. It's
called Rude by Magic and I promise it's not rude. I know the song. It's a lot of
fun. And he says, Addie, I love you. Saturday morning jumped out of bed. Put on
my best suit. Got in my car and raced like a jet. All the way to you. Knocked on
your door with heart in my hand. To ask you a question. Cause I know that you're
an old fashioned man. Can I have your daughter for the rest of my life? Say yes,
say yes. Cause I need you to know. You say I'll never get your blessing till the
day I die. So I fall in love with my friend. But the answer is no, no. Why you
gotta be so rude? Don't you know I'm human too? Why you gotta be so rude? I'm
gonna marry you anyway. Marry that girl. Marry her anyway. Marry that girl.
Yeah, no matter what you say. Marry that girl. And we'll be a family. Why you
gotta be so rude? I hate to do this. You leave no choice. Can't live without
her. Love me or hate me, we will be boys. Standing at that altar. Or we will run
away. To another guy that said you know. You know she's in love with me. She
will go anywhere I go. Can I have your daughter for the rest of my life? Say
yes, say yes. Cause I need you to know. You say I'll never get your blessing
till the day I die. I don't love my friend cause the answer's still no. Why you
gotta be so rude? Don't you know I'm human too? Why you gotta be so rude? I'm
gonna marry you anyway. Marry that girl. Marry her anyway. Marry that girl. No
matter what you say. Marry that girl. And we'll be a family. Why you gotta be so
rude? Ooh. Ooh. Ooh. Can I have your daughter for the rest of my life? Say yes,
say yes. Cause I need you to know. You say I'll never get your blessing till the
day I die. I don't love my friend cause the answer's still no. Why you gotta be
so rude? Don't you know I'm human too? Why you gotta be so rude? I'm gonna marry
you anyway. Marry that girl. Marry her anyway. Marry that girl. No matter what
you say. Marry that girl. And we'll be a family. Why you gotta be so rude? Yeah.
Why you gotta be so rude? Ooh. Why you gotta be so rude? That's the band called
Magic and Rude on Songs You Love and Dedications tonight with Kelly Higgins
Divine. Tim from Cairns asked for that song almost 10 years with Addy, his
partner. And he said the song, we have an interesting connection to it. Given
it's about a daddy won't give his permission for his daughter to marry the bloke
singing. Does make me wonder what was going on 10 years ago. You're with Kelly
Higgins Divine on ABC Radio Brisbane and Queensland. You certainly are. And
look, the next song goes out to Monique. And Monique has texted in saying, Kell,
my big 80th birthday is on Saturday. Monique, congratulations. The big 8-0. Well
done, you. Now, unfortunately we didn't have the song you wanted to hear. And we
really couldn't find it anywhere because it's a little bit, just a little bit
difficult to find. Anyhow, it was a song called Floyline by Chris Howland. So
Monique, I'm really sorry we couldn't get that song. But I've tried to find a
song from around the same time that I thought you might enjoy. So here's a bit
of Johnny Mathis for you and 12th of Never. You ask how much I need you. Must I
explain? I need you, oh my darling. My roses need rain. You ask how long I'll
love you. I'll tell you true. Until the 12th of Never, I'll still be loving you.
Hold me close. Never let me go. Hold me close. Melt my heart like April snow.
I'll love you till the bluebells forget to bloom. I'll love you till the clover
has lost its perfume. I'll love you till the poets run out of rhyme. Until the
12th of Never, and that's a long, long time. Until the 12th of Never, and that's
a long, long time. Happy 80th birthday for Saturday, Monique. You're listening
to ABC Radio Brisbane and Queensland. Songs you love and dedications. One for
Gil tonight. He's having a bit of a down day. We all get those, don't we? He
lost his wife last year. He just wants to hear a bit of Ricky Van Shelton
tonight, just as I am. I was lost and aimlessly searching. Lord knows I was one
lonely man. Then you came along like that old gospel song. You took me just as I
am. Just as I was about to lose hope. Just as I'd come to the end of my road.
You turned me around with one touch of your hand. And your love saved me just as
I am. It's amazing you saw the light shining. Through the darkness that light
had let me in. I think heaven above, you took me just as I was. And you love me
just as I am. Just as I was about to lose hope. Just as I'd come to the end of
my road. You turned me around with one touch of your hand. And your love saved
me just as I am. You turned me around with one touch of your hand. And your love
saved me just as I am. There you go, Gil. Hope that makes you feel better
tonight. Ricky Van Shelton, Just As I Am. Okay, now Tim, who wanted the Rude
song, well not rude with lyrics, just about a dad saying, no, you're not going
to get good enough for my daughter. He says, just to add to the Rude song, I now
go fishing with her dad as often as possible. And yes, it was an interesting
start, but a fantasy ending. Queensland. Queensland. Queensland. Queensland.
It's still got the old and the new. ABC Radio Brisbane and Queensland. And thank
you to John at Newmarket, he says, Cal, I'm driving back from the coast,
fantastic music tonight. I'm glad you like it. Your choice is tonight. You've
done beautifully. He's listening on digital radio too, which is great. Last song
for Songs You Love and Dedications tonight. Jack asked for a bit of Tony
Charles. This is one of my favourites, Jack. I hope it's one of yours too. Stop
your fussing. ABC Radio Brisbane and Queensland. Why you look so sad When the
sky is perfect blue You've obtained everything You ever wanted to Let's not talk
about the bad times We've been through that before And it's not right Can you
make me love anymore So stop your fussing But please stop your fussing For your
time will come Yes, your time will come Can you leave me now And talk to me your
things The light starts to burn into the night The moons that turn with purple
light That's what I want Just stop your fussing But please stop your fussing For
your time will come Can you just stop your fussing But please stop your fussing
For your time will come Yes, your time will come Someone tell me why We could
spend so many lonely years There's a light at the end of life It's enough, it's
enough, it's enough It's enough, it's enough, it's enough It's enough, it's
enough, it's enough It's enough to try We can see it in our lifetime We can see
it in our lifetime We can see it all in our lifetime Why you look so shy When
the sky is perfect blue And you're getting everything You're a fool