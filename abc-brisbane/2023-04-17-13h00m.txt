TRANSCRIPTION: ABC-BRISBANE_AUDIO/2023-04-17-13H00M.MP3
--------------------------------------------------------------------------------
 Alright, woohoo! Yeah! That's the truth. Well done, a fridge magnet winging its
way to you in cans, Terry. Bloody hell, I've been waiting all these years to get
one and now I've got one. Now you've got one. That is so good. I demand you send
me a photo at some point. I want to see it. Okay, alright. Terry, thank you so
much. When I learn how to navigate this phone, I'll be able to do that, but
yeah, I'll find somebody that'll help me out. Ah, that's it. It's all good.
Terry, thank you so much. The lovely Terry in cans tonight, scoring a fridge
magnet. We'll be back tomorrow night to give away another one. After nine
o'clock, songs you love and dedications. Dear Radio. ABC News with Glen Lauder.
The Prime Minister's rejected claims that he was told about child sexual abuse
victims being returned to their abusers in Alice Springs. Opposition leader
Peter Dutton claims the Prime Minister was made aware of the issue last year.
But speaking to the ABC 730 program, Anthony Albanese says Mr Dutton hasn't
raised any individual cases or instances with him. He says he doesn't know the
basis for Mr Dutton's claims and is again urging him to report any allegations
of abuse to police. Peter Dutton makes all sorts of claims. And the tragedy here
is that serious issues, like the abuse of children, shouldn't be used as a
political issue. The Foreign Minister's used a National Press Club address to
reflect on the work she's done since becoming Australia's top diplomat a year
ago, outlining the government's priorities over the immediate future. Here's
Defence correspondent Andrew Green. Penny Wong talked about the extensive travel
she's done in the Pacific, where she says the previous government really
neglected the region. She also talked about some of the efforts that have been
made by the Albanese government to prepare the region for the AUKUS agreement.
On Taiwan, she said that any war on the Democratic island would be catastrophic
for the region and certainly an articulation of the foreign policy priorities of
her and the government and what perhaps we could expect in the months and years
ahead. An Australian man's died while walking the Kokoda track in Papua New
Guinea. Here's PNG correspondent Tim Swanson. A spokesperson for Port Moresby's
Pacific International Hospital says the 48-year-old Australian man was brought
in on Sunday night when he was pronounced dead. The spokesperson says the man
fainted on the track while he was walking and those with him attempted CPR. The
Kokoda track authority says it was called by trekking company Adventure Kokoda
about the death last night. The authority has asked local police to conduct an
investigation. It's a busy time of year on the track with many Australians
trekking it for Anzac Day. Tim Swanson, ABC News, Port Moresby. A court's heard
an Australian businessman accused of selling defence and security secrets to
foreign spies sourced material from publicly available documents. The AFP
alleges Anthony Sergo was approached by someone claiming to be from a think tank
who then arranged a meeting with two other people offering payment for
information about Australia's defence, economic and national security
arrangements. His lawyer says that ASIO has confirmed all material found on his
laptop was publicly available. Mr Sergo was arrested in Sydney last week after
returning from China following lockdowns. An urban planning expert says
Melbourne's in need of better housing and transport infrastructure after
overtaking Sydney as the country's biggest city. The Australian Bureau of
Statistics has updated the geographical boundaries of Melbourne to include the
town of Melton, pushing its population ahead of Sydney by 20,000 people.
Associate professor at the University of Melbourne, Crystal Legacy says the
city's public transport system needs to catch up. Now more than ever, we need to
see good quality planning. As Melbourne grows, and it has been, we've been
anticipating this now for some time. We need to ensure that our infrastructure,
both housing and transport, are being delivered in a strategic and thoughtful
way. Heading overseas now, the United Kingdom is condemning Russia for
sentencing an outspoken Kremlin critic to 25 years in prison. With the story,
here's Harry Sekulich. Vladimir Karamutsa has long voiced his opposition to
President Vladimir Putin and has twice survived poisonings he's blamed on
Russian agents. His 25-year sentence stems from a speech he made to the House of
Representatives in the US state of Arizona a year ago where he denounced
Russia's invasion of Ukraine. Authorities have accused him of treason, an
allegation he denies likening his plight to the show trials that saw opponents
of Soviet dictator Joseph Stalin locked up. The UK says Mr Karamutsa's charges
are politically motivated and has summoned the Russian ambassador to lodge a
protest over his conviction. And the most powerful rocket ever built is
scheduled to take off for its first test flight in the coming day. The SpaceX
rocket and spacecraft that's collectively known as Starship is designed to be
rapidly reusable, allowing multiple trips into orbit and back again all within a
day. It's hoped that Starship will eventually be able to carry humans to Mars.
That's the latest from ABC News. For more at any time you can head to the
website at abc.net.au forward slash news. This is ABC Radio Brisbane and
Queensland. It certainly is and you are with Kelly Higgins-Divine tonight. And
it's so good to have you with me because we have coming up a chat with Jessica
Watson. Remember when she sailed solo in Ella's Pink Lady and there was all that
discussion beforehand about whether or not she should go. We all chimed in
because well that's what we do, we all chime in. She went and I don't remember
how I felt about it. I imagined that I was one of those going, oh good Lord, no,
don't go out there by yourself. Probably more about my own fears of a great big
wide ocean and being alone rather than any real fear for her because I didn't
know her. I didn't know that she was perfectly capable of dealing with those
things, that she had hundreds of thousands of hours of sailing under water. I
was sailing under her belt already. But she'd gone out for that test sail and
scraped up against a shipping boat, like shipping container boat, ship. It's not
a boat, it's a ship. And I think that got a few people a bit nervous. Not her
though, she was fine and her family were fine. But Jessica Watson is the subject
of tonight's Australian story, which you can now watch on iview. And I've met
her once, she was at a function that I emceed. And it was in, I reckon it was
probably 18 months after she came back. And she was so impressive, just so
beautifully spoken. She had a presentation that she gave that morning about her
voyage, her journey. And she's just so lovely as well, but so little and so
young. And I did think to myself, how impressive are you that you spent that
time on that boat by yourself in some horrific weather that was turning her boat
upside down and slamming it against, I can't imagine anything more terrifying
than that, truly. That would be my idea of hell. Don't worry about flames, water
would be my idea of hell, big waves. So she has my undying admiration. Anybody
who goes out in the ocean like that has my undying admiration because it's quite
frightening. But we're going to hear an interview in a moment that Jess Watson
did with the lovely Sarah Howes on the Sunshine Coast about doing the program,
because it's not about that journey, it's about what happened when she came home
and some of the challenges that she's faced since. So we'll hear from Jess
Watson, plus Songs You Love and Dedications tonight, 1300 222 612. I would love
for you to give us a call and tell us what song you would like to hear tonight.
Now, the title gives it away, Songs You Love. So if you just love it, give us a
call, 1300 222 612. You might want to dedicate it to somebody, could be your
anniversary, could be a birthday, could be anything. Maybe you're celebrating
the day you got divorced, maybe you're both really happy about it and you have a
divorce song, I don't know. 1300 222 612, you can text me as well, 0467 922 612.
A little bit earlier we were talking about fears and phobias. Linda from Miller
Miller says, Kel, I'm not afraid of any of those things you mentioned like
spiders or heights. Things that go around like large clocks or windmills creep
me out, even things that move up and down by themselves. Okay, Linda. I wonder
where that comes from. And Wendy says, Senator Lydia Thorpe resigned from the
Greens earlier this year, now sits on the crossbench. Ah, Wendy, thank you. Yes.
So my apologies to whoever got offended because I said she was former Greens
Senator. There we go. Now independent. And Julie at Mount Garnet says, Kel, I
see you every day on my magnet. Thanks, Julie. By the way, keep texting in with
your fears and your phobias. I think it's good to share these things because you
can feel a bit on the outer about sometimes about the strange things that you
might have a phobia about. But you'd be surprised to find how many other people
might have that same phobia. So 0467 922 612. Text me, text me either about the
song you love for dedication or fears and phobias. Give us a call about the
songs you want to hear tonight. 1300 222 612. Bet you haven't heard this one in
a while. Some Skyhooks, some Shell and the Boys. All my friends are getting
married. They're all doing what they're told. When I looked into the crowd the
other night and I saw an old familiar face. He said, how are you doing Cheryl my
boy? He said, tell me are you playing the same old place? I asked him all about
himself and he said that he was married with a kid. He showed me a picture of
his wife and we talked about all those things we did. All my friends are getting
married. Yes they're all growing old. They're all staying home on the weekends.
They're all doing what they're told. Well sometimes I feel like I'm left behind.
And sometimes I feel like I've just left school. I wonder if I'll ever grow up.
Maybe I'm the only fool. All my friends are getting married. Yes they're all
growing old. They're all staying home on the weekends. They're all doing what
they're told. But I'm caught up with this magic. Yes I'm caught up with this
fun. I'm all caught up with this music. Maybe I'll never have some. Maybe I'll
never have some. All my friends are getting married. Yes they're all growing
old. They're all staying home on the weekends. They're all doing what they're
told. But I don't want to be pushed around. They're all doing what they're told.
Shirley Strong, Sky Hooks, all my friends are getting married. And Caroline sent
me a text saying she remembers the day she died. Yeah. Awful accident. Awful.
Can I just say, every now and again I just check the Energex site just to make
sure that everything's okay where you are. And tonight something that rare has
happened where there's one lone soul in the southeast corner who doesn't have
power. One poor possum in Zilmere. And unfortunately you can't get any detail on
who it is. But just having a look, emergency repairs are in progress. So I don't
know who that person in Zilmere is but I hope your power's on. Oh wait a minute.
We might be able to get a street. Oh. Zilmere Road. Alright. That's what we
know. So to that one person in Zilmere Road, emergency repairs are in progress
and you should have your power on very soon. Mornings with Rebecca Levingston.
Elvis walked up to me. Mary are you hungry? Why don't we walk across the street
and get something before we go on the show. I said okay. But while we were
talking there was a line of about 15 girls that had already walked up behind him
and I said Elvis I think you have enough company but I'll be here when you get
back. Rebecca Levingston. Essentially turned down dinner with Elvis Presley. I
know that's what everybody's doing. Weekday mornings from 8.30 on ABC Radio
Brisbane. 100, 222, 612 to request a song for songs you love and dedications.
It's coming up around about 10 minutes time. It's 16 past 9. You're with Kelly
Higgins Divine tonight. Do you remember the emotion when then teenager Jessica
Watson returned home from her attempted solo circman navigation of the globe?
She's had some massive ups and downs notably the death of her partner in 2021.
Jessica Watson's journey is the focus of this week's Australian story which
you'll be able to see on ABC Eyeview. Sarah Howes of ABC Radio Queensland on the
Sunshine Coast spoke with Jess about what it was like to work with the Oz Story
team. It's been quite an adventure actually. It's been so cool to take the crew
sailing and show them some of my life. That's been fun. It's been an odd
experience I think for friends being interviewed. Yeah, mixed feelings but a bit
of fun as well behind the scenes. Oh nice. Sailing for you, I guess sailing can
be quite a solo pursuit but it's also something that made you instantly famous.
How did you cope with all that attention at the time? Yeah, for years and years
now it's been something I've really done with people and friends. I think the
biggest thing is you say how did I cope? What was normal? For years before the
voyage I was so focused on this thing and it wasn't like I was stepping out of a
normal-ish school kind of life and then needing to step back into that anyway.
It was pretty wild though. There were years there where it was just the most
extraordinary experiences and I don't think I came down from the high of that
adrenaline for years and years. As mentioned in that clip, there's been a movie
released about that journey as well. How's that been? Yeah, pretty crazy. It's
extraordinary to see this other world in terms of movie making and getting to
see the process and be involved with that. And then, I don't know, it's sort of
like a take on my story but it's its own thing as well. I love that about it,
it's kind of its own thing as well. Yeah, fair enough. Inspired by a few little
changes put in there I think. Yeah, there's definitely some parts that are a
little exaggerated or obviously some characters merged and things like that but
at the same time there's parts that are really unnervingly real. So that's the
bits that kind of get me like, oh okay, yep, that's exactly what it was or how
it felt. Yeah, wow. Well, not to take you right back to your 16-year-old self
too much this morning Jess but here at ABC Radio Queensland we were following
your journey quite closely. The late great John Stokes spoke to your mum on the
day that you crossed the equator and he asked her how it felt to have her
daughter out on the ocean for a month at that point. It just gives me so much
confidence and I'm just so proud of her that she's got through that first month
and she's just enjoying herself so much. She said it's so great out here mum,
there's no rushing to get to work or school or getting all these things done. I
mean she's got a lot to do but it's not the same as our kind of precious.
Clearly a very proud mum there. What was it that you learnt about yourself
during that time? Oh, it's so much really and so much obviously in the
preparation beforehand because sort of mentally it felt like I'd almost done the
trip before I set off because you really have to get your head into the space
and have really thought it through. But there's so much that I've obviously
drawn on over the years and particularly recently in terms of what you learnt
from being at sea in terms of just how resilient I suppose humans are. It's
pretty extraordinary what you can adapt to. But I think it's funny listening to
mum talk about having fun out there because that was actually one of the most
important things I think I did learn. And perhaps one of the things I'm quite
proud of with the voyage is I did have fun. I think I'm a person who probably
gets a bit caught up or used to get a bit caught up in all the serious things
and focused on what could go wrong and it's important to actually, such a cliché
and not a great sailing analogy, to have all the roses, to really appreciate the
beauty of being on the ocean and to enjoy these adventures while we're on them
as well. Yeah, I think that's a good message for life as well. Look for the good
moments as well as the tough moments that naturally tend to come in our lives.
Your friend and manager says that you're pretty good at overcoming adversity.
This is Andrew Fraser. Jessica's managed to tap into an inner strength and I
think a lot of those mechanisms have come from her around the world voyage,
which taught her how to overcome adversity. She's always managed to find a way
to navigate through the difficult times in her life. Jess, you did mention
before that those lessons that you learnt out on the ocean have served you well
in other parts of your life. Can you talk us through a little bit of how that
strength that you had navigating the world solo, how you've drawn on that in
your life since? Yeah, absolutely. My long-term partner of 10 years, Cam, passed
away back in 2020 and obviously it's been an extraordinarily tough few years
since then, absolutely incomparable to anything else. It has absolutely forced
me to go back and draw on some of the lessons from everything from sailing
around the world. It's funny actually listening to Fraser, Andrew, say that
because I have such a vivid recollection of him talking to me in the days after
Cam had passed away and it was kind of his relentless optimism that was probably
one of the most important things that actually sort of set me on a somewhat
positive path after Cam's passing. So yeah, it's funny him saying that because
I'm going, gosh, well you were one of the people who really kind of I suppose
helped me keep my thinking in some of the right direction. Yeah, good to have
good mates that can help because those moments in grief are so incredibly hard.
What else got you through it at that time? Well, there's so many good mates and
I think of one particularly who got me out sailing very early on and that was
just the most important thing of all I think. Getting out on the water and doing
it with some of Cam's best mates and just feeling like he was so much part of
that and it felt like such a purposeful thing to be doing and still does and
there's just also just nothing better than being on the water, right? The beauty
of it and the kind of adrenaline and the wind in your face is just absolutely
the best possible thing. Good therapy. Gosh, yeah. What would you tell someone
who's in the midst of that grief right now? I don't know how much is helpful
right in the midst of it and the worst of it, but certainly, you know,
absolutely lean heavily on people around you. It's extraordinary the way that
them just sort of being there or being in your life, not even physically being
there can help shoulder some of that burden. But goodness knows, get out on the
water, that helps. And for me, I think it was just really being open and kind of
aggressively seeking everything that's good because it's there and you just got
to, I suppose, really, really tune into it. Yeah, good advice. Well, what's next
for you? What have you got planned next, Jess? Look, you know, I think the kind
of clarity of what's next has kind of been all kind of turned on its head for
me, you know, in recent times, but I know that I'm really loving my sort of my
day job, my career and obviously loving sailing more than ever. So throwing
myself into kind of things that feel meaningful and that's enough for now.
Fantastic. Well, it sounds like a pretty good plan. Hey, thanks for dropping by
for a chat. Thanks for having me. Jessica Watson speaking with the ABC's Sarah
Howes and the Australian Story segment on Jess is available to watch now on
iview. G'day, Craig and Loretta here. A big guest in the studio tomorrow
morning, US Ambassador Caroline Kennedy. Yes, the daughter of former US
President John F. Kennedy is here on her first official visit to Queensland
since taking up her diplomatic post. She's already been riding the waves with
Mick Fanning and she's taken in some rugby league at the Titans Broncos game on
the weekend. So what else is she doing while she's here? Find out tomorrow
morning when US Ambassador Caroline Kennedy joins us on Breakfast on ABC Radio
Brisbane. Absolutely mad, those two. You're with Kelly Higgins-Divine on ABC
Radio Brisbane in Queensland. Songs you love and dedications. Kicking it off a
little bit early tonight, why not? Because we've already got some brilliant
songs that you've asked for. 1300, 222, 612, if there's a song you would really
love to hear tonight. Or you can flick me a text, we'll do our best for you.
0467, 922, 612. Let's start off, shall we, in North Queensland. James, flicked
us a text asking to hear some Louis Armstrong and Mack the Knife. Dig, man,
there goes Mack the Knife. Oh, the shark has pretty teeth, dear. And he shows
them a pretty wife, just a jackknife has Mack heath, dear. And he keeps it
outside when the shark bites with his teeth, dear. Scarlet pillows start to
spray, fancy gloves, though, where's Mack heath, dear? So there's not a trace of
red on the sidewalk. Sunday morning, baby, lies a body oozing life. Someone's
sneaking around the common house. Is there someone, Mack the Knife, from a dog
boat by the river. Seamant bags drooping down, yes, the seamant's just for the
weight, dear. Bet you Mack heath's back in town. Look a year, Louis Miller
disappeared, dear, after drawing out his cash. And Mack heath spends like a
seller. Did our boy do something rash? Don Quijote, Jenny Diver, Lottie Lenya,
sweet Lucy Brown. Oh, the lion's arms on the right, dear. Now that Mack heath's
back in town. Take a snatch. Louis Armstrong, Mack the Knife, suggested tonight
by James, who's in North Queensland, wants to send some love to his family. I
hope you enjoyed that. You're listening to ABC Radio Brisbane and Queensland.
1300222612 to request a song tonight, which Henry has done. Not sure where
Henry's from, but it's from Queensland. It's beautiful. We know that. And this
is a beautiful song. Thank you for requesting it. The Righteous Brothers and
Unchained Melody. And time goes by so slowly. And time can do so much for you
still more. I need your love. God, speed your love to me. Lonely rivers flow to
the sea, to the sea. To the open arms of the sea. Lonely rivers sigh, wait for
me, wait for me. I'll be coming home, wait for me. Oh, my love, my darling. I'm
hungry, hungry for your touch. Oh, my, you're retiring. And time goes by so
slowly. And time can do so much for you still more. I need your love. God, speed
your love to me. The Righteous Brothers and Unchained Melody. More of your songs
that you love and your dedications in just a moment. Drive with Steve Austin. Do
you accept any responsibility for the cost blowouts or the time delays at all,
Barnaby Joyce? Well, no. The project was sort of being invented as it went along
with no known end point. How can you not? Because when I got back, the first
thing I did was say that there's no more reviews. Don't have more reviews. I
don't want any moving paper. I want you moving dirt. Steve Austin. Weekday
Afternoons from 3.30 on ABC Radio Brisbane. Queensland. Queensland. Queensland.
Queensland. Queensland. It's still got the old and the new. ABC Radio Brisbane
and Queensland. And you're with Kelly Higgins, Divine tonight. We've got a
request for some John Farnham on the way. First up though, Claire. Is it Gimpy?
Hi, Claire. Hi, Kelly. How are you doing tonight? Bad, thanks. Excellent. Have
you had a big day? Oh, yeah. I've been doing a bit of gardening and going
through paperwork and things like that. Paperwork? No. It's the worst, isn't it?
It wasn't fun. No. There's something about having to do administration,
especially for your own life, which is just annoying. Don't you reckon? It's all
right if you're getting paid for it, but when it's just stuff you have to do for
your own... I got a parking fine the other day. Well, more than a month ago now,
and I thought I was within the 28 days. Now I'm not. Now there's another 50
bucks on it. Oh, okay. Gosh. I need a wife. I need a wife, Claire. Yes. Yes,
somebody who can do all that for me. At the very least, a personal assistant.
Yeah, that'd be good, wouldn't it? It would. It would be excellent. Claire, what
song can I play for you tonight? Amanda by Don Williams. Oh, okay. This is a
great song. Why this one? I just like Don Williams. I've always liked his
singing. Yep, I think he is too, and this is beautiful. Thank you for calling in
and for requesting this great song. Thank you. Bye, Claire. Bye now. I've held
it all in word, Lord knows I've tried. It's an awful awakening in a country
boy's life. To look in the mirror in total surprise. At the hair on your
shoulders and age in your eyes. Amanda, light of my life. Fate should have made
you a gentleman's wife. Amanda, light of my life. Fate should have made you a
gentleman's wife. I'm a measure of people who don't understand. The pleasures of
life in a hillbilly band. I got my first guitar when I was 14. Now I'm crowned
30 and still wearing jeans. Amanda, light of my life. Fate should have made you
a gentleman's wife. Amanda, light of my life. Fate should have made you a
gentleman's wife. Amanda, light of my life. Fate should have made you a
gentleman's wife. Amanda, light of my life. Fate should have made you a
gentleman's wife. John Williams and Amanda getting the big thumbs up tonight
from Gillidimble and Carolyn at Nundah. Streaming now at abc.net.au slash
Brisbane. This is ABC Radio Brisbane and Queensland. I think you're all in a bit
of a reflective mood tonight because this next song is beautiful as well. One of
my favourite John Farnham songs, I must admit. The Beautiful Burn for You. And
this one was suggested by Tim at Newmarket. Thanks Tim. I got myself into some
trouble tonight. Guess I'm just feeling blue. It's been so long since I've seen
your face. This distance between me and you. That voice you showed me is not the
one that I know. I must be strong out on what I do. Don't hang up again. There's
nothing else I know how to do. But I burn for you. What am I gonna do? I burn
for you. I guess it feels like you're always alone. And I feel that way too.
It's so hard to explain to you. Please understand what I do. I burn for you.
What am I gonna do? I burn for you. I burn for you. Took my trouble to a bar
tonight. For another point of view. But there's nothing new. I'm missing you. I
burn for you. What am I gonna do? I burn for you. I burn for you. I burn for
you. What am I gonna do? I burn for you. I burn for you. What am I gonna do? I
burn for you. I burn for you. I burn for you. Thank you, Kelly. I'm having a
great night listening to these beautiful songs. How are you going? Good, thanks.
What did you get up to? Cabbage, cauliflower, broccoli, and carrots. I've got
everything going. Tell us about this song. I came across it on the computer.
It's about New Orleans. I've been to all those places. It's rock and roll at its
best. Let's get into it. Thank you. I'm having a great night listening to these
beautiful songs. I burn for you. I burn for you. I burn for you. I burn for you.
I burn for you. I burn for you. I burn for you. I'm over to the twist and shout.
Kind of two-step partner and occasion beat. When it lifts me up, I'm going to
find my feet. Out in the middle of a big dance floor, and I hear that fiddle,
want to bang for more. Want to dance to a band from Melusia tonight. Hey! They
got an alligator stew and a crawfish pie, a golf storm blowing into town
tonight. Living on the Delta's quiet show. They got hurricane parties every time
it blows. But here out north, it's a cold, cold rain, and there ain't no cure
for my blues today. Except when the paper says both delays. I'm coming into
town, baby, let's go down. It's Saturday night, and the moon is out. I want to
head on over to the twist and shout. Kind of two-step partner and occasion beat.
When it lifts me up, I'm going to find my feet. Out in the middle of a big dance
floor, and I hear that fiddle, want to bang for more. Want to dance to a band
from Melusia tonight. Woo! Bring your mama, bring your papa, bring your sister,
too. They got lots of music and lots of room. When they play you a Waltz from
the 1910s, we're going to feel a little bit young again. When you learn to dance
with your rock and roll, you learn to swim with the do-se-do. But you learn to
love at the be-do-do, when you hear a little show live long. Saturday night, and
the moon is out. I want to head on over to the twist and shout. Kind of two-step
partner and occasion beat. When it lifts me up, I'm going to find my feet. Out
in the middle of a big dance floor, and I hear that fiddle, want to bang for
more. Want to dance to a band from Melusia tonight. Competition. They're all
having a very good time, aren't they? Mary Chapin Carpenter, down at the twist
and shout for Bob at Burpingarry tonight. Friendly. It's a great place. Good
weather. You're listening to ABC Radio Brisbane and Queensland. Queensland! Woo!
And we've still got that laid-back atmosphere. We certainly do tonight for songs
you love and dedications. And this one coming in from Will, who says, in light
of a Jess Watson interview, looking at life a bit differently. And I love the
song Traveller by Chris Stapleton. Now, if you've never heard of Chris
Stapleton, you haven't listened to a lot of country music. He is absolutely huge
and has eight Grammys, 10 Academy of Country Music Awards, 14 CMA Awards,
Country Music Association Awards, was named the Academy of Country Music's
Artist Songwriter of the Decade. He's worked with a heap of big names, has
written or co-written over 170 songs, including six number one country songs.
This is a great little tune too. And as I said, it's called Traveller. I see the
sunrise creeping in. Everything changes like a desert wind. Here she comes and
then she's gone again. And I'm just a traveler on this earth. Shut my heart
behind the pocket of my shirt. I just keep rolling until I'm in there, because
I'm a traveler. Oh, I'm a traveler. I couldn't tell you, honey, I don't know
where I'm going. But I've got to go, because every time it feels like I'm alone,
and I'm a traveler. Oh, I'm a traveler. My heartbeat's a rhythm that's lonesome
sound, just like a rubber turning on the ground. Always lost and nowhere bound.
I'm just a traveler on this earth. Shut my heart behind the pocket of my shirt.
I just keep rolling until I'm in there, because I'm a traveler. Oh, I'm a
traveler. I couldn't tell you, honey, I don't know where I'm going. But I've got
to go, because every time it feels like I'm alone, and I'm a traveler. Oh, I'm a
traveler. When I'm gone, somebody else will have to feel this long. Somebody
else will have to sing this song. Somebody else will have to sing along. I'm
just a traveler on this earth. Shut my heart behind the pocket of my shirt. I
just keep rolling until I'm in there, because I'm a traveler. Oh, I'm a
traveler. I couldn't tell you, honey, I don't know where I'm going. But I've got
to go, because every time it feels like I'm alone, and I'm a traveler. Oh, I'm a
traveler. That's a very cool song, Chris Stapleton and Traveler. Nice pick,
Will. Queensland. Queensland. Queensland. It's just an exciting place to live.
Fantastic. ABC Radio Brisbane and Queensland. Songs you love and dedications.
Let's head to Cairns. Hi, John. Well, good morning, Kelly. How are you? Good.
Oh, look, it's close enough. I get up a long time. That's it. How are you doing
tonight? Pretty good. We didn't switch the air on today. Oh, that's good for
Cairns. The thunder opened up last night and even Council. You there? Yes, even
Council got a soak. Oh, nice. So did it clear up some of the humidity? Did it
or? Yeah, well, there's a breeze in your face as you face them as you're wet.
Nice. It's just there, but that's it. Yep. I remember what it was like when you
just had that change in weather. It was almost overnight when the humidity would
leave and you'd get up the next morning and go, oh, this is great for six months
and then it was back again. The worst is at night. It feels like 35. Yeah, it's
not good. But you've had some great weather, which is always nice because it can
be unrelenting. What? John with his throat. Yeah. Anyway, yeah. Okay. I'd like
to hear some of it tonight. John, what, Slim Whitman, which one from Slim?
Rosemary, I love you. Let's do it. Thanks, John. ABC radio, Brisbane,
Queensland. Oh, ah, oh, oh, my rosemary. Oh, rosemary, I love you. I'm always
dreaming of you. I'm always dreaming of you No matter what I do, I can't forget
you Sometimes I wish that I never met you And yet if I should lose you It would
mean my very life to me Of all the queens that ever lived I'd choose you To rule
me my Rosemary Of all the queens that ever lived I'd choose you To rule me my
Rosemary Of all the queens that ever lived I'd choose you Yes, I'd choose you To
rule me my Rosemary Tonight, some credence. Coming up to ten o'clock. I see the
bad moon rising I see trouble on the way I see bearer of wakes and lightning I
see bad times of day Don't go around tonight But if you finally take your last
There's the bad moon on the rise I hear hurricanes are blowing I know the end is
coming soon I hear rivers overflowing I hear the voice of rage and ruin Don't go
around tonight But if you finally take your last There's the bad moon on the
rise There's the bad moon on the rise