TRANSCRIPTION: RADIO-FORMULA-91.3_AUDIO/2023-03-21-15H00M.AAC
--------------------------------------------------------------------------------
 La prolactina es una hormona que estimula la producción de leche en las
glándulas mamarias. Después de amamantar, si estás buscando un segundo embarazo,
es necesario que confirmes que los niveles de esta hormona sean los adecuados
para lograrlo. Soy Gina Ibarra. Sigan en Twitter en Arroba de mamá y en Bajo a
mamá. Y seamos amigos en el Facebook en De mamá a mamá. 8 de la mañana de
Yanira. Buenos días. Buenos días, Ciro. Vamos con la información. El nivel de
confianza de la Suprema Corte de Justicia es de 61%. De acuerdo con una encuesta
publicada en el periódico El Financiero. Según esta información, con la llegada
de la ministra presidenta Norma Piña, la opinión positiva de la Corte subió 9
puntos a la Corte de Justicia. La Corte de Justicia ha sido la primera de los
dos. La Corte de Justicia ha sido la primera de los dos. Con la llegada de la
ministra presidenta Norma Piña, la opinión positiva de la Corte subió 9 puntos
en comparación con febrero, cuando el nivel de confianza se ubicaba en 52%.
Estaré de lleno, al 100% apoyando la campaña de Alejandra del Moral. Aseguró por
la mañana Enrique Vargas, coordinador de los Diputados del PAN en el Estado de
México. Dijo que cumplirá su palabra y a partir del 3 de abril, saldrá con todas
sus redes 70 mil personas para estar de lleno en el proceso electoral del Estado
de México. Enrique Vargas va a estar al 100% en la campaña y todo su equipo y
acción nacional vamos a estar a fondo con grandes personalidades como Vicente
Foxx, el expresidente, como Santiago Cri, como todas las senadoras, todos vamos
a estar metidos de lleno. Enrique Vargas es una persona institucional y yo
cuando doy mi palabra la cumplo. De apoyar a Alejandra del Moral y así va a ser.
Toda la red Vargas-Iro que han creído en mi trabajo ya están listas para salir
el 3 de abril con todo porque se está ocultando el futuro del Estado de México,
el Estado más importante del país. Aunque por el Banco del Bienestar no llega ni
el 5% de las remesas, la decisión de Wells Fargo indica la falta de controles
antilavado de dinero del Banco del Bienestar. Aseguró por la mañana Mario de
Costanzo, ex presidente de la Conducef. Dijo que esto llama mucho la atención y
es un golpe muy fuerte porque la misma Profeco calificó a Wells Fargo como una
de las 20 mejores remesadoras en el mercado. El equipo de Claudia Sheinbaum es
quien dice que ella es la favorita del presidente López Obrador para que sea la
candidata de Morena a la presidencia. Yo no lo veo así, aseguró el canciller
Marcelo Ebrard en entrevista a Manuel Ferregrino después de que presentó su
libro El Camino de México en el Palacio de Minería. Le presentaremos la
entrevista completa. Y en estos momentos el dólar interbancario se cotiza en 18
pesos con 70 centavos. ¿Qué más van? Bueno, pues a pesar de haber sido un fin de
semana largo, 60 personas que están buscando formar parte de las quintetas para
renovar a los cuatro consejeros electorales que van a dejar a principios de
abrir su cargo. 60 de 92 ya comparecieron, ya pasaron a sus entrevistas con el
comité técnico. Hoy lo harán pues otro tanto y mañana termina, mañana termina.
Así que mañana 22 terminará esta etapa de acuerdo con el calendario de
entrevista de los aspirantes. Luego viene para el día 24 la fecha máxima en la
que el comité técnico envía a la Junta de Coordinación Política un documento con
las personas aspirantes mejor evaluadas. Ahí viene una primera etapa final ya
muy, muy, muy adelantada. Y para el día 26, que es domingo, la fecha máxima para
la revisión por parte del comité técnico de la lista de las quintetas. Y de ahí
sirvo la siguiente semana.