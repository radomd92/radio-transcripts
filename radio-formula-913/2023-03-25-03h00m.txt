TRANSCRIPTION: RADIO-FORMULA-91.3_AUDIO/2023-03-25-03H00M.AAC
--------------------------------------------------------------------------------
 el mundo no se detiene cada mañana despertamos con una nueva noticia la reforma
fue tornada a la cámara de diputados vamos a tener los detalles vamos a tener la
información entérese de todo lo que ocurre en el ámbito nacional e internacional
con Ciro Gómez Leyva lo último de la información sobre la intercepción ilegal de
una llamada telefónica Ciro Gómez Leyva en grupo fórmula lunes a viernes 7 de la
mañana hora del centro estás en grupo fórmula ya vuelven las noticias fórmula
financiera con Mari Carmen Cortés Marco Antonio Mares y José Juste las tres
voces más conocidas en el mundo de la economía en grupo fórmula hola qué tal
amigos muy buenas noches estamos aquí transmitiendo en vivo desde la ciudad de
méxico es fórmula financiera soy Mari Carmen Cortés y como siempre me da mucho
gusto que nos acompañe el día de hoy y me da todavía más gusto iniciar con una
muy buena noticia al menos para mí que yo sí marché en en defensa del INE hoy el
ministro oponente Javier Lainez Potisic a quien yo le mando 80 mil besos admitió
la demanda de controversia constitucional que interpuso el INE el INE interpuso
esta demanda de controversia en contra del decreto de la ley de la famosa ley B,
la ley general de partidos políticos de la ley orgánica de la Fuerza de la
Federación y que expide una nueva ley general de los medios de impunación en
materia electoral el famoso plan B y le instruye a las autoridades demandadas
que son en este caso el Congreso de la Unión y el Ejecutivo, el presidente que
presente una respuesta dentro del plazo legal que marca el que marca este
proceso pero en el inter que es la noticia más relevante es que se concede la
suspensión solicitada por el INE respecto de todos los artículos impugnados del
decreto para efecto de que las cosas se mantengan en el estado en que hoy se
encuentran y se fijen las disposiciones vigentes antes de la respectiva reforma
entonces bueno esto es muy interesante va seguramente no sé si mañana, no mañana
pero seguramente va a haber una respuesta muy enojada de la conservadora maldita
de la Suprema Corte de Justicia ahora se van a hablar de enojar contra los
ministros a mí me da mucho gusto porque la verdad es que sí está aquí lo que
está en juego es muy importante para el país Marco Mares muy buenas noches ¿Qué
tal cómo estás Maricarme Cortés? Muy buenas noches pues igual que tú celebrando
esta decisión de la Suprema Corte de Justicia de la Nación yo no le mando besos
pero le mando un fuerte abrazo al ministro oponente Javier Lainez Potishek él es
quien admitió esta demanda de controversia constitucional que interpuso el
Instituto Nacional Electoral bueno fue la corte pero él es el que está
respondiendo y pues también toma la decisión de conceder la suspensión
solicitada por el Instituto Nacional Electoral en contra del Plan B este decreto
por el que se reforma adiciona y de drogan diversas disposiciones de la Ley
General de Instituciones y procedimientos electorales de la Ley General de
Partidos Políticos de la Ley Orgánica del Poder Judicial de la Federación y que
expide una nueva ley general de los medios de impugnación en materia electoral
bueno pues hoy el la corte está dando un fuerte golpe al gobierno mexicano es el
gobierno el que presentó esta este plan de con el propósito de modificar
completamente al Instituto Nacional Electoral en una reforma que por los
expertos del ámbito político pues se sabe que hubiera representado un retroceso
histórico en la democracia mexicana afortunadamente la corte está respondiendo
como pues muchas personas estaban a la expectativa y con la esperanza de que
ocurriera era el último dique el último dique que pues representaba la
posibilidad de evitar que se llevara adelante esta reforma una reforma pues muy
muy fuerte una reforma que pues obviamente implicaba retroceder muchos años en
el ámbito de la democracia de nuestro país en el ámbito del sistema electoral
nacional mexicano pues organizado y construido por la ciudadanía y bueno por
otro lado se da a conocer por parte del INEGI ya tenemos en la línea Juan Carlos
Jiménez precisamente no a ver no es que ahora sé que no oigo a Juan Antonio
Jiménez estoy cambiando el nombre perdóname Juan Antonio Jiménez tú tienes la
nota precisamente de lo que pasó hoy en la Suprema Corte de Justicia te
escuchamos así es gracias muy buenas noches un placer saludarlos y como bien lo
señalan el ministro ponente Javier Maynes Potisé que admitió la demanda de
controversia constitucional que interpuso el Instituto Nacional Electoral en
contra del llamado plan de con lo que dicho decreto que era suspendido por
tiempo indefinido de esta manera el decreto por el que se reforma adiciona y
deroga en diversas disposiciones de la ley general de instituciones y
procedimientos electorales de la ley general de partidos políticos de la ley
orgánica del Poder Judicial de la Federación y que expide una nueva ley general
de los medios de impugnación en materia electoral así es el nombre completo del
plan de pues no va a estar vigente en este mismo acuerdo el ministro instructo
solicitó las autoridades demandadas tanto el Congreso de la Unión como el
Ejecutivo Federal que presenten su contestación dentro del plazo legal aunado a
ello el ministro Maynes Potisé concedió la suspensión solicitada por el INE
respecto de todos los artículos impugnados del decreto para efecto de que las
cosas se mantengan en el estado en que hoy se encuentran y rigen las
disposiciones vigentes antes de la respectiva reforma de esta suspensión fue
otorgada debido a que el decreto no sólo contiene normas de carácter general
sino también actos concretos de aplicación sobre los cuales la suspensión
generalmente se concede de esta manera la Suprema Corte ha resuelto en ocasiones
anteriores que en controversias constitucionales si corresponde otorgar la
suspensión aun tratándose de leyes cuando pudiera vulnerar de manera irreparable
los derechos humanos en este caso en este caso específico del llamado plan de ha
determinado el ministro oponente que se trata de la posible violación a los
derechos político electorales de toda la ciudadanía de esta manera donde pues
todavía todavía estamos con la ley anterior y está está suspendido por tiempo
indefinido el llamado plan de que les parece maricarle juan antonio cómo estás
te saluda marco antonio mares muy buenas noches gracias marco buenas noches juan
antonio platícanos tú que manejas toda esta información cuál es el plazo legal
en cuánto tiempo de cuántos días cuántas semanas estamos hablando para esta
respuesta a la que se está combinando a las autoridades demandadas al Congreso
de la Unión y al Ejecutivo Federal para que presenten su respuesta y pues los
tiempos también en función del proceso electoral que pues se estaba poniendo en
riesgo si hubiera sido aprobado este plan b el proceso electoral del 2024 y la
semana que entra para presentar precisamente la respuesta tanto el Congreso de
la Unión como la presidencia de la república vaya que esto como bien los señores
es muy importante porque había la duda cerca de lo que pudiera ocurrir si se
aprobara este plan de sin embargo también va a ser muy importante saber los
tiempos esos y ya no sabemos cuánto pueda tardar la Suprema Corte en que esta
esta ponencia de el ministro la Inés Potishek pueda llegar al pleno de lo que es
la Suprema Corte de Justicia esta sesión podría ser muy interesante para que los
11 ministros puedan votar al respecto pero por lo pronto tiene que ser en los
próximos en los próximos días a diles en los próximos cinco días que tienen que
presentar esta respuesta para que así pueda continuar este proceso que hay en
este llamado plan b. Y esta es una de varias controversias que tengo entendido
que se han presentado esta es la del INE pero hay otras controversias no hay
este para que no sé que hay que cuál procedimiento se van a juntar todas o qué
es lo que va a pasar. Todas estaban juntando principalmente con el ministro
Alberto Pérez Bellán pero esta controversia que viene directamente ya del
Instituto Nacional Electoral sin lugar a dudas es un golpe muy fuerte porque la
están congelando la están dando al refrigerador y bueno pues ante eso el plan de
parece que ya fue. Parece que ya fue Juan Antonio y afortunadamente lo que
estamos viendo hoy es que pues todo lo que se pretendía todo lo que se intentaba
por parte del gobierno mexicano hay que decirlo porque era el gobierno mexicano
el que estaba impulsando esta serie de cambios esta serie de modificaciones a la
ley y que buscaba de una manera muy radical pues transformar la estructura
reducir la estructura del INE y cambiar todo lo que implicaba su organización en
materia electoral. Esto ya definitivamente por lo pronto queda suspendido y está
vigente la ley actual. Todavía la ley actual va a quedar vigente. Queda
suspendido el llamado plan B pero todavía el proceso continua porque hay que
recordar que esto es tan solo la acción de inconstitucionalidad que interpuso el
INE pero también hay otras que han prestado los demás partidos políticos y bueno
hasta personas físicas han puesto también sus teces ante la Suprema Corte.
Estamos hablando de que habían recibido sin enamoramiento alrededor de 95 si no
es que más entonces todo esto falta muchísimo pero sin lugar a dudas la
respuesta que le da el INE es contundente y bueno pues ahora ahora yo creo que
alguien debe de estar preocupado no para ver si van a continuar ahora con un
plan C. Pues sí el plan C, Juan Antonio, viene todo el enamoramiento de los
consejeros y el muy probable porque ya está ahí también generando una gran
especulación el nombramiento de Berta Alcalde la hermana de la ministra de la
sacada del trabajo como nueva presidenta del INE. O sea ahora van a tratar de
destruir el INE desde adentro con los nuevos consejeros pero por lo pronto plan
B yo creo que ya bailó como tú dices. Sí, sí, sí, sin lugar a dudas lo que fue.
El plan A, pues eso fue desechado de manera instantánea. Ahora el plan B de los
ministros que era lo que la gente había incluso hasta hecho una marcha para que
lo hicieran y aquí está la respuesta por parte de los ministros está ya
completamente afuera este llamado plan B de la ley del INE. Pues sí, Juan
Antonio, afortunadamente para México, afortunadamente para la democracia de
nuestro país y como siempre muy completo tu reporte muchísimas gracias y muy
oportuno por supuesto. Gracias por estar aquí con nosotros en fórmula
financiera. Es para mí un placer estar con ustedes. Gracias Juan Antonio,
igualmente. Gracias. Pues sí, ya nos faltan los consejeros que estos van a ser
ya en los próximos días y ya la oposición dice que no quiere que se abierta
alcalde porque se está perfilando como presidenta del INE. Pues sí, la verdad es
que está muy muy muy cantada esa canción. Lo cierto es que pues ojalá que haya
algún movimiento que impida que se copte al Instituto Nacional Electoral con
este plan C. Es una gran victoria la que se tuvo hoy con la Suprema Corte de
Justicia de la Nación, pero es una batalla que todavía sigue. Bien por el
ministro Lainez, muy bien, pero vamos a un corte, no se vaya. Escuchemos en el
103.3 de FM y por el 970 de AM en grupo fórmula. Estás abriendo la conversación
en fórmula financiera. Recibe un abrazo al corazón de Adriana. Nadie te puede
ofender si no lo permites, ya que son tus ideas y expectativas acerca de cómo
deberían actuar las personas lo que te hiere. No intervengas en las decisiones
de otras personas. Déjalo ser y que vivan como mejor les plazca. Es su derecho.
Aléjate de la negatividad. Ábrete a la posibilidad de nuevas experiencias.
Perdonarte libera del astre del pasado. Te envió un abrazo al corazón. Síguela
en Instagram. Ay, viene la Cuarta Transformación con este ritmo que está
sabrosón unidos en una revolución que mi México lindo merece hoy. Es la 4T.
Dame, dame, dame todo el power. Perdón, esta semana en la hora nacional,
molotov. ¿Cómo saber si tienes depresión o ansiedad? Aquí toda la información.
Hablaremos también sobre los estereotipos de hombres y mujeres. ¿Tenemos las
mismas oportunidades? Además, nuevas secciones, invitados y mucho más. Los
esperamos en la hora nacional. Somos sus amigos Fernanda Tapia y Sergio Bonilla.
Esta es una producción de RTC de la Secretaría de Gobernación. Gobierno de
México. Para garantizar el interés superior de niñas, niños y adolescentes y
cuidar su sano desarrollo. El Senado de la República aprobó reformas legales
para impedir la discriminación en instituciones educativas. Promover sus
derechos humanos, propiciar actividades recreativas y culturales. Y garantizar
su acceso a la ciencia, la tecnología y la innovación. Se mejora así la vida y
se protege el futuro de las nuevas generaciones. Senado de la República.
Sexyquésima quinta legislatura. La interrupción legal del embarazo es un derecho
y debe ser garantizado por el Estado. Es sentencia. Con decisiones como esta, la
Corte construye una sociedad más justa e igualitaria, sin violencia ni
discriminación. Nos queda mucho por hacer. El 8 de menos recuerda que para
avanzar es urgente seguir reduciendo las desigualdades de género. Por la
protección de los derechos de todas las mujeres, niñas y adolescentes. Suprema
Corte. Visítanos en scjn.gov.mx8m. ¿Sabías qué? Los niños que miden menos de
1.37 metros deben ir en la parte trasera del auto y con una silla de seguridad.
Y si pesan menos de 10 kilos, con un asiento especial viendo hacia atrás. Soy
Gina Ibarra. Sigan en Twitter en Arroba de mamá y un bajo a mamá. Y seamos
amigos en el Facebook en De mamá a mamá. Sigue a Maricarmen Cortés en Twitter en
Arroba mcmaricarmen con la mejor información de negocios. Sigue a Maricarmen
Cortés en Twitter en Arroba mcmaricarmen con la mejor información de negocios.
Hola, bienvenidos a la CEDUBE. ¿Cómo estás Carlos? Muy buenas noches.
Maricarmen, mucho gusto. Muy buenas noches. Así es Marco y a tu auditorio. Oye,
cuéntanos. Ayer decíamos que todo lo que es simplificación de trámites hay que
darle la bienvenida. Porque de entrada implica menor extorsión, más facilidades
para abrir un negocio. ¿Qué es lo que están haciendo en esta simplificación para
el certificado de uso de suelo? Sí, mira, te platico muy buena noticia. Para un
sector muy importante que realmente detona y reactiva la economía en la Ciudad
de México hicimos una modificación al reglamento de la Ley de Desarrollo Urbano
para otorgar certificados de uso de suelo para aquellos negocios como salones de
fiesta, restaurantes, establecimientos de hospedaje, salas de cine, autosinema,
entre otros. Y fue una instrucción que giró la jefa de gobierno y yo como
titular de la Secretaría de Desarrollo Urbano y Vivienda pues hicimos una tarea
y estuvimos trabajando con la Canirac, con la Coparmex y vimos realmente que era
lo que podíamos omitir en el marco legal y que era lo más importante para ellos.
Entonces lo que hicimos fue todo este número de trámites que eliminamos y lo más
importante aquí también que hay que destacar que esos documentos que ellos no
podían acreditar eran como la boleta predial o el comprobante de pago de agua
que era uno de los requisitos indispensables anteriormente se eliminan porque
muchos de estos negocios, como tú sabes, están rentados. Entonces esa
documentación es titulada del inmueble, no es del inquilino. Entonces lo que
hicimos fue esta modificación y los requisitos ahora para poder otorgarles el
certificado de uso de suelo para que estén en la legalidad ante cualquier
autoridad, pues son documentos en los que se acredite la propiedad o la posesión
legal del inmueble o documentos con los que se acredita tener más de cinco años
ejerciendo el mismo giro, o sea, de manera continua. Y es la licencia sanitaria,
puede ser la licencia ambiental única, puede ser la licencia de manifestación de
construcción, hábito de terminación de obra y autorización de uso de suelo,
ocupación y licencia de funcionamiento, escrituras, copias de cualquier otro
documento que garantice que ha estado permanentemente en el mismo giro durante
cinco años o también incluso reportes fotográficos del interior o el exterior de
este inmueble. Eso es, digamos, en esencia, pero hay una cosa muy importante
también que hay que entender que este documento no podrá ser transferible. Será
únicamente para la persona quien lo transmite, la titularidad y durante la
permanencia que tenga ese negocio. Eso es la finalidad que tenemos, la COPA-MEX
y la camidad. Y esperemos que sea un alrededor de número de 10.000
establecimientos que puedan estar ya con este documento. Y como tú bien lo
decías al principio, pues estar en la legalidad de un negocio que garantice
también la seguridad para ellos, para los inversionistas y también enfrentarse
ante cualquier visita de cualquier autoridad. Ellos están automáticamente en
regla, es un pago de única vez por cada año de 1.922 pesos. Y yo creo que eso es
lo que hemos hecho en esta simplificación administrativa. Pues suena muy bien,
Carlos Alberto Ulloa. ¿Cómo estás? Te saluda Marco Antonio Mares. Muy buenas
noches, Carlos. Buenas noches, Marco. Pues igualmente hay muchas felicidades por
esta simplificación, todas las regulaciones y simplificaciones que vengan del
Gobierno, sobre todo para los negocios es algo muy importante. Hay un elevado
nivel de mortandad que ha sido histórico, no solamente hoy día, sino durante
mucho tiempo. Se ha visto que hay mucha mortandad de negocios que pues
prácticamente mueren al nacer de tanto trámite al que se ven obligados a
realizar. Por eso es importante que haya esta desregulación. Yo te preguntaría,
¿hay algún rezago en el trámite para este tipo de negocios hasta ahora? Una vez
que lo identificaron, me imagino decidieron simplificar. Y si esto también al
mismo tiempo está previendo evitar y poner candados para que se cuelen por ahí
los malosos, porque luego los lavadores de dinero andan muy sueltos. Estamos muy
atentos de alguna irregularidad. Sí lo hemos detectado, digamos, en los casos de
que sí hay gran número de establecimientos que requerían esta facilidad, esta
simplificación administrativa, que fue lo que platicamos con Candidático Parmex,
que sí era necesario. Hay un gran número de establecimientos que no cuentan con
ellos. Y pues esto sería una puerta muy indicada dentro del marco jurídico para
que se les pueda obtener este documento. Pero en lo que tú me dices, también
estamos muy atentos de aquellos establecimientos que no estén en su operación en
la regularidad. Tenemos otras distancias del mismo gobierno, como la PAUS misma,
por aquellos establecimientos que exceden de los decibeles o alguna
irregularidad de ocupación por parte del INVEA. Yo creo que estaremos también
muy atentos en alguna queja también con algunos vecinos, platicándonos,
atendiendo, escuchando. Pero te digo, esto va para aquella gente de Buenafe,
aquella gente que realmente necesita generar y potenciar su negocio. Y pues
recordemos que también, específicamente en el ramo restaurantero, pues es una
parte que más afectó la pandemia y es una parte que más genera empleo de manera
inmediata. Consideremos incluso a cocineros, garroteros, chefs, personal de
intendencia, como empleos directos, más los indirectos de los consumos para
producir sus alimentos. Yo creo que esta parte también, si ustedes recordarán
este proyecto, muy exitoso en la Ciudad de México, que es ciudad al aire libre,
que a los restaurantes se les permitió poner mesas, que fue quizás un poco al
principio la duda de cómo funcionaría. Y ha funcionado en la Ciudad de México a
comparación de otros países, que en la Ciudad de México se ha podido establecer
muy bien. Nos da un mensaje de seguridad de la Ciudad de México, donde puedes
estar con tus familiares, platicando con amigos durante la jornada, durante todo
el día. Y ahora es también parte importante de por qué la Ciudad de México está
ahora en otra situación. Y también recordarles que esto también, este sector
gastronómico, es muy importante ahora que viene el tianguis turístico en la
Ciudad de México, que arranque el domingo, que tenemos gente del medio
turístico, que somos fede de la Ciudad de México, de este evento nacional donde
tendremos visitantes extranjeros y nacionales. Y pues también es donde la Ciudad
de México, somos visto por muchos países como una parte muy atractiva para el
turismo y hay una actividad muy importante incluso este fin de semana. Oye, y
aquí este certificado de uso de suelo se tramita, que se paga de hechos cada
año, porque sí hay una gran rotación de restaurantes, hay restaurantes que sí
tienen ahí más de diez años, pero hay otros que desaparecen en un par de años.
De repente vas y llegas y ya no están. Mira, sí, es un pago anual de mil
ochocientos veintidós pesos. Eso se refrenda cada año. No tienes que volver a
tramitar el uso de suelo únicamente con actualizar el pago, se actualiza el
certificado de uso de suelo. Pero como decía al principio, este certificado de
uso de suelo es únicamente por el tiempo que perdura el negocio. Si el negocio,
el titular lo cierra o por equis circunstancia deja de operar, automáticamente
el uso de suelo pierde su legalidad. No es transferible, o sea, no puede
negociarse ese uso de suelo para poder transferírselo a otra persona que rentará
el local para poner el mismo negocio. Se pierde y la persona tendrá que tramitar
ese mismo uso de suelo u otro dependiendo del local y el giro que va a tener.
Pues es una buena medida. Carlos, aunque hay que considerar también que luego
los restaurantes tienen esa necesidad de estarse renovando, reinventando
constantemente y no solamente cambian el concepto, sino hasta de nombre y a
veces hasta de domicilio. ¿Cómo se va a considerar esto en el caso del
certificado de uso de suelo? El certificado de uso de suelo es únicamente como
lo tramiten, como lo acrediten, con esa misma dirección, con esa misma razón
social. Si hay un cambio, automáticamente el uso de suelo deja de tener
legitimidad. Oye, y ya que te traemos a la niña, pues vamos a aprovecharte.
¿Cómo va la situación de vivienda en México? Hay un, con todo esto de la
pandemia, con todo esto de la alza de las tasas de interés, hay una escasez de
vivienda, de créditos. Ya no se permiten edificios demasiado altos en toda la
ciudad. ¿Cómo va todo este proceso? Sí, te cuento. Mirá, nosotros cuando
entramos en la instrucción de la jefa de gobierno era regular esta parte
invasiva de control, de medida que había sobre ciertos desarrolladores
inmobiliarios afectando la calidad de vida de los vecinos. Hubieron edificios
emblemáticos que estaban en la total irregularidad. Se hizo procesos jurídicos
en esos casos. Pero después hablamos incluso con las cámaras que tienen que ver
con el asunto de la vivienda. Platicamos con ellos y entramos en una dinámica
totalmente diferente. Tenemos que considerar también que la Ciudad de México
tiene problemas de agua, más en este periodo de estiaje. Tenemos alcaldías donde
tenemos este problema de escasez donde se agudiza más. Pero sin embargo tenemos
también vialidades que son permitidas, que son vías primarias, que no afectan a
una colonia, que están, digamos, de cierta forma con un potencial de desarrollo
muy accesible. Y ahí es donde estamos trabajando con los desarrolladores para
que se invierta ahí. Tenemos, por ejemplo, Avenida Reforma, Insurgentes,
Periférico, Circuito Interior. Y esta parte que hicimos es un plan maestro muy
importante también, que es en la zona rosa que queremos reactivar su
habitabilidad. Hicimos un plan maestro de lo que es Lieja hasta Insurgentes,
entre Reforma y Chapultepec. Ahí tenemos ya un levantamiento muy importante de
los predios que están abandonados. Y la idea es reactivar y valorizar y
centralizar la vivienda, pero también que haya un control de precios. Pues ya
estaremos platicando contigo más ampliamente sobre este tema que sin duda es muy
relevante. Carlos Alberto Ulloa, titular de la CEDUBI. Gracias por haber estado
aquí con nosotros. Marical, me gusta escucharlo, que me hayan tomado la llamada
para permitirme hacer la entrevista. Muchas gracias también a toda tu auditorio.
Gracias. Hasta luego, Carlos. Vamos a un corte. Esto es Fórmula Financiera.
Estás abriendo la conversación en Fórmula Financiera. Fórmula Financiera. XHACE
91.3 FM Radio Fórmula Mazatlán. Transmitiendo con 10.000 watts de potencia
aparente radiada. Radio Fórmula Mazatlán. Abriendo la conversación. En Home
Depot juntos hacemos más para refrescar tu hogar con promociones y precios bajos
exclusivos de la marca LG. Aprovecha ahorros de hasta 20% en Mini Splits con
instalación básica gratis en productos seleccionados. Además de ahorros de hasta
30% en línea blanca. Home Depot. Haces más, logras más. Consulta más detalles en
HomeDepot.com.mx hasta más de 31. Llegó la temporada de calor y con ella los
mejores descuentos para refrescar tu espacio. Ven a Liverpool del 13 al 26 de
marzo y aprovecha descuentos sorprendentes en la Feria del Aire. En la que
podrás encontrar hasta 47% de descuento en aires acondicionados y ventiladores
de piso y techo. En todo lugar y en todo momento Liverpool es parte de mi vida.
La acuarezma está buena en ley. Encuentra las mejores ofertas y productos
siempre frescos, sabrosos para ti y tu familia. Agua Natural Gerber de 4 litros
2x49.90. Lava traste sección limón 900 mililitros 2x79.90. Limpiador fabuloso 1
litro 27.90. Buena la acuarezma en ley. Valido al 24 de marzo. Con el programa
Bien Pesca las familias de Sinaloa están construyendo un futuro mejor. Lo
ocupamos para reparaciones de lancha, artes de pesca, cosas que requerimos aquí
en el trabajo. Como Don Blas, más de 30 mil pescadores ya reciben apoyos para
producir más. Sinaloa, gobierno con sentido social. Las noticias de mayor
interés en México y el mundo desde la particular perspectiva de Joaquín López
Dóriga. Fíjese las dimensiones de la movilización delincuencial. Esto es crimen
organizado. No puede entenderse de otro modo. Lunes a viernes, 1.30 de la tarde.
Sábados y domingos, 3 de la tarde. Hora del centro. La imponidad. ¿Por qué?
Porque no nos toca. No tiene ningún costo leer. López Dóriga, abriendo la
conversación en Grupo Fórmula. ¿Sabías qué? En estos días de verano, no gastes
más de lo que puedes pagar. Es mejor que aprendas a manejar tu dinero de manera
simple y sensata para alcanzar metas financieras personales y familiares.
Felices vacaciones. Soy Gina Ibarra. Sigan en Twitter en Arroba de mamá y un
bajo a mamá. Y seamos amigos en el Facebook en De mamá a mamá. Cultura,
curiosidades y más. Aproximadamente el 5% de los niños que mueren en México
mueren de cáncer. Radio Fórmula. Rompiendo la distancia. Todos nuestros
contenidos en www.grupofórmula.com.mx. Escuchas Grupo Fórmula. Facebook.com.
Diagonal Fórmula Financiera. Y en Twitter en Arroba Alebrijes1. Regresamos aquí
a Fórmula Financiera y tenemos en la línea ICIDO. Y tenemos en la línea Isidoro
Pastor, director general de la IFA, el Aeropuerto Internacional Felipe Ángeles.
Isidoro, muchas gracias por darme la llamada. ¿Cómo estás? Muy buenas noches.
Bien, Mari Carmen. Un abrazo afectoso desde la IFA. Gracias. Para Marco Antonio.
Gracias. Ya no pudimos ir a la celebración del primer aniversario. Pero cifras.
La IFA, finalmente, yo lo conocí y gracias a la invitación estuve ahí. Y pues
sí, yo lo digo, sí está bonito el aeropuerto, está bien, está funcional. Lo
difícil es llegar. Pero creo que ya están trabajando mucho en facilitar el
acceso, que va a ser clave, no solamente la recuperación de la Categoria Aérea
Número 1, sino facilitarnos el acceso a llegar. Pero ¿cómo vamos en ese sentido
en cuanto a cuestión de cifras? Hasta el día de ayer, 23 de marzo, hemos tenido
14.651 operaciones aéreas, lo que han representado haber transportado 1.412.343
pasajeros. Eso es por lo que respecta al transporte de pasajeros. Por lo que
respecta al manejo de la carga, llevamos un por medio de 1.700 toneladas de
carga entre el día 28 de febrero hasta la fecha. Isidoro Pastor, General Isidoro
Pastor, ¿cómo estás? Te saluda Marco Antonio Mares, muy buenas noches. Bien,
Marco, buenas noches a sus órdenes. Me da mucho saludarte. Yo quisiera
preguntarte respecto de la operación de la IFA. Sin duda, nosotros tuvimos la
oportunidad de visitarlo y de estar ahí. Es una instalación bastante buena, una
instalación muy recomendable incluso. Sin embargo, hemos visto que a lo largo
del tiempo, ya pasó un año, todavía no ha podido tener un incremento importante
en la masa crítica y al mismo tiempo ha tenido que mantener elevados niveles de
costos. ¿Si nos puedes dar un poco los números de lo que ha representado el
apoyo que ha recibido la IFA de parte del gobierno mexicano para subsidiar esta
operación? Sí, para el ejercicio presupuestal de este año tenemos autorizados en
el gasto de operación 400 millones de pesos. Nos hacen falta 1.700 millones de
pesos que ya estamos solicitando a la Secretaría de Hacienda, nos lleve una
ampliación presupuestal para poder cubrir las necesidades de vida y operación
del aeropuerto. Me parece que en la ocasión anterior les mencionaba que el
aeropuerto tiene que funcionar con un seguro de toda la instalación y para
terceros también, con un servicio de control de fauna, con un servicio de
mantenimiento de las áreas restringidas operacionales, con un servicio médico
que es mandatorio para poder funcionar. Y, obviamente, para suministrar energía
eléctrica también gastamos en gas natural y especialmente la nómina también.
Entonces, ¿tienen un déficit, por así decirlo, de 1.700 millones de pesos en el
presupuesto, que son los que tendrá que autorizarlas a la Secretaría de Hacienda
para este año? Sí, estamos en ese camino y prácticamente estamos en las últimas
fases de la autorización de ese presupuesto y con ello estar listos para operar
todo 2023. Como usted sabe, el dinero no se lo entregan en una sola rogación.
Hay un calendario en el que se va otorgando el presupuesto autorizando en
función de las necesidades que se van cubriendo a través de los 12 meses del
año. Claro, estamos platicando con el director general de la IFA, Isidoro Pastor
Román, el general Isidoro Pastor Román. Román, yo preguntaría respecto de esta
numeralia, a partir de que inició operaciones a la fecha, el monto de este
subsidio es mayor, me imagino, si nos puedes dar la cifra. Y por el otro lado,
¿en cuánto tiempo más calculan, porque ya inició la operación de carga en la IFA
y esto le va a dar mayor movilidad, mayores ingresos, en cuánto tiempo más
calculan que podrían llegar al punto de equilibrio? Si se diera la posibilidad
de iniciar reparaciones mayores en el aeropuerto de la Ciudad de México en sus
edificios terminales, además de las zonas operacionales, eso pudiera detonar que
un buen número de operaciones comerciales se vinieran para acá. En el caso
contrario, en un crecimiento moderado, como está contemplado en el programa
maestro de desarrollo, ya actualizado, no recuerdo si la ocasión anterior les
mencioné que para poder iniciar operaciones cualquier aeropuerto necesita
presentar ante la autoridad aeronáutica su programa maestro de desarrollo con
una provisión asistencial. Y obviamente cuando esto inicia, como la empresa
todavía no estaba constituida, o ya estaba constituida, pero estábamos apenas
haciendo la contratación de personal, la estructura organizacional estaba
incompleta, especialmente en el área de planación estratégica, y por eso es que
se contrata una empresa especialista en provisión de la demanda, quien mencionó
estos dos famosos 2.4 millones de pasajeros. Y eso lo hace con fundamento en una
posibilidad de que en el mes de agosto del año pasado pudiéramos tener un
promedio de 200 operaciones diarias. Obviamente expresado por estas cantidades,
expresadas por las propias aerolíneas que están trabajando con nosotros, no se
dio este número de reparaciones porque argumentaron que la pandemia evitó que
les entregaran aeronaves nuevas y en reparación por razones de ese retraso en la
línea de producción y en la reparación de esos componentes mayores. Y nosotros
ya con esa información hicimos una reestimación de esa demanda y planteamos en
ese primer año 1.3 millones de pasajeros, pues ya los rebasamos. ¿Entonces
ahorita las operaciones diarias en cuánto están? ¿Cuántas operaciones diarias
son? Seguimos con 60, Maricarle. 60, ok. Prometo que se promene un día hasta 90
y a veces bajamos a 50. Ok. Claro, y en este sentido en cuanto a las operaciones
ya nos hablabas de los escenarios que se están previendo, pero también no sé si
están incluyendo en estos escenarios la posibilidad de que a partir del próximo
mes de julio, como lo calculan las autoridades de la Secretaría de
Infraestructura, Comunicaciones y Transportes, pudiera recuperarse la categoría
1. ¿Esto de qué manera impactaría a la IFA? ¿De qué manera aumentaría el flujo
de pasajeros y de operaciones aéreas? Como a toda la industria, en todo el
sistema aeroportuario mexicano, pues este sería un impacto positivo. En el caso
de la IFA, ya las diferentes aerolíneas, específicamente las tres nacionales que
están con nosotros, tienen puesta la mirada en diferentes ciudades de Estados
Unidos. Y obviamente en este ejercicio de proyección de esos vuelos, las
ciudades ya están definidas. Se están esperando nada más que se recupere la
categoría para convertir en realidad estos vuelos entre Estados Unidos y México.
Y de manera natural, en el momento en que tengamos vuelos internacionales, va a
haber una necesidad de tener vuelos domésticos con mayor frecuencia para poder
conectar a los pasajeros que van al interior de la República y que lleguen del
extranjero. Y bueno, por otro, ya viene también la obligación de que todas las
aerolíneas de carga, eso a partir de julio me parece, se vayan a la IFA. ¿Esto
va a ayudar mucho al aeropuerto? Sí, bastante, Mari Carmen, es potestad de las
aerolíneas venirse a este aeropuerto o irse a otro, donde convenga hacia sus
intereses. Sin embargo, ya tenemos nosotros las cartas sin tensión de cuatro
aerolíneas cargueras para venirse con nosotros y tres que ya están operando
aquí. De tal manera que también los recintos fiscalizados de los 15 espacios que
tenemos para carga doméstica e internacional, la mitad ya están listos y están
operando. Hemos tenido hasta el día de ayer 2.839 operaciones de carga. Aquí es
conveniente hacer una aclaración de este asunto de la carga que se va a la ICM.
De estas 2.839 operaciones de carga hay una modalidad que se denominan
tránsitos. Es en donde la aeronave llega al aeropuerto de la Ciudad de México,
al aeropuerto Felipe Ángeles, se sube la mercancía a los vehículos terrestres y
salen al otro aeropuerto para desaduanizar en ese lugar. ¿Por qué? Porque ese es
el modelo de negocio de la agente aduanal o de la propia empresa. Hemos tenido
tránsitos tanto de aquí de la AIFA a la ICM como de la ICM a la AIFA. Y esto lo
reporta específicamente la Agencia Nacional de Aduanas, que en este periodo tan
corto que llevamos de operación de carga de menos de un mes, han recaudado 1.480
millones de pesos. Pues es una cantidad importante. En general, si nos pudieras
mencionar los nombres de las líneas de carga que ya están operando y las que
están por... Sí, tenemos DHL, China's Outer y Ausome. Son las tres aerolíneas
que ya están aquí con sus aeronaves volando de manera regular con carga en los
aeropuertos. Y las que están en proceso de venir, que recuerdo yo ahorita, no
tengo los... Bueno, es más, si me permiten, mejor no lo digo porque luego se me
molesta la línea. Así que no nos oye nadie. Sí, sí. Tenemos cuatro cámaras de
extensión de aerolíneas de carga internacionales que ya están buscando un
espacio aquí con nosotros y estamos en ese proceso de relación contractual. Pues
ya te estaremos buscando general para que nos de la noticia. Muchísimas gracias
por la oportunidad, se nos acabó el tiempo. Gracias por haber estado aquí con
nosotros en Fórmula Financiera. Muchas gracias, un abrazo con mucho afecto.
Gracias, es que era el sidero pastor director general de la AIFA. Gracias, vamos
a un corte. ¿Y si sí? Consulta más información en www.hsbc.com.mxdiagonal.icc.
Noticia importante. Porque nacimos para ayudar. Ahora en farmacias similares
todos los jueves son los días de bondad. Lo invitamos a hacer su compra durante
los jueves de bondad. Y si a ustedes se le ha elegido, el Dr. Simi se le invita.
O sea, es gratis. Venga y contájese de bondad. Imagina poder comprar el vuelo
que quieras y reservar hoteles todo con tus puntos. Imagina vivir experiencias
únicas de gastronomía, los mejores conciertos y eventos. Imagina sentirte
realmente reconocido gracias a los puntos que te dan las mejores tarjetas.
Presentamos Santander Unique Rewards. Conoce más en
www.uniquerewards.santander.com.mx. Teniendo en cuenta la difícil situación en
que estamos, le sugerimos pensar en cosas bonitas. La bondad, la verdad, la
justicia y sobre todo en la ayuda a los demás. Por lo que les recomendamos
volver a ver la serie Mi Vida es Lucha. Sobre la vida de Víctor González, Dr.
Simi. Nominado al Premio Nobel de la Paz 2022. Que puede ver en redes poniendo
la frase Mi Vida es Lucha. Esta serie tiene más de 400 millones de
reproducciones. Imagina poder comprar el vuelo que quieras y reservar hoteles
todo con tus puntos. Imagina vivir experiencias únicas de gastronomía, los
mejores conciertos y eventos. Imagina sentirte realmente reconocido gracias a
los puntos que te dan las mejores tarjetas. Presentamos Santander Unique
Rewards. Conoce más en UniqueRewards.santander.com.mx. En el Antiguo Egipto
usaban peces que producen electricidad para aliviar ciertos dolores del cuerpo.
Obras de arte. Kaspar David Friedrich fue un pintor alemán que deleitó al mundo
con sus grandes obras. Una de las más representativas del Renacimiento fue el
caminante sobre un mar de nubes. Un óleo sobre tela de nubes. Un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro
de un Oro de un Oro de un Oro de un Oro de un Oro de un Oro de un