#!/bin/bash
set -x

echo `pwd`

# download from pi
for d in `cat sync_from_pi`; do 
	echo $d
	for day_ago in 1 2 3 4 5; do
		rsync -rv pi@192.168.2.106:/mnt/radio/$d/$(date +%Y-%m-%d --date "$day_ago day ago")* ../${d}_audio/
	done
	rsync -rv pi@192.168.2.106:/mnt/radio/$d/$(date +%Y-%m-%d)* ../${d}_audio/
done

if [[ -z $UPLOAD_ONLY ]]; then
	exit 0
fi

# upload to pi
for d in `cat sync_to_pi`; do 
	echo $d
	rsync -rv ../$d/* pi@192.168.2.106:/mnt/radio/$(echo $d | sed -e "s|_audio||g")
done

echo 'end transcription work'

