#!/bin/bash
TXT_FOLDER=/home/rado/dockerx/radio_transcripts/abc-brisbane
FOLDER=${TXT_FOLDER}_audio
if [[ ! -d $FOLDER ]]; then
    mkdir -p $FOLDER
fi

if [[ ! -d $TXT_FOLDER ]]; then
    mkdir -p $TXT_FOLDER
fi

if [[ -f ${FOLDER}/last_pid ]]; then 
    kill $(cat ${FOLDER}/last_pid)
fi
echo $$ > ${FOLDER}/last_pid

NOW=$(date +%Y-%m-%d-%Hh%Mm)
STREAM_URL="https://live-radio01.mediahubaustralia.com/4LRW/aac/;*.mp3"
OUTPUT_FILE=${FOLDER}/${NOW}.mp3
echo $OUTPUT_FILE > ${FOLDER}/latest

curl -L $STREAM_URL -o - > $OUTPUT_FILE &
echo $! > ${FOLDER}/last_pid
