#!/bin/bash
FOLDER=/home/rado/dockerx/radio_transcripts/radio-formula-91.3_audio
if [[ -f ${FOLDER}/last_pid ]]; then 
    kill $(cat ${FOLDER}/last_pid)
fi
echo $$ > ${FOLDER}/last_pid

NOW=$(date +%Y-%m-%d-%Hh%Mm)
STREAM_URL="https://n0a.radiojar.com/z86v178e3p8uv"
OUTPUT_FILE=${FOLDER}/${NOW}.aac
echo $OUTPUT_FILE > ${FOLDER}/latest

curl -L $STREAM_URL -o - > $OUTPUT_FILE &
echo $! > ${FOLDER}/last_pid
