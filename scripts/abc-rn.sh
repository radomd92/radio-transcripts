#!/bin/bash
FOLDER=/home/rado/dockerx/radio_transcripts/abc-rn_audio
if [[ -f ${FOLDER}/last_pid ]]; then 
    kill $(cat ${FOLDER}/last_pid)
fi
echo $$ > ${FOLDER}/last_pid

NOW=$(date +%Y-%m-%d-%Hh%Mm)
STREAM_URL="https://live-radio01.mediahubaustralia.com/2RNW/mp3/"
OUTPUT_FILE=${FOLDER}/${NOW}.mp3
echo $OUTPUT_FILE > ${FOLDER}/latest

curl -L $STREAM_URL -o - > $OUTPUT_FILE &
echo $! > ${FOLDER}/last_pid
