#!/bin/bash
FOLDER=/home/rado/dockerx/radio_transcripts/ndr-info_audio
if [[ -f ${FOLDER}/last_pid ]]; then 
    kill $(cat ${FOLDER}/last_pid)
fi
echo $$ > ${FOLDER}/last_pid

NOW=$(date +%Y-%m-%d-%Hh%Mm)
STREAM_URL="https://f121.rndfnk.com/ard/ndr/ndrinfo/hamburg/mp3/128/ct/stream.mp3?cid=01FBQ2FR04JVDRRM969ZTAMZNK&sid=2ND55vdOMpAo0seyjk2VAu3Ixjf&token=_zon8gBq7JXZi8xUPwX9bF3JTBewzIx74orHzYlMH9A&tvf=Ji6WyxS-TRdmMTIxLnJuZGZuay5jb20"
OUTPUT_FILE=${FOLDER}/${NOW}.mp3
echo $OUTPUT_FILE > ${FOLDER}/latest

curl -L $STREAM_URL -o - > $OUTPUT_FILE &
echo $! > ${FOLDER}/last_pid
