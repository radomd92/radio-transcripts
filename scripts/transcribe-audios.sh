#!/bin/bash
set -x
cd ..
echo `pwd`
if [[ -f /tmp/transciption.lock ]]; then
	lock=`cat /tmp/transciption.lock`
	if [[ ! -z $(ps aux | grep $lock | grep -v grep | wc -l) ]]; then
		echo "already running..."		
		exit 0
	else
		exho "but no process detected, continuing"
	fi
fi

echo 'begin transcription work'
echo $$ > /tmp/transciption.lock

export HSA_OVERRIDE_GFX_VERSION=10.3.0
source /dockerx/rbcst-env/bin/activate

MODEL=medium python transcriptor.py es radio-formula-91.3_audio/*.aac
MODEL=medium python transcriptor.py es mexiquense-radio_audio/*.mp3

MODEL=large python transcriptor.py fr rfi-afrique_audio/*.mp3

MODEL=large python transcriptor.py de ndr-info_audio/*.mp3

MODEL=large-v2 python transcriptor.py en abc-rn_audio/*.mp3
MODEL=large-v2 python transcriptor.py en abc-brisbane_audio/*.mp3
MODEL=large-v2 python transcriptor.py en bbc4_audio/*.mp3

MODEL=medium python transcriptor.py ja kzoo_audio/*.mp3

rm /tmp/transciption.lock
echo 'end transcription work'

