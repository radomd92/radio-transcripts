TRANSCRIPTION: MEXIQUENSE-RADIO_AUDIO/2023-03-20-14H00M.MP3
--------------------------------------------------------------------------------
 Suggesto Decret picked El Estado de México tiene una misma frecuencia. Nuestra
señal viaja por el aire. XHMS Noventa y uno punto siete FM Ename Kameka XE TUL
Mil ochenta AM En tu titlan XHZUM Ochenta y ocho punto cinco FM En Zumpango
XHVAL Ciento cuatro punto cinco FM En Valle de Bravo XHGEM Noventa y uno punto
siete FM En el Valle de Toluca XEGEM Mil seiscientos AM En METPEG Y las
repetidoras XHATL Ciento cinco punto cinco FM En Aslacomulco XETJ Mil doscientos
cincuenta AM En Tejupilco XEGEM Mil suquice XEGEM ¡La paz, hay que cuidarla! El
voto libre no se toca. Mi INE es valioso porque mi INE nos une. Mi voto es mi
voz en México. La programación de este horario es clasificación AA. Hola, soy
Arki el Guardacuentos. Y a continuación te voy a compartir un gran cuento. En un
llano grande se encontraba un árbol grande, frondoso. Ahí vivía un hermoso... Un
hermoso duende. No, no, había un hermoso grillo. ¿Era verde, grande, pequeño?
Dejen de interrumpir. ¿Tenían tenitas? Déjenme contarles las cosas que me
pasaron. ¿Tenían tenitas? No, no, no, no. ¿Tenían tenitas? Déjenme contarles las
aventuras de este grillo. Que no era un grillo cualquiera. ¿Cómo? ¿Tenía
superpoderes? ¿Viajaba como Flash? ¿Era un mago? ¡No! ¡Déjenme contarles, por
favor! ¡Sí! Este es un grillo madrugador. Con él podrás viajar, aprender,
explorar, sentirte un astronauta, un químico. Aquí tu imaginación no tendrá
límites. ¡Hasta los papás se sienten niños de nuevo! ¿Cómo? ¿Es eso posible?
¡Sí, claro! Aquí todos somos niños. Todos somos grillos. Todos jugamos con la
imaginación. Así que tomen su lugar y prepárense para una nueva aventura
aquí......en Grillos Madrugadores. Muy, pero muy buenos días, mis queridísimos
grillos madrugadores. Es lunes coquetón, es lunes de descanso, es lunes de fin
de semana largo. Y me parece correcto el poderlo saludar a través de las
frecuencias de la Mexiquense Radio......en este inicio de semana, por supuesto.
Aprovechando el fin de semana largo, por supuesto también pasándonos las de
pelos. ¿Y por qué razón? Porque fíjense que yo sé que ustedes, además de tener
un fin de semana reparador......y este lunes, es especial mañana. Bueno, pues en
teoría, ¿verdad? Mañana iniciaría la primavera, aunque en realidad el equinoccio
de primavera inicia hoy. Pero bueno, ahí está la información. Ahí están los
estudiosos y los investigadores que les saben este asunto. Por lo pronto
nosotros nos lo estamos pasando increíble. ¿Saben por qué? Porque también
estamos festejando un natalicio, natalicio de Benito Juárez......y también
estamos festejando que la semana va a estar increíble. Así es que les mandamos
un gran saludo. Por supuesto invitarlos para que se la pasen de pelos. E iniciar
esta semana como corresponde, como todos los lunes......que aquí en el programa
lo iniciamos con muchísima música. Así es que en esta ocasión no va a ser la
excepción. Pero déjenme decirles que este programa va a ser especial. ¿Por qué
razón, mis queridísimos grillos? En verdad queremos agradecer todos,
absolutamente todos los mensajes que nos llegan. En verdad, mis queridísimos
grillos, les agradecemos muchísimo......el que se tomen ese tiempo para
mandarnos sus mensajes, comentarios, saludos......recomendaciones musicales. Y
créanos que los viernes ya no nos aguantan. Bueno, de hecho, si se dieron
cuenta, toda la semana estuvimos ahí......pasando orrieles de mensajes de voz y
también estuve leyendo mensajes de texto. Y estuvimos apuradísimos para que
pudiésemos saludar a cada uno de ustedes. Bueno, para iniciar, les queremos
agradecer toda esta participación. En verdad, ya lo saben que este programa no
sería absolutamente nada sin su participación......sin sus orejas, sin sus
escuchas, sin el compartirnos, ¿verdad? Las mañanas y, por supuesto, el hacernos
reír también......con sus ocurrencias, con sus videos, con toda aquella
dinámica. Compartirnos cuando están enfermitos, compartirnos también cuando
están muy felices......compartirnos cuando están en exámenes, sean medio
presionadones. Y ojalá que escuchar este espacio los pueda relajar y también
mandales buenas vibras......y compartir con ustedes cuando están enfermitos,
cuando se están recuperando......cuando están pasando un día mal, también ojalá
y lo podamos compartir......porque eso hacemos los amigos, compartir
absolutamente todo y apoyarnos en todo. Mis queridísimos grillos, yo les
agradezco muchísimo el favor de su atención. Yo les agradezco también esos
momentos en los que pues andamos medio tristones, ¿verdad? Andamos medio
cabizbajones, pero a final de cuentas sus mensajes, comentarios,
opiniones......sugerencias musicales y todo, va cambiando mi estado de ánimo y
voy viendo las cosas de otra manera. Así es que abrazos para todos ustedes y
muchísimas gracias. Y bueno, en ese sentido, ¿verdad? Ya después de aventarme
toda esta cantaleta......¿qué les parece si este lunes también lo dedicamos para
mensajes de voz? Porque son muchísimos y varios grillos nos han escuchado y
luego me hablan y me dicen......no, Paco Pelos, es que mi mensaje no pasó. Sí,
sí pasa, sí pasa porque son muy importantes para nosotros......cada uno de sus
mensajes, cada uno de sus textos, cada uno de sus videos, de sus fotografías.
Créanos que es muy valioso para nosotros cuando nos comparten todas estas
cosas......y que nos comparten un pedacito de su vida. Eso nos puede encantar
muchísimo. Y bueno, pues en ese sentido queremos justamente hacer este lunes de
mensajes de voz......y también algunos mensajes de texto y también poner
muchísima música......porque los lunes son de complacencias, los lunes son para
iniciar la semana con muchísimo ritmo......los lunes son para iniciar la semana
con movimiento de cadera, con este movimiento que nos llena de ondas
positivas......y divertidas y por supuesto para alegrarnos el día. Así es que,
sin más preámbulo, déjenme decirle a Calitos Mandrugador que si ya tiene la
lista preparada......ya todo está listo, los rieles, los mensajes, todo mi
querísimo Calitos. Bueno, pues sin más preámbulo entonces, ¡que suene la música
en esta mañanita mis queridísimos grillos! ¡Mándanos tu mensaje al 722-443-1600!
¡Grillos Madrugadores, tu compañero del era! Hola Paco Pelos, ¿qué crees?
Hicimos un librito y ayer......había una feria de libros que estaba súper
padre......y sabes que, se me murió mi tortuga pero tengo otra, o sea no de
verdad, sino que de mentira......ya luego te cuento mañana, adiós, va. Hola Paco
Pelos, soy Karime, ya voy con mi nieto para la escuela. ¿Me puedes poner la
canción de......Pepo......Pepo Cumbia? Saludos a Chipilín, que ya no esté muy
gordito......que ya esté con su......que ya esté con su musculoso, que ya esté
tan fuerte, bye. ¡Grillos Madrugadores! Hola Paco Pelos, soy Iker, ya voy
caminito para la escuela. ¿Me puedes poner la canción de......La Pansil
Alombrís? Gracias, bye, saludos a Chipilín. Ya no estoy más gordito, Chipilín.
Hola Paco Peloso, me llamo Valentina Martínez Valdés. Quería ver si me podía
dedicar la canción de la Orquesta de Animales de Cri-Cri. Gracias. ¡Ay qué
bonito baila Pepo! ¡Cue, cue, cue! ¡Cue la Pepo! ¡Cue, cue, cue! ¡Cue la Pepo!
¡Cue, cue, cue! ¡Cumbia! Esta fiesta ya va a comenzar y la Pepo Cumbia va a
bailar con la Pepo Cumbia. Que levanten la mano, la Pepo libre. Que levanten la
mano, la que me sigue. ¡Ay qué bonito baila Pepo! ¡Cue, cue, cue! ¡Cumbia la
Pepo! ¡Cue, cue, cue! ¡Cumbia la Pepo! ¡Cumbia! Esta fiesta ya va a comenzar y
la Pepo Cumbia va a bailar con la Pepo Cumbia. Que levanten la mano, la Pepo
libre. Que levanten la mano, la que me sigue. ¡Ay qué bonito baila Pepo! ¡Cue,
cue, cue! ¡Cumbia la Pepo! ¡Cue, cue, cue! ¡Cumbia la Pepo! ¡Cue, cue, cue!
¡Cumbia la Pepo! ¡Cumbia! ¡Cumbia la Pepo! ¡Hola! Soy Spiki, soy una taza que
viaja por todo el mundo. Mi misión es enseñarte a hablar inglés. A lo largo de
esta serie radiofónica hemos aprendido vocabulario correspondiente al nivel A1 y
A2 de acuerdo al marco común europeo de referencia y también hemos practicado la
expresión oral en inglés. El marco común europeo de referencia para las lenguas
es el estándar internacional que define la competencia lingüística. Se utiliza
en todo el mundo para definir las destrezas lingüísticas de los estudiantes en
una escala de niveles inglés desde un A1 nivel básico hasta un C2 para aquellos
que dominan el inglés de manera excepcional. A lo largo de la serie Spiki, la
tacita bilingüe, hemos contado con la participación de niñas y niños que
estudian inglés y que quieren mejorar sus habilidades al hablar. Hablar inglés
no solo te abre las puertas a otros mundos, sino que también fortalece tu
autoestima porque te hace sentir capaz de comunicarte en una lengua adicional al
español. Si estudias inglés y quieres participar en las grabaciones de Spiki,
escríbenos a spikilatacita.com y nos pondremos en contacto contigo. Gracias por
escucharnos. Sigue aprendiendo inglés aquí en Mexicense Radio. ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete! ¡Suscríbete!
¡Suscríbete! ¡Suscríbete! ¡Suscríbete!