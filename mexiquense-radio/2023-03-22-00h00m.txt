TRANSCRIPTION: MEXIQUENSE-RADIO_AUDIO/2023-03-22-00H00M.MP3
--------------------------------------------------------------------------------
 del gobierno. Más información en una hora. Quienes conocimos en la pasada de
GSC y donde pudimos probar su nuevo proyecto, México 1921. Vienen a contarnos
con más calmita del proyecto que viene y qué es Macro Interactive y cómo ven el
panorama de la industria de videojuegos en México hoy en día. Quédense para esta
media hora en nivel 2.0, pero antes les dejo los números en cabina de aquí de
1600 AM. Recuerde que puede llamarnos al 722-275-5627, ese es el 722-275-5627,
al alada sin costo 800-593,000 y un mesejito de texto o de WhatsApp al
722-443-1600. 722-443-161600. Yo soy Mariano Ramírez, estás escuchando nivel 2.0
en Mexiquense Radio. Me encuentro con Paola Vera y Pedro Álvarez, ellos vienen
de Macro Interactive, les doy la bienvenida, ¿cómo están chicos? ¿Qué tal
Mariano? Muchas gracias. ¿Qué tal Mariano? Muchas gracias, buenas tardes. El
motivo de este entrevista es que nos puedan platicar un poco no solo de Macro
Interactive, de Macuya, sino que puedan compartir sus experiencias como un
estudio mexicano y uno que está armando este proyecto que vimos en la EGS, que
era un videojuego basado en la post o pre revolución, si mal no recuerdo,
revolución mexicana, pero ahorita platicamos de eso. Quería preguntarles
rápidamente que nos cuenten qué es Macro Interactive, cómo surgió, qué es. Claro
que sí, Mariano, pues primero que nada, saludos a todo el auditorio y muchas
gracias por invitarnos. Macula es un estudio de desarrollo de videojuegos
situado en la ciudad de México, nació hace un poquito más de dos años de manera
formal, pero en realidad Macula nació como idea hace muchos, muchos años. Se
fundó por un grupo de amigos, los cuales me incluyo y pues esta idea de
justamente el videojuego en la post revolución, que es lo que estabas
mencionando hace un momento, es el foco principal de Macula. En Macula nos
dedicamos a hacer videojuegos documentales que estén íntimamente ligados con la
cultura y con la historia de México. Específicamente, ahora tienen un proyecto,
¿no? Como en los años años mil novecientos veintiuno, me acuerdo, no, me acuerdo
ya fecha. Sí, es correcto. Justo sí, justo se llama así, es México mil
novecientos veintiuno, un sueño profundo, ese es el nombre completo y justamente
en este momento es el único juego que tenemos en desarrollo, pero tenemos por
ahí un minijuego que hicimos antes de la mano de Lina Talk show mexicanceğ.
Entre más yo me heamaz de la historia, y es un juego en el que juegas como una
revolución cultural, como una especie de archivo. Entonces no solamente es
entretenimiento, sino pues queremos que fuera una fuente o incluso una forma de
consulta. Es una discusión sobre la historia y es un juego en el que juegas como
Juan Aguirre, que es un fotoperiodista de esa época que poco a poco se va
involucrando. ¿Qué es lo que juega? ¿Lo considerarían un juego educativo o solo
tiene el, ahora sí que la intención de, de, ahora sí que es la primera intención
entretener o el educar al público que juegue en México mil novecientos
veintiuno. Yo creo que eso es una respuesta como un poquito compleja, porque en
realidad no está pensado con el fin específico de entretener ni con el fin
específico de educar al público. Entonces, yo creo que es una comparación de
estos conceptos, no como si solamente pudieras entretenerte o solamente pudieras
educar. Quiero que lo pensé en macula, lo pensamos un poco distinto en macula
creemos que la mejor manera de aprender es haciendo y divirtiéndote. Entonces,
ahí sí la respuesta es los dos, no la intención de este juego es ambos más que
educar como tal, pues es otraingo no sólo es helmetspewl allergies, entonces
pero como un teatro, como adds vídeo, con un el con un При Mostur nagel. No por
supuesto cuando daba sale ante datos terminamos de la época. Sino más pres poder
visitar y ir con otros ojos, la idea es por el当 free nah constitutes Hos Hok.
Por qué합as down, esa época lo que buscamos otra vez este juego es la
sensibilizaciones Wallace después de historia no el entenderla y poder en cierta
forma más bien el aprovechar el medio de los videojuegos para llevar este estos
mensajes esta parte de historia poco conocida más. Exactamente, por eso te digo
que a mí no me gusta tanto la idea de pensar el videojuego como un producto que
o sólo entretene o sólo educa. Tratamos de hacer una experiencia que contenga
ambos. Devisando un poquito al tema de Mácula, ¿qué dificultades encontraron
iniciando este estudio en México? ¡Uy! Un montón, depende de qué área me quieras
preguntar, pero en todos lados encontramos retos. Algunos pudimos resolverlos y
otros tuvimos que darles la vuelta, de manera creativa. Pero sin duda alguna
creo que el principal fue la falta de confianza del propio gremio. Pues hacer
videojuegos en México no es tan fácil, entonces creo que se fue principalmente o
el reto que primero nos encontramos, la falta o la duda se podrá hacer, se puede
hacer, lo van a hacer. Claro, porque el medio de los videojuegos, el medio
ahorita ya genera muchísimo más dinero que el cine, pero en México sigue siendo
un mercado totalmente nuevo. Y es, yo creo que en México debe ser muchísimo más
difícil porque la gente se puede creer así como de, es que, ¿cómo que es en
videojuegos en México? Vete a Estados Unidos o vete a Japón. Sí, que
precisamente es un momento muy interesante y hasta diría yo ideal para
desarrollar juegos en México, porque precisamente tenemos grandes juegos que ya
dieron de ese paso y que ya pudieron abrir la industria para todos nosotros.
Tenemos la infraestructura suficiente, también ya llevamos unos años en que el
desarrollo de videojuegos ha empezado a profesionalizar. Entonces creo que es
precisamente este momento donde justo al enfrentarnos con estas limitantes tanto
del mercado como de la preferencialización, la industria, pues es cuando se
presta mucho a experimentar y tratar cosas nuevas que no se hacen pues ya en
mercados mucho más establecidos o en industrias ya gigantes, sino que a partir
de nuestras propias limitantes y estos obstáculos, pues podemos ponernos
creativos y buscar nuevas soluciones. Y al final eso es lo bueno de este
proyecto, es llevar una parte de historia que no solo en México no se conoce,
sino tal vez alguien puede interesarse alrededor del mundo, o sea no sabemos
cuántas personas en otros países puedan ver este juego y descubrir una parte de
este país que no se conoce mundialmente. Exactamente, eso también ha sido como
muy satisfactorio y la verdad muy bonito como estudio, porque ahora hemos estado
participando en convocatorias extranjeras y hemos recibido un par de selecciones
importantes en Londres o en Berlín y justamente es lo que mencionas, ¿no? te das
cuenta que nuestra historia a lo mejor nosotros mismos decimos, bueno nos va a
interesar a nosotros los mexicanos, pero a quién más, ¿no? y resulta que sí
también les interesa mucho, ¿no? Entonces creo que lo que también queremos hacer
acá es que nos demos cuenta de lo interesante y cautivante y realmente
apasionante que es nuestra propia historia. ¿Me puedes contar un poquito más de
cómo ha sido esa aceptación y si yo no esa respuesta en otros países? Claro,
pues bueno nosotros aplicamos para diferentes convocatorias todo el tiempo, ¿no?
y últimamente, bueno las últimas dos que recibimos fueron en el London Games
Festival y en el Amaze Festival en Berlín. Son dos festivales que celebran los
videojuegos de una manera un poco más enfocada como sea el negocio y el otro es
un poco más como sea los medios experimentales o interactivos, ¿no? Entonces
pues ha sido muy lindo ver que justamente algo que a nosotros nos apasiona y que
nos parece como un momento muy particular y muy específico en la historia de
nuestro país despierta interés en otros lados, ¿no? O sea, por ejemplo, el
aproximarnos a nuestra propia historia y al hecho del asesinato de Álvaro
Obregón como un misterio que a pesar de que existe la versión oficial de lo que
pasó, nosotros como mexicanos conocemos que hay como pues esta conspiración
alrededor, ¿no? Entonces el aproximarnos de esa manera a nuestra propia historia
ha resultado muy interesante, no solo para nosotros evidentemente sino para toda
la gente que se quiere aproximar a la historia de México, ¿no? Que dicho, ha
sido una, perdón, una experiencia muy grata ver como también este momento
particular que pues, podríamos decirlo que es historia reciente que al final
cien años puede ser algo como mucho, pero es prácticamente ayerão pues empezar o
abrirte esa puerta de curiosidad. Ahorita que tocaron el tema del tema del
juego, que es el durante el misterio del asesinato del presidente Álvaro
Obregón. Me pueden contar un poquito más de cómo de qué se trata el juego. Yo
platicamos en el jefe, pero como digo, fue algo muy rápido, muy fugaz de que el
juego es como de misterio, de cómo va. Cuénteme. Bueno, pues nosotros lo
denominamos como una aventura narrativa, no? Sin embargo, si juegas como Juan
Aguirre, que es un fotorreportero, no es un periodista que tiene que investigar
el complot y tratar de meterse y comprender la situación política y social que
está viviendo el país en ese momento, no? Entonces, si nos aproximamos en el
formato de misterio a resolver el problema, porque justamente es un poco lo que
hace el periodista. No lo que buscamos a través de las mecánicas del juego es
precisamente eso. Poder materializar todos los aceres periodísticos en mecánicas
que le puedan servir al jugador para seguir avanzando dentro de la historia. No?
Entonces es básicamente una aventura narrativa que habla sobre la post
revolución en México. Juegas como Juan Aguirre, que es un fotorreportero que va
a tener que viajar en diferentes escenarios e diferentes años en el Isto de
México para tratar de resolver y en plan nivel asesinato de Álvaro Breguero.
Igual Pedro, por favor. Sí, pues precisamente mucho en la labor en la que
estamos haciendo el diseño del juego era plantearnos esta pregunta como qué es
un reportero o qué te hace sentir un reportero? Y precisamente de ahí vienen las
formas en las que tú puedes interactuar con este mundo que hemos construido, que
ya sea con la fotografía, entrevistando gente, inspeccionando objetos, incluso
este superpoder muy mexicano que es ser metiche. Todo el mundo es de superpoder
este sin ganas de hacer una comparativa así como son esto, pero con la post
revolución más como un para que la gente en casa se den una idea de cómo es más
o menos el juego en términos de gameplay. A qué se parece? Cuáles serían sus
inspiraciones? Bueno, pues la verdad es que ha habido muchas referencias. Hemos
hemos usado un montón de otros juegos que nos han despertado tanto el interés
como gamers y como diseñadores de juego. Pero yo diría que los principales son,
por ejemplo, what remains of the Finch, Colbert de Si, Firewatch, y el de Agatha
Christie siempre se me olvida el nombre Pedro, por favor. Ayuda a David C. David
C. Esa es como a ver si exacto. Pero sí, creo que es difícil ponerle un pin a
uno porque pues hemos tomado inspiración de muchos, muchos lados. Qué otro que
otros se lo podría estar olvidando, Pedro? No sé. Pues, por ejemplo, en la
propia aproximación, no sé, en la gráfica como en Disware of Mind, donde pues
sí, o sea, no solo tomamos un cómic, pero si no tomamos la gráfica y visualmente
la identidad de el moralismo de la época y todo el arte que se estaba
desarrollando. También está el de 1975, que pues precisamente juegas en una
guerra pero de una forma bastante pasiva y como reportero. Entonces, pues sí,
como dice Paola, es precisamente pues veíamos cosas que nos gustaban de juegos,
aproximaciones y pues índole armando poco a poco para hacer algo nuestro y que
tenga también nuestro propio sello. Y recuerdo que en la demo que jugué en la
EGS tenía un apartado visual bastante interesante que, como dicen, se inspiraba
en este en los en las pinturas de David Alfaro Siqueiros. Alfaro Siqueiros, sí,
cierto, que este y me pareció muy interesante y sobre todo que también tenía
como este aire, y si esto no por comparar, sino para que la gente se denigre,
como de Ellen O'Hare, de Rockstar, que era muy interesante y no quiero ser como
para que la gente se quede con la idea de que es eso, sino que también pues es
como poner el propio giro mexicano al género de misterio. Pero justo te iba a
decir que en el último showcase que tuvimos el fin de semana pasado, estuvimos
en el Latin American Game Showcase y pues es bien padre ver luego la gente que
comenta en el chat, ¿no? Y justamente la mayoría decía, me encanta que hayan
abordado el historia de México de este lado del misterio, ¿no? O de como de film
noir o del cine negro, ¿no? Entonces sí, creo que ha sido un acierto esa
aproximación. Retomando el punto que te hablábamos sobre el panorama en el
estudio de videojuegos en México, ¿cómo ven el futuro? Pedro, tú mencionabas que
ahorita estamos en buena época para empezar a ver este panorama de videojuegos
en México, pero ¿cómo ves el futuro en el país? Sí. Pregunta difícil. Creo que
como bien decía, sí estamos en este momento de que comenzamos a tener más una
cultura de consumir juegos independientes, que es algo que hace unos pocos años
no pasaba mucho, que precisamente consumimos mucho los grandes juegos, los
grandes títulos, los grandes estudios, pero poco a poco empezamos a tener esta
sensibilización de que hay pequeños juegos que tienen propuestas muy
interesantes, que mantienen que ser así el juego de videojuegos. El juego de 134
horas para ser valioso. Y también esta misma idea de que todo el mundo puede
hacer juegos. O sea, hay mucha gente por ahí haciendo juegos y si alguien le
interesa se puede meter, entonces creo que el futuro en México va mucho en poner
el ojo sobre los juegos independientes, que le creo que a la vez esto también va
a retribuir en que más gente va a querer hacer juegos aquí. Y al fin, eso al
final habrá de interés que va a haber en México de continuar con el desarrollo
de videojuegos. Sí, sobre todo ahí creo que algo que me gustaría agregar que
también me parece importante sobre el futuro o el panorama de la industria de
videojuegos en México es que también como desarrolladores creo que o bueno, como
muchos artistas, los cineastas o artistas plásticos, tenemos como la tarea un
poco de empezar a educar también al público, porque como dijiste al inicio del
entrevista, en México se juegan videojuegos y se juegan muchísimo. Pero, pues
qué videojuegos se juegan, ¿no? O sea, ¿qué es lo que estamos consumiendo?
Entonces, justamente creo que, como dice Pedro, es un gran momento para la
industria en el sentido de que tenemos muchas puertas y ya tenemos muchas
plataformas, no en donde pararnos para poder desarrollar cosas más atrevidas,
cosas más complejas, cosas con una idea conceptual más grande y robusta, ¿no?
Pero justamente como desarrolladores creo que tenemos la tarea justo de educar a
nuestro público y de demostrarles que estos otros juegos también son muy
valiosos, ¿no? La verdad quiero felicitarlos porque con la plática que hemos
tenido, aún con sus dificultades técnicas, pero con la plática que hemos tenido,
he notado que tienen una visión de los videojuegos, pues no tanto como un
producto, sino como ya un medio, y eso me parece súper valioso en un equipo de
desarrollo, que es una idea un poco moderna porque aún todavía se siguen viendo
los videojuegos como producto y digo, tiene sus ventajas, tiene sus desventajas,
pero el ver que estudios nuevos ya están viendo esto como un medio, la verdad
los felicito enormemente, les agradezco que me hayan permitido estos minutos de
su tiempo donde podemos encontrarlos para saber más del proyecto, para que la
gente sepa más y pueda ir ver sobre el proyecto de México 1921. Claro, pues
estamos en todas las redes sociales como Mácula, GionBajo, MX, ahí nos pueden
encontrar en Twitter, en Instagram, en TikTok, en Facebook, exactamente igual,
Mácula GionBajo MX, igual tenemos el juego México 1921, un sueño profundo
disponible ya para Wishlist en Steam, se nos pueden añadir a su lista de deseos,
el juego todavía está en desarrollo, entonces todavía no está disponible para
descargan y para compra, pero nos pueden añadir a su lista de deseados ahí en
Steam, y eso nos ayuda un montón. No, pues muchas gracias chicos, no más rápido
ya como para insight de la audiencia, ahorita en qué proceso están en alfa,
beta, todavía no, nada jugable, solo un demo. Ahorita justamente yo la siguiente
semana voy a estar en GDC, en la Game Diversity Conference y me voy a llevar un
showcase demo para allá, y para mayo vamos a estar por ahí en un eventillo que
todavía no les puedo decir bien en donde ni cuándo, pero en mayo vamos a tener
por ahí ya disponible algo para jugar. Chidísimo, les agradezco a ambos por su
tiempo, también les felicito mucho, les deseo mucha suerte con este proyecto,
que se oye súper interesante, sobre todo viniendo de aquí, de este país, luego
ya también les comparto ahí una idea que yo tengo para un videojuego, pero que
nadie me quiere comprar, ya le mandé miles de remakes a Ubisoft y siempre ya me
bloquearon, ya le mandé Nintendo, no me quieren escuchar, entonces pues ya que
los tengo atrapados, digo, entrevistados, puedo contarles de mi idea, pero les
agradezco mucho por su tiempo y pues gracias, les agradezco mucho. Gracias a ti
Mariano, gracias por el espacio y por la invitación. Muchas gracias Mariano, un
gusto. Nivo, Nivo, Nivo, dos, dos, punto cero. Nivo, Nivo, Nivo, dos, dos, punto
cero. Gracias amigas y amigos por escucharnos y acompañarnos esta tarde,
recuerde que cada martes le traigo lo más interesante del mundo de la tecnología
videojuegos y mundo geek aquí en Mexicanse Radio y pues espérense para la
próxima entrega, pues traemos una sorpresa para todos los fans del doblaje en
nuestra cobertura especial de la Molle Convention. Quiero antes de despedirme
agradecer a todo el equipo que permite que este programa llegue a ustedes,
agradecer a José Martínez en los controles técnicos y a Panchito en la
continuidad y a todo el equipo en cabina. Yo soy Mariano Arramínez, recuerden
que puedes seguirme a través de Twitter como arroba el mar 89 público de muchas
cosas desde videojuegos, memes, lo que se me va a ocurrir alrededor del día y
como probada del siguiente programa en nuestra sección de saludos, mis invitados
de la próxima semana es dejan algo muy especial que ojalá lo disfruten. Hasta
luego. A todos ustedes, amigos radio escuchas de Mexicanse Radio, yo, su amigo
Gabriel Chávez, más conocido como el señor Vance, les mando un saludo muy
afectuoso deseándoles que tengan ustedes un día excelente. Hombre, gracias a ti,
mi amigo. Mis amigos de Radio Mexiquense, cuídense mucho, les mando un fuerte
abrazo. No olviden ir al gimnasio. Cuídense. Les habla Grid. Amigos de
Mexiquense Radio, pues Harry Potter les manda un lumus máxima para que siempre
haya mucha luz en sus vidas. Y La Pulga, pues les manda mucha felicidad,
carnales. Y les dice que La Pulga dice que cuando nos veamos, le traigan una
recadona glaseada. La Pulga, le encantan. Y Sasuke Uchiha, no abandonen sus
sueños, sigan entrenando y no sean miedositos como Naruto. Y Rock del campamento
de la Asla, nada más les dice cuídense mucho. Y mi amigo Shinji Kari les dice
que nunca huyan. No deben huir, no deben huir, no deben huir. Hasta pronto,
amigos. Ay, Spider-Man, les dice que recuerden que un gran poder conlleva una
gran responsabilidad. Hasta pronto, su vecino amigable, Spider-Man, alias Peter-
Dos. Hola, ¿cómo están? Un saludo de su amigo Mega-Mente a Mexiquense Radio.
¡Ey, yo Will Smith también quiero mandarles un saludo! ¡Ey, Mexiquense Radio!
¡Saludos! Pssst. Hola, amigos de Mexiquense Radio. Los saluda Thanos. Y les
envío un saludo completamente inevitable. Jajaja. Bueno, pues les mando un
saludo con mucho cariño. Lo mejor de lo mejor. Recuerden que si esto fuera
cuestión de tiempo, el más viejo sería el mejor, pero no es así. Y como dice
Kiman, por el poder de Grayskull. ¡Yo tengo el poder! Bueno, pues ustedes
también lo tienen, así que no lo olviden y aprovechenlo. Quisiera hablarles como
Humberto Simpson, pero no puedo hablarles ni como Humberto Vélez. Así que les
voy a intentar hacer una frase de Homero. ¡Hola, soy Homero Simpson! Es una
especie de Homero Ronco. Para Radio Mexiquense, un abrazo. Claro que sí, amigos
de Mexiquense Radio. Mi nombre es Ani Rojas y les mando muchos, muchos saludos,
muchos besos y muchas gracias por su atención. Cuídense mucho. Gracias. Y un
saludo a toda Radio Mexiquense con todo cariño. Nivo 2.0 de tecnología al
alcance de todos. ¡Mejiquense Radio! ¡Estamos contigo! La radio no solo se
transmite por ondas. Nosotros no somos una aplicación de música con algoritmo.
Llegamos hasta tus oídos a través de ceros y unos que viajan en la red.
Encuentra y escucha todas nuestras frecuencias. A través de Radio y TV
Mexiquense punto MX. Conectate con la música, las voces, las ideas y el sonido
de Mexiquense Radio. Radio y TV Mexiquense punto MX. Después de escuchar estos
relatos, dormirás con las luces encendidas. Historias del más allá. Lunes a
viernes. Diez de la noche. ¡Pero qué más te pasa aquí! Hace cinco años nos
usaron. Nos vendieron honestidad y ni honestidad, ni 10% capacidad. El único 10%
fue el que ella les quitó a los maestros mexiquenses de su sueldo. Para salir de
esta hay que ser valientes y hay que estar unidos. Porque unidos somos más
fuertes. Unidos podemos. ¡Perde! En el Edomex, hoy somos más. Somos más los
valientes que queremos unir a la gente. Los que queremos un estado seguro y
próspero. Porque queremos un futuro mejor para nuestros hijos. Así es, en el
Edomex somos más. Porque somos más los que queremos un cambio para bien. Los que
apoyamos a las mujeres y los que trabajamos unidos para defender lo bueno y
cambiar lo malo. En el Edomex, hoy somos más los que trabajamos unidos para
proteger a nuestras familias. Somos más los valientes. ¡Pri! Es ensayista.
Estudió en la Escuela Nacional de Agricultura de Chapingo, Edmundo Flores
Fernández. Impartió clases en la Escuela Nacional de Agricultura de Chapingo y
en la Escuela Nacional de Economía de la Universidad Nacional Autónoma de
México. Además de ensayista de temas de su especialidad, es autor de un libro de
cuentos llamado Las Moscas, en el que a través de un fino sentido de la
observación evoca el ambiente sencillo de la provincia mexicana. Del municipio
de Toluca, Edmundo Flores Fernández forma parte de nuestras Bellas Artes
Mexiquenses. La programación de este horario es clasificación A. ¿Sabías qué? La
órbita de Venus es un elipse con una excentricidad de menos del 1%, lo que
significa que Venus cuenta con la órbita más circular de todos los planetas que
hay en el sistema solar. El Tribunal Electoral evoluciona para consolidar una
justicia más abierta, incluyente, dialogante, de carrera, eficiente y digital.
Se adapta a las necesidades ciudadanas, a las nuevas dinámicas políticas para
contribuir al desarrollo de la sociedad con total certeza jurídica. Conforma un
sistema de justicia electoral progresiva y plural que ha dado solidez a la
democracia mexicana. Tribunal Electoral del Poder Judicial de la Federación,
justicia que fortalece la voluntad ciudadana. ¿Sabes por qué es valioso que
participes como observador u observadora electoral? Porque tu participación es
un elemento más que brinda certeza, transparencia y confianza a la ciudadanía en
las próximas elecciones del Estado de México. Con tu contribución, nuestra
democracia se fortalece. Presenta tu solicitud para ser observador u observadora
electoral en las oficinas de línea de tu localidad. Infórmate en ine.mx. Tienes
hasta el 7 de mayo del 2023 para inscribirte. Participa. ¿Qué es valioso?
Introducción de podcas. los podcast de mexiquense radio somos la música e ideas
que suenan mientras el mundo se transforma mexiquense radio viviendo ya de tus
mentiras sé que tu cariño no es sincero sé que mientes al besar y mientes al
decirme que me conformo porque sé que pago mi maldad de ayer siempre fui llevada
por la mano es por eso que te quiero tanto más si das a mi vivir la dicha con tu
amor fingido y entro en una eternidad que me hace tu maldad feliz mexiquense
radio siempre fui llevada por la mano es por eso que te quiero tanto más si das
a mi vivir la dicha con tu amor fingido y entro en una eternidad que me hace tu
maldad feliz y que pasa la vida es una mentira y entro en una eternidad que me
hace tu maldad feliz mexiquense radio mexiquense radio sembramos de espinas el
camino cercamos de penas el amor y luego culpamos al destino de nuestro error
validad por tu culpa he perdido un amor vanita que no puedo olvidar vanidad con
las alas doradas yo pensaba reír y hoy me pongo a llorar me seguí la ranque de
mi vida pero yo la volviera a besar vanidad con las alas doradas yo pensaba reír
y hoy me pongo a llorar vanidad me seguí la ranque de mi vida pero yo la
volviera a besar vanidad con las alas doradas yo pensaba reír y hoy me pongo a
llorar llorar vanidad mexiquense radio con tu mirada perdida buscando aquel
firmamento de mundos casi reales emancipando lamento decides liberarte y tus
notas danzan con el viento y atrás tu espíritu lucha por conectarte con el
universo vanidad con las alas doradas vanidad con las alas doradas vanidad si
decidés alejarte y tu aura sigue prendida aquí en mi pecho ilustrando mil
figuras parecen viajar bien y pintando otro universo Imagen en cinema uh ah
¡Me並é un mapaibelress, sobre la que todo ha Roughhound! ¡Rub Summit respecto! ¡
CH I J E P L I C A S Q U gonna beat her up to towards the end! Trust me. Rosas
Yo no quiero rosas Yo quiero un amor Que sea como el mar El mar Profundo e
inmenso Que me acompaña donde voy Y me llega a otro lugar Recuerdo cuando
comenzó Hablando tú y yo de amor En el horizonte de este paraíso Que ahora dejó
ir Dentro de mi ebu y el universo Al calor del sol que acaba de morir Busco muy
al fondo de mí Más allá, más allá, más allá Algo que quizás nunca voy a
encontrar Mi corazón inquieto no puede parar Recuerdo cuando comenzó Hablando tú
y yo de amor En el horizonte de este paraíso Que ahora dejó ir Recuerdo cuando
comenzó Hablando tú y yo de amor Recuerdo cuando comenzó Hablando tú y yo de
amor Recuerdo cuando comenzó ¡Mejiquece Radio! ¡Espera! Esperando que llegue la
hora De regresar a mi tierra En el valle de Pubensa me he metido Lejanía que me
tiene entristecido Y en mi pecho floreció una cumbia de la nostalgia Como una
lágrima que se escapa Cumbia de la alma cumbia que madruga Sobre Pubensa con
insistencia Buscando a cauca y me da Tristeza que me da y me da Me da la lejanía
y me da Esa tristeza que me da Estar tan lejos de la tierra mía Y me da Tristeza
que me da y me da Me da la lejanía y me da Esa tristeza que me da Estar tan
lejos de la tierra mía Mi guajira se quedó esperando Que yo regrese algún día
Por dentro yo siento el llamado Que me hace la tierra mía Y un amor que cada
noche me desvela Que se ocupa del momento que me queda Y por dentro floreció una
cumbia de la nostalgia Como una lágrima que se escapa Cumbia de la alma cumbia
que madruga Sobre Pubensa con insistencia Buscando a cauca y me da Tristeza que
me da y me da Me da la lejanía y me da Esa tristeza que me da Estar tan lejos de
la tierra mía Y me da Tristeza que me da y me da Me da la lejanía y me da Esa
tristeza que me da Estar tan lejos de la tierra mía Que estar tan lejos de la
tierra mía Que estar tan lejos de la tierra mía Que estar tan lejos de la tierra
mía Cuando hay sed que no se calma Cuando amarme ya no alcanza Te vas Y cuando
un gesto te derrata Mi mirada te desarma Te vas Cuando hay sed que no se calma
Cuando amarme ya no alcanza Te vas Sin más Cerré puertas y ventanas No tenía
dónde ir Y si entonces tú me llamas No podría ya salir Si este viento no se
cansa Ya no hay brazos que me cubran Si amanezco en los escombros No habrá
puertas ni ventanas No tendría dónde ir Y si entonces tú me llamas No podría ya
salir Sé que la lucha avanza Sé que vendrá conmigo Como un venta válida Como un
venta válida Sé que la lucha avanza Sé que vendrá conmigo Como un venta válida
Como un venta válida XHATL 105.5 Atlacomulco Mexiquense Radio Somos una estación
del sistema mexiquense de medios públicos Mexiquense Radio Mexiquense Radio
Mexiquense Radio Mexiquense Radio Mexiquense Radio Mexiquense Radio Mexiquense
Radio Mexiquense Radio Mexiquense Radio Mexiquense Radio Mexiquense Radio
Mexiquense Radio Mexiquense Radio Mexiquense Radio Mexiquense Radio Mexiquense
Radio Mexiquense Radio Mexiquense Radio Mexiquense Radio Mexiquense Radio
Mexiquense Radio Mexiquense Radio Mexiquense Radio Mexiquense Radio Mexiquense
Radio Mexiquense Radio Mexiquense Radio Mexiquense Radio