TRANSCRIPTION: MEXIQUENSE-RADIO_AUDIO/2023-03-22-01H00M.MP3
--------------------------------------------------------------------------------
 La lista de cosas que han ocurrido en el día es inmensa. Ideas, idas, regresos,
pendientes, encuentros y desencuentros. Pero por fin ha llegado el momento de
parar este tren y tomarnos juntos la tarde libre. La aventura de los sentidos en
la que disfrutamos los placeres de la vida. Tarde libre. Sé que respiro porque
sigo huyendo de todo el que me exige que muera y resucite y si algún pacumbral
me lo repite que se vaya buscando un clavo ardiendo. A veces me va mal y a veces
no. A veces quien se ríe el último soy yo. El día de los difuntos pongo flores
en las tumbas de mis enterradores. Por ti el pastel sin apagar las velas vi al
sol salir en México y ponerse en New York si hago un cameo en tu telenovela. Que
sea en un capítulo de amor. El Lotres judea la revolución. Buenos Aires barnizó
mi corazón. La vida me enseñó a jugar con fuego y a decirme si donde dije Diego.
Contra todo pronóstico aprendí a caer de pie. Contra todo pronóstico por la boca
muere en el pez. Que asegura que soy mi peor enemigo. Que aquí me pillo, aquí me
mato. Que me llevo conmigo como el perro y el gato. Ya he firmado daños a
terceros. Me traje esa medida de príncipes desnudos. Si me abrazas y me usas
como escudo correspondo disparando al mensajero. A veces tengo todo y quiero
más. A veces duermo y no me acuerdo de soñar. Y a veces cicatrizó las heridas
bailando en rock and roll de los suicidas. Contra todo pronóstico guitarra en
bandolera. Contra todo pronóstico vuelvo a la carretera. Hay quien dice que soy
mi peor enemigo. Contra todo pronóstico el pro fighter era un traidor. Contra
todo pronóstico ha ganado el perdedor. Y aquí me pillo, aquí me mato. Pues me
llevo conmigo como el perro y el gato. Contra todo pronóstico aprendí a caer de
pie. Contra todo pronóstico derrapan otra vez los que dicen que soy mi peor
enemigo. El disfrute de los sentidos. Tarde libre. Caramba, o sea hasta la
sonrisa flora cuando escuchamos este tipo de piecesitas. No lo habíamos podido
compartir la semana pasada por ahí de que fue el, ah dios, cuando vió la luz
este asunto, así, así, paciencia cierta. Si no estoy equivocado fue por ahí del
miércoles, jueves, una cosa por el estilo. Y bueno, sí, sí, sí, había que darle
espacio y además digo que entre que se nos venía el asunto del asueto de ayer,
la ingeniería mexicana, jugarista a todo lo que daba y todo lo demás, pues ya no
había manera de que este asunto funcionara así en vivo y toda la cuestión. El
jueves me reportan por acá, pues cuando vió la luz este asunto, la canción que
acabamos de estrenar en tarde libre de compartir por primera ocasión y que narra
las peripecias que suceden contra todo pronóstico. Lo más reciente de mi
compadre Joaquín Ramón Martínez Sabina y con esta canción hemos comenzado la
emisión de hoy de tarde libre. Así más o menos este asunto ha comenzado. Ya se
están, nos estamos diría el otro, saboreando el disco completito contra todo
pronóstico. Ya se ha desvelado la primera de las canciones. La gira ya inició,
ha iniciado, ya se ha hecho presente en América, en el cono sur. Ya ha comenzado
en el caso de España y regresará a América el tal Sabina, para ahí de
septiembre, octubre más o menos, a finales de octubre y principios de noviembre
estará pisando tierras aztecas, por supuesto ante la algarabía mucha gente que
le sigue desde hace tantísimos años y de todos aquellos nobeles corazones rotos
y desangelados que le han ido conociendo a lo largo de las décadas. Así que por
aquí estará el hombre y por supuesto ahí está toda la información relacionada
con este el más reciente de los materiales de Sabina, la más reciente de las
giras, ya lo habíamos comentado, se jactó en su momento y lo declaró a todos los
vientos posibles que él iba a regresar a los escenarios en el momento en que ya
no hubiera mas carillas, así decía el hombre. Cuando ya el público no tuviera
que estar utilizando el dichoso cubrebocas, en ese momento él iba a pensar en
regresar a los escenarios. Así que contra todo pronóstico Joaquín Sabina regresa
a los escenarios de muchas partes de Iberoamérica, bueno hasta va a estar en
Estados Unidos, al menos eso es lo que se me cuenta. El punto es que esto ya
inició justo como la tarde libre y esto nos pone bastante felices en esta
jornada de martes, 21 de marzo de 2023, ojalá si tuvieron el deleite de
regodearse estando cerca de los suyos un día más después de sábado y domingo,
ojalá que se haya disfrutado de forma plena, si no es así porque había
pendientes o si se cansaron de cansarse y ya no hubo manera de ponerle descanso
a ese atroz giro de la vida, pues bueno ya vendrá pronto más antes que después
el fin de semana. Pero así con la voz de Sabina hemos comenzado este asunto
contra todo pronóstico y me da mucho gusto saludarles a nombre de Pepito
Martínez que está en los controles técnicos, a nombre de Panchito Díaz en los
tiempos de cabina, de Orestes Solache Guillén que bueno, o sea algo quiere el
ingrato porque desea congraciarse con todos nosotros sabiendo que la sabinada
nos puede ni nos puede mucho. Entonces él ha programado justo este tema con el
cual hemos empezado, ¿qué música tenemos para hoy? De verdad, yo nada más
adelanto, agárrense y quédense por supuesto porque esto se va a poner muy bueno
y se va a poner muy bueno además por los temas que hoy vamos a tocar, hoy vamos
a platicar acerca de caramba, un asunto que como nos resulta indispensable para
poder entender el hoy por hoy y para podernos entender como seres de nuestro
tiempo. Ya estuvo bueno estarnos haciendo patos de estar dejando de lado cosas
que tienen que ver con nuestro bienestar. De entrada el personal, ya lo habíamos
platicado con la invitada que hoy presentaré a continuación, la última ocasión
que ella nos visitó, decíamos caramba es que necesitamos estar bien primero
nosotros, si mi mundo no está bien lo que pueblo a mi mundo no va a estar bien,
si yo no estoy bien mi mundo no va a estar bien. Y dentro de esto una de las
herramientas que de verdad a veces nos falla porque creemos que somos
bonachones, que debemos ser tolerantes, hay que ser empáticos, hay que ser en
fin muy abiertos, el horno no está para bollos, para andar peleando todo el
tiempo, no hay que hacer la de tos, y jodé, como nos cuesta de pronto trabajo
entender la importancia de poner límites. Así que hoy tengo el gusto de saludar
en la vía remota a mi queridísima amiga Cecilia Martínez ella es psicoterapeuta
y está presente en la tarde libre de hoy. Querida Ceci, ¿cómo estás? Hola,
chato, muy bien, muchas gracias. Oye, qué gusto tenerte por acá y qué gusto
además que nos puedas ayudar a ir dibujando, a ir poniéndole un especie de
esquema a este asunto que de pronto nos cuesta trabajo, ¿no? El tema de los
límites de forma sana por supuesto. Así es, pues ya lo decías bien tú, ¿no? De
estas cosas que se vuelven indispensables en la vida y que yo creo que cada vez
lo tenemos un poquito más claro. La cosa es que a veces la vida nos se encarga
de hacernoslo saber a veces de forma un poco más dramática, ¿no? Pero sí es
cierto, es uno de los elementos más importantes del bienestar en general y por
supuesto de la salud mental, de la salud emocional. El conocer nuestros propios
límites, tenerlos muy claros y entonces expresarlos, ¿no? Que es de pronto ahí
donde a veces puede complicarse un poquito, pero definitivamente es bien
importante aprender a conocer nuestros límites. Por qué es, y por qué es
importante conocerlos y por qué es importante además determinarlos, ¿no? En esta
narrativa cotidiana que tenemos, en esta dinámica en la cual pues de pronto
parece que se nos pasa la vida frente a nuestros ojos mientras estamos ocupados
en mil cosas y ya cuando nos dimos cuenta como si fueran paracaidistas de estos
de grupos políticos de presión, ¿no? Un piecito más para allá y ya llegaron a un
terreno que está desocupado y ya se establecieron y un piecito más y un piecito
y cuando te das cuenta ya están completamente hechos en ese sitio, ya se
establecieron, ya echaron raíces y ahora para quitarlos. Más o menos así
funciona el tema de los límites, ¿no? Absolutamente. Fíjate que es un enfoque
quizá, pues como te decía de los últimos años, que como que la tradición, ¿no?
Dictaba que siempre habría que pensar primero en los otros y ponernos a nosotros
al final. Y hoy este enfoque de bienestar nos ayuda a entender que efectivamente
si nosotros no identificamos esta posibilidad de hasta dónde llego yo y hasta
dónde llegas tú. En ese sentido se empiezan a desdibujar mis espacios
saludables, ¿no? Mis espacios vitales muchas veces, pero otros son espacios
saludables. Y aquí el enfoque yo te diría, podemos tener esta creencia de que
los límites son para las demás personas. Y yo te diría, sí, hay que
comunicárselos o decírselos a las demás personas, pero en realidad los límites
son para nosotros. Es decir, un límite es este terreno o este territorio en
donde tú dejas de estar bien o tu salud mental, tu salud emocional, tu salud
física, tu bienestar, tu paz, digámosle como quieras, se ve afectada. Porque
finalmente las personas, pongámosle el nombre que tú quieras, ¿no? Los demás, el
trabajo, la vida, las relaciones, muchas otras cosas que están allá afuera, pues
finalmente van a tomar lo que nosotros estemos dispuestos a dar, ¿no? Es cuando
nosotros establecemos como decía la la raya o el límite de hasta aquí te permito
o hasta aquí permito que esto suceda, que efectivamente podemos poner en un, yo
digo, como en un lugar muy, pues sí un poco santo diría yo, ¿no? Como de respeto
de hasta aquí te permito que tú pases o que tú entres o que esto suceda o que
suceda de esta manera. Y cuando empezamos a trabajar así es decir, el límite lo
pongo yo y a mí me gusta mucho ponerlo como con una metáfora de piensa un poco
en el lugar donde donde tú vives, ¿no? O sea, tienes pues obviamente un un
espacio que tiene probablemente pues paredes de diferentes materiales, ventanas,
puertas, quizá las ventanas tengan cortinas persianas, quizá tu puerta tenga un
tipo de chapa, una cerradura, qué sé yo. Todo eso son cosas que tú has ido
poniendo porque has delimitado de alguna manera tu terreno, tu casa, tu espacio.
Y te has ido, digamos, pues limitando, delimitando de cierta manera. Entonces,
al final los límites funcionan un poco así, ¿no? Los estableces tú, son para ti,
para sentirte seguro y protegido tú. Y el siguiente paso es comunicárselos a los
demás, ¿no? Ahí es en donde está, digamos, pues el el la clave porque muchas
veces lo que puede sucedernos en esto que nos juega en contra es no se lo digo
porque si le digo que no o le digo que no quiero o le digo que no tengo ganas o
le digo que no me gusta eh va a decir que quema la onda o va a decir que que
irresponsable o va a decir que que grosera o va a decir y entonces nos empieza a
preocupar lo que el otro va a pensar. Y y desdibujamos por completo esa paz, esa
estabilidad, ese bienestar, etcétera, etcétera. Que que por supuesto hemos
aprendido con el paso de los años con terapia, con con tantos textos que se han
hecho al respecto, con tantos especialistas que como tú nos cuentan este tipo de
ideas y las planteando de manera tal que nos queda muy claro el el sentido
relevante que implica tomar en cuenta los demás. No yo suelo decir esta frase de
yo soy los demás de los demás para tomar justo pues una suerte de proporción de
lo que implican mis actos. No si los actos de los demás a mí me impactan cuando
ellos piensan o ellas piensan que son únicos en este mundo, que lo son para
ellos. No mis actos por supuesto tienen ese impacto para los demás considerando
que yo soy único para mí y por las razones que hablábamos la vez pasada. Es
decir, no hay nadie más importante en el mundo que uno mismo para sí mismo
porque si no estás pues tu mundo no existe. Pero además si no estás sano, si no
estás pleno, si no estás pues feliz, obviamente todo tu entorno se va a
contaminar de esa falta de. Ahora aquí me parece fundamental un término que tú
tocas es el tema de la protección que aquí vendría más bien aplicado a la
autoprotección. Cómo soy yo capaz y si soy o no capaz de establecer justo esos
límites para que no se transgredan y para que no a la luz de insistir yo hace un
en el comienzo de la charla de ser buena onda, de ser tolerante, de ser amable,
de llevar la fiesta en paz para que no se entienda que esas ganas de que todo
esté bien no se comprendan erróneamente como este este cuate es de fácil acceso
y es de fácil abordaje para pasar los límites, porque porque no te dice nada.
Entonces de ahí la importancia de esto. Sí, fíjate y creo que eso es bien
importante porque es como si nos hicieramos un paso para atrás y yo te diría
tiene todo que ver con estos términos de los que de pronto también hemos hablado
el autoconcepto, autoestima, porque finalmente es saber de qué soy capaz, qué
quiero, qué necesito, cuándo lo necesito, cuándo no lo necesito y honrar eso
poniéndome como decíamos en un lugar seguro en todos los sentidos. Y creo que
cuando empezamos o aprendemos a honrar eso, a respetar quién soy, porque es como
consecuencia de un proceso de autoconocimiento, como decíamos de saber qué
quiero, hasta dónde, qué sí, qué no, etcétera, empezamos a trabajar mucho en la
prevención. O sea, por ejemplo un tema que hoy nos ha llevado una consecuencia,
que hoy nos ha llevado en general a la sociedad con grandes niveles de born out
de este famoso síndrome del quemado que también hemos platicado es precisamente
la falta de límites. A lo mejor en un entorno laboral, a lo mejor en ciertas
profesiones más o a lo mejor en ciertos tipos de familia más o en ciertas
actividades más, pero la falta de límites es lo que me lleva a eso, a quemarme,
a llegar a un nivel de agotamiento, de estrés, de en muchos casos de depresión,
de hartazgo, de enfermedad, en muchos otros. Entonces ahí es en donde creo que
hay que hacer como una rectificación, en medida que yo sé quién soy, sé lo que
valgo, sé lo importante que soy, me propicio a esos espacios seguros sin que lo
que piensen los demás me lo quite o sin que eso obstruya ese proceso. Ahora, si
es cierto, creo que a lo mejor en donde vale la pena recalcar un poco es hay que
trabajar quizás si tú o alguien que nos escucha, eres una persona que le cuesta
de pronto particularmente establecer un límite, a lo mejor hay que trabajar en
esa creencia de qué van a pensar los otros y luego el tema de cómo lo digo,
porque claro, si es cierto, es importante también el cómo se dice, el cuándo se
dice, la quién se le dice, pero creo que partiendo de esa base puede ser como
por lo menos trabajarlo en un nivel un poquito más consciente y entender que
poner límites no significa ser egoísta, no significa no pensar en los demás,
significa que estás conectado con quien eres tú o conectada con quien eres tú y
entonces te va a ser un poquito más sencillo pues ponerte en el lugar que
necesitas estar siempre. Vaya que sí y claro, justo lo que se impone es en este
caso pues tratar de conocer esas estrategias con las cuales poder entrar y salir
de este juego, lo digo con toda la seriedad del mundo por supuesto, pero es un
juego porque es esta dinámica de te doy y me das, no? Este toma y daca de hasta
qué punto permito que transijas mis límites, sí? En pos justamente puede ser de
esta convivencia sana y de mil cosas más, no? Porque porque se traducen en
dinámicas con los con los hijos o las hijas, con la pareja, no? Con los amigos,
con el tema de laboral, no? Porque también es entendible, oye, pues tampoco me
voy a poner al brinco y voy a estar demandando permanentemente que no se violen
mis derechos, que lo ideal es eso, por supuesto, no? Es decir, ante cualquier
amenaza de violación de derechos, de límites, pues sí hay que poner un alto,
pero también viene el riesgo de, oye, pero es que si hago esto, pues corro el
riesgo de que digan, tú eres prescindible, no? Para quien no se queje, quien
cobre menos y quien trabaje más, no? Entonces también es ese riesgo. Cómo le
hacemos así para ir trabajando en esta estrategia? Sí, me encanta llegar a este
punto porque aquí es en donde está, pues digamos, la parte medular, no? El cómo
y yo te diría y justo el otro día un paciente me lo decía, es que no puede ser
que deje de ser flexible o no puede ser que deje de dar o de estar para las
personas que quiero. Y creo que es bien cierto, pero yo creo que la vida
finalmente se trata justamente de encontrar esa flexibilidad y ahí la
recomendación sería buscar un tipo semáforo, digamos, en donde hay elementos que
son límites que están en luz roja, o sea, que no hay manera de que tú negocies y
que si es no, es no, independientemente de quién sea, a qué hora sea, por qué
sea, siempre será no, hay otros que están como en la luz amarilla, como en la
preventiva, no? O sea, están empezando a generar por ahí algún tema, alguna
incomodidad, empiezan a generar ciertas cosas, pero es algo que puedes todavía
seguir manejando, hay que hablarlo, pero es algo que puedes negociar, no? Y
habrá otras situaciones, personas, eventos, etcétera, que estén, digamos, con
una luz verde, es decir, son deseables, es algo que siempre te vendría mejor o
te haría sentir mejor, pero que si no se llega a respetar ese límite, ahí es en
donde está tu margen de flexibilidad, es decir, aquellos en donde tu salud
mental, tu salud física, tu bienestar no está en riesgo y entonces ahí puedes
moverte en esa luz verde, pues insisto, con mayor flexibilidad. Pero en cuáles
son, esas en los que a veces nos cuesta más trabajar, pues hacen esos que son de
luz roja, no? Porque hay que ser absolutamente tajantes y una vez que los
identificamos, o sea, quizá incluso yo el ejercicio al que les invitaría a hacer
ahora es hasta identificarlos en estos términos cuáles están en luz roja, cuáles
están en luz amarilla y cuáles en luz verde y sobre eso empezar a trabajar. Si
identifico uno, dos o tres en luz roja, ok, el siguiente paso es cómo lo voy a
comunicar a esta persona o este grupo de personas o en mi trabajo o en esta
relación en particular y entonces trabajar en cómo decirlo. Pero ya sabes que
eso es no negociable, no? Porque está en juego tu salud emocional o tu
bienestar. Claro, sí, sí, sí. Me parece tremendamente elocuente, no? Esto y
útil, por supuesto, el poderle colocar justo estas etiquetas para darnos una
idea. Claro, aquí es imprescindible el ejercicio de autocrítica y también una
honestidad brutal para no estarnos haciendo todos a nosotros mismos no? Para
poder entender que realmente hay rojos, no hay amarillos o hay verdes, no? Y
para no estar por ahí tratando de autocomplacernos o auto, más bien engañarnos
en términos de ay, pues que tanto es tantito. Ay, pues es que es que es mi hija,
no es mi hijo o es mi pareja o es que es mi trabajo. Claro, claro, insisto. Si
sabemos la situación, cómo está, sabemos que tampoco es tan tan fácil este
asunto de le brinco de aquí a otro trabajo sin problema, no? Pero tienes toda la
razón. En qué momento se está poniendo en peligro la integridad y no física
necesariamente, aunque también hablabas del burnout hace un momento y cuántas
cosas se han derivado y ocurrió en la época pandémica de estar disponible todo
el tiempo y que además las empresas pensarán, no todas, pero hubo muchos casos
de gente que desde las empresas pensó que bueno, pues es estar en casa haciendo
estar haciendo como fisco estar en el hogar implica que están localizables todo
el tiempo a la hora que sea. Y yo supe de casos por ahí algún amigo que decía,
bueno, es que en el momento en que empezamos a trabajar las ocho de la mañana,
siete de la mañana, se asumía que si estaban localizables y la última reunión no
podía ocurrir antes de las diez de la noche. Entonces decías tú caramba. Y sí,
ya sabes, bien conscientes, por ahí daban un margencito para comer de una hora y
deje. Ay, mira qué conscientes, no vaya a ser la de malas, no? Pero pero de
verdad, o sea, esta idea de ser honestos nos lleva también de la mano con esta
estructura que tú nos propones. Sí, sí, creo que identificar lo es el primer
paso muy importante y otro tema que tú decías el proceso que implica
autoconocernos, pero también identificar esta creencia que a veces puede estar
por ahí de la necesidad de complacer a los otros o el temor de qué va a pasar o
qué voy a perder si digo que no o si establezco cierto líneas. Empezar a ser
conscientes de eso, creo que podría ser un buen inicio. Ya lo creo que sí.
César, qué importante tocar este tema, qué importante que nos puedas brindar por
ahí herramientas para dar cuenta de ello, para saber si si lo estamos viviendo y
si es así, qué podemos hacer al respecto y entender que que la vida nos está
yendo en ello. No, no quiero ser catastrofista, pero los grandes males que
requieren grandes remedios en su momento fueron solamente asuntos para tratarse
de forma muy breve, muy insípida, muy insignificante, quizá. Pero al principio
es mucho más fácil ir poniendo ese alto antes de que se convierta en una gran
bola de nieve y más tarde resulta más complicado porque además va a ser como más
más reactivo que preventivo. Sí, totalmente, totalmente. Más vale trabajar
cuando son verdes a cuando ya estamos en rojo. Exactamente. Sí, porque híjole,
es más complicado. Y la otra, pues la gente ya se acostumbró. También es eso.
Entonces, llegado el momento, si uno no repela ni dice nada o no argumenta nada,
llegado el momento es que nunca dijiste nada y de repente truenas, de repente
explotas. Pues es que uno no sabe lo que hay dentro de cada cabeza. Así es,
totalmente de acuerdo. Ay, sí, sí, pues muchas gracias. Y digo, me queda así
como que híjole, hay mucho por trabajar, siempre mucho por trabajar, como si no
estuviéramos ya permanentemente en proceso de burnout, de burning out, no? Pero
pues hay que hacerlo porque insisto, la vida se nos puede ir en ella. A poco no?
Sí, y todos los días, todos los días. Creo que trabajar y nosotros es una cosa
de todos los días. Somos, somos pijos en construcción y creo que es válido ir
profundizando y conocernos un poquito más. Ya lo creo que sí. Querida Ceci,
muchas gracias de verdad por estos minutos que nos has regalado. Recuérdanos,
por favor, dónde encontrarte. Claro que sí, estoy en todas las redes sociales
como Anasensimx, Lampajosicoterapeuta y bueno, pues ahí muy pendiente con cosas
en el podcast también, así que pues muy, muy pendiente por ahí en redes
sociales. Eso, Mero Ceci, te mando un fuerte abrazo. Muchas gracias. Gracias a
ti, Charlie, bonita tarde. Igualmente gracias, Ceci Martínez, psicoterapeuta y
platicándonos de la importancia que tiene poner límites. Y nosotros por lo
pronto ponemos un límite a este programa por el momento. ¿Por qué? Porque nos
vamos a la pausa, pero la pausa también tiene un límite. Sí, hoy un poquito más
largo porque el corte está un poquito choncho, pero después del límite
regresamos porque hay que ponerle un límite a la pausa y vamos con la música y
luego de la pausa hay otro límite porque tenemos una cápsula y así nos podemos
ir. Entonces, de límite en límite, mientras no sea límite, todo está bien en la
música. Así que vamos y volvemos aquí a Tarde Libre. La paternidad positiva o
crianza positiva es un método de educación que busca erradicar la violencia como
método de enseñanza, provocando encuentros de diálogo cara a cara para comunicar
una enseñanza o un principio que deseas que incorpore. El Sistema Nacional de
Protección para Niños, Niñas y Adolescentes propone la implementación de la
crianza positiva como método para utilizar prácticas respetuosas de su dignidad.
Para más información consulta www.gov.mx Diagonal Cipila. México es el radio. Ir
para delante es lo que nos une en el pan. Sabemos cómo hacerlo con buenos
gobiernos con visión humanista en el pan. Pensamos en el progreso y el futuro,
inspirados en lo que nos motiva a todas y todos. El bien de nuestras familias en
donde cada día sea una oportunidad para construir, para avanzar y ser felices en
nuestro estado de México con el pan siempre hacia adelante. Pan, acción por
Domex. La palabra mecanografía proviene del griego y se forma con los vocablos
miyani, máquina, graféin, escribir, grabar, más el sufijo ia que se usa para
crear sustantivos abstractos que expresan una relación al excema primario, por
lo que cuando usamos la expresión mecanografía lo hacemos para referirnos a la
técnica que se usa para escribir en máquina rápida y correctamente empleando
todos los dedos de ambas manos. Una razón, una palabra. ¿Cómo ves Pepe? Ayer fue
una entrevista de trabajo y me dijeron que un mes aprueba y siempre se dé
sueldo. Que mal caray, siempre es lo mismo, o te piden años de experiencia o
pésima paga de prestaciones mejor ni hablamos. Sí, me choca eso. Llevo un año
sin trabajar y no tengo por el súper. Las cosas tienen que cambiar. Hoy más que
nunca los jóvenes tenemos que demostrar de qué estamos hechos. Es tiempo del
verde. Partido Verde, Estado de México. La programación de este horario es
clasificación A. En este periodo electoral, la Fiscalía General de Justicia del
Estado de México, a través de la Fiscalía Especializada en materia de delitos
electorales, te brinda atención en caso de ser testigo o víctima de un delito
electoral. Ofrecerte dinero a cambio de tu voto, recoger tu credencial para
votar, condicionar tu voto mediante cualquier presión o amenaza. Son solo
algunos de ellos. Infórmate. Cualquier anomalía, repórtalas al 0187 20 28 770 en
nuestras redes sociales o en la aplicación móvil FGJDOMEX disponible de manera
gratuita. Mexiquece Radio. ¿Dónde la dejé? Justo ahora que hay elecciones. ¿Y
que como la mayoría decidí salir a votar? ¿Dónde la dejé? ¿Mamá? Ay, hija.
Extravié mi INE y no sé qué hacer. Tranquila. Aún estás a tiempo de sacar una re
impresión idéntica. Es muy sencillo. Puedes hacerlo hasta el 19 de mayo. Estaba
tan preocupada. ¿Mamá? Voy con mi re impresión y el 4 de junio salgo a votar.
INE es valioso porque nos identifica y nos une. P.T. es la 4T. Nosotros se les
recibe con cariño en un ambiente lleno de emoción, cohetes, música y copal. Los
peregrinos descienden de los modernos autobuses, se les da la bienvenida con
abrazos de sus morenos rostros, brotan lágrimas. Un solo rostro en una sola
llama que ilumina a los espíritus y enciende la más profunda de las emociones.
Mexiquece Radio. Fortaleciendo y enriqueciendo nuestra cultura. El libre, la
aventura de los sentidos. El libre, la aventura de los sentidos. El libre, la
aventura de los sentidos. El libre, la aventura de los sentidos. El libre, la
aventura de los sentidos. Cuestiones de carácter emocional, de salud, mental. De
pronto se hace indispensable acudir al encuentro con distintos conceptos que
pueden primero ayudarnos a nombrar las cosas que pasan. Bajo la lógica de que lo
que no se dice no existe, aunque exista, es importante referirlo. Y por supuesto
es importante acudir a los especialistas, es importante enunciarlo, no hincarle
el diente a esa narrativa que representa estos temas que ya se están
mencionando, se están definiendo, se están nombrando y uno de ellos tiene que
ver con el título de la canción que recién ha finalizado. Escuchamos parte de la
música nobísima que se da por aquí, por allá y por acuya y por supuesto lo hemos
hecho de manera muy fresca con Millie Tomee, es Tomee, como Marisa Tomee, Millie
Tomee o Tomee según se quiere decir, esta mujer se hace llamar Girlie, así como
Chiquitina, más o menos así. Ella es británica, es londinense y tiene a bien
darle vida a un material que lleva el nombre justo de la canción que acabamos de
escuchar. Se llama Impostor Syndrome o el síndrome de la impostora o del
impostor, que por supuesto hoy ya sabemos que tiene que ver con esta idea, en
muchos casos errónea, en otros tantos no, pero finalmente es cierta en tanto se
está percibiendo de alguien que no se siente completamente capacitado o con las
credenciales necesarias para llevar a cabo una determinada actividad. El
síndrome del impostor no es tanto que alguien se asuma como tal sin serlo, que
también sabemos que hay gente que se las da, pero de verdad para adoptar esta
actitud y este comportamiento. En un país como el nuestro hay casos lamentables
y muy notorios y recientes, además de gente que es impostora, porque ha hecho
trampa, porque no se tenga las credenciales necesarias en efecto y aún así se
jactan y de aquí no me muevo y no se mueve. Pero más bien se refiere a esta idea
al síndrome del impostor que asume que no está a la altura de lo que se
requiere, que nunca va a ser un esfuerzo lo suficientemente fuerte, rico en
calidad, en cantidad, en tiempo, en todo lo que se debe tomar en cuenta para
valorar el trabajo que se hace. Entonces ahí está otra canción más de la tarde
libre y yo adelante al principio de este programa que estaríamos compartiendo
muy buenos sonidos. Como la canción que viene después de esta pincelita a la
cual nos vamos y que nos habla en una reflexión bastante airada de lo que
implica que estemos sobrepoblando este planeta. Y joder, los chamacos en pocas
palabras nos hacen ver muchas cosas que nos permiten justo ir identificando de
qué va esta cuestión, de irnos pensando en la realidad, de ir analizándola desde
distintas perspectivas como lo hemos hecho con otros sitios más. Bueno, los de
en pocas palabras titulan esta pieza sobre población, la explosión demográfica
humana explicada. Uno puede meterse en internet y si solamente tecleamos
población mundial actual, aparece una cifra brutal. En este momento, mientras yo
hago este comentario, han llegado a este planeta 30 personas. Mientras hice este
comentario, 40, 50. 8.023.219.833 personas en este planeta. Lo cual ha
significado que hasta las 6.42 de la tarde, que es este momento, han nacido
286.235 seres humanos. Y mientras estos segundos han pasado, ya se sumaron 20
más. Es increíble. Por 143.365 personas que han muerto hoy. Y en este lapso que
guarda silencio, murieron 5 personas más. Entonces, claro, mientras se sigan
haciendo más de lo que se muere, el superávit o el exceso de población en el
mundo, pues va a dar de sí. De eso y mucho más habla esta pieza a la que vamos a
continuación. Insisto, se titula Sobre Población, la Explosión Demográfica
Humana Explicada. Escuchemos esto. En toda nuestra historia, nunca ha habido
tanta gente en la Tierra. Las cifras se han disparado y hemos pasado de 1.000
millones en 1800, 2.300 millones en 1940 y 3.700 millones en 1970 a 7.800
millones en 2020. Si la población mundial se ha cuadruplicado en el último
siglo, ¿qué pasará el próximo? ¿Y qué significa ese crecimiento para nuestro
futuro? Habrá migraciones en masa, continentes cubiertos de suburbios
superpoblados en megaciudades, enfermedades y contaminación, caos y violencia
por la energía, el agua y la comida? ¿Y una humanidad pendiente solo de
sobrevivir? ¿El crecimiento de la población destruirá nuestro estilo de vida? ¿O
se trata de un pronóstico tremendista sin base alguna? En la década de 1960, la
tasa de crecimiento alcanzó una cifra récord que provocó profecías
apocalípticas. Los pobres procrearían sin medida y se apoderarían del mundo
desarrollado. Y así nació la leyenda de la sobrepoblación. Pero resulta que las
tasas de natalidad alta y las explosiones demográficas no son características
permanentes de algunas culturas o países, sino más bien parte de un proceso de
cuatro etapas por el que atraviesa todo el mundo, la transición demográfica. La
mayoría de los países desarrollados ya la han pasado, mientras que otros países
lo hacen en este momento. Volvamos al siglo XVIII, cuando todo el mundo,
incluida Europa, estaba en la primera etapa de la transición demográfica. Para
los estándares actuales, Europa estaba mucho peor que una región en desarrollo y
soportaba malas condiciones higiénicas, alimentarias y médicas. Nacía mucha
gente, pero muchos morían igual de rápido, así que la población apenas crecía.
Las mujeres tenían entre cuatro y seis hijos, pero solo dos de ellos llegaban a
adultos. Después en el Reino Unido se produjo la Revolución Industrial y con
ella el mayor cambio en las condiciones de vida desde la Revolución Agrícola.
Las personas pasaron de campesinos a trabajadores. Los productos manufacturados
se fabricaban en masa y eran asequibles. Las ciencias florecieron y lograron
avances en el transporte, las comunicaciones y la medicina. El papel de la mujer
en la sociedad cambió y se crearon las condiciones para su emancipación.
Lentamente, además de formarse una clase media, el progreso económico aumentó
los estándares de vida y atención sanitaria de los trabajadores pobres y comenzó
la segunda etapa de la transición. Con una alimentación, higiene y medicina
mejores, la gente ya no se moría a cada rato, especialmente cuando eran muy
jóvenes. El resultado fue una explosión demográfica con la que el Reino Unido
dobló su población entre 1750 y 1850. El motivo principal por el que las
familias tenían muchos hijos era que pocos sobrevivían normalmente. Una vez que
eso cambió, se inició la tercera etapa de la transición, se dejaron de concebir
tantos bebés y el crecimiento demográfico se ralentizó. Al final llegó el
equilibrio. Morían menos personas y nacían menos niños, de modo que las tasas de
mortalidad y nacimiento se estabilizaron. El Reino Unido había alcanzado la
cuarta etapa de la transición demográfica. Esto también sucedió en otros lugares
y cada vez más países recorrieron las cuatro etapas. Primera, muchos nacimientos
y muertes por las malas condiciones de vida. Segunda, mejores condiciones de
vida que llevan a menos muertes y explosión demográfica. Tercera, menos muertes
que se traducen en menos nacimientos y fin del crecimiento demográfico. Pero si
las tasas de nacimiento han caído tanto, ¿por qué la población sigue creciendo
tan deprisa? Bueno, los niños nacidos en la explosión demográfica de los 70 y 80
ahora se están convirtiendo en padres y por ello se ha producido un importante
pico de población. Pero como media tienen muchos menos hijos que sus padres. Hoy
la media es de 2,5, cuando hace 40 años era de 5. Así a medida que esta
generación se haga mayor y la fertilidad caiga más, la tasa de crecimiento
demográfico seguirá ralentizándose. Es algo que se cumple en todos los países.
En Occidente se tiende a subestimar el progreso en otras regiones del mundo,
pero lo cierto es que en la mayoría ya se ha alcanzado la cuarta etapa.
Fijémonos en Bangladesh. En 1971 las mujeres tenían una media de 7 hijos, aunque
el 25% moría antes de cumplir los 5. En 2015 la tasa de mortalidad había bajado
hasta el 3,8% y las mujeres solo tenían 2,2 hijos como media. Es la regla, no
una excepción. Occidente no es especial, solo empezó antes. A los países
desarrollados les ha costado unos 80 años reducir la fertilidad de más de 6
hijos a menos de 3. Otros lo están consiguiendo rápidamente, Malasia y Sudáfrica
en 34 años, Bangladesh en 20, Irán en solo 10. Todos estos países que han
superado la transición no han tenido que empezar de cero y cuanto más apoyo
reciben, antes lo consiguen. Por eso son importantes los programas para reducir
la mortalidad infantil o ayudar al desarrollo de las naciones pobres. No importa
la motivación, ya se sueñe con un mundo en el que todos seamos libres y ricos o
se quiera evitar las avalanchas de refugiados, la simple realidad es que a todos
nos beneficia personalmente que la gente viva bien en todas las partes del
planeta. Y lo estamos consiguiendo. El porcentaje de personas extremadamente
pobres nunca ha sido tan bajo como hoy. Por eso el futuro del crecimiento de la
población mundial en realidad no es una profecía apocalíptica, sino una promesa.
El crecimiento de la población terminará. La ONU prevé que nunca alcancemos los
12.000 millones de habitantes. Y a medida que aumente el nivel de desarrollo del
mundo, el número de personas con más educación se multiplicará por 10. Y los
países que solían estar necesitados ayudarán a avanzar en el desarrollo. Más
personas significa más individuos para poder avanzar como especie. La ONU es el
nombre libre, la aventura de los sentidos. La ONU es el nombre libre, la
aventura de los sentidos. La ONU es el nombre libre, la aventura de los
sentidos. La ONU es el nombre libre, la aventura de los sentidos. La ONU es el
nombre libre, la aventura de los sentidos. La ONU es el nombre libre, la
aventura de los sentidos. La ONU es el nombre libre, la aventura de los
sentidos. La ONU es el nombre libre, la aventura de los sentidos. La ONU es el
nombre libre, la aventura de los sentidos. La ONU es el nombre libre, la
aventura de los sentidos. La ONU es el nombre libre, la aventura de los
sentidos. La ONU es el nombre libre, la aventura de los sentidos. La ONU es el
nombre libre, la aventura de los sentidos. La ONU es el nombre libre, la
aventura de los sentidos. La ONU es el nombre libre, la aventura de los
sentidos. La ONU es el nombre libre, la aventura de los sentidos. La ONU es el
nombre libre, la aventura de los sentidos. La ONU es el nombre libre, la
aventura de los sentidos. La ONU es el nombre libre, la aventura de los
sentidos. La ONU es el nombre libre, la aventura de los sentidos. La ONU es el
nombre libre, la aventura de los sentidos. La ONU es el nombre libre, la
aventura de los sentidos. La ONU es el nombre libre, la aventura de los
sentidos. La ONU es el nombre libre, la aventura de los sentidos. La ONU es el
nombre libre, la aventura de los sentidos. La ONU es el nombre libre, la
aventura de los sentidos. La ONU es el nombre libre, la aventura de los
sentidos. La ONU es el nombre libre, la aventura de los sentidos. La ONU es el
nombre libre, la aventura de los sentidos.