TRANSCRIPTION: MEXIQUENSE-RADIO_AUDIO/2023-03-21-00H00M.MP3
--------------------------------------------------------------------------------
 Sin embargo, uno de ellos falleció. Más información en una hora. Música linda y
querida. Desde hace mucho que me gusta, así lo que me gusta obtengo con toda
seguridad. Yo no he perdido la esperanza de que un día tú me quieras y algún día
me querrás. Tardío temprano serás mío, yo seré tuyo algún día y lo tengo que
lograr. Qué cosa amor, que ya te lo advertí, que no descansaré hasta que esías
mío nomás. Pues tú me gustas de hace tiempo, mucho tiempo atrás. Me gustas
mucho, me gustas mucho tú. Tardío temprano seré tuya, mío tú serás. Me gustas
mucho, me gustas mucho tú. Tardío temprano seré tuya, mío tú serás. Qué cosa
amor, que ya te lo advertí, que no descansaré hasta que esías mío nomás. Pues tú
me gustas de hace tiempo, mucho tiempo atrás. Me gustas mucho, me gustas mucho
tú. Tardío temprano seré tuya, mío tú serás. Me gustas mucho, me gustas mucho
tú. Me gustas mucho, me gustas mucho tú. Tardío temprano seré tuya, mío tú
serás. Me gustas mucho, me gustas mucho tú. Tardío temprano seré tuya, mío tú
serás. La española más mexicana, Rocio Durkal. Me gustas mucho. De esos temas
que simplemente no se pueden olvidar y no dejan de tener un lugar especial en la
fonoteca de nuestro recuerdo, ¿verdad? Porque también detrás de estas canciones
hay infinidad de historias. Por ejemplo, no olvidemos que Rocio Durkal conoció a
Juan Gabriel en los años 70s y a partir de ahí trabajaron juntos y forjaron una
fuerte amistad. Cada que estos dos grandes artistas se unían para crear una
canción o se paraban en un escenario, tenían mucho éxito. Durante varios años,
la Durkal fue una de las cantantes que interpretó temas escritos por El Divo de
Juárez como costumbres, amor eterno, déjame vivir, entre muchas otras.
Lamentablemente su amistad terminó mal. Rocio dejó de cantar los temas escritos
por Alberto Aguilera y su relación se deterioró. Lo que se comenta es que fueron
problemas entre las disqueras y disputas mediáticas, cuestiones de permisos y
derechos que dio por terminar a una gran amistad. Y qué triste, ¿no? ¿Qué pasa
en estas situaciones, no creen? Porque finalmente nada nos llevamos al morir y
mientras estemos en vida es momento de dar lo mejor de nosotros y mantener
buenas relaciones, tal como nos lo canta a continuación Alberto Araneda, a quien
le dicen la voz doble de Alejandro Fernández, y nos interpreta esto llamado El
Tiempo Se Va, evocando la esencia de nuestro México a través de nuestra música
linda y querida. Música linda y querida. Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música linda y
querida. Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música
Música Música Música Música Música Música Música Música Música Música Música