TRANSCRIPTION: MEXIQUENSE-RADIO_AUDIO/2023-03-22-22H00M.MP3
--------------------------------------------------------------------------------
 El más allá. Lunes a viernes. Diez de la noche. Este no es el más allá. La
programación de este horario es Clasificación AA. Cuando tengo ganas de cantar
me pongo a gritar ¡Grillos, grillos, grillos! ¡Hasta en la sopa! Sopa! Cuando
tengo ganas de platicar Platicar Me pongo a gritar ¡Grillos, grillos, grillos!
¡Hasta en la sopa! Sopa! Cuando tengo ganas de bailar Bailar Me pongo a gritar
¡Grillos, grillos, grillos! ¡Hasta en la sopa! Sopa! Sopa! ¡Grillos, hasta en la
sopa! Nunca eres cosa seria Nunca eres trabajar Un momento yo aprendo Fantasia y
realidad Nunca eres importante Es como respirar Un momento yo descubro El mundo
y mi habitación Un momento imagino Lo que puede suceder Que fue lo cual si fue
el héroe Que princesa puedo ser Puedo ser Puedo ser Puedo ser Cujarme hace
socios De niños de mi edad Comparto y respeto Cualquier diversidad Juguendo
aprendo mucho Como la libertad Lo bueno y lo malo Cualquier posibilidad Jugar es
necesario Pienso en lo principal Jugando yo te quiero Mil historias contar Mil
historias contar Mil historias contar Mil historias contar Jugar es cosa seria
Jugar es trabajar Juguendo yo aprendo Fantasia y realidad Juguendo aprendo mucho
Como la libertad Lo bueno y lo malo Cualquier posibilidad Juguendo imagino Lo
que puede suceder Que fue lo cual si fue el héroe Que princesa puedo ser Puedo
ser Mil historias contar Mil historias contar Grillos hasta en la sopa El vecino
se encerró en su casa Cuando saque a mi tiburón a pasear Saque a mi tiburón a
pasear Saque a mi tiburón a pasear Al buzón se metió el cartero Cuando saque a
mi tiburón a pasear Saque a mi tiburón a pasear Saque a mi tiburón a pasear La
maestra se trepo en un árbol La maestra se trepo en un árbol La maestra se trepo
en un árbol Cuando saque a mi tiburón a pasear Saque a mi tiburón a pasear Saque
a mi tiburón a pasear El policía se trago el silbato El policía se trago el
silbato El policía se trago el silbato Cuando saque a mi tiburón a pasear Saque
a mi tiburón a pasear Saque a mi tiburón a pasear La viejita se quedó en
Chamuela La viejita se quedó en Chamuela La viejita se quedó en Chamuela Cuando
saque a mi tiburón a pasear Saque a mi tiburón a pasear Saque a mi tiburón a
pasear Todos los perros se hicieron pipi Cuando saque a mi tiburón a pasear
Saque a mi tiburón a pasear Todos los gatos se hicieron a un lado Cuando saque a
mi tiburón a pasear De repente que se pone morado Pues con el aire no podía
respirar Saque a mi tiburón a pasear La la la la la la la la la Saque a mi
tiburón a pasear Corría a mi casa y lo aventaba excesado Corría a mi casa y lo
aventaba excesado Corría a mi casa y lo aventaba excesado Y mi mascota comenzó a
respirar Saque a mi tiburón a pasear Grillos hasta en la sopa Mi gallina
sarabiada Hace días puso un huevito Y del huevito nacieron dos pollitos
chiquiticos Mi gallina sarabiada Hace días puso un huevito Y del huevito
nacieron dos pollitos chiquiticos Que le decía Pío pío hermanita, pío pío pío
hermana Pío pío pon, pío pío pon Con el correr de los días Un pollito fue
pollita Y el pollito compañero completó la parejita Con el correr de los días Un
pollito fue pollita Y el pollito compañero completó la parejita Que le decía Pío
pío hermanita, pío pío pío hermana Pío pío pon, pío pío pon La pareja fue
creciendo Y un día desde mi balcón Vi a la pollita, ella pollá, y al pollito un
volantón La pareja fue creciendo Y un día desde mi balcón Vi a la pollita, ella
pollá, y al pollito un volantón Y cocorriaba Pío pío, pío, pío, mami, pío, pío,
pío, pío, mami Mamita La polla se hizo gallina cuando comenzó a poner Y el
volantón es el gallo que canta al amanecer La polla se hizo gallina cuando
comenzó a poner Y el volantón es el gallo que canta al amanecer Y así me canta A
cinco A seis Que a cinco A seis A cinco, a seis Que a cinco, a seis Al fin el
sol se fue a dormir, la noche apareció Junto a la fuente en el jardín, la fiesta
comenzó Seis patitas por aquí, acaso nocho allá son mil Con alitas, con antenas,
con rayitas, con melenas Con pancitas bien rellenas se dejan oír Sin aviso de
repente la luna se asomó Sigilosa, refulgente, todo iluminó Seis patitas por
aquí, acaso nocho allá son mil Trinacates, cochinillas, alacanes, tijerillas
Campa muchas y bolillas bailan por aquí Por bailar siempre al revés un callo le
salió Pero como eran cien pies, les quedaban noventa y nueve patitas Para poder
seguir bailando Pero tenía que desabrocharse los cincuenta paredes zapatos
Estirarse muy bien los calcetines y checar cuál era el pie que le dolía Pero
como no pudo encontrarlo, nada le importó Seis patitas por aquí, acaso nocho
allá son mil Con alitas, con antenas, con rayitas, con melenas Mismo techo,
bicho y hecho y esto se acabó Y esto se acabó ¡Grillos hasta en la sopa! Buenas
tardes, bienvenidas, bienvenidos, qué gusto me da saludarles En esta tarde ya
del miércoles 22 de marzo del año 2023 Yo soy Pablo Panda aquí en la cabina de
música infantil de Mexiquense Radio ¡Grillos hasta en la sopa! Un gusto, un
honor, un privilegio abrir estos micrófonos Para reencontrarme con ustedes, para
saludarles y darles obviamente La más cordial bienvenida al miércoles de
complacencias musicales Ya saben lo que hay que hacer, está en línea el WhatsApp
722-443-1600 Ahí estamos listos para recibir sus peticiones musicales también
Está en 800-593-000 o al 275-5627 Y pues a esta hora también comentarles que
ustedes pueden pedir su canción favorita Del repertorio infantil, yo he
encantado de hacerla sonar Aquí al aire en Mexiquense Radio En el control
técnico esta tarde me acompaña José Ramón Martínez En la continuidad Mariano
Ramírez Listos Para llevar hasta ustedes la música infantil Pero también ya está
con nosotros en línea Pequitas allá en el 1080 de AM, Mexiquense Radio en el
Valle de México Bienvenida, ¿cómo estás? Buenas tardes ¿Cómo estás mi querido
amigo Pablo Panda? Yo estoy super acherequete mega, increíblemente feliz y
emocionada de estar con todos ustedes En este ya miércoles mitad de semana Hoy
lo puedes creer que rápido se nos ha ido la semana Pues cómo no, si hemos tenido
realmente una semana que se fue rapidísimo Porque nos estuvimos llenos de
puentes Pero ahora que estamos ya de regreso la verdad es que estamos super
acherequete mega Increíblemente felices de estarlos acompañando Así que la
invitación como tú ya bien lo dijiste A que se pongan en contacto con nosotros,
a que nos llamen A que tiran sus temas musicales y por supuesto comenten qué tal
les fue en la escuela O qué tal las clases o sus maestras Cuáles son las tareas
que les han dedicado Ay por cierto hablando de maestras quiero mandarlo un super
acherequete mega Increíblemente feliz y sobre todo saludo A la maestra Liz que
nos está acompañando Y también a sus pequeñas hijas Samantha y Andraíta Que
están sintonizando Pablo Panda y Pequitas aquí a través de Grillos Hasta en la
Sopa Les mandamos un super acherequete mega increíblemente Saludo para que la
pasen muy bien y felices en este miércoles Y también nos comenten qué tema
musical quieren escuchar Como ves mi querido amigo Pablo Panda Saludos a estos
profesores que escuchan Grillos Hasta en la Sopa También de mi parte eh, muchas
gracias por su sintonía Y preferencia es momento de hacer una pausa Nosotros
vamos al corte pero regresamos rapidísimo Porque ya hay mensajes, ya hay
canciones ahí formaditas Esperando salir al aire en esta tarde miércoles de
complacencias Esto es Grillos Hasta en la Sopa Grillos Hasta en la Sopa Aprender
a poner límites es algo fundamental que los niños, niñas y adolescentes deberían
saber Para así lograr un óptimo desarrollo cerebral y personal Los límites, de
la mano de una armoniosa educación desde casa Preparan para la buena toma de
decisiones y recuperación ante las dificultades Es importante considerar que
tanto para niños como adolescentes La falta de límites y el exceso pueden ser
perjudiciales para su autoestima, identidad y autonomía Sin límites, su
desarrollo se vería despreocupado Y con el exceso sería un individuo dependiente
e inseguro Para más información consulta www.gov.mx diagonal Cipina Mexiquece
Radio ¿Cómo ves Pepe? Ayer fui a una entrevista de trabajo y me dijeron que un
mes aprueba y se empoce sueldo Que mal caray, siempre es lo mismo O te piden
años de experiencia o pésima paga de prestaciones sin dinero O pésima paga de
prestaciones y mejor ni hablamos Sí, me choca eso, llevo un año sin trabajar y
no tengo para el super Las cosas tienen que cambiar, hoy más que nunca los
jóvenes tenemos que demostrar de qué estamos hechos Es Tipo del Verde Partido
Verde, Estado de México La cultura lacustre existe todavía en las lenguas de los
pobladores del sur de la ciudad de México Y de una parte de pobladores en el
norte de la ciudad de México que es alrededor del lago de Zumpanco Estamos
hablando de agua que todavía existe producto de esa cultura que tuvimos hace
muchísimos siglos Tenochtitlan se construyó con calles de agua y otras mitad
agua y mitad tierra El agua potable era traída por acueductos Fue una ciudad
lacustre protegida por diques y albarradones sostenida de los frutos de sus
chinampas, lagunas y campos Para entonces el albarradón Dénes Agualcóyotl separó
las aguas dulces de las aladas Así que los pueblos crecieron literalmente al
filo del agua Después de una experiencia de dos siglos, aprendieron a conservar
sus recursos naturales y aprovecharlos al máximo sin destruirlos Al inicio de la
conquista, la cuenca del Palle de México contaba con 300.000 personas Nunca
faltó el agua Actualmente al valle de México se distribuyen 64.500 litros de
agua por segundo para alrededor de 21 millones de habitantes Con todo, el agua
falta Este 22 de marzo Porque el agua importa Mexiquense Radio, presente en el
Día Mundial del Agua ¡Por casi 100 años en nuestro estado, las y los del PRIAN
han hecho de todo para mantenerse en el poder! Fraudes, mentiras, injusticias,
corrupción Pero ni un año más. Su tiempo ya se acabó De la mano del pueblo se
consolidará la cuarta transformación en el estado de México Para iniciar una
nueva era de esperanza y de fortaleza transformación en el Estado de México para
iniciar una nueva era de esperanza y honestidad. Ya viene el cambio. Morena, la
esperanza del Edomex. La programación de este horario es clasificación a... El
Cáncer de colon después del de pulmón es el segundo más mortífero en el mundo,
con un millón de fallecimientos al año. Y es que la metástasis que es la
propagación del tumor a otros órganos del cuerpo se ha convertido en una de las
principales problemáticas para luchar contra esta enfermedad. Esto motivó para
que un equipo internacional de científicos del Instituto de Investigación
Biomédica de Barcelona, dirigidos por el biólogo español Edward Vale,
descubrieran las células malignas que se desprenden del cáncer y provocan las
muertes por metástasis. Mediante su estudio, han concebido un nuevo método que
es capaz de capturar de forma microscópica células minúsculas de estos tumores
malignos que se desprenden del colon para evitar que viajen por el torrente
sanguíneo e invadan el hígado. Hasta ahora, estas células malignas eran
invisibles, pero gracias a este descubrimiento los científicos pueden
eliminarlas y evitar en muchos de los casos que se generen tumores secundarios
malignos. Ahora solo falta demostrar la eficacia de esta estrategia en humanos,
un ensayo clínico que ya se ha llevado a cabo en países bajos y cuyos resultados
podrían beneficiar a muchos pacientes. ¡Grillos hasta en la sopa! Las 3,25
minutos a esta hora de la tarde es un gusto acompañarles a través de Mexiquense
Radio AM en el 105.5 FM en Atlacomulco, el 1250 de AM en Tejupilco, el 1080 de
AM en el Valle de México y obviamente el 1600 en el Valle de Toluca, desde donde
les saludamos aquí en Metepec, Estado de México. Desde aquí la señal viaja vía
satélite hasta encontrarse con ustedes. Y hoy, miércoles de complacencias, aquí
en Grillos hasta en la sopa. ¿Ya saben lo que hay que hacer? Estamos en línea en
WhatsApp, 722-443-1600. Dice, bonita tarde chicos, Pablo Panda y Pequitas, me
dedican la de Chiquita, pero picoza. Soy Adri, saludos Adri. Desde Jocotitlante
mandamos el saludo, gracias por ser parte de Grillos hasta en la sopa. En breve
sonará este tema musical de Nina O'Limon. Y también dice, hola Pablo Panda,
¿puedes enviarle un saludo a mis grillitos hambrientos, Kenai y Leilani por
favor? Dedicales la canción de Mueve el ombligo con Cristel. Muchas gracias y
saludos para todos los grillos que están en sintonía de parte de Kenai y
Leilani. Pues ahí está el saludo, gracias por participar. Y también sonará este
tema musical con Cristel, las 3.26, que comience a sonar Nina O'Limon, chiquita
pero picoza. Y así de fácil, ustedes pueden participar. Pedir su canción
favorita del repertorio infantil 722-443-1600, que suene la música, esto es
Grillos hasta en la sopa. Dicen que me veo chiquita y tantas otras cosas. Dicen
que porque lo como un poco más. Dicen que seguramente soy muy débil y luchar.
Pero están equivocados, les voy a enseñar. Que soy mucho más fuerte que el más
gran dulón, que mi rugido es igual al de un león. Y que soy sincera, tengo un
gran corazón. Que no ves que soy perfecta como soy, alejando mi camino a donde
voy. Y que soy chiquita como dice la canción. Chiquita pero picoza, paso a
pasito yo voy orgullosa. Si confundan yo soy poderosa, quien me detendrá.
Chiquita pero picoza, paso a pasito yo voy orgullosa. Si confundan yo soy
poderosa, quien me detendrá. Creo que por ser pequeña soy maravillosa. Salto de
una estrella a otra hacia el sol. Ven conmigo volaremos entre mariposas. Donde
las barreras son una ilusión. Que soy mucho más fuerte que el más gran dulón,
que mi rugido es igual al de un león. Y que soy sincera, tengo un gran corazón.
Que no ves que soy perfecta como soy, alejando mi camino a donde voy. Y que soy
chiquita como dice la canción. Chiquita pero picoza, paso a pasito yo voy
orgullosa. Si confundan yo soy poderosa, quien me detendrá. Chiquita pero
picoza, paso a pasito yo voy orgullosa. Si confundan yo soy poderosa, quien me
detendrá. Chiquita pero picoza, paso a pasito yo voy orgullosa. Si confundan yo
soy poderosa, quien me detendrá. Chiquita pero picoza, paso a pasito yo voy
orgullosa. Si confundan yo soy poderosa, quien me detendrá. Chiquita pero
picoza, paso a pasito yo voy orgullosa. Si confundan yo soy poderosa, quien me
detendrá. Chiquita pero picoza, paso a pasito yo voy orgullosa. Si confundan yo
soy poderosa, quien me detendrá. ¡Grillos hasta en la sopa! Buenas tardes, buen
provecho, las 3.30 minutos. ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Grillos hasta en la sopa! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Grillos hasta en la sopa! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes!
¡Buenas tardes! ¡Buenas tardes! ¡Buenas tardes! Les dedico la canción de 31
minutos de mi muñeca me hablo que los quiero mucho mucho mucho Les mando un gran
saludo Saludos Reggie ahí está escucharon tu saludo de viva voz oye gracias por
dejarnos Pues escuchar tu voz aquí al aire en grillos hasta en la sopa gracias
también por participar y tenemos más mensajes dice hola pablo panda y pequitas
por favor pueden poner la canción de que se pongan botas y se quiten tenis es
para mi mamá marique la quiero mucho de parte de sarah y gracias pues que suenen
este par de temas musicales las 3 con 48 ya casi en la despedida miércoles de
complacencias musicales a king grillos hasta la sopa Que no puedo repetir porque
me habla solo a mí Me dijo cosas secretas que tú no puedes oír me confeso
algunos pecados que prefiero no decir me dijo algunas cosas locas que no te voy
a contar Tomamos temas muy profundos muy difíciles de hablar Y aunque no creas
que ella habla de verdad es parla en china se sabe un montón de cuentos Unos de
la vecina pone cara de inocente pero es tan peladora mi muñeca sabe todo es como
una grabadora Tu muñeca te habló y me dijo cosas que no puedes repetir porque te
habla solo a mí Solo a mí Grillos hasta la sopa O sea que en el hombre muchachos
se sabota y suena y suena tremendo el taconazo Que bailen todos al son de la
tambora y agarren pista bailando bien pegado Oscar, Kalimant y Luis andan
bailando Ay, y Cambalia y Liliana se agarraron Mariana y Lidia de plano andan
buscando para bailar con el aria Alejandro Son las parejas que bailan mis
parejas y suena y suena tremendo el taconazo Que bailen todos al son de una
tambora porque la fiesta apenas va empezando Que se pongan botas que se quiten
tenis que la va bailando todos con la banda nueva Oscar, Kalimant y Luis andan
bailando Ay, y Cambalia y Mariana se agarraron Mariana y Lidia de plano andan
buscando para bailar con el aria Alejandro Son las parejas que bailan mis
parejas y suena y suena tremendo el taconazo Que bailen todos al son de la
tambora porque la fiesta apenas va empezando Que se pongan botas que se quiten
tenis que la va bailando todos con la banda nueva Grillos hasta en la sopa Las 3
con 54 minutos de vuelta a quien grillos hasta en la sopa prácticamente Para la
despedida dice este mensaje Hola, buenas tardes, le puedes enviar un saludo a
mis hijos Juan Diego y David También a mi hermana Chave, por favor te escuchamos
en Tlalpujagua, les puedes dedicar una canción Claro que si, bonita tarde para
ustedes y pues el mejor el deseo que esté todo de maravilla allá en Tlalpujagua
Pequitas, adelante Mi querido amigo Pablo Panda, pues es momento de despedirnos
Pero no sin antes hacerle la invitación para que mañana en punto de las 3 de la
tarde se supera Cherequitmega increíblemente Conecten, ya saben a través del
1600 del 1080 de AM y nosotros vamos a estar super felices de estar con todos
ustedes Amigo que pases una gran tarde, comer rico y por supuesto a todos
nuestros amigos que se pusieron en contacto con nosotros Y a los que no también
les mandamos un pequi beso, muchas gracias Gracias a ti Pequitas, saludos a
todas las grillas y grillos del Valle de México Ahora si, ha llegado el momento
de la despedida, disfruten lo que resta de este miércoles a la mitad de la
semana Aceleren y terminemos con muy buena actitud, gracias a José Ramón
Martínez en el control técnico A Francisco Díaz en la continuidad, yo soy Pablo
Panda Disfruten lo que resta de este día, aprovechen cada segundito, adelante en
las tareas y sean muy, pero muy felices Cuando tengo ganas de comer, me pongo a
gritar, grillos, grillos, grillos hasta en la sopa Grillos hasta en la sopa
Unisquince Radio Sabías que... En Oaxaca, el petate era símbolo de prosperidad
El petate es una artesanía, hecho de fibras de palma trenzadas Su uso principal
era como una cama, aunque hay otros usos para este y muchos simbolismos para las
diferentes culturas Sabías que... La radio no solo se transmite por ondas
Nosotros no somos una aplicación de música con algoritmo Llegamos hasta tus
oídos a través de ceros y unos que viajan en la red Encuentra y escucha todas
nuestras frecuencias A través de Radio y TV Mexiquense punto MX Conectate con la
música, las voces, las ideas y el sonido de Mexiquense Radio Radio y TV
Mexiquense punto MX Edomex, decisiones firmes, resultados fuertes La
programación de este horario es clasificación A Después de escuchar estos
relatos, dormirás con las luces encendidas Historias del más allá Lunes a
viernes 10 de la noche Historias del más allá Bain AMX Noticias Este día el
gobernador del Estado de México, Alfredo del Mazo Maza, realizó la entrega de la
construcción del Hospital Integral Coacalco Que beneficiará a más de 168 mil
habitantes de dicha región del estado En Tlalnepantla, Jesús y María Concepción
fueron detenidos en un operativo realizado por personal de la Fiscalía General
de Justicia del Estado de México Por su probable participación en delitos contra
la flora y la fauna Al ofrecer animales exóticos de los cuales no contaban con
la documentación para llevar a cabo esta actividad En dicho operativo fueron
rescatados dos cocodrilos de 35 centímetros de longitud Una serpiente, ocho
tortugas, dos iguanas, cuatro erizos, ajolotes y hasta hamsters Una avioneta de
fumigación que salió del aeropuerto de Campeche cayó por una falla mecánica Y el
piloto, aunque con golpes, salvó la vida Elementos de la Sedena interrogaron al
piloto quien les comentó que la avioneta particular presentó una falla mecánica
y tuvo que planear mientras caía En Tlaxcala un grupo de alumnos del CETI 132
fue exhibido por la brutal golpiza que le dieron a uno de sus compañeros Al que
dejaron inconsciente, pese a la evidencia, estos estudiantes solo fueron
suspendidos tres días El secretario de Educación Pública de Tlaxcala, Homero
Meneses, confirmó que los agresores fueron alumnos del sexto semestre del CETI
132 al igual que la víctima La Reserva Federal de Estados Unidos elevó este
miércoles las tasas de interés de referencia en un cuarto de punto porcentual
Pero indicó que está a punto de frenar nuevas alzas en los costos del
endeudamiento tras el colapso de los bancos estados