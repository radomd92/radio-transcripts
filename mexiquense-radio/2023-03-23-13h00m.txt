TRANSCRIPTION: MEXIQUENSE-RADIO_AUDIO/2023-03-23-13H00M.MP3
--------------------------------------------------------------------------------
 mexiquense radio estamos contigo la programación de este horario es
clasificación a xe gem 1600 2500 watts de potencia mexiquense radio avenida
estado de mexicoriente 1601 colonia llano grande somos una estación del sistema
mexiquense de medios públicos mexiquense radio radio educación presenta can gold
no La tía Julia y el escribidor de Mario Vargas Llosa Después de haber abordado
el tema doloroso para él de los argentinos, Pedro Camacho cerró las ventanas de
su cubil, cuadró el rodillo de la Remington y le colocó su funda. Se acomodó la
corbatita de lazo, sacó de su escritorio un grueso libro que se puso bajo el
sovaco y me indicó con un gesto que saliéramos. Apagó la luz y de afuera echó
llave a su cueva. Ya está. ¿Qué libro es ese señor Camacho? Puede usted verlo.
Diez mil citas literarias de los cien mejores escritores del mundo. Lo que
dijeron Cervantes, Shakespeare, Molière, etcétera, sobre Dios, la vida, la
muerte, el amor, el sufrimiento. Un viejo compañero de aventuras, un amigo fiel
y un buen ayudante de trabajo, de Adalberto Castejón de la reguera. Licenciado
por la Universidad de Murcia en letras clásicas, gramática y retórica. Ah, lo
conoce usted. No, aquí dice. Ah, sí. Escribió esa obra en el momento culminante
de su vida, a los 50 años. La edad de oro del hombre. ¿Eh? ¿Eh? La edad del
apogeo cerebral y de la fuerza sensual, de la experiencia digerida. La edad en
que se es más deseado por las mujeres y más temido por los hombres. ¿Qué hora
es? Deben ser las, las diez, ¡qué barbaridad! Es tardísimo. Buena hora para el
refrigerio no, pero no. Yo lo dejo, señor Camacho. Voy a ver qué ha hecho
Pascual del boletín de las nueve. Vaya usted, mi amigo. Buenas noches. ¡Adiós!
Corría Panamericana convencido de que Pascual habría dedicado los 15 minutos del
boletín de las nueve a algún pirómano de Turquía o algún infanticidio en el
porvenir. Pero las cosas no debían de haber ido tan mal, pues me encontré a
Genaro Hijo en el ascensor y no parecía furioso. Pues nada, Mario, que esta
tarde he firmado contrato con Lucho Gatica para que venga una semana a Lima como
exclusivo de Radio Panamericana. ¿Qué te parece, eh? Hombre, las mujeres de Lima
nos van a destrozar las instalaciones. Ah, nuestras instalaciones van para
arriba, Mario, para arriba. Eso es cierto, necesito tu ayuda. ¿Para qué? Hay que
redactar los avisos para anunciar los radioteatros de Pedro Camacho que arrancan
el lunes. Tienen que estar listos mañana mismo para publicarlos el domingo.
Bueno, pero ¿por qué no los redacta el propio artista? La vaina es que se ha
negado. Buenas noches, Mario. Llamó a tu tío. ¿Eh? Tu tío Lucho, que dejaste
plantada a Julita, que tenían que ir al cine, que eres un salvaje que la llames
para disculparte. Mi madre. No, tu tío Lucho. Bueno, Mario, el caso es que Pedro
se niega a redactar los avisos. Pero qué imbécil, ¿cómo se me fue a olvidar?
Mario, hazme caso. Según Pedro, sus libretos no necesitan publicidad mercenaria.
Se imponen solos y no sé qué otras tonterías. El tipo está resultando
complicado, muchas manías. ¿Supiste lo de los argentinos, no? Anda, algo ahí. El
tipo nos ha obligado a rescindir contratos, a pagar indemnizaciones. Espero que
sus programas justifiquen esos engreimientos. Permíteme un momento, ¿sí, genero?
Oye, Menelli, ayúdame a salir de este lío. Ah, no, tus líos de falda lo
resuelves tú. Es mi tía, tonta. Es un lío familiar. Ah, bueno, entonces sí.
Mira, toma. Mañana en la mañanita ve y escoge un ramo en la florería de la
esquina y me lo mandas a esta dirección. ¿Qué flores? Las que quieras. Rosas, me
encantan. Toma, les pones esta tarjeta. Ajá. Rendidas excusas, Mario. ¡Ay, qué
elegante! Ya, hazme el favor y cállate. ¿Ya acabaste? Ya, ya. Gracias, Nelly. No
me lo agradezcas, que un día te lo voy a cobrar. Buenas noches, Mario. Buenas
noches, don Genaro. Buenas noches, Nelly. Pues sí, Mario. Este camacho ha
resultado bastante pesado en sus exigencias y acabo de tener otro pleito con él.
¿Por qué? Pues le hemos explicado que todos los surveys han demostrado que el
público quiere galanes de entre 30 y 35 años, pero es una mula. Los
protagonistas de los cuatro radioteatros con que debuta son cincuentones. En
todos ellos, el galán es un cincuentón que conserva maravillosamente la
juventud. Oye, ¿y si he metido la pata y el boliviano este es un fracaso
descomunal? ¿Quién sabe, Genaro? A lo mejor el público quiere algo nuevo y
todavía no lo sabe. Bueno, si quieres, nos podemos ver mañana para redactar los
avisos. Sí, sí. Mañana paso por aquí después del boletín de las once. Te invito
a almorzar al Raimondi y allí trabajamos. Bien. Hasta mañana. Hasta mañana,
Mario. Y gracias. Al día siguiente era cumpleaños del tío Lucho. Cuando
terminamos de redactar los avisos, Genaro, hijo y yo, era tarde para dar un
salto a casa de los tíos. De modo que telefoné para decirle que lo felicitaría
en la noche. Mario, ¿qué horas de llegar? Ya se fueron todos. Perdón, es que
tuve mucho trabajo todo el día. Felicidades, tío Lucho. Qué bueno que llegaste,
muchacho. Toma. Hombre, pero cómo te has molestado. A ver si te gusta. Un
whisky, Marito. Estamos dándole mate a uno de los regalos de mi cuñado. Bueno,
gracias. Qué corbata. Muchas gracias, Mario. De veras que es linda.
Definitivamente solo los hombres deben regalar corbatas. Una siempre mete la
pata. Toma, Marito. Gracias, Julia. Por cierto, gracias por las rosas. Ya las
puse en un florero. De nada, Julita. Espero que me habrás perdonado el plantón.
¿Aquellas son las rosas? Sí, muy bonitas. Maldita Nelly, son poquísimas. Pero no
creas que por las rosas te librarás de llevarme al cine cualquiera de estos
días. A ver, dime la verdad. ¿Por qué me dejaste plantada? ¿Alguna piba de la
universidad? ¿Alguna huachafita de la radio? La tía Julia llevaba un vestido
azul, zapatos blancos, maquillaje y peinado de peluquería. Se reía con una risa
fuerte y directa. Y tenía voz ronca y ojos insolentes. Descubría algo
tardíamente que era una mujer atractiva. Bueno, qué caray. Cincuenta años se
cumplen una sola vez en la vida. Vámonos al Grill Bolívar. Yo invito. ¡Al Grill
Bolívar! ¡Sensacional, cuñado! Vamos. Un momento, un momento. Este muchacho no
está nada bien para presentarse en el Grill Bolívar. ¿Capaz que no lo dejan
entrar? ¿Cómo que no lo dejan entrar? A ver, Lucho, préstale una camisa y una
corbata llamativa para que no se note tanto lo viejo del terno y lo arrugado.
Pero tía, si... Tiene razón, Olga Mario. Ven, ven. Que te presto algo. La camisa
me quedó grande y yo sentía angustia por mi cuello bailando en el aire. Pareces
Popeye, Marito. Bueno, de todas formas está mejor que como estaba. Vámonos.
Lucho, apague el tocadiscos. Voy por mi bolso. Vamos, Popeye. ¡Apaga el
tocadiscos! ¡Apaga el tocadiscos! ¡Apaga el tocadiscos! ¡Vamos, Popeye! ¡Al
Grill Bolívar! Nunca había ido al Grill Bolívar y me pareció el lugar más
refinado y elegante del mundo. Y la comida, la más exquisita que había probado
jamás. Una orquesta tocaba boleros, pasodobles, blues. Y la estrella del show
era una francesa, blanca como la leche, que daba la impresión de masturbar el
micro con las manos mientras cantaba. ¡Bravo! ¡Bravo! ¡Vamos el chery! ¡Bravo,
bravo! ¡Ay, este Lucho está resaltado! Son 50 los que cumplen. ¡Pero el corazón
es de 15! Vamos a bailarte a Olga. ¡Oh, la la! ¡Qué bel vamos a hacer! Oye
Maritos, pero tú sabes bailar. ¿No lo estás viendo? Bueno, la verdad es que no
sé si eres tú el que me pisa o la pareja de junto. ¡Qué cantidad de gente! ¡Uy!
¡Mira a tu tío Lucho bailando con Julita! ¡Eh, tío Lucho! ¡Se va a encelar la
francesa! ¡No hay manera, hijo! ¿No ves que esta mujer me obliga a bailar
suelto? ¡Así no se encela nadie! ¡Qué papelón, Lucho! ¡Qué papelón! ¡Vuelta,
Lucho! ¡Vuelta! ¡Esa es! Oye, baila bien la Julia. Sí, bastante. Adiós, Bozoa.
Bueno, Maritos, hace años que no bailaba. Estoy que me derrumbo. Para mí fue un
placer, tía. El de los cincuenta años soy yo, holga, ¿eh? ¡Vamos a la mesa! Ah,
no, no, no. Yo no me voy a quedar sin pareja. A ver, Julia. ¡Vamos! Paso a la
juventud. Vamos, holga. ¡Una copa para el sofoco! En realidad yo no sé bailar.
No está mal, no está mal. Yo estaba convencido de que una vocación literaria era
incompatible con el baile y los deportes. Pero felizmente había mucha gente en
la pista y en la apretura y penumbra nadie en realidad pudo advertirlo. Y como
el blues era lentísimo, desempeñé mi función con decoro ante la tía Julia. En el
instante en que ella hacía un movimiento para apartarse de mí y volver a la
mesa, la retuve y la besé en la mejilla, muy cerca de los labios. Me miró con
asombro, como si presenciara un prodigio. La orquesta de Palito Quiroz volverá
con nosotros dentro de media hora con la sensacional Motherland. Demos la
bienvenida a la Orquesta de los Bajapontinos de Raúl Mireles. ¿Qué tal baila ese
mozalvete, Julia? ¿No te pisó los callos? No baila tan mal, aunque nadie te
llega a ti, cuñado. ¡Ay, no le digas eso! ¡Que se lo va a creer! A los 50 es
cuando hay que tener cuidado con los hombres, porque a partir de esas edades que
se vuelven viejos verdes. ¡Ay, no me digas eso! La tía Julia me lanzaba a ratos
una rápida hojeada, como para verificar si yo estaba realmente ahí, y en sus
ojos se podía leer clarísimo que todavía no le cabía en la cabeza que la hubiera
besado. Bueno, es hora de irse. Te digo, mujer, te estás poniendo vieja. Una
pieza más y nos vamos, tía. ¡Mire qué bonito bolero para bailar! El intelectual
se corrompe. ¡Vamos, Olga! Nuestra última pieza de la noche. ¡Ay, Lucho!
¿Bailamos, Julia? A ver, Julia, que no se diga que tú no aguantaste hasta el
final. Está bien. Mientras bailábamos, ella permanecía por primera vez muda.
Cuando entre la masa de parejas el tío Lucho y la tía Olga quedaron
distanciados, la estreché un poco contra mí y le junté la mejilla. Oye, Marito.
Te prohíbo que me vuelvas a llamar Marito. Ella separó un poco la cara para
mirarme e intentó sonreír, y entonces, en una acción casi mecánica, me incliné y
la besé en los labios. Fue un contacto muy rápido, pero no lo esperaba, y la
sorpresa hizo que esta vez dejara un momento de bailar. Ahora su estupefacción
era total. Abría los ojos y estaba con la boca abierta. Bueno, ahora sí. Ya
pagué y todo. Vámonos, familia. ¡Ay, qué noche! ¿Te divertiste, Julia? Sí,
estuvo bien, ¿no? Sí. En el trayecto a Miraflores íbamos los dos en el asiento
de atrás. Cogí la mano de la tía Julia, la apreté con ternura y la mantuve entre
las mías. No la retiró, pero se le notaba aún sorprendida y no abría la boca. Al
bajar en casa de los abuelos, me pregunté cuántos años mayor que yo sería. La
tía Julia y el escribidor, de Mario Vargas Llosa. Esta fue una producción de
Radio Educación. Actuaron por orden alfabético Marta Aura, Fernando Balsaretti,
Álvaro Carcaño, Miguel Gómez Checa, Luisa Huertas, Luis Miranda, Jorge Pol.
Colaboramos en este capítulo, Frutuoso López en los controles técnicos,
asistencia de producción Sonia Riquier, musicalización y efectos físicos Vicente
Morales, adaptación radiofónica y dirección de actores Silvia Mariscal,
producción Luisa Fernanda González. XETJ 1250 de Jupico. En autor could use Sa
DNA Information presents speeders. Y Day perfection en los gobiernos con visión
humanista. En el PAN pensamos en el progreso y el futuro, inspirados en lo que
nos motiva a todas y todos. El bien de nuestras familias, en donde cada día sea
una oportunidad para construir, para avanzar y ser felices. En nuestro estado de
México, con el PAN siempre hacia adelante. PAN, acción por Domex. ¿Cómo ves,
Pepe? Ayer fui a una entrevista de trabajo, me dijeron que un mes aprueba y se
encose el sueldo. Qué mal, Karay. Siempre es lo mismo. O te piden años de
experiencia, o pésima paga de prestaciones, mejor ni hablamos. Sí, me choca eso.
Llevo un año sin trabajar y no tengo para el súper. Las cosas tienen que
cambiar. Hoy más que nunca, los jóvenes tenemos que demostrar de qué estamos
hechos. Es tiempo del verde. Partido Verde, Estado de México. La programación de
este horario es clasificación A. Reinvención Total. Desarrolla tu conciencia
para llegar a la libertad interior. Martes y jueves, doce del día. Reinvención
Total. En el proceso electoral del Estado de México, las personas en prisión
preventiva podrán participar a través de la modalidad del voto anticipado.
Personal de las Juntas Distritales Ejecutivas del INE les visitarán para
invitarles a participar. Y en su caso, realicen su solicitud de inscripción en
la página web de la web de la web de la web. De esta forma podrán emitir su voto
del 15 al 19 de mayo. Mi INE es valioso porque mi INE nos une. ¡Aunque el año
más su tiempo ya se acabó! De la mano del pueblo se consolidará la Cuarta
Transformación en el Estado de México para iniciar una nueva era de esperanza y
honestidad. Ya viene el cambio. Morena, la Esperanza del Edomex. ¡Ay viene la
Cuarta Transformación! ¡Con este ritmo que está sabrosón! ¡Unidos en una
revolución que México lindo merece hoy! ¡A la izquierda toma el corazón! PT, PT
es la Cuarta T. PT es la Cuarta Transformación porque México merece más. ¡Ay PT!
¡Es la Cuarta T! ¡Mira! Me acostumbra a las cosas buenas de la vida Ya tanto
tiempo que no viajo en limusina Los sacrificios que hago por ti Antes de
vacaciones cuatro meses para Italia Con todo pago tres criadas y una nana Ahora
me quedo en la casita preparando de comida Era princesa pero no era feliz de esa
manera Y ahora contigo estoy mejor aunque no tenga dinero Porque yo me alimento
de tus besos Mi cielo, mi cielo No tengo dinero pero tengo amor No tengo dinero
pero tengo amor Bucci, Prada, Versace, Cristian, Dioria No son nada de digo La
paso bien sin ropa entre tu cama ¡Qué rico! Y ahora mis amigas ricas mueren de
la envidia Era princesa pero no era feliz de esa manera Y ahora contigo estoy
mejor aunque no tenga dinero Porque yo me alimento de tus besos Mi cielo, mi
cielo Era princesa pero no era feliz de esa manera Y ahora contigo estoy mejor
aunque no tenga dinero Porque yo me alimento de tus besos Mi cielo, mi cielo No
tengo dinero pero tengo amor No tengo diamantes pero tengo tu calor No tengo
dinero pero tengo amor Ya no tengo lujo pero tengo tu pasión No tengo dinero
pero tengo amor Tengo una vida llena de amor No tengo dinero pero tengo amor
Tengo amor, tengo amor Como yo nadie te quiere princesita, mamacita Tu boquita
de azucrita que me vuelve loco Me patina el co, lo que quieras que te de Cuando
quieras te lo doy Si tengo dinero yo te prometo que te compro el cielo Te lleno
de diamantes como tenías antes Pero eso no es pasar de dar de amor Era princesa
No tengo dinero pero tengo amor Ahora soy tu reina No tengo dinero pero tengo
amor Nada me falta porque yo me alimento de tus besos Mi cielo, mi cielo Mira
que manera de quererte Vengo caminando desde lejos para verte Solo para verte a
ti He venido yo aquí Tu carita con la mía Si cerquita cada día Y al amanecer
quiero verte y tenerte ahí Y comerte a besos Lo que quiero yo Me gustaría
compartir Mi vida con la tuya, mi nena Poder sentir como crece cada día Mi amor
por ti, mi amor por ti You can jazz it up, spice it up, dice it up Any way you
cut it I will sum it what you got You got the main cuisine Keeps me coming back
for seconds if you know what I mean Cause life does work in mysterious ways Look
at you, I'm amazed Can we make it better? De my day Y comerte a besos Lo que
quiero yo Me gustaría compartir Mi vida con la tuya, mi nena Poder sentir como
crece cada día Mi amor por ti, mi amor por ti Me gustaría compartir Mi vida con
la tuya, mi nena Poder sentir como crece cada día Mi amor por ti, mi amor por ti
Crece, crece como crece Antes eran doce y ahora trece Trece rosas traigo yo Para
celebrar mi amor No me digan que no te quiero Si por ti mamita yo me muero Hoy o
lo bien, hoy o lo bien Mi amor, mi amor Me gustaría Me gustaría Me gustaría Me
gustaría Me gustaría Me gustaría Me gustaría Me gustaría NO WAIT! 1600
Mijiquense Radio Ay lluvia me agüita Mi cielo esta gris Extraño a quien falta No
me hallo aqui Ayuhuey Ayuhuey Ayuhuey Ayuhuey Que sepan los ojos Que son pa'
mirar Estan perdidos De tanto extrañar Ayuhuey Ayuhuey Ayuhuey Ayuhuey Una vez
me encontre a un lobo Me hizo pensar que era el fin Yo le lami las heridas Me
volvi un lobo a mi Me volvi un lobo a mi Me volvi un lobo a mi Ay quiebra este
cielo Y venga el llorar Lo que anda marchito Reverde sera Ayuhuey Ayuhuey
Ayuhuey Ayuhuey Ayuhuey El aire se respira Huele a tierra moja Mi perro duerme a
mis pies El cuida de mi hogar El tiempo se para aquí Mi amor esta a punto de
llegar El tiempo se para aquí Aquí encuentro la paz Oh oh oh oh La curva de la
carretera Me invitan a viajar Hay tanto por recorrer Tanto por conocer El mapa
se hace pequeño Mi alma pide más Mi amor llega en la tortuga Y él me lo enseñara
Oh oh oh oh Ah ah ah Oh oh oh oh Ah ah ah Me enseñara La voz del mar Me enseñara
A no llorar Me enseñara a reconocer Que hay daños que te enseñan a crecer Me
enseñara a ver en sus ojos Aunque no esté Oh oh oh oh Ah ah ah Oh oh oh oh Ah ah
ah Me enseñara La voz del mar Me enseñara A no llorar Me enseñara a reconocer
Que hay daños que te enseñan a crecer Me enseñara a ver en sus ojos Aunque no
esté Oh oh oh oh Ah ah ah Ah ah ah Me enseñara La voz del mar Me enseñara A no
llorar Me enseñara La voz del mar Me enseñara a reconocer Me enseñara Me lloro
por quererte Por amarte, por desearte Me lloro por quererte Por amarte, por
desearte Ay cariño, ay mi vida Nunca, pero nunca Me abandones cariñito Nunca,
pero nunca Me abandones cariñito Me lloro por quererte Por amarte, por desearte
Me lloro por quererte Por amarte, por desearte Me lloro por quererte Por amarte,
por desearte Ay cariño, ay mi vida Nunca, pero nunca Me abandones cariñito
Nunca, pero nunca Me abandones cariñito Nunca, pero nunca Me lloro por quererte
Ay cariño, ay mi vida Me lloro por quererte Por amarte, por desearte Me lloro
por quererte Por amarte, por desearte Ay cariño, ay mi vida Nunca, pero nunca Me
abandones cariñito Nunca, pero nunca Me abandones cariñito Me abandones cariñito
Me abandones cariñito Me abandones cariñito Ah, ah, ah Tengo un gran recuerdo
aquí en mi corazón Pongo de una rumba buena que se está formando Marca mi
destino como el callejón Pasos y aguaceros voces que se van sumando Oh yeah
Traigo vino Oh yeah, de la brioja del camino Traigo vela Ay, de la que dura la
noche entera Traigo vino Vino, vino, vino, vino, vino Traigo vela Ay, ah, la
rumba buena Ah, ah, ah Ese sentimiento que me ha regalado la cuna de la raza
Mami, el conocimiento Bum, bum, sigue creciendo Tengo un gran recuerdo aquí en
mi corazón Chama, te lo estoy diciendo Regalando, comprimiendo Traigo vino Oh,
de la brioja del camino Traigo vela Ay, para abajo en la escalera Traigo vino
Vino, vino, vino, vino, vino Traigo vela Oh yeah, también traigo rumba buena A
ver Eh, eh, eh A ver Son los momentos, mira, de la infancia que recordaré A ver
Tanta mansión que compartí, tanta distra de carnaval A ver Regalos de la rumba
buena que mi alma necesita A ver Un beso de amor a la luz de la luna Me lo
suelto y margarita A ver Eh, eh, eh, ah, eh, eh, ah Suéltate, mami Que llegaron
los 70, mira Acabando Este sentimiento que me regaló la cuna de las razas Todas
el conocimiento Un pun, siga creciendo Tengo un gran recuerdo aquí en mi corazón
Nena, te lo estoy diciendo Ay, eh, eh, oh, do, do, do, do, do, do, do, do, do,
do, do, do, do Yo de baile como un cachamona Ay, yo tengo un amor que me quiere
en los pez Agua Sólo sé que tengo un amor Ay, rumba buena Carnaval Oh yeah Ay
Cuidale, bomba, tu vez Soy el último eslabón de la pirámide Desclasificado soy
el último eslabón que miraron del lado El corazón anclado, desparramado porque
nunca seré aceptado El tipo al límite el que nadie mide El que enseñaron que
nunca será libre Educarse para mí no es accesible Por más que el comercial diga
que todo es posible A veces una y a veces nada En donde quiebo que casaría mi
cara marcada Perdida la mirada, la vista hacia la ventana ¿Qué será lo que
define ella de mi tramo? Toqué todas las puertas La vida desfilaba en el borde
de esta vereda Me siento silenciado Yo soy el desclasificado Soy el último
eslabón de la pirámide Soy el último eslabón de la pirámide Subir para el año
toma tiempo tomar Los últimos de la fila luego serán primero Desclasificado soy
el último peón que cambiaron del lado El sujeto problema, objeto de dilema El
que no cabe en este esquema sigo pateando piedra el que sueña su manera Si me
preguntas soy de cemento y tierra ¿Cuál es mi clase? ¿Quién dicta las bases?
Corrido en el desfase ya que mi envase no clase Todo me delata mi pelo mi facha
¿Cuál es la justicia cuando siempre se detacha? Me siento silenciado Yo soy el
desclasificado Soy el último eslabón de la pirámide Soy el último eslabón de la
pirámide Esperé y esperé ya no sé si traje en el limbo Quizás fracase Esperé y
esperé ya no sé si traje en el limbo Quizás fracase Esperé y esperé ya no sé si
traje en el limbo Quizás fracase Esperé y esperé ya no sé si traje en el limbo
Quizás fracase Pirámide Soy el último eslabón de la pirámide Pirámide Pirámide
Cuíalo, mi amor Para pasar igual, igual Sumo aquí el fondo de sus letras, pasar,
sonar Vaminar su tumba Ya te puedes morir feliz Siempre así su sombra Lo viene a
buscar, buscar Vienen el reloj, yo tengo el tiempo Guardan la prisa, yo cuido el
momento Es la presión de mover deprisa De pensar sin verlo De pensar sin verlo
Sin moverte lento Mientras espero, tú me miras, yo te siento Mientras te miro,
tú me ves y yo te pienso Tienes los zapatos y yo camino lento Verano en la
arena, descalzo en el tiempo Ya lo intuía, todo está en tu risa Ahora sé lo que
no quiero El resto me lo quedo Mientras espero, tú me miras, yo te siento
Mientras te miro, tú me ves y yo te pienso Juraba esperarte a que tú me
rescatabas Del cozo de mentiras en el que me ahogabas Ya lo intuía, todo está en
tu risa Ahora sé lo que no quiero El resto me lo quedo Mientras espero, tú me
miras, yo te siento Mientras te miro, tú me ves y yo te pienso Mientras espero,
tú me miras, yo te siento Mientras te miro, tú me ves y yo te pienso Una botella
de vino, otra botella de más Todos borrachos de felicidad Y una canción que se
va Una botella de vino, otra botella de más Todos borrachos de felicidad Y una
canción que se va El pájaro abre su compuerta de hierro Cuando las ruedas toco
el suelo La gente que veo a través de la ventana No sabe lo que siento y me
encanta El bajo se encaje de olores La arquitectura mi vista La lengua hablada,
la temperatura sentida Y el deseo de sentir que hay vida Ciudad, después otra
ciudad Mañana tenés una más Mi corazón acá volverá Y una canción que se va
Ciudad, después otra ciudad Mañana tenés una más Mi corazón acá volverá Y una
canción que se va El pájaro abre su compuerta de hierro Cuando las ruedas toco
el suelo La gente que veo a través de la ventana No sabe lo que siento y me
encanta El pájaro abre su compuerta de hierro Cuando las ruedas toco el suelo La
gente que veo a través de la ventana No sabe lo que siento y me encanta El
pájaro encaje de olores La arquitectura mi vista La lengua hablada, la
temperatura sentida Y el deseo de sentir que hay vida Ciudad, después otra
ciudad Mañana tenés una más Todos borrachos de felicidad Y una canción que se va
Ciudad, después otra ciudad Mañana tenés una más Mi corazón acá volverá Y una
canción que se va Que se va ¡Venida, Estado de México Oriente 1601! Colonia
Llano Grande, Metepec Somos una estación del sistema mexiquense de medios
públicos Mexiquense Radio Aquí aprendemos y nos divertimos Código y Domingo 12
del día Código y ¿Sabías qué? Los riñones tienen el trabajo de filtrar las
toxinas que se encuentran en el organismo Produciendo la orina para eliminarlos
Es decir, son órganos vitales en forma de frijol Que procesan 190 litros de
sangre aproximadamente para eliminar cada día lo malo ¿Sabías qué? La herencia
de nuestros pueblos originarios Conexiones Domingo, 2 de la tarde Conexiones
Conexiones Estado de México Pasado y presente con identidad Nicolás Romero es un
municipio que alberga historia, tradición y cultura Como muestra se encuentra el
Teatro El Centenario En este lugar se practica música, danza, cine y se realizan
conferencias y asambleas La historia del séptimo arte la podemos encontrar en
las ruinas del cine Hidalgo Construido a principios del siglo XX, además en este
municipio se puede realizar la ruta truchera Para que visitantes y compradores
adquieran y consuman este alimento Según la cultura Nahuatl, Tlaloc, dios de la
lluvia, regía fenómenos meteorológicos como los relámpagos, truenos Y
frecuentemente se le asociaba a las cuevas La palabra agua viene del latín aqua,
originada de la raíz indoeuropea aqua, con el mismo significado Se conoce que a
través de huez, su otra raíz, se deriva la noción de humedad o mojado El agua
simboliza para los antiguos mexicanos el fundamento de la vida Ocupando su lugar
central en ceremonias y ritos, considerándola un elemento curativo Una razón,
una palabra