TRANSCRIPTION: MEXIQUENSE-RADIO_AUDIO/2023-03-20-16H00M.MP3
--------------------------------------------------------------------------------
 Quality music punto, o si, those y así E, e ése rá teu es que es el radio.
Estamostan La vuelta al disco. Recorridos alrededor de libros, música y cosas de
esta tierra. Porque un disco también es un mundo. 60 minutos para acercarnos
a... La vuelta al disco. La vuelta al disco. ¡Cámbis, hui, hui, jocó y nanque
nos dé! ¡Ni nanque nos dé! ¡Aleluya, aleluya! A la cancha, a la canilla de
puente arbaidando en peñado el plato de carne siempre a la salida mío ¿Cómo
están ustedes? Bienvenidos a la vuelta al disco aquí en Mexiquense Radio. Les
damos la bienvenida. Mi nombre es Lorena Romero Moreno. Me acompaña Carlos
Alberto Tapia en la cabina de transmisión al aire. Mariano Ramírez Robles en la
continuidad. Hoy es un día de asueto para algunas personas, pero aquí en
Mexiquense Radio tenemos un disco que ofrecerles. Iniciamos con una canción ya
tradicional, ya dentro del imaginario de estas canciones de arrullo que se les
cantan a las crías y se les procura el sueño a través de estos dulces arrullos.
Makochi Pitenzin es una recopilación que se hizo por el Instituto Nacional de
Antropología y Historia en 1980. Apareció un disco titulado Cantos de la
Tradición Náhuatl de Morelos y Guerrero. Un disco que tenía grabaciones de 1962
y 1968. Aparece también por el trabajo del güero gringo, el Gran Helmer, que
hizo estas grabaciones de campo. Después, en 2005, el Coro de los Pueblos
Indígenas, Yolotli, también hizo su propia versión. En 2013, también interpretó
este tema de manera bilingüe, dándole una gran proyección. Y ahora el Ensamble
de Tirambo la agrega a este disco de verdad sorprendente, que es el recetario
nuevo hispano, el Mole Nueva España, un modelo de mestizaje musical. Ensamble de
Tirambo que ha crecido a lo largo de ya varias décadas de trabajo,
principalmente en el Valle de Toluca, pero ha tenido ya un reconocimiento
importante dentro de esta música que nos recrea la historia de nuestro país y de
varios territorios a lo largo de estos siglos de colonialismo. Y la música
también es parte del mestizaje, así como lo es este platillo, que yo creo que
por antonomasia es el que representa cómo estos dos mundos se enfrentaron y
después de confrontarse también lograron tener algún tipo de diálogo, por lo
menos en el asunto gastronómico. El mole probablemente sea el alimento más
mestizo, el platillo más mestizo de nuestro país. Vamos a ir escuchando algunas
piezas, sí, de origen indígena, otras de compositores que venían de la Nueva
España, algunas leyendas también aquí, en la vuelta al disco, presentándoles el
recetario nuevo hispano, el Mole Nueva España, un modelo de mestizaje musical
con el ensamble de Tirambo. Vamos ahora con la leyenda del Huehuatl y el
Teponazle. Cuenta la leyenda que cuando murieron los dioses en Teotihuacán,
dejaron las mantas con que se cubrían a sus sacerdotes. Estos celosos servidores
de la religión guardaron como convenía la dignidad y honra de sus antiguos
poseedores las sagradas reliquias. Labraron unos trozos de madera, haciéndoles
una muesca en un extremo en el que colocaron las preciosas joyas. Cubrieron los
compieles de serpiente y de tigre y por último los envolvieron con las sacras
vestiduras. Llenos de pesar, con el corazón oprimido, dieronse a caminar
invadidos de profunda tristeza. Y erraron, llevando en alto los divinos ropajes,
temerosos de mancharlos con el polvo del camino o con el sudor de sus cuerpos,
respetuosos de la memoria de sus divinos muertos. Uno de ellos llegó a la orilla
del mar, se tendió en la arena, cansado de su continuo peregrinar, y allí
permaneció varios días. Habiéndole ha hablado tezcaltlipoca, instruyéndole para
que fuera a pedirle al Dios Sol cantores e instrumentos musicales para honrar el
recuerdo de los dioses muertos y mitigar un poco sus penas. Y el sacerdote se
dispuso a hacer el viaje a la morada del sol. Formaron en las tortugas, las
ballenas y los manatíes un puente sobre el mar por el que caminó el afligido
creyente, llamando con enternecedoras y dulces palabras. Llegó a la morada del
sol y le expuso el motivo de su viaje. El sol, no queriendo disminuir su corte,
había prevenido a todos sus acompañantes a que no contestasen a la que habían
escuchado, so pena de arrojarlos a la tierra. Pero obró tal efecto el meliflo o
decir del mensajero que hubo quien no pudiera resistir a la tentación de
contestarlo, cayendo bajo la sanción del divino mandato. Y fueron arrojados de
la presencia del sol dos personajes, el uno, Huehuetl, el otro, Teponaztl, que
vinieron a este planeta en compañía del devoto peregrino. Desde entonces tienen
música los hombres en la tierra, una música triste y melancólica, como que era
producida por los ayes de dolor que a cada golpe arrancaba a los señores de la
corte del sol, que no supieron cumplir con una orden divina. Hoy aquí en La
Vuelta al Disco escucharemos algunas leyendas, algunos poemas, algunos guiños a
recopilaciones históricas dentro de esta propuesta que hace el ensamble de
Tirambo en el recetario novo hispano, el mole, Nueva España, un modelo de
mestizaje musical, y también pues el recetario de la presencia y el crédito
estudiosos de la música y la historia, como es en el caso de la leyenda del
Huehuetl y del Teponaztli, de Gabriel Saldívar. También oímos una danza
prehispánica del maíz acompañando esta leyenda. Y ahora vamos a ir con una de
las partes de la poesía prehispánica. Uno de los grandes estudiosos, un maestro
de maestros, es justamente Miguel León Portilla, que dedicó prácticamente toda
su vida a indagar los imaginarios culturales, religiosos, los alcances, los
ríos, los ríos, los imaginarios culturales, religiosos, los alcances también de
la filosofía náhuatl y de este periodo histórico que es tan importante para
entender la identidad del México actual antes y después de que llegaran los
españoles. Vamos a oír un texto titulado de Charles Chewitl, que es de el poeta
rey de Texcoco. Y también escucharemos música, un anónimo de esa época aquí en
La vuelta al disco a través de Mexíquense Radio. Porque un disco también es un
mundo. Chiquiye huay sachi Chiquiye huay panujo hola Panpanimistra sotla
Chiquiye huay sachi Chiquiye huay panujo hola Panpanimistra sotla Panpanimistra
sotla Ikanujo hola Chiquiye huay sachi Chiquiye huay panujo hola Panpanimistra
sotla Ikanujo hola Panpanimistra sotla Ikanujo hola Panpanimistra sotla
Panpanimistra sotla Continuamos aquí en La vuelta al disco a través de
Mexíquense Radio. Escuchamos este texto convertido en canción a través del
ensamble de Tirambo. Chiquiye huay sachi Que dice algo así como guarda esta flor
en tu corazón porque te amo, porque te amo con todo mi corazón. Estamos
escuchando el recitario nuevo hispano, el mole Nueva España, un modelo de
mestizaje musical a cargo justamente del ensamble de Tirambo que bueno ha tenido
varias alineaciones pero en este disco participa Rosalba Mancilla como soprano,
soprano también Selena Martínez, Tatiana Burgos Alto, Gabriel Díaz, contratenor,
Rafael Vega, tenor, Daniel Vega, barítono, Enrique Martínez, violín barroco,
Paulina Serna viola da gamba, Luis Reyes el violín y también Aura Martínez en el
clavecín. Luis Reyes corrijo violone que debe de ser algún tipo de instrumento
con ciertas particularidades, Horacio Nájera en las percusiones y la leona, Yura
Vivero la jarana, las flautas y también participa en la voz, su voz de tenor,
Daniela Salazar actriz porque también recuerden que estamos escuchando algunas
leyendas y también oiremos algunos poemas pero por lo pronto vamos con una pieza
que es del maestro de capillas, Hernando Franco, él nació en España en 1532,
recordemos que Colón llegó a este nuevo continente en 1492, en 1519 Hernán
Cortés pues arribó a las tierras veracruzanas y la caída de Tenochtitlan fue en
1521, en 1532 estaba naciendo Hernando Franco y se trasladó a través del
Atlántico a esta nueva tierra donde murió en 1585, les decía que fue maestro de
capillas en México y también en Guatemala, vamos a escucharlo después nos vamos
al corte y volvemos. En el Hígrica, en el Hígrica, en el Hígrica, en el Hígrica,
en el Hígrica, en el Hígrica, Jesucristo. Dios se imponente, se me pate exposti,
se muere mi cabello exposti. Se me pate exposti, se me pate exposti, se me pate
exposti, inilvica, expansinco, imotra sodolecin, Jesucristo. Camba de colesita,
el tal disco. México es radio. En este periodo electoral, la Fiscalía General de
Justicia del Estado de México, a través de la fiscalía especializada en materia
de delitos electorales, te brinda atención en caso de ser testigo o víctima de
un delito electoral. Ofrecerte dinero a cambio de tu voto, recoger tu credencial
para votar, condicionar tu voto mediante cualquier presión o amenaza. Son sólo
algunos de ellos. Infórmate. Cualquier anomalía, repórtalas al 01800 70 28 770
en nuestras redes sociales o en la aplicación móvil FGJDomex, disponible de
manera gratuita. Soy capacitadora asistente electoral y quiero contarte que en
México son las y los ciudadanos quienes reciben y cuentan los votos de sus
vecinos. Por eso ya estamos recorriendo Estado de México para invitarte a
participar como funcionaria y funcionario de Casilla. Las y los capacitadores
estamos uniformados y nos identificamos con credenciales oficiales del INE.
Participa y ábrele la puerta a la democracia. Mi INE es valioso porque nuestro
INE nos une. El 4 de junio participa. INE. Desde hace 32 años el INE ha
organizado más de 330 procesos electorales para elegir a quienes nos gobiernan y
este ha sido el resultado después de cada elección. La paz pública. Gracias a la
ciudadanía y al INE, hemos tenido elecciones libres, auténticas y pacíficas.
Elecciones sin fraude porque hay reglas claras. Elecciones donde podemos elegir
en libertad y sin presión. La paz hay que cuidarla. El voto libre no se toca. Mi
INE es valioso porque mi INE nos une. Según la UNESCO, el Día Mundial de la
Poesía es una ocasión para honrar a los poetas. Revivir tradiciones orales de
recitales de poesía, promover la lectura, la escritura y la enseñanza. Es por
eso que el 21 de marzo se celebra el Día Mundial de la Poesía con el objetivo de
reconocer y fomentar la diversidad lingüística. La vida es la que nos da la fe,
la vida es la que nos da la fe, la vida es la que nos da la fe, la vida es la
que nos da la fe, el corazón es el que nos da la fe, el corazón es el que nos da
la fe. Poema en Otomí. Raíces Profundas. 21 de marzo. Día Mundial de la Poesía.
Legado Cultural de la Humanidad. Mexiquense Radio. Es. Cuentista, novelista y
etnólogo. Estudió artes plásticas en la Academia de San Carlos. Por su labor en
pro de la indígena Alfonso Fabiola Montes de Oca, fue conocido como el apóstol
del indio. Asimismo formó parte del fugaz movimiento literario denominado el
aforismo. Por su obra narrativa se le considera como uno de los precursores de
la literatura indigenista, cuya obra se condensa en novelas y libros de cuentos
entre los que se encuentran. Sangre de mi sangre, Los brazos en Cruz, seis
cuentos mexicanos de la revolución, Aurora Campesina y Entre la tormenta. Del
municipio de Amanalco, Alfonso Fabiola Montes de Oca forma parte de nuestras.
Bellas artes, mexiquenses. La vuelta al disco. Seguimos aquí en Mexiquense Radio
en La vuelta al disco, compartiendo con ustedes algunas de las piezas de este
recetario nuevo hispano, el mole. Nueva España, un modelo de mestizaje musical,
un disco que sacara recientemente el ensamble de tirambo, pero que ya tiene un
largo recorrido incluso en algunos festivales allá en España, festivales de
música antigua, donde pues han ganado reconocimiento. Vamos ahora con una
leyenda que yo creo que es una de las más famosas e importantes. Hay varias
versiones, algunas que datan de la época prehispánica, algunas otras ya situadas
en la colonia y también ha llegado al cine. Seguro saben que estamos hablando de
la llorona. Los cuatro sacerdotes aguardaban expectantes. Sus ojillos vivacios
iban del cielo estrellado en donde señoreaba la gran luna blanca, al espejo
argentino del lago de Texcoco. De pronto estalló el grito. ¡Vuelve conmigo!
¡Vuelve conmigo! Era un sonido lastimoso, hiriente, sobrecogedor, un sonido
agudo como escapado de la garganta de una mujer en agonía. El grito se fue
extendiendo sobre el agua y rebotando contra los montes y enroscándose en las
alpardas y los taludes de los templos y pareció quedar flotando en el
maravilloso palacio del entonces emperador Moctezuma Sokojutsu. Es Ihuacoatl, la
diosa a salido de las aguas y bajado de la montaña para prevenirnos nuevamente,
exclamó el interrogador de las estrellas y la noche. Subieron al lugar más alto
del templo y pudieron ver hacia el oriente una figura blanca arrastrando una
cauda de tela vaporosa que jugueteaba con el fresco de la noche de Nirona.
Cuando se hubo apacado el grito y sus setos se perdieron a lo lejos, todo quedó
en silencio. Sombras ominosas huyeron hacia las aguas hasta que el pavor fue
roto por algo que los sacerdotes interpretaron de este modo. Hijo míos, amados
hijos de la Nahuac, vuestra destrucción está próxima. ¿A dónde iréis? ¿A dónde
os podré llevar para que escapéis a tan funesto destino? Hijo míos, estáis a
punto de perderos. Al oír estas palabras, los cuatro sacerdotes estuvieron de
acuerdo en que aquella aparición que llenaba de terror a la gente era la misma
diosa Ihuacoatl, la deidad protectora de la raza, aquella buena madre que había
heredado a los dioses para finalmente depositar su poder y sabiduría. El
emperador Moctezuma solo miraba con asombro a los códices multicolores hasta que
los sacerdotes le interpretaron lo allí escrito. Señor, le dijeron, estos viejos
anuales nos hablan de que la diosa Ihuacoatl aparecerá para anunciarnos la
destrucción de vuestro imperio, que hombres extraños vendrán por el oriente y se
juzgarán a tu pueblo y a ti mismo, y tú y los tuyos serán de muchos lloros y
grandes penas, que tu raza desaparecerá devorada y nuestros dioses humillados
por otros dioses más poderosos. ¿Dioses más poderosos que nuestro dios
Huichilopostli y que el gran destructor de Escalclipoca? preguntó Moctezuma
bajando la cabeza con temor y humildad. Así lo dicen los sabios, señor, por eso
la diosa Ihuacoatl vaga por Lanahuac, lanzando lloros y arrastrando penas,
gritando las desdichas que han de llegar a vuestro imperio. Al llegar los
españoles a iniciar la conquista, según cuentan los cronistas de la época, una
mujer igualmente vestida de blanco, con las negras crines tremolando al viento
de la noche plenirunar, cruzaba calles y plasuelas como el impulso del viento,
deteniéndose ante las cruces, templos y cementerios para lanzar ese grito que
hería el alma. ¡Ay, mis hijos! ¡Ay, mis hijos! El lamento se repetía tantas
veces como horas tenía la noche, en que la dama de vestiduras vaporosas se
detenía en la plaza mayor, y mirándose a la catedral musitaba una larga y
doliente oración para volver a levantarse, lanzar de nuevo su lamento y
desaparecer sobre el lago que entonces llegaba hasta las goteras de la ciudad.
¡Ay, mis hijos! Continuemos aquí, en la vuelta al disco, estamos presentando el
recetario nuevo hispano, el mole Nueva España, un modelo de mestizaje musical,
un disco que tiene en total 70 minutos y 58 segundos, vamos a tratar de escuchar
lo más posible. Y enseguida, este son oaxaqueño de la región del Istmo, que es
tan conocido, que es La Llorona, yo creo que es una de las piezas fundamentales
de este disco que les presentamos hoy aquí en Mexiquense Radio, integra por
supuesto el son de La Llorona, también hay un fandango para violín y danza
ibérica, hay un fandanguito, un jarocho, fandango para clavecín y danza, una
sandunga que es también un sonismeño, la petenera que pertenece a la tradición
jarocho que es igual que el cascabel, así que vamos a escucharla aquí en la
vuelta al disco. Amoni, la hadit, lechibian, chucani, insocet, meteo, tralpan.
Tleni, cuarín, ocalí, chucani, no sé qué tienen las flores, llorona, las flores
del campo santo. Que cuando las mueve el viento, llorona, parece que están
llorando. Dos besos llevo él en el alma, llorona, que no se apartan de mí. El
último de mi madre, llorona, y el primero que te di. Señores, ¿qué sonés este?
Señores, el fandanguito, la primera vez que lo oigo, pálgame Dios, qué bonito.
Señores, el fandanguito, la primera vez que lo oigo, pálgame Dios, qué bonito.
Señores, ¿qué sonés este? Señores, el fandanguito, la primera vez que lo oigo,
pálgame Dios, qué bonito. Señores, el fandanguito, la primera vez que lo oigo,
pálgame Dios, qué bonito. Señores, el fandanguito, la primera vez que lo oigo,
pálgame Dios, qué bonito. Ante noche fui a tu casa, resolves de día el cantado,
tú no sirves para amores, tienes el sueño pesado. Ay, sandukta, sandukta, mamá
por Dios, sandukta, no seas sin trata, mamá de mi corazón. La pendetera,
señores, es una mala mujer, es una mala mujer, la pendetera, señores, quien
pende sus amores siempre habrá de padecer, siempre habrá de padecer desgrichas y
sin sabores. Ay, soledad, soledad de cerro en cerro, todos tienen sus amores y a
mí que me guarda un perro. La pendetera, señores, es una mala mujer, es una mala
mujer, la pendetera, señores, quien pende sus amores siempre habrá de padecer,
siempre habrá de padecer desgrichas y sin sabores. ¿Qué onda? Yo tenía mi
cascabella, yo tenía mi cascabella, pon una cinta dorada, yo tenía mi
cascabella. Mi amigo, tu cascabella, mi vida ni alguien te lo dio, mi vida ni
alguien te lo dio, mi vida ni alguien te lo dio, mi vida ni alguien te lo dio,
mi vida ni alguien te lo dio. Mi vida ni alguien te lo dio, mi vida ni alguien
te lo dio. Mi vida ni alguien te lo dio, mi vida ni alguien te lo dio. Mi vida
ni alguien te lo dio, mi vida ni alguien te lo dio. Mi vida ni alguien te lo
dio, mi vida ni alguien te lo dio. Mi vida ni alguien te lo dio, mi vida ni
alguien te lo dio. Mi vida ni alguien te lo dio, mi vida ni alguien te lo dio.
Mi vida ni alguien te lo dio, mi vida ni alguien te lo dio. Mi vida ni alguien
te lo dio, mi vida ni alguien te lo dio. Mi vida ni alguien te lo dio, mi vida
ni alguien te lo dio. Mi vida ni alguien te lo dio, mi vida ni alguien te lo
dio. Antes de 1996 no existía un mecanismo de defensa para proteger los derechos
político-electorales de la ciudadanía. Hace 25 años no existía un tribunal
electoral en el Estado de México. Hoy somos toda una institución. En todo este
tiempo han pasado muchas cosas y en el TEM seguiremos escribiendo historia.
Sabías que el estrato volcán Agung es la montaña más alta y sagrada de Indonesia
y aunque se encuentra en erupción de manera casi continua, su última gran
erupción fue en 1963, la cual ha sido una de las más devastadoras en su
historia, ya que esta produjo una peligrosa lluvia de ceniza y flujos
piroclásticos, además de haber durado 11 meses. Hemos escuchado el más reciente
de Ensamble de Tirambo que fue editado por Lindoro en 2022. Todavía hay
conciertos que está dando esta agrupación, así que si los ven anunciados no se
lo pierdan porque de verdad vale mucho la pena. Ya escuchamos piezas musicales
de Hernaldo Franco, este maestro de capilla que nació y vivió durante el siglo
XVI y también hemos escuchado en el fondo musical a la jacara de Juan Gutiérrez
de Padilla, un compositor nacido en 1590 en Málaga y fallecido en Puebla en 1664
ya en el siglo XVII y en la catedral de Puebla justamente los estudiosos
encontraron sus partituras y se pudo recrear su música. También hemos escuchado
algunas participaciones que tienen que ver con leyendas y también con la
recopilación de la poesía náhuatl. Este disco se llama Recetario Novo Hispano,
el mole, Nueva España, un modelo de mestizaje musical y dirán ustedes ¿y cuándo
vamos a oír hablar del mole? Enseguida. Cuenta la leyenda que en una ocasión
Juan de Palafox, virrey de la Nueva España y arzobispo de Puebla, visitó un
convento poblano donde le ofrecieron un banquete. La comunidad religiosa puso
todo su esmero para que la comida estuviera deliciosa. Fray Pascual, el cocinero
principal, se sentía especialmente nervioso y regañaba a sus ayudantes ante la
inminente visita del virrey. Porque un disco también es un mundo. Debido a la
premura y a la desesperación, Fray Pascual comenzó a apilar en una charola todos
los ingredientes que habían en la cocina con el fin de guardarlos en la lacena.
Tanta era su prisa que tropezó frente a la cazuela donde unos guajolotas estaban
siendo hervidos para el banquete. Chiles, pedazos de chocolate, galletas y demás
especias fueron a parar a la cazuela, echando a perder la comida que debía
ofrecerse al virrey. Fue tanta la angustia de Fray Pascual que comenzó a rezar
con toda su fe para ver si podía solucionar su desafío. En ese momento le
avisaron que los comensales estaban sentados a la mesa y degustando los
alimentos. Cuando terminó el banquete, el virrey mandó llamar a Fray Pascual. El
sacerdote angustiadísimo pensó que lo reprenderían, pero cuando llegó a la mesa
de los comensales lo felicitaron enormemente por el delicioso platillo. Se dice
que en honor a ese episodio, en algunos pueblos permanece la costumbre entre las
amas de casa de invocar al frayle con la siguiente frase. ¡San Pascual Bailón,
atiza mi fogón! San Pascual Bailón, atiza mi fogón. Llegamos al final de esta
vuelta al disco, la verdad es que no nos ha dado tiempo de escuchar completo
este recetario nuevo hispano, el mole. Nueva España, un modelo de mestizaje
musical del ensamble de tirambo. Se lo recomendamos muchísimo y nos vamos a
despedir. Un saludo a todos los que han estado aquí y que han estado aquí. Se
llama Esorrigo Repente. Muchas gracias por habernos escuchado. Nos escuchamos
próximamente. Así es que quédense aquí en Mexíquense Radio. ¡Ah, señor Landlea!
¡Ah, señor Landlea! ¡Ah, señor Landlea! ¡Ah, señor Landlea! ¡Ah, señor Landlea!
¡Ah, señor Landlea! ¡Ah, señor Landlea! ¡Ah, señor Tomé! ¿Tenemos guitarra?
¡Guitarra tenemos! ¿Sabemos tocaya? ¡Tocaya sabemos! ¿Qué me contás? ¡Tocay!
Pues vamos duro a Belén y al andioso que es Ayoranda, le cantémosle a la banda.
¡A ver, sobré! ¡Y a mi también! ¡Tocá plimo, coltupe! Así, así que el ope se me
anda. Así, así que me bulle el ope. ¡Cantémosle al Redentor, la bienvenida y
llegara! ¡Santo ronca y resfriara! ¡Cantaremos mal, señoles! ¡Résipe de la
metoli, porque tengamos voz clara! ¿De botica una cucharada? ¿Cuánto basta su
PC? ¡Vale, si bebé! ¡De los blancos nos guardemos que tosemos a los villacos!
Deberé tomar el tabaco, o tanto anhelo tosemos. A López se brellejemos y a los
son de tromentillo, guitarrilla y banderillo hagamos fiesta en Belén. ¡Parece
Belén! ¡Y a mi también! ¡Tocá plimo, coltupe! Así, así que el ope se me anda.
Así, así que me bulle el ope. De esos rigores, repente, juro a quince niños y
hitos, que aunque nace poco, braquito, todo somos nosotros parientes, no tenemos
branco grande. ¡Tenle, primo, tenle, caliente! ¡Usi, usi, aparacia! ¡Tú,
canegrillo, tú canegrillo tan bonitillo! ¡Tanta paredes! ¡Solo abandate entre
creces! ¡Solo abandate entre creces! ¡Solo abandate entre creces! ¡Sopaca su
cucu, me cúbume! ¡Es el oche branco semeo! ¡Oh, qué su que risa te dimo! ¡Oh,
qué risa, santo tuve! ¡Papá negro de Ibeano, que se brito sola! ¡Nos vamos negro
de colate, sacudo de glaceo! ¡Queremos que mi hijo vea negro, puliso y galano!
¡De robo, solo serrano, tenle, voy al palazzo así! ¡Con camilla, no hay corilla,
baila, no hay robo, libremente! ¡La cantilla regra, no te llega, pa' los
equidillo! ¡Mandele, arrebo, si no confite, curva, samba te! ¡Y lectura, te
fasciuela, bante en la misa! ¡La pisa y nada frisa acá! Recorridos alrededor de
libros, música y cosas de esta tierra. La Vuelta al Disco. ¡Avenida Estado de
México Oriente 1601! ¡Colonia Llano Grande! ¡Metepec! Somos una estación del
sistema mexiquense de medios públicos. Mexiquense Radio Este 2023, aprovecha el
subsidio a la tenencia y paga solo el refrendo. Si estás o te pones al corriente
en el pago de tu tenencia y tu auto tiene un valor factura de hasta 400 mil
pesos sin IVA, o tu motocicleta, un valor factura de hasta 115 mil pesos sin
IVA, paga solo el refrendo. Obtien tu formato de pago en el portal de servicios
al contribuyente. Cuentenencia.edomex.gov.mx. También puedes pagarlo en línea.
Tienes hasta el 31 de marzo. Informate al 800-715-4350 y 722-226-1751. Edomex.
Decisiones firmes, resultados fuertes. Estado de México. Pasado y presente con
identidad. Erigida por fray Felipe de Jesús Cueto, la parroquia del divino
Salvador es otro de los atractivos históricos con los que cuenta el municipio de
Atizapán de Zaragoza, ya que esta construcción virreinal es de gran importancia
tanto arquitectónica como artística para la región, debido a que con casi 400
años es el templo más antiguo de esta localidad, cuya fiesta en honor a su santo
patrono se lleva a cabo el 6 de agosto, fecha en la que este templo se llena de
flores y los feligreses asisten a rendirle culto. Cainström