TRANSCRIPTION: NDR-INFO_AUDIO/2023-04-07-10H00M.MP3
--------------------------------------------------------------------------------
 Hier sind NDR Info und WDR 5 angeschlossen Bremen 2. Aus der St. Nathanus-
Kirche in Schöppenstedt übertragen wir einen evangelischen Gottesdienst. Die
Predigtät Landesbischof Christoph Mainz. Die kirchliche Leitung hat Pastor
Oliver Vorwald. Er führt auch in den Gottesdienst ein. Karfreitag. Die
Totenglocke beklagt den Kummer dieser Welt. Sie allein. Wie ein Mensch in seiner
Trauer. In dieser Welt ist jeden Tag irgendwo Karfreitag. Menschen weinen über
das Leid. Das eigene, das ihrer Kinder. Und das Stabat Marta fasst den Schmerz
in Worte. Ein mittelalterliches Gedicht, komponiert auf Latein. Stabat Marta
Dolorosa. Es stand die Mutter schmerzerfüllt unter dem Kreuz des Sohnes. Es sind
zehn Strophen, vielfach vertont, immer wieder neu. Weil der Kummer der Mütter
über das Leid ihrer Kinder immer wieder neu ist. Willkommen zum
Radiogottesdienst. Wir sind in St. Stefanus in Schöppenstedt. Eine Stadt östlich
von Braunschweig. Barockes Schiff, romanischer Turm. Mit dabei ist heute die
Kantorei. Auf den Lippen das Stabat Marta von Josef Gabriel Rheinberger. Seine
Kantate wird später in diesem Gottesdienst zu hören sein. Zum Eingang das
Choralforspiel Olam Gottes unschuldig vom Theophil Forchhammer. Olam Gottes
unschuldig vom Theophil Forchhammer. Olam Gottes unschuldig vom Theophil
Forchhammer. Olam Gottes unschuldig vom Theophil Forchhammer. Olam Gottes
unschuldig vom Theophil Forchhammer. Olam Gottes unschuldig vom Theophil
Forchhammer. Olam Gottes unschuldig vom Theophil Forchhammer. Olam Gottes
unschuldig vom Theophil Forchhammer. Olam Gottes unschuldig vom Theophil
Forchhammer. Olam Gottes unschuldig vom Theophil Forchhammer. Willkommen zum
Gottesdienst am Karfreitag. Wir werden uns nie daran gewöhnen, an dieses Bild
vom Karfreitag. An die Einsamkeit Jesu am Kreuz. An unsere Einsamkeit und
Gottverlassenheit angesichts dieses Anblicks. Das Leiden damals wie heute. Die
unzähligen Kreuze in dieser Welt, in der Ukraine und in Russland. Die Trümmer in
der Türkei und in Syrien, die Abertausende unter sich begraben haben. Die
Gräberfelder an anderen Orten dieser Welt, zu Lande, zu Wasser. Wir werden uns
nie daran gewöhnen, an dieses Bild von Karfreitag. Also hat Gott die Welt
geliebt, dass er seinen eingeborenen Sohn gab, damit alle, die an ihm glauben,
nicht verloren werden, sondern das ewige Leben haben. Im Namen des Vaters und
des Sohnes und des Heiligen Geistes. Amen. Wir singen nun, Herr stärke mich,
dein Leiden zu bedenken. Zu finden im evangelischen Gesangbuch unter der Nummer
91, die Strophen 1, 5 und 8. Herr stärke mich, dein Leiden zu bedenken. Mit in
das Herde Liebe zu versenken, die dich beruflich aller schönes Bösen und zu
erlösen. Sieb ich dein Freund, den du gebiesen hättest, heilender Wiss und
deiner Leut werden, Todesreis und mir trotz deiner Fretenschrottes, jeweils ein
Gottes. Ich will dich fast mit Leid erpassen, wenn man mich nicht mit reißen
Füßen schelte. Du, Heilige Herr und du, Herr und Haupterbieter, schreist auch
nicht wieder. Wir beten mit Worten des Psalms 22 im evangelischen Gesangbuch
unter der Nummer 709. Wir beten den Psalms im Wechsel. Ich beginne und ich bitte
Sie, die eingerückten Verse zu sprechen. Mein Gott, mein Gott, warum hast du
mich verlassen? Ich schreie, aber meine Hilfe ist ferne. Aber du bist heilig,
der du drohenst über den Lobgesängen Israels. Unsere Frege erhofften auf dich
und nach dem Hoffen reißt du die Linien heraus. Zu dir schrien sie und wurden
errettet. Sie hofften auf dich und wurden nicht zu Schanden. Sei nicht ferne von
mir, den Angst zu zahlen, der lässt sich schwerer ein Helfer. Aber du Herr, sei
nicht ferne, meine Stärke eile, mir zu helfen. Ehre sei dem Vater und dem Sohn
und dem Heiligen Geist, wie es war im Anfang, jetzt und immer da und von
Ewigkeit zu Ewigkeit. Amen. Kiel, der Leis war. Kiel, der Leis war. Kiel, der
Leis war. Kiel, der Leis war. Kiel, der Leis war. Christe, der Leis war.
Christe, der Leis war. Christe, der Leis war. Christe, den Leis war. Christe,
der Leis war. Christe eleison. Christe eleison. Christe eleison. Christe
eleison. Christe eleison. Christe eleison. Christe eleison. Christe eleison.
Christe eleison. Christe eleison. Christe eleison. Christe eleison. Christe
eleison. Christe eleison. Christe eleison. Christe eleison. Christe eleison.
Christe eleison. Christe eleison. Christe eleison. Christe eleison. Christe
eleison. Christe eleison. Christe eleison. Christe eleison. Christe eleison.
Christe eleison. Liebe lässt die Weise und ihr Alles trüb. In der Steigerlobe
auf der Saarertoum und naherst der Kanger in der Stürmung trau'n. Himmim, wie
nahe, befindet Aubergine, wie möchtest du die Weise und ihr Alles trüb. Als aber
die Soldaten Jesus gekreuzigt hatten, nahmen sie seine Kleider und machten vier
Teile. Für jeden Soldaten ein Teil, dazu auch das Gewand. Das war aber ungenäht,
von oben angewebt, in einem Stück. Da sprachen sie untereinander. Lasst uns das
nicht zerteilen, sondern darum losen, wem es gehören soll. So sollte die Schrift
erfüllt werden, die sagt, Sie haben meine Kleider unter sich geteilt und haben
über mein Gewand das Los geworfen. Das taten die Soldaten. Es standen aber bei
dem Kreuz Jesu seine Mutter und seiner Mutterschwester Maria, die Frau des
Klopas und Maria von Magdala. Als nun Jesus seine Mutter sah und bei ihr den
Jünger, den er lieb hatte, sprichte er zu seiner Mutter. Frau, siehe, das ist
dein Sohn. Danach sprichte er zu dem Jünger. Siehe, das ist deine Mutter. Und
von der Stunde an nahmen sie der Jünger zu sich. Danach, als Jesus wusste, dass
schon alles vollbracht war, spricht er, damit die Schrift erfüllt würde. Mich
dürstet. Da stand ein Gefäß voll Essig. Sie aber füllten einen Schwamm mit Essig
und steckten ihn auf ein Ysebror und hielten es ihm an den Mund. Als nun Jesus
den Essig genommen hatte, sprach er. Es ist vollbracht. Und neigte das Haupt und
verschied. Da stand ein Gefäß voll Essig, die am Atemung in dir liegt. Träum'
beredert, weltverlebt, die am Atemung in dir liegt. Na, sieb' Brüder sind in
dir. Dieses Baubuch, wie uns fliegt, mag für Christlichsieb' in die Welt. In
Tantus, und Visinus, qu'ils usus et condis tarin, Christi, Mathe, und
Kontemplarin, un ente confidino. Pro peccatis, o e gentis, fide Jesu, victor,
mellus, et maris supitum, visum pulsenatum, du emis et spiritum. Lade sei mit
euch und Friede von Gott, unserem Vater und dem Herrn Jesus Christus. Amen.
Liebe Gemeinde, zwischen der Passionsgeschichte aus dem Johannes Evangelium und
der Musik, die wir gerade gehört haben, besteht ein eigenartiger Kontrast. Das
Stabat Marte von Josef Reinberger ist voller Schwere. Mit einer ersten Reihe
absteigender Töne, mit Reibungen, Dissonanzen und Mollklängen nimmt uns das Werk
hinein in die Trauer Marias über ihren sterbenden Sohn. Für sie wie für seine
Jünger bricht eine Welt zusammen. Mit Jesus sollte doch ein neues, wunderbares
Reich des Friedens und der Gerechtigkeit anbrechen. Mit ihm sollte sich der
Geist der Liebe und der Mitmenschlichkeit ausbreiten, ohne Unterdrückung, ohne
Unrecht, ohne Sünde, ohne Krankheit, ohne Tod. Aber es kommt anders. Die
Mächtigen seines Volkes lassen Jesus als Aufrührer verhaften und foltern. Danach
übergeben sie ihn an den römischen Präfekten Pontius Pilatus. Der verurteilt ihn
zum Tode und lässt Jesus am Kreuz hinrichten. Die Passionsgeschichte aus dem
Johannes Evangelium dagegen, die wir eben als Lesung gehört haben, erzielt den
Tod Jesus anders als den letzten Gang eines erhabenen Königs, der sich ganz in
den Dienst seines Volkes stellt und dafür alles gibt, und am Ende sogar sein
Leben. Er stirbt, aber sein Opfer ist nicht vergebens. Johannes beschreibt Jesus
als einen solchen Herrscher, seiner wichtigen Rolle voll bewusst, Gott ergeben
mit großer innerer Stärke. Kurz vor seinem Tod schaffte er es sogar noch, die
Verantwortung für seine Mutter an einen Jünger zu übergeben, damit sie gut
versorgt ist. Mich berührt es sehr, wie Jesus vom Kreuz herab zu seiner Mutter
sagt, Frau, siehe, das ist dein Sohn, und zu seinem Jünger, siehe, das ist deine
Mutter. Es sind Worte voller Liebe und Fürsorge. Am Ende kann er mit großer
Souveränität sagen, es ist vollbracht. Worte voller Gewissheit und
Gottvertrauen. Zusammenbruch und Vollendung, Leiden und Liebe, Trauer und Trost
sind am Karfreitag eng miteinander verwoben. Für mich heißt das an diesem Tag,
ich gehe hinein in die Passionsgeschichte. Ich stelle mich unter das Kreuz und
lasse die Erinnerung an das zu, was mich belastet. Die Schmerzen und das Leid,
die ich erfahren habe, die Krisen, die ich erleben musste, die Konflikte, die
ich nicht lösen konnte, das, was mir aktuell zu schaffen macht, meine
Erinnerungen an Schuld und Versagen, meine Erfahrungen mit Sterben und Tod,
meine Gefühle der Überforderung und der Ohnmacht angesichts der aktuellen
politischen, wirtschaftlichen und sozialen Krisen. Und ich stelle mich den
Fragen, die in meinem Kopf herumschwirren. Wie wird sich Europa durch den Krieg
in der Ukraine verändern? Was wird das alles bedeuten für mein Leben und für das
Zusammenleben in unserem Land? Wie kann der Wandel hin zu einer neuen
Lebensweise angesichts des Klimawandels überhaupt gelingen? Was wird da alles
noch auf mich? Was wird da alles noch auf uns zukommen? Gleichzeitig spüre ich
die tiefe Liebe und Zuwendung in dem, was Jesus zu seiner Mutter und seinem
Jünger sagt. Das tröstet, das trägt und stärkt mich sehr. Und ich höre darin
eine tiefe Wahrheit. Die Liebe bleibt. Worte des Mitgefühls bleiben. Taten
bleiben, die von sich absehen und anderen geben, was sie brauchen, damit es
ihnen gut geht. Und ich sehe darin einen Weg, der weiterführt. Egal, was
geschieht, lasst uns niemanden zurücklassen. Lasst uns aufeinander achten. Lasst
uns zusammenbleiben als Familien, als Freunde, als Nachbarn, als Menschen. Lasst
uns sorgen für Schwerstkranke, Sterbende und Trauernde. Lasst uns die sozial
Schwachen stützen, die Schuldbeladenen, die Gescheiterten, die Hungernden und
die Verfolgten, die Flüchtlinge. Die Liebe bleibt. Worte des Mitgefühls bleiben.
Lasst uns aufeinander achten. Lasst uns zusammenbleiben als Familien, als
Menschen, als Nachbarn, als Menschen, als Menschen, als Menschen. Lasst uns
zusammenbleiben als Familien, als Menschen, als Menschen, als Menschen, als
Menschen, als Menschen, als Menschen, als Menschen, als Menschen, als Menschen,
als Menschen, als Menschen, als Menschen, als Menschen, als Menschen, als
Menschen, als Menschen, und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und die und die und die und die und die und die und die und die
und die und die und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das
und das und das und das und das und das und das und das und das und das und das