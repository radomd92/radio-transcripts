TRANSCRIPTION: NDR-INFO_AUDIO/2023-03-20-21H00M.MP3
--------------------------------------------------------------------------------
 Die Nachrichten. Um 21 Uhr mit Martin Wilhelmi. Der Weltklimarat hat so
deutlich wie nie angemahnt, dass im Kampf gegen die Erderwärmung zu wenig getan
wird. Um das 1,5 Grad Ziel noch zu erreichen, müssten demnach klimaschädliche
Emissionen bis 2030 drastisch heruntergefahren werden. Aus Berlin, Christopher
Jennert. Wir sägen an dem Ast, auf dem wir sitzen, sagt Außenministerin
Baerbock. Es gebe aber weiterhin Hoffnung, wenn der CO2-Ausstoß weltweit
halbiert werde, und zwar in den nächsten 7 Jahren. Die Umweltorganisation German
Watch ruft die Bundesregierung dazu auf, schneller voranzugehen. Heißt, mehr in
erneuerbare Energien investieren und, der Zitat, Rechtsbruch im Verkehrs- und
Gebäudesektor müsse beendet werden. Gemeint ist damit, dass in den beiden
Sektoren nach wie vor deutlich zu viel CO2 ausgestoßen wird, obwohl das
Klimaschutzgesetz entsprechende Maßnahmen für diesen Fall vorschreibt. Die EU-
Staaten wollen die Ukraine schnell mit frischer Munition versorgen, konkret mit
einer Million Schuss Artillerie-Munition in den nächsten 12 Monaten. Eine
entsprechende Vereinbarung haben die Außen- und Verteidigungsminister der EU-
Staaten unterzeichnet. Aus Brüssel, Stefan Überbach. 2 Mrd. Euro will die EU aus
dem Europäischen Friedensfonds mobilisieren, um der ukrainischen Armee in den
nächsten 12 Monaten eine Million Artillerie-Granaten vom Kaliber 155 mm zur
Verfügung zu stellen. Damit bündele Europa seine Marktmacht, sagte der deutsche
Verteidigungsminister Boris Pistorius, räumte aber ein, dass die EU damit
Neuland betrete. Die Geschosse sollen aus eigenen Beständen geliefert sowie mit
Sammelbestellungen zu möglichst günstigen Preisen eingekauft werden. In der
französischen Nationalversammlung sind zwei Misstrauensanträge gegen die
Regierung gescheitert. Der Opposition gelang es nicht, bei der Abstimmung die
nötigen 287 Ja-Stimmen für eine absolute Mehrheit zu erreichen. Die
Misstrauensanträge waren vom rechtsnationalen Rassemblement National und der
Zentrumspartei Lyo eingebracht worden. Hintergrund ist die umstrittene
Rentenreform. Präsident Macron hatte die schrittweise Anhebung des
Renteneintrittsalters von 62 auf 64 Jahre per Schnellverfahren durchgesetzt,
ohne das Parlament darüber abschließend entscheiden zu lassen. Die
internationale Gemeinschaft will für den Wiederaufbau der Edbebengebiete in der
Türkei und Syrien 7 Milliarden Euro bereitstellen. Das teilte der schwedische
Regierungschef Krister schon nach einer Geberkonferenz in Brüssel mit. Die EU
sagte 1 Milliarde Euro für die Türkei zu, sowie 108 Millionen Euro für
humanitäre Hilfe und den Wiederaufbau in Syrien. Deutschland verdoppelt seinen
Beitrag auf 240 Millionen Euro. Soweit die Meldungen. Das Wetter in
Norddeutschland. In der Nacht überwiegend dichte Wolken und zeitweise Regen.
Morgen erneut bewölkt mit Regen bei maximal 7 bis 12 Grad. Und das waren die
Nachrichten. Hallo und ein erleichtertes Willkommen zu unserem Satire-Magazin.
Wir waren uns ehrlich gesagt nicht ganz sicher, ob die Intensivstation in dieser
Woche stattfinden kann. Denn wir haben ja Mangelwirtschaft in Deutschland. Wo
man hinguckt, zu wenig. Aber zum Glück haben wir noch genug Satire
zusammengekratzt, konnten auf dem Flur auch noch einen Sendetechniker einfangen
und jetzt hat es doch noch geklappt. Sogar ein Mikrofon haben wir noch
aufgetrieben. Und an dem sitzt heute für Sie Torben Pölz. Ansonsten aber überall
im Land Mangel. Zu wenig Fachkräfte, zu wenig Landärzte, zu wenig Lehrkräfte und
viel zu wenig Ersatzteile. Im Grunde wie damals in der DDR. Ich brauche ein
Ersatzteil. Und da hat mir der Handwerksmeister gesagt, das kriegt er in 9
Monaten. Da habe ich gedacht, das kann doch nicht wahr sein. Das kenne ich ja
wirklich von früher. Und dann hat er mir gesagt, er hat so viele Aufträge, dass
er das doch gar nicht gleich machen kann, sondern dass ich warten muss. Das
kannte ich auch aus der DDR. Tja, wir sind wieder so weit. Mal abwarten, wie
lange es noch dauert, weil es da heute Bananen geben soll. Mangelwirtschaft. Ein
Trend, bei dem die Bundeswehr vorneweg marschiert. Das tut sie ja sonst nicht so
oft, aber beim Mangel da schon. Die Bundeswehr hat von allem zu wenig. Und seit
dem 24. Februar noch weniger. Das ist der Zustand. Sagt die Wehrbeauftragte Eva
Högl. Und die kennt sich aus. Reist im ganzen Land umher und besucht viele
Ruinen, die mal richtige Kasernen waren. 70-mal hat Högl im vergangenen Jahr die
Truppe besucht. Darunter ein Hubschrauber-Geschwader, das seit 10 Jahren auf
neue Helme wartet. In allen Truppenteilen fehlen wiederum die, die Helme tragen.
Personalmangel. Da stellt sich die Huhn-Ei-Frage. Gibt es so wenig Personal,
weil potentielle Bewerber sagen, also wenn ihr keine Helme habt, dann kommen wir
nicht. Oder gibt es so wenig Helme, weil eh kein Personal da ist, das die Dinge
aufsetzen kann. Am Geld kann es ja eigentlich nicht liegen. Wir werden ein
Sondervermögen Bundeswehr einrichten. Der Bundeshaushalt 2022 wird dieses
Sondervermögen einmalig mit 100 Mrd. Euro ausstatten. Die Mittel werden wir für
notwendige Investitionen und Rüstungsvorhaben nutzen. Das war vor über einem
Jahr. Da müssten doch inzwischen eigentlich aus einzelnen Feldbetten ganze
Bettenfelder geworden sein, oder? Ich muss leider feststellen, dass im Jahr 2022
von diesem Sondervermögen noch kein Euro und kein Cent ausgegeben wurde. Da
fragen sich nicht nur passionierte Wehrdienstverweigerer, haben die bei der
Bundeswehr noch alle Patronen am Gürtel? Nee, haben sie nicht. Weil die
inzwischen längst verschossen sind und das Beschaffungswesen eher umständlich
und bürokratisch ist. Da haben ja alle Verantwortung. Die militärische Führung,
das Bundesministerium der Verteidigung mit allen Verantwortlichen, der Deutsche
Bundestag, der ja auch beteiligt ist an den ganzen Beschaffungsprozessen, das
entsprechende Amt in Koblenz, alle drumherum haben da Verantwortung zu tragen.
Mit anderen Worten, bis jeder seinen Feldwebel-Willi unter den
Beschaffungsauftrag gesetzt hat, ist die Panzerhaubitze schon weggerostet. Das
müsste doch auch schneller gehen. In einem Bundeswehrkaufhaus z.B. Da werden ja
demnächst ein paar Flächen frei, auch in Innenstadtlage. Und die ließen sich
doch wunderbar als Kasernen nutzen. Galeria Karstadt-Kasern. Winterschussverkauf
bei uns in der Galeria Karstadt-Kaserne Hagen. Heute bezahlen und in 4 Jahren
abholen. Gilt für alles außer Kriegsnahrung. Guten Tag, wir suchen Munition für
die Panzerhaubitze 2000. Haben Sie da was? Nur noch das, was da hängt. Danke
schön. Die Truppenküche in der 4. Etage lädt Sie ein zu einer lauwarmen
Erbsensuppe aus der Gulaschkanone. Was haben Sie da? Eine Feldjacke, eine
ballistische Unterhose und 2 Socken, Herr Obergefreiter. Das sind mehr als 3
Teile in der Kabine. 200 Liegestützen in der Sportabteilung. Abmarsch.
Untergefreiter Wagner bitte zum Tischdienst in Etage 4. Untergefreiter Wagner
bitte. Entschuldigung, ich bin mit meinem Leo ins Parkhaus gefahren. Und jetzt
soll ich Ihnen das Parkticket abstempeln? Nee, ich bin stationiert hier. Ich hab
eine Dauerkarte. Ich hab beim rückwärts einparken leider ein Motorrad touchiert.
Das stand da auf dem Frauenparkplatz. Oh nee, das ist bestimmt das von der
Strakzimmermann. Die ist hier? Ja, die macht heute eine Autogrammstunde. Wo? In
der Stabsgefreite. Ein wichtiger Hinweis für alle Stabsgefreiten. Der kleine
Rekrut Hemmersbach. Sucht seinen Stabsgefreiten, der kleine Rekrut Hemmersbach.
Bitte holen Sie ihn im Stahlbad an der Information ab. Kann ich Ihnen helfen?
Ich schau mich hier nur um. Das heißt, ich schau mich hier nur um, Herr Oberst.
Doppelwummswochen. 5000 Helme gratis bei jedem Kauf in unserer
Schießwarenabteilung. Ähm, hallo? Hallo? Ich suche Tarnkleidung. Ist da jemand?
Hallo? Ja, ich bin doch hier. Äh, wo? Genau vor Ihnen. Die gesamte Kompanie
bitte zur Übung an die Rolltreppen. So, das macht dann 27,80. Sammeln Sie
Punkte, Herr General? Nein, nur Sterne. Und jetzt möchte ich zahlen. Was ist das
denn? 100 Milliarden? Da kann ich nicht rausgeben. An alle Kameraden, unsere
Galeria Karstadt-Kaserne schließt in 15 Minuten. Bitte begeben Sie sich zügig in
Ihre Stuben im Untergeschoss. Wir sind S-H-O-B-B-I-N-G. Wir shoppen. Es ist
einfach, wenn man alle Informationen hat. Inhaltliche Hilfe, keine Untersuchung.
Investigation, Investigation, Investigation. No questions in the house, no give
and take. There's a big bang in the city. We're on the make. We're S-H-O-B-B-I-
N-G. We're shopping. Our gain is your loss. That's the price you pay. I heard it
in the House of Commons. Everything's for sale. We're shopping. We're shopping.
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es!
Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen
es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir schaffen es! Wir
schaffen es!