TRANSCRIPTION: NDR-INFO_AUDIO/2023-03-22-13H00M.MP3
--------------------------------------------------------------------------------
 Für den Norden. Um 13 Uhr mit Astrid Vietz. Spezialkräfte der Polizei haben
heute früh im Zusammenhang mit Ermittlungen gegen sogenannte Reichsbürger
mehrere Wohnungen durchsucht. Die Razzien fanden in insgesamt sieben
Bundesländern statt, darunter Hamburg, Niedersachsen und Mecklenburg-Vorpommern.
Aus Berlin Michael Götchenberg. Nach Informationen von ARD Hauptstadtstudio und
SWR wurde heute morgen auch bei einem aktiven Polizisten und einem Angehörigen
der Bundeswehr durchsucht. Bei einer geplanten Durchsuchung in Reutlingen
eröffnete die Zielperson das Feuer auf die Beamten des herannahenden SEK. Die
Person wurde eigentlich nur als Zeuge in dem Verfahren geführt, gilt nun aber
auch als Beschuldigter. Ein Beamter erlitt einen glatten Durchschuss am Arm,
soll aber gesundheitlich stabil sein. Über den Einsatz in Reutlingen hinaus gab
es keine Festnahmen, sondern lediglich Durchsuchungsmaßnahmen. Im Falle des
Hamburger Amokschützen gibt es neue Informationen zu einem Buch des Mannes. Die
Waffenbehörde war durch einen anonymen Hinweis auf einen möglicherweise wirrend
Inhalt des Werks hingewiesen worden. Laut ersten Angaben der Polizei nach der
Amoktat war das Buch aber nicht gefunden worden. Wie der NDR erfahren hat,
stimmt das nicht. Aus Hamburg, Sven Kessler. Eine Mitarbeiterin der
Waffenbehörde hatte nach dem anonymen Hinweis den Buchtitel gefunden, es aber
weder bestellt noch heruntergeladen. Der Titel habe die Mitarbeiter der
Waffenbehörde nicht sonderlich alarmiert, heißt es von der Hamburger Polizei.
Sie erklärt diesen Vorfall durch einen Kommunikationsfehler. Der sei bei der
Befragung der betroffenen Mitarbeiterin entstanden. Sie wurde gefragt, ob sie
das Buch kannte. Weil sie es aber nicht gelesen hatte, sagte sie Nein. Dass sie
es gefunden hatte, erwähnte sie offenbar nicht. Zwei von der Polizei in Auftrag
gegebene Gutachter, die das Buch untersucht haben, kommen unterdessen zu dem
Schluss, dass es keine Hinweise auf eine Gewalttat enthält. Die sogenannten
Wirtschaftsweisen rechnen für dieses Jahr mit einem leichten Wirtschaftswachstum
von 0,2 Prozent. Damit korrigieren die Sachverständigen der Bundesverfassung und
der Bundesregierung ihre Konjunkturprognose vom Herbst etwas nach oben. Aus
Berlin Hans-Joachim Viehweger. Zugleich warnen die Wirtschaftsweisen, ein milder
Winter mache noch keinen Frühling. Soll heißen, auch wenn wir trotz der
Energiekrise vergleichsweise gut durch den Winter gekommen sind, eben auch wegen
der relativ milden Temperaturen. Die Probleme auf dem Energiemarkt dürften uns
noch eine Weile begleiten. Selbst eine Gasmangelage im nächsten Winter sei nicht
ausgeschlossen, heißt es. Das zweite große Problem bleibt die Inflation. Einfach
weil die höheren Preise längst nicht nur bei Benzin, Strom oder bei den
Lebensmitteln angekommen sind, sondern auch bei vielen anderen Produkten und
Dienstleistungen. Und das dürfte die Konsumlaune der Verbraucher dämpfen, sagen
die Wirtschaftsweisen. Dazu kommt noch ein drittes Problem, die ausgeprägte
Schwäche im Bausektor. Das Landgericht Münster hat im Prozess um den gewaltsamen
Tod eines Transmanns sein Urteil gesprochen. Der 20-jährige Angeklagte erhielt
eine Jugendstrafe von fünf Jahren. Das Gericht ordnete zudem an, den jungen Mann
in einer Erziehungsanstalt unterzubringen. Er wurde wegen Körperverletzung mit
Todesfolge schuldig gesprochen. Der Angeklagte hatte eingeräumt, den Transmann
im vergangenen August nach dem Christopher Street Day in Münster
niedergeschlagen zu haben. Der 25-jährige starb Tage später an den Folgen eines
Schädelhirntraumas. Die Tat hatte bundesweit für Entsetzen gesorgt und auch
Debatten um Queerfeindlichkeit ausgelöst. Die EU-Kommission hat ein sogenanntes
Recht auf Reparatur bei Elektrogeräten vorgeschlagen. Dem Entwurf nach müssen
noch etwas das EU-Parlament und die Mitgliedstaaten zustimmen. Aus Brüssel
Astrid Korall. Reparieren statt wegwerfen, so lautet die Devise der EU-
Kommission auch im Sinne des Klima- und Umweltschutzes. Die Brüsseler Behörde
schlägt deshalb vor, dass Verbraucherinnen und Verbraucher künftig das Recht
bekommen, dass defekte Geräte nicht nur innerhalb der gesetzlichen Garantiezeit
von zwei Jahren, sondern auch danach für bis zu zehn Jahre repariert werden
müssen, wenn das technisch möglich ist und die Reparatur günstiger ist als ein
Ersatz. Zu den Produkten zählen Waschmaschinen, Trockner, Kühlschränke oder
Staubsauger. In einem zweiten Schritt soll die Regelung auch für Smartphones und
Tablets gelten. Mesut Özil hat seine Karriere als Fußballprofi beendet. Der
ehemalige deutsche Nationalspieler gab seinen sofortigen Rücktritt bekannt. Özil
war 2014 mit der deutschen Stadion-Kommission und wurde im letzten Jahr mit der
deutschen Nationalmannschaft Weltmeister geworden. In der Bundesliga spielte er
für Schalke und Bremen. Zuletzt war er bei Basakir Istanbul aktiv. Soweit die
Meldungen. Von der Nordsee und der Ems her ziehen Regenschauer auf. Es gibt aber
auch trockene Abschnitte, vor allem zwischen Südniedersachsen und Vorpommern. Da
zeigt sich auch mal die Sonne. Es ist 8 bis 16 Grad. Dazu starke, teils auch
stürmische Böen. Morgen viele Wolken, teilweise auch wieder Regen. Später neben
gelegentlichen Schauern, aber auch freundliche Phasen. Die Höchstwerte morgen
wiederum 8 bis 16 Grad. Es bleibt sehr windig. Dann schauen wir noch auf den
Freitag. Erst mal Regen, später noch ein paar Schauer, aber auch ein wenig
Sonne. Und es bleibt bei 8 bis 16 Grad. NDR Info mit dem Verkehrsservice. Die A1
Bremen Richtung Hamburg ist zwischen Hollenstedt und Rade nach einem Unfall
gesperrt. 4 km Stau dadurch. Die Umleitung führt über die U 49. A7 Flensburg
Richtung Hamburg. Zwischen Hamburg-Stellingen und dem Elbtunnel 4 km stockend.
Das betrifft Lkw. Die A7 Kassel Richtung Hannover ist zwischen Latzen und der
Raststätte Hannover-Wilferode wegen Bergungsarbeiten gesperrt. 3 km Stau
zwischen Hamburg und Stellingen. 3 km Stau dadurch. Die Umleitung führt über die
A 37 und B 6 zur B 65. Und dann weiter in Richtung A 7 Hannover-Anderten. Dann
haben wir noch eine Meldung von der A 33 Osnabrück Richtung Bielefeld. Da ist
die Ausfahrt Dissen-Bad Rothenfelde nach einem Unfall gesperrt. Kommen Sie gut
an.