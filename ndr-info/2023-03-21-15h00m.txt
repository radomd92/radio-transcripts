TRANSCRIPTION: NDR-INFO_AUDIO/2023-03-21-15H00M.MP3
--------------------------------------------------------------------------------
 Um 15 Uhr mit Sönke Peters. Einem Großteil der Bäume in deutschen Wäldern geht
es schlecht. Das zeigt der aktuelle Waldzustandsbericht. Danach wiesen im Jahr
2022 mehr als ein Drittel aller Bäume deutliche Schäden an ihrer Krone auf.
Bundeslandwirtschaftsminister Estemi erklärt, welche Bäume besonders betroffen
sind. Die Fichte bleibt weiterhin das Sorgenkind Nummer eins. Wir verzeichnen
die höchste jemals gemessene Absterberate im Rahmen der Waldzustandserhebungen.
Und auch die einstigen Hoffnungsträger in der Klimakrise, die Buchen, Eichen und
Kiefern, bereiten uns Sorge. Fast die Hälfte aller Buchen weisen deutliche
Kronen-Vitalitätsverluste auf, gefolgt von der Eiche mit 40 Prozent.
Erschreckend ist der negative Trend bei der Kiefer. Nur noch 13 Prozent der
Kiefern sind voll vital. Estemi will gegensteuern. Mit einem 900-Millionen-Euro-
Programm sollen die Wälder zu Mischwäldern aufgeforstet werden. So könnten die
Bäume widerstandsfähiger werden gegenüber Trockenheit und höheren Temperaturen.
Dieselfahrerinnen und Fahrer haben unter Umständen Chancen, vom Hersteller
Schadenersatz zu bekommen, wenn in ihren Wagen unzulässige Abgas-Technik verbaut
ist. Hintergrund ist ein Urteil des Europäischen Gerichtshofs. Aus Karlsruhe,
Gigi Deppen. Der Bundesgerichtshof, Deutschlands oberstes Zivilgericht, hatte in
der Vergangenheit immer wieder gesagt, nein, es gibt keinen Schadensersatz,
falls die Abgasreinigung bei bestimmten Temperaturen runtergefahren wird. Das
sieht nun das oberste Gericht der EU anders. Es gäbe doch die Papiere beim
Neuwagenkauf, die sogenannte Konformitätsbescheinigung, die besagt, dass das
Auto den Normen der EU entspricht. Das bedeutet, nach europäischem Recht würde
direkt eine Verbindung zwischen dem Hersteller und dem einzelnen Käufer
bestehen. Deswegen müssten die Käufer auch die Möglichkeit bekommen, gegen den
Hersteller vorzugehen. Dabei ist allerdings nicht sicher, wie viel Geld es am
Ende gibt. Denn die gefahrenen Kilometer dürfen grundsätzlich mit dem
Schadensersatz verrechnet werden. Das erlaubt der EuGH. Am zweiten Tag seines
Besuchs in Moskau spricht Chinas Staatschef Xi Jinping erneut mit Russlands
Präsident Putin. Vorab betonte Xi, Peking werde der umfassenden strategischen
Partnerschaft zwischen China und Russland weiterhin Priorität einräumen. Er habe
Putin auch zu einem Besuch in China eingeladen, so Xi. Nach Kreml-Angaben wollen
Putin und Xi vor allem besprechen, wie sie ihre wirtschaftlichen Beziehungen
vertiefen können. Seit Russland von den europäischen Märkten weitgehend
ausgeschlossen ist, bemüht sich der Kreml verstärkt um Energie-Exporte nach
China. Bundesweit gibt es heute in Kliniken wegen eines Warnstreiks der
Ärztinnen und Ärzte nur eine Notversorgung. Die Gewerkschaft Marburger Bund hat
zu den Arbeitsniederlegungen aufgerufen. Betroffen sind auch Krankenhäuser im
Norden. Etwa 2000 Mediziner beteiligten sich an der Abschlusskundgebung in
Hamburg. Der Marburger Bund fordert von den Arbeitgebern 2,5 Prozent mehr Gehalt
und einen Inflationsausgleich. Ende des Monats soll es weitere Warnstreiks
geben. Die Londoner Polizei ist einer Untersuchung zufolge institutionell
rassistisch, frauenfeindlich und homophob. Erst gestern war bekannt geworden,
dass mehr als 100 Polizisten, gegen die wegen sexuellen Fehlverhaltens ermittelt
wird, regulär im Dienst sind. Aus London Gaby Biesinger. Weibliche Beschäftigte
säen sich routinemäßig mit Sexismus und Frauenfeindlichkeit konfrontiert, heißt
es in dem Bericht. Die Behörde habe ihre weiblichen Angestellten oder Mitglieder
der Öffentlichkeit weder vor Tätern in der Polizei, die häusliche Gewalt
anwenden, noch vor denen geschützt, die ihre Position für sexuelle Zwecke
missbrauchen. Die Behörde sei außerdem institutionell rassistisch. Ein
muslimischer Polizist fand Speck in seinen Schuhen, einem Sikh wurde der Bart
abgeschnitten. Weniger als eine Woche nach dem Start haben mehr als eine Million
Studierende und Fachschüler ihre Energiekostenhilfe erhalten. Laut der Website