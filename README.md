## Radio broadcast transcripts

The main objective is to provide a searchable repository of recorded radio broadcasts using [whisper-ai](https://github.com/radomd92/whisper) models. 
text files are sorted by radio stations.
